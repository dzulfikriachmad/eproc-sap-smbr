package eproc.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.joda.time.DateTime;
import org.joda.time.Seconds;

import com.eproc.utils.SessionGetter;
import com.eproc.utils.TimerEauction;
import com.eproc.x.global.command.EauctionTableHistoryModel;
import com.eproc.x.global.command.EauctionTableModel;
import com.orm.model.AdmDelegasi;
import com.orm.model.AdmUser;
import com.orm.model.EauctionHeader;
import com.orm.model.EauctionHistoryLatihan;
import com.orm.model.EauctionVendor;
import com.orm.model.PrcMainHeader;
import com.orm.model.PrcMainVendor;
import com.orm.model.PrcMainVendorQuote;
import com.orm.model.TmpVndHeader;
import com.orm.model.VndHeader;
import com.orm.tools.GenericDao;

public class EauctionServiceImpl extends GenericDao implements EauctionService {

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> listPrcMainHeaderEauction(Object object)
			throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMainHeader.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
		List<AdmDelegasi> lst = new AdmServiceImpl()
				.getListDelegasiByUserKepadaAllAktifNotAktif(SessionGetter
						.getUser());
		if (lst != null && lst.size() > 0) {
			List<AdmUser> lsu = new ArrayList<AdmUser>();
			for (AdmDelegasi d : lst) {
				lsu.add(d.getDari());
			}
			Disjunction dj = Restrictions.disjunction();
			dj.add(Restrictions.eq("admUserByPpmBuyer", SessionGetter.getUser()));
			dj.add(Restrictions.in("admUserByPpmBuyer", lsu));
			crit.add(dj);
		} else
			crit.add(Restrictions.eq("admUserByPpmBuyer",
					SessionGetter.getUser()));
		crit.add(Restrictions.eq("ppmProsesLevel", 4));// yang lagi evaluasi
		return (List<PrcMainHeader>) getListByDetachedCiteria(crit);
	}

	@Override
	public EauctionHeader eauctionHeaderByPpm(PrcMainHeader prcMainHeader)
			throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(EauctionHeader.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.idEq(prcMainHeader.getPpmId()));
		return (EauctionHeader) getEntityByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<EauctionVendor> listVendorEauctionByHeader(
			EauctionHeader eauctionHeader, PrcMainHeader prcMainHeader)
			throws Exception {
		if (eauctionHeader != null) {
			DetachedCriteria crit = DetachedCriteria
					.forClass(EauctionVendor.class);
			crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			crit.setFetchMode("vndHeader", FetchMode.JOIN);
			crit.setFetchMode("eauctionHeader", FetchMode.JOIN);
			crit.add(Restrictions.eq("eauctionHeader", eauctionHeader));
			return (List<EauctionVendor>) getListByDetachedCiteria(crit);
		} else {
			// init data
			List<PrcMainVendor> listVnd = new PrcServiceImpl()
					.listMitraKerjaLulusTeknis(prcMainHeader);
			List<EauctionVendor> retMe = new ArrayList<EauctionVendor>();
			if (listVnd != null && listVnd.size() > 0) {
				for (PrcMainVendor p : listVnd) {
					EauctionVendor ev = new EauctionVendor();
					ev.setVndHeader(p.getVndHeader());
					retMe.add(ev);
				}
			}
			return retMe;
		}
	}

	@Override
	public void saveEauction(EauctionHeader eauctionHeader,
			List<EauctionVendor> listEauctionVendor) throws Exception {
		try {
			startTransaction();
			EauctionHeader head = eauctionHeaderByPpm(eauctionHeader
					.getPrcMainHeader());
			if (head != null) {
				
					head.setAdmCurr(eauctionHeader.getAdmCurr());
					head.setBatasAtas(eauctionHeader.getBatasAtas());
					head.setBatasBawah(eauctionHeader.getBatasBawah());
					head.setDeskripsi(eauctionHeader.getDeskripsi());
					head.setHps(eauctionHeader.getHps());
					head.setJudul(eauctionHeader.getJudul());
					head.setMinimumPenurunan(eauctionHeader.getMinimumPenurunan());
					head.setTanggalMulai(eauctionHeader.getTanggalMulai());
					DateTime dt = new DateTime(eauctionHeader.getTanggalMulai());
					dt = dt.plusMinutes(eauctionHeader.getWaktu().intValue());
					head.setTanggalBerakhir(dt.toDate());
					head.setWaktu(eauctionHeader.getWaktu());
					head.setLatihan(2);
					
					saveEntity(head);
				
			} else {
				DateTime dt = new DateTime(eauctionHeader.getTanggalMulai());
				dt = dt.plusMinutes(eauctionHeader.getWaktu().intValue());
				eauctionHeader.setTanggalBerakhir(dt.toDate());
				eauctionHeader.setLatihan(2);
				saveEntity(eauctionHeader);
				head = eauctionHeader;

				// insert vendor
				List<PrcMainVendor> lppm = new PrcServiceImpl()
						.listMitraKerjaLulusTeknis(eauctionHeader
								.getPrcMainHeader());
				if (lppm != null && lppm.size() > 0) {
					for (PrcMainVendor ev : lppm) {
						PrcMainVendorQuote q = getVendorQuoteByPrcMainVendor(ev);
						EauctionVendor e = new EauctionVendor();
						e.setVndHeader(ev.getVndHeader());
						e.setEauctionHeader(eauctionHeader);
						e.setLastBid(q.getPqmTotalPenawaran());
//						e.setTglBid(q.getUpdatedDate());
						saveEntity(e);
					}
				}
			}
			
			// instance timer
			/*if (SessionGetter.TIMER_EAUCTION != null) {
				SessionGetter.TIMER_EAUCTION.cancelTimer();
			}
			if (head.getTanggalBerakhir().after(new Date())) {
				TimerEauction te = new TimerEauction(
						eauctionHeader.getTanggalMulai(), eauctionHeader
								.getWaktu().intValue(), head.getPpmId());
				te.startTimer();
				SessionGetter.TIMER_EAUCTION = te;
			}*/

			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	private PrcMainVendorQuote getVendorQuoteByPrcMainVendor(PrcMainVendor ev) {
		DetachedCriteria crit = DetachedCriteria
				.forClass(PrcMainVendorQuote.class);
		crit.add(Restrictions.eq("prcMainVendor", ev));
		return (PrcMainVendorQuote) getEntityByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<EauctionTableModel> listEauctionVendor(String listVendorEauction)
			throws Exception {
		List<EauctionTableModel> retMe = new ArrayList<EauctionTableModel>();
		try {
			startTransaction();
			Query q = session.createSQLQuery(listVendorEauction);
			List<Object[]> obj = q.list();
			if (obj != null) {
				Iterator<Object[]> it = obj.iterator();
				while (it.hasNext()) {
					Object o[] = it.next();
					it.remove();
					EauctionTableModel v = new EauctionTableModel();
					v.setId(Long.valueOf(String.valueOf(o[0])));
					v.setEauctionId(Long.valueOf(String.valueOf(o[1])));
					v.setNamaVendor(String.valueOf(o[2]));
					v.setLastBid(o[3] != null ? Double.valueOf(String
							.valueOf(o[3])) : 0.0);
					v.setTglBid(o[4] != null ? (Date) o[4] : null);
					v.setTglBidString(o[5] != null ? String.valueOf(o[5])
							: null);
					retMe.add(v);
				}
			}

			commitAndClear();
		} catch (Exception e) {
			throw e;
		}
		return retMe;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<EauctionTableHistoryModel> listEauctionHistory(
			String listHistoryEauction) throws Exception {
		List<EauctionTableHistoryModel> retMe = new ArrayList<EauctionTableHistoryModel>();
		try {
			startTransaction();
			Query q = session.createSQLQuery(listHistoryEauction);
			List<Object[]> obj = q.list();
			if (obj != null) {
				Iterator<Object[]> it = obj.iterator();
				while (it.hasNext()) {
					Object o[] = it.next();
					it.remove();
					EauctionTableHistoryModel v = new EauctionTableHistoryModel();
					v.setId(Long.valueOf(String.valueOf(o[0])));
					v.setPpmId(Long.valueOf(String.valueOf(o[1])));
					v.setVendorName(String.valueOf(o[2]));
					v.setJumlahBid(o[3] != null ? Double.valueOf(String
							.valueOf(o[3])) : 0.0);
					v.setTglBid(o[4] != null ? (Date) o[4] : null);
					v.setTglBidString(o[5] != null ? String.valueOf(o[5])
							: null);
					retMe.add(v);
				}
			}
			commitAndClear();
		} catch (Exception e) {
			throw e;
		}
		return retMe;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<EauctionHeader> listEauctionHeaderByVendorActive(
			TmpVndHeader headr) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(EauctionHeader.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.le("tanggalMulai", new Date()));
		crit.add(Restrictions.ge("tanggalBerakhir", new Date()));
		DetachedCriteria sb = crit.createCriteria("eauctionVendors",
				JoinType.INNER_JOIN);
		VndHeader h = new VndHeader();
		h.setVendorId(headr.getVendorId());
		sb.add(Restrictions.eq("vndHeader", h));
		return (List<EauctionHeader>) getListByDetachedCiteria(crit);
	}

	@Override
	public EauctionVendor eauctionVendorByHeaderByVendorId(
			EauctionHeader eauctionHeader, VndHeader vh) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(EauctionVendor.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("eauctionHeader", eauctionHeader));
		crit.add(Restrictions.eq("vndHeader", vh));
		return (EauctionVendor) getEntityByDetachedCiteria(crit);
	}

	@Override
	public Double getLowestBid(Long ppmId) throws Exception {
		Double data = 0.0;
		try {
			startTransaction();
			Query q = session
					.createSQLQuery("select min(a.last_bid) from eauction_vendor a where a.eauction_header="
							+ ppmId + " and (a.status is null or a.status <> -1) ");
			Object obj = q.uniqueResult();
			if (obj != null) {
				data = Double.valueOf(String.valueOf(obj));
			}
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
		return data;
	}
	
	@Override
	public EauctionHeader getEauctionHeaderByPpmId(Long ppmId) throws Exception {
		try {
			DetachedCriteria crit = DetachedCriteria.forClass(EauctionHeader.class);
			crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			crit.add(Restrictions.idEq(ppmId));
			return (EauctionHeader) getEntityByDetachedCiteria(crit);
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public void diskualifikasiVendor(Integer eauctionVendorId) throws Exception {
		try {
			DetachedCriteria crit = DetachedCriteria
					.forClass(EauctionVendor.class);
			crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			crit.add(Restrictions.idEq(Long.valueOf(eauctionVendorId)));
			EauctionVendor vnd = (EauctionVendor) getEntityByDetachedCiteria(crit);
			startTransaction();
			
			if(vnd.getStatus()==null){
				vnd.setStatus(-1);
			}else if(vnd.getStatus()==-1){
				vnd.setStatus(null);
			}
			saveEntity(vnd);
			
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			e.printStackTrace();
			throw e;
		}
	}

	@Override
	public void resetEauction(EauctionHeader eauctionHeader, PrcMainHeader prcMainHeader) throws Exception {
		try {
			startTransaction();
			//Delete Vendor
			List<EauctionVendor> lst = listVendorEauctionByHeader(eauctionHeader, prcMainHeader);
			
			if(lst!=null & lst.size()>0){
				for(EauctionVendor e:lst){
					//delete history
					String query = "delete from eauction_history where eauction_vendor="+e.getId();
					Query qq = session.createSQLQuery(query);
					qq.executeUpdate();
				}
				
			}
			String queryData = "delete from EAUCTION_VENDOR where EAUCTION_HEADER="+eauctionHeader.getPpmId();
		//	String queryData = "update eauction_vendor set last_bid = null, tgl_bid = null where eauction_header = " + eauctionHeader.getPpmId();
			Query q = session.createSQLQuery(queryData);
			q.executeUpdate();
			deleteEntity(eauctionHeader);
			
			String qry = "update PRC_MAIN_VENDOR_QUOTE set PQM_TOTAL_EAUCTION = null where PPM_ID="+eauctionHeader.getPpmId();
			Query qqq = session.createSQLQuery(qry);
			qqq.executeUpdate();
			
		
			
			
			
			if(SessionGetter.TIMER_EAUCTION!=null)
				SessionGetter.TIMER_EAUCTION.cancelTimer();
			commitAndClear();
			
			
		} catch (Exception e){
			RollbackAndClear();
			throw e;
		}
		
		
	
	}

	@Override
	public void latihanEauction(EauctionHeader eauctionHeader, List<EauctionVendor> listEauctionVendor)
			throws Exception {
		try {
			startTransaction();
			EauctionHeader head = eauctionHeaderByPpm(eauctionHeader
					.getPrcMainHeader());
			if (head != null) {
				head.setAdmCurr(eauctionHeader.getAdmCurr());
				head.setBatasAtas(eauctionHeader.getBatasAtas());
				head.setBatasBawah(eauctionHeader.getBatasBawah());
				head.setDeskripsi(eauctionHeader.getDeskripsi());
				head.setHps(eauctionHeader.getHps());
				head.setJudul(eauctionHeader.getJudul());
				head.setMinimumPenurunan(eauctionHeader.getMinimumPenurunan());
				head.setTanggalMulai(eauctionHeader.getTanggalMulai());
				DateTime dt = new DateTime(eauctionHeader.getTanggalMulai());
				dt = dt.plusMinutes(eauctionHeader.getWaktu().intValue());
				head.setTanggalBerakhir(dt.toDate());
				head.setWaktu(eauctionHeader.getWaktu());
				head.setLatihan(1);
				saveEntity(head);
			} else {
				DateTime dt = new DateTime(eauctionHeader.getTanggalMulai());
				dt = dt.plusMinutes(eauctionHeader.getWaktu().intValue());
				eauctionHeader.setTanggalBerakhir(dt.toDate());
				eauctionHeader.setLatihan(1);
				saveEntity(eauctionHeader);
				head = eauctionHeader;

				// insert vendor
				List<PrcMainVendor> lppm = new PrcServiceImpl()
						.listMitraKerjaLulusTeknis(eauctionHeader
								.getPrcMainHeader());
				if (lppm != null && lppm.size() > 0) {
					for (PrcMainVendor ev : lppm) {
						PrcMainVendorQuote q = getVendorQuoteByPrcMainVendor(ev);
						EauctionVendor e = new EauctionVendor();
						e.setVndHeader(ev.getVndHeader());
						e.setEauctionHeader(eauctionHeader);
						e.setLastBid(q.getPqmTotalPenawaran());
//						e.setTglBid(q.getUpdatedDate());
						saveEntity(e);
					}
				}
			}

			// instance timer
			if (SessionGetter.TIMER_EAUCTION != null) {
				SessionGetter.TIMER_EAUCTION.cancelTimer();
			}
			if (head.getTanggalBerakhir().after(new Date())) {
				TimerEauction te = new TimerEauction(
						eauctionHeader.getTanggalMulai(), eauctionHeader
								.getWaktu().intValue(), head.getPpmId());
				te.startTimer();
				SessionGetter.TIMER_EAUCTION = te;
			}
			
			commitAndClear();
		
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
		

	}
	@Override
	public void pauseEauction(EauctionHeader eauctionHeader, PrcMainHeader prcMainHeader) throws Exception {
		try {
			
			if(SessionGetter.TIMER_EAUCTION!=null)
				SessionGetter.TIMER_EAUCTION.paused();
			//commitAndClear();
			
			
			
		} catch (Exception e){
			//RollbackAndClear();
			throw e;
		}	
	}

	@Override
	public void resumeEauction(EauctionHeader eauctionHeader, PrcMainHeader prcMainHeader) throws Exception {
		try {
		
			if(SessionGetter.TIMER_EAUCTION!=null)
				SessionGetter.TIMER_EAUCTION.resume();
			//commitAndClear();
			
			
		} catch (Exception e){
			//RollbackAndClear();
			throw e;
		}	
	}
	
	@Override
	public void saveLatihan(EauctionHeader eauctionHeader, List<EauctionVendor> listEauctionVendor)
			throws Exception {
		try {
			startTransaction();
			
			
			String qr;
			Query query;
			qr = "insert into EAUCTION_HEADER_LATIHAN select * from EAUCTION_HEADER where PPM_ID = "+eauctionHeader.getPpmId();
			query  = session.createSQLQuery(qr);
			query.executeUpdate();
			
			qr = "insert into eauction_vendor_latihan select * from eauction_vendor where eauction_header = " + eauctionHeader.getPpmId();
			query  = session.createSQLQuery(qr);
			query.executeUpdate();
			
			
			List<EauctionVendor> lev = new PrcServiceImpl()
					.listVndEauctionVendor(eauctionHeader);
			if (lev != null && lev.size() > 0) {
				for (EauctionVendor ev : lev) {
					EauctionHistoryLatihan ehl = new EauctionHistoryLatihan();
					ehl.setEauctionVendor(ev);
					ehl.setJumlahBid(ev.getLastBid().doubleValue());
					ehl.setTglBid(ev.getTglBid());
					saveEntity(ehl);					
				}
			}
			
			commitAndClear();
			
		}catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public Integer getTimeEauction(Long ppmId, Date end) {
		try {
			Integer remaining = 0;
			Date sysDate = new Date();
			System.out.println(">>>" + sysDate);
			DateTime systemDate = new DateTime(sysDate);
			DateTime endDate = new DateTime (end);
			Seconds sec = Seconds.secondsBetween(systemDate, endDate);
		
			remaining = sec.getSeconds();
			if(remaining > 0) {
				return remaining;
			}else {
				return 0;
			}
		}catch(Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	@Override
	public Integer countdownTime(Integer remaining) {
		Integer rem = remaining;
		if(rem<=0) {
			rem = 0;
		}else {
			rem = remaining-1;
		}
		return rem;
	}
	
	@Override
	public Date getDatabaseTime() throws Exception, ParseException {
		startTransaction();
		String query = "select to_char(SYSDATE, 'yyyy-mm-dd hh24:MI:ss') from dual";
		Query q =  session.createSQLQuery(query);
		String dateString = (String) q.uniqueResult();
		commitAndClear();
		return new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(dateString);
	}
	
	
}
		



