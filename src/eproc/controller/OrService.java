package eproc.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.orm.model.AdmJenis;
import com.orm.model.ComGroup;
import com.orm.model.HistMainComment;
import com.orm.model.OrDetail;
import com.orm.model.OrHeader;
import com.orm.model.OrHeaderId;
import com.orm.model.PrcMainHeader;

public interface OrService {

	/**
	 * count active or
	 * @param object
	 * @return
	 * @throws Exception
	 */
	int countActiveOr(Object object) throws Exception;

	/**
	 * List Active OR
	 * @param object
	 * @param page
	 * @param i
	 * @return
	 * @throws Exception
	 */
	List<OrHeader> listActiveOrHeader(Object object, int page, int i) throws Exception;
	
	List<OrHeader> listActiveOrHeader(Object object) throws Exception;

	/**
	 * OR Header by ID
	 * @param id
	 * @return
	 * @throws Exception
	 */
	OrHeader getOrHeaderById(OrHeaderId id) throws Exception;

	/**
	 * List Or detail by OrHeader
	 * @param orHeader
	 * @return
	 * @throws Exception
	 */
	List<OrDetail> getOrDetailByOrHeader(OrHeader orHeader) throws Exception;

	/**
	 * list commodity Group
	 * @param object
	 * @return
	 * @throws Exception
	 */
	List<ComGroup> getListComGrup(Object object) throws Exception;

	/**
	 * generate PR from OR
	 * @param orHeader
	 * @param prcMainHeader
	 * @throws Exception
	 */
	void generatePr(OrHeader orHeader, PrcMainHeader prcMainHeader) throws Exception;

	/**
	 * 
	 * @param orHeader
	 * @param prcMainHeader
	 * @throws Exception
	 */
	void batalOR(OrHeader orHeader, PrcMainHeader prcMainHeader) throws Exception;

	List<AdmJenis> getListAdmJenis(String itemCategory);

	//AdmJenis getAdmJenisById(Integer id);
	
	void generatePrSap(OrHeader orHeader, PrcMainHeader prcMainHeader) throws Exception;

	void getDocument(OrHeader orHeader, List<OrDetail> listOrDetail) throws IOException;

	void prManualDispatcher(OrHeader orHeader, PrcMainHeader prcMainHeader, List<File> prcManDocument,
			List<String> prcManDocumentCategory, HistMainComment histMainComment, List<String> prcManDocumentDescription,
			List<String> prcManDocumentFileName, File docsKomentar,
			String docsKomentarFileName) throws Exception;
}
