package eproc.controller;

import java.util.List;

import javax.persistence.FetchType;

import org.hibernate.FetchMode;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.eproc.interceptor.OrmVndInterceptor;
import com.eproc.utils.EmailSessionBean;
import com.eproc.utils.SessionGetter;
import com.orm.model.AdmAddType;
import com.orm.model.AdmCompanyType;
import com.orm.model.AdmCountry;
import com.orm.model.TmpVndAddress;
import com.orm.model.TmpVndAkta;
import com.orm.model.TmpVndBank;
import com.orm.model.TmpVndBoard;
import com.orm.model.TmpVndCert;
import com.orm.model.TmpVndCompanyType;
import com.orm.model.TmpVndCv;
import com.orm.model.TmpVndDistrict;
import com.orm.model.TmpVndDomisili;
import com.orm.model.TmpVndEquip;
import com.orm.model.TmpVndFinRpt;
import com.orm.model.TmpVndHeader;
import com.orm.model.TmpVndHistory;
import com.orm.model.TmpVndIjin;
import com.orm.model.TmpVndProduct;
import com.orm.model.TmpVndSdm;
import com.orm.model.TmpVndTambahan;
import com.orm.tools.GenericDao;

public class VndRegServiceImpl extends GenericDao implements VndRegService {

	@SuppressWarnings("unchecked")
	@Override
	public List<AdmCompanyType> listCompanyType() {
		DetachedCriteria dc = DetachedCriteria.forClass(AdmCompanyType.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		
		return (List<AdmCompanyType>) getListByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AdmAddType> listAddType() {
		DetachedCriteria dc = DetachedCriteria.forClass(AdmAddType.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		
		return (List<AdmAddType>) getListByDetachedCiteria(dc);
	}

	@Override
	public boolean saveOrUpdateTmpVndCompanyType(TmpVndCompanyType type) {
		boolean ret = false;
		try {
			startTransaction(new OrmVndInterceptor());
			ret = saveEntity(type);
			commitAndClear();
		} catch (Exception e) {
			e.printStackTrace();
			RollbackAndClear();
			return ret;
		}
		return ret;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TmpVndCompanyType> getTmpCompanyTypeByVndHeader(TmpVndHeader vndHeader) {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndCompanyType.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admCompanyType", FetchMode.JOIN);
		dc.add(Restrictions.eq("tmpVndHeader.id", vndHeader.getVendorId()));
		
		return (List<TmpVndCompanyType>) getListByDetachedCiteria(dc);
	}

	@Override
	public boolean deleteTmpVndCompanyTypeByVndHeader(TmpVndHeader tmp) {
		boolean ret = false;
		String hql = "delete TmpVndCompanyType where tmpVndHeader.vendorId = '" + tmp.getVendorId() + "'";

		try {
			startTransaction();
			ret = executeByHql(hql);
			commitAndClear();
		} catch (Exception e) {
			e.printStackTrace();
			RollbackAndClear();
			return ret;
		}
		return ret;
	}

	@Override
	public boolean saveNextPage(int page, TmpVndHeader vndHeader) {
		boolean ret = false;
		String hql = "update TmpVndHeader set vendorNextPage = '" + page + "' where vendorId = '" + vndHeader.getVendorId() + "'";
		if(page==10){
			if(vndHeader.getVendorStatus().trim().equalsIgnoreCase("E")){
				hql = "update TmpVndHeader set vendorNextPage = '" + page + "' , vendorStatus='EE'  where vendorId = '" + vndHeader.getVendorId() + "'";
			}else
				hql = "update TmpVndHeader set vendorNextPage = '" + page + "' , vendorStatus='P'  where vendorId = '" + vndHeader.getVendorId() + "'";
			try {
				String msg = SessionGetter.getPropertiesValue("vnd.reg.email.finish", null);
				//send Email
				EmailSessionBean.sendMail("eProc PT. Semen Baturaja (Persero)", msg, vndHeader.getVendorEmail());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		try {
			startTransaction();
			ret = executeByHql(hql);
			commitAndClear();
		} catch (Exception e) {
			e.printStackTrace();
			RollbackAndClear();
			return ret;
		}
		return ret;
	}

	@Override
	public boolean saveOrUpdateTmpVndAdd(TmpVndAddress tmpVndAdd) {
		boolean ret = false;
		try {
			startTransaction(new OrmVndInterceptor());
			ret = saveEntity(tmpVndAdd);
			commitAndClear();
		} catch (Exception e) {
			e.printStackTrace();
			RollbackAndClear();
			return ret;
		}
		return ret;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TmpVndAddress> listTmpVndAddByVndHeader(TmpVndHeader vndHeader) {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndAddress.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admAddType", FetchMode.JOIN);
		dc.setFetchMode("admCountry", FetchMode.JOIN);
		
		dc.add(Restrictions.eq("tmpVndHeader.id", vndHeader.getVendorId()));
		
		return (List<TmpVndAddress>) getListByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AdmCountry> listAdmCountry() {
		DetachedCriteria dc = DetachedCriteria.forClass(AdmCountry.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
				
		return (List<AdmCountry>) getListByDetachedCiteria(dc);
	}

	@Override
	public boolean deleteTmpVndAdd(TmpVndAddress tmpVndAdd) {
		boolean ret = false;
		try {
			startTransaction(new OrmVndInterceptor());
			ret = deleteEntity(tmpVndAdd);
			commitAndClear();
		} catch (Exception e) {
			e.printStackTrace();
			RollbackAndClear();
			return ret;
		}
		return ret;
	}

	@Override
	public TmpVndAddress getTmpVndAddById(long id, TmpVndHeader vndHeader) {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndAddress.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.add(Restrictions.eq("id", id));
		dc.add(Restrictions.eq("tmpVndHeader", vndHeader));
		
		return (TmpVndAddress) getEntityByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TmpVndAkta> listTmpVndAkta(TmpVndHeader vndHeader) {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndAkta.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		
		dc.add(Restrictions.eq("tmpVndHeader.id", vndHeader.getVendorId()));
		
		return (List<TmpVndAkta>) getListByDetachedCiteria(dc);
	}

	@Override
	public boolean saveOrUpdateTmpVndAkta(TmpVndAkta tmpVndAkta) {
		boolean ret = false;
		try {
			startTransaction(new OrmVndInterceptor());
			ret = saveEntity(tmpVndAkta);
			commitAndClear();
		} catch (Exception e) {
			e.printStackTrace();
			RollbackAndClear();
			return ret;
		}
		return ret;
	}

	@Override
	public boolean deleteTmpVndAkta(TmpVndAkta tmpVndAkta) {
		boolean ret = false;
		try {
			startTransaction();
			ret = deleteEntity(tmpVndAkta);
			commitAndClear();
		} catch (Exception e) {
			e.printStackTrace();
			RollbackAndClear();
			return ret;
		}
		return ret;
	}

	@Override
	public TmpVndAkta getTmpVndAktaById(long id, TmpVndHeader vndHeader) {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndAkta.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.add(Restrictions.eq("id", id));
		dc.add(Restrictions.eq("tmpVndHeader", vndHeader));
		
		return (TmpVndAkta) getEntityByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TmpVndDomisili> listTmpVndDom(TmpVndHeader vndHeader) {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndDomisili.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admCountry", FetchMode.JOIN);
		
		dc.add(Restrictions.eq("tmpVndHeader.id", vndHeader.getVendorId()));
		
		return (List<TmpVndDomisili>) getListByDetachedCiteria(dc);
	}

	@Override
	public boolean saveOrUpdateTmpVndDom(TmpVndDomisili tmpVndDom) {
		boolean ret = false;
		try {
			startTransaction(new OrmVndInterceptor());
			ret = saveEntity(tmpVndDom);
			commitAndClear();
		} catch (Exception e) {
			e.printStackTrace();
			RollbackAndClear();
			return ret;
		}
		return ret;
	}

	@Override
	public boolean deleteTmpVndDom(TmpVndDomisili tmpVndDom) {
		boolean ret = false;
		try {
			startTransaction();
			ret = deleteEntity(tmpVndDom);
			commitAndClear();
		} catch (Exception e) {
			e.printStackTrace();
			RollbackAndClear();
			return ret;
		}
		return ret;
	}

	@Override
	public TmpVndDomisili getTmpVndDomById(long id, TmpVndHeader vndHeader) {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndDomisili.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.add(Restrictions.eq("id", id));
		dc.add(Restrictions.eq("tmpVndHeader", vndHeader));
		
		return (TmpVndDomisili) getEntityByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TmpVndBoard> listTmpVndBoardKomisaris(TmpVndHeader tmpVndHeader) {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndBoard.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admCountry", FetchMode.JOIN);
		
		dc.add(Restrictions.eq("tmpVndHeader.id", tmpVndHeader.getVendorId()));
		dc.add(Restrictions.eq("boardTipe", 2));
		
		return (List<TmpVndBoard>) getListByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TmpVndBoard> listTmpVndBoardDireksi(TmpVndHeader tmpVndHeader) {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndBoard.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admCountry", FetchMode.JOIN);
		
		dc.add(Restrictions.eq("tmpVndHeader.id", tmpVndHeader.getVendorId()));
		dc.add(Restrictions.eq("boardTipe", 1));
		
		return (List<TmpVndBoard>) getListByDetachedCiteria(dc);
	}

	@Override
	public boolean saveOrUpdateTmpVndBoard(TmpVndBoard tmpVndBoard) {
		boolean ret = false;
		try {
			startTransaction(new OrmVndInterceptor());
			ret = saveEntity(tmpVndBoard);
			commitAndClear();
		} catch (Exception e) {
			e.printStackTrace();
			RollbackAndClear();
			return ret;
		}
		return ret;
	}

	@Override
	public boolean deleteTmpVndBoard(TmpVndBoard tmpVndBoard) {
		boolean ret = false;
		try {
			startTransaction();
			ret = deleteEntity(tmpVndBoard);
			commitAndClear();
		} catch (Exception e) {
			e.printStackTrace();
			RollbackAndClear();
			return ret;
		}
		return ret;
	}

	@Override
	public TmpVndBoard getTmpVndBoardById(long id, TmpVndHeader vndHeader) {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndBoard.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.add(Restrictions.eq("id", id));
		dc.add(Restrictions.eq("tmpVndHeader", vndHeader));
		
		return (TmpVndBoard) getEntityByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TmpVndBank> listTmpVndBank(TmpVndHeader tmpVndHeader) {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndBank.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.add(Restrictions.eq("tmpVndHeader.id", tmpVndHeader.getVendorId()));
		
		return (List<TmpVndBank>) getListByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TmpVndFinRpt> listTmpVndFinRpt(TmpVndHeader tmpVndHeader) {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndFinRpt.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admCurrency", FetchMode.JOIN);
		
		dc.add(Restrictions.eq("tmpVndHeader.id", tmpVndHeader.getVendorId()));
		
		return (List<TmpVndFinRpt>) getListByDetachedCiteria(dc);
	}

	@Override
	public boolean saveOrUpdateTmpVndBank(TmpVndBank tmpVndBank) {
		boolean ret = false;
		try {
			startTransaction(new OrmVndInterceptor());
			ret = saveEntity(tmpVndBank);
			commitAndClear();
		} catch (Exception e) {
			e.printStackTrace();
			RollbackAndClear();
			return ret;
		}
		return ret;
	}

	@Override
	public boolean saveOrUpdateTmpVndFinRpt(TmpVndFinRpt tmpVndFinRpt) {
		boolean ret = false;
		try {
			startTransaction(new OrmVndInterceptor());
			ret = saveEntity(tmpVndFinRpt);
			commitAndClear();
		} catch (Exception e) {
			e.printStackTrace();
			RollbackAndClear();
			return ret;
		}
		return ret;
	}

	@Override
	public boolean deleteTmpVndBank(TmpVndBank tmpVndBank) {
		boolean ret = false;
		try {
			startTransaction();
			ret = deleteEntity(tmpVndBank);
			commitAndClear();
		} catch (Exception e) {
			e.printStackTrace();
			RollbackAndClear();
			return ret;
		}
		return ret;
	}

	@Override
	public boolean deleteTmpVndFinRpt(TmpVndFinRpt tmpVndFinRpt) {
		boolean ret = false;
		try {
			startTransaction();
			ret = deleteEntity(tmpVndFinRpt);
			commitAndClear();
		} catch (Exception e) {
			e.printStackTrace();
			RollbackAndClear();
			return ret;
		}
		return ret;
	}

	@Override
	public TmpVndBank getTmpVndBankById(long id, TmpVndHeader tmpVndHeader) {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndBank.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.add(Restrictions.eq("id", id));
		dc.add(Restrictions.eq("tmpVndHeader", tmpVndHeader));
		
		return (TmpVndBank) getEntityByDetachedCiteria(dc);
	}

	@Override
	public TmpVndFinRpt getTmpVndFinRptById(long id, TmpVndHeader tmpVndHeader) {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndFinRpt.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admCurrency", FetchMode.JOIN);
		
		dc.add(Restrictions.eq("id", id));
		dc.add(Restrictions.eq("tmpVndHeader", tmpVndHeader));
		
		return (TmpVndFinRpt) getEntityByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TmpVndCert> listTmpVndCert(TmpVndHeader tmpVndHeader) {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndCert.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.add(Restrictions.eq("tmpVndHeader.id", tmpVndHeader.getVendorId()));
		
		return (List<TmpVndCert>) getListByDetachedCiteria(dc);
	}

	@Override
	public boolean saveOrUpdateTmpVndCert(TmpVndCert tmpVndCert) {
		boolean ret = false;
		try {
			startTransaction(new OrmVndInterceptor());
			ret = saveEntity(tmpVndCert);
			commitAndClear();
		} catch (Exception e) {
			e.printStackTrace();
			RollbackAndClear();
			return ret;
		}
		return ret;
	}

	@Override
	public boolean deleteTmpVndCert(TmpVndCert tmpVndCert) {
		boolean ret = false;
		try {
			startTransaction();
			ret = deleteEntity(tmpVndCert);
			commitAndClear();
		} catch (Exception e) {
			e.printStackTrace();
			RollbackAndClear();
			return ret;
		}
		return ret;
	}

	@Override
	public TmpVndCert getTmpVndCertById(long id, TmpVndHeader tmpVndHeader) {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndCert.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.add(Restrictions.eq("id", id));
		dc.add(Restrictions.eq("tmpVndHeader", tmpVndHeader));
		
		return (TmpVndCert) getEntityByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TmpVndSdm> listTmpVndSdm(TmpVndHeader tmpVndHeader) {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndSdm.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.add(Restrictions.eq("tmpVndHeader.id", tmpVndHeader.getVendorId()));
		
		return (List<TmpVndSdm>) getListByDetachedCiteria(dc);
	}

	@Override
	public boolean saveOrUpdateTmpVndSdm(TmpVndSdm tmpVndSdm) {
		boolean ret = false;
		try {
			startTransaction(new OrmVndInterceptor());
			ret = saveEntity(tmpVndSdm);
			commitAndClear();
		} catch (Exception e) {
			e.printStackTrace();
			RollbackAndClear();
			return ret;
		}
		return ret;
	}

	@Override
	public boolean deleteTmpVndSdm(TmpVndSdm tmpVndSdm) {
		boolean ret = false;
		try {
			startTransaction();
			ret = deleteEntity(tmpVndSdm);
			commitAndClear();
		} catch (Exception e) {
			e.printStackTrace();
			RollbackAndClear();
			return ret;
		}
		return ret;
	}

	@Override
	public TmpVndSdm getTmpVndSdmById(long id, TmpVndHeader tmpVndHeader) {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndSdm.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.add(Restrictions.eq("id", id));
		dc.add(Restrictions.eq("tmpVndHeader", tmpVndHeader));
		
		return (TmpVndSdm) getEntityByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TmpVndEquip> listTmpVndEquip(TmpVndHeader tmpVndHeader) {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndEquip.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.add(Restrictions.eq("tmpVndHeader.id", tmpVndHeader.getVendorId()));
		
		return (List<TmpVndEquip>) getListByDetachedCiteria(dc);
	}

	@Override
	public boolean saveOrUpdateTmpVndEquip(TmpVndEquip tmpVndEquip) {
		boolean ret = false;
		try {
			startTransaction(new OrmVndInterceptor());
			ret = saveEntity(tmpVndEquip);
			commitAndClear();
		} catch (Exception e) {
			e.printStackTrace();
			RollbackAndClear();
			return ret;
		}
		return ret;
	}

	@Override
	public boolean deleteTmpVndEquip(TmpVndEquip tmpVndEquip) {
		boolean ret = false;
		try {
			startTransaction();
			ret = deleteEntity(tmpVndEquip);
			commitAndClear();
		} catch (Exception e) {
			e.printStackTrace();
			RollbackAndClear();
			return ret;
		}
		return ret;
	}

	@Override
	public TmpVndEquip getTmpVndEquipById(long id, TmpVndHeader tmpVndHeader) {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndEquip.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.add(Restrictions.eq("id", id));
		dc.add(Restrictions.eq("tmpVndHeader", tmpVndHeader));
		
		return (TmpVndEquip) getEntityByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TmpVndCv> listTmpVndCv(TmpVndHeader tmpVndHeader) {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndCv.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admCurrency", FetchMode.JOIN);
		dc.add(Restrictions.eq("tmpVndHeader.id", tmpVndHeader.getVendorId()));
		
		return (List<TmpVndCv>) getListByDetachedCiteria(dc);
	}

	@Override
	public boolean saveOrUpdateTmpVndCv(TmpVndCv tmpVndCv) {
		boolean ret = false;
		try {
			startTransaction(new OrmVndInterceptor());
			ret = saveEntity(tmpVndCv);
			commitAndClear();
		} catch (Exception e) {
			e.printStackTrace();
			RollbackAndClear();
			return ret;
		}
		return ret;
	}

	@Override
	public boolean deleteTmpVndCv(TmpVndCv tmpVndCv) {
		boolean ret = false;
		try {
			startTransaction();
			ret = deleteEntity(tmpVndCv);
			commitAndClear();
		} catch (Exception e) {
			e.printStackTrace();
			RollbackAndClear();
			return ret;
		}
		return ret;
	}

	@Override
	public TmpVndCv getTmpVndCvById(long id, TmpVndHeader tmpVndHeader) {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndCv.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.add(Restrictions.eq("id", id));
		dc.add(Restrictions.eq("tmpVndHeader", tmpVndHeader));
		
		return (TmpVndCv) getEntityByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TmpVndProduct> listTmpVndProduct(TmpVndHeader tmpVndHeader) {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndProduct.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("comCatalog", FetchMode.JOIN);
		dc.setFetchMode("comGroup", FetchMode.JOIN);
		dc.add(Restrictions.eq("tmpVndHeader.id", tmpVndHeader.getVendorId()));
		
		return (List<TmpVndProduct>) getListByDetachedCiteria(dc);
	}

	@Override
	public boolean saveOrUpdateTmpVndProduct(TmpVndProduct tmpVndProduct) {
		boolean ret = false;
		try {
			startTransaction(new OrmVndInterceptor());
			ret = saveEntity(tmpVndProduct);
			commitAndClear();
		} catch (Exception e) {
			e.printStackTrace();
			RollbackAndClear();
			return ret;
		}
		return ret;
	}

	@Override
	public boolean deleteTmpVndProduct(TmpVndProduct tmpVndProduct) {
		boolean ret = false;
		try {
			startTransaction();
			ret = deleteEntity(tmpVndProduct);
			commitAndClear();
		} catch (Exception e) {
			e.printStackTrace();
			RollbackAndClear();
			return ret;
		}
		return ret;
	}

	@Override
	public TmpVndProduct getTmpVndProductById(long id, TmpVndHeader tmpVndHeader) {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndProduct.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.add(Restrictions.eq("id", id));
		dc.add(Restrictions.eq("tmpVndHeader", tmpVndHeader));
		
		return (TmpVndProduct) getEntityByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TmpVndDistrict> listTmpVndDistrict(TmpVndHeader tmpVndHeader) {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndDistrict.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admDistrict", FetchMode.JOIN);
		dc.add(Restrictions.eq("tmpVndHeader.id", tmpVndHeader.getVendorId()));
		
		return (List<TmpVndDistrict>) getListByDetachedCiteria(dc);
	}

	@Override
	public boolean saveOrUpdateTmpVndDistrict(TmpVndDistrict tmpVndDistrict) {
		boolean ret = false;
		try {
			startTransaction(new OrmVndInterceptor());
			ret = saveEntity(tmpVndDistrict);
			commitAndClear();
		} catch (Exception e) {
			e.printStackTrace();
			RollbackAndClear();
			return ret;
		}
		return ret;
	}

	@Override
	public boolean deleteTmpVndDistrictByTmpVndHeader(TmpVndHeader tmpVndHeader) {
		boolean ret = false;
		String hql = "delete TmpVndDistrict where tmpVndHeader.vendorId = '" + tmpVndHeader.getVendorId() + "'";

		try {
			startTransaction();
			ret = executeByHql(hql);
			commitAndClear();
		} catch (Exception e) {
			e.printStackTrace();
			RollbackAndClear();
			return ret;
		}
		return ret;
	}

	@Override
	public boolean updateStatusTmpVndHeader(String status, TmpVndHeader vndHeader) {
		boolean ret = false;
		String hql = "update TmpVndHeader set vendorStatus = '" + status + "' where vendorId = '" + vndHeader.getVendorId() + "'";

		try {
			startTransaction();
			ret = executeByHql(hql);
			commitAndClear();
		} catch (Exception e) {
			e.printStackTrace();
			RollbackAndClear();
			return ret;
		}
		return ret;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TmpVndTambahan> listTmpVndTambahanHeader(TmpVndHeader tmp) {
		DetachedCriteria crit = DetachedCriteria.forClass(TmpVndTambahan.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("negara", FetchMode.JOIN);
		crit.add(Restrictions.eq("vendorId", tmp));
		return (List<TmpVndTambahan>)getListByDetachedCiteria(crit);
	}

	@Override
	public void saveTmpVndTambahan(TmpVndTambahan tmpVndTambahan)
			throws Exception {
		try {
			startTransaction();
			saveEntity(tmpVndTambahan);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public TmpVndTambahan getTmpVndTambahanById(int id,
			TmpVndHeader tmpVndHeader) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(TmpVndTambahan.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("negara", FetchMode.JOIN);
		crit.add(Restrictions.idEq(id));
		crit.add(Restrictions.eq("vendorId", tmpVndHeader));
		return (TmpVndTambahan) getEntityByDetachedCiteria(crit);
	}

	@Override
	public boolean deleteTmpVndTambahan(TmpVndTambahan tmpVndTambahan)
			throws Exception {
		try {
			startTransaction();
			deleteEntity(tmpVndTambahan);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			return false;
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TmpVndIjin> listTmpVndIjin(TmpVndHeader tmp) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(TmpVndIjin.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("tmpVndHeader", tmp));
		return (List<TmpVndIjin>) getListByDetachedCiteria(crit);
	}

	@Override
	public TmpVndIjin getTmpVndIjinById(int id) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(TmpVndIjin.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.idEq(id));
		return (TmpVndIjin) getEntityByDetachedCiteria(crit);
	}

	@Override
	public boolean deleteTmpVndIjin(TmpVndIjin tmpVndIjinById) throws Exception {
		try {
			startTransaction();
			deleteEntity(tmpVndIjinById);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			return false;
		}
		return true;
	}

	@Override
	public boolean saveOrUpdateTmpVndIjin(TmpVndIjin tmpVndIjin)
			throws Exception {
		try {
			startTransaction();
			saveEntity(tmpVndIjin);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			return false;
		}
		return true;
	}

}
