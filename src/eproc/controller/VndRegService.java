package eproc.controller;

import java.util.List;

import com.orm.model.AdmAddType;
import com.orm.model.AdmCompanyType;
import com.orm.model.AdmCountry;
import com.orm.model.TmpVndAddress;
import com.orm.model.TmpVndAkta;
import com.orm.model.TmpVndBank;
import com.orm.model.TmpVndBoard;
import com.orm.model.TmpVndCert;
import com.orm.model.TmpVndCompanyType;
import com.orm.model.TmpVndCv;
import com.orm.model.TmpVndDistrict;
import com.orm.model.TmpVndDomisili;
import com.orm.model.TmpVndEquip;
import com.orm.model.TmpVndFinRpt;
import com.orm.model.TmpVndHeader;
import com.orm.model.TmpVndIjin;
import com.orm.model.TmpVndProduct;
import com.orm.model.TmpVndSdm;
import com.orm.model.TmpVndTambahan;

public interface VndRegService {
	public List<AdmCompanyType> listCompanyType();
	public List<AdmAddType> listAddType();
	public List<AdmCountry> listAdmCountry();
	public List<TmpVndAddress> listTmpVndAddByVndHeader(TmpVndHeader vndHeader);
	public List<TmpVndCompanyType> getTmpCompanyTypeByVndHeader(TmpVndHeader vndHeader);
	public List<TmpVndAkta> listTmpVndAkta(TmpVndHeader vndHeader);
	public List<TmpVndDomisili> listTmpVndDom(TmpVndHeader vndHeader);
	public List<TmpVndBoard> listTmpVndBoardKomisaris(TmpVndHeader tmpVndHeader);
	public List<TmpVndBoard> listTmpVndBoardDireksi(TmpVndHeader tmpVndHeader);
	public List<TmpVndBank> listTmpVndBank(TmpVndHeader tmpVndHeader);
	public List<TmpVndFinRpt> listTmpVndFinRpt(TmpVndHeader tmpVndHeader);
	public List<TmpVndCert> listTmpVndCert(TmpVndHeader tmpVndHeader);
	public List<TmpVndSdm> listTmpVndSdm(TmpVndHeader tmpVndHeader);
	public List<TmpVndEquip> listTmpVndEquip(TmpVndHeader tmpVndHeader);
	public List<TmpVndCv> listTmpVndCv(TmpVndHeader tmpVndHeader);
	public List<TmpVndProduct> listTmpVndProduct(TmpVndHeader tmpVndHeader);
	public List<TmpVndDistrict> listTmpVndDistrict(TmpVndHeader tmpVndHeader);
	
	public boolean saveOrUpdateTmpVndAdd(TmpVndAddress tmpVndAdd);
	public boolean saveOrUpdateTmpVndCompanyType(TmpVndCompanyType type); 
	public boolean saveOrUpdateTmpVndAkta(TmpVndAkta tmpVndAkta);
	public boolean saveOrUpdateTmpVndDom(TmpVndDomisili tmpVndDom);
	public boolean saveOrUpdateTmpVndBoard(TmpVndBoard tmpVndBoard);
	public boolean saveOrUpdateTmpVndBank(TmpVndBank tmpVndBank);
	public boolean saveOrUpdateTmpVndFinRpt(TmpVndFinRpt tmpVndFinRpt);
	public boolean saveOrUpdateTmpVndCert(TmpVndCert tmpVndCert);
	public boolean saveOrUpdateTmpVndSdm(TmpVndSdm tmpVndSdm);
	public boolean saveOrUpdateTmpVndEquip(TmpVndEquip tmpVndEquip);
	public boolean saveOrUpdateTmpVndCv(TmpVndCv tmpVndCv);
	public boolean saveOrUpdateTmpVndProduct(TmpVndProduct tmpVndProduct);
	public boolean saveOrUpdateTmpVndDistrict(TmpVndDistrict tmpVndDistrict);
	
	public boolean deleteTmpVndAdd(TmpVndAddress tmpVndAdd);
	public boolean deleteTmpVndCompanyTypeByVndHeader(TmpVndHeader tmp);
	public boolean deleteTmpVndAkta(TmpVndAkta tmpVndAkta);
	public boolean deleteTmpVndDom(TmpVndDomisili tmpVndDom);
	public boolean deleteTmpVndBoard(TmpVndBoard tmpVndBoard);
	public boolean deleteTmpVndBank(TmpVndBank tmpVndBank);
	public boolean deleteTmpVndFinRpt(TmpVndFinRpt tmpVndFinRpt);
	public boolean deleteTmpVndCert(TmpVndCert tmpVndCert);
	public boolean deleteTmpVndSdm(TmpVndSdm tmpVndSdm);
	public boolean deleteTmpVndEquip(TmpVndEquip tmpVndEquip);
	public boolean deleteTmpVndCv(TmpVndCv tmpVndCv);
	public boolean deleteTmpVndProduct(TmpVndProduct tmpVndProduct);
	public boolean deleteTmpVndDistrictByTmpVndHeader(TmpVndHeader tmpVndHeader);
	
	public TmpVndAddress getTmpVndAddById(long id,TmpVndHeader vndHeader);
	public TmpVndAkta getTmpVndAktaById(long id, TmpVndHeader vndHeader);
	public TmpVndDomisili getTmpVndDomById(long id, TmpVndHeader vndHeader);
	public TmpVndBoard getTmpVndBoardById(long id, TmpVndHeader vndHeader);
	public TmpVndBank getTmpVndBankById(long id, TmpVndHeader tmpVndHeader);
	public TmpVndFinRpt getTmpVndFinRptById(long id, TmpVndHeader tmpVndHeader);
	public TmpVndCert getTmpVndCertById(long id, TmpVndHeader tmpVndHeader);
	public TmpVndSdm getTmpVndSdmById(long id, TmpVndHeader tmpVndHeader);
	public TmpVndEquip getTmpVndEquipById(long id, TmpVndHeader tmpVndHeader);
	public TmpVndCv getTmpVndCvById(long id, TmpVndHeader tmpVndHeader);
	public TmpVndProduct getTmpVndProductById(long id, TmpVndHeader tmpVndHeader);
	
	public boolean saveNextPage(int page, TmpVndHeader vndHeader);
	public boolean updateStatusTmpVndHeader(String status, TmpVndHeader vndHeader);
	public List<TmpVndTambahan> listTmpVndTambahanHeader(TmpVndHeader tmp);
	public void saveTmpVndTambahan(TmpVndTambahan tmpVndTambahan) throws Exception;
	public TmpVndTambahan getTmpVndTambahanById(int id,
			TmpVndHeader tmpVndHeader) throws Exception;
	public boolean deleteTmpVndTambahan(TmpVndTambahan tmpVndTambahan) throws Exception;
	public List<TmpVndIjin> listTmpVndIjin(TmpVndHeader tmp) throws Exception;
	public TmpVndIjin getTmpVndIjinById(int id) throws Exception;
	public boolean deleteTmpVndIjin(TmpVndIjin tmpVndIjinById) throws Exception;
	public boolean saveOrUpdateTmpVndIjin(TmpVndIjin tmpVndIjin) throws Exception;
}
