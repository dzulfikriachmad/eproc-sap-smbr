package eproc.controller;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import com.eproc.utils.GlobalComparator;
import com.eproc.utils.RandomCode;
import com.eproc.utils.SessionGetter;
import com.orm.model.AdmAddType;
import com.orm.model.AdmCompanyType;
import com.orm.model.AdmCostCenter;
import com.orm.model.AdmCountry;
import com.orm.model.AdmCurrency;
import com.orm.model.AdmDelegasi;
import com.orm.model.AdmDelegasiDetail;
import com.orm.model.AdmDept;
import com.orm.model.AdmDistrict;
import com.orm.model.AdmDoc;
import com.orm.model.AdmEmployee;
import com.orm.model.AdmIssues;
import com.orm.model.AdmJadwalDrt;
import com.orm.model.AdmJenis;
import com.orm.model.AdmMenu;
import com.orm.model.AdmProperty;
import com.orm.model.AdmProses;
import com.orm.model.AdmProsesFlow;
import com.orm.model.AdmProsesHirarki;
import com.orm.model.AdmRole;
import com.orm.model.AdmRoleMenu;
import com.orm.model.AdmRoleMenuId;
import com.orm.model.AdmUom;
import com.orm.model.AdmUser;
import com.orm.model.AdmUserManager;
import com.orm.model.CtrContractHeader;
import com.orm.model.PanitiaVnd;
import com.orm.model.PanitiaVndDetail;
import com.orm.model.PrcMainHeader;
import com.orm.model.PrcMappingKeuangan;
import com.orm.model.PrcTemplate;
import com.orm.model.PrcTemplateDetail;
import com.orm.model.UsulanVnd;
import com.orm.tools.GenericDao;
import com.orm.tools.HibernateUtil;

public class AdmServiceImpl extends GenericDao implements AdmService {

	@SuppressWarnings("unchecked")
	@Override
	public List<AdmCurrency> listAdmCurr() {
		DetachedCriteria dc = DetachedCriteria.forClass(AdmCurrency.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		
		return (List<AdmCurrency>) getListByDetachedCiteria(dc);
	}
	
	@Override
	public AdmCurrency getCurrName(AdmCurrency cur) {
		DetachedCriteria dc = DetachedCriteria.forClass(AdmCurrency.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.add(Restrictions.idEq(cur.getId()));
		return (AdmCurrency) getEntityByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AdmCompanyType> listCompanyType() {
		DetachedCriteria dc = DetachedCriteria.forClass(AdmCompanyType.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		
		return (List<AdmCompanyType>) getListByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AdmAddType> listAddType() {
		DetachedCriteria dc = DetachedCriteria.forClass(AdmAddType.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		
		return (List<AdmAddType>) getListByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AdmCountry> listAdmCountry() {
		DetachedCriteria dc = DetachedCriteria.forClass(AdmCountry.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
				
		return (List<AdmCountry>) getListByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AdmDistrict> listAdmDistrict() {
		DetachedCriteria dc = DetachedCriteria.forClass(AdmDistrict.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		
		return (List<AdmDistrict>) getListByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AdmDoc> listAdmDoc() {
		DetachedCriteria dc = DetachedCriteria.forClass(AdmDoc.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		
		return (List<AdmDoc>) getListByDetachedCiteria(dc);
	}

	@Override
	public AdmUser doLogin(AdmUser admUser) {
		DetachedCriteria dc = DetachedCriteria.forClass(AdmUser.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		
		DetachedCriteria sub = dc.createCriteria("admEmployee", JoinType.LEFT_OUTER_JOIN);
		DetachedCriteria sub2 = sub.createCriteria("admDept", JoinType.LEFT_OUTER_JOIN);
		@SuppressWarnings("unused")
		DetachedCriteria sub3 = sub2.createCriteria("admDistrict", JoinType.LEFT_OUTER_JOIN);
		if(admUser != null){
			dc.add(Restrictions.eq("username", admUser.getUsername().trim()));
		//	dc.add(Restrictions.eq("password", RandomCode.stringToMd5(admUser.getPassword().trim()).trim()));
			AdmUser val = (AdmUser) getEntityByDetachedCiteria(dc);
			if(val != null){
				return val;
			}else{
				return null;
			}
		}else{
			return null;
		}
	}

	@Override
	public AdmEmployee getAdmEmployeeById(long id) {
		DetachedCriteria dc = DetachedCriteria.forClass(AdmEmployee.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		dc.setFetchMode("admDept.admDistrict", FetchMode.JOIN);
		dc.add(Restrictions.eq("id", id));
		
		return (AdmEmployee) getEntityByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AdmUom> listAdmUom() {
		DetachedCriteria dc = DetachedCriteria.forClass(AdmUom.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		
		return (List<AdmUom>) getListByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AdmCostCenter> listAdmCostCenterByDept(AdmDept admDept) {
		DetachedCriteria dc = DetachedCriteria.forClass(AdmCostCenter.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.add(Restrictions.eq("admDept", admDept));
		
		return (List<AdmCostCenter>) getListByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AdmDept> listAdmDept() {
		DetachedCriteria dc = DetachedCriteria.forClass(AdmDept.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		
		return (List<AdmDept>) getListByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AdmUser> listAdmUser() {
		DetachedCriteria dc = DetachedCriteria.forClass(AdmUser.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		
		return (List<AdmUser>) getListByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AdmDept> getListAdmDept(Object object) throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(AdmDept.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admDistrict", FetchMode.JOIN);
		dc.setFetchMode("direktur", FetchMode.JOIN);
		dc.setFetchMode("kordang", FetchMode.JOIN);
		return (List<AdmDept>) getListByDetachedCiteria(dc);
	}

	@Override
	public void saveDept(AdmDept dept) throws Exception {
		try {
			startTransaction();
			saveEntity(dept);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			e.printStackTrace();
			throw e;
		}
	}

	@Override
	public AdmDept getAdmDeptById(AdmDept dept) throws Exception {
		DetachedCriteria cr = DetachedCriteria.forClass(AdmDept.class);
		cr.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		cr.setFetchMode("admDistrict", FetchMode.JOIN);
		cr.setFetchMode("direktur", FetchMode.JOIN);
		cr.setFetchMode("kordang", FetchMode.JOIN);
		cr.add(Restrictions.idEq(dept.getId()));
		return (AdmDept) getEntityByDetachedCiteria(cr);
	}

	@Override
	public void deleteDept(AdmDept dept) throws Exception {
		try {
			startTransaction();
			deleteEntity(dept);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			e.printStackTrace();
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AdmDistrict> getListDistrict(Object object) throws Exception {
		DetachedCriteria cr = DetachedCriteria.forClass(AdmDistrict.class);
		cr.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		return (List<AdmDistrict>)getListByDetachedCiteria(cr);
	}

	@Override
	public void saveDistrict(AdmDistrict district, PrcMappingKeuangan keuangan) throws Exception {
		try {
			startTransaction();
			saveEntity(district);
			//get keuangan
			PrcMappingKeuangan tmp = getMappingKeuanganByKantor(district);
			if(tmp!=null){
				tmp.setAdmUser(keuangan.getAdmUser());
				saveEntity(tmp);
			}else{
				keuangan.setDistrict(district);
				saveEntity(keuangan);
			}
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public AdmDistrict getDistrictById(AdmDistrict district) throws Exception {
		DetachedCriteria cr = DetachedCriteria.forClass(AdmDistrict.class);
		cr.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		cr.setFetchMode("district", FetchMode.JOIN);
		cr.add(Restrictions.idEq(district.getId()));
		return (AdmDistrict) getEntityByDetachedCiteria(cr);
	}

	@Override
	public void deleteDistrict(AdmDistrict district) throws Exception {
		try {
			startTransaction();
			deleteEntity(district);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public void saveCountry(AdmCountry country) throws Exception {
		try {
			startTransaction();
			saveEntity(country);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public AdmCountry getCountryById(AdmCountry country) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmCountry.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.idEq(country.getId()));
		return (AdmCountry) getEntityByDetachedCiteria(crit);
	}

	@Override
	public void deleteCountry(AdmCountry country) throws Exception {
		try {
			startTransaction();
			deleteEntity(country);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AdmDoc> getAdmDoc(Object object) throws Exception {
		DetachedCriteria cr = DetachedCriteria.forClass(AdmDoc.class);
		cr.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		return (List<AdmDoc>) getListByDetachedCiteria(cr);
	}

	@Override
	public void saveDoc(AdmDoc doc) throws Exception {
		try {
			startTransaction();
			saveEntity(doc);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public AdmDoc getDocById(AdmDoc doc) throws Exception {
		DetachedCriteria cr = DetachedCriteria.forClass(AdmDoc.class);
		cr.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		cr.add(Restrictions.idEq(doc.getId()));
		return (AdmDoc) getEntityByDetachedCiteria(cr);
	}

	@Override
	public void deleteDoc(AdmDoc doc) throws Exception {
		try {
			startTransaction();
			deleteEntity(doc);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public void deleteRole(AdmRole role) throws Exception {
		try {
			startTransaction();
			deleteEntity(role);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public AdmRole getRoleById(AdmRole role) throws Exception {
		DetachedCriteria crot = DetachedCriteria.forClass(AdmRole.class);
		crot.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crot.add(Restrictions.idEq(role.getId()));
		return (AdmRole) getEntityByDetachedCiteria(crot);
	}

	@Override
	public void saveRole(AdmRole role) throws Exception {
		try {
			startTransaction();
			saveEntity(role);
			//cek list role
			List<AdmRoleMenu> lrm = getListRoleMenuByRole(role);
			List<AdmMenu> lmenu = getListMenu(null);
			if(lrm.size()==0){
				for(AdmMenu m:lmenu){
					AdmRoleMenu rm = new AdmRoleMenu();
					AdmRoleMenuId id = new AdmRoleMenuId(role.getId(), m.getId());
					rm.setId(id);
					rm.setAdmMenu(m);
					rm.setAdmRole(role);
					rm.setDelete(0);
					rm.setUpdate(0);
					rm.setRetrieve(0);
					rm.setCreate(0);
					saveEntity(rm);
				}
			}else{
				List<AdmMenu> lm = getListMenu(lrm);
				for(AdmMenu m:lm){
					AdmRoleMenu rm = new AdmRoleMenu();
					AdmRoleMenuId id = new AdmRoleMenuId(role.getId(), m.getId());
					rm.setId(id);
					rm.setAdmMenu(m);
					rm.setAdmRole(role);
					rm.setDelete(0);
					rm.setUpdate(0);
					rm.setRetrieve(0);
					rm.setCreate(0);
					saveEntity(rm);
				}
			}
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AdmRole> getListRole(Object object) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmRole.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		return (List<AdmRole>) getListByDetachedCiteria(crit);
	}

	@Override
	public void deleteMenu(AdmMenu menu) throws Exception {
		try {
			//check if role menu exist
			deleteFromRoleMenu(menu);
			startTransaction();
			deleteEntity(menu);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@SuppressWarnings({"unchecked"})
	private void deleteFromRoleMenu(AdmMenu menu) throws Exception{
		DetachedCriteria crit = DetachedCriteria.forClass(AdmRoleMenu.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("admMenu", menu));
		List<AdmRoleMenu> mn = (List<AdmRoleMenu>)getListByDetachedCiteria(crit);
		if(mn!=null && mn.size()>0){
			try {
				startTransaction();
				for(AdmRoleMenu m:mn){
					deleteEntity(m);
				}
				commitAndClear();
			} catch (Exception e) {
				RollbackAndClear();
				throw e;
			}
		}
	}

	@Override
	public AdmMenu getMenuById(AdmMenu menu) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmMenu.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.idEq(menu.getId()));
		return (AdmMenu) getEntityByDetachedCiteria(crit);
	}

	@Override
	public void saveMenu(AdmMenu menu) throws Exception {
		try {
			startTransaction();
			boolean baru = false;
			if(menu.getId()==0)
				baru = true;
			saveEntity(menu);
			
			if(baru){
				List<AdmRole> listRole = getListRole(null);
				if(listRole!=null && listRole.size()>0){
					for(AdmRole r:listRole){
						AdmRoleMenuId id = new AdmRoleMenuId(r.getId(), menu.getId());
						AdmRoleMenu rm = new AdmRoleMenu();
						rm.setId(id);
						rm.setAdmMenu(menu);
						rm.setAdmRole(r);
						rm.setCreate(0);
						rm.setDelete(0);
						rm.setRetrieve(0);
						rm.setUpdate(0);
						saveEntity(rm);
					}
				}
			}
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AdmMenu> getListMenu(Object object) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmMenu.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		
		@SuppressWarnings("unused")
		DetachedCriteria sub = crit.createCriteria("admMenus", JoinType.LEFT_OUTER_JOIN);
		
		if(object!=null){
			if(object instanceof List<?>){
				List<AdmRoleMenu> tmp = (List<AdmRoleMenu>)object;
				List<Integer> menu = new ArrayList<Integer>();
				if(tmp!=null){
					for(AdmRoleMenu m:tmp){
						menu.add(m.getAdmMenu().getId());
					}
				}
				crit.add(Restrictions.not(Restrictions.in("id", menu)));
			}
		}
		return (List<AdmMenu>) getListByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AdmUser> getListUser(Object object) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmUser.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		@SuppressWarnings("unused")
		DetachedCriteria cr = crit.createCriteria("admEmployee", JoinType.LEFT_OUTER_JOIN);
		crit.addOrder(Order.asc("completeName"));
		List<AdmUser> retMe =  arrangeUser((List<AdmUser>) getListByDetachedCiteria(crit));
		return retMe;
	}
	
	private List<AdmUser> arrangeUser(List<AdmUser> users) {
		List<AdmUser> retMe = new ArrayList<AdmUser>();
		if(users!=null && users.size()>0){
			for(AdmUser u:users){
				String completeName = u.getCompleteName().concat(" ("+u.getAdmEmployee().getEmpNpp()+") - ").concat(u.getAdmEmployee().getEmpStructuralPos()!=null ?u.getAdmEmployee().getEmpStructuralPos():"");
				u.setCompleteName(completeName);
				retMe.add(u);
			}
		}
		return retMe;
	}

	@Override
	public void saveUser(AdmUser user) throws Exception {
		try {
			startTransaction();
			saveEntity(user.getAdmEmployee());
			user.setUsername(user.getAdmEmployee().getEmpNpp());
			if(user.getId()==null)
				user.setPassword(RandomCode.stringToMd5("123456"));
			user.setIsActive(1);
			user.setEmail(user.getAdmEmployee().getEmpEmail());
			user.setCompleteName(user.getAdmEmployee().getEmpFullName());
			saveEntity(user);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public AdmUser getUserById(AdmUser user) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmUser.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		@SuppressWarnings("unused")
		DetachedCriteria cr = crit.createCriteria("admEmployee", JoinType.LEFT_OUTER_JOIN);
		crit.add(Restrictions.idEq(user.getId()));
		return (AdmUser) getEntityByDetachedCiteria(crit);
	}

	@Override
	public void deleteUser(AdmUser user) throws Exception {
		try {
			startTransaction();
			user =  getUserById(user);
			if(user.getIsActive()==1)
				user.setIsActive(0);
			else
				user.setIsActive(1);
			saveEntity(user);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public void deleteUom(AdmUom uom) throws Exception {
		try {
			startTransaction();
			deleteEntity(uom);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public AdmUom getUomById(AdmUom uom) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmUom.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.idEq(uom.getUomId()));
		return (AdmUom) getEntityByDetachedCiteria(crit);
	}

	@Override
	public void saveUom(AdmUom uom) throws Exception {
		try {
			startTransaction();
			saveEntity(uom);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AdmUom> getListUom(Object object) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmUom.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		return (List<AdmUom>) getListByDetachedCiteria(crit);
	}

	@Override
	public void deleteProses(AdmProses proses) throws Exception {
		try {
			startTransaction();
			deleteEntity(proses);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public AdmProses getProsesById(AdmProses proses) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmProses.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.idEq(proses.getId()));
		return (AdmProses) getEntityByDetachedCiteria(crit);
	}

	@Override
	public void saveProses(AdmProses proses) throws Exception {
		try {
			startTransaction();
			saveEntity(proses);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AdmProses> getListProses(Object object) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmProses.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		return (List<AdmProses>) getListByDetachedCiteria(crit);
	}

	@Override
	public void deleteHirarki(AdmProsesHirarki hirarki) throws Exception {
		try {
			startTransaction();
			deleteEntity(hirarki);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public AdmProsesHirarki getHirarkiById(AdmProsesHirarki hirarki) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmProsesHirarki.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.idEq(hirarki.getId()));
		return (AdmProsesHirarki) getEntityByDetachedCiteria(crit);
	}

	@Override
	public void saveHirarki(AdmProsesHirarki hirarki) throws Exception {
		try {
			startTransaction();
			hirarki.setIsActive(1);//Flow aktif
			saveEntity(hirarki);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@SuppressWarnings({ "unchecked", "unused" })
	@Override
	public List<AdmProsesHirarki> getListHirarki(Object object)
			throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmProsesHirarki.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		
		DetachedCriteria sub1 = crit.createCriteria("admDept", JoinType.LEFT_OUTER_JOIN);
		DetachedCriteria sub2 = crit.createCriteria("admDistrict", JoinType.LEFT_OUTER_JOIN);
		DetachedCriteria sub3 = crit.createCriteria("admProses", JoinType.LEFT_OUTER_JOIN);
		DetachedCriteria sub4 = crit.createCriteria("admRoleByCurrentPos", JoinType.LEFT_OUTER_JOIN);
		DetachedCriteria sub5 = crit.createCriteria("admRoleByNextPos", JoinType.LEFT_OUTER_JOIN);
		
		crit.addOrder(Order.asc("admDistrict"));
		crit.addOrder(Order.asc("admDept"));
		crit.addOrder(Order.asc("admProses"));
		crit.addOrder(Order.asc("tingkat"));
		return (List<AdmProsesHirarki>) getListByDetachedCiteria(crit);
	}

	@SuppressWarnings({ "unchecked", "unused" })
	@Override
	public List<AdmRoleMenu> getListRoleMenuByRole(AdmRole role)
			throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmRoleMenu.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		
		DetachedCriteria sub = crit.createCriteria("admRole", JoinType.LEFT_OUTER_JOIN);
		DetachedCriteria sub2 = crit.createCriteria("admMenu", JoinType.LEFT_OUTER_JOIN);
		DetachedCriteria sub3 = sub2.createCriteria("admMenus",JoinType.LEFT_OUTER_JOIN);
		crit.add(Restrictions.eq("admRole", role));
		
		return (List<AdmRoleMenu>) getListByDetachedCiteria(crit);
		
	}

	@Override
	public void saveListRoleMenu(List<AdmRoleMenu> listRoleMenu)
			throws Exception {
		try {
			System.out.println(">>>  SAVING "+listRoleMenu.size());
			startTransaction();
			if(listRoleMenu!=null && listRoleMenu.size()>0){
				for(AdmRoleMenu rm:listRoleMenu){
					if(rm!=null){
						saveEntity(rm);
					}
				}
			}
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public void deleteCostCenter(AdmCostCenter costCenter) throws Exception {
		try {
			startTransaction();
			deleteEntity(costCenter);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@SuppressWarnings("unused")
	@Override
	public AdmCostCenter getCostCenterById(AdmCostCenter costCenter)
			throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmCostCenter.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.idEq(costCenter.getId()));
		DetachedCriteria sub = crit.createCriteria("admDept", JoinType.LEFT_OUTER_JOIN);
		return (AdmCostCenter) getEntityByDetachedCiteria(crit);
	}

	@Override
	public void saveCostCenter(AdmCostCenter costCenter) throws Exception {
		try {
			startTransaction();
			saveEntity(costCenter);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AdmCostCenter> getListCostCenter(Object object)
			throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmCostCenter.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		DetachedCriteria sub = crit.createCriteria("admDept", org.hibernate.sql.JoinType.LEFT_OUTER_JOIN);
		if(object!=null){
			if(object instanceof AdmDept){
				AdmDept d =(AdmDept)object;
				sub.add(Restrictions.idEq(d.getId()));
			}
		}
		
		return (List<AdmCostCenter>) getListByDetachedCiteria(crit);
	}
	
	@SuppressWarnings("unchecked")
	public List<AdmCostCenter> getListCostCenterAnggaran(Object object)
			throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmCostCenter.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		DetachedCriteria sub = crit.createCriteria("admDept", org.hibernate.sql.JoinType.LEFT_OUTER_JOIN);
		if(object!=null){
			if(object instanceof AdmDept){
				AdmDept d =(AdmDept)object;
				sub.add(Restrictions.idEq(d.getId()));
			}
		}
		List<AdmCostCenter> retMe = new ArrayList<AdmCostCenter>();
		List<AdmCostCenter> tmp = (List<AdmCostCenter>) getListByDetachedCiteria(crit);
		if(tmp!=null && tmp.size()>0){
			for(AdmCostCenter ct:tmp){
				ct.setCostCenterName("("+ct.getId().getCostCenterCode()+") "+ct.getCostCenterName());
				retMe.add(ct);
			}
		}
		return retMe;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<AdmMenu> getListRoleMenuByEmployee(AdmEmployee admEmployee)
			throws Exception {
		admEmployee  = getAdmEmployeeById(admEmployee);
		List<AdmRole> listRole = new ArrayList<AdmRole>();
		if(admEmployee!=null){
			if(admEmployee.getAdmRoleByEmpRole1()!=null)
				listRole.add(admEmployee.getAdmRoleByEmpRole1());
			if(admEmployee.getAdmRoleByEmpRole2()!=null)
				listRole.add(admEmployee.getAdmRoleByEmpRole2());
			if(admEmployee.getAdmRoleByEmpRole3()!=null)
				listRole.add(admEmployee.getAdmRoleByEmpRole3());
			if(admEmployee.getAdmRoleByEmpRole4()!=null)
				listRole.add(admEmployee.getAdmRoleByEmpRole4());
		}
		
		List<AdmMenu> retMe = new ArrayList<AdmMenu>();
		if(listRole.size()>0){
			for(AdmRole r: listRole){
				List<AdmRoleMenu> lsMenu = getListRoleMenuByRoleActive(r);
				if(lsMenu.size()>0){
					for(AdmRoleMenu rm:lsMenu){
						if(!retMe.contains(rm.getAdmMenu()))
							retMe.add(rm.getAdmMenu());
					}
				}
			}
		}
		
		Map<Integer, AdmMenu> mapMenu = new HashMap<Integer, AdmMenu>();
			if(retMe!=null && retMe.size()>0){
				for(AdmMenu m:retMe){
					Integer key = m.getId();
		    		if(!mapMenu.containsKey(key)){
		    			mapMenu.put(key, m);
		    		}
		    	}
		}
		    
		List<AdmMenu> returnMe = new ArrayList<AdmMenu>();
		returnMe.addAll(mapMenu.values());
		Collections.sort(returnMe, new GlobalComparator(new AdmMenu()));
		return returnMe;
	}

	@SuppressWarnings({ "unchecked", "unused" })
	private List<AdmRoleMenu> getListRoleMenuByRoleActive(AdmRole r) {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmRoleMenu.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		
		DetachedCriteria sub = crit.createCriteria("admRole", JoinType.LEFT_OUTER_JOIN);
		DetachedCriteria sub2 = crit.createCriteria("admMenu", JoinType.LEFT_OUTER_JOIN);
		DetachedCriteria sub3 = sub2.createCriteria("admMenus",JoinType.LEFT_OUTER_JOIN);
		sub2.addOrder(Order.asc("id"));
		crit.add(Restrictions.eq("admRole", r));
		crit.add(Restrictions.eq("retrieve", 1));//yang listnya aktif saja
		
		return (List<AdmRoleMenu>) getListByDetachedCiteria(crit);
	}

	public AdmEmployee getAdmEmployeeById(AdmEmployee admEmployee) {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmEmployee.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.idEq(admEmployee.getId()));
		return (AdmEmployee) getEntityByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	public AdmRoleMenu getAdmRoleMenuByAction(String aksi, AdmEmployee admEmployee) {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmRoleMenu.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		DetachedCriteria sub2 = crit.createCriteria("admMenu", JoinType.LEFT_OUTER_JOIN);
		sub2.add(Restrictions.or(Restrictions.eq("menuUrl", aksi), Restrictions.eq("createUrl", aksi)));
		crit.add(Restrictions.or(Restrictions.or(Restrictions.eq("admRole", admEmployee.getAdmRoleByEmpRole1()), Restrictions.eq("admRole", admEmployee.getAdmRoleByEmpRole2())), 
				Restrictions.or(Restrictions.eq("admRole", admEmployee.getAdmRoleByEmpRole3()), Restrictions.eq("admRole", admEmployee.getAdmRoleByEmpRole4()))));
		List<AdmRoleMenu> lrm = (List<AdmRoleMenu>) getListByDetachedCiteria(crit);
		if(lrm.size()>0){
			int data=0;
			List<AdmRoleMenu> retMe = new ArrayList<AdmRoleMenu>();
			for(AdmRoleMenu r:lrm){
				int temp=0;
				if(r.getCreate()!=null)
					temp = temp+r.getCreate();
				if(r.getRetrieve()!=null)
					temp = temp+r.getRetrieve();
				if(r.getUpdate()!=null)
					temp = temp+r.getUpdate();
				if(r.getDelete()!=null)
					temp = temp+r.getDelete();
				if(temp>data){
					data = temp;
					retMe.add(0, r);
				}
				
			}
			if(retMe.size()>0)
				return retMe.get(0);
			else
				return null;
		}else{
			return null;
		}
	}

	@Override
	public void deleteJadwal(AdmJadwalDrt jadwal) throws Exception {
		try {
			startTransaction();
			deleteEntity(jadwal);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public AdmJadwalDrt getJadwalById(AdmJadwalDrt jadwal) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmJadwalDrt.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("district", FetchMode.JOIN);
		crit.add(Restrictions.idEq(jadwal.getId()));
		return (AdmJadwalDrt) getEntityByDetachedCiteria(crit);
	}

	@Override
	public void saveJadwal(AdmJadwalDrt jadwal) throws Exception {
		try {
			startTransaction();
			saveEntity(jadwal);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AdmJadwalDrt> getListJadwal(AdmUser user) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmJadwalDrt.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("district", FetchMode.JOIN);
		if(user!=null){
			crit.add(Restrictions.eq("district", user.getAdmEmployee().getAdmDept().getAdmDistrict()));
		}
		return (List<AdmJadwalDrt>) getListByDetachedCiteria(crit);
	}

	public boolean isDrtJadwal(AdmDistrict district) {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmJadwalDrt.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("district", FetchMode.JOIN);
		crit.add(Restrictions.eq("district", district));
		crit.add(Restrictions.le("tanggalPembukaan", new Date()));
		crit.add(Restrictions.ge("tanggalPenutupan", new Date()));
		AdmJadwalDrt drt = (AdmJadwalDrt)getEntityByDetachedCiteria(crit);
		if(drt!=null)
			return true;
		return false;
	}

	public boolean isMemberPanitiaDrt(AdmUser user) {
		System.out.println("QUERY ---->>>>>>>>>");
		DetachedCriteria crit=DetachedCriteria.forClass(PanitiaVndDetail.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		
		DetachedCriteria parent = crit.createCriteria("panitia", JoinType.LEFT_OUTER_JOIN);
		parent.add(Restrictions.eq("status", 1));//harus yang aktif
		parent.add(Restrictions.eq("satker", user.getAdmEmployee().getAdmDept()));
		
		DetachedCriteria anggota = crit.createCriteria("anggota",JoinType.LEFT_OUTER_JOIN);
		anggota.add(Restrictions.idEq(user.getId()));
		
		PanitiaVndDetail retMe = (PanitiaVndDetail)getEntityByDetachedCiteria(crit);
		if(retMe!=null)
			return true;
		return false;
	}

	public AdmProsesHirarki getHirarkiProsesGlobal(AdmProses admProses,
			int level, AdmDept admDept, Double limit) throws Exception {
		AdmProsesHirarki retMe = null;
		try{
			DetachedCriteria crit = DetachedCriteria.forClass(AdmProsesHirarki.class);
			crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			crit.setFetchMode("admRoleByCurrentPos", FetchMode.JOIN);
			crit.setFetchMode("admRoleByNextPos", FetchMode.JOIN);
			crit.setFetchMode("admProses", FetchMode.JOIN);
			crit.setFetchMode("admDistrict", FetchMode.JOIN);
			crit.setFetchMode("admDept", FetchMode.JOIN);
			crit.add(Restrictions.eq("admDept", admDept));
			crit.add(Restrictions.eq("tingkat", level));
			crit.add(Restrictions.eq("admProses", admProses));
			crit.add(Restrictions.le("minimalValue", new BigDecimal(limit)));
			retMe = (AdmProsesHirarki)getEntityByDetachedCiteria(crit);
			if(retMe==null){
				if(level<30)
					getHirarkiProsesGlobal(admProses, level+1, admDept, limit);
			}
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		return retMe;
	}

	public AdmEmployee getAdmEmployeeByRole(AdmRole admRoleByCurrentPos) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmEmployee.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("admUser", FetchMode.JOIN);
		crit.add(Restrictions.or(Restrictions.or(Restrictions.eq("admRoleByEmpRole1", admRoleByCurrentPos), Restrictions.eq("admRoleByEmpRole2", admRoleByCurrentPos)),
				Restrictions.or(Restrictions.eq("admRoleByEmpRole3", admRoleByCurrentPos), Restrictions.eq("admRoleByEmpRole4", admRoleByCurrentPos))));
		return (AdmEmployee)getEntityByDetachedCiteria(crit);
	}
	
	public AdmEmployee getAdmEmployeeByRoleName(String roleName) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmEmployee.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("admUser", FetchMode.JOIN);
		crit.add(Restrictions.like("empStructuralPos", roleName));
		return (AdmEmployee)getEntityByDetachedCiteria(crit);
	}
	
	public AdmEmployee getAdmEmployeeByRolePanitiaDRT(AdmRole admRoleByCurrentPos, UsulanVnd usulan) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmEmployee.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("admUser", FetchMode.JOIN);
		crit.add(Restrictions.or(Restrictions.or(Restrictions.eq("admRoleByEmpRole1", admRoleByCurrentPos), Restrictions.eq("admRoleByEmpRole2", admRoleByCurrentPos)),
				Restrictions.or(Restrictions.eq("admRoleByEmpRole3", admRoleByCurrentPos), Restrictions.eq("admRoleByEmpRole4", admRoleByCurrentPos))));
		crit.add(Restrictions.eq("admUser", usulan.getCreator()));
		return (AdmEmployee)getEntityByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PanitiaVnd> getListPanitiaVnd(Object object) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PanitiaVnd.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("satker", FetchMode.JOIN);
		return (List<PanitiaVnd>) getListByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PanitiaVndDetail> getListPanitiaDetailByPanitia(
			PanitiaVnd panitiaVnd) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PanitiaVndDetail.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("panitia", FetchMode.JOIN);
		crit.setFetchMode("anggota", FetchMode.JOIN);
		crit.add(Restrictions.eq("panitia", panitiaVnd));
		return (List<PanitiaVndDetail>) getListByDetachedCiteria(crit);
	}

	@Override
	public PanitiaVnd getPanitiaVndById(PanitiaVnd panitiaVnd) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PanitiaVnd.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.idEq(panitiaVnd.getId()));
		return (PanitiaVnd) getEntityByDetachedCiteria(crit);
	}

	@Override
	public void hapusPanitia(PanitiaVnd panitiaVnd) throws Exception {
		try {
			startTransaction();
			deleteEntity(panitiaVnd);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public void savePanitiaVnd(PanitiaVnd panitiaVnd) throws Exception {
		try {
			startTransaction();
			saveEntity(panitiaVnd);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public void savePanitiaDetail(PanitiaVndDetail panitiaVndDetail)
			throws Exception {
		try {
			startTransaction();
			saveEntity(panitiaVndDetail);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public PanitiaVndDetail getPanitiaDetailById(
			PanitiaVndDetail panitiaVndDetail) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PanitiaVndDetail.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("anggota", FetchMode.JOIN);
		crit.add(Restrictions.idEq(panitiaVndDetail.getId()));
		return (PanitiaVndDetail) getEntityByDetachedCiteria(crit);
	}

	@Override
	public void hapusPanitiaDetail(PanitiaVndDetail panitiaVndDetail)
			throws Exception {
		try {
			startTransaction();
			deleteEntity(panitiaVndDetail);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	public AdmProsesHirarki getHirarkiProsesPengadaanGlobal(AdmProses proses,
			int level, AdmDistrict admDistrict, double doubleValue) throws Exception {
		AdmProsesHirarki retMe = null;
		try{
			DetachedCriteria crit = DetachedCriteria.forClass(AdmProsesHirarki.class);
			crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			crit.setFetchMode("admRoleByCurrentPos", FetchMode.JOIN);
			crit.setFetchMode("admRoleByNextPos", FetchMode.JOIN);
			crit.setFetchMode("admProses", FetchMode.JOIN);
			crit.setFetchMode("admDistrict", FetchMode.JOIN);
			crit.setFetchMode("admDept", FetchMode.JOIN);
			crit.add(Restrictions.eq("admDistrict", admDistrict));
			crit.add(Restrictions.eq("tingkat", level));
			crit.add(Restrictions.eq("admProses", proses));
			crit.add(Restrictions.le("minimalValue", new BigDecimal(doubleValue)));
			crit.add(Restrictions.eq("isActive", 1));
			
			System.out.println(">>NILAI>> "+NumberFormat.getInstance().format(doubleValue)+"<><>LVL <><> "+level+"<><>DIST<><>< "+admDistrict.getId());
			retMe = (AdmProsesHirarki)getEntityByDetachedCiteria(crit);	
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		if(retMe!=null){
			return retMe;
		}else{
			if(level<30){
				return getHirarkiProsesPengadaanGlobal(proses, level+1, admDistrict, doubleValue);
			}else
				return null;
		}
	}
	
	public AdmProsesHirarki getHirarkiProsesPengadaanJenis(AdmProses proses,
			int level, AdmDistrict admDistrict, double doubleValue, Integer jenis) throws Exception {
		AdmProsesHirarki retMe = null;
		try{
			DetachedCriteria crit = DetachedCriteria.forClass(AdmProsesHirarki.class);
			crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			crit.setFetchMode("admRoleByCurrentPos", FetchMode.JOIN);
			crit.setFetchMode("admRoleByNextPos", FetchMode.JOIN);
			crit.setFetchMode("admProses", FetchMode.JOIN);
			crit.setFetchMode("admDistrict", FetchMode.JOIN);
			crit.setFetchMode("admDept", FetchMode.JOIN);
			crit.add(Restrictions.eq("admDistrict", admDistrict));
			crit.add(Restrictions.eq("tingkat", level));
			crit.add(Restrictions.eq("admProses", proses));
			crit.add(Restrictions.eq("jenis", jenis));
			crit.add(Restrictions.le("minimalValue", new BigDecimal(doubleValue)));
			crit.add(Restrictions.eq("isActive", 1));
			
			System.out.println(">>NILAI>> "+NumberFormat.getInstance().format(doubleValue)+"<><>LVL <><> "+level+"<><>DIST<><>< "+admDistrict.getId());
			retMe = (AdmProsesHirarki)getEntityByDetachedCiteria(crit);	
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		if(retMe!=null){
			return retMe;
		}else{
			if(level<30){
				if(jenis==1) {
					retMe = getHirarkiProsesPengadaanJenis(proses, level+1, admDistrict, doubleValue,1);
				}else if(jenis == 2) {
					retMe = getHirarkiProsesPengadaanJenis(proses, level+1, admDistrict, doubleValue,2);
				}
				return retMe;
			}else
				return null;
		}
	}
	

	/**
	 * pakai ID
	 * @param admProses
	 * @return
	 */
	public AdmProses getProsesFlowByLevel(AdmProses admProses) {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmProsesFlow.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("admProses", FetchMode.JOIN);
		crit.add(Restrictions.idEq(admProses.getId()));
		return ((AdmProsesFlow)getEntityByDetachedCiteria(crit)).getAdmProses();
	}

	@Override
	public PrcTemplateDetail getTemplateDetailById(PrcTemplateDetail prcTemplateDetail)
			throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcTemplateDetail.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.idEq(prcTemplateDetail.getId()));
		return (PrcTemplateDetail) getEntityByDetachedCiteria(crit);
	}

	@Override
	public void hapusTemplateDetail(PrcTemplateDetail templateDetailById)
			throws Exception {
		try {
			startTransaction();
			deleteEntity(templateDetailById);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcTemplate> getListTemplate(Object object) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcTemplate.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		return (List<PrcTemplate>) getListByDetachedCiteria(crit);
	}

	@Override
	public void saveTemplatePengadaan(PrcTemplate template,
			List<PrcTemplateDetail> sessionValue) throws Exception {
		try {
			startTransaction();
			saveEntity(template);
			System.out.println("SAVE TEMPLATE");
			if(sessionValue!=null && sessionValue.size()>0){
				for(PrcTemplateDetail s:sessionValue){
					System.out.println("SAVE TEMPLATE ITEM "+s.getId());
					System.out.println("SAVE TEMPLATE ITEM "+s.getTempdetName());
					if(s.getId()==0){
						PrcTemplateDetail d = new PrcTemplateDetail();
						d.setPrcTemplate(template);
						d.setTempdetName(s.getTempdetName());
						d.setTipe(s.getTipe());
						d.setTempdetWeight(s.getTempdetWeight());
						saveEntity(d);
					}else{
						s.setPrcTemplate(template);
						saveEntity(s);
					}
				}
			}
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public PrcTemplate getTemplateById(PrcTemplate template) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcTemplate.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.idEq(template.getId()));
		return (PrcTemplate) getEntityByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcTemplateDetail> getListTemplateDetailByTemplate(
			PrcTemplate prcTemplate) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcTemplateDetail.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("prcTemplate", prcTemplate));
		return (List<PrcTemplateDetail>) getListByDetachedCiteria(crit);
	}

	@Override
	public AdmUser getAdmUser(AdmUser o) {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmUser.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("admEmployee", FetchMode.JOIN);
		crit.add(Restrictions.eq("id", o.getId()));
		
		return (AdmUser) getEntityByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AdmDelegasi> listDelegasi(Object object) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmDelegasi.class);
		crit.setResultTransformer(DetachedCriteria.ROOT_ENTITY);
		crit.setFetchMode("dari", FetchMode.JOIN);
		crit.setFetchMode("kepada", FetchMode.JOIN);
		return (List<AdmDelegasi>) getListByDetachedCiteria(crit);
	}

	@Override
	public AdmDelegasi getDelegasiById(AdmDelegasi admDelegasi)
			throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmDelegasi.class);
		crit.setResultTransformer(DetachedCriteria.ROOT_ENTITY);
		crit.add(Restrictions.idEq(admDelegasi.getId()));
		return (AdmDelegasi) getEntityByDetachedCiteria(crit);
	}

	@Override
	public void saveDelegasi(AdmDelegasi admDelegasi) throws Exception {
		try {
			startTransaction();
			saveEntity(admDelegasi);
			//move task to delegate user
			if(admDelegasi.getStatus()==1){
				Query q = session.createSQLQuery("delete from adm_delegasi_detail where delegasi="+admDelegasi.getId()+" ");
				q.executeUpdate();
				
//				List<PrcMainHeader> listPpm = getListPpmByCurrentUser(admDelegasi.getDari());
				List<PrcMainHeader> listPpm = getListPpmByCurrentUserAll(admDelegasi.getDari());
				//insert to delegate detail
				if(listPpm!=null && listPpm.size()>0){
					//remove all delegate record
					for(PrcMainHeader ppm:listPpm){
						AdmDelegasiDetail d =  new AdmDelegasiDetail();
						d.setAdmDelegasi(admDelegasi);
						d.setPpmId(ppm.getPpmId());
						saveEntity(d);
						
						if(ppm.getAdmUserByPpmCurrentUser()!=null && ppm.getAdmUserByPpmCurrentUser().getId().equals(admDelegasi.getDari().getId())){
						  ppm.setAdmUserByPpmCurrentUser(admDelegasi.getKepada());
						}
						
//						if(ppm.getAdmUserByPpmEstimator()!=null && ppm.getAdmUserByPpmEstimator().getId() ==  admDelegasi.getDari().getId())
//							ppm.setAdmUserByPpmEstimator(admDelegasi.getKepada());
//						
//						if(ppm.getAdmUserByPpmBuyer()!=null && ppm.getAdmUserByPpmBuyer().getId() ==  admDelegasi.getDari().getId())
//							ppm.setAdmUserByPpmBuyer(admDelegasi.getKepada());
//						
//						if(ppm.getAdmUserByPpmAdmBuyer()!=null && ppm.getAdmUserByPpmAdmBuyer().getId() ==  admDelegasi.getDari().getId())
//							ppm.setAdmUserByPpmAdmBuyer(admDelegasi.getKepada());
//						
//						if(ppm.getPengawasPp()!=null && ppm.getPengawasPp().getId() ==  admDelegasi.getDari().getId())
//							ppm.setPengawasPp(admDelegasi.getKepada());
						
						saveEntity(ppm);
					}
				}
				List<CtrContractHeader> listContract = getListContractByCurrentUser(admDelegasi.getDari());
				if(listContract!=null && listContract.size()>0){
					//remove all delegate record
					for(CtrContractHeader ctr:listContract){
						AdmDelegasiDetail d =  new AdmDelegasiDetail();
						d.setAdmDelegasi(admDelegasi);
						d.setContractId(ctr.getContractId());
						saveEntity(d);
						
						if(ctr.getAdmUserByCurrentUser()!=null && ctr.getAdmUserByCurrentUser().getId().equals(admDelegasi.getDari().getId()))
							ctr.setAdmUserByCurrentUser(admDelegasi.getKepada());
						
						if(ctr.getAdmUserByContractCreator()!=null && ctr.getAdmUserByContractCreator().getId().equals(admDelegasi.getDari().getId()))
							ctr.setAdmUserByContractCreator(admDelegasi.getKepada());
						
						saveEntity(ctr);
					}
				}
			}else if(admDelegasi.getStatus()==2){
				//remove all delegate record
				Query q = session.createSQLQuery("delete from adm_delegasi_detail where delegasi="+admDelegasi.getId()+" ");
				q.executeUpdate();
			}
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	private List<CtrContractHeader> getListContractByCurrentUser(AdmUser dari) {
		Session ses = HibernateUtil.getSession();
		String query = "from CtrContractHeader where ( admUserByCurrentUser="+dari.getId()+" or admUserByContractCreator="+dari.getId()+" )  and (contractProsesLevel!=99 and contractProsesLevel!=-99 and contractProsesLevel!=-98) ";
		Query q = ses.createQuery(query);
		List<CtrContractHeader> retMe =   q.list();
		ses.clear();
		ses.close();
		return retMe;
	}

	@SuppressWarnings("unchecked")
	private List<PrcMainHeader> getListPpmByCurrentUser(AdmUser dari) {
		Session ses = HibernateUtil.getSession();
		String query = "from PrcMainHeader where admUserByPpmCurrentUser="+dari.getId()+" and (ppmProsesLevel!=99 and ppmProsesLevel!=-99 and ppmProsesLevel!=-98 and ppmProsesLevel!=-100) ";
		Query q = ses.createQuery(query);
		List<PrcMainHeader> retMe =   q.list();
		ses.clear();
		ses.close();
		return retMe;
	}
	
	@SuppressWarnings("unchecked")
	private List<PrcMainHeader> getListPpmByCurrentUserAll(AdmUser dari) {
		DetachedCriteria crit =  DetachedCriteria.forClass(PrcMainHeader.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
		crit.setFetchMode("admUserByPpmEstimator", FetchMode.JOIN);
		crit.setFetchMode("admUserByPpmAdmBuyer", FetchMode.JOIN);
		crit.setFetchMode("admUserByPpmBuyer", FetchMode.JOIN);
		crit.setFetchMode("pengawasPp", FetchMode.JOIN);
		
		Disjunction dj  = Restrictions.disjunction();
		dj.add(Restrictions.eq("admUserByPpmCurrentUser", dari));
		dj.add(Restrictions.eq("admUserByPpmEstimator", dari));
		dj.add(Restrictions.eq("admUserByPpmAdmBuyer", dari));
		dj.add(Restrictions.eq("admUserByPpmBuyer", dari));
		dj.add(Restrictions.eq("pengawasPp", dari));
		
		crit.add(dj);
		return (List<PrcMainHeader>) getListByDetachedCiteria(crit);
	}
	
	private List<PrcMainHeader> getListPpmByCurrentUserOnly(AdmUser dari)
	  {
	    DetachedCriteria crit = DetachedCriteria.forClass(PrcMainHeader.class);
	    crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
	    crit.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
	    crit.setFetchMode("admUserByPpmEstimator", FetchMode.JOIN);
	    crit.setFetchMode("admUserByPpmAdmBuyer", FetchMode.JOIN);
	    crit.setFetchMode("admUserByPpmBuyer", FetchMode.JOIN);
	    crit.setFetchMode("pengawasPp", FetchMode.JOIN);
	    
	    Disjunction dj = Restrictions.disjunction();
	    dj.add(Restrictions.eq("admUserByPpmCurrentUser", dari));
	    crit.add(dj);
	    return (List)getListByDetachedCiteria(crit);
	  }

	public AdmDelegasi getDelegasiByUserDari(AdmUser admUserByPpmCurrentUser) {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmDelegasi.class);
		crit.setResultTransformer(DetachedCriteria.ROOT_ENTITY);
		crit.setFetchMode("dari", FetchMode.JOIN);
		crit.setFetchMode("kepada", FetchMode.JOIN);
		crit.add(Restrictions.eq("dari", admUserByPpmCurrentUser));
		crit.add(Restrictions.eq("status", 1));//aktif
		return (AdmDelegasi) getEntityByDetachedCiteria(crit);
	}
	
	public AdmDelegasi getDelegasiByUserKepada(AdmUser admUserByPpmCurrentUser) {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmDelegasi.class);
		crit.setResultTransformer(DetachedCriteria.ROOT_ENTITY);
		crit.setFetchMode("dari", FetchMode.JOIN);
		crit.setFetchMode("kepada", FetchMode.JOIN);
		crit.add(Restrictions.eq("kepada", admUserByPpmCurrentUser));
		crit.add(Restrictions.eq("status", 1));//aktif
		return (AdmDelegasi) getEntityByDetachedCiteria(crit);
	}
	
	public List<AdmDelegasi> getListDelegasiByUserKepada(AdmUser admUserByPpmCurrentUser) {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmDelegasi.class);
		crit.setResultTransformer(DetachedCriteria.ROOT_ENTITY);
		crit.setFetchMode("dari", FetchMode.JOIN);
		crit.setFetchMode("kepada", FetchMode.JOIN);
		crit.add(Restrictions.eq("kepada", admUserByPpmCurrentUser));
		crit.add(Restrictions.eq("status", 1));//aktif
		return (List<AdmDelegasi>) getListByDetachedCiteria(crit);
	}
	
	public List<AdmDelegasi> getListDelegasiByUserKepadaAllAktifNotAktif(AdmUser admUserByPpmCurrentUser) {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmDelegasi.class);
		crit.setResultTransformer(DetachedCriteria.ROOT_ENTITY);
		crit.setFetchMode("dari", FetchMode.JOIN);
		crit.setFetchMode("kepada", FetchMode.JOIN);
		crit.add(Restrictions.eq("kepada", admUserByPpmCurrentUser));
		return (List<AdmDelegasi>) getListByDetachedCiteria(crit);
	}

	public String getPropertyByKey(String key) {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmProperty.class);
		crit.setResultTransformer(DetachedCriteria.ROOT_ENTITY);
		crit.add(Restrictions.idEq(key));
		AdmProperty p = (AdmProperty) getEntityByDetachedCiteria(crit);
		if(p!=null)
			return p.getValue();
		return null;
	}

	@Override
	public void savePassword(AdmUser user) throws Exception {
		try {
			startTransaction();
			AdmUser us = getAdmUser(SessionGetter.getUser());
			us.setPassword(RandomCode.stringToMd5(user.getPassword()));
			saveEntity(us);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}


	@Override
	public PrcMappingKeuangan getMappingKeuanganByKantor(AdmDistrict district)
			throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMappingKeuangan.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("district", district));
		return (PrcMappingKeuangan) getEntityByDetachedCiteria(crit);
	}

	@Override
	public void deleteTemplate(PrcTemplate template) throws Exception {
		try {
			startTransaction();
			PrcTemplate tm = getTemplateById(template);
			deleteEntity(tm);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public AdmDept getAdmDeptByCode(String kodeSatker) throws Exception{
		DetachedCriteria cr = DetachedCriteria.forClass(AdmDept.class);
		cr.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		cr.add(Restrictions.eq("deptCode", kodeSatker.trim()));
		List<AdmDept> retMe = (List<AdmDept>) getListByDetachedCiteria(cr);
		if(retMe!=null && retMe.size()>0){
			return retMe.get(0);
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AdmIssues> getListIssues(Object object) throws Exception {
		DetachedCriteria crit =  DetachedCriteria.forClass(AdmIssues.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.addOrder(Order.asc("createdDate"));
		return (List<AdmIssues>) getListByDetachedCiteria(crit);
	}

	@Override
	public void saveIssues(AdmIssues issues) throws Exception {
		try {
			startTransaction();
			saveEntity(issues);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			e.printStackTrace();
			throw e;
		}
		
	}

	@Override
	public AdmIssues getIssuesById(AdmIssues issues) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmIssues.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.idEq(issues.getId()));
		return (AdmIssues) getEntityByDetachedCiteria(crit);
	}
	
	@Override
	public AdmJenis getAdmJenisById(Integer id) {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmJenis.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("id", id));
		return (AdmJenis) getEntityByDetachedCiteria(crit);
	}
	

	
	public AdmUserManager getUserByJenisKodeSatker(AdmJenis jenis, Integer jabatan, Integer kodeSatker) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmUserManager.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("admUser", FetchMode.JOIN);
		crit.setFetchMode("admJenis", FetchMode.JOIN);
		crit.add(Restrictions.eq("admJenis.id", jenis.getId()));
		crit.add(Restrictions.eq("jabatan", jabatan));
		/*if(kodeSatker!=1201) {
			crit.add(Restrictions.le("minimalValue", new BigDecimal(doubleValue)));
		}*/
		crit.add(Restrictions.or(
				Restrictions.eq("kodeSatker1", kodeSatker), 
				Restrictions.eq("kodeSatker2", kodeSatker), 
				Restrictions.eq("kodeSatker3", kodeSatker), 
				Restrictions.eq("kodeSatker4", kodeSatker), 
				Restrictions.eq("kodeSatker5", kodeSatker)));
		return (AdmUserManager) getEntityByDetachedCiteria(crit);
	}
	
	public AdmUserManager getUserByJenis(AdmJenis jenis, Integer jabatan) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmUserManager.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("admUser", FetchMode.JOIN);
		crit.setFetchMode("admJenis", FetchMode.JOIN);
		crit.add(Restrictions.eq("admJenis", jenis));
		crit.add(Restrictions.eq("jabatan", jabatan));
		return (AdmUserManager) getEntityByDetachedCiteria(crit);
	}

	public AdmUser getManagerByPr(PrcMainHeader prcMainHeader) throws Exception{
		AdmUser user = new AdmUser();
		if(prcMainHeader.getPpmNomorPrSap().contains("1201")) {
			user = SessionGetter.getUserByJenisKodeSatker(prcMainHeader.getPpmJenisDetail(), 2, 1201).getAdmUser();
		}else if(prcMainHeader.getPpmNomorPrSap().contains("1202")){
			user = SessionGetter.getUserByJenisKodeSatker(prcMainHeader.getPpmJenisDetail(), 2, 1202).getAdmUser();
		}else if(prcMainHeader.getPpmNomorPrSap().contains("1203")) {
			user = SessionGetter.getUserByJenisKodeSatker(prcMainHeader.getPpmJenisDetail(), 2, 1203).getAdmUser();
		}else if(prcMainHeader.getPpmNomorPrSap().contains("1204")) {
			user = SessionGetter.getUserByJenisKodeSatker(prcMainHeader.getPpmJenisDetail(), 2, 1204).getAdmUser();
		}
		return user;
	}
	
	@Override
	public void resetPasswordUser(AdmUser user) throws Exception {
		try {
			startTransaction();
			user =  getUserById(user);
			if(user.getIsActive()==1) {
				user.setPassword(RandomCode.stringToMd5("init").trim());
				saveEntity(user);
			}else {
				throw new Exception("User tidak aktif");
			}
				
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

}
