package eproc.controller;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.type.StandardBasicTypes;

import com.eproc.interceptor.OrmInterceptor;
import com.eproc.sap.controller.IntegrationService;
import com.eproc.sap.controller.IntegrationServiceImpl;
import com.eproc.sap.dto.ResultDto;
import com.eproc.utils.JulianToGregorianViceVersa;
import com.eproc.utils.SessionGetter;
import com.eproc.utils.UploadUtil;
import com.eproc.x.global.command.TodoUserCommand;
import com.google.gxp.com.google.common.collect.Lists;
import com.jde.model.PODetail;
import com.jde.model.POHeader;
import com.orm.model.AdmDelegasi;
import com.orm.model.AdmDept;
import com.orm.model.AdmDistrict;
import com.orm.model.AdmJenis;
import com.orm.model.AdmProses;
import com.orm.model.AdmProsesHirarki;
import com.orm.model.AdmRole;
import com.orm.model.AdmUser;
import com.orm.model.ComGroup;
import com.orm.model.CtrContractComment;
import com.orm.model.CtrContractHeader;
import com.orm.model.EauctionHeader;
import com.orm.model.EauctionVendor;
import com.orm.model.HistMainComment;
import com.orm.model.OrHeader;
import com.orm.model.OrHeaderId;
import com.orm.model.PanitiaPengadaan;
import com.orm.model.PanitiaPengadaanDetail;
import com.orm.model.PrcAnggaranJdeDetail;
import com.orm.model.PrcAnggaranJdeHeader;
import com.orm.model.PrcMainDoc;
import com.orm.model.PrcMainHeader;
import com.orm.model.PrcMainItem;
import com.orm.model.PrcMainPreparation;
import com.orm.model.PrcMainVendor;
import com.orm.model.PrcMainVendorId;
import com.orm.model.PrcMainVendorQuote;
import com.orm.model.PrcMainVendorQuoteAdmtech;
import com.orm.model.PrcMainVendorQuoteId;
import com.orm.model.PrcMainVendorQuoteItem;
import com.orm.model.PrcManualDoc;
import com.orm.model.PrcMappingKeuangan;
import com.orm.model.PrcMethod;
import com.orm.model.PrcMsgChat;
import com.orm.model.PrcTemplate;
import com.orm.model.PrcTemplateDetail;
import com.orm.model.VndHeader;
import com.orm.tools.GenericDao;
import com.orm.tools.HibernateUtil;

public class PrcServiceImpl extends GenericDao implements PrcService {
	

	@Override
	public Long saveOrUpdatePrcMainHeader(PrcMainHeader prcMainHeader) {
		long ret = 0;
		try {
			startTransaction(new OrmInterceptor());
			session.saveOrUpdate(prcMainHeader);
			session.flush();
			ret = prcMainHeader.getPpmId();
			commitAndClear();
		} catch (Exception e) {
			e.printStackTrace();
			RollbackAndClear();
			return ret;
		}
		return ret;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ComGroup> getListComGrup(Object object) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(ComGroup.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		return (List<ComGroup>) getListByDetachedCiteria(crit);
	}
	
	@Override
	public boolean saveOrUpdatePrcMainItem(PrcMainItem prcMainItem) {
		boolean ret = false;
		try {
			startTransaction(new OrmInterceptor());
			ret = saveEntity(prcMainItem);
			commitAndClear();
		} catch (Exception e) {
			e.printStackTrace();
			RollbackAndClear();
			return ret;
		}
		return ret;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainItem> listPrcMainItemByHeader(PrcMainHeader prcMainHeader) {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainItem.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("vndHeader", FetchMode.JOIN);
		dc.setFetchMode("admUom", FetchMode.JOIN);
		dc.add(Restrictions.eq("prcMainHeader.ppmId", prcMainHeader.getPpmId()));
		dc.addOrder(Order.asc("id"));
		return (List<PrcMainItem>) getListByDetachedCiteria(dc);
	}

	@Override
	public boolean deletePrcMainItem(PrcMainItem prcMainItem) {
		boolean ret = false;
		try {
			startTransaction();
			ret = deleteEntity(prcMainItem);
			commitAndClear();
		} catch (Exception e) {
			e.printStackTrace();
			RollbackAndClear();
			return ret;
		}
		return ret;
	}

	@Override
	public PrcMainItem getPrcMainItemById(long id) {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainItem.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.add(Restrictions.eq("id", id));
		return (PrcMainItem) getEntityByDetachedCiteria(dc);
	}

	@Override
	public PrcMainHeader getPrcMainIHeaderById(long id) {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByPpmEstimator", FetchMode.JOIN);
		dc.setFetchMode("admUserByPpmCreator", FetchMode.JOIN);
		dc.setFetchMode("admUserByPpmBuyer", FetchMode.JOIN);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		dc.setFetchMode("admDistrict", FetchMode.JOIN);
		dc.setFetchMode("ppmJenis", FetchMode.JOIN);
		dc.setFetchMode("ppmJenisDetail", FetchMode.JOIN);
		dc.add(Restrictions.eq("ppmId", id));
		return (PrcMainHeader) getEntityByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> listPrcMainHeader() {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		dc.add(Restrictions.isNull("ppmNomorRfq"));
		return (List<PrcMainHeader>) getListByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<HistMainComment> listHistMainCommentByPrcHeader(
			PrcMainHeader prcMainHeader) {
		DetachedCriteria dc = DetachedCriteria.forClass(HistMainComment.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.add(Restrictions.eq("prcMainHeader", prcMainHeader));
		dc.addOrder(Order.asc("createdDate"));
		return (List<HistMainComment>) getListByDetachedCiteria(dc);
	}

	@Override
	public boolean saveOrUpdateHistMainComment(HistMainComment histMainComment) {
		boolean ret = false;
		try {
			startTransaction(new OrmInterceptor());
			ret = saveEntity(histMainComment);
			commitAndClear();
		} catch (Exception e) {
			e.printStackTrace();
			RollbackAndClear();
			return ret;
		}
		return ret;
	}

	@Override
	public AdmProsesHirarki getAdmProsesHirarki(AdmProses proses,
			AdmRole role) {
		DetachedCriteria c = DetachedCriteria.forClass(AdmProsesHirarki.class);
		c.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		c.add(Restrictions.eq("admRoleByCurrentPos.id", role.getId()));
		c.add(Restrictions.eq("admProses.id", proses.getId()));
		return (AdmProsesHirarki) getEntityByDetachedCiteria(c);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> listPrcMainHeaderByCreator(AdmUser user) {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		dc.add(Restrictions.eq("admUserByPpmCreator.id", user.getId()));
		return (List<PrcMainHeader>) getListByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> listPrcMainHeaderApproval(AdmRole role) {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		dc.add(Restrictions.eq("admRole.id", role.getId()));
		dc.add(Restrictions.ge("ppmProsesLevel", 2));
		dc.add(Restrictions.eq("admProses.id", 100));
		return (List<PrcMainHeader>) getListByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> listPrcMainHeaderOe(AdmRole role, int tingkat) {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		if(role != null){
			dc.add(Restrictions.eq("admRole.id", role.getId()));
		}
		if(tingkat != 0){
			dc.add(Restrictions.eq("ppmProsesLevel", tingkat)); 
		}
		dc.add(Restrictions.eq("admProses.id", 102));
		return (List<PrcMainHeader>) getListByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> listPrcMainHeaderOE(Object obj) {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		dc.add(Restrictions.eq("admProses", SessionGetter.OE_PROSES_ID));
		dc.add(Restrictions.eq("admUserByPpmCurrentUser", SessionGetter.getUser()));
		//pembuatan OE
		dc.add(Restrictions.eq("ppmProsesLevel", 2));
		return (List<PrcMainHeader>) getListByDetachedCiteria(dc);
	}

	@Override
	public AdmProsesHirarki getAdmProsesHirarki(AdmProses proses, int tingkat) {
		DetachedCriteria c = DetachedCriteria.forClass(AdmProsesHirarki.class);
		c.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		c.add(Restrictions.eq("tingkat", tingkat));
		c.add(Restrictions.eq("admProses.id", proses.getId()));
		return (AdmProsesHirarki) getEntityByDetachedCiteria(c);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> listPrcMainHeaderToDo(AdmRole role) {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		dc.add(Restrictions.eq("admRole.id", role.getId()));
		return (List<PrcMainHeader>) getListByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> listPrcMainHeaderEstimator(AdmUser user) {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		dc.add(Restrictions.eq("ppmProsesLevel", 2)); 
		dc.add(Restrictions.eq("admUserByPpmEstimator.id", user.getId()));
		dc.add(Restrictions.eq("admProses", SessionGetter.OE_PROSES_ID));
		return (List<PrcMainHeader>) getListByDetachedCiteria(dc);
	}

	/**
	 * Save PR
	 */
	@Override
	public PrcMainHeader savePR(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, List<PrcMainItem> listPrcMainItem)
			throws Exception {
		try {
			startTransaction();
			prcMainHeader.setPpmProsesLevel(1);
			prcMainHeader.setAdmUserByPpmCurrentUser(prcMainHeader.getAdmUserByPpmCreator());
			prcMainHeader.setAdmRole(SessionGetter.getUser().getAdmEmployee().getAdmRoleByEmpRole1());
			prcMainHeader.setPpmJumlahEe(SessionGetter.getEeFromItems(listPrcMainItem));
			saveEntity(prcMainHeader);
			
			/** save Item **/
			saveItems(listPrcMainItem,prcMainHeader);
			
			/** save history **/
			saveHistory(histMainComment,prcMainHeader);
			
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
		return prcMainHeader;
	}

	private void saveHistory(HistMainComment histMainComment,
			PrcMainHeader prcMainHeader) throws Exception {
		HistMainComment lastComment = SessionGetter.getLastCommentByHeader(prcMainHeader);
		if(lastComment!=null){
			lastComment.setAksi(histMainComment.getAksi());
			lastComment.setProsesAksi(histMainComment.getProsesAksi());
			lastComment.setComment(histMainComment.getComment());
			lastComment.setDocument(histMainComment.getDocument());
			saveEntity(lastComment);
		}else{
			histMainComment.setPrcMainHeader(prcMainHeader);
			saveEntity(histMainComment);
		}
		
	}

	private void saveItems(List<PrcMainItem> listPrcMainItem,PrcMainHeader header) throws Exception {
		if(listPrcMainItem!=null && listPrcMainItem.size()>0){
			for(PrcMainItem it:listPrcMainItem){
				it.setPrcMainHeader(header);
				saveEntity(it);
			}
		}
		
	}

	@Override
	public PrcMainHeader savePRFinish(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, List<PrcMainItem> listPrcMainItem)
			throws Exception {
		try {
			startTransaction();
			prcMainHeader.setPpmJumlahEe(SessionGetter.getEeFromItems(listPrcMainItem));
			AdmProsesHirarki hirarki = SessionGetter.getHirarkiProses(prcMainHeader.getAdmProses(), 2, prcMainHeader.getAdmDept(), prcMainHeader.getPpmJumlahEe().doubleValue());
			prcMainHeader.setPpmProsesLevel(hirarki.getTingkat());
			prcMainHeader.setAdmRole(hirarki.getAdmRoleByCurrentPos());
			prcMainHeader.setAdmUserByPpmCurrentUser(SessionGetter.getEmployeeByRole(hirarki.getAdmRoleByCurrentPos()).getAdmUser());
			saveEntity(prcMainHeader);
			
			/** save Item **/
			saveItems(listPrcMainItem,prcMainHeader);
			
			/** save history **/
			saveHistoryFinish(histMainComment,prcMainHeader);
			
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
		return prcMainHeader;
	}

	private void saveHistoryFinish(HistMainComment histMainComment,
			PrcMainHeader prcMainHeader) throws Exception{
		HistMainComment lastComment = SessionGetter.getLastCommentByHeader(prcMainHeader);
		if(lastComment!=null){
			lastComment.setAksi(histMainComment.getAksi());
			lastComment.setProsesAksi(histMainComment.getProsesAksi());
			lastComment.setComment(histMainComment.getComment());
			lastComment.setDocument(histMainComment.getDocument());
			saveEntity(lastComment);
		}else{
			histMainComment.setPrcMainHeader(prcMainHeader);
			saveEntity(histMainComment);
		}
		
		/** new User **/
		HistMainComment cmt = new HistMainComment();
		cmt.setPrcMainHeader(prcMainHeader);
		cmt.setUsername(prcMainHeader.getAdmUserByPpmCurrentUser().getCompleteName());
		cmt.setPosisi(prcMainHeader.getAdmUserByPpmCurrentUser().getAdmEmployee().getEmpStructuralPos());
		cmt.setProses(prcMainHeader.getAdmProses().getId());
		cmt.setProsesAksi(SessionGetter.getPropertiesValue("pr.2", null));
		saveEntity(cmt);

	}

	@SuppressWarnings("unchecked")
	public HistMainComment getLastCommentByHeader(PrcMainHeader prcMainHeader) {
		DetachedCriteria crit = DetachedCriteria.forClass(HistMainComment.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("prcMainHeader", prcMainHeader));
		crit.addOrder(Order.desc("id"));
		List<HistMainComment> retMe = (List<HistMainComment>)getListByDetachedCiteria(crit);
		if(retMe!=null && retMe.size()>0){
			return retMe.get(0);
		}
		return null;
	}

	/**
	 * Main Header List based on current user
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> listPrcMainHeaderByCurrentUser(AdmUser user)
			throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		dc.add(Restrictions.eq("admUserByPpmCurrentUser.id", user.getId()));
		dc.add(Restrictions.eq("admProses", SessionGetter.PR_PROSES_ID));
		return (List<PrcMainHeader>) getListByDetachedCiteria(dc);
	}

	/**
	 * Revisi PR
	 */
	@Override
	public void revisiPR(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception {
		try {
			startTransaction();
			prcMainHeader = getPrcMainIHeaderById(prcMainHeader.getPpmId());
			prcMainHeader.setPpmProsesLevel(1);//revisi
			prcMainHeader.setAdmUserByPpmCurrentUser(prcMainHeader.getAdmUserByPpmCreator());
			prcMainHeader.setAdmRole(prcMainHeader.getAdmUserByPpmCreator().getAdmEmployee().getAdmRoleByEmpRole1());
			prcMainHeader.setAdmProses(SessionGetter.PR_PROSES_ID);
			saveEntity(prcMainHeader);
			
			// uploda document
			if(SessionGetter.isNotNull(docsKomentar)){
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}
			//save Komentar
			SessionGetter.saveHistoryGlobalPrOE(histMainComment,prcMainHeader,session);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
		
	}

	@Override
	public void setujuPR(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception {
		try {
			if(SessionGetter.getPropertiesValue("perusahaan", null).equalsIgnoreCase("ASKES")){
				setujuPRAskes(prcMainHeader, histMainComment, docsKomentar, docsKomentarFileName);
			}else{
				setujuPRGlobal(prcMainHeader, histMainComment, docsKomentar, docsKomentarFileName);
			}
		} catch (Exception e) {
			throw e;
		}
	}
	
	/**
	 * untuk kode perusahaan global
	 * @param prcMainHeader
	 * @param histMainComment
	 * @param docsKomentar
	 * @param docsKomentarFileName
	 * @throws Exception
	 */
	public void setujuPRGlobal(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception {
		try {
			startTransaction();
			Integer pembelian = prcMainHeader.getPembelian();
			prcMainHeader = getPrcMainIHeaderById(prcMainHeader.getPpmId());
			if(pembelian!=null){
				prcMainHeader.setPembelian(pembelian);
			}
			AdmProsesHirarki ahp = SessionGetter.getHirarkiProses(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel()+1, prcMainHeader.getAdmDept(), prcMainHeader.getPpmJumlahEe().doubleValue());
			if(ahp==null){
				AdmProses proses = SessionGetter.getProsesFlowByLevel(-2);//-2
				prcMainHeader.setPpmProsesLevel(1);
				prcMainHeader.setAdmProses(proses);
				ahp = SessionGetter.getHirarkiProsesByDistrict(proses, 1, prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahEe().doubleValue());
				if(ahp!=null){
					prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
					prcMainHeader.setAdmUserByPpmCurrentUser(SessionGetter.getEmployeeByRole(ahp.getAdmRoleByCurrentPos()).getAdmUser());
				}
				histMainComment.setProsesAksi(SessionGetter.getPropertiesValue("oe.1", null));
			}else{
				if(ahp.getAdmRoleByCurrentPos()!=null){
					prcMainHeader.setPpmProsesLevel(ahp.getTingkat());//setuju
					prcMainHeader.setAdmUserByPpmCurrentUser(SessionGetter.getEmployeeByRole(ahp.getAdmRoleByCurrentPos()).getAdmUser());
					prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
				}
			}
			saveEntity(prcMainHeader);
			
			// uploda document
			if(SessionGetter.isNotNull(docsKomentar)){
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}
			//save Komentar
			SessionGetter.saveHistoryGlobalPrOE(histMainComment,prcMainHeader,session);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}
	
	/**
	 * flow askes only
	 * @param prcMainHeader
	 * @param histMainComment
	 * @param docsKomentar
	 * @param docsKomentarFileName
	 * @throws Exception
	 */
	public void setujuPRAskes (PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception {
		try {
			startTransaction();
			Integer pembelian = prcMainHeader.getPembelian();
			prcMainHeader = getPrcMainIHeaderById(prcMainHeader.getPpmId());
			if(pembelian!=null){
				prcMainHeader.setPembelian(pembelian);
			}
			
			// uploda document
			if(SessionGetter.isNotNull(docsKomentar)){
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}
						
			AdmProsesHirarki ahp = SessionGetter.getHirarkiProses(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel(), prcMainHeader.getAdmDept(), prcMainHeader.getPpmJumlahEe().doubleValue());
			if(ahp==null){
				throw new Exception("Data Tidak Bisa di Simpan");
			}else{
				if(ahp.getAdmRoleByNextPos()!=null){
					prcMainHeader.setPpmProsesLevel(ahp.getTingkat()+1);//setuju
					prcMainHeader.setAdmUserByPpmCurrentUser(SessionGetter.getEmployeeByRole(ahp.getAdmRoleByNextPos()).getAdmUser());
					prcMainHeader.setAdmRole(ahp.getAdmRoleByNextPos());
					//save Komentar
					SessionGetter.saveHistoryGlobalPrOE(histMainComment,prcMainHeader,session);
					ahp = SessionGetter.getHirarkiProses(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel(), prcMainHeader.getAdmDept(), prcMainHeader.getPpmJumlahEe().doubleValue());
					if(ahp.getAdmRoleByNextPos()==null){
						//berarti terakhir
						AdmProses proses = SessionGetter.getProsesFlowByLevel(-3);//-3 langsung ke persiapn pengadaan
						prcMainHeader.setPpmProsesLevel(1);
						prcMainHeader.setAdmProses(proses);
						//insert OE
						List<PrcMainItem> items = listPrcMainItemByHeader(prcMainHeader);
						prcMainHeader.setPpmJumlahOe(SessionGetter.getEeFromItems(items));//disamaian dengan EE
						if(items!=null){
							for(PrcMainItem it:items){
								it.setItemQuantityOe(it.getItemQuantity());
								it.setItemPriceOe(it.getItemPrice());
								it.setItemTaxOe(it.getItemTax());
								it.setItemTotalPriceOe(it.getItemTotalPrice());
								saveEntity(it);
							}
						}
						ahp = SessionGetter.getHirarkiProsesByDistrict(proses, 1, prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahEe().doubleValue());
						if(ahp!=null){
							prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
							prcMainHeader.setAdmUserByPpmCurrentUser(SessionGetter.getEmployeeByRole(ahp.getAdmRoleByCurrentPos()).getAdmUser());
						}
						histMainComment.setProsesAksi(SessionGetter.getPropertiesValue("pp.1", null));
						//save Komentar
						SessionGetter.saveHistoryGlobalPrOE(histMainComment,prcMainHeader,session);
					}
				}else{
					//jika terakhir
					if(prcMainHeader.getPembelian()!=null && prcMainHeader.getPembelian()==0){//jika bukan pembelian
						AdmProses proses = SessionGetter.getProsesFlowByLevel(-3);//-3 langsung ke persiapn pengadaan
						prcMainHeader.setPpmProsesLevel(1);
						prcMainHeader.setAdmProses(proses);
						//insert OE
						List<PrcMainItem> items = listPrcMainItemByHeader(prcMainHeader);
						prcMainHeader.setPpmJumlahOe(SessionGetter.getEeFromItems(items));//disamaian dengan EE
						if(items!=null){
							for(PrcMainItem it:items){
								it.setItemQuantityOe(it.getItemQuantity());
								it.setItemPriceOe(it.getItemPrice());
								it.setItemTaxOe(it.getItemTax());
								it.setItemTotalPriceOe(it.getItemTotalPrice());
								saveEntity(it);
							}
						}
						ahp = SessionGetter.getHirarkiProsesByDistrict(proses, 1, prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahEe().doubleValue());
						if(ahp!=null){
							prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
							prcMainHeader.setAdmUserByPpmCurrentUser(SessionGetter.getEmployeeByRole(ahp.getAdmRoleByCurrentPos()).getAdmUser());
						}
						histMainComment.setProsesAksi(SessionGetter.getPropertiesValue("pp.1", null));
						//save Komentar
						SessionGetter.saveHistoryGlobalPrOE(histMainComment,prcMainHeader,session);
					}else{
						ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel(), prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahEe().doubleValue());
						if(ahp!=null){
							prcMainHeader.setPpmProsesLevel(SessionGetter.SELESAI);//selesai;
							prcMainHeader.setAdmRole(null);
							prcMainHeader.setAdmUserByPpmCurrentUser(null);
						}
						histMainComment.setProsesAksi(SessionGetter.getPropertiesValue("pr.2", null));
						//save Komentar
						SessionGetter.saveHistoryGlobalFinish(histMainComment,prcMainHeader,session);
					}
				}
			}
			saveEntity(prcMainHeader);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AdmUser> listEstimator(Object object) throws Exception {
		DetachedCriteria cr = DetachedCriteria.forClass(AdmUser.class);
		cr.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		DetachedCriteria crit = cr.createCriteria("admEmployee", JoinType.INNER_JOIN);
		crit.add(Restrictions.or(Restrictions.or(Restrictions.eq("admRoleByEmpRole1", SessionGetter.EstimatorRole()), Restrictions.eq("admRoleByEmpRole2", SessionGetter.EstimatorRole())),
				Restrictions.or(Restrictions.eq("admRoleByEmpRole3", SessionGetter.EstimatorRole()), Restrictions.eq("admRoleByEmpRole4", SessionGetter.EstimatorRole()))));
		return (List<AdmUser>)getListByDetachedCiteria(cr);
	}

	@Override
	public void saveEstimator(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception {
		try {
			startTransaction();
			AdmUser estimator = prcMainHeader.getAdmUserByPpmEstimator();
			prcMainHeader = getPrcMainIHeaderById(prcMainHeader.getPpmId());
			prcMainHeader.setAdmUserByPpmEstimator(estimator);
			prcMainHeader.setPpmProsesLevel(2);
			AdmProsesHirarki ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(), 2, prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahEe().doubleValue());
			prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
			prcMainHeader.setAdmUserByPpmCurrentUser(new AdmServiceImpl().getUserById(estimator));
			saveEntity(prcMainHeader);
			
			//upload dokumen
			if(docsKomentar!=null){
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}
			//save History
			SessionGetter.saveHistoryGlobalPrOE(histMainComment, prcMainHeader, session);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public void saveOeItems(List<PrcMainItem> listPrcMainItem) throws Exception {
		try {
			startTransaction();
			if(listPrcMainItem!=null && listPrcMainItem.size()>0){
				for(PrcMainItem item:listPrcMainItem){
					saveEntity(item);
				}
			}
			commitAndClear();
		} catch (Exception e) {;
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public void saveOeFinis(PrcMainHeader prcMainHeader,
			List<PrcMainItem> listPrcMainItem, HistMainComment histMainComment,
			File docsKomentar, String docsKomentarFileName) throws Exception {
		try {
			startTransaction();
			PrcMainHeader tmp = prcMainHeader;
			prcMainHeader = getPrcMainIHeaderById(prcMainHeader.getPpmId());
			prcMainHeader.setPpmJumlahOe(SessionGetter.getEeFromItems(listPrcMainItem));
			prcMainHeader.setLokasiPenyerahan(tmp.getLokasiPenyerahan());
			AdmProsesHirarki ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel()+1, prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahOe().doubleValue());
			if(ahp==null){
				throw new Exception("Kesalahan dalam Hirarki Proses");
			}else{
				if(ahp.getAdmRoleByCurrentPos()!=null){
					prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
					prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
					prcMainHeader.setAdmUserByPpmCurrentUser(SessionGetter.getEmployeeByRole(ahp.getAdmRoleByCurrentPos()).getAdmUser());
					/*if(prcMainHeader.getPpmJenisDetail()!=null) {
						AdmUser manager = SessionGetter.getManagerByPr(prcMainHeader);
						prcMainHeader.setAdmUserByPpmCurrentUser(manager);
					}else {
						throw new Exception("Untuk menyesuaikan SOP yang baru, Jenis Permintaan harus diupdate agar bisa melanjutkan");
					}*/
				}
			}
						
			//cek anggaran
			Double jumlahOe =0.0;
			//update item
			if(listPrcMainItem!=null && listPrcMainItem.size()>0){
				for(PrcMainItem item:listPrcMainItem){
					saveEntity(item);
					BigDecimal total = item.getItemQuantityOe().multiply(item.getItemPriceOe());
					jumlahOe = jumlahOe+total.doubleValue();
				}
			}
			
			
			//update jumlah OE
			prcMainHeader.setPpmJumlahOe(new BigDecimal(jumlahOe));
			saveEntity(prcMainHeader);
			
			
			
			//upload dokumen
			if(SessionGetter.isNotNull(docsKomentar)){
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}
			IntegrationService integrationService = new IntegrationServiceImpl();
			//Validate Anggaran
			ResultDto result = integrationService.changePr(listPrcMainItem,prcMainHeader);
			if(result.getStatus()!=200) {
				throw new Exception("Terjadi kesalahan saat proses checking anggaran ");
			}
			
			//insert history
			SessionGetter.saveHistoryGlobalPrOE(histMainComment, prcMainHeader, session);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public void revisiOE(PrcMainHeader prcMainHeader,
			List<PrcMainItem> listPrcMainItem, HistMainComment histMainComment,
			File docsKomentar, String docsKomentarFileName) throws Exception {
		try {
			startTransaction();
			prcMainHeader = getPrcMainIHeaderById(prcMainHeader.getPpmId());
			prcMainHeader.setAdmUserByPpmCurrentUser(prcMainHeader.getAdmUserByPpmEstimator());
			prcMainHeader.setPpmProsesLevel(2);
			AdmProsesHirarki ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(), 2, prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahOe().doubleValue());
			prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
			saveEntity(prcMainHeader);
			
			
			//upload dokumen
			if(SessionGetter.isNotNull(docsKomentar)){
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}
			
			//insert history
			SessionGetter.saveHistoryGlobalPrOE(histMainComment, prcMainHeader, session);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public void setujuOE(PrcMainHeader prcMainHeader,
			List<PrcMainItem> listPrcMainItem, HistMainComment histMainComment,
			File docsKomentar, String docsKomentarFileName) throws Exception {
		try {
			startTransaction();
			prcMainHeader = getPrcMainIHeaderById(prcMainHeader.getPpmId());
			AdmProsesHirarki ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel()+1, prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahOe().doubleValue());
			if(ahp==null){
					/*AdmProses proses = SessionGetter.ANGGARAN_PROSES_ID;//ke Anggaran
					prcMainHeader.setAdmProses(proses);
					prcMainHeader.setPpmProsesLevel(1);*/
					//cek jika panjang, palembang or baturaja
					AdmDistrict kantor = null;
					AdmDistrict kantorForUser = null;
					if(prcMainHeader.getPpmNomorPrSap().contains("1201")){//palembang
						kantor = new AdmDistrict(104);
						kantor = new AdmServiceImpl().getDistrictById(kantor);
						kantorForUser = kantor;
					}else if(prcMainHeader.getPpmNomorPrSap().contains("1202")){//baturaja
						kantor = new AdmDistrict(107);
						kantor = new AdmServiceImpl().getDistrictById(kantor);
						if(prcMainHeader.getPpmJumlahOe().doubleValue()>kantor.getLimit().doubleValue()){
							kantor = kantor.getDistrict();//ambil parentnya aka ke palembang
						}
						 if(prcMainHeader.getPpmJumlahOe().doubleValue() < 20000000) {
				        	  kantor = new AdmDistrict(107);
				          }
						kantorForUser = kantor;
					}else if(prcMainHeader.getPpmNomorPrSap().contains("1204")){//panjang
						kantor = new AdmDistrict(110);
						kantor = new AdmServiceImpl().getDistrictById(kantor);
						if(prcMainHeader.getPpmJumlahOe().doubleValue()>kantor.getLimit().doubleValue()){
							kantor = kantor.getDistrict();//ambil parentnya aka ke palembang
						}
						kantorForUser = kantor;

					}else if(prcMainHeader.getPpmNomorPrSap().contains("1203")){//baturaja 2
						kantor = new AdmDistrict(111);
						kantor = new AdmServiceImpl().getDistrictById(kantor);
						kantor = kantor.getDistrict();//ambil parentnya aka ke palembang
						kantorForUser = kantor;
					}else if(prcMainHeader.getPpmNomorPrSap().contains("11202")){//baturaja 2
						kantor = new AdmDistrict(113);
				          kantor = new AdmServiceImpl().getDistrictById(kantor);
				          if (prcMainHeader.getPpmJumlahOe().doubleValue() > kantor.getLimit().doubleValue()) {
				            kantor = kantor.getDistrict();
				          }
				          //jika dibawah 20 jt 
				          if(prcMainHeader.getPpmJumlahOe().doubleValue() < 20000000) {
				        	  kantor = new AdmDistrict(107);
				          }
							kantorForUser = kantor;

					}else if(prcMainHeader.getPpmNomorPrSap().contains("1601")){//baturaja
						kantor = new AdmDistrict(107);
						kantor = new AdmServiceImpl().getDistrictById(kantor);
						if(prcMainHeader.getPpmJumlahOe().doubleValue()>kantor.getLimit().doubleValue()){
							kantor = kantor.getDistrict();//ambil parentnya aka ke palembang
						}
						 if(prcMainHeader.getPpmJumlahOe().doubleValue() < 20000000) {
				        	  kantor = new AdmDistrict(107);
				          }
						kantorForUser = kantor;
					}
					
					prcMainHeader.setDistrictKeuangan(kantor);
					
					AdmRole smBarang = new AdmRole(112);//Langsung ke kabiro pengadaan barang
					AdmRole smJasa = new AdmRole(195);//langsung ke kabiro pengadaan jasa
					AdmProses proses = SessionGetter.PP_PROSES_ID;//langsung ke pemilihan buyer
					prcMainHeader.setAdmProses(proses);
					prcMainHeader.setPpmProsesLevel(2);//disposisi otomatis
					AdmUser usr  = SessionGetter.getUserByJenis(prcMainHeader.getPpmJenisDetail().getParentId(), 1).getAdmUser();
					prcMainHeader.setAdmUserByPpmCurrentUser(usr);
					prcMainHeader.setPengawasPp(prcMainHeader.getAdmUserByPpmCurrentUser());
					if(prcMainHeader.getPpmJenis().getId()==1){
						//langsung ke kabiro pengadaan barang
						//ahp = SessionGetter.getHirarkiProsesByJenis(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel(), prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahOe().doubleValue(), 1);

						prcMainHeader.setAdmRole(smBarang);
					}else if(prcMainHeader.getPpmJenis().getId()== 2){
						//langsung ke kabiro pengadaan jasa
						//ahp = SessionGetter.getHirarkiProsesByJenis(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel(), prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahOe().doubleValue(), 2);
						prcMainHeader.setAdmRole(smJasa);

					}else{
						throw new Exception("Kesalahan dalam hirarki");
					}
					
					//cek kewenangan
					
					/*ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(), 1, kantor, prcMainHeader.getPpmJumlahOe().doubleValue());
					if(ahp==null){
						throw new Exception("Kesalahan dalam hirarki");
					}*/
					
					//get mapping user
					/*AdmUser us = getMappingUserKeuangan(kantorForUser);
					if(us==null){
						throw new Exception("User Keuangan Belum di mapping");
					}else{
						prcMainHeader.setAdmUserByPpmCurrentUser(new AdmServiceImpl().getAdmUser(us));
						histMainComment.setCreatedBy(SessionGetter.getPropertiesValue("aa.1", null));
						
					}*/
					histMainComment.setCreatedBy(SessionGetter.getPropertiesValue("pp.2", null));
			}else{
				if(ahp.getAdmRoleByCurrentPos()!=null){
					prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
					prcMainHeader.setAdmUserByPpmCurrentUser(SessionGetter.getEmployeeByRole(ahp.getAdmRoleByCurrentPos()).getAdmUser());
					prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
					
					ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel(), prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahOe().doubleValue());
				}
			}
			saveEntity(prcMainHeader);
			
			//upload dokumen
			if(SessionGetter.isNotNull(docsKomentar)){
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}
			
			listPrcMainItem = listPrcMainItemByHeader(new PrcMainHeader(prcMainHeader.getPpmId()));
			
			//insert history
			SessionGetter.saveHistoryGlobalPrOE(histMainComment, prcMainHeader, session);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	public AdmUser getMappingUserKeuangan(AdmDistrict kantor) {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMappingKeuangan.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("admUser", FetchMode.JOIN);
		crit.add(Restrictions.eq("district", kantor));
		PrcMappingKeuangan keu = (PrcMappingKeuangan) getEntityByDetachedCiteria(crit);
		if(keu!=null){
			return keu.getAdmUser();
		}else
			return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> listPrcMainHeaderApprovalOE(Object object)
			throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		dc.add(Restrictions.eq("admProses", SessionGetter.OE_PROSES_ID));
		dc.add(Restrictions.eq("admUserByPpmCurrentUser", SessionGetter.getUser()));
		//Persetujuan OE
		dc.add(Restrictions.gt("ppmProsesLevel", 2));
		return (List<PrcMainHeader>) getListByDetachedCiteria(dc);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> listPrcMainHeaderRevisiEEInit(Object object)
			throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		dc.add(Restrictions.eq("admProses", SessionGetter.REVISI_EE));
		dc.add(Restrictions.eq("admUserByPpmCurrentUser", SessionGetter.getUser()));
		dc.add(Restrictions.eq("ppmProsesLevel", 1));
		return (List<PrcMainHeader>) getListByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AdmUser> listUserByProsesLevel(PrcMainHeader prcMainHeader)
			throws Exception {
		AdmProsesHirarki ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel(), prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahEe().doubleValue());
		DetachedCriteria crit = DetachedCriteria.forClass(AdmUser.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		DetachedCriteria sb = crit.createCriteria("admEmployee", JoinType.INNER_JOIN);
		sb.add(Restrictions.or(Restrictions.or(Restrictions.eq("admRoleByEmpRole1", ahp.getAdmRoleByNextPos()), Restrictions.eq("admRoleByEmpRole2", ahp.getAdmRoleByNextPos())),
				Restrictions.or(Restrictions.eq("admRoleByEmpRole3", ahp.getAdmRoleByNextPos()), Restrictions.eq("admRoleByEmpRole4", ahp.getAdmRoleByNextPos()))));
		return (List<AdmUser>) getListByDetachedCiteria(crit);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<AdmUser> listBuyer(PrcMainHeader prcMainHeader)
			throws Exception {
		AdmRole buyer = new AdmRole(109);
		DetachedCriteria crit = DetachedCriteria.forClass(AdmUser.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		DetachedCriteria sb = crit.createCriteria("admEmployee", JoinType.INNER_JOIN);
		sb.add(Restrictions.or(Restrictions.or(Restrictions.eq("admRoleByEmpRole1", buyer ), Restrictions.eq("admRoleByEmpRole2", buyer)),
				Restrictions.or(Restrictions.eq("admRoleByEmpRole3", buyer), Restrictions.eq("admRoleByEmpRole4", buyer))));
		return (List<AdmUser>) getListByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> listPrcMainHeaderDisposisiPp(Object object)
			throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		dc.add(Restrictions.eq("admProses", SessionGetter.PP_PROSES_ID));
		dc.add(Restrictions.eq("admUserByPpmCurrentUser", SessionGetter.getUser()));
		//pembuatan disposisi
		dc.add(Restrictions.isNull("admUserByPpmBuyer"));
		return (List<PrcMainHeader>) getListByDetachedCiteria(dc);
	}

	@Override
	public void saveDisposisiPP(PrcMainHeader ppm,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception {
		try {
			startTransaction();
			PrcMainHeader prcMainHeader = getPrcMainIHeaderById(ppm.getPpmId());
			prcMainHeader.setBrt(ppm.getBrt());
			prcMainHeader.setAdmUserByPpmBuyer(ppm.getAdmUserByPpmBuyer());
			if(prcMainHeader.getPpmJenisDetail()==null) {
				AdmJenis jenis = SessionGetter.getAdmJenisById(ppm.getPpmJenisDetail().getId());
				prcMainHeader.setPpmJenisDetail(jenis);
			}
			AdmProsesHirarki ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel()+1, prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahOe().doubleValue());

			if(ahp==null){
				throw new Exception("Kesalahan dalam hirarki");
			}else{
				if(ahp.getAdmRoleByCurrentPos()!=null){
					AdmUser user = new AdmServiceImpl().getUserById(prcMainHeader.getAdmUserByPpmBuyer());
					AdmUser watchingUser = new AdmServiceImpl().getUserById(prcMainHeader.getAdmUserByPpmCurrentUser());
					prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
					prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
					prcMainHeader.setAdmUserByPpmCurrentUser(user);
					prcMainHeader.setIsWatched(ppm.getIsWatched());
					
				}
			}
			//save header
			if(prcMainHeader.getAdmUserByPpmBuyer().getId().longValue()!=prcMainHeader.getAdmUserByPpmCurrentUser().getId().longValue()){
				throw new Exception("Pemilihan Buyer Gagal");
			}
			saveEntity(prcMainHeader);
			
			//upload doc
			if(SessionGetter.isNotNull(docsKomentar)){
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}
			
			//insert history
			SessionGetter.saveHistoryGlobalPrOE(histMainComment, prcMainHeader, session);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> listPrcMainHeaderPpSetup(AdmUser user)
			throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		
		List<AdmDelegasi> del = new AdmServiceImpl().getListDelegasiByUserKepadaAllAktifNotAktif(user);
		if(del!=null && del.size()>0){
			dc.add(Restrictions.or(Restrictions.eq("admUserByPpmBuyer", user), Restrictions.eq("admUserByPpmBuyer", del.get(0).getDari())));
		}else
			dc.add(Restrictions.eq("admUserByPpmBuyer", user));
		
		dc.add(Restrictions.eq("admUserByPpmCurrentUser", user));
		dc.add(Restrictions.eq("admProses", SessionGetter.PP_PROSES_ID));
		dc.add(Restrictions.isNull("admUserByPpmAdmBuyer"));
		dc.add(Restrictions.isNull("finalisasiSpph"));
		return (List<PrcMainHeader>) getListByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMethod> listMetode(Object object) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMethod.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		return (List<PrcMethod>) getListByDetachedCiteria(crit);
	}

	@Override
	public void savePpSetup(PrcMainHeader prcMain,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName, PanitiaPengadaan panitiaPengadaan,
			List<PanitiaPengadaanDetail> listPanitiaDetail) throws Exception {
		try {
			startTransaction();
			PrcMainHeader prcMainHeader = getPrcMainIHeaderById(prcMain.getPpmId());
			prcMainHeader.setPpmNomorRfq(prcMain.getPpmNomorRfq());
			
			if(panitiaPengadaan!=null){
				saveEntity(panitiaPengadaan);
				if(listPanitiaDetail!=null && listPanitiaDetail.size()>0){
					for(PanitiaPengadaanDetail d:listPanitiaDetail){
						d.setPanitia(panitiaPengadaan);
						saveEntity(d);
					}
				}
				prcMainHeader.setPanitia(panitiaPengadaan);
			}
			
				prcMainHeader.setAdmUserByPpmAdmBuyer(prcMainHeader.getAdmUserByPpmBuyer());
				prcMainHeader.setPpmProsesLevel(prcMainHeader.getPpmProsesLevel()+1);
				prcMainHeader.setAdmRole(prcMainHeader.getAdmRole());
			
			prcMainHeader.setPrcMethod(prcMain.getPrcMethod());
			if(prcMainHeader.getAdmUserByPpmAdmBuyer().getId().longValue()!=prcMainHeader.getAdmUserByPpmCurrentUser().getId().longValue()){
				throw new Exception("Kesahalan dalam Proses ADM BUYER");
			}
			saveEntity(prcMainHeader);
			
			//upload doc
			if(SessionGetter.isNotNull(docsKomentar)){
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}
			
			//upload history
			SessionGetter.saveHistoryGlobalPrOE(histMainComment, prcMainHeader, session);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<VndHeader> listMitraKerjaDiUndang(PrcMainHeader prcMainHeader)
			throws Exception {
		List<Integer> finClass = new ArrayList<Integer>();
		if(SessionGetter.isNotNull(prcMainHeader.getKecil()) && prcMainHeader.getKecil()==1){
			finClass.add(1);
		}
		if(SessionGetter.isNotNull(prcMainHeader.getMenengah()) && prcMainHeader.getMenengah()==1){
			finClass.add(2);
		}
		if(SessionGetter.isNotNull(prcMainHeader.getBesar()) && prcMainHeader.getBesar()==1){
			finClass.add(3);
		}
		if(SessionGetter.isNotNull(prcMainHeader.getGrade2()) && prcMainHeader.getGrade2()==1){
			finClass.add(4);
		}
		if(SessionGetter.isNotNull(prcMainHeader.getGrade3()) && prcMainHeader.getGrade3()==1){
			finClass.add(5);
		}
		if(SessionGetter.isNotNull(prcMainHeader.getGrade4()) && prcMainHeader.getGrade4()==1){
			finClass.add(6);
		}
		if(SessionGetter.isNotNull(prcMainHeader.getGrade5()) && prcMainHeader.getGrade5()==1){
			finClass.add(7);
		}
		if(SessionGetter.isNotNull(prcMainHeader.getGrade6()) && prcMainHeader.getGrade6()==1){
			finClass.add(8);
		}
		if(SessionGetter.isNotNull(prcMainHeader.getGrade7()) && prcMainHeader.getGrade7()==1){
			finClass.add(9);
		}
		
		DetachedCriteria cr = DetachedCriteria.forClass(VndHeader.class);
		if(finClass.size()>0)
			cr.add(Restrictions.in("vendorFinClass", finClass));
		else
			return new ArrayList<VndHeader>();
		
		cr.add(Restrictions.and(
				Restrictions.ne("vendorStatus", "S"), 
				Restrictions.ne("vendorStatus", "B")
//				Restrictions.in("vendorId", vendorNotExpired()
						));//suspend
	    String vendorFilter = SessionGetter.valueProperty("vendor.aktif.filter");
		    if((StringUtils.isNotBlank(vendorFilter)) && (StringUtils.equalsIgnoreCase(vendorFilter, "1"))) {
		            cr.add(Restrictions.in("vendorId", vendorNotExpired()));
		}
		
//		cr.add(Restrictions.in("vendorId", vendorNotExpired()));//yang gak expired yang di undang
		DetachedCriteria crit = cr.createCriteria("vndProducts", JoinType.INNER_JOIN);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		DetachedCriteria sb = crit.createCriteria("comGroup", JoinType.INNER_JOIN);
		//yang sama groupnya
		sb.add(Restrictions.eq("groupCode", prcMainHeader.getGroupCode().trim()));
		
		cr.addOrder(Order.asc("vendorName"));
		return (List<VndHeader>) getListByDetachedCiteria(cr);
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> listPrcMainHeaderPpVendor(Object object)
			throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		
		List<AdmDelegasi> del = new AdmServiceImpl().getListDelegasiByUserKepadaAllAktifNotAktif(SessionGetter.getUser());
		if(del!=null && del.size()>0){
			dc.add(Restrictions.or(Restrictions.eq("admUserByPpmAdmBuyer", SessionGetter.getUser()), Restrictions.eq("admUserByPpmAdmBuyer", del.get(0).getDari())));
			dc.add(Restrictions.or(Restrictions.eq("admUserByPpmBuyer", SessionGetter.getUser()), Restrictions.eq("admUserByPpmBuyer", del.get(0).getDari())));
		}else{
			dc.add(Restrictions.eq("admUserByPpmAdmBuyer", SessionGetter.getUser()));
			dc.add(Restrictions.eq("admUserByPpmBuyer", SessionGetter.getUser()));
		}
		dc.add(Restrictions.eq("admUserByPpmCurrentUser", SessionGetter.getUser()));
		dc.add(Restrictions.eq("admProses", SessionGetter.PP_PROSES_ID));
		dc.add(Restrictions.isNull("finalisasiSpph"));
		return (List<PrcMainHeader>) getListByDetachedCiteria(dc);
	}
	
	private List<Long> vendorNotExpired() throws Exception{
		List<Long> retMe = Lists.newArrayList();
		try {
			startTransaction();
			Query q = session.createSQLQuery("SELECT vendor_id FROM vw_vendor_expired WHERE (statsiup+stattdp+domisili+ijin+cert) = 0");
			List<Object> obj = q.list();
			if(obj!=null && obj.size()>0){
				Iterator<Object> it = obj.iterator();
				while(it.hasNext()){
					retMe.add(Long.valueOf(String.valueOf(it.next())));
					it.remove();
				}
			}
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
		return retMe;
	}
	private Integer countExpiredFlag(long vendorId) {
		Integer retMe = 0;
		try {
			startTransaction();
			Query q = session.createSQLQuery("select (statsiup+stattdp+domisili+ijin+cert) from (select * from vw_vendor_expired) where vendor_id="+vendorId);
			Object obj = q.uniqueResult();
			retMe = Integer.valueOf(String.valueOf(obj));
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			e.printStackTrace();
		}
		return retMe;
	}

	@Override
	public PrcMainHeader getPrcMainHeaderSPPHById(Long ppmId) throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByPpmEstimator", FetchMode.JOIN);
		dc.setFetchMode("admUserByPpmCreator", FetchMode.JOIN);
		dc.setFetchMode("admUserByPpmBuyer", FetchMode.JOIN);
		dc.setFetchMode("admUserByPpmAdmBuyer", FetchMode.JOIN);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		dc.setFetchMode("admDistrict", FetchMode.JOIN);
		dc.setFetchMode("prcMethod", FetchMode.JOIN);
		dc.setFetchMode("prcTemplate", FetchMode.JOIN);
		dc.setFetchMode("prcTemplate", FetchMode.JOIN);
		dc.setFetchMode("panitia", FetchMode.JOIN);
		dc.setFetchMode("ppmJenisDetail", FetchMode.JOIN);
		dc.add(Restrictions.eq("ppmId", ppmId));
		return (PrcMainHeader) getEntityByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcTemplate> listTemplate(Object obj)
			throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcTemplate.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		return (List<PrcTemplate>) getListByDetachedCiteria(crit);
	}

	@Override
	public void savePpMitraKerja(PrcMainHeader mainHeader,
			HistMainComment histMainComment,
			List<PrcMainVendor> listPrcMainVendor, File docsKomentar,
			String docsKomentarFileName, List<File> ppdDocument,
			List<String> ppdCategory, List<String> ppdDescription,PrcMainPreparation preparation, List<String> ppdDocumentFileName)
			throws Exception {
		try {
			startTransaction();
			PrcMainHeader prcMainHeader = getPrcMainIHeaderById(mainHeader.getPpmId());
			prcMainHeader.setPrcTemplate(mainHeader.getPrcTemplate());
			prcMainHeader.setPpmTipeA(mainHeader.getPpmTipeA());
			prcMainHeader.setPpmTipeB(mainHeader.getPpmTipeB());
			prcMainHeader.setPpmTipeC(mainHeader.getPpmTipeC());
			prcMainHeader.setKecil(mainHeader.getKecil());
			prcMainHeader.setMenengah(mainHeader.getMenengah());
			prcMainHeader.setGroupCode(mainHeader.getGroupCode());
			prcMainHeader.setBesar(mainHeader.getBesar());
			prcMainHeader.setGrade2(mainHeader.getGrade2());
			prcMainHeader.setGrade3(mainHeader.getGrade3());
			prcMainHeader.setGrade4(mainHeader.getGrade4());
			prcMainHeader.setGrade5(mainHeader.getGrade5());
			prcMainHeader.setGrade6(mainHeader.getGrade6());
			prcMainHeader.setGrade7(mainHeader.getGrade7());
			prcMainHeader.setOeOpen(mainHeader.getOeOpen());
			prcMainHeader.setPpmEauction(mainHeader.getPpmEauction());
			prcMainHeader.setPrcMethod(mainHeader.getPrcMethod());
			
			if(prcMainHeader.getPrcMethod().getId()>=4){//larikan ke pra kualifikasi (lelang 4,5,6)
				prcMainHeader.setAdmProses(SessionGetter.PQ_PROSES_ID);
				prcMainHeader.setPpmProsesLevel(1);
				prcMainHeader.setAdmRole(prcMainHeader.getAdmRole());
				prcMainHeader.setAdmUserByPpmCurrentUser(prcMainHeader.getAdmUserByPpmBuyer());
				histMainComment.setCreatedBy(SessionGetter.getPropertiesValue("pq.1", null));
			}else{
				AdmProsesHirarki ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel()+1, prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahOe().doubleValue());
				
				if(ahp==null){
					throw new Exception("Kesalahan dalam hirarki");
				}else{
					
					if(ahp.getAdmRoleByCurrentPos()!=null){
						if(ahp.getAdmRoleByCurrentPos().getRoleName().trim().equalsIgnoreCase("pembeli")){
//						if(ahp.getAdmRoleByCurrentPos().getRoleName().trim().equalsIgnoreCase("pengawas pp")){
//							prcMainHeader.setAdmUserByPpmCurrentUser(prcMainHeader.getPengawasPp());
//							prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
							prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
							prcMainHeader.setAdmUserByPpmCurrentUser(prcMainHeader.getAdmUserByPpmBuyer());
							prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
							histMainComment.setCreatedBy("Finalisasi SPPH");
							prcMainHeader.setFinalisasiSpph(1);
						}else{
							prcMainHeader.setAdmUserByPpmCurrentUser(SessionGetter.getEmployeeByRole(ahp.getAdmRoleByCurrentPos()).getAdmUser());
							prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
						}
						
//						/** masukan ke panitia **/
//						if(prcMainHeader.getPanitia()!=null){
//							prcMainHeader.setAdmProses(SessionGetter.PANITIA_PP_PROSES_ID);
//							prcMainHeader.setPpmProsesLevel(1);
//							ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel(), prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahOe().doubleValue());
//							prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
//							prcMainHeader.setAdmUserByPpmCurrentUser(SessionGetter.getEmployeeByRole(ahp.getAdmRoleByCurrentPos()).getAdmUser());
//							histMainComment.setCreatedBy(SessionGetter.getPropertiesValue("pp.7", null));
//						}
					}
				}
			}
			saveEntity(prcMainHeader);
			
			//praparation
			preparation.setPrcMainHeader(prcMainHeader);
			saveEntity(preparation);
			
			//prc vendor insert jika bukan lelang
			if(SessionGetter.isNotNull(listPrcMainVendor) && prcMainHeader.getAdmProses()!=SessionGetter.PQ_PROSES_ID){
				for(PrcMainVendor p: listPrcMainVendor){
					PrcMainVendorId id = new PrcMainVendorId();
					id.setPpmId(new BigDecimal(prcMainHeader.getPpmId()));
					id.setVendorId(p.getVndHeader().getVendorId());
					p.setId(id);
					p.setPrcMainHeader(prcMainHeader);
					if(p.getPmpStatus()==1)
						saveEntity(p);
				}
			}
			
			//upload dokumen
			if(SessionGetter.isNotNull(ppdDocument)){
				int i = 0;
				for(File f:ppdDocument){
					PrcMainDoc doc = new PrcMainDoc();
					doc.setPrcMainHeader(prcMainHeader);
					doc.setDocCategory(ppdCategory.get(i));
					doc.setDocName(ppdDescription.get(i));
					doc.setDocPath(UploadUtil.uploadDocsFile(f, ppdDocumentFileName.get(i)));
					saveEntity(doc);
					i++;
				}
			}
			
			//upload dok hist
			if(SessionGetter.isNotNull(docsKomentar)){
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}
			
			//upload history
			SessionGetter.saveHistoryGlobalPrOE(histMainComment, prcMainHeader, session);
			
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainVendor> listMitraKerjaByHeader(PrcMainHeader prcMainHeader)
			throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMainVendor.class);
	    crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
	    crit.add(Restrictions.eq("prcMainHeader", prcMainHeader));
	    DetachedCriteria sb = crit.createCriteria("vndHeader", JoinType.INNER_JOIN);
	    sb.addOrder(Order.asc("vendorName"));
	    return (List)getListByDetachedCiteria(crit);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainVendor> listMitraKerjaByStatusPq(PrcMainHeader prcMainHeader)
			throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMainVendor.class);
	    crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
	    crit.add(Restrictions.eq("prcMainHeader", prcMainHeader));
	    crit.add(Restrictions.eq("pmpPraqStatus", 1));
	    DetachedCriteria sb = crit.createCriteria("vndHeader", JoinType.INNER_JOIN);
	    sb.addOrder(Order.asc("vendorName"));
	    return (List)getListByDetachedCiteria(crit);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainVendor> listMitraKerjaTidakLulusByStatusPq(PrcMainHeader prcMainHeader)
			throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMainVendor.class);
	    crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
	    crit.add(Restrictions.eq("prcMainHeader", prcMainHeader));
	    crit.add(Restrictions.or(Restrictions.isNull("pmpPraqStatus"), Restrictions.eq(("pmpPraqStatus"), 0)));
	    DetachedCriteria sb = crit.createCriteria("vndHeader", JoinType.INNER_JOIN);
	    sb.addOrder(Order.asc("vendorName"));
	    return (List)getListByDetachedCiteria(crit);
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainVendor> listMitraKerjaByStatusTeknis(PrcMainHeader prcMainHeader)
			throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMainVendor.class);
	    crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
	    crit.add(Restrictions.eq("prcMainHeader", prcMainHeader));
	    crit.add(Restrictions.eq("pmpPraqStatus", 1));
	    crit.add(Restrictions.eq("pmpTechnicalStatus", 1));
	    DetachedCriteria sb = crit.createCriteria("vndHeader", JoinType.INNER_JOIN);
	    sb.addOrder(Order.asc("vendorName"));
	    return (List)getListByDetachedCiteria(crit);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainVendor> listMitraKerjaTerdaftar(PrcMainHeader prcMainHeader)
			throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMainVendor.class);
	    crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
	    crit.add(Restrictions.eq("prcMainHeader", prcMainHeader));
	    crit.add(Restrictions.eq("pmpStatus", 3));
	    DetachedCriteria sb = crit.createCriteria("vndHeader", JoinType.INNER_JOIN);
	    sb.addOrder(Order.asc("vendorName"));
	    return (List)getListByDetachedCiteria(crit);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainDoc> listPrcMainDocByHeader(PrcMainHeader prcMainHeader)
			throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMainDoc.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("prcMainHeader", prcMainHeader));
		return (List<PrcMainDoc>) getListByDetachedCiteria(crit);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PrcManualDoc> listPrcManualDocByHeader(PrcMainHeader prcMainHeader)
			throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcManualDoc.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("prcMainHeader", prcMainHeader));
		return (List<PrcManualDoc>) getListByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> listPrcMainHeaderPpApproval(Object object)
			throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		dc.add(Restrictions.eq("admUserByPpmCurrentUser", SessionGetter.getUser()));
		dc.add(Restrictions.eq("admProses", SessionGetter.PP_PROSES_ID));
		dc.add(Restrictions.gt("ppmProsesLevel", 4));//masih di HC
		return (List<PrcMainHeader>) getListByDetachedCiteria(dc);
	}

	@Override
	public PrcMainPreparation getPrcMainPreparationByHeader(
			PrcMainHeader prcMainHeader) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMainPreparation.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.idEq(prcMainHeader.getPpmId()));
		return (PrcMainPreparation) getEntityByDetachedCiteria(crit);
	}

	@Override
	public void revisiApprovalPP(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception {
		try {
			startTransaction();
			prcMainHeader  = getPrcMainIHeaderById(prcMainHeader.getPpmId());
			AdmProsesHirarki ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(), 3, prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahOe().doubleValue());
			prcMainHeader.setPpmProsesLevel(4);
			prcMainHeader.setAdmUserByPpmCurrentUser(prcMainHeader.getAdmUserByPpmAdmBuyer());
			prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
			prcMainHeader.setPanitia(null);
			saveEntity(prcMainHeader);
			
			//upload doc
			if(SessionGetter.isNotNull(docsKomentar)){
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}
			
			//insert History
			SessionGetter.saveHistoryGlobalPrOE(histMainComment, prcMainHeader, session);
			
			//remove preparation
			PrcMainPreparation pre = getPrcMainPreparationByHeader(prcMainHeader);
			deleteEntity(pre);
			
			//remove doc
			List<PrcMainDoc> lst = listPrcMainDocByHeader(prcMainHeader);
			if(lst!=null){
				for(PrcMainDoc d:lst){
					deleteEntity(d);
				}
			}
			
			//remove vendor
			List<PrcMainVendor> lstv = listMitraKerjaByHeader(prcMainHeader);
			if(lstv!=null){
				for(PrcMainVendor v:lstv){
					deleteEntity(v);
				}
			}
			
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}
	
	@Override
	public void setujuApprovalPP(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception {
		try {
			startTransaction();
			prcMainHeader  = getPrcMainIHeaderById(prcMainHeader.getPpmId());
			
			AdmProsesHirarki ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel()+1, prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahOe().doubleValue());
			if(ahp!=null){
							System.out.println(">>>>. "+ahp.getAdmRoleByCurrentPos().getRoleName());
							prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
							prcMainHeader.setAdmUserByPpmCurrentUser(prcMainHeader.getAdmUserByPpmBuyer());
							prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
							histMainComment.setCreatedBy("Finalisasi SPPH");
							prcMainHeader.setFinalisasiSpph(1);
				}else{
							//means last go to next proses
							AdmProses proses = SessionGetter.getProsesFlowByLevel(-4);//RFQ Proses Pengadaan
							ahp = SessionGetter.getHirarkiProsesByDistrict(proses, 1, prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahOe().doubleValue());
							prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
							prcMainHeader.setAdmUserByPpmCurrentUser(prcMainHeader.getAdmUserByPpmAdmBuyer());
							prcMainHeader.setPpmProsesLevel(1);
							prcMainHeader.setAdmProses(proses);
							
							if(prcMainHeader.getPanitia()!=null) {
								List<PanitiaPengadaanDetail> listPPPD = getListPanitiaPengadaanDetailByPanitiaStatus(
										prcMainHeader.getPanitia(), 0);
								for (PanitiaPengadaanDetail pd : listPPPD) {
										pd.setStatus(1);
										saveEntity(pd);
								}
							}
							
							histMainComment.setCreatedBy(SessionGetter.getPropertiesValue("rfq.1", null));
							histMainComment.setAksi("Finalisasi SPPH");
							histMainComment.setProsesAksi("Finalisasi SPPH");
				}
			
			saveEntity(prcMainHeader);
			//upload doc
			if(SessionGetter.isNotNull(docsKomentar)){
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}
			
			//insert History
			SessionGetter.saveHistoryGlobalPrOE(histMainComment, prcMainHeader, session);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}
//	@Override
//	public void setujuApprovalPP(PrcMainHeader prcMainHeader,
//			HistMainComment histMainComment, File docsKomentar,
//			String docsKomentarFileName) throws Exception {
//		try {
//			startTransaction();
//			prcMainHeader  = getPrcMainIHeaderById(prcMainHeader.getPpmId());
//			
//			AdmProsesHirarki ahp = checkIsAhpNull(prcMainHeader);
//			if(ahp!=null){
//				System.out.println(">>>>. "+ahp.getAdmRoleByCurrentPos().getRoleName());
//				if(ahp.getAdmRoleByCurrentPos()!=null){
//					
//					if(ahp.getAdmRoleByCurrentPos().getRoleName().trim().equalsIgnoreCase("pembeli")){
//						prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
//						prcMainHeader.setAdmUserByPpmCurrentUser(prcMainHeader.getAdmUserByPpmBuyer());
//						prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
//						histMainComment.setCreatedBy("Finalisasi SPPH");
//						prcMainHeader.setFinalisasiSpph(1);
//					}else if(StringUtils.equalsIgnoreCase(ahp.getAdmRoleByCurrentPos().getRoleName(), "panitia pengadaan")){
//						/** masukan ke panitia **/
//						if(prcMainHeader.getPanitia()!=null){
//							prcMainHeader.setAdmProses(SessionGetter.PANITIA_PP_PROSES_ID);
//							prcMainHeader.setPpmProsesLevel(1);
//							ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel(), prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahOe().doubleValue());
//							prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
//							prcMainHeader.setAdmUserByPpmCurrentUser(SessionGetter.getEmployeeByRole(ahp.getAdmRoleByCurrentPos()).getAdmUser());
//							histMainComment.setCreatedBy(SessionGetter.getPropertiesValue("pp.7", null));
//						}
////					} else if (StringUtils.equalsIgnoreCase(ahp.getAdmRoleByCurrentPos().getRoleName(),
////							"pengawas pp")) {
////						if (prcMainHeader.getPanitia() != null) {
////							prcMainHeader.setAdmProses(SessionGetter.PANITIA_PP_PROSES_ID);
////							prcMainHeader.setPpmProsesLevel(1);
////							ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(),
////									prcMainHeader.getPpmProsesLevel(), prcMainHeader.getAdmDistrict(),
////									prcMainHeader.getPpmJumlahOe().doubleValue());
////							prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
////							prcMainHeader.setAdmUserByPpmCurrentUser(
////									SessionGetter.getEmployeeByRole(ahp.getAdmRoleByCurrentPos()).getAdmUser());
////							histMainComment.setCreatedBy(SessionGetter.getPropertiesValue("pp.7", null));
////						} else {
////							prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
////							prcMainHeader.setAdmUserByPpmCurrentUser(
////									SessionGetter.getEmployeeByRole(ahp.getAdmRoleByCurrentPos()).getAdmUser());
////							prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
////						}
//					}
//					else{
//						if(prcMainHeader.getPpmJenis()== new AdmJenis(1)){
//							ahp = SessionGetter.getHirarkiProsesByJenis(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel()+1, 
//									prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahOe().doubleValue(), 1);
//							prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
//							prcMainHeader.setAdmUserByPpmCurrentUser(SessionGetter.getEmployeeByRole(ahp.getAdmRoleByCurrentPos()).getAdmUser());
//							prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
//						}else if(prcMainHeader.getPpmJenis()== new AdmJenis(2)){
//							ahp = SessionGetter.getHirarkiProsesByJenis(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel()+1, 
//									prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahOe().doubleValue(), 2);
//							prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
//							prcMainHeader.setAdmUserByPpmCurrentUser(SessionGetter.getEmployeeByRole(ahp.getAdmRoleByCurrentPos()).getAdmUser());
//							prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
//						}
//						
//					}
//				}
//			}else{
//				//means last go to next proses
//				AdmProses proses = SessionGetter.getProsesFlowByLevel(-4);//RFQ Proses Pengadaan
//				ahp = SessionGetter.getHirarkiProsesByDistrict(proses, 1, prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahOe().doubleValue());
//				
//				prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
//				prcMainHeader.setAdmUserByPpmCurrentUser(prcMainHeader.getAdmUserByPpmAdmBuyer());
//				prcMainHeader.setPpmProsesLevel(1);
//				prcMainHeader.setAdmProses(proses);
//				histMainComment.setCreatedBy(SessionGetter.getPropertiesValue("rfq.1", null));
//				histMainComment.setAksi("Finalisasi SPPH");
//				histMainComment.setProsesAksi("Finalisasi SPPH");
//			}
//			
//			saveEntity(prcMainHeader);
//			//upload doc
//			if(SessionGetter.isNotNull(docsKomentar)){
//				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
//			}
//			
//			//insert History
//			SessionGetter.saveHistoryGlobalPrOE(histMainComment, prcMainHeader, session);
//			commitAndClear();
//		} catch (Exception e) {
//			RollbackAndClear();
//			throw e;
//		}
//		
//	}

	@Override
	public int countPrcMainHeaderPR(Object object) throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setProjection(Projections.rowCount());
		Object obj = getEntityByDetachedCiteria(dc);
		return Integer.valueOf(String.valueOf(obj)).intValue();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> listPrcMainHeaderPR(Object object, int page,
			int i) throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		dc.add(Restrictions.isNull("ppmNomorRfq"));
		return (List<PrcMainHeader>) getListByDetachedCiteriaPaging(dc, page, i);
	}

	@Override
	public PrcMainVendorQuote getQuoteByVendor(PrcMainVendor prcMainVendor)
			throws Exception {
		System.out.println(">> "+org.hibernate.Version.getVersionString());
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMainVendorQuote.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		PrcMainVendorQuoteId id = new PrcMainVendorQuoteId();
		id.setPpmId(prcMainVendor.getId().getPpmId());
		id.setVendorId(prcMainVendor.getId().getVendorId());
		crit.add(Restrictions.idEq(id));
		return (PrcMainVendorQuote) getEntityByDetachedCiteria(crit);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainVendorQuoteAdmtech> listItemAdministrasi(
			PrcMainVendorQuote prcMainVendorQuote,PrcMainHeader mainHeader) throws Exception {
		if(prcMainVendorQuote!=null){
			DetachedCriteria crit = DetachedCriteria.forClass(PrcMainVendorQuoteAdmtech.class);
			crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			crit.add(Restrictions.eq("prcMainVendorQuote", prcMainVendorQuote));
			crit.add(Restrictions.eq("tempTipe", "1"));
			return (List<PrcMainVendorQuoteAdmtech>) getListByDetachedCiteria(crit);
		}else{
			return getListItemAdministrasiInitial(mainHeader);
		}
	}

	@SuppressWarnings("unchecked")
	private List<PrcMainVendorQuoteAdmtech> getListItemAdministrasiInitial(
			PrcMainHeader mainHeader) {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcTemplateDetail.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		DetachedCriteria sb = crit.createCriteria("prcTemplate", JoinType.INNER_JOIN);
		crit.add(Restrictions.eq("prcTemplate", mainHeader.getPrcTemplate()));
		crit.add(Restrictions.eq("tipe", 1));
		List<PrcTemplateDetail> lstD = (List<PrcTemplateDetail>) getListByDetachedCiteria(crit);
		List<PrcMainVendorQuoteAdmtech> retMe = new ArrayList<PrcMainVendorQuoteAdmtech>();
		if(lstD!=null && lstD.size()>0 ){
			for(PrcTemplateDetail d:lstD){
				PrcMainVendorQuoteAdmtech pq = new PrcMainVendorQuoteAdmtech();
				pq.setPrcTemplateDetail(d);
				pq.setTempItem(d.getTempdetName());
				pq.setTempTipe(d.getTipe().toString());
				pq.setTempCheckVendor(0);
				retMe.add(pq);
			}
		}
		return retMe;
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainVendorQuoteAdmtech> listItemTeknis(
			PrcMainVendorQuote prcMainVendorQuote,PrcMainHeader mainHeader) throws Exception {
		if(prcMainVendorQuote!=null){
			DetachedCriteria crit = DetachedCriteria.forClass(PrcMainVendorQuoteAdmtech.class);
			crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			DetachedCriteria sb = crit.createCriteria("prcTemplateDetail", JoinType.INNER_JOIN);
			crit.add(Restrictions.eq("prcMainVendorQuote", prcMainVendorQuote));
			crit.add(Restrictions.eq("tempTipe", "2"));
			return (List<PrcMainVendorQuoteAdmtech>) getListByDetachedCiteria(crit);
		}else{
			return getListItemTeknisInitial(mainHeader);
		}
	}
	
	@SuppressWarnings("unchecked")
	private List<PrcMainVendorQuoteAdmtech> getListItemTeknisInitial(
			PrcMainHeader mainHeader) {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcTemplateDetail.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("prcTemplate", mainHeader.getPrcTemplate()));
		crit.add(Restrictions.eq("tipe", 2));
		List<PrcTemplateDetail> lstD = (List<PrcTemplateDetail>) getListByDetachedCiteria(crit);
		List<PrcMainVendorQuoteAdmtech> retMe = new ArrayList<PrcMainVendorQuoteAdmtech>();
		if(lstD!=null && lstD.size()>0 ){
			for(PrcTemplateDetail d:lstD){
				PrcMainVendorQuoteAdmtech pq = new PrcMainVendorQuoteAdmtech();
				pq.setPrcTemplateDetail(d);
				pq.setTempItem(d.getTempdetName());
				pq.setTempTipe(d.getTipe().toString());
				pq.setTempCheckVendor(0);
				pq.setTempWeight(d.getTempdetWeight());
				retMe.add(pq);
			}
		}
		return retMe;
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainVendorQuoteItem> listQuoteItem(PrcMainVendorQuote quote, List<PrcMainItem> listItem)
			throws Exception {
		if(quote!=null){
			DetachedCriteria crit = DetachedCriteria.forClass(PrcMainVendorQuoteItem.class);
			crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			DetachedCriteria cr = crit.createCriteria("prcMainItem", JoinType.INNER_JOIN);
			cr.setFetchMode("admUom", FetchMode.JOIN);
			crit.add(Restrictions.eq("prcMainVendorQuote", quote));
			return (List<PrcMainVendorQuoteItem>) getListByDetachedCiteria(crit);
		}else{
			List<PrcMainVendorQuoteItem> retMe = new ArrayList<PrcMainVendorQuoteItem>();
			if(listItem!=null && listItem.size()>0){
				for(PrcMainItem it: listItem){
					PrcMainVendorQuoteItem item = new PrcMainVendorQuoteItem();
					item.setItemDescription(it.getItemDescription());
					item.setItemPajak(it.getItemTaxOe());
					item.setItemPrice(new BigDecimal(0));
					item.setItemPriceTotal(new BigDecimal(0));
					item.setItemQuantity(it.getItemQuantityOe());
					item.setItemPriceTotalPpn(new BigDecimal(0));
					item.setPrcMainItem(it);
					retMe.add(item);
				}
			}
			  Collections.sort(retMe, new Comparator<PrcMainVendorQuoteItem>(){
		             public int compare(PrcMainVendorQuoteItem s1, PrcMainVendorQuoteItem s2) {
		               return s1.getPrcMainItem().getId().compareTo(s2.getPrcMainItem().getId());
		            }
		        });
			return retMe;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainVendor> listMitraKerjaEvaluasi(
			PrcMainHeader prcMainHeader) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMainVendor.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("vndHeader", FetchMode.JOIN);
		crit.add(Restrictions.eq("prcMainHeader", prcMainHeader));
		crit.add(Restrictions.sqlRestriction(" (pmp_status=5 or pmp_status=6 or pmp_status=7 or pmp_status=-6 or pmp_status=-7) "));
		return (List<PrcMainVendor>) getListByDetachedCiteria(crit);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainVendor> listMitraKerjaEvaluasiLulusTeknisHarga(
			PrcMainHeader prcMainHeader) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMainVendor.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("vndHeader", FetchMode.JOIN);
		crit.add(Restrictions.eq("prcMainHeader", prcMainHeader));
		crit.add(Restrictions.sqlRestriction(" (pmp_status=6 or pmp_status=7)"));
		return (List<PrcMainVendor>) getListByDetachedCiteria(crit);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainVendor> listMitraKerjaEvaluasiLulusTeknis(
			PrcMainHeader prcMainHeader) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMainVendor.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("vndHeader", FetchMode.JOIN);
		crit.add(Restrictions.eq("prcMainHeader", prcMainHeader));
		crit.add(Restrictions.sqlRestriction(" (pmp_status=6)"));
		return (List<PrcMainVendor>) getListByDetachedCiteria(crit);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainVendor> listMitraKerjaEvaluasiLulusHarga(
			PrcMainHeader prcMainHeader) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMainVendor.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("vndHeader", FetchMode.JOIN);
		crit.add(Restrictions.eq("prcMainHeader", prcMainHeader));
		crit.add(Restrictions.sqlRestriction(" (pmp_status=7)"));
		return (List<PrcMainVendor>) getListByDetachedCiteria(crit);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainVendor> listMitraKerjaEvaluasiTidakLulusTeknis(
			PrcMainHeader prcMainHeader) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMainVendor.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("vndHeader", FetchMode.JOIN);
		crit.add(Restrictions.eq("prcMainHeader", prcMainHeader));
		crit.add(Restrictions.sqlRestriction(" (pmp_status=-6)"));
		return (List<PrcMainVendor>) getListByDetachedCiteria(crit);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainVendor> listMitraKerjaEvaluasiTidakLulusHarga(
			PrcMainHeader prcMainHeader) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMainVendor.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("vndHeader", FetchMode.JOIN);
		crit.add(Restrictions.eq("prcMainHeader", prcMainHeader));
		crit.add(Restrictions.sqlRestriction(" (pmp_status=-7)"));
		return (List<PrcMainVendor>) getListByDetachedCiteria(crit);
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainVendor> listMitraKerjaEvaluasiTidakLulusTeknisHarga(
			PrcMainHeader prcMainHeader) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMainVendor.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("vndHeader", FetchMode.JOIN);
		crit.add(Restrictions.eq("prcMainHeader", prcMainHeader));
		crit.add(Restrictions.sqlRestriction(" (pmp_status=-6 or pmp_status=-7)"));
		return (List<PrcMainVendor>) getListByDetachedCiteria(crit);
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainVendorQuoteItem> listQuoteItemEvaluasi(
			List<PrcMainVendor> listVendor) throws Exception {
		List<PrcMainVendorQuote> retMe = new ArrayList<PrcMainVendorQuote>();
		if(SessionGetter.isNotNull(listVendor)){
			for(PrcMainVendor a: listVendor){
				PrcMainVendorQuote q = new PrcMainVendorQuote();
				q.setId(new PrcMainVendorQuoteId(a.getId().getPpmId(), a.getId().getVendorId()));
				retMe.add(q);
			}
		}
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMainVendorQuoteItem.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		DetachedCriteria sb = crit.createCriteria("prcMainItem", JoinType.INNER_JOIN);
		crit.add(Restrictions.in("prcMainVendorQuote", retMe));
		return (List<PrcMainVendorQuoteItem>) getListByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainVendor> listMitraKerjaNegosiasi(
			PrcMainHeader prcMainHeader) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMainVendor.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("vndHeader", FetchMode.JOIN);
		crit.add(Restrictions.eq("prcMainHeader", prcMainHeader));
		crit.add(Restrictions.sqlRestriction(" pmp_technical_status=1 and pmp_price_status=1"));//yang lulus tknis dan harga
		return (List<PrcMainVendor>) getListByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> listPrcMainHeaderOEPemilihanEstimator(
			Object object) throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		dc.add(Restrictions.eq("admProses", SessionGetter.OE_PROSES_ID));
		dc.add(Restrictions.eq("admUserByPpmCurrentUser", SessionGetter.getUser()));
		//pembuatan OE
		dc.add(Restrictions.eq("ppmProsesLevel", 1));
		return (List<PrcMainHeader>) getListByDetachedCiteria(dc);
	}

	@Override
	public void saveDisposisiPraPP(PrcMainHeader ppm,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception {
		try {
			startTransaction();
			PrcMainHeader prcMainHeader = getPrcMainIHeaderById(ppm.getPpmId());
			if(ppm.getBrt()==1){
				//via BRT not implemented yet
			}
			prcMainHeader.setBrt(ppm.getBrt());
			prcMainHeader.setAdmUserByPpmCurrentUser(ppm.getPengawasPp());
			prcMainHeader.setPengawasPp(ppm.getPengawasPp());
			AdmProsesHirarki ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel()+1, prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahOe().doubleValue());
			if(ahp==null){
				throw new Exception("Kesalahan dalam hirarki");
			}else{
				if(ahp.getAdmRoleByCurrentPos()!=null){
					AdmUser user = new AdmServiceImpl().getUserById(prcMainHeader.getAdmUserByPpmCurrentUser());
					prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
					prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
					prcMainHeader.setAdmUserByPpmCurrentUser(user);
				}
			}
			//save header
			
			//jika tidak sama
			if(prcMainHeader.getPengawasPp().getId().longValue()!=prcMainHeader.getAdmUserByPpmCurrentUser().getId().intValue()){
				throw new Exception("Pemilihan Kabag/ Kasie tidak berhasil");
			}
			
			saveEntity(prcMainHeader);
			
			//upload doc
			if(SessionGetter.isNotNull(docsKomentar)){
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}
			
			//insert history
			SessionGetter.saveHistoryGlobalPrOE(histMainComment, prcMainHeader, session);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public CtrContractComment getLastCommentByContractHeader(
			CtrContractHeader mainHeader) {
		DetachedCriteria crit = DetachedCriteria.forClass(CtrContractComment.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("contractId", new BigDecimal(mainHeader.getContractId())));
		crit.addOrder(Order.desc("id"));
		List<CtrContractComment> retMe = (List<CtrContractComment>)getListByDetachedCiteria(crit);
		if(retMe!=null && retMe.size()>0){
			return retMe.get(0);
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TodoUserCommand> getListTodoUser(String todoListUserSpphPr)
			throws Exception {
		List<TodoUserCommand> retMe = new ArrayList<TodoUserCommand>();
		try {
			startTransaction();
			Query q =  session.createSQLQuery(todoListUserSpphPr);
			List<Object[]> obj = q.list();
			if(obj!=null && obj.size()>0){
				Iterator<Object[]> it = obj.iterator();
				while(it.hasNext()){
					Object o[] = it.next();
					it.remove();
					TodoUserCommand cmd = new TodoUserCommand();
					cmd.setPpmId(Integer.valueOf(String.valueOf(o[0])));
					cmd.setNomorPr(String.valueOf(o[1]));
					cmd.setNomorSpph(o[2]!=null?String.valueOf(o[2]):"");
					cmd.setCurrentUser(o[3]!=null?Integer.valueOf(String.valueOf(o[3])):0);
					cmd.setCompleteName(o[4]!=null?String.valueOf(o[4]):"");
					cmd.setUpdatedDate(o[5]!=null?(Date)o[5]:null);
					cmd.setKeterangan(String.valueOf(o[6]));
					cmd.setUrl(String.valueOf(o[7]));
					cmd.setJudulSpph(String.valueOf(o[8]));
					Integer prioritas = Integer.valueOf(String.valueOf(o[9]));
					String prio=null;
					if(prioritas==1){
						prio = "High";
					}else if(prioritas==2){
						prio = "Normal";
					}else{
						prio = "Low";
					}
					cmd.setKeterangan(cmd.getKeterangan()+" <br/> ( "+prio+" )");
					retMe.add(cmd);
				}
			}
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
		return retMe;
	}

	@Override
	public int countPrcMainHeaderPRSPPHReport(Object object) throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setProjection(Projections.rowCount());
		if(object!=null){
			if(object instanceof AdmDistrict){
				AdmDistrict d = (AdmDistrict)object;
				dc.add(Restrictions.eq("districtUser", d));
			}
		}
		
		Object obj = getEntityByDetachedCiteria(dc);
		return Integer.valueOf(String.valueOf(obj)).intValue();
	}

	@Override
	public int countPrcMainHeaderPRSPPHReportByYear(Object o, Integer year) throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setProjection(Projections.rowCount());
		if(o!=null){
			if(o instanceof AdmDistrict){
				AdmDistrict d = (AdmDistrict)o;
				dc.add(Restrictions.eq("districtUser", d));
			}
		}
		dc.setFetchMode("admDept", FetchMode.JOIN);
		dc.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);

		
		if(year!=0) {
			dc.add(Restrictions.sqlRestriction("to_char({alias}."
					+ "CREATED_DATE, 'yyyy') = ?", year, StandardBasicTypes.INTEGER));
		}
		
		Object obj = getEntityByDetachedCiteria(dc);
		return Integer.valueOf(String.valueOf(obj)).intValue();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> listPrcMainHeaderPRSPPHReport(Object object,
			int page, int i) throws Exception {
	
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		if(object!=null){
			if(object instanceof AdmDistrict){
				AdmDistrict d = (AdmDistrict)object;
				dc.add(Restrictions.eq("districtUser", d));
			}
		}
		
		dc.addOrder(Order.desc("updatedDate"));
		return ((List<PrcMainHeader>) getListByDetachedCiteriaPaging(dc, page, i));
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> listPrcMainHeaderPRSPPHReportByYear(Object object,
			int page, int i, Integer year) throws Exception {
	
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		if(object!=null){
			if(object instanceof AdmDistrict){
				AdmDistrict d = (AdmDistrict)object;
				dc.add(Restrictions.eq("districtUser", d));
			}
		}
		dc.add(Restrictions.sqlRestriction("to_char({alias}."
				+ "CREATED_DATE, 'yyyy') = ?", year, StandardBasicTypes.INTEGER));
		dc.addOrder(Order.desc("updatedDate"));
		return ((List<PrcMainHeader>) getListByDetachedCiteriaPaging(dc, page, i));
	}
	
	@Override
	public int countPrcMainHeaderPRSPPHReportRfq(Object object) throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setProjection(Projections.rowCount());
		if(object!=null){
			if(object instanceof AdmDistrict){
				AdmDistrict d = (AdmDistrict)object;
				dc.add(Restrictions.eq("districtUser", d));
			}
		}
		dc.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
		Object obj = getEntityByDetachedCiteria(dc);
		return Integer.valueOf(String.valueOf(obj)).intValue();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> listPrcMainHeaderPRSPPHReportRfq(Object object,
			int page, int i) throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		if(object!=null){
			if(object instanceof AdmDistrict){
				AdmDistrict d = (AdmDistrict)object;
				dc.add(Restrictions.eq("districtUser", d));
			}
		}
		dc.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
		dc.add(Restrictions.eq("isWatched", 1));
		dc.addOrder(Order.desc("updatedDate"));
		return ((List<PrcMainHeader>) getListByDetachedCiteriaPaging(dc, page, i));
	}

	@Override
	public void revisiApprovalPanitiaPp(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception {
		try {
			startTransaction();
			prcMainHeader  = getPrcMainIHeaderById(prcMainHeader.getPpmId());
			prcMainHeader.setPpmProsesLevel(4);
			prcMainHeader.setAdmUserByPpmCurrentUser(prcMainHeader.getAdmUserByPpmAdmBuyer());
			prcMainHeader.setAdmRole(new AdmServiceImpl().getAdmUser(prcMainHeader.getAdmUserByPpmAdmBuyer()).getAdmEmployee().getAdmRoleByEmpRole1());
			prcMainHeader.setAdmProses(SessionGetter.PP_PROSES_ID);
			saveEntity(prcMainHeader);
			
			//update panitia detail jadi null lagi
			if(prcMainHeader.getPanitia()!=null){
				List<PanitiaPengadaanDetail> lpp = new PrcServiceImpl().getListPanitiaPengadaanDetail(prcMainHeader.getPanitia());
				if(lpp!=null && lpp.size()>0){
					for(PanitiaPengadaanDetail p:lpp){
						p.setStatus(null);
						saveEntity(p);
					}
				}
			}
			
			//upload doc
			if(SessionGetter.isNotNull(docsKomentar)){
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}
			
			//insert History
			SessionGetter.saveHistoryGlobalPanitia(histMainComment, prcMainHeader, session);
			
			//remove preparation
			PrcMainPreparation pre = getPrcMainPreparationByHeader(prcMainHeader);
			if(pre!=null)
				deleteEntity(pre);
			
			//remove doc
			List<PrcMainDoc> lst = listPrcMainDocByHeader(prcMainHeader);
			if(lst!=null){
				for(PrcMainDoc d:lst){
					deleteEntity(d);
				}
			}
			
			//remove vendor
			List<PrcMainVendor> lstv = listMitraKerjaByHeader(prcMainHeader);
			if(lstv!=null){
				for(PrcMainVendor v:lstv){
					deleteEntity(v);
				}
			}
			
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}
	
	@Override
	public void setujuApprovalPanitiaPP(PrcMainHeader prcMainHeader, HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception {
		try {
			startTransaction();
			prcMainHeader = getPrcMainIHeaderById(prcMainHeader.getPpmId());
			// cek panitia
			List<PanitiaPengadaanDetail> listPPPD = getListPanitiaPengadaanDetailByPanitiaStatus(
					prcMainHeader.getPanitia(), 1);
			if (listPPPD != null && listPPPD.size() > 0) {
				if (listPPPD.size() == 1) {
					for (PanitiaPengadaanDetail pd : listPPPD) {
						if (pd.getAnggota().getId().longValue() == SessionGetter.getUser().getId().longValue()) {
							pd.setStatus(1);
							saveEntity(pd);
						}
					}

					// go to finalisasi spph
					AdmProsesHirarki ap = getProsesMaxLevelProcess(SessionGetter.PP_PROSES_ID,
							prcMainHeader.getAdmDistrict());
					prcMainHeader.setFinalisasiSpph(1);
					prcMainHeader.setAdmUserByPpmCurrentUser(prcMainHeader.getAdmUserByPpmBuyer());
					if (ap != null) {
						prcMainHeader.setAdmProses(SessionGetter.PP_PROSES_ID);
						prcMainHeader.setPpmProsesLevel(ap.getTingkat());
						prcMainHeader.setAdmRole(ap.getAdmRoleByCurrentPos());
						histMainComment.setCreatedBy("Finalisasi SPPH");
						prcMainHeader.setFinalisasiSpph(1);
					}

				} else {
					List<Integer> lstTmp = new ArrayList();
					for (PanitiaPengadaanDetail pd : listPPPD) {
						if (pd.getAnggota().getId().longValue() == SessionGetter.getUser().getId().longValue()) {
							pd.setStatus(1);
							saveEntity(pd);
							lstTmp.add(Integer.valueOf(1));
						} else {
							lstTmp.add(Integer.valueOf(0));
						}
					}
					int countPanitia = 0;
					if ((lstTmp != null) && (lstTmp.size() > 0)) {
						for (Integer a : lstTmp) {
							if (a.intValue() == 0) {
								countPanitia++;
							}
						}
					}
					if (countPanitia == 0) {
						AdmProsesHirarki ap = getProsesMaxLevelProcess(SessionGetter.PP_PROSES_ID,
								prcMainHeader.getAdmDistrict());
						prcMainHeader.setFinalisasiSpph(Integer.valueOf(1));
						prcMainHeader.setAdmUserByPpmCurrentUser(prcMainHeader.getAdmUserByPpmBuyer());
						if (ap != null) {
							prcMainHeader.setAdmProses(SessionGetter.PP_PROSES_ID);
							prcMainHeader.setPpmProsesLevel(ap.getTingkat());
							prcMainHeader.setAdmRole(ap.getAdmRoleByCurrentPos());
							histMainComment.setCreatedBy("Finalisasi SPPH");
							prcMainHeader.setFinalisasiSpph(Integer.valueOf(1));
						}
					}

				}
			}

			saveEntity(prcMainHeader);

			// upload doc
			if (SessionGetter.isNotNull(docsKomentar)) {
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}

			// insert History
			SessionGetter.saveHistoryGlobalPanitia(histMainComment, prcMainHeader, session);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}

	}

//	@Override
//	public void setujuApprovalPanitiaPP(PrcMainHeader prcMainHeader,
//			HistMainComment histMainComment, File docsKomentar,
//			String docsKomentarFileName) throws Exception {
//		try {
//			startTransaction();
//			prcMainHeader  = getPrcMainIHeaderById(prcMainHeader.getPpmId());
//			//cek panitia
//			List<PanitiaPengadaanDetail> listPPPD  = getListPanitiaPengadaanDetailByPanitiaStatus(prcMainHeader.getPanitia(),1);
//			if(listPPPD!=null && listPPPD.size()>0){
//				if(listPPPD.size()==1){
//					for(PanitiaPengadaanDetail pd:listPPPD){
//						if(pd.getAnggota().getId().longValue() == SessionGetter.getUser().getId().longValue()){
//							pd.setStatus(1);
//							saveEntity(pd);
//						}
//					}
//					
//					prcMainHeader.setAdmProses(SessionGetter.PP_PROSES_ID);
//					prcMainHeader.setPpmProsesLevel(8);
//					AdmProsesHirarki ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel()+1, 
//							prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahOe().doubleValue());
//					System.out.println(">>>"+ahp.getAdmRoleByCurrentPos().getRoleName());
//					System.out.println(">>>"+ahp.getAdmRoleByCurrentPos().getId());
//
//					if(ahp.getAdmRoleByCurrentPos().getRoleName().trim().equalsIgnoreCase("pembeli")){
//						AdmProsesHirarki ap = getProsesMaxLevelProcess(SessionGetter.PP_PROSES_ID,prcMainHeader.getAdmDistrict());
//						prcMainHeader.setFinalisasiSpph(1);
//						prcMainHeader.setAdmUserByPpmCurrentUser(prcMainHeader.getAdmUserByPpmBuyer());
//						if(ap!=null){
////							//go to finalisasi spph
//							prcMainHeader.setAdmProses(SessionGetter.PP_PROSES_ID);
//							prcMainHeader.setPpmProsesLevel(ap.getTingkat());
//							prcMainHeader.setAdmRole(ap.getAdmRoleByCurrentPos());
//							histMainComment.setCreatedBy("Finalisasi SPPH");
//							prcMainHeader.setFinalisasiSpph(1);
//						}
//					}else if (StringUtils.equalsIgnoreCase(ahp.getAdmRoleByCurrentPos().getRoleName(), "DIR TERKAIT")) {
//						if (prcMainHeader.getSatkerPeminta() == null) {
//							throw new Exception("Satker Peminta Kosong, silahkan Update via Update Tools");
//						} else {
//							AdmDept satkerPeminta = new AdmServiceImpl()
//									.getAdmDeptById(prcMainHeader.getSatkerPeminta());
//							prcMainHeader.setAdmRole(satkerPeminta.getDirektur());
//							prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
//							prcMainHeader.setAdmUserByPpmCurrentUser(SessionGetter
//									.getEmployeeByRole(satkerPeminta.getDirektur()).getAdmUser());
//						}
//					}else{
//						prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
//						prcMainHeader.setAdmUserByPpmCurrentUser(SessionGetter.getEmployeeByRole(ahp.getAdmRoleByCurrentPos()).getAdmUser());
//						prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());	
//					}
//					
//					
//				}else{
//					for(PanitiaPengadaanDetail pd:listPPPD){
//						if(pd.getAnggota().getId().longValue() == SessionGetter.getUser().getId().longValue()){
//							pd.setStatus(1);
//							saveEntity(pd);
// 							break;
//						}
//					}
//					
//				}
//			}
//			
//			saveEntity(prcMainHeader);
//			
//			//upload doc
//			if(SessionGetter.isNotNull(docsKomentar)){
//				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
//			}
//			
//			//insert History
//			SessionGetter.saveHistoryGlobalPanitia(histMainComment, prcMainHeader, session);
//			commitAndClear();
//		} catch (Exception e) {
//			RollbackAndClear();
//			throw e;
//		}
//		
//	}

	@SuppressWarnings("unchecked")
	public AdmProsesHirarki getProsesMaxLevelProcess(AdmProses ppProsesId,AdmDistrict district) {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmProsesHirarki.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("admDistrict", district));
		crit.add(Restrictions.eq("admProses", ppProsesId));
		crit.addOrder(Order.desc("tingkat"));
		List<AdmProsesHirarki> retMe = (List<AdmProsesHirarki>) getListByDetachedCiteria(crit);
		if(retMe!=null && retMe.size()>0){
			return retMe.get(0);
		}else{
			return null;
		}
	}

	/**
	 * yang belum setuju
	 * @param panitia
	 * @param i
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private List<PanitiaPengadaanDetail> getListPanitiaPengadaanDetailByPanitiaStatus(
			PanitiaPengadaan panitia, int i) {
		DetachedCriteria crit = DetachedCriteria.forClass(PanitiaPengadaanDetail.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("panitia", panitia));
		Disjunction dj = Restrictions.disjunction();
		dj.add(Restrictions.isNull("status"));
		dj.add(Restrictions.eq("status", 0));
		crit.add(dj);
		return (List<PanitiaPengadaanDetail>) getListByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> listPrcMainHeaderPpPanitiaApproval(Object object)
			throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		dc.add(Restrictions.eq("admProses", SessionGetter.PANITIA_PP_PROSES_ID));
		dc.add(Restrictions.eq("ppmProsesLevel", 1));
		
		DetachedCriteria sb = dc.createCriteria("panitia", JoinType.INNER_JOIN);
		DetachedCriteria sc = sb.createCriteria("panitiaPengadaanDetails", JoinType.INNER_JOIN);
		sc.add(Restrictions.eq("anggota", SessionGetter.getUser()));
		sc.add(Restrictions.isNull("status"));
		return (List<PrcMainHeader>) getListByDetachedCiteria(dc);
	}

	@Override
	public void savePengumumanLelang(PrcMainHeader mainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception {
		try {
			startTransaction();
			PrcMainHeader prcMainHeader  = getPrcMainIHeaderById(mainHeader.getPpmId());
			AdmUser currentUser = new AdmServiceImpl().getUserById(mainHeader.getAdminPq());
			
			prcMainHeader.setAdmUserByPpmCurrentUser(currentUser);
			prcMainHeader.setAdmRole(currentUser.getAdmEmployee().getAdmRoleByEmpRole1());
			prcMainHeader.setPpmProsesLevel(2);
			prcMainHeader.setPengumumanLelang(mainHeader.getPengumumanLelang());
			prcMainHeader.setAdminPq(currentUser);
			saveEntity(prcMainHeader);
			
			
			//upload doc
			if(SessionGetter.isNotNull(docsKomentar)){
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}
			
			//insert History
			SessionGetter.saveHistoryGlobalPrOE(histMainComment, prcMainHeader, session);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> listPrcMainHeaderPpPengumumanLelang(Object object)
			throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		dc.add(Restrictions.eq("admUserByPpmCurrentUser", SessionGetter.getUser()));
		dc.add(Restrictions.eq("admProses", SessionGetter.PQ_PROSES_ID));
		dc.add(Restrictions.eq("ppmProsesLevel", 1));
		return (List<PrcMainHeader>) getListByDetachedCiteria(dc);
	}

	@Override
	public void savePrakualifikasi(PrcMainHeader mainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName, List<PrcMainVendor> listPrcMainVendor)
			throws Exception {
		try {
			startTransaction();
			PrcMainHeader prcMainHeader  = getPrcMainIHeaderById(mainHeader.getPpmId());
			AdmProsesHirarki ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel()+1, prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahOe().doubleValue());
			if(ahp!=null){
				if(ahp.getAdmRoleByCurrentPos()!=null){
					prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
					prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
					prcMainHeader.setAdmUserByPpmCurrentUser(new AdmServiceImpl().getUserById(prcMainHeader.getAdmUserByPpmBuyer()));
				}
			}
			saveEntity(prcMainHeader);
			
			//update prcMainVendor
			if(listPrcMainVendor!=null){
				for(PrcMainVendor pv : listPrcMainVendor){
					System.out.println(">>> "+pv.getId());
					System.out.println(">>> "+pv.getPmpPraqReason());
					System.out.println(">>> "+pv.getPmpPraqStatus());
					PrcMainVendor v = getPrcMainVendorById(pv.getId());
					v.setPmpPraqReason(pv.getPmpPraqReason());
					v.setPmpPraqStatus(pv.getPmpPraqStatus());
					saveEntity(v);
				}
			}
			//upload doc
			if(SessionGetter.isNotNull(docsKomentar)){
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}
			
			//insert History
			SessionGetter.saveHistoryGlobalPrOE(histMainComment, prcMainHeader, session);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public PrcMainVendor getPrcMainVendorById(PrcMainVendorId id) {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMainVendor.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.idEq(id));
		return (PrcMainVendor) getEntityByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> listPrcMainHeaderPpPq(Object object)
			throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		dc.add(Restrictions.eq("admUserByPpmCurrentUser", SessionGetter.getUser()));
		dc.add(Restrictions.eq("admProses", SessionGetter.PQ_PROSES_ID));
		dc.add(Restrictions.gt("ppmProsesLevel", 1));
		return (List<PrcMainHeader>) getListByDetachedCiteria(dc);
	}

//	@Override
//	public void finalisasiPq(PrcMainHeader mainHeader,
//			HistMainComment histMainComment, File docsKomentar,
//			String docsKomentarFileName, List<PrcMainVendor> listPrcMainVendor)
//			throws Exception {
//		try {
//			startTransaction();
//			PrcMainHeader prcMainHeader  = getPrcMainIHeaderById(mainHeader.getPpmId());
//			AdmProsesHirarki ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel(), prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahOe().doubleValue());
//			
//				prcMainHeader.setAdmProses(SessionGetter.PP_PROSES_ID);
//				prcMainHeader.setPpmProsesLevel(5);
//				ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel(), prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahOe().doubleValue());
//				if(ahp!=null){
//					if(ahp.getAdmRoleByCurrentPos()!=null){
//						if(ahp.getAdmRoleByCurrentPos().getRoleName().trim().equalsIgnoreCase("pengawas pp")){
//							prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
//							prcMainHeader.setAdmUserByPpmCurrentUser(new AdmServiceImpl().getUserById(prcMainHeader.getPengawasPp()));
//							histMainComment.setCreatedBy(SessionGetter.getPropertiesValue("pp.5", null));
//						}else{
//							prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
//							prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
//							prcMainHeader.setAdmUserByPpmCurrentUser(new AdmServiceImpl().getUserById(prcMainHeader.getAdmUserByPpmBuyer()));
//						}
//					}		
//				}
//			
//			saveEntity(prcMainHeader);
//			
//			//update prcMainVendor
//			if(listPrcMainVendor!=null){
//				for(PrcMainVendor pv : listPrcMainVendor){
//					PrcMainVendor v = getPrcMainVendorById(pv.getId());
//					v.setPmpPraqReason(pv.getPmpPraqReason());
//					v.setPmpPraqStatus(pv.getPmpPraqStatus());
//					saveEntity(v);
//				}
//			}
//			//upload doc
//			if(SessionGetter.isNotNull(docsKomentar)){
//				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
//			}
//			
//			//insert History
//			SessionGetter.saveHistoryGlobalPrOE(histMainComment, prcMainHeader, session);
//			commitAndClear();
//		} catch (Exception e) {
//			RollbackAndClear();
//			throw e;
//		}
//	}
	
	@Override
	public void finalisasiPq(PrcMainHeader mainHeader, HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName, List<PrcMainVendor> listPrcMainVendor) throws Exception {
		try {
			startTransaction();
			PrcMainHeader prcMainHeader = getPrcMainIHeaderById(mainHeader.getPpmId());
			AdmProsesHirarki ahp = new AdmProsesHirarki();
			ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(),
					prcMainHeader.getPpmProsesLevel(), prcMainHeader.getAdmDistrict(),
					prcMainHeader.getPpmJumlahOe().doubleValue());

			if (ahp != null) {
				if (ahp.getAdmRoleByCurrentPos() != null) {
					prcMainHeader.setAdmProses(SessionGetter.PP_PROSES_ID);
					prcMainHeader.setPpmProsesLevel(9);//go to finalisasi spph
					prcMainHeader.setAdmUserByPpmCurrentUser(prcMainHeader.getAdmUserByPpmBuyer());
					prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
					histMainComment.setCreatedBy("Finalisasi SPPH");
					prcMainHeader.setFinalisasiSpph(1);
					/*if (ahp.getAdmRoleByCurrentPos().getRoleName().trim().equalsIgnoreCase("pembeli")) {
//						if (ahp.getAdmRoleByCurrentPos().getRoleName().trim().equalsIgnoreCase("pengawas pp")) {
//						prcMainHeader.setAdmProses(SessionGetter.PP_PROSES_ID);
//						prcMainHeader.setPpmProsesLevel(5);
//						ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(),
//								prcMainHeader.getPpmProsesLevel(), prcMainHeader.getAdmDistrict(),
//								prcMainHeader.getPpmJumlahOe().doubleValue());
//						prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
//						prcMainHeader.setAdmUserByPpmCurrentUser(
//								new AdmServiceImpl().getUserById(prcMainHeader.getPengawasPp()));
//						histMainComment.setCreatedBy(SessionGetter.getPropertiesValue("pp.5", null));
						prcMainHeader.setAdmProses(SessionGetter.PP_PROSES_ID);
						prcMainHeader.setPpmProsesLevel(9);//go to finalisasi spph
						prcMainHeader.setAdmUserByPpmCurrentUser(prcMainHeader.getAdmUserByPpmBuyer());
						prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
						histMainComment.setCreatedBy("Finalisasi SPPH");
						prcMainHeader.setFinalisasiSpph(1);
					}else {
						throw new Exception("Kesalahan dalam proses hirarki");
					}*/
					/*else{
						prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
						prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
						prcMainHeader.setAdmUserByPpmCurrentUser(
								new AdmServiceImpl().getUserById(prcMainHeader.getAdmUserByPpmBuyer()));
					}*/
 						
				}else{
					throw new Exception("Kesalahan dalam proses hirarki");
				}
			}
			/** masukan ke panitia **/
			/*if (prcMainHeader.getPanitia() != null) {
				prcMainHeader.setAdmProses(SessionGetter.PANITIA_PP_PROSES_ID);
				prcMainHeader.setPpmProsesLevel(1);
				AdmProsesHirarki ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(),
						prcMainHeader.getPpmProsesLevel(), prcMainHeader.getAdmDistrict(),
						prcMainHeader.getPpmJumlahOe().doubleValue());
				ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(),
						prcMainHeader.getPpmProsesLevel(), prcMainHeader.getAdmDistrict(),
						prcMainHeader.getPpmJumlahOe().doubleValue());
				prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
				prcMainHeader.setAdmUserByPpmCurrentUser(
						SessionGetter.getEmployeeByRole(ahp.getAdmRoleByCurrentPos()).getAdmUser());
				histMainComment.setCreatedBy(SessionGetter.getPropertiesValue("pp.7", null));
			} else {
				prcMainHeader.setAdmProses(SessionGetter.PP_PROSES_ID);
				prcMainHeader.setPpmProsesLevel(5);
				AdmProsesHirarki ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(),
						prcMainHeader.getPpmProsesLevel(), prcMainHeader.getAdmDistrict(),
						prcMainHeader.getPpmJumlahOe().doubleValue());
				if (ahp != null) {
					if (ahp.getAdmRoleByCurrentPos() != null) {
						if (ahp.getAdmRoleByCurrentPos().getRoleName().trim().equalsIgnoreCase("pengawas pp")) {
							prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
							prcMainHeader.setAdmUserByPpmCurrentUser(
									new AdmServiceImpl().getUserById(prcMainHeader.getPengawasPp()));
							histMainComment.setCreatedBy(SessionGetter.getPropertiesValue("pp.5", null));
						} else {
							prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
							prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
							prcMainHeader.setAdmUserByPpmCurrentUser(
									new AdmServiceImpl().getUserById(prcMainHeader.getAdmUserByPpmBuyer()));
						}
					}
				}
			}*/
			saveEntity(prcMainHeader);

			// update prcMainVendor
			if (listPrcMainVendor != null) {
				for (PrcMainVendor pv : listPrcMainVendor) {
					PrcMainVendor v = getPrcMainVendorById(pv.getId());
					v.setPmpPraqReason(pv.getPmpPraqReason());
					v.setPmpPraqStatus(pv.getPmpPraqStatus());
					saveEntity(v);
				}
			}
			// upload doc
			if (SessionGetter.isNotNull(docsKomentar)) {
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}

			// insert History
			SessionGetter.saveHistoryGlobalPrOE(histMainComment, prcMainHeader, session);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public void revisiPq(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception {
		try {
			startTransaction();
			prcMainHeader  = getPrcMainIHeaderById(prcMainHeader.getPpmId());
			AdmProsesHirarki ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(), 2, prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahOe().doubleValue());
			prcMainHeader.setPpmProsesLevel(2);
			prcMainHeader.setAdmUserByPpmCurrentUser(prcMainHeader.getAdminPq());
			prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
			prcMainHeader.setAdmProses(SessionGetter.PQ_PROSES_ID);
			prcMainHeader.setPanitia(null);
			saveEntity(prcMainHeader);
			
			//upload doc
			if(SessionGetter.isNotNull(docsKomentar)){
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}
			
			//insert History
			SessionGetter.saveHistoryGlobalPrOE(histMainComment, prcMainHeader, session);
			
//			//remove preparation
//			PrcMainPreparation pre = getPrcMainPreparationByHeader(prcMainHeader);
//			deleteEntity(pre);
			
			//remove doc
//			List<PrcMainDoc> lst = listPrcMainDocByHeader(prcMainHeader);
//			if(lst!=null){
//				for(PrcMainDoc d:lst){
//					deleteEntity(d);
//				}
//			}
//			
//			//remove vendor
//			List<PrcMainVendor> lstv = listMitraKerjaByHeader(prcMainHeader);
//			if(lstv!=null){
//				for(PrcMainVendor v:lstv){
//					deleteEntity(v);
//				}
//			}
			
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public void saveChat(PrcMsgChat chat) throws Exception {
		try {
			startTransaction();
			saveEntity(chat);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMsgChat> listChatByHeader(PrcMainHeader prcMainHeader)
			throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMsgChat.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("prcMainHeader", prcMainHeader));
		crit.addOrder(Order.desc("createdDate"));
		return (List<PrcMsgChat>) getListByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	public List<PrcMainVendor> listMitraKerjaLulusTeknis(
			PrcMainHeader prcMainHeader) {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMainVendor.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("vndHeader", FetchMode.JOIN);
		crit.add(Restrictions.eq("prcMainHeader", prcMainHeader));
		crit.add(Restrictions.sqlRestriction(" (pmp_status=6 or pmp_status=7) "));//teknis or harga
		return (List<PrcMainVendor>) getListByDetachedCiteria(crit);
	}
	

	@SuppressWarnings("unchecked")
	public List<EauctionVendor> listVndEauctionVendor(
			EauctionHeader eauctionHeader) {
		DetachedCriteria crit = DetachedCriteria.forClass(EauctionVendor.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("vndHeader", FetchMode.JOIN);
		crit.add(Restrictions.eq("eauctionHeader", eauctionHeader));
		return (List<EauctionVendor>) getListByDetachedCiteria(crit);
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> listPrcMainHeaderPpAnggaran(Object object)
			throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		dc.add(Restrictions.eq("admUserByPpmCurrentUser", SessionGetter.getUser()));
		dc.add(Restrictions.eq("admProses", SessionGetter.ANGGARAN_PROSES_ID));
		return (List<PrcMainHeader>) getListByDetachedCiteria(dc);
	}

	@Override
	public void revisiApprovalAnggaran(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception {
		try {
			startTransaction();
			prcMainHeader = getPrcMainIHeaderById(prcMainHeader.getPpmId());
			AdmDistrict kantor =null;
			if(prcMainHeader.getDistrictKeuangan()!=null){
				kantor = prcMainHeader.getDistrictKeuangan();
			}else{
				kantor = prcMainHeader.getAdmDistrict();
			}
			
			AdmUser us = getMappingUserKeuangan(kantor);
			AdmProsesHirarki ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(), 1, kantor, prcMainHeader.getPpmJumlahOe().doubleValue());
			if(ahp.getAdmRoleByCurrentPos()!=null){
				prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
				if(us!=null){
					prcMainHeader.setAdmUserByPpmCurrentUser(new AdmServiceImpl().getAdmUser(us));
				}else{
					throw new Exception("User Keuangan Belum di mapping");
				}
				prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
			}
			
			saveEntity(prcMainHeader);
			
			//upload dokumen
			if(SessionGetter.isNotNull(docsKomentar)){
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}
			
			//insert history
			SessionGetter.saveHistoryGlobalPrOE(histMainComment, prcMainHeader, session);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	
//			startTransaction();
//			prcMainHeader = getPrcMainIHeaderById(prcMainHeader.getPpmId());
//			AdmDistrict kantor = null;
//			if(prcMainHeader.getDistrictKeuangan()!=null){
//				kantor = prcMainHeader.getDistrictKeuangan();
//			}else{
//				kantor = prcMainHeader.getAdmDistrict();
//			}
//			
//			List<PrcAnggaranJdeDetail> lst = listAnggaranDetailByPPmId(prcMainHeader);
//			AdmProsesHirarki ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel()+1, kantor, prcMainHeader.getPpmJumlahOe().doubleValue());
//			if(ahp==null){//means last go to PP
//					AdmProses proses = SessionGetter.PP_PROSES_ID;//level ke 3
//					prcMainHeader.setAdmProses(proses);
//					prcMainHeader.setPpmProsesLevel(1);
//					ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(), 1, prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahOe().doubleValue());
//					if(ahp==null){
//						throw new Exception("Kesalahan dalam hirarki");
//					}
////					prcMainHeader.setAdmUserByPpmCurrentUser(SessionGetter.getEmployeeByRole(ahp.getAdmRoleByCurrentPos()).getAdmUser());
////					histMainComment.setCreatedBy(SessionGetter.getPropertiesValue("pp.1", null));
////					//insert ke JDE
////					//update status Anggaran
////					PrcAnggaranJdeHeader head = getJdeAnggaranHeaderByPpmId(prcMainHeader);
////					head.setKaapfl("A");
//////					head.setKatorg(SessionGetter.getUser().getUsername().toUpperCase());//ambil dari JDE
//////					head.setKauser(SessionGetter.getUser().getUsername().toUpperCase());
////					head.setKatorg("ALAM");//ambil dari JDE
////					head.setKauser("ALAM");
////					saveEntity(head);
////					
////					List<PrcAnggaranJdeDetail> lst = listAnggaranDetailByPPmId(prcMainHeader);
////					if(lst!=null){
////						for(PrcAnggaranJdeDetail d:lst){
////							d.setRkapfl("A");
//////							d.setRkuser(SessionGetter.getUser().getUsername().toUpperCase());
////							d.setRkuser("ALAM");
////							saveEntity(d);
////						}
////					}
////					
////					PrcAnggaranJdeHeader prcHeader = getJdeAnggaranHeaderByPpmId(prcMainHeader);
////					List<PrcAnggaranJdeDetail> listDetail = listAnggaranDetailByPPmId(prcMainHeader);
////					//get doco
////					Integer sessionID = JdeSessionUtil.loginJde("ALAM","ALAM", SessionGetter.valueProperty("jde.env"), "*ALL");
////					UserSession session = Connector.getInstance().getUserSession(sessionID);
////					if(sessionID!=null){
////						int data = Calendar.getInstance().get(Calendar.YEAR);
////						String fiscal = String.valueOf(data).substring(2);
////						prcHeader.setKadoco(JdeService.getNextNumberAnggaran("00000","KA","20",fiscal,session,sessionID));
////						updateEntity(prcHeader);
////						JdeSessionUtil.loggofJDE(sessionID);
////					}
////					JdeService.insertAnggaran(prcHeader, listDetail);
//					//update status Anggaran
//					//create line ID
//					Double data = 0.0;
//					
//					if(lst!=null){
//						int line = 1000;
//						for(PrcAnggaranJdeDetail d:lst){
//							d.setRklnid(line);
//							System.out.println(">>>LINE" + d.getRklnid());
//							saveEntity(d);
//							line = line+1000;
//							data = data+d.getRkacm1().doubleValue();
//						}
//					}
//					
//					//update PrcAnggaranJde
//					PrcAnggaranJdeHeader ph = getJdeAnggaranHeaderByPpmId(prcMainHeader);
//					ph.setKaacm1(new BigDecimal(data));
//					ph.setKaea(new BigDecimal(data));
//					saveEntity(ph);
//					ph.setKaapfl("A");
////					head.setKatorg(SessionGetter.getUser().getUsername().toUpperCase());//ambil dari JDE
////					head.setKauser(SessionGetter.getUser().getUsername().toUpperCase());
//					ph.setKatorg("ALAM");//ambil dari JDE
//					ph.setKauser("ALAM");
//					saveEntity(ph);
//					
//					
//					if(lst!=null){
//						for(PrcAnggaranJdeDetail d:lst){
//							System.out.println(d.getRklnid());
//							d.setRkapfl("A");
////							d.setRkuser(SessionGetter.getUser().getUsername().toUpperCase());
//							d.setRkuser("ALAM");
//							saveEntity(d);
//						}
//					}
//					
//
//					PrcAnggaranJdeHeader prcHeader = getJdeAnggaranHeaderByPpmId(prcMainHeader);
//					List<PrcAnggaranJdeDetail> listDetail = listAnggaranDetailByPPmId(prcMainHeader);
//					//get doco
//					Integer sessionID = JdeSessionUtil.loginJde("ALAM","ALAM", SessionGetter.valueProperty("jde.env"), "*ALL");
//					UserSession session = Connector.getInstance().getUserSession(sessionID);
//					if(sessionID!=null){
//						int data1 = Calendar.getInstance().get(Calendar.YEAR);
//						String fiscal = String.valueOf(data1).substring(2);
//						prcHeader.setKadoco(JdeService.getNextNumberAnggaran("00000","KA","20",fiscal,session,sessionID));
//						System.out.println("SESSION>>>"+session);
//						System.out.println("SESSION>>>"+sessionID);
//						System.out.println("KADOCO>>>"+prcHeader.getKadoco());
//						System.out.println("KAKCOO>>>"+prcHeader.getKakcoo());
//						System.out.println("KADCTO>>>"+prcHeader.getKadcto());
//						updateEntity(prcHeader);
//						JdeSessionUtil.loggofJDE(sessionID);
//					}
//					JdeService.insertAnggaran(prcHeader, lst);
//					
//					
//			}else{
//				if(ahp.getAdmRoleByCurrentPos()!=null){
//						prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
//						prcMainHeader.setAdmUserByPpmCurrentUser(SessionGetter.getEmployeeByRole(ahp.getAdmRoleByCurrentPos()).getAdmUser());
//						prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
//				}
//			}
//			updateEntity(prcMainHeader);
//			
//			//upload dokumen
//			if(SessionGetter.isNotNull(docsKomentar)){
//				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
//			}
//			
//			Double data = 0.0;
//			
//			if(lst!=null){
//				for(PrcAnggaranJdeDetail d:lst){
//					data = data+d.getRkacm1().doubleValue();
//				}
//			}
//			if(data.doubleValue()!=prcMainHeader.getPpmJumlahOe().doubleValue()){
//				RollbackAndClear();
//				throw new Exception("Jumlah Anggaran Harus Sama dengan Jumlah OE");
//			}
//			//insert history
//			System.out.println("SESSION HISTORY>>>"+session);
//			
//			SessionGetter.saveHistoryGlobalPrOE(histMainComment, prcMainHeader, session);
//			commitAndClear();
	@Override
	public void setujuApprovalAnggaran(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception {
		try {
			startTransaction();
			prcMainHeader = getPrcMainIHeaderById(prcMainHeader.getPpmId());
			//cek kantor
			AdmDistrict kantor = null;
			if(prcMainHeader.getDistrictKeuangan()!=null){
				kantor = prcMainHeader.getDistrictKeuangan();
			}else{
				kantor = prcMainHeader.getAdmDistrict();
			}
			
			
			List<PrcAnggaranJdeDetail> lst = listAnggaranDetailByPPmId(prcMainHeader);
			AdmProsesHirarki ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel()+1, kantor, prcMainHeader.getPpmJumlahOe().doubleValue());
			
			if(ahp==null){
				AdmRole smBarang = new AdmRole(112);//Langsung ke kabiro pengadaan barang
				AdmRole smJasa = new AdmRole(195);//langsung ke kabiro pengadaan jasa
				AdmProses proses = SessionGetter.PP_PROSES_ID;//level ke 3
				prcMainHeader.setAdmProses(proses);
				prcMainHeader.setPpmProsesLevel(2);//disposisi otomatis
				if(prcMainHeader.getPpmJenis().getId()== 1){
					//ahp = SessionGetter.getHirarkiProsesByJenis(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel(), prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahOe().doubleValue(), 1);
					//AdmUser usr = SessionGetter.getEmployeeByRoleName("SENIOR MANAGER SPAREPART & MATERIAL PROCUREMENT").getAdmUser();
//					AdmRole = SessionGetter.getRoleById(new AdmRole(112));
					AdmUser usr = SessionGetter.getEmployeeByRoleName("SENIOR MANAGER SPAREPART & MATERIAL PROCUREMENT").getAdmUser();
					prcMainHeader.setAdmUserByPpmCurrentUser(usr);
					prcMainHeader.setPengawasPp(prcMainHeader.getAdmUserByPpmCurrentUser());
					prcMainHeader.setAdmRole(smBarang);
				}else if(prcMainHeader.getPpmJenis().getId()== 2){
					//ahp = SessionGetter.getHirarkiProsesByJenis(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel(), prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahOe().doubleValue(), 2);
					//AdmUser usr = SessionGetter.getEmployeeByRoleName("SENIOR MANAGER SPAREPART & MATERIAL PROCUREMENT").getAdmUser();
//					AdmRole = SessionGetter.getRoleById(new AdmRole(112));
					AdmUser usr = SessionGetter.getEmployeeByRoleName("SENIOR MANAGER SERVICE PROCUREMENT").getAdmUser();
					prcMainHeader.setAdmUserByPpmCurrentUser(usr);
					prcMainHeader.setPengawasPp(prcMainHeader.getAdmUserByPpmCurrentUser());
					prcMainHeader.setAdmRole(smJasa);
				}else{
					throw new Exception("Kesalahan dalam hirarki");
				}
				
//				prcMainHeader.setPpmProsesLevel(1);
//				ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(), 1, prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahOe().doubleValue());
//				if(ahp==null){
//					throw new Exception("Kesalahan dalam hirarki");
//				}
//				prcMainHeader.setAdmUserByPpmCurrentUser(SessionGetter.getEmployeeByRole(ahp.getAdmRoleByCurrentPos()).getAdmUser());
				histMainComment.setCreatedBy(SessionGetter.getPropertiesValue("pp.1", null));
				//insert ke JDE
				
				//update status Anggaran
				//create line ID
				Double data = 0.0;
				
				if(lst!=null){
					int line = 1000;
					for(PrcAnggaranJdeDetail d:lst){
						d.setRklnid(line);
						System.out.println(">>>LINE" + d.getRklnid());
						saveEntity(d);
						line = line+1000;
						data = data+d.getRkacm1().doubleValue();
					}
				}
				
				//update PrcAnggaranJde
				PrcAnggaranJdeHeader ph = getJdeAnggaranHeaderByPpmId(prcMainHeader);
				ph.setKaacm1(new BigDecimal(data));
				ph.setKaea(new BigDecimal(data));
				saveEntity(ph);
				ph.setKaapfl("A");
//				head.setKatorg(SessionGetter.getUser().getUsername().toUpperCase());//ambil dari JDE
//				head.setKauser(SessionGetter.getUser().getUsername().toUpperCase());
				ph.setKatorg("ALAM");//ambil dari JDE
				ph.setKauser("ALAM");
				saveEntity(ph);
				
				
				if(lst!=null){
					for(PrcAnggaranJdeDetail d:lst){
						System.out.println(d.getRklnid());
						d.setRkapfl("A");
//						d.setRkuser(SessionGetter.getUser().getUsername().toUpperCase());
						d.setRkuser("ALAM");
						saveEntity(d);
					}
				}
				

				PrcAnggaranJdeHeader prcHeader = getJdeAnggaranHeaderByPpmId(prcMainHeader);
				List<PrcAnggaranJdeDetail> listDetail = listAnggaranDetailByPPmId(prcMainHeader);
				//get doco
				//Integer sessionID = JdeSessionUtil.loginJde("ALAM","ALAM", SessionGetter.valueProperty("jde.env"), "*ALL");
				//UserSession session = Connector.getInstance().getUserSession(sessionID);
				//if(sessionID!=null){
					int data1 = Calendar.getInstance().get(Calendar.YEAR);
					String fiscal = String.valueOf(data1).substring(2);
					//prcHeader.setKadoco(JdeService.getNextNumberAnggaran("00000","KA","20",fiscal,session,sessionID));
					System.out.println("SESSION>>>"+session);
					//System.out.println("SESSION>>>"+sessionID);
					
					System.out.println("KADOCO>>>"+prcHeader.getKadoco());
					System.out.println("KAKCOO>>>"+prcHeader.getKakcoo());
					System.out.println("KADCTO>>>"+prcHeader.getKadcto());
					updateEntity(prcHeader);
					//JdeSessionUtil.loggofJDE(sessionID);
				//}
				//JdeService.insertAnggaran(prcHeader, lst);
				updateEntity(prcHeader);
			}else{
				if(StringUtils.equalsIgnoreCase(ahp.getAdmRoleByCurrentPos().getRoleName(), "DIR KEUANGAN")){
					prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
					prcMainHeader.setAdmUserByPpmCurrentUser(SessionGetter.getEmployeeByRole(ahp.getAdmRoleByCurrentPos()).getAdmUser());
					prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
				}else{
					prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
					prcMainHeader.setAdmUserByPpmCurrentUser(SessionGetter.getEmployeeByRole(ahp.getAdmRoleByCurrentPos()).getAdmUser());
					prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
				}
			}
			
			updateEntity(prcMainHeader);

			//upload dokumen
			if(SessionGetter.isNotNull(docsKomentar)){
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}
			
			Double data = 0.0;
			
			if(lst!=null){
				for(PrcAnggaranJdeDetail d:lst){
					data = data+d.getRkacm1().doubleValue();
				}
			}
			if(data.doubleValue()!=prcMainHeader.getPpmJumlahOe().doubleValue()){
				RollbackAndClear();
				throw new Exception("Jumlah Anggaran Harus Sama dengan Jumlah OE");
			}
			//insert history
			System.out.println("SESSIONHISTORY>>>"+session);
			SessionGetter.saveHistoryGlobalPrOE(histMainComment, prcMainHeader, session);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public void createAnggaran(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception {
		try {
			startTransaction();
			prcMainHeader = getPrcMainIHeaderById(prcMainHeader.getPpmId());
			//cek kantor
			AdmDistrict kantor = null;
			if(prcMainHeader.getDistrictKeuangan()!=null){
				kantor = prcMainHeader.getDistrictKeuangan();
			}else{
				kantor = prcMainHeader.getAdmDistrict();
			}
			
			AdmProsesHirarki ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel()+1, kantor, prcMainHeader.getPpmJumlahOe().doubleValue());
			
			List<PrcAnggaranJdeDetail> lst = listAnggaranDetailByPPmId(prcMainHeader);
			if(ahp==null){
				AdmRole smBarang = new AdmRole(112);//Langsung ke kabiro pengadaan barang
				AdmRole smJasa = new AdmRole(195);//langsung ke kabiro pengadaan jasa
				AdmProses proses = SessionGetter.PP_PROSES_ID;//langsung ke pemilihan buyer
				prcMainHeader.setAdmProses(proses);
				prcMainHeader.setPpmProsesLevel(2);//disposisi otomatis
				if(prcMainHeader.getPpmJenis().getId()==1){
					//langsung ke kabiro pengadaan barang
					//ahp = SessionGetter.getHirarkiProsesByJenis(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel(), prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahOe().doubleValue(), 1);
					AdmUser usr = SessionGetter.getEmployeeByRoleName("SENIOR MANAGER SPAREPART & MATERIAL PROCUREMENT").getAdmUser();
					prcMainHeader.setAdmUserByPpmCurrentUser(usr);
					prcMainHeader.setPengawasPp(prcMainHeader.getAdmUserByPpmCurrentUser());
					prcMainHeader.setAdmRole(smBarang);
				}else if(prcMainHeader.getPpmJenis().getId()== 2){
					//langsung ke kabiro pengadaan jasa
					//ahp = SessionGetter.getHirarkiProsesByJenis(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel(), prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahOe().doubleValue(), 2);
					AdmUser usr = SessionGetter.getEmployeeByRoleName("SENIOR MANAGER SERVICE PROCUREMENT").getAdmUser();
					prcMainHeader.setAdmUserByPpmCurrentUser(usr);
					prcMainHeader.setPengawasPp(prcMainHeader.getAdmUserByPpmCurrentUser());
					prcMainHeader.setAdmRole(smJasa);

				}else{
					throw new Exception("Kesalahan dalam hirarki");
				}
//				ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(), 2, prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahOe().doubleValue());
//				if(ahp==null){
//					throw new Exception("Kesalahan dalam hirarki");
//				}
//				prcMainHeader.setAdmUserByPpmCurrentUser(SessionGetter.getEmployeeByRole(ahp.getAdmRoleByCurrentPos()).getAdmUser());
				histMainComment.setCreatedBy(SessionGetter.getPropertiesValue("pp.2", null));
				//insert ke JDE
				
				//update status Anggaran
				//create line ID
				Double data = 0.0;
				
				if(lst!=null){
					int line = 1000;
					for(PrcAnggaranJdeDetail d:lst){
						d.setRklnid(line);
						System.out.println(">>>LINE" + d.getRklnid());
						saveEntity(d);
						line = line+1000;
						data = data+d.getRkacm1().doubleValue();
					}
				}
				
				//update PrcAnggaranJde
				PrcAnggaranJdeHeader ph = getJdeAnggaranHeaderByPpmId(prcMainHeader);
				ph.setKaacm1(new BigDecimal(data));
				ph.setKaea(new BigDecimal(data));
				saveEntity(ph);
				ph.setKaapfl("A");
//				head.setKatorg(SessionGetter.getUser().getUsername().toUpperCase());//ambil dari JDE
//				head.setKauser(SessionGetter.getUser().getUsername().toUpperCase());
				ph.setKatorg("ALAM");//ambil dari JDE
				ph.setKauser("ALAM");
				saveEntity(ph);
				
				
				if(lst!=null){
					for(PrcAnggaranJdeDetail d:lst){
						System.out.println(d.getRklnid());
						d.setRkapfl("A");
						//d.setRkuser(SessionGetter.getUser().getUsername().toUpperCase());
						d.setRkuser("ALAM");
						saveEntity(d);
					}
				}
				

				PrcAnggaranJdeHeader prcHeader = getJdeAnggaranHeaderByPpmId(prcMainHeader);
				List<PrcAnggaranJdeDetail> listDetail = listAnggaranDetailByPPmId(prcMainHeader);
//				//get doco
				//Integer sessionID = JdeSessionUtil.loginJde("ALAM","ALAM", SessionGetter.valueProperty("jde.env"), "*ALL");
				//UserSession session = Connector.getInstance().getUserSession(sessionID);
				//if(sessionID!=null){
					int data1 = Calendar.getInstance().get(Calendar.YEAR);
					String fiscal = String.valueOf(data1).substring(2);
					//prcHeader.setKadoco(JdeService.getNextNumberAnggaran("00000","KA","20",fiscal,session,sessionID));
					System.out.println("KADOCO>>>"+prcHeader.getKadoco());
					System.out.println("KAKCOO>>>"+prcHeader.getKakcoo());
					System.out.println("KADCTO>>>"+prcHeader.getKadcto());	
					updateEntity(prcHeader);
					//JdeSessionUtil.loggofJDE(sessionID);
				//}
				//JdeService.insertAnggaran(prcHeader, lst);
				updateEntity(prcHeader);
			}else{
				if(StringUtils.equalsIgnoreCase(ahp.getAdmRoleByCurrentPos().getRoleName(), "DIR KEUANGAN")){
					prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
					prcMainHeader.setAdmUserByPpmCurrentUser(SessionGetter.getEmployeeByRole(ahp.getAdmRoleByCurrentPos()).getAdmUser());
					prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
				}else{
					throw new Exception("Kesalahan pada proses hirarki");
				}
			}
			
			updateEntity(prcMainHeader);

			//upload dokumen
			if(SessionGetter.isNotNull(docsKomentar)){
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}
			
			Double data = 0.0;
			
			if(lst!=null){
				for(PrcAnggaranJdeDetail d:lst){
					data = data+d.getRkacm1().doubleValue();
				}
			}
			if(data.doubleValue()!=prcMainHeader.getPpmJumlahOe().doubleValue()){
				RollbackAndClear();
				throw new Exception("Jumlah Anggaran Harus Sama dengan Jumlah OE");
			}
//			//insert history
			System.out.println("SESSIONHISTORY>>>"+session);
			SessionGetter.saveHistoryGlobalPrOE(histMainComment, prcMainHeader, session);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public void createAnggaranDetail(PrcAnggaranJdeDetail anggaranJdeDetail)
			throws Exception {
		try {
			startTransaction();
			//creaate header if not exist
			PrcMainHeader mainHeader = getPrcMainIHeaderById(anggaranJdeDetail.getPrcMainHeader().getPpmId());
			PrcAnggaranJdeHeader jdeHeader = getJdeAnggaranHeaderByPpmId(anggaranJdeDetail.getPrcMainHeader());
			boolean or = true;
			if(mainHeader.getPpmNomorPrSap().contains("ZST")){
				or = true;
			}else{
				or = false;
			}
			if(jdeHeader==null){
				//retrive prcMainHeader);
				OrHeader orh = getOrHeaderById(mainHeader); 
				String tmp [] = mainHeader.getPpmNomorPrSap().contains("ZMN")? mainHeader.getPpmNomorPrSap().trim().split("ZMN"):
					mainHeader.getPpmNomorPrSap().contains("ZST")?mainHeader.getPpmNomorPrSap().trim().split("ZST"):
						mainHeader.getPpmNomorPrSap().contains("ZMN")?mainHeader.getPpmNomorPrSap().trim().split("ZMN"):
							mainHeader.getPpmNomorPrSap().trim().split("ZST");				
				jdeHeader = new PrcAnggaranJdeHeader();
				jdeHeader.setPrcMainHeader(anggaranJdeDetail.getPrcMainHeader());
				jdeHeader.setKakcoo(tmp[1].trim());
				jdeHeader.setKadcto("KA");
				jdeHeader.setKaaid("");
				jdeHeader.setKatrdj(new Date());
				jdeHeader.setKaapdj("0");
				jdeHeader.setKaco(tmp[1].trim());
				jdeHeader.setKakco(tmp[1].trim());
				jdeHeader.setKadct(or?"ZST":"ZMN");
				jdeHeader.setKadoc(Long.valueOf(tmp[0].trim()));//dokumen awal
				if(orh!=null)
					if(orh.getTglOr()!=null)
						jdeHeader.setKabasedj(JulianToGregorianViceVersa.getJulian7FromDate(orh.getTglOr()));//tgl dokumen awal OR
					else
						jdeHeader.setKabasedj("0");//tgl dokumen awal OR
				jdeHeader.setKamcu(tmp[1].trim());
				jdeHeader.setKammcu(tmp[1].trim());
				jdeHeader.setKaan8("0");//kode koordinator anggaran
				jdeHeader.setKaan82("0");//kode penandatangan KA
				if(mainHeader.getPpmSubject().length()>=30){
					jdeHeader.setKadsc1(mainHeader.getPpmSubject().substring(0, 29));
				}else
					jdeHeader.setKadsc1(mainHeader.getPpmSubject().trim());
				
				if(mainHeader.getPpmJenis()!=null && mainHeader.getPpmJenis()== new AdmJenis(2))
					jdeHeader.setKajj("");//jenis jasa
				jdeHeader.setKaapfl("");//status anggaran
				jdeHeader.setKaska("02");//status penanda tanganan
				jdeHeader.setKahst("");
				jdeHeader.setKaaap(0);
				jdeHeader.setKaea(mainHeader.getPpmJumlahOe());
				jdeHeader.setKaacm1(mainHeader.getPpmJumlahOe());
				jdeHeader.setKaaa(new BigDecimal("0"));
//				jdeHeader.setKatorg(SessionGetter.getUser().getUsername().toUpperCase());//ambil dari JDE
//				jdeHeader.setKauser(SessionGetter.getUser().getUsername().toUpperCase());
				jdeHeader.setKatorg(SessionGetter.getUser().getUsername().toUpperCase());//ambil dari JDE
				jdeHeader.setKauser(SessionGetter.getUser().getUsername().toUpperCase());
				jdeHeader.setKajobn("MUSI");//komputer
				jdeHeader.setKupmj(new Date());
				jdeHeader.setKupmt(new Date());
				jdeHeader.setKapid("");
				saveEntity(jdeHeader);
			}else{
				//retrive prcMainHeader
				OrHeader orh = getOrHeaderById(mainHeader); 
				String tmp [] = mainHeader.getPpmNomorPrSap().contains("ZMN")? mainHeader.getPpmNomorPrSap().trim().split("ZMN"):
					mainHeader.getPpmNomorPrSap().contains("ZST")?mainHeader.getPpmNomorPrSap().trim().split("ZST"):
						mainHeader.getPpmNomorPrSap().contains("ZMN")?mainHeader.getPpmNomorPrSap().trim().split("ZMN"):
							mainHeader.getPpmNomorPrSap().trim().split("ZST");				
				jdeHeader.setPrcMainHeader(anggaranJdeDetail.getPrcMainHeader());
				jdeHeader.setKakcoo(tmp[1].trim());
				jdeHeader.setKadcto("KA");
				jdeHeader.setKaaid("");
				jdeHeader.setKatrdj(new Date());
				jdeHeader.setKaapdj("0");
				jdeHeader.setKaco(tmp[1].trim());
				jdeHeader.setKakco(tmp[1].trim());
				jdeHeader.setKadct(or?"ZST":"ZMN");
				jdeHeader.setKadoc(Long.valueOf(tmp[0].trim()));//dokumen awal
				if(orh!=null)
					if(orh.getTglOr()!=null)
						jdeHeader.setKabasedj(JulianToGregorianViceVersa.getJulian7FromDate(orh.getTglOr()));//tgl dokumen awal OR
					else
						jdeHeader.setKabasedj(JulianToGregorianViceVersa.getJulian7FromDate(new Date()));//tgl dokumen awal
				jdeHeader.setKamcu(tmp[1].trim());
				jdeHeader.setKammcu(tmp[1].trim());
				jdeHeader.setKaan8("0");//kode koordinator anggaran
				jdeHeader.setKaan82("0");//kode penandatangan KA
				if(mainHeader.getPpmSubject().length()>=30){
					jdeHeader.setKadsc1(mainHeader.getPpmSubject().substring(0, 29));
				}else
					jdeHeader.setKadsc1(mainHeader.getPpmSubject().trim());
				if(mainHeader.getPpmJenis()!=null && mainHeader.getPpmJenis()== new AdmJenis(2))
					jdeHeader.setKajj("J03");//jenis jasa
				jdeHeader.setKaapfl("");//status anggaran
				jdeHeader.setKaska("02");//status penanda tanganan
				jdeHeader.setKahst("");
				jdeHeader.setKaaap(0);
				Double total = mainHeader.getPpmJumlahOe().doubleValue();
				jdeHeader.setKaea(new BigDecimal(total));
				jdeHeader.setKaacm1(new BigDecimal(total));
				jdeHeader.setKaaa(new BigDecimal(0));
//				jdeHeader.setKatorg(SessionGetter.getUser().getUsername().toUpperCase());//ambil dari JDE
//				jdeHeader.setKauser(SessionGetter.getUser().getUsername().toUpperCase());
				jdeHeader.setKatorg(SessionGetter.getUser().getUsername().toUpperCase().trim());//ambil dari JDE
				jdeHeader.setKauser(SessionGetter.getUser().getUsername().toUpperCase().trim());
				jdeHeader.setKajobn("MUSI");//komputer
				jdeHeader.setKupmj(new Date());
				jdeHeader.setKupmt(new Date());
				jdeHeader.setKapid("");
				saveEntity(jdeHeader);
			}
			
			anggaranJdeDetail.setPrcAnggaranJdeHeader(jdeHeader);
			anggaranJdeDetail.setRkaap(new BigDecimal(0));
			anggaranJdeDetail.setRkaa(new BigDecimal(0));
			anggaranJdeDetail.setRkaea(new BigDecimal(0));
			anggaranJdeDetail.setRkdl01("");
			anggaranJdeDetail.setRkdl02("");
			anggaranJdeDetail.setRktorg("");
//			anggaranJdeDetail.setRkuser(SessionGetter.getUser().getUsername().toUpperCase());
			anggaranJdeDetail.setRkuser(SessionGetter.getUser().getUsername().toUpperCase().trim());
			anggaranJdeDetail.setRkjobn("MUSI");
			anggaranJdeDetail.setRkapid("");
			if(mainHeader.getAdmProses().getId()!=SessionGetter.ANGGARAN_PROSES_ID.getId()){
				anggaranJdeDetail.setStatus("R");
			}
			saveEntity(anggaranJdeDetail);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	public OrHeader getOrHeaderById(PrcMainHeader mainHeader) {
		OrHeaderId id = new OrHeaderId();
		String tmp [] = mainHeader.getPpmNomorPrSap().split(" ");
		id.setHeaderSite(tmp[2]);
		id.setNomorOr(Long.valueOf(tmp[0]));
		id.setOrH("ZMN");
		DetachedCriteria dc = DetachedCriteria.forClass(OrHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.add(Restrictions.idEq(id));
		return (OrHeader) getEntityByDetachedCiteria(dc);
	}

	public PrcAnggaranJdeHeader getJdeAnggaranHeaderByPpmId(
			PrcMainHeader prcMainHeader) {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcAnggaranJdeHeader.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.idEq(prcMainHeader.getPpmId()));
		return (PrcAnggaranJdeHeader) getEntityByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcAnggaranJdeDetail> listAnggaranDetailByPPmId(
			PrcMainHeader prcMainHeader) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcAnggaranJdeDetail.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("prcMainHeader", prcMainHeader));
		return (List<PrcAnggaranJdeDetail>) getListByDetachedCiteria(crit);
	}
	
	@SuppressWarnings("unchecked")
	public List<PrcAnggaranJdeDetail> listAnggaranDetailByPPmIdManual(
			PrcMainHeader prcMainHeader) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcAnggaranJdeDetail.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("prcMainHeader", prcMainHeader));
		crit.addOrder(Order.asc("rklnid"));
		return (List<PrcAnggaranJdeDetail>) getListByDetachedCiteria(crit);
	}

	@Override
	public void deleteAnggaran(PrcAnggaranJdeDetail anggaranJdeDetail)
			throws Exception {
		try {
			startTransaction();
			DetachedCriteria crit = DetachedCriteria.forClass(PrcAnggaranJdeDetail.class);
			crit.add(Restrictions.idEq(anggaranJdeDetail.getId()));
			anggaranJdeDetail = (PrcAnggaranJdeDetail) getEntityByDetachedCiteria(crit);
			if(anggaranJdeDetail!=null)
				deleteEntity(anggaranJdeDetail);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<PanitiaPengadaanDetail> getListPanitiaPengadaanDetail(
			PanitiaPengadaan o) {
		DetachedCriteria crit = DetachedCriteria.forClass(PanitiaPengadaanDetail.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("panitia", FetchMode.JOIN);
		crit.setFetchMode("anggota", FetchMode.JOIN);
		crit.add(Restrictions.eq("panitia", o));
		
		return (List<PanitiaPengadaanDetail>) getListByDetachedCiteria(crit);
	}

	@Override
	public PanitiaPengadaan getPanitiaPengadaan(PanitiaPengadaan o) {
		DetachedCriteria crit = DetachedCriteria.forClass(PanitiaPengadaan.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("id", o.getId()));
		
		return (PanitiaPengadaan) getEntityByDetachedCiteria(crit);
	}

	@Override
	public PrcAnggaranJdeDetail getAnggaranDetailById(PrcAnggaranJdeDetail anggaranJdeDetail)
			throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcAnggaranJdeDetail.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.idEq(anggaranJdeDetail.getId()));
		return (PrcAnggaranJdeDetail) getEntityByDetachedCiteria(crit);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainVendorQuote> getListMainVendorQuoteByPpmId(
			PrcMainHeader prcMainHeader) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMainVendorQuote.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("id.ppmId", new BigDecimal(prcMainHeader.getPpmId())));
		return (List<PrcMainVendorQuote>) getListByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TodoUserCommand> getListTodoUserKontrak(String todoListKontrak)
			throws Exception {
		List<TodoUserCommand> retMe = new ArrayList<TodoUserCommand>();
		try {
			startTransaction();
			Query q =  session.createSQLQuery(todoListKontrak);
			List<Object[]> obj = q.list();
			if(obj!=null && obj.size()>0){
				Iterator<Object[]> it = obj.iterator();
				while(it.hasNext()){
					Object o[] = it.next();
					it.remove();
					TodoUserCommand cmd = new TodoUserCommand();
					cmd.setContractId(Integer.valueOf(String.valueOf(o[0])));
					cmd.setNomorSpph(o[2]!=null?String.valueOf(o[1]):"");
					cmd.setNomorKontrak(String.valueOf(o[2]));
					cmd.setCurrentUser(o[3]!=null?Integer.valueOf(String.valueOf(o[3])):0);
					cmd.setCompleteName(o[4]!=null?String.valueOf(o[4]):"");
					cmd.setUpdatedDate(o[5]!=null?(Date)o[5]:null);
					cmd.setKeterangan(String.valueOf(o[6]));
					cmd.setUrl(String.valueOf(o[7]));
					cmd.setJudulSpph(String.valueOf(o[8]));
					Integer prioritas = Integer.valueOf(String.valueOf(o[9]));
					String prio=null;
					if(prioritas==1){
						prio = "High";
					}else if(prioritas==2){
						prio = "Normal";
					}else{
						prio = "Low";
					}
					cmd.setKeterangan(cmd.getKeterangan()+" <br/> ( "+prio+" )");
					cmd.setPemenang(o[10]!=null?String.valueOf(o[10]).toUpperCase():"");
					retMe.add(cmd);
				}
			}
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
		return retMe;
	}

	@Override
	public void batalPrSpph(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment) throws Exception {
		try {
			startTransaction();
			PrcMainHeader ph = getPrcMainIHeaderById(prcMainHeader.getPpmId());
			ph.setPpmProsesLevel(-99);//batal
			ph.setAdmUserByPpmCurrentUser(null);
			ph.setAdmRole(null);
			saveEntity(ph);
			
			//insert history
			HistMainComment comment = new HistMainComment();
			comment.setAksi("Pembatalan");
			comment.setComment(histMainComment.getComment());
			comment.setPosisi(SessionGetter.getUser().getAdmEmployee().getEmpStructuralPos());
			comment.setPrcMainHeader(ph);
			comment.setProses(null);
			comment.setProsesAksi("Pembatalan");
			comment.setUpdatedDate(new Date());
			comment.setUsername(SessionGetter.getUser().getCompleteName());;
			comment.setDocument(histMainComment.getDocument());
			saveEntity(comment);
			
			//update JDE
			/*String tmp [] = ph.getPpmNomorPr().trim().split("ZMN");
			POHeader poHeader = new POHeader();
			poHeader.setSzOrderCompany(tmp[1].trim());//kcoo
			poHeader.setMnOrderNumber(Integer.valueOf(tmp[0].trim()));//doco
			poHeader.setSzOrderType("ZMN");//DCTO
			
			//get anggaran header
			PrcAnggaranJdeHeader pah = getJdeAnggaranHeaderByPpmId(ph);
			
			//update status batal
			String orderType = ph.getPpmNomorPrSap().contains("ZMN")?"ZMN":"ZST";
			updateOrStatus(poHeader,pah,orderType);*/
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			e.printStackTrace();
			throw e;
		}
		
	}
	
	private void updateOrStatus(POHeader poHeader, PrcAnggaranJdeHeader pah, String orderType) throws Exception {
		Session ses = HibernateUtil.getSessionJde();
		try {
			ses.beginTransaction();
			List<PODetail> lpd = new RfqServiceImpl().getListPoDetail(poHeader,orderType);
			if(lpd!=null && lpd.size()>0){
				for(PODetail p:lpd){
					String query = "update "+SessionGetter.valueProperty("jde.schema")+".f4311 a set a.pdnxtr='999', a.pdlttr='980',  a.pduopn=0, a.pdaopn=0 " +
							" where trim(a.pdkcoo)='"+poHeader.getSzOrderCompany().trim()+"' and a.pdlnid="+p.getMnOrderLineNumber()+" and a.pddoco="+p.getSzOriginalOrderNumber()+" and a.pddcto='"+orderType+"'";
					Query q = ses.createSQLQuery(query);
					q.executeUpdate();
				}
				
				//update anggaran
				if(pah!=null && pah.getKadoco()!=null && pah.getKadoco().trim().length()>0){
					String query = "update "+SessionGetter.valueProperty("jde.schema")+".f559901 set kaapfl='R' where kakcoo ='"+pah.getKakcoo()+"' and  kadcto = '"+pah.getKadcto()+"' and kadoco='"+pah.getKadoco()+"' ";
					Query q = ses.createSQLQuery(query);
					q.executeUpdate();
					
					//update detail
					String queryDetail="update "+SessionGetter.valueProperty("jde.schema")+".f559911 set rkapfl='R' where rkkcoo='"+pah.getKakcoo()+"' and rkdcto ='"+pah.getKadcto()+"' and rkdoco='"+pah.getKadoco()+"' ";
					Query qd = ses.createSQLQuery(queryDetail);
					qd.executeUpdate();
				}
			}
			ses.getTransaction().commit();
		} catch (Exception e) {
			ses.getTransaction().rollback();
			ses.clear();
			ses.close();
			e.printStackTrace();
			throw e;
		}
		
	}

	@Override
	public int countsearchPrcMainHeade(PrcMainHeader o)
			throws Exception {
		Integer retMe = 0;
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMainHeader.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setProjection(Projections.rowCount());
		crit.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
		crit.createAlias("admUserByPpmCurrentUser", "us");
		crit.setFetchMode("admDept", FetchMode.JOIN);
		crit.add(Restrictions.or(Restrictions.ilike("us.completeName", o.getPpmNomorPr().toLowerCase(),MatchMode.ANYWHERE),Restrictions.or(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE), Restrictions.like("ppmNomorPr", o.getPpmNomorPr(),MatchMode.ANYWHERE))));
		if(o.getDistrictUser()!=null){
			crit.add(Restrictions.eq("districtUser", o.getDistrictUser()));
		}
		retMe = Integer.valueOf(String.valueOf(getEntityByDetachedCiteria(crit)));
		return retMe;
	}

	@Override
	public void saveOeFinisManual(PrcMainHeader prcMainHeader,
			List<PrcMainItem> listPrcMainItem, HistMainComment histMainComment,
			File docsKomentar, String docsKomentarFileName) throws Exception {
		try {
			startTransaction();
			PrcMainHeader tmp = prcMainHeader;
			prcMainHeader = getPrcMainIHeaderById(prcMainHeader.getPpmId());
			saveEntity(prcMainHeader);
			
			Double jumlahOe =0.0;
			//update item
			if(listPrcMainItem!=null && listPrcMainItem.size()>0){
				for(PrcMainItem item:listPrcMainItem){
					saveEntity(item);
					BigDecimal total = item.getItemQuantityOe().multiply(item.getItemPriceOe());
					jumlahOe = jumlahOe+total.doubleValue();
				}
			}
			
		
			
			//update jumlah OE
			prcMainHeader.setPpmJumlahOe(new BigDecimal(jumlahOe));
			saveEntity(prcMainHeader);
			IntegrationService integrationService = new IntegrationServiceImpl();
			//Validate Anggaran
			ResultDto result = integrationService.changePr(listPrcMainItem,prcMainHeader);
			if(result.getStatus()!=200) {
				throw new Exception("Terjadi kesalahan saat proses checking anggaran ");
			}
			//upload dokumen
			if(SessionGetter.isNotNull(docsKomentar)){
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}
			
			//insert history
			HistMainComment last = SessionGetter.getLastCommentByHeader(prcMainHeader);
			HistMainComment tmps = SessionGetter.getLastCommentByHeader(prcMainHeader);
			
			//old stuff
			Date crDate = tmps.getCreatedDate();
			String user = tmps.getUsername();
			String posisi = tmps.getPosisi();
			String aksi = tmps.getAksi();
			String prosesAksi = tmps.getProsesAksi();
			
			//next user
			last.setPrcMainHeader(prcMainHeader);
			last.setUsername(SessionGetter.getUser().getCompleteName());
			last.setPosisi(SessionGetter.getUser().getAdmEmployee().getEmpStructuralPos());
			last.setProses(prcMainHeader.getAdmProses().getId());
			last.setAksi("Update OE");
			last.setProsesAksi("Update OE");
			last.setDocument(histMainComment.getDocument());
			last.setComment(histMainComment.getComment());
			last.setCreatedDate(new Date());
			last.setUpdatedDate(new Date());
			updateEntity(last);
			
			//save old stuff
			HistMainComment oldes = new HistMainComment();
			oldes.setAksi(aksi);
			oldes.setProsesAksi(prosesAksi);
			oldes.setPosisi(posisi);
			oldes.setUsername(user);
			oldes.setPrcMainHeader(prcMainHeader);
			oldes.setCreatedDate(crDate);
			saveEntity(oldes);

			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> listPrcMainHeaderOEManual(Object object)
			throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		dc.add(Restrictions.eq("admUserByPpmEstimator", SessionGetter.getUser()));
		dc.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
		dc.add(Restrictions.eq("ppmProsesLevel", 5));
		return (List<PrcMainHeader>) getListByDetachedCiteria(dc);
	}

	@Override
	public void createAnggaranManual(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception {
		try {
			startTransaction();
			prcMainHeader = getPrcMainIHeaderById(prcMainHeader.getPpmId());
			
			//create line ID
			List<PrcAnggaranJdeDetail> lst = listAnggaranDetailByPPmIdManual(prcMainHeader);
			Double data = 0.0;
			if(lst!=null){
				int line = 1000;
				for(PrcAnggaranJdeDetail d:lst){
					d.setRklnid(line);
					saveEntity(d);
					line = line+1000;
					data = data+d.getRkacm1().doubleValue();
				}
			}
			//update PrcAnggaranJde
			PrcAnggaranJdeHeader ph = getJdeAnggaranHeaderByPpmId(prcMainHeader);
			ph.setKaacm1(new BigDecimal(data));
			ph.setKaea(new BigDecimal(data));
			saveEntity(ph);

			//upload dokumen
			if(SessionGetter.isNotNull(docsKomentar)){
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}
			
			//insert history
			//insert history
			HistMainComment last = SessionGetter.getLastCommentByHeader(prcMainHeader);
			HistMainComment tmps = SessionGetter.getLastCommentByHeader(prcMainHeader);
			
			//old stuff
			Date crDate = tmps.getCreatedDate();
			String user = tmps.getUsername();
			String posisi = tmps.getPosisi();
			String aksi = tmps.getAksi();
			String prosesAksi = tmps.getProsesAksi();
			
			//next user
			last.setPrcMainHeader(prcMainHeader);
			last.setUsername(SessionGetter.getUser().getCompleteName());
			last.setPosisi(SessionGetter.getUser().getAdmEmployee().getEmpStructuralPos());
			last.setProses(prcMainHeader.getAdmProses().getId());
			last.setAksi("Revisi Anggaran");
			last.setProsesAksi("Revisi Anggaran");
			last.setDocument(histMainComment.getDocument());
			last.setComment(histMainComment.getComment());
			updateEntity(last);
			
			//save old stuff
			HistMainComment oldes = new HistMainComment();
			oldes.setAksi(aksi);
			oldes.setProsesAksi(prosesAksi);
			oldes.setPosisi(posisi);
			oldes.setUsername(user);
			oldes.setPrcMainHeader(prcMainHeader);
			oldes.setCreatedDate(crDate);
			saveEntity(oldes);
			
			//delete dulu
			JdeService.deleteAnggaranDetail(ph);
			
			//update ke jde
			JdeService.updateAnggaran(ph, lst);
			
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> listPrcMainHeaderPpAnggaranManual(Object object)
			throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		dc.setFetchMode("admProses", FetchMode.JOIN);
		dc.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
		dc.add(Restrictions.eq("ppmProsesLevel", 5));
		return (List<PrcMainHeader>) getListByDetachedCiteria(dc);
	}

	@Override
	public void updateSpph(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName,
			List<PanitiaPengadaanDetail> listPanitiaDetail,
			List<File> ppdDocument, List<String> ppdDocumentFileName,
			List<String> ppdCategory, List<String> ppdDescription,
			List<PrcMainDoc> listPrcMainDoc) throws Exception {
		try {
			startTransaction();
			PrcMainHeader old = getPrcMainIHeaderById(prcMainHeader.getPpmId());
			//cek panitia dan Update
			if(listPanitiaDetail!=null && listPanitiaDetail.size()>0){
				for(PanitiaPengadaanDetail p:listPanitiaDetail){
					saveEntity(p);
				}
			}
			
			String userName="";
			String posisiOld="";
			//cek jika jika buyer berubah
			if(old.getAdmUserByPpmBuyer()!=null){
				if(old.getAdmUserByPpmBuyer().getId().longValue()!=prcMainHeader.getAdmUserByPpmBuyer().getId().longValue()){
					//update buyer
					if(old.getAdmUserByPpmBuyer().getId().longValue()== old.getAdmUserByPpmCurrentUser().getId().longValue()){
						//jika buyer sama dengan current user
						old.setAdmUserByPpmCurrentUser(prcMainHeader.getAdmUserByPpmBuyer());
						AdmUser user =new AdmServiceImpl().getAdmUser(old.getAdmUserByPpmCurrentUser());
						userName = user.getCompleteName();
						posisiOld = user.getAdmEmployee().getEmpStructuralPos();
					}
					old.setAdmUserByPpmBuyer(prcMainHeader.getAdmUserByPpmBuyer());
					old.setAdmUserByPpmAdmBuyer(prcMainHeader.getAdmUserByPpmBuyer());
				}
			}
			
			//cek jika jika estimator berubah
			if(old.getAdmUserByPpmEstimator()!=null) {
				if(old.getAdmUserByPpmEstimator().getId().longValue() == old.getAdmUserByPpmCurrentUser().getId().longValue()) {
					//jika estimator sama dengan current user
					old.setAdmUserByPpmCurrentUser(prcMainHeader.getAdmUserByPpmEstimator());
				}
				old.setAdmUserByPpmEstimator(prcMainHeader.getAdmUserByPpmEstimator());
			}
			
			//cek jika kabag
			if(old.getPengawasPp()!=null){
				if(old.getPengawasPp().getId().longValue()!=prcMainHeader.getPengawasPp().getId().longValue()){
					//update kabag
					if(old.getPengawasPp().getId().longValue()== old.getAdmUserByPpmCurrentUser().getId().longValue()){
						//jika kabag sama dengan current user
						old.setAdmUserByPpmCurrentUser(prcMainHeader.getPengawasPp());
						AdmUser user =new AdmServiceImpl().getAdmUser(old.getAdmUserByPpmCurrentUser());
						userName = user.getCompleteName();
						posisiOld = user.getAdmEmployee().getEmpStructuralPos();
					}
					old.setPengawasPp(prcMainHeader.getPengawasPp());
				}
			}
			
			saveEntity(old);
			
			//delete dockumen if requested
			if(listPrcMainDoc!=null && listPrcMainDoc.size()>0){
				for(PrcMainDoc d: listPrcMainDoc){
					if(d.getUpdatedBy().equalsIgnoreCase("1")){
						DetachedCriteria dr = DetachedCriteria.forClass(PrcMainDoc.class);
						dr.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
						dr.add(Restrictions.idEq(d.getId()));
						PrcMainDoc dd = (PrcMainDoc) getEntityByDetachedCiteria(dr);
						if(dd!=null)
							deleteEntity(dd);
					}
				}
			}
			
			//upload dokumen
			if(SessionGetter.isNotNull(ppdDocument)){
				int i = 0;
				for(File f:ppdDocument){
					PrcMainDoc doc = new PrcMainDoc();
					doc.setPrcMainHeader(prcMainHeader);
					doc.setDocCategory(ppdCategory.get(i));
					doc.setDocName(ppdDescription.get(i));
					doc.setDocPath(UploadUtil.uploadDocsFile(f, ppdDocumentFileName.get(i)));
					saveEntity(doc);
					i++;
				}
			}
			
			
			//insert History
			//upload dokumen
			if(SessionGetter.isNotNull(docsKomentar)){
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}
			
			//insert history
			HistMainComment last = SessionGetter.getLastCommentByHeader(prcMainHeader);
			HistMainComment tmps = SessionGetter.getLastCommentByHeader(prcMainHeader);
			
			//old stuff
			Date crDate = tmps.getCreatedDate();
			String user = tmps.getUsername();
			String posisi = tmps.getPosisi();
			String aksi = tmps.getAksi();
			String prosesAksi = tmps.getProsesAksi();
			
			//next user
			last.setPrcMainHeader(prcMainHeader);
			last.setUsername(SessionGetter.getUser().getCompleteName());
			last.setPosisi(SessionGetter.getUser().getAdmEmployee().getEmpStructuralPos());
			last.setProses(prcMainHeader.getAdmProses().getId());
			last.setAksi("Update SPPH");
			last.setProsesAksi("Update SPPH");
			last.setDocument(histMainComment.getDocument());
			last.setComment(histMainComment.getComment());
			last.setCreatedDate(new Date());
			last.setUpdatedDate(new Date());
			updateEntity(last);
			
			//save old stuff
			HistMainComment oldes = new HistMainComment();
			oldes.setAksi(aksi);
			oldes.setProsesAksi(prosesAksi);
			if(userName!="" && userName.length()>0){
				oldes.setPosisi(posisiOld);
				oldes.setUsername(userName);
			}else{
				oldes.setPosisi(posisi);
				oldes.setUsername(user);
			}
			oldes.setPrcMainHeader(prcMainHeader);
			oldes.setCreatedDate(crDate);
			saveEntity(oldes);
			
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			e.printStackTrace();
			throw e;
		}
		
	}

	@Override
	public PanitiaPengadaan savePanitiaHeader(PanitiaPengadaan panitia, PrcMainHeader mainHeader)
			throws Exception {
		try {
			startTransaction();
			saveEntity(panitia);
			
			PrcMainHeader old = getPrcMainIHeaderById(mainHeader.getPpmId());
			old.setPanitia(panitia);
			saveEntity(old);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
		return panitia;
	}

	@Override
	public void savePanitiaDetail(PanitiaPengadaanDetail panitiaDetail)
			throws Exception {
		try {
			startTransaction();
			saveEntity(panitiaDetail);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public void deletePanitiaDetail(PanitiaPengadaanDetail panitiaDetail)
			throws Exception {
		try {
			startTransaction();
			String query = "delete from panitia_pengadaan_detail where id="+panitiaDetail.getId();
			Query q = session.createSQLQuery(query);
			q.executeUpdate();
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
		
	}
	
	
	
	//THIS METHOD IS NO LONGER USED. REMOVE OR TAKE IT OUT
	@SuppressWarnings("rawtypes")
	@Override
	public void generateAnggaran(PrcMainHeader prcMainHeader) throws Exception {
		try {
			startTransaction();
			PrcMainHeader mainHeader = getPrcMainIHeaderById(prcMainHeader.getPpmId());
			List lst = JdeService.generateAnggaranFromON(mainHeader,session);
			
			if(lst!=null && lst.size()>0){
				Iterator it = lst.iterator();
				int i =0 ;
				PrcAnggaranJdeHeader jdeHeader = getJdeAnggaranHeaderByPpmId(mainHeader);
				if(jdeHeader==null){
					jdeHeader = new PrcAnggaranJdeHeader();
				}
				while(it.hasNext()){
					Object o[] = (Object[])it.next();
					it.remove();
					String KAID = String.valueOf(o[0]);
					BigDecimal nilai = new BigDecimal(String.valueOf(o[1])).divide(new BigDecimal("100"));
					
					if(i==0){
						//retrive prcMainHeader);
						OrHeader orh = getOrHeaderById(mainHeader); 
						String tmp [] = mainHeader.getPpmNomorPr().trim().split("ZMN");
						
						jdeHeader.setPrcMainHeader(mainHeader);
						jdeHeader.setKakcoo(tmp[1].trim());
						jdeHeader.setKadcto("KA");
						jdeHeader.setKaaid("");
						jdeHeader.setKatrdj(new Date());
						jdeHeader.setKaapdj("0");
						jdeHeader.setKaco(tmp[1].trim());
						jdeHeader.setKakco(tmp[1].trim());
						jdeHeader.setKadct("ZMN");
						jdeHeader.setKadoc(Long.valueOf(tmp[0].trim()));//dokumen awal
						if(orh!=null)
							if(orh.getTglOr()!=null)
								jdeHeader.setKabasedj(JulianToGregorianViceVersa.getJulian7FromDate(orh.getTglOr()));//tgl dokumen awal OR
							else
								jdeHeader.setKabasedj("0");//tgl dokumen awal OR
						jdeHeader.setKamcu(tmp[1].trim());
						jdeHeader.setKammcu(tmp[1].trim());
						jdeHeader.setKaan8("0");//kode koordinator anggaran
						jdeHeader.setKaan82("0");//kode penandatangan KA
						if(mainHeader.getPpmSubject().length()>=30){
							jdeHeader.setKadsc1(mainHeader.getPpmSubject().substring(0, 29));
						}else
							jdeHeader.setKadsc1(mainHeader.getPpmSubject().trim());
						
						if(mainHeader.getPpmJenis()!=null && mainHeader.getPpmJenis()== new AdmJenis(2))
							jdeHeader.setKajj("");//jenis jasa
						jdeHeader.setKaapfl("");//status anggaran
						jdeHeader.setKaska("02");//status penanda tanganan
						jdeHeader.setKahst("");
						jdeHeader.setKaaap(0);
						jdeHeader.setKaea(mainHeader.getPpmJumlahOe());
						jdeHeader.setKaacm1(mainHeader.getPpmJumlahOe());
						jdeHeader.setKaaa(new BigDecimal("0"));
//						jdeHeader.setKatorg(SessionGetter.getUser().getUsername().toUpperCase());//ambil dari JDE
//						jdeHeader.setKauser(SessionGetter.getUser().getUsername().toUpperCase());
						jdeHeader.setKatorg(SessionGetter.getUser().getUsername().toUpperCase());//ambil dari JDE
						jdeHeader.setKauser(SessionGetter.getUser().getUsername().toUpperCase());
						jdeHeader.setKajobn("MUSI");//komputer
						jdeHeader.setKupmj(new Date());
						jdeHeader.setKupmt(new Date());
						jdeHeader.setKapid("");
						saveEntity(jdeHeader);
					}
					PrcAnggaranJdeDetail anggaranJdeDetail = new PrcAnggaranJdeDetail();
					anggaranJdeDetail.setRkaid(KAID.trim());
					anggaranJdeDetail.setRkacm1(nilai);
					anggaranJdeDetail.setKeterangan(JdeService.getRekeningAnggaran(KAID));
					anggaranJdeDetail.setPrcAnggaranJdeHeader(jdeHeader);
					anggaranJdeDetail.setRkaap(new BigDecimal(0));
					anggaranJdeDetail.setRkaa(new BigDecimal(0));
					anggaranJdeDetail.setRkaea(new BigDecimal(0));
					anggaranJdeDetail.setRkdl01("");
					anggaranJdeDetail.setRkdl02("");
					anggaranJdeDetail.setRktorg("");
					anggaranJdeDetail.setRkuser(SessionGetter.getUser().getUsername().toUpperCase().trim());
					anggaranJdeDetail.setRkjobn("MUSI");
					anggaranJdeDetail.setRkapid("");
					if(mainHeader.getAdmProses().getId()!=SessionGetter.ANGGARAN_PROSES_ID.getId()){
						anggaranJdeDetail.setStatus("R");
					}
					anggaranJdeDetail.setPrcMainHeader(mainHeader);
					saveEntity(anggaranJdeDetail);
					
					i++;
				}
			}
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public void saveRevisiEEInit(PrcMainHeader prcMainHeader, List<PrcMainItem> listPrcMainItem,
			HistMainComment histMainComment, File docsKomentar, String docsKomentarFileName) throws Exception {
		try {
			startTransaction();
			PrcMainHeader tmp = prcMainHeader;
			prcMainHeader = getPrcMainIHeaderById(prcMainHeader.getPpmId());
			prcMainHeader.setPpmJumlahOe(SessionGetter.getEeFromItems(listPrcMainItem));
			prcMainHeader.setLokasiPenyerahan(tmp.getLokasiPenyerahan());
			prcMainHeader.setPpmProsesLevel(1);
			prcMainHeader.setAdmProses(SessionGetter.REVISI_EE);
			AdmProsesHirarki ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel(), prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahOe().doubleValue());
			if(ahp==null){
				throw new Exception("Kesalahan dalam Hirarki Proses");
			}else{
				if(ahp.getAdmRoleByCurrentPos()!=null){
					prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
					prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
					//get Kordang By Satker Peminta
					if(prcMainHeader.getSatkerPeminta()!=null){
						AdmDept dept = new AdmServiceImpl().getAdmDeptById(prcMainHeader.getSatkerPeminta());
						if(dept!=null && dept.getKordang()!=null){
							prcMainHeader.setAdmUserByPpmCurrentUser(new AdmServiceImpl().getAdmUser(dept.getKordang()));
						}else{
							throw new Exception("Kordang Satuan Kerja "+dept.getDeptName()+" , belum di set");
						}
					}else{
						throw new Exception("Satuan Kerja Peminta Harus diisi");
					}
					
				}
			}
			
			saveEntity(prcMainHeader);
			
			Double jumlahOe =0.0;
			//update item
			if(listPrcMainItem!=null && listPrcMainItem.size()>0){
				for(PrcMainItem item:listPrcMainItem){
					saveEntity(item);
					BigDecimal total = item.getItemQuantityOe().multiply(item.getItemPriceOe());
					jumlahOe = jumlahOe+total.doubleValue();
				}
			}
			
			//update jumlah OE
			prcMainHeader.setPpmJumlahOe(new BigDecimal(jumlahOe));
			saveEntity(prcMainHeader);
			
			
			//upload dokumen
			if(SessionGetter.isNotNull(docsKomentar)){
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}
			IntegrationService integrationService = new IntegrationServiceImpl();
			//Validate Anggaran
			ResultDto result = integrationService.changePr(listPrcMainItem,prcMainHeader);
			if(result.getStatus()!=200) {
				throw new Exception("Terjadi kesalahan saat proses checking anggaran ");
			}
			
			//insert history
			SessionGetter.saveHistoryGlobalPrOE(histMainComment, prcMainHeader, session);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
		
	}

	@Override
	public void saveEEFinis(PrcMainHeader prcMainHeader, List<PrcMainItem> listPrcMainItem,
			HistMainComment histMainComment, File docsKomentar, String docsKomentarFileName) throws Exception {
		try {
			startTransaction();
			prcMainHeader = getPrcMainIHeaderById(prcMainHeader.getPpmId());
			Double jumlahEe =0.0;
			//update item ee
			if(listPrcMainItem!=null && listPrcMainItem.size()>0){
				for(PrcMainItem item:listPrcMainItem){
					saveEntity(item);
					BigDecimal total = item.getItemQuantity().multiply(item.getItemPrice());
					jumlahEe = jumlahEe+total.doubleValue();
				}
			}
			
			//update jumlah EE
			prcMainHeader.setPpmJumlahEe(new BigDecimal(jumlahEe));
			
			AdmProsesHirarki ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel()+1, prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahEe().doubleValue());
			if(ahp==null){
				//larikan ke Pembuatan OE
				prcMainHeader.setAdmProses(SessionGetter.OE_PROSES_ID);
				prcMainHeader.setPpmProsesLevel(2);
				prcMainHeader.setAdmUserByPpmCurrentUser(new AdmServiceImpl().getAdmUser(prcMainHeader.getAdmUserByPpmEstimator()));
				histMainComment.setCreatedBy("Pembuatan OE");
			}else{
				if(ahp.getAdmRoleByCurrentPos()!=null){
					if(StringUtils.equalsIgnoreCase(ahp.getAdmRoleByCurrentPos().getRoleName(), "DIR TERKAIT")){
						AdmDept satkerPeminta = new AdmServiceImpl().getAdmDeptById(prcMainHeader.getSatkerPeminta());
						if(satkerPeminta!=null){
							prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
							prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
							if(satkerPeminta.getDirektur()!=null){
								prcMainHeader.setAdmUserByPpmCurrentUser(SessionGetter.getEmployeeByRole(satkerPeminta.getDirektur()).getAdmUser());
							}else{
								throw new Exception("Direktur Untuk Satuan Kerja Peminta "+satkerPeminta.getDeptName()+" , Belum di Set, Silahkan di Set di Menu Satuan Kerja");
							}
						}else{
							throw new Exception(" Satuan Kerja Peminta Null , Belum di Set. Silakan di Set di Update Tools");
						}
					}else{
						prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
						prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
						prcMainHeader.setAdmUserByPpmCurrentUser(SessionGetter.getEmployeeByRole(ahp.getAdmRoleByCurrentPos()).getAdmUser());
					}
				}
			}
			
			saveEntity(prcMainHeader);
		
			//upload dokumen
			if(SessionGetter.isNotNull(docsKomentar)){
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}
			
			//insert history
			SessionGetter.saveHistoryGlobalPrOE(histMainComment, prcMainHeader, session);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public void revisiEEApproval(PrcMainHeader prcMainHeader, List<PrcMainItem> listPrcMainItem,
			HistMainComment histMainComment, File docsKomentar, String docsKomentarFileName) throws Exception {
		try {
			startTransaction();
			prcMainHeader = getPrcMainIHeaderById(prcMainHeader.getPpmId());
			AdmDept satkerPeminta = new AdmServiceImpl().getAdmDeptById(prcMainHeader.getSatkerPeminta());
			prcMainHeader.setAdmUserByPpmCurrentUser(new AdmServiceImpl().getAdmUser(satkerPeminta.getKordang()));
			prcMainHeader.setPpmProsesLevel(1);
			AdmProsesHirarki ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(), 1, prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahOe().doubleValue());
			prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
			saveEntity(prcMainHeader);
			
			//upload dokumen
			if(SessionGetter.isNotNull(docsKomentar)){
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}
			
			//insert history
			SessionGetter.saveHistoryGlobalPrOE(histMainComment, prcMainHeader, session);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
		
	}

	@Override
	public void setujuEEApproval(PrcMainHeader prcMainHeader, List<PrcMainItem> listPrcMainItem,
			HistMainComment histMainComment, File docsKomentar, String docsKomentarFileName) throws Exception {
		try {
			startTransaction();
			prcMainHeader = getPrcMainIHeaderById(prcMainHeader.getPpmId());
			
			AdmProsesHirarki ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel()+1, prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahEe().doubleValue());
			if(ahp==null){
				//larikan ke Pembuatan OE
				prcMainHeader.setAdmProses(SessionGetter.OE_PROSES_ID);
				prcMainHeader.setPpmProsesLevel(2);
				prcMainHeader.setAdmUserByPpmCurrentUser(new AdmServiceImpl().getAdmUser(prcMainHeader.getAdmUserByPpmEstimator()));
				histMainComment.setCreatedBy("Pembuatan OE");
			}else{
				if(ahp.getAdmRoleByCurrentPos()!=null){
					if(StringUtils.equalsIgnoreCase(ahp.getAdmRoleByCurrentPos().getRoleName(), "DIR TERKAIT")){
						AdmDept satkerPeminta = new AdmServiceImpl().getAdmDeptById(prcMainHeader.getSatkerPeminta());
						if(satkerPeminta!=null){
							prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
							prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
							if(satkerPeminta.getDirektur()!=null){
								prcMainHeader.setAdmUserByPpmCurrentUser(SessionGetter.getEmployeeByRole(satkerPeminta.getDirektur()).getAdmUser());
							}else{
								throw new Exception("Direktur Untuk Satuan Kerja Peminta "+satkerPeminta.getDeptName()+" , Belum di Set, Silahkan di Set di Menu Satuan Kerja");
							}
						}else{
							throw new Exception(" Satuan Kerja Peminta Null , Belum di Set. Silakan di Set di Update Tools");
						}
					}else{
						prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
						prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
						prcMainHeader.setAdmUserByPpmCurrentUser(SessionGetter.getEmployeeByRole(ahp.getAdmRoleByCurrentPos()).getAdmUser());
					}
				}
			}
			
			saveEntity(prcMainHeader);
			
			
			//upload dokumen
			if(SessionGetter.isNotNull(docsKomentar)){
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}
			
			//insert history
			SessionGetter.saveHistoryGlobalPrOE(histMainComment, prcMainHeader, session);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
		
	}

	@Override
	public void relokasiAnggaran(PrcMainHeader prcMainHeader, HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception {
		try {
			startTransaction();
			prcMainHeader = getPrcMainIHeaderById(prcMainHeader.getPpmId());
			AdmDept satkerPeminta = new AdmServiceImpl().getAdmDeptById(prcMainHeader.getSatkerPeminta());
			prcMainHeader.setAdmUserByPpmCurrentUser(new AdmServiceImpl().getAdmUser(satkerPeminta.getKordang()));
			prcMainHeader.setAdmProses(SessionGetter.REALOKASI);
			prcMainHeader.setPpmProsesLevel(1);
			AdmProsesHirarki ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(), 1, new AdmDistrict(104), prcMainHeader.getPpmJumlahOe().doubleValue());
			prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
			saveEntity(prcMainHeader);
			
			//upload dokumen
			if(SessionGetter.isNotNull(docsKomentar)){
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}
			
			//insert history
			SessionGetter.saveHistoryGlobalPrOE(histMainComment, prcMainHeader, session);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public void saveRelokasiAnggaran(PrcMainHeader prcMainHeader, HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception {
		try {
			startTransaction();
			prcMainHeader = getPrcMainIHeaderById(prcMainHeader.getPpmId());
			AdmProses proses = SessionGetter.ANGGARAN_PROSES_ID;//ke Anggaran
			prcMainHeader.setAdmProses(proses);
			prcMainHeader.setPpmProsesLevel(1);
			//cek jika panjang, palembang or baturaja
			AdmDistrict kantor = null;
			if(prcMainHeader.getPpmNomorPrSap().contains("1201")){//palembang
				kantor = new AdmDistrict(104);
				kantor = new AdmServiceImpl().getDistrictById(kantor);
			}else if(prcMainHeader.getPpmNomorPrSap().contains("1202")){//baturaja
				kantor = new AdmDistrict(107);
				kantor = new AdmServiceImpl().getDistrictById(kantor);
				if(prcMainHeader.getPpmJumlahOe().doubleValue()>kantor.getLimit().doubleValue()){
					kantor = kantor.getDistrict();//ambil parentnya aka ke palembang
				}
			}else if(prcMainHeader.getPpmNomorPrSap().contains("1204")){//panjang
				kantor = new AdmDistrict(110);
				kantor = new AdmServiceImpl().getDistrictById(kantor);
				if(prcMainHeader.getPpmJumlahOe().doubleValue()>kantor.getLimit().doubleValue()){
					kantor = kantor.getDistrict();//ambil parentnya aka ke palembang
				}
			}else if(prcMainHeader.getPpmNomorPrSap().contains("1203")){//baturaja 2
				kantor = new AdmDistrict(111);
				kantor = new AdmServiceImpl().getDistrictById(kantor);
				kantor = kantor.getDistrict();//ambil parentnya aka ke palembang
			}
			
			prcMainHeader.setDistrictKeuangan(kantor);
			
			//cek kewenangan
			
			AdmProsesHirarki ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(), 1, kantor, prcMainHeader.getPpmJumlahOe().doubleValue());
			if(ahp==null){
				throw new Exception("Kesalahan dalam hirarki");
			}
			
			//get mapping user
			AdmUser us = getMappingUserKeuangan(kantor);
			if(us==null){
				throw new Exception("User Keuangan Belum di mapping");
			}else{
				prcMainHeader.setAdmUserByPpmCurrentUser(new AdmServiceImpl().getAdmUser(us));
				histMainComment.setCreatedBy(SessionGetter.getPropertiesValue("aa.1", null));
			}
			saveEntity(prcMainHeader);
			
			//upload dokumen
			if(SessionGetter.isNotNull(docsKomentar)){
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}
			
			//insert history
			SessionGetter.saveHistoryGlobalPrOE(histMainComment, prcMainHeader, session);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}
	
	@Override
	public AdmProsesHirarki getHirarkiProsesJenis(PrcMainHeader prcMainHeader) throws Exception {
		try{
			AdmProsesHirarki ahp = new AdmProsesHirarki();
			if(prcMainHeader.getPpmJenis().getId()== 1){
				ahp = SessionGetter.getHirarkiProsesByJenis(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel()+1, prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahOe().doubleValue(), 1);
			}else if(prcMainHeader.getPpmJenis().getId()==2){
				ahp = SessionGetter.getHirarkiProsesByJenis(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel()+1, prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahOe().doubleValue(), 2);
			}else{
				throw new Exception("Jenis OR tidak terdaftar");
			}
//			if(prcMainHeader.getPpmJenis() == 1){
//				ahp = SessionGetter.getHirarkiProsesByJenis(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel()+1, prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahOe().doubleValue(), 1);
//			}else if(prcMainHeader.getPpmJenis()== 2){
//				ahp = SessionGetter.getHirarkiProsesByJenis(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel()+1, prcMainHeader.getAdmDistrict(), prcMainHeader.getPpmJumlahOe().doubleValue(), 2);
//			}else{
//				throw new Exception("Jenis OR tidak terdaftar");
//			}

			if(ahp==null){
				return null;
			}else{
				return ahp;
			}
			
		}catch(Exception e){
			throw e;
		}
		
		
	}
	
	@Override
	public List<AdmJenis> getListAdmJenisByParent(AdmJenis jenis){
		DetachedCriteria crit = DetachedCriteria.forClass(AdmJenis.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.and(Restrictions.ne("id", 1), Restrictions.ne("id", 2)));
		crit.add(Restrictions.eq("parentId", jenis));
		return (List<AdmJenis>) getListByDetachedCiteria(crit);
	}
	
	@Override
	public void pengadaanManual(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, List<File> prcManDocument,
			List<String> prcManDocumentCategory, List<String> prcManDocumentDescription,
			List<String> prcManDocumentFileName, File docsKomentar,
			String docsKomentarFileName) throws Exception {
		try {
			startTransaction();
			PrcMainHeader ph = getPrcMainIHeaderById(prcMainHeader.getPpmId());
			ph.setPpmProsesLevel(-100);//pengadaan manual
			ph.setAdmUserByPpmCurrentUser(null);
			ph.setAdmRole(null);
			saveEntity(ph);
			
			//upload dokumen
			if(SessionGetter.isNotNull(prcManDocument)){
				int i = 0;
				for(File f:prcManDocument){
					PrcManualDoc doc = new PrcManualDoc();
					doc.setPrcMainHeader(prcMainHeader);
					doc.setDocCategory(prcManDocumentCategory.get(i));
					doc.setDocName(prcManDocumentDescription.get(i));
					doc.setDocPath(UploadUtil.uploadDocsFile(f, prcManDocumentFileName.get(i)));
					saveEntity(doc);
					i++;
				}
			}
			
			//insert history
			HistMainComment comment = new HistMainComment();
			comment.setAksi("Pengadaan Manual");
			comment.setComment(histMainComment.getComment());
			comment.setPosisi(SessionGetter.getUser().getAdmEmployee().getEmpStructuralPos());
			comment.setPrcMainHeader(ph);
			comment.setProses(null);
			comment.setProsesAksi("Pengadaan Manual");
			comment.setUpdatedDate(new Date());
			comment.setUsername(SessionGetter.getUser().getCompleteName());;
			comment.setDocument(histMainComment.getDocument());
			saveEntity(comment);
			
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			e.printStackTrace();
			throw e;
		}
		
	}

}
