package eproc.controller;

import java.util.List;

import com.orm.model.ComCatalog;
import com.orm.model.ComGroup;

public interface ComService {
	public List<ComCatalog> listComCatalog();

	public List<ComGroup> listComGroup(Object object) throws Exception;
}
