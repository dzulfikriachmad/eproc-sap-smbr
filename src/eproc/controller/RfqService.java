package eproc.controller;

import java.io.File;
import java.util.List;

import com.eproc.sap.dto.ResultDto;
import com.eproc.x.rfq.command.MainMasterJdeCommand;
import com.eproc.x.rfq.command.TabulasiPenilaian;
import com.jde.model.PODetail;
import com.jde.model.POHeader;
import com.jdedwards.system.connector.dynamic.UserSession;
import com.orm.model.AdmProsesHirarki;
import com.orm.model.AdmPurchasingOrganization;
import com.orm.model.CtrContractComment;
import com.orm.model.CtrContractHeader;
import com.orm.model.CtrContractItem;
import com.orm.model.CtrContractPb;
import com.orm.model.CtrContractTop;
import com.orm.model.HistMainComment;
import com.orm.model.HistoryHargaNegosiasi;
import com.orm.model.PrcMainHeader;
import com.orm.model.PrcMainItem;
import com.orm.model.PrcMainPreparation;
import com.orm.model.PrcMainVendor;
import com.orm.model.PrcMainVendorId;
import com.orm.model.PrcMainVendorQuote;
import com.orm.model.PrcMainVendorQuoteAdmtech;
import com.orm.model.PrcMsgNegotiation;

public interface RfqService {

	List<PrcMainHeader> listPrcMainHeaderUndangVendor(Object object) throws Exception;
	PrcMainHeader getPrcMainHeaderSPPHById(Long ppmId) throws Exception;
	void saveUndangVendor(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName, List<PrcMainVendor> listVendor) throws Exception;
	PrcMainVendor getPrcMainVendorById(PrcMainVendorId id) throws Exception;
	
	/** Aan wijzing **/
	void saveAanwijzing(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName, List<PrcMainVendor> listVendor) throws Exception;
	List<PrcMainHeader> listPrcMainHeaderAanwijzing(Object object) throws Exception;
	
	/** Bid Opening **/
	void saveVerifikasiAdm(PrcMainVendorQuote quote,
			List<PrcMainVendorQuoteAdmtech> listAdministrasi,
			PrcMainVendor prcMainVendor) throws Exception;
	List<PrcMainHeader> listPrcMainHeaderOpening(Object object) throws Exception;
	void saveOpening(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception;
	List<PrcMainHeader> listPrcMainHeaderEvaluasi(Object object) throws Exception;
	List<TabulasiPenilaian> getListTabulasi(String tabulasiEvaluasi) throws Exception;
	void saveVerifikasiTeknis(PrcMainVendorQuote quote,
			List<PrcMainVendorQuoteAdmtech> listTeknis,
			PrcMainVendor prcMainVendor) throws Exception;
	void saveVerifikasiHarga(List<PrcMainVendor> listPrcMainVendor) throws Exception;
	void saveEvaluasi(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception;
	void saveEvaluasiSampul2(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName, List<PrcMainVendor> listVendorLulus, 
			List<PrcMainVendor> listVendorTidakLulus) throws Exception;
	List<PrcMsgNegotiation> listNegosiasiByMainHeader(
			PrcMainHeader prcMainHeader) throws Exception;
	List<PrcMainHeader> listPrcMainHeaderNegosiasi(Object object) throws Exception;
	void saveNegosiasi(PrcMainVendor prcMainVendor, PrcMsgNegotiation negosiasi) throws Exception;
	void saveNegosiasi(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception;
	List<PrcMainHeader> listPrcMainHeaderUsulanPemenang(Object object) throws Exception;
	void saveUsulanPemenang(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName, List<PrcMainItem> listPrcMainItem) throws Exception;
	void savePersetujuanPemenang(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception;
	void savePersetujuanPemenangPanitia(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception;
	List<PrcMainHeader> listPrcMainHeaderPersetujuanUsulanPemenang(Object object) throws Exception;
	List<PrcMainHeader> listPrcMainHeaderPersetujuanUsulanPemenang2(Object object) throws Exception;
	void revisiPemenang(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName, List<PrcMainItem> listPrcMainItem) throws Exception;
	void finalisasiPemenang(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName, List<PrcMainItem> listPrcMainItem) throws Exception;
	List<CtrContractHeader> listContractPemilihanPembuatKontrak(Object object) throws Exception;
	CtrContractHeader getContractHeaderById(CtrContractHeader contractHeader) throws Exception;
	List<CtrContractItem> listContractItemByHeader(
			CtrContractHeader contractHeader) throws Exception;
	List<CtrContractComment> listCommentByContractHeader(
			CtrContractHeader contractHeader) throws Exception;
	List<CtrContractPb> listContractPbByHeader(CtrContractHeader contractHeader) throws Exception;
	List<CtrContractTop> listContractTopByHeader(
			CtrContractHeader contractHeader) throws Exception;
	void savePemilihanPembuatKontrak(CtrContractHeader contractHeader,
			CtrContractComment ctrContractComment, File docsKomentar,
			String docsKomentarFileName) throws Exception;
	CtrContractTop getTopItemById(int id) throws Exception;
	void deleteTopItem(Object topItemById) throws Exception;
	CtrContractPb getPbItemById(int id) throws Exception;
	void deletePbItem(CtrContractPb pbItemById) throws Exception;
	List<CtrContractHeader> listContractPembuatanDraftKontrak(Object object) throws Exception;
	void saveDraftKontrak(CtrContractHeader contractHeader,
			CtrContractComment ctrContractComment, File docsKomentar,
			String docsKomentarFileName, List<CtrContractTop> listTop,
			List<CtrContractPb> listPb) throws Exception;
	void saveDraftKontrakLegal(CtrContractHeader contractHeader,
			CtrContractComment ctrContractComment, File docsKomentar,
			String docsKomentarFileName, List<CtrContractTop> listTop,
			List<CtrContractPb> listPb) throws Exception;
	void saveApprovalKontrak(CtrContractHeader contractHeader,
			CtrContractComment ctrContractComment, File docsKomentar,
			String docsKomentarFileName) throws Exception;
	List<CtrContractHeader> listContractApprovalDraftKontrak(Object object) throws Exception;
	void saveRevisiKontrak(CtrContractHeader contractHeader,
			CtrContractComment ctrContractComment, File docsKomentar,
			String docsKomentarFileName) throws Exception;
	void rebidPengadaan(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName, List<PrcMainVendor> listVendor) throws Exception;
	void revisiPengadaan(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName, List<PrcMainVendor> listVendor) throws Exception;
	void saveSetujuKontrakLegal(CtrContractHeader contractHeader,
			CtrContractComment ctrContractComment, File docsKomentar,
			String docsKomentarFileName) throws Exception;
	void createAdendum(CtrContractHeader contractHeaderAdendum,
			CtrContractHeader contractHeader,
			List<CtrContractItem> listContractItem,
			CtrContractComment ctrContractComment) throws Exception;
	List<MainMasterJdeCommand> listJdeMasterMain(Object object) throws Exception;
	void savePOJde(POHeader poHeader, List<PODetail> listPoDetail,
			CtrContractHeader contractHeader,Integer sessionID, UserSession userSession) throws Exception;
	String getDeskripsiAnggaran(String kode) throws Exception;
	List<MainMasterJdeCommand> listJdeMasterAnggaran(Integer tahunAnggaran,
			String kodeAnggaran) throws Exception;
	POHeader setPoHeaderByOr(CtrContractHeader contractHeader, String orderType) throws Exception;
	List<PODetail> getListPoDetail(POHeader poHeader, String orderType) throws Exception;
	Integer countMessage(Integer status) throws Exception;
	void updateTanggalKontrak(CtrContractHeader contractHeader) throws Exception;
	void saveCloseKontrak(CtrContractHeader contractHeader,
			CtrContractComment ctrContractComment, File docsKomentar,
			String docsKomentarFileName) throws Exception;
	List<HistoryHargaNegosiasi> listHargaNegosiasi(PrcMainHeader prcMainHeader) throws Exception;
	void saveEvaluasiSampul1(PrcMainHeader prcMainHeader, HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName, PrcMainPreparation preparation, List<PrcMainVendor> listVendorLulus, List<PrcMainVendor> listVendorTidakLulus) throws Exception;
	List<PrcMainVendor> listVndUsulanPemenang(PrcMainHeader prcMainHeader, double jumlahOe)
			throws Exception;
	public void sendEmailToVendor(PrcMainHeader prcMainHeader) throws Exception;
	AdmProsesHirarki getHirarkiProsesJenisKontrak(CtrContractHeader ctrContractHeader) throws Exception;
	AdmProsesHirarki getHirarkiProsesJenisByDistrict(PrcMainHeader prcMainHeader) throws Exception;
	CtrContractHeader getContractHeaderForSAP(CtrContractHeader contractHeader) throws Exception;
	void saveContractNumberSap(CtrContractHeader contractHeader, String contractNumber) throws Exception;
	List<AdmPurchasingOrganization> listAdmPurchOrgByPlant(String kodePlant) throws Exception;
	ResultDto saveContractSap(CtrContractHeader ctrContractHeader, List<CtrContractItem> listContractItem);
	AdmPurchasingOrganization getAdmPurchOrgByKode(String kodePurchOrg, String kodePlant);
	public PrcMainHeader getPrcMainHeaderSPPHByPpmId(Long ppmId) throws Exception;
}
