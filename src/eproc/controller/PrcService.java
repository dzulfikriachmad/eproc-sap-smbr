package eproc.controller;

import java.io.File;
import java.util.List;

import com.eproc.x.global.command.TodoUserCommand;
import com.orm.model.AdmJenis;
import com.orm.model.AdmProses;
import com.orm.model.AdmProsesHirarki;
import com.orm.model.AdmRole;
import com.orm.model.AdmUser;
import com.orm.model.ComGroup;
import com.orm.model.HistMainComment;
import com.orm.model.PanitiaPengadaan;
import com.orm.model.PanitiaPengadaanDetail;
import com.orm.model.PrcAnggaranJdeDetail;
import com.orm.model.PrcMainDoc;
import com.orm.model.PrcMainHeader;
import com.orm.model.PrcMainItem;
import com.orm.model.PrcMainPreparation;
import com.orm.model.PrcMainVendor;
import com.orm.model.PrcMainVendorId;
import com.orm.model.PrcMainVendorQuote;
import com.orm.model.PrcMainVendorQuoteAdmtech;
import com.orm.model.PrcMainVendorQuoteItem;
import com.orm.model.PrcManualDoc;
import com.orm.model.PrcMethod;
import com.orm.model.PrcMsgChat;
import com.orm.model.PrcTemplate;
import com.orm.model.VndHeader;

public interface PrcService {
	public List<PrcMainItem> listPrcMainItemByHeader(PrcMainHeader prcMainHeader);
	public List<PrcMainHeader> listPrcMainHeader();
	public List<PrcMainHeader> listPrcMainHeaderByCreator(AdmUser user);
	public List<PrcMainHeader> listPrcMainHeaderApproval(AdmRole role);
	public List<PrcMainHeader> listPrcMainHeaderOe(AdmRole role, int tingkat);
	public List<PrcMainHeader> listPrcMainHeaderOE(Object obj) throws Exception;
	public List<PrcMainHeader> listPrcMainHeaderEstimator(AdmUser user);
	public List<PrcMainHeader> listPrcMainHeaderToDo(AdmRole role);
	public List<PanitiaPengadaanDetail> getListPanitiaPengadaanDetail(PanitiaPengadaan o);
	public PanitiaPengadaan getPanitiaPengadaan(PanitiaPengadaan o);
	public List<HistMainComment> listHistMainCommentByPrcHeader(PrcMainHeader prcMainHeader);
	List<ComGroup> getListComGrup(Object object) throws Exception;
	
	public Long saveOrUpdatePrcMainHeader(PrcMainHeader prcMainHeader);
	public boolean saveOrUpdatePrcMainItem(PrcMainItem prcMainItem);
	
	public boolean deletePrcMainItem(PrcMainItem prcMainItem);
	public boolean saveOrUpdateHistMainComment(HistMainComment histMainComment);
	
	public PrcMainItem getPrcMainItemById(long id);
	public PrcMainHeader getPrcMainIHeaderById(long id);
	
	public AdmProsesHirarki getAdmProsesHirarki(AdmProses proses, AdmRole role);
	public AdmProsesHirarki getAdmProsesHirarki(AdmProses proses, int tingkat);
	public PrcMainHeader savePR(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, List<PrcMainItem> listPrcMainItem) throws Exception;
	public PrcMainHeader savePRFinish(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, List<PrcMainItem> listPrcMainItem) throws Exception;
	public List<PrcMainHeader> listPrcMainHeaderByCurrentUser(AdmUser user) throws Exception;
	public void revisiPR(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception;
	public void setujuPR(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception;
	public List<AdmUser> listEstimator(Object object) throws Exception;
	public void saveEstimator(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception;
	public void saveOeItems(List<PrcMainItem> listPrcMainItem) throws Exception;
	public void saveOeFinis(PrcMainHeader prcMainHeader,
			List<PrcMainItem> listPrcMainItem, HistMainComment histMainComment,
			File docsKomentar, String docsKomentarFileName) throws Exception;
	public void revisiOE(PrcMainHeader prcMainHeader,
			List<PrcMainItem> listPrcMainItem, HistMainComment histMainComment,
			File docsKomentar, String docsKomentarFileName) throws Exception;
	public void setujuOE(PrcMainHeader prcMainHeader,
			List<PrcMainItem> listPrcMainItem, HistMainComment histMainComment,
			File docsKomentar, String docsKomentarFileName) throws Exception;
	public List<PrcMainHeader> listPrcMainHeaderApprovalOE(Object object) throws Exception;
	
	/** Persiapan Pengadaan **/
	public List<AdmUser> listUserByProsesLevel(PrcMainHeader prcMainHeader) throws Exception;
	public List<PrcMainHeader> listPrcMainHeaderDisposisiPp(Object object) throws Exception;
	public void saveDisposisiPP(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception;
	public List<PrcMainHeader> listPrcMainHeaderPpSetup(AdmUser user) throws Exception;
	public List<PrcMethod> listMetode(Object object) throws Exception;
	public void savePpSetup(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName, PanitiaPengadaan panitiaPengadaan, List<PanitiaPengadaanDetail> listPanitiaDetail) throws Exception;
	public List<VndHeader> listMitraKerjaDiUndang(PrcMainHeader prcMainHeader) throws Exception;
	public List<PrcMainHeader> listPrcMainHeaderPpVendor(Object object) throws Exception;
	public PrcMainHeader getPrcMainHeaderSPPHById(Long ppmId) throws Exception;
	public List<PrcTemplate> listTemplate(Object obj) throws Exception;
	public void savePpMitraKerja(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment,
			List<PrcMainVendor> listPrcMainVendor, File docsKomentar,
			String docsKomentarFileName, List<File> ppdDocument,
			List<String> ppdCategory, List<String> ppdDescription, PrcMainPreparation prcMainPreparation, List<String> ppdDocumentFileName) throws Exception;
	public List<PrcMainVendor> listMitraKerjaByHeader(PrcMainHeader prcMainHeader) throws Exception;
	public List<PrcMainHeader> listPrcMainHeaderPpApproval(Object object) throws Exception;
	public List<PrcMainDoc> listPrcMainDocByHeader(PrcMainHeader prcMainHeader) throws Exception;
	public PrcMainPreparation getPrcMainPreparationByHeader(
			PrcMainHeader prcMainHeader) throws Exception;
	public void revisiApprovalPP(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception;
	public void setujuApprovalPP(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception;
	public int countPrcMainHeaderPR(Object object) throws Exception;
	public List<PrcMainHeader> listPrcMainHeaderPR(Object object, int page,
			int i) throws Exception;
	public PrcMainVendorQuote getQuoteByVendor(PrcMainVendor prcMainVendor) throws Exception;
	public List<PrcMainVendorQuoteAdmtech> listItemAdministrasi(
			PrcMainVendorQuote prcMainVendorQuote, PrcMainHeader prcMainHeader) throws Exception;
	public List<PrcMainVendorQuoteAdmtech> listItemTeknis(
			PrcMainVendorQuote prcMainVendorQuote, PrcMainHeader prcMainHeader) throws Exception;
	public List<PrcMainVendorQuoteItem> listQuoteItem(PrcMainVendorQuote quote, List<PrcMainItem> listItem) throws Exception;
	public List<PrcMainVendor> listMitraKerjaEvaluasi(
			PrcMainHeader prcMainHeader) throws Exception;
	public List<PrcMainVendorQuoteItem> listQuoteItemEvaluasi(
			List<PrcMainVendor> listVendor) throws Exception;
	public List<PrcMainVendor> listMitraKerjaNegosiasi(
			PrcMainHeader prcMainHeader) throws Exception;
	public List<PrcMainHeader> listPrcMainHeaderOEPemilihanEstimator(
			Object object) throws Exception;
	public void saveDisposisiPraPP(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception;
	public List<TodoUserCommand> getListTodoUser(String todoListUserSpphPr) throws Exception;
	public int countPrcMainHeaderPRSPPHReport(Object object) throws Exception;
	public List<PrcMainHeader> listPrcMainHeaderPRSPPHReport(Object object,
			int page, int i) throws Exception;
	public void revisiApprovalPanitiaPp(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception;
	public void setujuApprovalPanitiaPP(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception;
	public List<PrcMainHeader> listPrcMainHeaderPpPanitiaApproval(Object object) throws Exception;
	public void savePengumumanLelang(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception;
	public List<PrcMainHeader> listPrcMainHeaderPpPengumumanLelang(Object object) throws Exception;
	public void savePrakualifikasi(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName, List<PrcMainVendor> listPrcMainVendor) throws Exception;
	public List<PrcMainHeader> listPrcMainHeaderPpPq(Object object) throws Exception;
	public void finalisasiPq(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName, List<PrcMainVendor> listPrcMainVendor) throws Exception;
	public void revisiPq(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception;
	public void saveChat(PrcMsgChat chat) throws Exception;
	public List<PrcMsgChat> listChatByHeader(PrcMainHeader prcMainHeader) throws Exception;
	public List<PrcMainHeader> listPrcMainHeaderPpAnggaran(Object object) throws Exception;
	public void revisiApprovalAnggaran(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception;
	public void setujuApprovalAnggaran(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception;
	public void createAnggaran(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception;
	public void createAnggaranDetail(PrcAnggaranJdeDetail anggaranJdeDetail) throws Exception;
	public List<PrcAnggaranJdeDetail> listAnggaranDetailByPPmId(
			PrcMainHeader prcMainHeader) throws Exception;
	public void deleteAnggaran(PrcAnggaranJdeDetail anggaranJdeDetail) throws Exception;
	public PrcAnggaranJdeDetail getAnggaranDetailById(PrcAnggaranJdeDetail anggaranJdeDetail) throws Exception;
	public List<PrcMainVendorQuote> getListMainVendorQuoteByPpmId(
			PrcMainHeader prcMainHeader) throws Exception;
	public List<TodoUserCommand> getListTodoUserKontrak(String todoListKontrak) throws Exception;
	public void batalPrSpph(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment) throws Exception;
	public int countsearchPrcMainHeade(PrcMainHeader prcMainHeader) throws Exception;
	public void saveOeFinisManual(PrcMainHeader prcMainHeader,
			List<PrcMainItem> listPrcMainItem, HistMainComment histMainComment,
			File docsKomentar, String docsKomentarFileName) throws Exception;
	public List<PrcMainHeader> listPrcMainHeaderOEManual(Object object) throws Exception;
	public void createAnggaranManual(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception;
	public List<PrcMainHeader> listPrcMainHeaderPpAnggaranManual(Object object) throws Exception;
	public void updateSpph(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName,
			List<PanitiaPengadaanDetail> listPanitiaDetail,
			List<File> ppdDocument, List<String> ppdDocumentFileName,
			List<String> ppdCategory, List<String> ppdDescription,
			List<PrcMainDoc> listPrcMainDoc) throws Exception;
	public PanitiaPengadaan savePanitiaHeader(PanitiaPengadaan panitia, PrcMainHeader prcMainHeader) throws Exception;
	public void savePanitiaDetail(PanitiaPengadaanDetail panitiaDetail) throws Exception;
	public void deletePanitiaDetail(PanitiaPengadaanDetail panitiaDetail) throws Exception;
	public void generateAnggaran(PrcMainHeader prcMainHeader) throws Exception;
	public void saveRevisiEEInit(PrcMainHeader prcMainHeader, List<PrcMainItem> listPrcMainItem,
			HistMainComment histMainComment, File docsKomentar, String docsKomentarFileName) throws Exception;
	public List<PrcMainHeader> listPrcMainHeaderRevisiEEInit(Object object)
			throws Exception;
	public void saveEEFinis(PrcMainHeader prcMainHeader, List<PrcMainItem> listPrcMainItem,
			HistMainComment histMainComment, File docsKomentar, String docsKomentarFileName) throws Exception;
	public void revisiEEApproval(PrcMainHeader prcMainHeader, List<PrcMainItem> listPrcMainItem,
			HistMainComment histMainComment, File docsKomentar, String docsKomentarFileName) throws Exception;
	public void setujuEEApproval(PrcMainHeader prcMainHeader, List<PrcMainItem> listPrcMainItem,
			HistMainComment histMainComment, File docsKomentar, String docsKomentarFileName) throws Exception;
	public void relokasiAnggaran(PrcMainHeader prcMainHeader, HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception;
	public void saveRelokasiAnggaran(PrcMainHeader prcMainHeader, HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception;
	//public void disposisiBySistem(PrcMainHeader prcMainHeader) throws Exception;
	List<PrcMainVendor> listMitraKerjaByStatusPq(PrcMainHeader prcMainHeader) throws Exception;
	List<PrcMainVendor> listMitraKerjaTerdaftar(PrcMainHeader prcMainHeader) throws Exception;
	List<PrcMainVendor> listMitraKerjaByStatusTeknis(PrcMainHeader prcMainHeader) throws Exception;
	List<PrcMainVendor> listMitraKerjaEvaluasiLulusTeknisHarga(PrcMainHeader prcMainHeader) throws Exception;
	public PrcMainVendor getPrcMainVendorById(PrcMainVendorId id);
	List<PrcMainVendor> listMitraKerjaEvaluasiTidakLulusTeknisHarga(PrcMainHeader prcMainHeader) throws Exception;
	List<PrcMainVendor> listMitraKerjaTidakLulusByStatusPq(PrcMainHeader prcMainHeader) throws Exception;
	List<PrcMainVendor> listMitraKerjaEvaluasiLulusTeknis(PrcMainHeader prcMainHeader) throws Exception;
	List<PrcMainVendor> listMitraKerjaEvaluasiLulusHarga(PrcMainHeader prcMainHeader) throws Exception;
	List<PrcMainVendor> listMitraKerjaEvaluasiTidakLulusTeknis(PrcMainHeader prcMainHeader) throws Exception;
	List<PrcMainVendor> listMitraKerjaEvaluasiTidakLulusHarga(PrcMainHeader prcMainHeader) throws Exception;
	int countPrcMainHeaderPRSPPHReportRfq(Object object) throws Exception;
	List<PrcMainHeader> listPrcMainHeaderPRSPPHReportRfq(Object object, int page, int i) throws Exception;
	List<AdmUser> listBuyer(PrcMainHeader prcMainHeader) throws Exception;
	AdmProsesHirarki getHirarkiProsesJenis(PrcMainHeader prcMainHeader) throws Exception;
	List<AdmJenis> getListAdmJenisByParent(AdmJenis jenis);
	int countPrcMainHeaderPRSPPHReportByYear(Object o, Integer year) throws Exception;
	List<PrcMainHeader> listPrcMainHeaderPRSPPHReportByYear(Object object, int page, int i, Integer year) throws Exception;
	void pengadaanManual(PrcMainHeader prcMainHeader, HistMainComment histMainComment, List<File> prcManDocument,
			List<String> prcManDocumentCategory, List<String> prcManDocumentDescription,
			List<String> prcManDocumentFileName, File docsKomentar,
			String docsKomentarFileName) throws Exception;
	List<PrcManualDoc> listPrcManualDocByHeader(PrcMainHeader prcMainHeader) throws Exception;

}
