package eproc.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.eproc.utils.RandomCode;
import com.eproc.utils.SessionGetter;
import com.eproc.v.global.command.TodoListVendorCommand;
import com.eproc.x.global.command.TodoUserCommand;
import com.orm.model.EauctionHistory;
import com.orm.model.EauctionHistoryLatihan;
import com.orm.model.EauctionVendor;
import com.orm.model.HistoryHargaNegosiasi;
import com.orm.model.PrcMainHeader;
import com.orm.model.PrcMainVendor;
import com.orm.model.PrcMainVendorId;
import com.orm.model.PrcMainVendorQuote;
import com.orm.model.PrcMainVendorQuoteAdmtech;
import com.orm.model.PrcMainVendorQuoteAdmtechId;
import com.orm.model.PrcMainVendorQuoteId;
import com.orm.model.PrcMainVendorQuoteItem;
import com.orm.model.PrcMainVendorQuoteItemId;
import com.orm.model.PrcMsgNegotiation;
import com.orm.model.TmpVndHeader;
import com.orm.model.VndHeader;
import com.orm.tools.GenericDao;
import com.orm.tools.HibernateUtil;

public class RfqVendorServiceImpl extends GenericDao implements
		RfqVendorService {

	@Override
	public String getJumlahDashboardVendor(String jumlahDashboardVendor, int status)
			throws Exception {
		try {
			session = HibernateUtil.getSession();
			if(status==2){
				jumlahDashboardVendor = jumlahDashboardVendor.concat(" and c.registrastion_opening < ? and c.registrastion_closing > ?");
			}else if(status==3 || status==4){
				jumlahDashboardVendor = jumlahDashboardVendor.concat(" and c.quotation_opening > ? and c.quotation_opening > ?");
			}else if(status==5 || status==6){
				jumlahDashboardVendor = jumlahDashboardVendor.concat(" and c.quotation_opening_2 > ? and c.quotation_opening_2 > ? and a.PMP_TECHNICAL_STATUS = 1");
			}
			Query q = session.createSQLQuery(jumlahDashboardVendor);
			q.setTimestamp(0, new Date());
			q.setTimestamp(1, new Date());
			String retMe = String.valueOf(q.uniqueResult());
			session.clear();
			session.close();
			return retMe;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "0";
	}

	@SuppressWarnings({ "unchecked"})
	@Override
	public List<TodoListVendorCommand> listTodoListDaftar(String query)
			throws Exception {
		try {
			session = HibernateUtil.getSession();
			query = query.concat(" and (b.registrastion_opening < ? and b.registrastion_closing > ?) ");
			Query q = session.createSQLQuery(query);
			q.setTimestamp(0, new Date());
			q.setTimestamp(1, new Date());
			List<TodoListVendorCommand> retMe = new ArrayList<TodoListVendorCommand>();
			List<Object> tmp = q.list();
			if(SessionGetter.isNotNull(tmp)){
				for(Object o:tmp){
					Object obj[]  = (Object[])o;
					TodoListVendorCommand c = new TodoListVendorCommand();
					c.setPpmId(Long.valueOf(SessionGetter.objectToString(obj[0])));
					c.setVendorId(Long.valueOf(SessionGetter.objectToString(obj[1])));
					c.setNomorRfq((String)obj[2]);
					c.setDaftarBuka((Date)obj[3]);
					c.setDaftarTutup((Date)obj[4]);
					c.setAanwijzingDate((Date)obj[5]);
					c.setQuotationDate((Date)obj[6]);
					retMe.add(c);
				}
			}
			session.clear();
			session.close();
			return retMe;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Override
	public void daftarPengadaan(PrcMainVendor prcMainVendor) throws Exception {
		try {
			startTransaction();
			PrcMainVendor vv = getPrcMainVendorById(prcMainVendor.getId());
			vv.setPmpStatus(prcMainVendor.getPmpStatus());
			saveEntity(vv);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	public PrcMainVendor getPrcMainVendorById(PrcMainVendorId id) {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMainVendor.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.idEq(id));
		return (PrcMainVendor) getEntityByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TodoListVendorCommand> listTodoListPenawaran(
			String queryAanwijzing, String queryNonAanwijzing, String penawaranHarga) throws Exception {
		try {
			session = HibernateUtil.getSession();
			queryAanwijzing = queryAanwijzing.concat(" and b.quotation_opening > ? and a.pmp_aanwijzing_present is null and b.aanwijzing_date is null");
			Query q = session.createSQLQuery(queryAanwijzing);
			q.setTimestamp(0, new Date());
			System.out.println(new Date());
			List<TodoListVendorCommand> retMe = new ArrayList<TodoListVendorCommand>();
			List<Object> tmp = q.list();
			if(SessionGetter.isNotNull(tmp)){
				for(Object o:tmp){
					Object obj[]  = (Object[])o;
					TodoListVendorCommand c = new TodoListVendorCommand();
					c.setPpmId(Long.valueOf(SessionGetter.objectToString(obj[0])));
					c.setVendorId(Long.valueOf(SessionGetter.objectToString(obj[1])));
					c.setNomorRfq((String)obj[2]);
					c.setDaftarBuka((Date)obj[3]);
					c.setDaftarTutup((Date)obj[4]);
					c.setAanwijzingDate((Date)obj[5]);
					c.setQuotationDate((Date)obj[6]);
					retMe.add(c);
				}
			}
			
			queryNonAanwijzing = queryNonAanwijzing.concat(" and b.quotation_opening > ? and a.pmp_aanwijzing_present=1 and b.aanwijzing_date is not null");
			q = session.createSQLQuery(queryNonAanwijzing);
			q.setTimestamp(0, new Date());
			tmp = q.list();
			if(SessionGetter.isNotNull(tmp)){
				for(Object o:tmp){
					Object obj[]  = (Object[])o;
					TodoListVendorCommand c = new TodoListVendorCommand();
					c.setPpmId(Long.valueOf(SessionGetter.objectToString(obj[0])));
					c.setVendorId(Long.valueOf(SessionGetter.objectToString(obj[1])));
					c.setNomorRfq((String)obj[2]);
					c.setDaftarBuka((Date)obj[3]);
					c.setDaftarTutup((Date)obj[4]);
					c.setAanwijzingDate((Date)obj[5]);
					c.setQuotationDate((Date)obj[6]);
					retMe.add(c);
				}
			}
			penawaranHarga = penawaranHarga.concat(" and b.quotation_opening_2 > ? and a.pmp_praq_status = 1 and a.pmp_technical_status=1");
			q = session.createSQLQuery(penawaranHarga);
			q.setTimestamp(0, new Date());
			tmp = q.list();
			if(SessionGetter.isNotNull(tmp)){
				for(Object o:tmp){
					Object obj[]  = (Object[])o;
					TodoListVendorCommand c = new TodoListVendorCommand();
					c.setPpmId(Long.valueOf(SessionGetter.objectToString(obj[0])));
					c.setVendorId(Long.valueOf(SessionGetter.objectToString(obj[1])));
					c.setNomorRfq((String)obj[2]);
					c.setDaftarBuka((Date)obj[3]);
					c.setDaftarTutup((Date)obj[4]);
					c.setAanwijzingDate((Date)obj[5]);
					c.setQuotationDate((Date)obj[6]);
					retMe.add(c);
				}
			}
			
			session.clear();
			session.close();
			return retMe;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Override
	public void savePenawaran(PrcMainVendor prcMainVendor,
			PrcMainHeader prcMainHeader, PrcMainVendorQuote quote,
			List<PrcMainVendorQuoteAdmtech> listAdministrasi,
			List<PrcMainVendorQuoteAdmtech> listTeknis,
			List<PrcMainVendorQuoteItem> listQuoteItem) throws Exception {
		try {
			startTransaction();
			prcMainHeader = new RfqServiceImpl().getPrcMainHeaderLite(prcMainHeader.getPpmId());
			
			//update status 
			prcMainVendor =  getPrcMainVendorById(new PrcMainVendorId(prcMainVendor.getId().getPpmId(), prcMainVendor.getId().getVendorId()));
			prcMainVendor.setPmpStatus(4);//sudah menawarakan
			if(SessionGetter.isNotNull(prcMainVendor.getPmpStatus())){
				if(SessionGetter.isNotNull(prcMainVendor.getPmpTechnicalStatus()) && prcMainVendor.getPmpTechnicalStatus() == 1){
					prcMainVendor.setPmpStatus(6);
				}
			}
			saveEntity(prcMainVendor);
			
			
			//save quote
			quote.setPrcMainVendor(prcMainVendor);
			quote.setId(new PrcMainVendorQuoteId(prcMainVendor.getId().getPpmId(), prcMainVendor.getId().getVendorId()));
			saveEntity(quote);
			
			//save administrasi
			if(listAdministrasi!=null && listAdministrasi.size()>0){
				for(PrcMainVendorQuoteAdmtech a:listAdministrasi){
					a.setId(new PrcMainVendorQuoteAdmtechId(prcMainVendor.getId().getPpmId(), prcMainVendor.getId().getVendorId(),a.getPrcTemplateDetail().getId()));
					saveEntity(a);
					
				}
			}
			
			//save Teknis
			if(listTeknis!=null && listTeknis.size()>0){
				for(PrcMainVendorQuoteAdmtech a:listTeknis){
					a.setId(new PrcMainVendorQuoteAdmtechId(prcMainVendor.getId().getPpmId(), prcMainVendor.getId().getVendorId(),a.getPrcTemplateDetail().getId()));
					saveEntity(a);
					
				}
			}
			
			//save item
			if(listQuoteItem!=null && listQuoteItem.size()>0){
				for(PrcMainVendorQuoteItem a:listQuoteItem){
					a.setId(new PrcMainVendorQuoteItemId(new BigDecimal(a.getPrcMainItem().getId()), prcMainVendor.getId().getPpmId(), prcMainVendor.getId().getVendorId()));
					a.setPrcMainVendorQuote(quote);
					//add validate total
					BigDecimal total = a.getItemPrice().multiply(a.getItemQuantity());
					Double pajak = (a.getItemPajak().doubleValue()+100) /100;
					BigDecimal totalPpn = total.multiply(new BigDecimal(pajak));
					a.setItemPriceTotal(total);
					a.setItemPriceNegoTotalPpn(totalPpn);
					saveEntity(a);
				}
			}
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public String getJumlahNegosiasiVendor(String query) throws Exception {
		try {
			session = HibernateUtil.getSession();
			Query q = session.createSQLQuery(query);
			String retMe = String.valueOf(q.uniqueResult());
			session.clear();
			session.close();
			return retMe;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "0";
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TodoListVendorCommand> listTodoListNego(
			String query) throws Exception {
		try {
			session = HibernateUtil.getSession();
			Query q = session.createSQLQuery(query);
			List<TodoListVendorCommand> retMe = new ArrayList<TodoListVendorCommand>();
			List<Object> tmp = q.list();
			if(SessionGetter.isNotNull(tmp)){
				for(Object o:tmp){
					Object obj[]  = (Object[])o;
					TodoListVendorCommand c = new TodoListVendorCommand();
					c.setPpmId(Long.valueOf(SessionGetter.objectToString(obj[0])));
					c.setVendorId(Long.valueOf(SessionGetter.objectToString(obj[1])));
					c.setNomorRfq((String)obj[2]);
					c.setDaftarBuka((Date)obj[3]);
					c.setDaftarTutup((Date)obj[4]);
					c.setAanwijzingDate((Date)obj[5]);
					c.setQuotationDate((Date)obj[6]);
					retMe.add(c);
				}
			}
			session.clear();
			session.close();
			return retMe;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMsgNegotiation> getListNegosiasiByVendor(
			PrcMainVendor prcMainVendor) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMsgNegotiation.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("prcMainHeader", new PrcMainHeader(prcMainVendor.getId().getPpmId().longValue())));
		VndHeader he = new VndHeader();
		he.setVendorId(prcMainVendor.getId().getVendorId());
		crit.add(Restrictions.eq("vndHeader", he));
		crit.addOrder(Order.asc("msgDate"));
		return (List<PrcMsgNegotiation>) getListByDetachedCiteria(crit);

	}

	@Override
	public void saveNegosiasi(PrcMainVendor prcMainVendor,
			PrcMsgNegotiation negosiasi) throws Exception {
		try {
			startTransaction();
			prcMainVendor = getPrcMainVendorById(prcMainVendor.getId());
			prcMainVendor.setPmpIsNegotatiate(null);
			saveEntity(prcMainVendor);
			
			TmpVndHeader headr = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
			negosiasi.setMsgFrom(headr.getVendorName());
			negosiasi.setMsgDate(new Date());
			saveEntity(negosiasi);
			
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public void savePenawaranNego(PrcMainVendor prcMainVendor,
			PrcMainHeader prcMainHeader, PrcMainVendorQuote quote,
			List<PrcMainVendorQuoteItem> listQuoteItem) throws Exception {
		try {
			startTransaction();
			prcMainHeader = new RfqServiceImpl().getPrcMainHeaderLite(prcMainHeader.getPpmId());
			
			PrcMainVendorQuote qq = new PrcServiceImpl().getQuoteByVendor(prcMainVendor);
			if(qq.getPqmTotalEauction()!=null && qq.getPqmTotalEauction().doubleValue() > 0){
				if(quote.getPqmTotalPenawaranNego().doubleValue() > qq.getPqmTotalEauction().doubleValue()){
					throw new Exception("Nilai Nego Harus Lebih Kecil atau Sama dengan Nilai Eauction ");
				}
			}

			//save quote
			quote.setPrcMainVendor(prcMainVendor);
			quote.setId(new PrcMainVendorQuoteId(prcMainVendor.getId().getPpmId(), prcMainVendor.getId().getVendorId()));
			saveEntity(quote);
			
			//save item
			if(listQuoteItem!=null && listQuoteItem.size()>0){
				for(PrcMainVendorQuoteItem a:listQuoteItem){
					a.setId(new PrcMainVendorQuoteItemId(new BigDecimal(a.getPrcMainItem().getId()), prcMainVendor.getId().getPpmId(), prcMainVendor.getId().getVendorId()));
					a.setPrcMainVendorQuote(quote);
					saveEntity(a);
				}
			}
			
			//insert history
			HistoryHargaNegosiasi hnego = new HistoryHargaNegosiasi();
			hnego.setPpmId(prcMainHeader.getPpmId());
			hnego.setVendorName(StringUtils.upperCase(((TmpVndHeader)SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION)).getVendorName()));
			hnego.setVendorId(((TmpVndHeader)SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION)).getVendorId());
			hnego.setNilaiNego(quote.getPqmTotalPenawaranNego());
			saveEntity(hnego);
			
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TodoUserCommand> listLelang(String todoListLelangOpen)
			throws Exception {
		List<TodoUserCommand> retMe =new ArrayList<TodoUserCommand>();
		try {
			startTransaction();
			Query q =  session.createSQLQuery(todoListLelangOpen);
			q.setTimestamp(0, new Date());
			q.setTimestamp(1, new Date());
			List<Object[]> lst = q.list();
			if(lst!=null){
				Iterator<Object[]> it = lst.iterator();
				while(it.hasNext()){
					Object o[] = it.next();
					it.remove();
					TodoUserCommand cmd = new TodoUserCommand();
					cmd.setPpmId(Integer.valueOf(String.valueOf(o[0])));
					cmd.setNomorPr(String.valueOf(o[1]));
					cmd.setNomorSpph(o[2]!=null?String.valueOf(o[2]):"");
					cmd.setCurrentUser(o[3]!=null?Integer.valueOf(String.valueOf(o[3])):0);
					cmd.setCompleteName(o[4]!=null?String.valueOf(o[4]):"");
					cmd.setUpdatedDate(o[5]!=null?(Date)o[5]:null);
					cmd.setKeterangan(String.valueOf(o[6]));
					cmd.setUrl(String.valueOf(o[7]));
					cmd.setJudulSpph(String.valueOf(o[8]));
					retMe.add(cmd);
					
				}
			}
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
		return retMe;
	}

	@Override
	public void daftarLelang(PrcMainHeader prcMainHeader, VndHeader vndHeader)
			throws Exception {
		try {
			startTransaction();
			//cek vendor
			TmpVndHeader vh = getVndHeaderActive(vndHeader);
			if(vh!=null){
				System.out.println(">>>>> VENDOR NOT NULL");
				//cek jika sudah terdaftar
				PrcMainVendorId idTest = new PrcMainVendorId();
				idTest.setPpmId(new BigDecimal(prcMainHeader.getPpmId()));
				idTest.setVendorId(vh.getVendorId());
				
				PrcMainVendor testPq = getPrcMainVendorById(idTest);
				
				if(testPq==null){
					System.out.println(">>>>> BELOM TERDAFTAR");
					PrcMainVendorId id = new PrcMainVendorId();
					id.setPpmId(new BigDecimal(prcMainHeader.getPpmId()));
					id.setVendorId(vh.getVendorId());
					
					PrcMainVendor pmv = new PrcMainVendor();
					pmv.setId(id);
					VndHeader hh = new VndHeader();
					hh.setVendorId(vh.getVendorId());
					pmv.setVndHeader(hh);
					pmv.setPrcMainHeader(prcMainHeader);
					pmv.setPmpStatus(1);
					saveEntity(pmv);
				}else{
					RollbackAndClear();
					throw new Exception("Anda Sudah Terdaftar Dipengadaan ini .....");
				}
			}else{
				RollbackAndClear();
				throw new Exception("User Name Password Anda Salah ..");
			}
			commitAndClear();
		} catch (Exception e) {
			if(session.isOpen()){
				RollbackAndClear();
			}
			throw e;
		}
	}

	private TmpVndHeader getVndHeaderActive(VndHeader vndHeader) throws Exception{
		try {
			DetachedCriteria crit = DetachedCriteria.forClass(TmpVndHeader.class);
			crit.add(Restrictions.ne("vendorStatus", "B"));
			crit.add(Restrictions.ne("vendorStatus", "S"));
			System.out.println(">>> "+RandomCode.stringToMd5(vndHeader.getVendorPassword())+" === "+vndHeader.getVendorPassword() +" >>>> "+vndHeader.getVendorLogin());
			//crit.add(Restrictions.eq("vendorPassword", vndHeader.getVendorPassword()));
			crit.add(Restrictions.eq("vendorLogin", vndHeader.getVendorLogin()));
			return (TmpVndHeader) getEntityByDetachedCiteria(crit);
		} catch (Exception e) {
			throw new Exception("Anda Belum Terdaftar");
		}
		
	}

	@Override
	public void savePenawaranEauction(PrcMainVendorQuote quote, EauctionVendor eauctionVendor,
			List<PrcMainVendorQuoteItem> listQuoteItem) throws Exception {
		try {
			startTransaction();
			PrcMainVendorId id = new PrcMainVendorId(quote.getId().getPpmId(), quote.getId().getVendorId());
			PrcMainVendor pv = new PrcMainVendor();
			pv.setId(id);
			quote = new PrcServiceImpl().getQuoteByVendor(pv);
//			quote.setPqmTotalPenawaran(eauctionVendor.getLastBid());
			quote.setPqmTotalEauction(eauctionVendor.getLastBid());
			saveEntity(quote);

			//save item
			/**
			if(listQuoteItem!=null && listQuoteItem.size()>0){
				for(PrcMainVendorQuoteItem a:listQuoteItem){
					a.setId(new PrcMainVendorQuoteItemId(new BigDecimal(a.getPrcMainItem().getId()), quote.getId().getPpmId(), quote.getId().getVendorId()));
					a.setPrcMainVendorQuote(quote);
					saveEntity(a);
				}
			}
			**/
			//inser ke eauction vendor
			eauctionVendor.setTglBid(new Date());
			saveEntity(eauctionVendor);
			
			//insert ke history
			EauctionHistory ah = new EauctionHistory();
			ah.setEauctionVendor(eauctionVendor);
			ah.setJumlahBid(eauctionVendor.getLastBid().doubleValue());
			ah.setTglBid(eauctionVendor.getTglBid());
			saveEntity(ah);
			
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public String getJumlahDashboardEauction(String jumlahEauction)
			throws Exception {
		try {
			session = HibernateUtil.getSession();
			Query q = session.createSQLQuery(jumlahEauction);
			q.setTimestamp(0, new Date());
			String retMe = String.valueOf(q.uniqueResult());
			session.clear();
			session.close();
			return retMe;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "0";
	}

	@Override
	public String getJumlahDashboardHistory(String jumlahHistory)
			throws Exception {
		try {
			session = HibernateUtil.getSession();
			Query q = session.createSQLQuery(jumlahHistory);
			String retMe = String.valueOf(q.uniqueResult());
			session.clear();
			session.close();
			return retMe;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "0";
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TodoListVendorCommand> listTodoListHistory(
			String historyPengadaan) throws Exception {
		List<TodoListVendorCommand> retMe = new ArrayList<TodoListVendorCommand>();
		try {
			startTransaction();
			Query q =  session.createSQLQuery(historyPengadaan);
			
			List<Object> tmp = q.list();
			if(SessionGetter.isNotNull(tmp)){
				for(Object o:tmp){
					Object obj[]  = (Object[])o;
					TodoListVendorCommand c = new TodoListVendorCommand();
					c.setPpmId(Long.valueOf(SessionGetter.objectToString(obj[0])));
					c.setVendorId(Long.valueOf(SessionGetter.objectToString(obj[1])));
					c.setNomorRfq((String)obj[2]);
					c.setStatus(obj[3]!=null?String.valueOf(obj[3]):"");
					c.setAdministrasi(obj[4]!=null?String.valueOf(obj[4]):"");
					c.setTeknis(obj[5]!=null?String.valueOf(obj[5]):"");
					c.setHarga(obj[6]!=null?String.valueOf(obj[6]):"");
					retMe.add(c);
				}
			}
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
		return retMe;
	}

	@Override
	public void editProfile(TmpVndHeader headr) throws Exception {
		try {
			startTransaction();
			TmpVndHeader hh = new VendorServiceImpl().getTmpVndHeaderById(headr.getVendorId());
			if(!hh.getVendorStatus().trim().equals("E")){
				hh.setVendorStatus("E");
				hh.setVendorNextPage(9);
				saveEntity(hh);
			}
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TodoListVendorCommand> listTodoListContract(String historyPo)
			throws Exception {
		List<TodoListVendorCommand> retMe = new ArrayList<TodoListVendorCommand>();
		try {
			startTransaction();
			Query q =  session.createSQLQuery(historyPo);
			List<Object> tmp = q.list();
			if(SessionGetter.isNotNull(tmp)){
				for(Object o:tmp){
					Object obj[]  = (Object[])o;
					TodoListVendorCommand c = new TodoListVendorCommand();
					c.setContractId(Long.valueOf(SessionGetter.objectToString(obj[0])));
					c.setContractNumber(SessionGetter.nullObjectToEmptyString(obj[1]));
					c.setContractAmount(SessionGetter.nullNumberToEmptyString(obj[2]));
					c.setNomorPembayaran(SessionGetter.nullObjectToEmptyString(obj[3]));
					c.setJudulKontrak(SessionGetter.nullObjectToEmptyString(obj[4]));
					c.setTglKontrakAwal(obj[5]!=null?(Date)obj[5]:null);
					c.setTglKontrakAkhir(obj[6]!=null?(Date)obj[6]:null);
					if(c.getNomorPembayaran()!=null && c.getNomorPembayaran()!="")
						c.setStatusPembayaran(getStatusPembayaran(c.getNomorPembayaran()));
					retMe.add(c);
				}
			}
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
		return retMe;
	}

	public String getStatusPembayaran(String nomorPembayaran) {
		System.out.println("STATUS PEMBAYARAN");
		String NO [] = null;
		String tipe = "";
		if(nomorPembayaran.contains("O1")){
			NO = nomorPembayaran.split("O1");
			tipe = "O1";
		}else if(nomorPembayaran.contains("O2")){
			NO = nomorPembayaran.split("O2");
			tipe = "O2";
		}else if(nomorPembayaran.contains("O4")){
			NO = nomorPembayaran.split("O4");
			tipe = "O4";
		}else if(nomorPembayaran.contains("O9")){
			NO = nomorPembayaran.split("O9");
			tipe = "O9";
		}
		
		String retMe="";
		Session ses = HibernateUtil.getSessionJde();
		try {
				String query = "select distinct rppst from "+SessionGetter.getPropertiesValue("jde.schema", null)+".f0411 where rppo='"+NO[0]+"' and rpkco='"+NO[1]+"' and rppdct='"+tipe+"'";
				Query qq = ses.createSQLQuery(query);
				retMe = SessionGetter.nullObjectToEmptyString(qq.uniqueResult());
			ses.clear();
			ses.close();
		} catch (Exception e) {
			ses.clear();
			ses.close();
			e.printStackTrace();
		}
		return retMe;
	}

}
