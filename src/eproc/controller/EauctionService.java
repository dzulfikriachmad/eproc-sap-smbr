package eproc.controller;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import com.eproc.x.global.command.EauctionTableHistoryModel;
import com.eproc.x.global.command.EauctionTableModel;
import com.orm.model.EauctionHeader;
import com.orm.model.EauctionVendor;
import com.orm.model.PrcMainHeader;
import com.orm.model.TmpVndHeader;
import com.orm.model.VndHeader;

public interface EauctionService {

	List<PrcMainHeader> listPrcMainHeaderEauction(Object object) throws Exception;

	EauctionHeader eauctionHeaderByPpm(PrcMainHeader prcMainHeader) throws Exception;

	List<EauctionVendor> listVendorEauctionByHeader(
			EauctionHeader eauctionHeader, PrcMainHeader prcMainHeader) throws Exception;

	void saveEauction(EauctionHeader eauctionHeader,
			List<EauctionVendor> listEauctionVendor) throws Exception;

	List<EauctionTableModel> listEauctionVendor(String listVendorEauction) throws Exception;

	List<EauctionTableHistoryModel> listEauctionHistory(
			String listHistoryEauction) throws Exception;

	List<EauctionHeader> listEauctionHeaderByVendorActive(TmpVndHeader headr) throws Exception;

	EauctionVendor eauctionVendorByHeaderByVendorId(
			EauctionHeader eauctionHeader, VndHeader vh) throws Exception;

	Double getLowestBid(Long ppmId) throws Exception;

	void diskualifikasiVendor(Integer eauctionVendorId) throws Exception;
	
	void resetEauction(EauctionHeader eauctionHeader, PrcMainHeader prcMainHeader) throws Exception;

	void latihanEauction(EauctionHeader eauctionHeader,
			List<EauctionVendor> listEauctionVendor) throws Exception;
	
	void pauseEauction(EauctionHeader eauctionHeader, PrcMainHeader prcMainHeader) throws Exception;
	void resumeEauction(EauctionHeader eauctionHeader, PrcMainHeader prcMainHeader) throws Exception;
	
	void saveLatihan(EauctionHeader eauctionHeader, List<EauctionVendor> listEauctionVendor) throws Exception;
	
	Integer getTimeEauction(Long ppmId,  Date endDate);

	EauctionHeader getEauctionHeaderByPpmId(Long ppmId) throws Exception;

	Date getDatabaseTime() throws Exception, ParseException;

	Integer countdownTime(Integer remaining);
	
}
