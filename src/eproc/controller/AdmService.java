package eproc.controller;

import java.util.List;

import com.orm.model.AdmAddType;
import com.orm.model.AdmCompanyType;
import com.orm.model.AdmCostCenter;
import com.orm.model.AdmCountry;
import com.orm.model.AdmCurrency;
import com.orm.model.AdmDelegasi;
import com.orm.model.AdmDept;
import com.orm.model.AdmDistrict;
import com.orm.model.AdmDoc;
import com.orm.model.AdmEmployee;
import com.orm.model.AdmIssues;
import com.orm.model.AdmJadwalDrt;
import com.orm.model.AdmJenis;
import com.orm.model.AdmMenu;
import com.orm.model.AdmProses;
import com.orm.model.AdmProsesHirarki;
import com.orm.model.AdmRole;
import com.orm.model.AdmRoleMenu;
import com.orm.model.AdmUom;
import com.orm.model.AdmUser;
import com.orm.model.PanitiaVnd;
import com.orm.model.PanitiaVndDetail;
import com.orm.model.PrcMappingKeuangan;
import com.orm.model.PrcTemplate;
import com.orm.model.PrcTemplateDetail;

public interface AdmService {
	public List<AdmCurrency> listAdmCurr();
	public List<AdmCompanyType> listCompanyType();
	public List<AdmAddType> listAddType();
	public List<AdmCountry> listAdmCountry();
	public List<AdmDistrict> listAdmDistrict();
	public List<AdmDept> listAdmDept();
	public List<AdmDoc> listAdmDoc();
	public List<AdmUom> listAdmUom();
	public List<AdmUser> listAdmUser();
	public List<AdmCostCenter> listAdmCostCenterByDept(AdmDept admDept);
	public AdmEmployee getAdmEmployeeById(long id);
	public AdmUser doLogin(AdmUser admUser);
	public AdmUser getAdmUser(AdmUser o);
	
	/** Dept **/
	public List<AdmDept> getListAdmDept(Object object) throws Exception;
	public void saveDept(AdmDept dept) throws Exception;
	public AdmDept getAdmDeptById(AdmDept dept) throws Exception;
	public void deleteDept(AdmDept dept) throws Exception;
	
	/** district **/
	public List<AdmDistrict> getListDistrict(Object object) throws Exception;
	public void saveDistrict(AdmDistrict district, PrcMappingKeuangan prcMappingKeuangan) throws Exception;
	public AdmDistrict getDistrictById(AdmDistrict district) throws Exception;
	public void deleteDistrict(AdmDistrict district) throws Exception;
	
	/** Country **/
	public void saveCountry(AdmCountry country) throws Exception;
	public AdmCountry getCountryById(AdmCountry country) throws Exception;
	public void deleteCountry(AdmCountry country) throws Exception;
	
	/** document **/
	public List<AdmDoc> getAdmDoc(Object object) throws Exception;
	public void saveDoc(AdmDoc doc) throws Exception;
	public AdmDoc getDocById(AdmDoc doc) throws Exception;
	public void deleteDoc(AdmDoc doc) throws Exception;
	
	/** role **/
	public void deleteRole(AdmRole role) throws Exception;
	public AdmRole getRoleById(AdmRole role) throws Exception;
	public void saveRole(AdmRole role) throws Exception;
	public List<AdmRole> getListRole(Object object) throws Exception;
	public List<AdmRoleMenu> getListRoleMenuByRole(AdmRole role) throws Exception;
	public void saveListRoleMenu(List<AdmRoleMenu> listRoleMenu) throws Exception;
	public List<AdmMenu> getListRoleMenuByEmployee(AdmEmployee admEmployee) throws Exception;
	
	/** Menu **/
	public void deleteMenu(AdmMenu menu) throws Exception;
	public AdmMenu getMenuById(AdmMenu menu) throws Exception;
	public void saveMenu(AdmMenu menu) throws Exception;
	public List<AdmMenu> getListMenu(Object object) throws Exception;
	
	/**user **/
	public List<AdmUser> getListUser(Object object) throws Exception;
	public void saveUser(AdmUser user) throws Exception;
	public AdmUser getUserById(AdmUser user) throws Exception;
	public void deleteUser(AdmUser user) throws Exception;
	
	/** UOM **/
	public void deleteUom(AdmUom uom) throws Exception;
	public AdmUom getUomById(AdmUom uom) throws Exception;
	public void saveUom(AdmUom uom) throws Exception;
	public List<AdmUom> getListUom(Object object) throws Exception;
	
	/** proses **/
	public void deleteProses(AdmProses proses) throws Exception;
	public AdmProses getProsesById(AdmProses proses) throws Exception;
	public void saveProses(AdmProses proses) throws Exception;
	public List<AdmProses> getListProses(Object object) throws Exception;
	
	/** hirarki proses **/
	public void deleteHirarki(AdmProsesHirarki hirarki) throws Exception;
	public AdmProsesHirarki getHirarkiById(AdmProsesHirarki hirarki) throws Exception;
	public void saveHirarki(AdmProsesHirarki hirarki) throws Exception;
	public List<AdmProsesHirarki> getListHirarki(Object object) throws Exception;
	
	/** cost center **/
	public void deleteCostCenter(AdmCostCenter costCenter) throws Exception;
	public AdmCostCenter getCostCenterById(AdmCostCenter costCenter) throws Exception;
	public void saveCostCenter(AdmCostCenter costCenter) throws Exception;
	public List<AdmCostCenter> getListCostCenter(Object object) throws Exception;
	
	/** jadwal drt **/
	public void deleteJadwal(AdmJadwalDrt jadwal) throws Exception;
	public AdmJadwalDrt getJadwalById(AdmJadwalDrt jadwal) throws Exception;
	public void saveJadwal(AdmJadwalDrt jadwal) throws Exception;
	public List<AdmJadwalDrt> getListJadwal(AdmUser user) throws Exception;
	
	/** panitia drt **/
	public List<PanitiaVnd> getListPanitiaVnd(Object object) throws Exception;
	public List<PanitiaVndDetail> getListPanitiaDetailByPanitia(
			PanitiaVnd panitiaVnd) throws Exception;
	public PanitiaVnd getPanitiaVndById(PanitiaVnd panitiaVnd) throws Exception;
	public void hapusPanitia(PanitiaVnd panitiaVnd) throws Exception;
	public void savePanitiaVnd(PanitiaVnd panitiaVnd) throws Exception;
	public void savePanitiaDetail(PanitiaVndDetail panitiaVndDetail) throws Exception;
	public PanitiaVndDetail getPanitiaDetailById(
			PanitiaVndDetail panitiaVndDetail) throws Exception;
	public void hapusPanitiaDetail(PanitiaVndDetail panitiaVndDetail) throws Exception;
	
	/** template Pengadaan **/
	public PrcTemplateDetail getTemplateDetailById(PrcTemplateDetail prcTemplateDetail) throws Exception;
	public void hapusTemplateDetail(PrcTemplateDetail templateDetailById) throws Exception;
	public List<PrcTemplate> getListTemplate(Object object) throws Exception;
	public void saveTemplatePengadaan(PrcTemplate template,
			List<PrcTemplateDetail> sessionValue) throws Exception;
	public PrcTemplate getTemplateById(PrcTemplate template) throws Exception;
	public List<PrcTemplateDetail> getListTemplateDetailByTemplate(PrcTemplate prcTemplate) throws Exception;
	public List<AdmDelegasi> listDelegasi(Object object) throws Exception;
	public AdmDelegasi getDelegasiById(AdmDelegasi admDelegasi) throws Exception;
	public void saveDelegasi(AdmDelegasi admDelegasi) throws Exception;
	public void savePassword(AdmUser user) throws Exception;
	public PrcMappingKeuangan getMappingKeuanganByKantor(AdmDistrict district) throws Exception;
	public void deleteTemplate(PrcTemplate template) throws Exception;
	public List<AdmIssues> getListIssues(Object object) throws Exception;
	public void saveIssues(AdmIssues issues) throws Exception;
	public AdmIssues getIssuesById(AdmIssues issues) throws Exception;
	public AdmCurrency getCurrName(AdmCurrency cur);
	public AdmJenis getAdmJenisById(Integer id);
	void resetPasswordUser(AdmUser user) throws Exception;
	
	
	
}
