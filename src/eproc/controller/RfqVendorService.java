package eproc.controller;

import java.util.List;

import com.eproc.v.global.command.TodoListVendorCommand;
import com.eproc.x.global.command.TodoUserCommand;
import com.orm.model.EauctionVendor;
import com.orm.model.PrcMainHeader;
import com.orm.model.PrcMainVendor;
import com.orm.model.PrcMainVendorQuote;
import com.orm.model.PrcMainVendorQuoteAdmtech;
import com.orm.model.PrcMainVendorQuoteItem;
import com.orm.model.PrcMsgNegotiation;
import com.orm.model.TmpVndHeader;
import com.orm.model.VndHeader;

public interface RfqVendorService {

	String getJumlahDashboardVendor(String jumlahDashboardVendor, int status) throws Exception;

	List<TodoListVendorCommand> listTodoListDaftar(String query) throws Exception;

	void daftarPengadaan(PrcMainVendor prcMainVendor) throws Exception;

	List<TodoListVendorCommand> listTodoListPenawaran(String nonAanwijzing, String aanWijzing, String penawaranHarga) throws Exception;

	void savePenawaran(PrcMainVendor prcMainVendor,
			PrcMainHeader prcMainHeader, PrcMainVendorQuote quote,
			List<PrcMainVendorQuoteAdmtech> listAdministrasi,
			List<PrcMainVendorQuoteAdmtech> listTeknis,
			List<PrcMainVendorQuoteItem> listQuoteItem) throws Exception;

	String getJumlahNegosiasiVendor(String query) throws Exception;

	List<TodoListVendorCommand> listTodoListNego(String todoListVendorSQLNego) throws Exception;

	List<PrcMsgNegotiation> getListNegosiasiByVendor(PrcMainVendor prcMainVendor) throws Exception;

	void saveNegosiasi(PrcMainVendor prcMainVendor, PrcMsgNegotiation negosiasi) throws Exception;

	void savePenawaranNego(PrcMainVendor prcMainVendor,
			PrcMainHeader prcMainHeader, PrcMainVendorQuote quote,
			List<PrcMainVendorQuoteItem> listQuoteItem) throws Exception;

	List<TodoUserCommand> listLelang(String todoListLelangOpen) throws Exception;

	void daftarLelang(PrcMainHeader prcMainHeader, VndHeader vndHeader) throws Exception;

	void savePenawaranEauction(PrcMainVendorQuote prcMainVendorQuote, EauctionVendor eauctionVendor,
			List<PrcMainVendorQuoteItem> listQuoteItem) throws Exception;

	String getJumlahDashboardEauction(String jumlahEauction) throws Exception;

	String getJumlahDashboardHistory(String jumlahHistory) throws Exception;

	List<TodoListVendorCommand> listTodoListHistory(String historyPengadaan) throws Exception;

	void editProfile(TmpVndHeader headr) throws Exception;

	List<TodoListVendorCommand> listTodoListContract(String historyPo) throws Exception;

}
