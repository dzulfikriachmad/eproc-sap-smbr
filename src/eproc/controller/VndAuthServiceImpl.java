package eproc.controller;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import com.eproc.interceptor.OrmVndInterceptor;
import com.eproc.utils.EmailSessionBean;
import com.eproc.utils.QueryUtil;
import com.eproc.utils.RandomCode;
import com.eproc.utils.SessionGetter;
import com.orm.model.TmpVndHeader;
import com.orm.model.VndHeader;
import com.orm.tools.GenericDao;

public class VndAuthServiceImpl extends GenericDao implements VndAuthService {

	@Override
	public TmpVndHeader doLogin(TmpVndHeader vndHeader) {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		
		if(vndHeader != null){
			dc.add(Restrictions.eq("vendorLogin", vndHeader.getVendorLogin()));
			//dc.add(Restrictions.eq("vendorPassword", RandomCode.stringToMd5(vndHeader.getVendorPassword())));
			
			TmpVndHeader val = (TmpVndHeader) getEntityByDetachedCiteria(dc);
			if(val != null){
				return val;
			}else{
				return null;
			}
		}else{
			return null;
		}
	}

	@Override
	public TmpVndHeader getVndHeaderByMail(String vendorEmail) {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.add(Restrictions.eq("vendorEmail", vendorEmail));
		return (TmpVndHeader) getEntityByDetachedCiteria(dc);
	}

	@Override
	public TmpVndHeader getVndHeaderByLogin(String vendorLogin) {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.add(Restrictions.eq("vendorLogin", vendorLogin));
		return (TmpVndHeader) getEntityByDetachedCiteria(dc);
	}
	
	@Override
	public TmpVndHeader getVndHeaderByNpwp(String vendorNpwp) {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.add(Restrictions.eq("vendorNpwpNo", vendorNpwp));
		return (TmpVndHeader) getEntityByDetachedCiteria(dc);
	}

	@Override
	public boolean saveOrUpdateTmpVndHeader(TmpVndHeader vndHeader) {
		boolean ret = false;
		try {
			startTransaction(new OrmVndInterceptor());
			ret = saveEntity(vndHeader);
			commitAndClear();
		} catch (Exception e) {
			e.printStackTrace();
			RollbackAndClear();
			return ret;
		}
		return ret;
	}

	@Override
	public TmpVndHeader getVndHeaderByMailAndLogin(String email, String login) {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.add(Restrictions.eq("vendorLogin", login));
		dc.add(Restrictions.eq("vendorEmail", email));
		
		return (TmpVndHeader) getEntityByDetachedCiteria(dc);
	}

	@Override
	public TmpVndHeader getTmpVndHeaderById(long id) {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.add(Restrictions.eq("vendorId", id)); 
		
		return (TmpVndHeader) getEntityByDetachedCiteria(dc);
	}

	@Override
	public boolean setVendorActivation(String login, String email) {
		boolean ret = false;
		String hql = "update TmpVndHeader set vendorIsActivate = '1' where vendorLogin = '" + login + "' and vendorEmail = '" + email + "'";

		try {
			startTransaction();
			ret = executeByHql(hql);
			commitAndClear();
		} catch (Exception e) {
			e.printStackTrace();
			RollbackAndClear();
			return ret;
		}
		return ret;
	}

	@Override
	public void resetPassword(TmpVndHeader vndHeader) throws Exception {
		try {
			startTransaction();
			DetachedCriteria crit = DetachedCriteria.forClass(TmpVndHeader.class);
			crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			crit.add(Restrictions.eq("vendorEmail", vndHeader.getVendorEmail()));
			TmpVndHeader tmp = (TmpVndHeader) getEntityByDetachedCiteria(crit); 
			if(tmp==null){
				commitAndClear();
				throw new Exception("Email Tidak Terdaftar (Email Address Not Registered)");
			}
			
			tmp.setVendorPassword(RandomCode.stringToMd5(vndHeader.getVendorPassword()));
			saveEntity(tmp);
			
			//cek jika ada vnd Header
			DetachedCriteria cr = DetachedCriteria.forClass(VndHeader.class);
			cr.add(Restrictions.idEq(tmp.getVendorId()));
			VndHeader hh = (VndHeader) getEntityByDetachedCiteria(cr);
			if(hh!=null){
				hh.setVendorPassword(tmp.getVendorPassword());
				saveEntity(hh);
			}
			//Email Vendor
			Object obj [] = new Object[2];
			obj[0] = tmp.getVendorLogin();
			obj[1] = vndHeader.getVendorPassword();
			String message = SessionGetter.getPropertiesValue("vnd.reg.reset.password", obj);
			EmailSessionBean.sendMail("eProc PT. Semen Baturaja (Persero) ", message, tmp.getVendorEmail());
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public void aktivasiManual(TmpVndHeader vndHeader) throws Exception {
		try {
			startTransaction();
			DetachedCriteria cr = DetachedCriteria.forClass(TmpVndHeader.class);
			cr.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			cr.add(Restrictions.ilike("vendorEmail", vndHeader.getVendorEmail().trim(),MatchMode.EXACT));
			@SuppressWarnings("unchecked")
			List<TmpVndHeader> lst = (List<TmpVndHeader>) getListByDetachedCiteria(cr);
			if(lst!=null && lst.size()>0){
				for(TmpVndHeader h:lst){
					h.setVendorIsActivate(Integer.valueOf(1));
					saveEntity(h);
				}
				
			}else{
				throw new Exception("EMail Tidak Ditemukan");
			}
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
		
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public boolean isExistNpwp(String npwp) throws Exception{
		startTransaction();
		npwp = npwp.replaceAll("[^A-Za-z0-9]+", "");
		Query q = session.createSQLQuery(QueryUtil.vndCheckNpwp(npwp));
		List<Object[]> lst = q.list(); 
		System.out.println(lst);
		if(lst.size()>0){
			return true;
		}
		return false;
	}
}
