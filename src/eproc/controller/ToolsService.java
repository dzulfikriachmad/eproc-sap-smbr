package eproc.controller;

import java.util.List;

import com.orm.model.PrcMainHeader;
import com.orm.model.PrcMainPreparation;
import com.orm.model.PrcMainVendor;
import com.orm.model.PrcMsgSanggahan;
import com.orm.model.TmpVndHeader;
import com.orm.model.VndHeader;

public interface ToolsService {
	public List<PrcMainHeader> searchPrcMainHeader(PrcMainHeader o,Integer page, Integer size) throws Exception;
	public boolean editTanggal(PrcMainPreparation o) throws Exception; 
	public PrcMainPreparation getPrcMainPreparation(PrcMainHeader o) throws Exception;
	public List<PrcMainHeader> getListSanggahanPq(Object o) throws Exception;
	public PrcMainHeader getPrcMainHeader(PrcMainHeader o) throws Exception;
	public List<PrcMsgSanggahan> getListSanggahan(PrcMainHeader h, VndHeader v) throws Exception;
	public boolean saveSanggahan(PrcMsgSanggahan o);
	public VndHeader getVndHeader(TmpVndHeader o);
	public List<PrcMainVendor> getListPrcVendor(PrcMainHeader o) throws Exception;
	List<PrcMainHeader> searchPrcMainHeaderRfq(PrcMainHeader o,  Integer year) throws Exception;
	public List<Integer> getListOfYears();
	List<PrcMainHeader> searchPrcMainHeaderAll(PrcMainHeader o, Integer year) throws Exception;
	List<PrcMainHeader> searchPrcMainHeaderAllByYear(PrcMainHeader o, Integer year) throws Exception;
	List<PrcMainHeader> searchPrcMainHeaderManual(PrcMainHeader o) throws Exception;

}
