package eproc.controller;

import java.io.File;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import com.eproc.sap.controller.IntegrationService;
import com.eproc.sap.controller.IntegrationServiceImpl;
import com.eproc.sap.dto.PoHeaderDto;
import com.eproc.sap.dto.ResultDto;
import com.eproc.utils.EmailSessionBean;
import com.eproc.utils.GlobalComparator;
import com.eproc.utils.JulianToGregorianViceVersa;
import com.eproc.utils.SessionGetter;
import com.eproc.utils.UploadUtil;
import com.eproc.x.rfq.command.MainMasterJdeCommand;
import com.eproc.x.rfq.command.TabulasiPenilaian;
import com.jde.model.PODetail;
import com.jde.model.POHeader;
import com.jdedwards.system.connector.dynamic.UserSession;
import com.orm.model.AdmDelegasi;
import com.orm.model.AdmDept;
import com.orm.model.AdmDistrict;
import com.orm.model.AdmProsesHirarki;
import com.orm.model.AdmPurchasingOrganization;
import com.orm.model.AdmUser;
import com.orm.model.CtrContractComment;
import com.orm.model.CtrContractHeader;
import com.orm.model.CtrContractItem;
import com.orm.model.CtrContractPb;
import com.orm.model.CtrContractTop;
import com.orm.model.HistMainComment;
import com.orm.model.HistoryHargaNegosiasi;
import com.orm.model.PanitiaPengadaan;
import com.orm.model.PanitiaPengadaanDetail;
import com.orm.model.PrcAnggaranJdeDetail;
import com.orm.model.PrcAnggaranJdeHeader;
import com.orm.model.PrcMainHeader;
import com.orm.model.PrcMainItem;
import com.orm.model.PrcMainPreparation;
import com.orm.model.PrcMainVendor;
import com.orm.model.PrcMainVendorId;
import com.orm.model.PrcMainVendorQuote;
import com.orm.model.PrcMainVendorQuoteAdmtech;
import com.orm.model.PrcMainVendorQuoteItem;
import com.orm.model.PrcMainVendorQuoteItemId;
import com.orm.model.PrcMsgChat;
import com.orm.model.PrcMsgNegotiation;
import com.orm.model.VndHeader;
import com.orm.tools.GenericDao;
import com.orm.tools.HibernateUtil;

public class RfqServiceImpl extends GenericDao implements RfqService {
	

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> listPrcMainHeaderUndangVendor(Object object) throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		dc.add(Restrictions.eq("admUserByPpmCurrentUser", SessionGetter.getUser()));
		dc.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
		dc.add(Restrictions.eq("ppmProsesLevel", 1));
		return (List<PrcMainHeader>) getListByDetachedCiteria(dc);
	}

	@Override
	public PrcMainHeader getPrcMainHeaderSPPHById(Long ppmId) throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByPpmEstimator", FetchMode.JOIN);
		dc.setFetchMode("admUserByPpmCreator", FetchMode.JOIN);
		dc.setFetchMode("admUserByPpmBuyer", FetchMode.JOIN);
		dc.setFetchMode("admUserByPpmAdmBuyer", FetchMode.JOIN);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		dc.setFetchMode("admDistrict", FetchMode.JOIN);
		dc.setFetchMode("prcMethod", FetchMode.JOIN);
		dc.setFetchMode("prcTemplate", FetchMode.JOIN);
		dc.add(Restrictions.eq("ppmId", ppmId));
		return (PrcMainHeader) getEntityByDetachedCiteria(dc);
	}

	public PrcMainHeader getPrcMainHeaderLite(Long ppmId) {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("panitia", FetchMode.JOIN);
		dc.setFetchMode("pengawasPp", FetchMode.JOIN);
		dc.setFetchMode("ppmJenis", FetchMode.JOIN);
		dc.setFetchMode("ppmJenisDetail", FetchMode.JOIN);
		dc.add(Restrictions.eq("ppmId", ppmId));
		return (PrcMainHeader) getEntityByDetachedCiteria(dc);
	}
	
	@Override
	public PrcMainHeader getPrcMainHeaderSPPHByPpmId(Long ppmId)
		    throws Exception
		  {
		    DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		    dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		    dc.setFetchMode("admUserByPpmEstimator", FetchMode.JOIN);
		    dc.setFetchMode("admUserByPpmCreator", FetchMode.JOIN);
		    dc.setFetchMode("admUserByPpmBuyer", FetchMode.JOIN);
			dc.setFetchMode("panitia", FetchMode.JOIN);
		    dc.setFetchMode("admUserByPpmAdmBuyer", FetchMode.JOIN);
		    dc.setFetchMode("admDept", FetchMode.JOIN);
		    dc.setFetchMode("admDistrict", FetchMode.JOIN);
		    dc.setFetchMode("prcMethod", FetchMode.JOIN);
		    dc.setFetchMode("prcTemplate", FetchMode.JOIN);
		dc.setFetchMode("ppmJenis", FetchMode.JOIN);
		    dc.setFetchMode("ppmJenisDetail", FetchMode.JOIN);
		dc.add(Restrictions.eq("ppmId", ppmId));
		return (PrcMainHeader) getEntityByDetachedCiteria(dc);
	}

	@Override
	public void saveUndangVendor(PrcMainHeader prcMainHeader, HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName, List<PrcMainVendor> listVendor) throws Exception {
		try {
			startTransaction();
			PrcService service = new PrcServiceImpl();
			prcMainHeader = getPrcMainHeaderLite(prcMainHeader.getPpmId());

			// undang vendor
			if (listVendor != null) {
				String recipients[] = new String[listVendor.size()];
				int i = 0;
				for (PrcMainVendor v : listVendor) {
					v = getPrcMainVendorById(v.getId());
					v.setPmpStatus(2);// di undang
					saveEntity(v);
					recipients[i] = v.getVndHeader().getVendorEmail();
					i++;
				}

				// send email
				PrcMainPreparation prep = new PrcServiceImpl().getPrcMainPreparationByHeader(prcMainHeader);
				SimpleDateFormat d = new SimpleDateFormat("dd.MMM.yyyy HH:mm:ss");
				Object o[] = new Object[5];
				o[0] = prcMainHeader.getPpmNomorRfq();
				o[1] = prcMainHeader.getPpmSubject();
				o[2] = d.format(prep.getRegistrastionClosing());
				o[3] = prep.getAanwijzingDate() != null ? d.format(prep.getAanwijzingDate()) : "";
				o[4] = d.format(prep.getQuotationOpening());
				String message = SessionGetter.getPropertiesValue("email.eproc.undangvendor", o);
				EmailSessionBean.sendMail("eProc PT. Semen Baturaja (Persero)", message, recipients);
			}

			PrcMainPreparation prep = service.getPrcMainPreparationByHeader(prcMainHeader);
			if (SessionGetter.isNotNull(prep.getAanwijzingDate())) {
				prcMainHeader.setPpmProsesLevel(2);// aanwijzing
			} else {
				prcMainHeader.setPpmProsesLevel(3);// pembukaan panwaran
				histMainComment.setCreatedBy(SessionGetter.getPropertiesValue("rfq.3", null));
			}
			saveEntity(prcMainHeader);

			// upload doc
			if (SessionGetter.isNotNull(docsKomentar)) {
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}

			// upload history
			SessionGetter.saveHistoryGlobalPrOE(histMainComment, prcMainHeader, session);

			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public PrcMainVendor getPrcMainVendorById(PrcMainVendorId id) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMainVendor.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		DetachedCriteria sb = crit.createCriteria("vndHeader", JoinType.INNER_JOIN);
		sb.add(Restrictions.isNotNull("vendorId"));
		crit.add(Restrictions.idEq(id));
		return (PrcMainVendor) getEntityByDetachedCiteria(crit);
	}

	@Override
	public void saveAanwijzing(PrcMainHeader prcMainHeader, HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName, List<PrcMainVendor> listVendor) throws Exception {
		try {
			startTransaction();
			prcMainHeader = getPrcMainHeaderLite(prcMainHeader.getPpmId());

			// vendor
			if (listVendor != null) {
				for (PrcMainVendor v : listVendor) {
					Integer present = v.getPmpAanwijzingPresent();
					v = getPrcMainVendorById(v.getId());
					v.setPmpAanwijzingPresent(present);// hadir aanwijzing
					saveEntity(v);
				}
			}
			prcMainHeader.setPpmProsesLevel(3);// pembukaan panwaran
			saveEntity(prcMainHeader);

			// upload doc
			if (SessionGetter.isNotNull(docsKomentar)) {
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}

			// upload history
			SessionGetter.saveHistoryGlobalPrOE(histMainComment, prcMainHeader, session);

			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> listPrcMainHeaderAanwijzing(Object object) throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		dc.add(Restrictions.eq("admUserByPpmCurrentUser", SessionGetter.getUser()));
		dc.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
		dc.add(Restrictions.eq("ppmProsesLevel", 2));
		DetachedCriteria sb = dc.createCriteria("prcMainPreparation", JoinType.INNER_JOIN);
		sb.add(Restrictions.le("aanwijzingDate", new Date()));
		return (List<PrcMainHeader>) getListByDetachedCiteria(dc);
	}

	@Override
	public void saveVerifikasiAdm(PrcMainVendorQuote quote, List<PrcMainVendorQuoteAdmtech> listAdministrasi,
			PrcMainVendor prcMainVendor) throws Exception {
		try {
			startTransaction();
			// save main vendor
			PrcMainVendor v = getPrcMainVendorById(prcMainVendor.getId());
			v.setPmpStatus(prcMainVendor.getPmpStatus());
			v.setPmpAdmStatus(prcMainVendor.getPmpAdmStatus());
			v.setPmpAdmNotes(prcMainVendor.getPmpAdmNotes());
			saveEntity(v);

			// save item administrasi
			if (listAdministrasi != null && listAdministrasi.size() > 0) {
				for (PrcMainVendorQuoteAdmtech c : listAdministrasi) {
					saveEntity(c);
				}
			}
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> listPrcMainHeaderOpening(Object object) throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		dc.add(Restrictions.eq("admUserByPpmCurrentUser", SessionGetter.getUser()));
		dc.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
		dc.add(Restrictions.eq("ppmProsesLevel", 3));
		DetachedCriteria sb = dc.createCriteria("prcMainPreparation", JoinType.INNER_JOIN);
		sb.add(Restrictions.le("quotationOpening", new Date()));
		return (List<PrcMainHeader>) getListByDetachedCiteria(dc);
	}

	@Override
	public void saveOpening(PrcMainHeader prcMainHeader, HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception {
		try {
			startTransaction();
			prcMainHeader = getPrcMainHeaderLite(prcMainHeader.getPpmId());

			prcMainHeader.setPpmProsesLevel(4);// evaluasi panwaran
			saveEntity(prcMainHeader);

			// upload doc
			if (SessionGetter.isNotNull(docsKomentar)) {
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}

			// upload history
			SessionGetter.saveHistoryGlobalPrOE(histMainComment, prcMainHeader, session);

			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> listPrcMainHeaderEvaluasi(Object object) throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		dc.add(Restrictions.eq("admUserByPpmCurrentUser", SessionGetter.getUser()));
		dc.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
		dc.add(Restrictions.eq("ppmProsesLevel", 4));
		DetachedCriteria sb = dc.createCriteria("prcMainPreparation", JoinType.INNER_JOIN);
		sb.add(Restrictions.le("quotationOpening", new Date()));
		return (List<PrcMainHeader>) getListByDetachedCiteria(dc);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<TabulasiPenilaian> getListTabulasi(String query) throws Exception {
		try {

			session = HibernateUtil.getSession();
			Query q = session.createSQLQuery(query);
			List<TabulasiPenilaian> retMe = new ArrayList<TabulasiPenilaian>();
			List<Object> tmp = q.list();
			if (SessionGetter.isNotNull(tmp)) {
				for (Object o : tmp) {
					Object obj[] = (Object[]) o;
					TabulasiPenilaian c = new TabulasiPenilaian();
					c.setVendorId(Integer.valueOf(SessionGetter.objectToString(obj[0])));
					c.setVendorName(SessionGetter.objectToString(obj[1]));
					c.setPmpAdmStatus(Integer.valueOf(SessionGetter.objectToString(obj[2])));
					c.setPassingGrade(new BigDecimal(SessionGetter.objectToStringNumber(obj[3])));
					c.setBobotTeknis(new BigDecimal(SessionGetter.objectToStringNumber(obj[4])));
					c.setNilaiTeknis(new BigDecimal(SessionGetter.objectToStringNumber(obj[5])));
					c.setBobotHarga(new BigDecimal(SessionGetter.objectToStringNumber(obj[6])));
					c.setNilaiHarga(new BigDecimal(SessionGetter.objectToStringNumber(obj[7])));
					c.setTotalPenawaran(new BigDecimal(SessionGetter.objectToStringNumber(obj[8])));
					c.setStatusTeknis(Integer.valueOf(SessionGetter.objectToStringNumber(obj[9])));
					c.setCatatanTeknis((String) (obj[10]));
					c.setStatusHarga(Integer.valueOf(SessionGetter.objectToStringNumber(obj[11])));
					c.setCatatanHarga((String) (obj[12]));
					
					
					c.setPpmId(Integer.valueOf(SessionGetter.objectToStringNumber(obj[14])));
					Double teknis = (c.getBobotTeknis().doubleValue() * c.getNilaiTeknis().doubleValue() / 100);
					Double harga = (c.getBobotHarga().doubleValue() * c.getNilaiHarga().doubleValue() / 100);
					Double totalEval = teknis + harga;
					c.setTotalNilai(new BigDecimal(totalEval));
					c.setTotalPenawaranNego(new BigDecimal(SessionGetter.objectToStringNumber(obj[15])));
					c.setTotalPenawaranEauction(new BigDecimal(SessionGetter.objectToStringNumber(obj[16])));
					c.setPriceEvaluated(Integer.valueOf(SessionGetter.objectToStringNumber(obj[17])));
					c.setTechEvaluated(Integer.valueOf(SessionGetter.objectToStringNumber(obj[18])));
					retMe.add(c);
				}
			}
			Collections.sort(retMe, new GlobalComparator(new TabulasiPenilaian()));
			session.clear();
			session.close();
			return retMe;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Override
	public void saveVerifikasiTeknis(PrcMainVendorQuote quote, List<PrcMainVendorQuoteAdmtech> listTeknis,
			PrcMainVendor prcMainVendor) throws Exception {
		try {
			startTransaction();
			// save main vendor
			PrcMainVendor v = getPrcMainVendorById(prcMainVendor.getId());
			v.setPmpStatus(prcMainVendor.getPmpStatus());
			v.setPmpTechnicalNotes(prcMainVendor.getPmpTechnicalNotes());
			v.setPmpTechnicalStatus(prcMainVendor.getPmpTechnicalStatus());
			v.setPmpTechnicalValue(prcMainVendor.getPmpTechnicalValue());
			v.setPmpEvalTotal(prcMainVendor.getPmpEvalTotal());
			v.setTechEvaluated(1);
			saveEntity(v);

			// save item administrasi
			if (listTeknis != null && listTeknis.size() > 0) {
				for (PrcMainVendorQuoteAdmtech c : listTeknis) {
					saveEntity(c);
				}
			}
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public void saveVerifikasiHarga(List<PrcMainVendor> listPrcMainVendor) throws Exception {
		try {
			startTransaction();
			if (SessionGetter.isNotNull(listPrcMainVendor) && SessionGetter.isNotEmptyList(listPrcMainVendor)) {
				for (PrcMainVendor v : listPrcMainVendor) {
					PrcMainVendor data = getPrcMainVendorById(v.getId());
					System.out.println(">>> BID " + v.getValidityBidbond());
					System.out.println(">>> OFF " + v.getValidityOffer());
					if (v.getValidityBidbond() != null && v.getValidityBidbond() == 1 && v.getValidityOffer() != null
							&& v.getValidityOffer() == 1) {
						data.setPmpStatus(7);
						data.setPmpPriceStatus(1);
					} else {
						data.setPmpStatus(-7);
						data.setPmpPriceStatus(-1);
					}
					data.setPmpPriceNotes(v.getPmpPriceNotes());
					data.setPmpPriceValue(v.getPmpPriceValue());
					data.setPmpEvalTotal(v.getPmpEvalTotal());
					data.setValidityBidbond(v.getValidityBidbond());
					data.setValidityOffer(v.getValidityOffer());
					data.setPriceEvaluated(1);
					saveEntity(data);
				}
			}
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public void saveEvaluasi(PrcMainHeader prcMainHeader, HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception {
		try {
			startTransaction();
			prcMainHeader = getPrcMainHeaderLite(prcMainHeader.getPpmId());
			prcMainHeader.setStatusEvaluasi(2);
			prcMainHeader.setPpmProsesLevel(5);// next to negosiasi
			saveEntity(prcMainHeader);

			// upload doc
			if (SessionGetter.isNotNull(docsKomentar)) {
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}

			// upload history
			SessionGetter.saveHistoryGlobalPrOE(histMainComment, prcMainHeader, session);

			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public void saveEvaluasiSampul1(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName, PrcMainPreparation preparation, List<PrcMainVendor> listVendorLulus, 
			List<PrcMainVendor> listVendorTidakLulus) throws Exception{
		try {
			startTransaction();
			PrcService service = new PrcServiceImpl();
			prcMainHeader = getPrcMainHeaderLite(prcMainHeader.getPpmId());
			prcMainHeader.setStatusEvaluasi(1);
			prcMainHeader.setPpmProsesLevel(4);//evaluasi panwaran
			if(prcMainHeader.getPrcMethod().getId() == 6){
				//get Quotation Opening 2
				PrcMainPreparation prep = service.getPrcMainPreparationByHeader(prcMainHeader);
				prep.setQuotationOpening2(preparation.getQuotationOpening2());
				saveEntity(prep);
				prcMainHeader.setPpmProsesLevel(3);//bid opening 2
				prcMainHeader.setTahap(1);
				histMainComment.setCreatedBy(SessionGetter.getPropertiesValue("rfq.3", null));
			}
			saveEntity(prcMainHeader);
			
			AdmProsesHirarki ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel(), 
					prcMainHeader.getLokasiPengadaan(), prcMainHeader.getPpmJumlahOe().doubleValue());
			if(ahp==null){
				throw new Exception("Hirarki Proses Belum Di Setup");
			}
			
			if(prcMainHeader.getPrcMethod().getId() == 6){
				//undang vendor bid opening 2 
				//lulus teknis
				if(listVendorLulus!=null){
					
					String recipients[] = new String[listVendorLulus.size()];
					int i=0;
					for(PrcMainVendor v:listVendorLulus){
						v = getPrcMainVendorById(v.getId());
						System.out.println(v);
						recipients[i] = v.getVndHeader().getVendorEmail();
						i++;
					
					}
					
				//send email
				SimpleDateFormat d = new SimpleDateFormat("dd.MMM.yyyy HH:mm:ss");
				Object o[] = new Object[3];
				o[0] = prcMainHeader.getPpmNomorRfq();
				o[1] = prcMainHeader.getPpmSubject();
				o[2] = d.format(preparation.getQuotationOpening2());
				String message = SessionGetter.getPropertiesValue("email.eproc.lulusteknis1", o);
				EmailSessionBean.sendMail("eProc PT. Semen Baturaja (Persero)", message, recipients);
				}
			} else {

				//lulus teknis
				if(listVendorLulus!=null){
					
					String recipients[] = new String[listVendorLulus.size()];
					int i=0;
					for(PrcMainVendor v:listVendorLulus){
						v = getPrcMainVendorById(v.getId());
						System.out.println(v);
						recipients[i] = v.getVndHeader().getVendorEmail();
						i++;
					
					}

				//send email
				Object o[] = new Object[2];
				o[0] = prcMainHeader.getPpmNomorRfq();
				o[1] = prcMainHeader.getPpmSubject();
				String message = SessionGetter.getPropertiesValue("email.eproc.lulusteknis2", o);
				EmailSessionBean.sendMail("eProc PT. Semen Baturaja (Persero)", message, recipients);
				}
			}
			
			
			
			
			
			
			//tidak lulus teknis
			if (listVendorTidakLulus != null) {

				String recipients[] = new String[listVendorTidakLulus.size()];
				int i = 0;
				for (PrcMainVendor v : listVendorTidakLulus) {
					v = getPrcMainVendorById(v.getId());
					recipients[i] = v.getVndHeader().getVendorEmail();
					i++;

				}

				// send email
				Object o[] = new Object[2];
				o[0] = prcMainHeader.getPpmNomorRfq();
				o[1] = prcMainHeader.getPpmSubject();
				String message = SessionGetter.getPropertiesValue("email.eproc.tidaklulusteknis", o);
				EmailSessionBean.sendMail("eProc PT. Semen Baturaja (Persero)", message, recipients);
			}
			//upload doc
			if(SessionGetter.isNotNull(docsKomentar)){
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}
			
			
			
			//upload history
			
			SessionGetter.saveHistoryGlobalPrOE(histMainComment, prcMainHeader, session);
			
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	private PrcMainPreparation getPreparationByPpmId(Long ppmId) {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMainPreparation.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("ppmId", ppmId));
		return (PrcMainPreparation) getEntityByDetachedCiteria(crit);
	}

	@Override
	public void saveEvaluasiSampul2(PrcMainHeader prcMainHeader,
			HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName, List<PrcMainVendor> listVendorLulus, 
			List<PrcMainVendor> listVendorTidakLulus) throws Exception {
		try {
			startTransaction();
			prcMainHeader = getPrcMainHeaderLite(prcMainHeader.getPpmId());
			prcMainHeader.setStatusEvaluasi(2);
			prcMainHeader.setPpmProsesLevel(5);//next to negosiasi
			saveEntity(prcMainHeader);
			
			//lulus teknis
			if(listVendorLulus!=null){
				
				String recipients[] = new String[listVendorLulus.size()];
				int i=0;
				for(PrcMainVendor v:listVendorLulus){
					v = getPrcMainVendorById(v.getId());
					System.out.println(v);
					recipients[i] = v.getVndHeader().getVendorEmail();
					i++;
				
				}
			
			
			
				
			
			//send email
			
			Object o[] = new Object[2];
			o[0] = prcMainHeader.getPpmNomorRfq();
			o[1] = prcMainHeader.getPpmSubject();
			String message = SessionGetter.getPropertiesValue("email.eproc.lulusharga", o);
			EmailSessionBean.sendMail("eProc PT. Semen Baturaja (Persero)", message, recipients);
			}
			
			//tidak lulus teknis
			if (listVendorTidakLulus != null) {

				String recipients[] = new String[listVendorTidakLulus.size()];
				int i = 0;
				for (PrcMainVendor v : listVendorTidakLulus) {
					v = getPrcMainVendorById(v.getId());
					recipients[i] = v.getVndHeader().getVendorEmail();
					i++;

				}

				// send email
				Object o[] = new Object[2];
				o[0] = prcMainHeader.getPpmNomorRfq();
				o[1] = prcMainHeader.getPpmSubject();
				String message = SessionGetter.getPropertiesValue("email.eproc.tidaklulusharga", o);
				EmailSessionBean.sendMail("eProc PT. Semen Baturaja (Persero)", message, recipients);
			}
			
			
			
			
			
			//tidak lulus teknis
			if (listVendorTidakLulus != null) {

				String recipients[] = new String[listVendorTidakLulus.size()];
				int i = 0;
				for (PrcMainVendor v : listVendorTidakLulus) {
					v = getPrcMainVendorById(v.getId());
					recipients[i] = v.getVndHeader().getVendorEmail();
					i++;

				}

				// send email
				Object o[] = new Object[2];
				o[0] = prcMainHeader.getPpmNomorRfq();
				o[1] = prcMainHeader.getPpmSubject();
				String message = SessionGetter.getPropertiesValue("email.eproc.tidaklulusteknis", o);
				EmailSessionBean.sendMail("eProc PT. Semen Baturaja (Persero)", message, recipients);
			}
			
			//upload doc
			if(SessionGetter.isNotNull(docsKomentar)){
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}
			
			//upload history
			SessionGetter.saveHistoryGlobalPrOE(histMainComment, prcMainHeader, session);
			
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMsgNegotiation> listNegosiasiByMainHeader(PrcMainHeader prcMainHeader) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMsgNegotiation.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("prcMainHeader", prcMainHeader));
		crit.addOrder(Order.asc("msgDate"));
		return (List<PrcMsgNegotiation>) getListByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> listPrcMainHeaderNegosiasi(Object object) throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		dc.add(Restrictions.eq("admUserByPpmCurrentUser", SessionGetter.getUser()));
		dc.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
		dc.add(Restrictions.eq("ppmProsesLevel", 5));
		DetachedCriteria sb = dc.createCriteria("prcMainPreparation", JoinType.INNER_JOIN);
		sb.add(Restrictions.le("quotationOpening", new Date()));
		return (List<PrcMainHeader>) getListByDetachedCiteria(dc);
	}

	@Override
	public void saveNegosiasi(PrcMainVendor prcMainVendor, PrcMsgNegotiation negosiasi) throws Exception {
		try {
			startTransaction();

			prcMainVendor = getPrcMainVendorById(prcMainVendor.getId());
			prcMainVendor.setPmpIsNegotatiate(1);
			saveEntity(prcMainVendor);

			negosiasi.setMsgFrom("PEMBELI");
			negosiasi.setMsgDate(new Date());
			saveEntity(negosiasi);

			PrcMainHeader h = getPrcMainHeaderLite(prcMainVendor.getId().getPpmId().longValue());

			// send mail to vendor
			// send email
			Object o[] = new Object[4];
			o[0] = h.getPpmNomorRfq();
			o[1] = h.getPpmSubject();
			o[2] = prcMainVendor.getVndHeader().getVendorName().toUpperCase();
			o[3] = h.getPpmNomorRfq();
			String message = SessionGetter.getPropertiesValue("email.eproc.nego", o);
			EmailSessionBean.sendMail("eProc PT. Semen Baturaja (Persero)", message,
					prcMainVendor.getVndHeader().getVendorEmail());

			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public void saveNegosiasi(PrcMainHeader prcMainHeader, HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception {
		try {
			startTransaction();
			prcMainHeader = getPrcMainHeaderLite(prcMainHeader.getPpmId());
			prcMainHeader.setPpmProsesLevel(6);// next usulan pemenang
			saveEntity(prcMainHeader);

			// cek anggaran vs OE
			/*PrcAnggaranJdeHeader anggaranHead = new PrcServiceImpl().getJdeAnggaranHeaderByPpmId(prcMainHeader);
			if (anggaranHead.getKaacm1().doubleValue() != prcMainHeader.getPpmJumlahOe().doubleValue()) {
				RollbackAndClear();
				throw new Exception("Anggaran harus sama dengan OE");
			}*/

			// upload doc
			if (SessionGetter.isNotNull(docsKomentar)) {
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}

			// upload history
			SessionGetter.saveHistoryGlobalPrOE(histMainComment, prcMainHeader, session);

			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainVendor> listVndUsulanPemenang(PrcMainHeader prcMainHeader, double jumlahOe) throws Exception{		
			startTransaction();
				DetachedCriteria crit = DetachedCriteria.forClass(PrcMainVendor.class);
				crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
				crit.setFetchMode("vndHeader", FetchMode.JOIN);
				crit.add(Restrictions.eq("prcMainHeader", prcMainHeader));
				DetachedCriteria sb = crit.createCriteria("prcMainVendorQuote", JoinType.INNER_JOIN);
				sb.add(Restrictions.le("pqmTotalPenawaranNego", prcMainHeader.getPpmJumlahOe()));
				return (List<PrcMainVendor>) getListByDetachedCiteria(crit);
	}
			
//			Query q;
//			session = HibernateUtil.getSession();
//			
//				String query = "select a.vendor_id, a.ppm_id from prc_main_vendor_quote a "
//						+ "join vnd_header b on a.vendor_id = b.vendor_id "
//						+ "join prc_main_header c on a.ppm_id = c.ppm_id"
//						+ "where a.ppm_id = "+prcMainHeader.getPpmId() +" and a.pqm_total_penawaran_nego <= c.ppm_jumlah_oe";
//				q  = session.createSQLQuery(query);
//				List<Long> vndId = new ArrayList<Long>();
//				List<Object> tmp = q.list();
//				if (SessionGetter.isNotNull(tmp)) {
//					for (Object o : tmp) {
//						Object obj[] = (Object[]) o;
//						vndId.add(Long.valueOf(String.valueOf(obj[0])));
//						System.out.println(String.valueOf(vndId));
//					}
//				}
//				if(vndId.size()>0){
//					DetachedCriteria crit = DetachedCriteria.forClass(PrcMainVendor.class);
//					crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
//					crit.setFetchMode("vndHeader", FetchMode.JOIN);
//					crit.add(Restrictions.in("id.vendorId", vndId));
//					crit.add(Restrictions.eq("prcMainHeader", prcMainHeader));
//					listVendor.add((List<PrcMainVendor>) getListByDetachedCiteria(crit));
//				}else {
//					listVendor.add(null);
//				}
				
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> listPrcMainHeaderUsulanPemenang(Object object) throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		dc.add(Restrictions.eq("admUserByPpmCurrentUser", SessionGetter.getUser()));
		dc.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
		dc.add(Restrictions.eq("ppmProsesLevel", 6));
		DetachedCriteria sb = dc.createCriteria("prcMainPreparation", JoinType.INNER_JOIN);
		sb.add(Restrictions.le("quotationOpening", new Date()));
		return (List<PrcMainHeader>) getListByDetachedCiteria(dc);
	}

	@Override
	public void saveUsulanPemenang(PrcMainHeader prcMainHeader, HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName, List<PrcMainItem> listPrcMainItem) throws Exception {
		try {
			startTransaction();
			prcMainHeader = getPrcMainHeaderLite(prcMainHeader.getPpmId());

			AdmProsesHirarki ahp = SessionGetter.getHirarkiProsesJenisByDistrict(prcMainHeader);
			if (ahp != null) {
				if (ahp.getAdmRoleByCurrentPos() != null) {
					if(prcMainHeader.getPpmJenisDetail()!=null) {
						AdmUser user = new AdmUser();
						Integer tingkat = ahp.getTingkat();
						AdmUser manager = SessionGetter.getUserByJenis(prcMainHeader.getPpmJenisDetail(), 2).getAdmUser();
						
						if(prcMainHeader.getPpmNomorPrSap().contains("1201")) {
							user = SessionGetter.getUserByJenisKodeSatker(prcMainHeader.getPpmJenisDetail(), 2, 1201).getAdmUser();						
						}else if(prcMainHeader.getPpmNomorPrSap().contains("1202")){
							user = SessionGetter.getUserByJenisKodeSatker(prcMainHeader.getPpmJenisDetail(), 3, 1202).getAdmUser();							
							if(prcMainHeader.getPpmJumlahOe().doubleValue() > 20000000) {
								user = SessionGetter.getUserByJenisKodeSatker(prcMainHeader.getPpmJenisDetail(), 2, 1202).getAdmUser();
								tingkat = tingkat+1;
							}
						}else if(prcMainHeader.getPpmNomorPrSap().contains("1203")) {
							user = SessionGetter.getUserByJenisKodeSatker(prcMainHeader.getPpmJenisDetail(), 3, 1203).getAdmUser();
							if(prcMainHeader.getPpmJumlahOe().doubleValue() > 20000000) {
								user = SessionGetter.getUserByJenisKodeSatker(prcMainHeader.getPpmJenisDetail(), 2, 1203).getAdmUser();

								tingkat = tingkat+1;
							}
						}else if(prcMainHeader.getPpmNomorPrSap().contains("1204")) {
							user = SessionGetter.getUserByJenisKodeSatker(prcMainHeader.getPpmJenisDetail(), 3, 1204).getAdmUser();							
							if(prcMainHeader.getPpmJumlahOe().doubleValue() > 20000000) {
								user = SessionGetter.getUserByJenisKodeSatker(prcMainHeader.getPpmJenisDetail(), 2, 1204).getAdmUser();	
								tingkat = tingkat+1;
							}	
						}else if(prcMainHeader.getPpmNomorPrSap().contains("1601")) {
							user = SessionGetter.getUserByJenisKodeSatker(prcMainHeader.getPpmJenisDetail(), 3, 1601).getAdmUser();					
							if(prcMainHeader.getPpmJumlahOe().doubleValue() > 20000000) {
								user = SessionGetter.getUserByJenisKodeSatker(prcMainHeader.getPpmJenisDetail(), 2, 1601).getAdmUser();
								tingkat = tingkat+1;
							}	
						}
						/*if(prcMainHeader.getPpmNomorPrSap().contains("1201")) {
							if(prcMainHeader.getPpmJenisDetail().getId() == 3) {
								 user = SessionGetter.getEmployeeByRoleName("PGS MANAGER RAW MATERIAL PROCUREMENT TK. JUNIOR MANAGER").getAdmUser();
							}else if(prcMainHeader.getPpmJenisDetail().getId() == 4) {
								 user = SessionGetter.getEmployeeByRoleName("MANAGER MECANICAL & ELECTRICAL PROCUREMENT").getAdmUser();
							}else if(prcMainHeader.getPpmJenisDetail().getId() == 5) {
								user = SessionGetter.getEmployeeByRoleName("MANAGER GENERAL PROCUREMENT").getAdmUser();
							}else if(prcMainHeader.getPpmJenisDetail().getId() == 6) {
								user = SessionGetter.getEmployeeByRoleName("MANAGER GENERAL SERVICE PROCUREMENT").getAdmUser();
							}else if(prcMainHeader.getPpmJenisDetail().getId() == 7) {
								user = SessionGetter.getEmployeeByRoleName("PGS MANAGER TECHNICAL SERVICE PROCUREMENT TK. JUNIOR MANAGER").getAdmUser();
							}
							
						}else if(prcMainHeader.getPpmNomorPrSap().contains("1202") || prcMainHeader.getPpmNomorPrSap().contains("11202")) {
							System.out.println(prcMainHeader.getPpmJumlahOe().doubleValue() );
							if(prcMainHeader.getPpmJumlahOe().doubleValue() > 20000000) {
								if(prcMainHeader.getPpmJenisDetail().getId() == 3) {
									 user = SessionGetter.getEmployeeByRoleName("PGS MANAGER RAW MATERIAL PROCUREMENT TK. JUNIOR MANAGER").getAdmUser();
								}else if(prcMainHeader.getPpmJenisDetail().getId() == 4) {
									 user = SessionGetter.getEmployeeByRoleName("MANAGER MECANICAL & ELECTRICAL PROCUREMENT").getAdmUser();
								}else if(prcMainHeader.getPpmJenisDetail().getId() == 5) {
									user = SessionGetter.getEmployeeByRoleName("MANAGER GENERAL PROCUREMENT").getAdmUser();
								}else if(prcMainHeader.getPpmJenisDetail().getId() == 6) {
									user = SessionGetter.getEmployeeByRoleName("PGS MANAGER TECHNICAL SERVICE PROCUREMENT TK. JUNIOR MANAGER").getAdmUser();
								}else if(prcMainHeader.getPpmJenisDetail().getId() == 7) {
									user = SessionGetter.getEmployeeByRoleName("MANAGER GENERAL SERVICE PROCUREMENT").getAdmUser();
								}
								tingkat = tingkat + 1;
							}else {
								if(prcMainHeader.getPpmJenisDetail().getId() == 3 || prcMainHeader.getPpmJenisDetail().getId() == 4 || prcMainHeader.getPpmJenisDetail().getId() == 5) {
									 user = SessionGetter.getEmployeeByRoleName("JUNIR MANAGER SPAREPART PROC MECH & ELECT BTA").getAdmUser();
								}else if(prcMainHeader.getPpmJenisDetail().getId() == 6 || prcMainHeader.getPpmJenisDetail().getId() == 7) {
									 user = SessionGetter.getEmployeeByRoleName("JUNIOR MANAGER SERVICE PROCUREMENT BATURAJA").getAdmUser();
								}
							}*/
							
						/*}else if(prcMainHeader.getPpmNomorPrSap().contains("1204") || (prcMainHeader.getPpmNomorPrSap().contains("1203"))) {
							if(prcMainHeader.getPpmJumlahOe().doubleValue() > 20000000) {
								if(prcMainHeader.getPpmJenisDetail().getId() == 3) {
									 user = SessionGetter.getEmployeeByRoleName("PGS MANAGER RAW MATERIAL PROCUREMENT TK. JUNIOR MANAGER").getAdmUser();
									//user = SessionGetter.getEmployeeByRoleName("MANAGER RAW MATERIAL").getAdmUser();
								}else if(prcMainHeader.getPpmJenisDetail().getId() == 4) {
									 user = SessionGetter.getEmployeeByRoleName("MANAGER MECANICAL & ELECTRICAL PROCUREMENT").getAdmUser();
								}else if(prcMainHeader.getPpmJenisDetail().getId() == 5) {
									user = SessionGetter.getEmployeeByRoleName("MANAGER GENERAL PROCUREMENT").getAdmUser();
								}else if(prcMainHeader.getPpmJenisDetail().getId() == 6) {
									user = SessionGetter.getEmployeeByRoleName("PGS MANAGER TECHNICAL SERVICE PROCUREMENT TK. JUNIOR MANAGER").getAdmUser();
								}else if(prcMainHeader.getPpmJenisDetail().getId() == 7) {
									user = SessionGetter.getEmployeeByRoleName("MANAGER GENERAL SERVICE PROCUREMENT").getAdmUser();
								}
								tingkat = tingkat + 1;
							}else {
								 user = SessionGetter.getEmployeeByRoleName("JUNIOR MANAGER SEVICE MERANGKAP SPAREPART PROC PPJ").getAdmUser();
							}
						}*/
							
						
						prcMainHeader.setPengawasPp(manager);
						prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
						prcMainHeader.setPpmProsesLevel(tingkat);
						prcMainHeader.setAdmUserByPpmCurrentUser(user);
					}else {
						throw new Exception("Untuk menyesuaikan SOP yang baru, Jenis Permintaan harus diupdate agar bisa melanjutkan");
					}
					
					
				}
			}
			saveEntity(prcMainHeader);

			if (listPrcMainItem != null) {
				PrcService service = new PrcServiceImpl();
				for (PrcMainItem i : listPrcMainItem) {
					PrcMainItem pmi = service.getPrcMainItemById(i.getId());
					pmi.setVndHeader(i.getVndHeader());
					saveEntity(pmi);
				}
			}

			// upload doc
			if (SessionGetter.isNotNull(docsKomentar)) {
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}

			// upload history
			SessionGetter.saveHistoryGlobalPrOE(histMainComment, prcMainHeader, session);

			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public void savePersetujuanPemenang(PrcMainHeader prcMainHeader, HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName) throws Exception {
		try {
			startTransaction();
			prcMainHeader = getPrcMainHeaderSPPHById(prcMainHeader.getPpmId());
			
			AdmProsesHirarki ahp = SessionGetter.getHirarkiProsesJenisByDistrict(prcMainHeader);
					if (ahp != null) {
						if (ahp.getAdmRoleByCurrentPos() != null) {
							if (ahp.getAdmRoleByCurrentPos().getRoleName().trim().equalsIgnoreCase("panitia pengadaan")) {
								prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
								prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
								prcMainHeader.setAdmUserByPpmCurrentUser(
										SessionGetter.getEmployeeByRole(ahp.getAdmRoleByCurrentPos()).getAdmUser());
							} else if (ahp.getAdmRoleByCurrentPos().getRoleName().trim().equalsIgnoreCase("pembeli")){
								prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
								prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
								prcMainHeader.setAdmUserByPpmCurrentUser(
										prcMainHeader.getAdmUserByPpmBuyer());
								prcMainHeader.setFinalisasiSpph(2);// flag ke kontrak
							}else if (ahp.getAdmRoleByCurrentPos().getRoleName().trim().equalsIgnoreCase("pengawas pp")){
								if(ahp.getAdmRoleByNextPos().getRoleName().trim().equalsIgnoreCase("pengawas pp")) {//untuk diatas 50 juta
									if(ahp.getAdmDistrict().getId() == 107 || ahp.getAdmDistrict().getId() == 110) {
										  /*AdmUser user = new AdmUser();
											if(prcMainHeader.getPpmJenisDetail().getId() == 3) {
												 user = SessionGetter.getEmployeeByRoleName("PGS MANAGER RAW MATERIAL PROCUREMENT TK. JUNIOR MANAGER").getAdmUser();
											}else if(prcMainHeader.getPpmJenisDetail().getId() == 4) {
												 user = SessionGetter.getEmployeeByRoleName("MANAGER MECANICAL & ELECTRICAL PROCUREMENT").getAdmUser();
											}else if(prcMainHeader.getPpmJenisDetail().getId() == 5) {
												user = SessionGetter.getEmployeeByRoleName("MANAGER GENERAL PROCUREMENT").getAdmUser();
											}else if(prcMainHeader.getPpmJenisDetail().getId() == 6) {
												user = SessionGetter.getEmployeeByRoleName("PGS MANAGER TECHNICAL SERVICE PROCUREMENT TK. JUNIOR MANAGER").getAdmUser();
											}else if(prcMainHeader.getPpmJenisDetail().getId() == 7) {
												user = SessionGetter.getEmployeeByRoleName("MANAGER GENERAL SERVICE PROCUREMENT").getAdmUser();
											}*/
											AdmUser user = SessionGetter.getUserByJenis(prcMainHeader.getPpmJenisDetail(), 2).getAdmUser();
											prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
											prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
											prcMainHeader.setAdmUserByPpmCurrentUser(user);
									}
									prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
									prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
									prcMainHeader.setAdmUserByPpmCurrentUser(
											SessionGetter.getEmployeeByRole(ahp.getAdmRoleByCurrentPos()).getAdmUser());
								}else {
									prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
									prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
									prcMainHeader.setAdmUserByPpmCurrentUser(
											SessionGetter.getEmployeeByRole(ahp.getAdmRoleByCurrentPos()).getAdmUser());
								}
								
							}else {
								prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
								System.out.println(">>>>> " + ahp.getAdmRoleByCurrentPos().getRoleName());
								prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
								prcMainHeader.setAdmUserByPpmCurrentUser(
										SessionGetter.getEmployeeByRole(ahp.getAdmRoleByCurrentPos()).getAdmUser());
								
							}
						}
					}
				
				
				
			

			saveEntity(prcMainHeader);

			// upload doc
			if (SessionGetter.isNotNull(docsKomentar)) {
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}

			// upload history
			SessionGetter.saveHistoryGlobalPrOE(histMainComment, prcMainHeader, session);

			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public List<PrcMainVendor> getListMainVendorNotWinner(List<VndHeader> lVh, PrcMainHeader prcMainHeader) {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMainVendor.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("vndHeader", FetchMode.JOIN);
		crit.add(Restrictions.eq("prcMainHeader", prcMainHeader));
		crit.add(Restrictions.not(Restrictions.in("vndHeader", lVh)));
		return (List<PrcMainVendor>) getListByDetachedCiteria(crit);
	}

	@Override
	public void savePersetujuanPemenangPanitia(PrcMainHeader prcMainHeader, HistMainComment histMainComment,
			File docsKomentar, String docsKomentarFileName) throws Exception {
		try {
			startTransaction();
			prcMainHeader = getPrcMainHeaderSPPHById(prcMainHeader.getPpmId());
			AdmDistrict kantor = new AdmDistrict();
			
			if(prcMainHeader.getPpmNomorPrSap().contains("1201")) {
				kantor = new AdmDistrict(104);
				kantor = new AdmServiceImpl().getDistrictById(kantor);
			}else if(prcMainHeader.getPpmNomorPrSap().contains("1202")  || prcMainHeader.getPpmNomorPrSap().contains("1203")  || (prcMainHeader.getPpmNomorPrSap().contains("1601"))) {
				kantor = new AdmDistrict(107);
				kantor = new AdmServiceImpl().getDistrictById(kantor);
			}else if(prcMainHeader.getPpmNomorPrSap().contains("1204")) {
				kantor = new AdmDistrict(110);
				kantor = new AdmServiceImpl().getDistrictById(kantor);
			}
			/*if(prcMainHeader.getPpmNomorPrSap().contains("1201")) {
				kantor = new AdmDistrict(104);
				kantor = new AdmServiceImpl().getDistrictById(kantor);
			}else if(prcMainHeader.getPpmNomorPrSap().contains("1202") || prcMainHeader.getPpmNomorPrSap().contains("11202")) {
				kantor = new AdmDistrict(107);
				kantor = new AdmServiceImpl().getDistrictById(kantor);
			}else if(prcMainHeader.getPpmNomorPrSap().contains("1204")) {
				kantor = new AdmDistrict(110);
				kantor = new AdmServiceImpl().getDistrictById(kantor);
			}*/
			
			AdmProsesHirarki ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(),
					prcMainHeader.getPpmProsesLevel(), kantor,
					prcMainHeader.getPpmJumlahOe().doubleValue());
			
			if (ahp != null) {
				if (ahp.getAdmRoleByCurrentPos() != null) {
//						prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
//						prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
//						prcMainHeader.setAdmUserByPpmCurrentUser(
//								SessionGetter.getEmployeeByRole(ahp.getAdmRoleByCurrentPos()).getAdmUser());

						// update status
						DetachedCriteria cr = DetachedCriteria.forClass(PanitiaPengadaanDetail.class);
						cr.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
						cr.add(Restrictions.eq("panitia", prcMainHeader.getPanitia()));
						cr.add(Restrictions.eq("status", 1));
						@SuppressWarnings("unchecked")
						List<PanitiaPengadaanDetail> lst = (List<PanitiaPengadaanDetail>) getListByDetachedCiteria(cr);
						List<PanitiaPengadaanDetail> lstTmp = new ArrayList<PanitiaPengadaanDetail>();
						for (PanitiaPengadaanDetail pd : lst) {
							if (pd.getAnggota().getId().intValue() == SessionGetter.getUser().getId().intValue()) {
								pd.setStatus(2);
								saveEntity(pd);
							}
							lstTmp.add(pd);
						}

						int countPanitia = 0;
						if (lstTmp != null && lstTmp.size() > 0) {
							for (PanitiaPengadaanDetail a : lstTmp) {
								if (a.getStatus() == 1) {
									countPanitia += 1;
								}
							}
						}

						if (countPanitia == 0 || lst.size() == 1) {
							ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(),
									prcMainHeader.getPpmProsesLevel() + 1, prcMainHeader.getAdmDistrict(),
									prcMainHeader.getPpmJumlahOe().doubleValue());
							if (ahp.getAdmRoleByCurrentPos() != null) {
								if (ahp.getAdmRoleByCurrentPos().getRoleName().trim().equalsIgnoreCase("pembeli")) {
									prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
									prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
									prcMainHeader.setAdmUserByPpmCurrentUser(prcMainHeader.getAdmUserByPpmBuyer());
									prcMainHeader.setFinalisasiSpph(2);// flag ke kontrak
									sendEmailToVendor(prcMainHeader);
								} else if (StringUtils.equalsIgnoreCase(ahp.getAdmRoleByCurrentPos().getRoleName(),
										"DIR TERKAIT")) {
									if (prcMainHeader.getSatkerPeminta() == null) {
										throw new Exception("Satker Peminta Kosong, silahkan Update via Update Tools");
									} else {
										AdmDept satkerPeminta = new AdmServiceImpl()
												.getAdmDeptById(prcMainHeader.getSatkerPeminta());
										prcMainHeader.setAdmRole(satkerPeminta.getDirektur());
										prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
										prcMainHeader.setAdmUserByPpmCurrentUser(SessionGetter
												.getEmployeeByRole(satkerPeminta.getDirektur()).getAdmUser());
									}
								} else {
									prcMainHeader.setAdmRole(ahp.getAdmRoleByCurrentPos());
									prcMainHeader.setPpmProsesLevel(ahp.getTingkat());
									prcMainHeader.setAdmUserByPpmCurrentUser(
											SessionGetter.getEmployeeByRole(ahp.getAdmRoleByCurrentPos()).getAdmUser());
								}
							}
						}
					
				}
			}
			else {
	            throw new Exception("Hirarki Tidak Ditemukan");
	          }
			saveEntity(prcMainHeader);

			// upload doc
			if (SessionGetter.isNotNull(docsKomentar)) {
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}

			// upload history
			SessionGetter.saveHistoryGlobalPanitia(histMainComment, prcMainHeader, session);

			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> listPrcMainHeaderPersetujuanUsulanPemenang(Object object) throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		dc.add(Restrictions.eq("admUserByPpmCurrentUser", SessionGetter.getUser()));

		dc.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
		dc.add(Restrictions.gt("ppmProsesLevel", 6));
		DetachedCriteria sb = dc.createCriteria("prcMainPreparation", JoinType.INNER_JOIN);
		sb.add(Restrictions.le("quotationOpening", new Date()));

		List<PrcMainHeader> retMe = (List<PrcMainHeader>) getListByDetachedCiteria(dc);
		List<PrcMainHeader> listPanitia = listPrcMainHeaderPersetujuanPemenangPanitia(object);
		retMe.addAll(listPanitia);
		return retMe;
	}

	@SuppressWarnings("unchecked")
	private List<PrcMainHeader> listPrcMainHeaderPersetujuanPemenangPanitia(Object object) {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		dc.add(Restrictions.ne("admUserByPpmCurrentUser", SessionGetter.getUser()));
		DetachedCriteria ss = dc.createCriteria("panitia", JoinType.INNER_JOIN);
		DetachedCriteria sc = ss.createCriteria("panitiaPengadaanDetails", JoinType.INNER_JOIN);
		sc.add(Restrictions.and(Restrictions.eq("anggota", SessionGetter.getUser()), Restrictions.eq("status", 1)));

		dc.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
		dc.add(Restrictions.gt("ppmProsesLevel", 6));
		DetachedCriteria sb = dc.createCriteria("prcMainPreparation", JoinType.INNER_JOIN);
		sb.add(Restrictions.le("quotationOpening", new Date()));
		return (List<PrcMainHeader>) getListByDetachedCiteria(dc);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> listPrcMainHeaderPersetujuanUsulanPemenang2(Object object) throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
		dc.setFetchMode("admDept", FetchMode.JOIN);
		dc.add(Restrictions.eq("admUserByPpmCurrentUser", SessionGetter.getUser()));
		dc.add(Restrictions.eq("admProses", SessionGetter.PANITIA_PP_PROSES_ID));
		dc.add(Restrictions.gt("ppmProsesLevel", 1));
		DetachedCriteria sb = dc.createCriteria("prcMainPreparation", JoinType.INNER_JOIN);
		sb.add(Restrictions.le("quotationOpening", new Date()));
		DetachedCriteria pan = dc.createCriteria("panitia", JoinType.INNER_JOIN);
		DetachedCriteria panAng = pan.createCriteria("");

		return (List<PrcMainHeader>) getListByDetachedCiteria(dc);
	}

	@Override
	public void revisiPemenang(PrcMainHeader prcMainHeader, HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName, List<PrcMainItem> listPrcMainItem) throws Exception {
		try {
			startTransaction();
			prcMainHeader = getPrcMainHeaderLite(prcMainHeader.getPpmId());
			prcMainHeader.setPpmProsesLevel(6);// usulan pemenang
			prcMainHeader.setAdmUserByPpmCurrentUser(prcMainHeader.getAdmUserByPpmBuyer());
			prcMainHeader.setFinalisasiSpph(1);
			saveEntity(prcMainHeader);

			// jika ada panitia reset all member jadi 1
			if (prcMainHeader.getPanitia() != null) {
				List<PanitiaPengadaanDetail> listPan = new PrcServiceImpl()
						.getListPanitiaPengadaanDetail(prcMainHeader.getPanitia());
				if (listPan != null) {
					for (PanitiaPengadaanDetail d : listPan) {
						d.setStatus(1);
						saveEntity(d);
					}
				}
			}

			// upload doc
			if (SessionGetter.isNotNull(docsKomentar)) {
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}

			// upload history
			SessionGetter.saveHistoryGlobalPrOE(histMainComment, prcMainHeader, session);

			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}

	}

	@Override
	public void finalisasiPemenang(PrcMainHeader prcMainHeader, HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName, List<PrcMainItem> listPrcMainItem) throws Exception {
		try {
			startTransaction();
			prcMainHeader = getPrcMainHeaderSPPHByPpmId(prcMainHeader.getPpmId());
			prcMainHeader.setPpmProsesLevel(99);// Selesai

			// create kontrak
			List<Long> listVendor = getListPemenang(prcMainHeader.getPpmId(), session);
			PrcService service = new PrcServiceImpl();
			String rcpt[] = new String[listVendor.size()];
			int i = 0;
			for (Long vid : listVendor) {
				VndHeader vh = new VendorServiceImpl().getVndHeaderById(vid);
				vh.setVendorId(vid);
				rcpt[i] = vh.getVendorEmail();
				i++;

				PrcMainVendor pmv = new PrcMainVendor();
				PrcMainVendorId id = new PrcMainVendorId();
				id.setPpmId(new BigDecimal(prcMainHeader.getPpmId()));
				id.setVendorId(vid.longValue());
				pmv.setId(id);

				PrcMainVendorQuote quote = service.getQuoteByVendor(pmv);
				// create header
				CtrContractHeader ch = new CtrContractHeader();
				ch.setAdmDistrict(prcMainHeader.getAdmDistrict());
				ch.setAdmProses(SessionGetter.KONTRAK_PROSES_ID);
				ch.setContractProsesLevel(2);
				ch.setAdmUserByContractCreator(prcMainHeader.getAdmUserByPpmBuyer());
				ch.setContractAmount(
						(quote.getPqmTotalPenawaranNego() != null && quote.getPqmTotalPenawaranNego().doubleValue() > 0)
								? quote.getPqmTotalPenawaranNego() : quote.getPqmTotalPenawaran());// amount
				ch.setPrcMainHeader(prcMainHeader);
				ch.setVndHeader(vh);
				ch.setNomorPr(prcMainHeader.getPpmNomorPr());
				// String nomorContract =
				// prcMainHeader.getPpmNomorRfq().replaceAll("OQ", "OP");
				// if(nomorContract.contains("-")){
				//
				// ch.setContractNumber("");
				// }else
				if (StringUtils.isBlank(prcMainHeader.getPpmNomorRfq())) {
					prcMainHeader.setPpmNomorRfq(prcMainHeader.getPpmNomorPrSap().contains("ZMN")
							? StringUtils.replace(prcMainHeader.getPpmNomorPr(), "ZMN", "RFQ")
							: StringUtils.replace(prcMainHeader.getPpmNomorPr(), "ZST", "RFQ"));
				}

				if (prcMainHeader.getPpmNomorRfq().contains("RFQ")) {
					ch.setContractNumber(prcMainHeader.getPpmNomorRfq().replace("RFQ", "PO"));
				} 

				AdmProsesHirarki ahp = SessionGetter.getHirarkiProsesByDistrict(ch.getAdmProses(), 1,
						ch.getAdmDistrict(), ch.getContractAmount().doubleValue());
				if (ahp != null) {
					ch.setAdmRole(ahp.getAdmRoleByCurrentPos());
					// ch.setAdmUserByCurrentUser(prcMainHeader.getPengawasPp());
					ch.setAdmUserByCurrentUser(prcMainHeader.getAdmUserByPpmBuyer());// langsung
//																						// ke
//																						// buyer
					//ch.setAdmUserByCurrentUser(prcMainHeader.getPengawasPp());//berubah menjadi pengawas pp
				}

				saveEntity(ch);

				List<PrcMainItem> listMainItem = listPrcMainItemByHeaderByPemenang(prcMainHeader, vh);
				Double totalPen = 0.0;
				for (PrcMainItem ipt : listMainItem) {
					System.out.println(">>> " + ipt.getId());
					System.out.println(">>> " + vh.getVendorId());
					System.out.println(">>>> " + quote.getId().getPpmId());
					PrcMainVendorQuoteItem it = getPrcMainVendorQuoteByItemHeader(ipt, vh, quote);
					System.out.println(">>> " + it);
					CtrContractItem citem = new CtrContractItem();
					citem.setAdmUom(it.getPrcMainItem().getAdmUom());
					citem.setAttachment(it.getPrcMainItem().getAttachment());
					citem.setContractHeader(ch);
					citem.setGroupCode(it.getPrcMainItem().getGroupCode());
					citem.setItemCode(it.getPrcMainItem().getItemCode());
					citem.setItemCostCenter(it.getPrcMainItem().getItemCostCenter());
					citem.setItemDescription(it.getItemDescription());
					citem.setItemPriceOe((it.getItemPriceNego() != null && it.getItemPriceNego().doubleValue() > 0)
							? it.getItemPriceNego() : it.getItemPrice());
					citem.setItemQuantityOe(it.getItemQuantity());
					citem.setItemTaxOe(it.getItemPajak());
					citem.setItemTipe(it.getPrcMainItem().getItemTipe());
					citem.setItemTotalPriceOe(
							(it.getItemPriceNegoTotalPpn() != null && it.getItemPriceNegoTotalPpn().doubleValue() > 0)
									? it.getItemPriceNegoTotalPpn() : it.getItemPriceTotalPpn());
					citem.setStorageLocation(ipt.getStorageLocation());
					if(prcMainHeader.getPpmJenis().getParentId().getId()==2) {
						citem.setServicePriceOe(citem.getItemPriceOe());
					}else {
						citem.setServicePriceOe(new BigDecimal(0));
					}
					citem.setLineId(ipt.getLineId());
					saveEntity(citem);
					totalPen = totalPen + citem.getItemTotalPriceOe().doubleValue();
				}

				ch.setContractAmount(new BigDecimal(totalPen));
				saveEntity(ch);

				// cek delegasi
				AdmDelegasi del = new AdmServiceImpl().getDelegasiByUserDari(ch.getAdmUserByCurrentUser());
				if (del != null) {
					ch.setAdmUserByCurrentUser(del.getKepada());
					saveEntity(ch);
				}

				CtrContractComment comment = new CtrContractComment();
				comment.setAksi("");
				comment.setContractId(new BigDecimal(ch.getContractId()));
				comment.setUsername(new AdmServiceImpl().getUserById(ch.getAdmUserByCurrentUser()).getCompleteName());
				comment.setPosisi(ch.getAdmUserByCurrentUser().getAdmEmployee().getEmpStructuralPos());
				if (SessionGetter.isNotNull(docsKomentar)) {
					comment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
				}
				comment.setProses(ch.getAdmProses().getId());
				//comment.setProsesAksi("Pemilihan Pembuat Kontrak");
				comment.setProsesAksi(SessionGetter.getPropertiesValue("ctr.2", null));
				saveEntity(comment);
			}

			Object o2[] = new Object[3];
			o2[0] = "Perusahaan Bapak/Ibu ";
			o2[1] = prcMainHeader.getPpmNomorRfq();
			o2[2] = prcMainHeader.getPpmSubject();
			String msg = SessionGetter.getPropertiesValue("email.eproc.pemenang", o2);
			EmailSessionBean.sendMail("eProc PT. Semen Baturaja (Persero)", msg, rcpt);

			// upload doc
			if (SessionGetter.isNotNull(docsKomentar)) {
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}

			// upload history
			SessionGetter.saveHistoryGlobalFinish(histMainComment, prcMainHeader, session);

			prcMainHeader.setAdmUserByPpmCurrentUser(null);
			prcMainHeader.setAdmProses(null);
			saveEntity(prcMainHeader);

			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			e.printStackTrace();
			throw e;
		}
	}

	private PrcMainVendorQuoteItem getPrcMainVendorQuoteByItemHeader(PrcMainItem ipt, VndHeader vh,
			PrcMainVendorQuote quote) {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMainVendorQuoteItem.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("prcMainItem", FetchMode.JOIN);
		PrcMainVendorQuoteItemId id = new PrcMainVendorQuoteItemId(new BigDecimal(ipt.getId()),
				quote.getId().getPpmId(), quote.getId().getVendorId());
		// crit.add(Restrictions.eq("prcMainItem", ipt));
		// crit.add(Restrictions.eq("prcMainVendorQuote", quote));
		// crit.add(Restrictions.eq("vndHeader", vh));
		crit.add(Restrictions.idEq(id));
		return (PrcMainVendorQuoteItem) getEntityByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	private List<PrcMainItem> listPrcMainItemByHeaderByPemenang(PrcMainHeader prcMainHeader, VndHeader vh) {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMainItem.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("prcMainHeader", prcMainHeader));
		crit.add(Restrictions.eq("vndHeader", vh));
		crit.addOrder(Order.asc("id"));
		return (List<PrcMainItem>) getListByDetachedCiteria(crit);
	}

	/**
	 * get distinct pemenang
	 * 
	 * @param ppmId
	 * @param session
	 * @return
	 */
	private List<Long> getListPemenang(Long ppmId, Session session) {
		Query q = session
				.createSQLQuery("select distinct a.pemenang from prc_main_item a where a.ppm_id=" + ppmId + " ");
		@SuppressWarnings("unchecked")
		List<Object> lst = q.list();
		Iterator<Object> it = lst.iterator();
		List<Long> retMe = new ArrayList<Long>();
		while (it.hasNext()) {
			Object o = it.next();
			it.remove();
			retMe.add(Long.valueOf(String.valueOf(o)));
		}
		return retMe;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CtrContractHeader> listContractPemilihanPembuatKontrak(Object object) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(CtrContractHeader.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("prcMainHeader", FetchMode.JOIN);
		crit.setFetchMode("admUserByCurrentUser", FetchMode.JOIN);
		crit.add(Restrictions.eq("admProses", SessionGetter.KONTRAK_PROSES_ID));
		crit.add(Restrictions.eq("admUserByCurrentUser", SessionGetter.getUser()));
		crit.add(Restrictions.isNull("admUserByContractCreator"));
		return (List<CtrContractHeader>) getListByDetachedCiteria(crit);
	}

	@Override
	public CtrContractHeader getContractHeaderById(CtrContractHeader contractHeader) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(CtrContractHeader.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("vndHeader", FetchMode.JOIN);
		crit.setFetchMode("prcMainHeader", FetchMode.JOIN);
		crit.setFetchMode("ctrContractHeader", FetchMode.JOIN);
		crit.setFetchMode("admUserByContractCreator", FetchMode.JOIN);
		crit.add(Restrictions.idEq(contractHeader.getContractId()));
		return (CtrContractHeader) getEntityByDetachedCiteria(crit);
	}
	
	@Override
	public CtrContractHeader getContractHeaderForSAP(CtrContractHeader contractHeader) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(CtrContractHeader.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("vndHeader", FetchMode.JOIN);
		crit.setFetchMode("prcMainHeader", FetchMode.JOIN);
		crit.setFetchMode("ctrContractHeader", FetchMode.JOIN);
		crit.setFetchMode("admUserByContractCreator", FetchMode.JOIN);
		crit.setFetchMode("admCurrency", FetchMode.JOIN);
		

		crit.add(Restrictions.idEq(contractHeader.getContractId()));
		return (CtrContractHeader) getEntityByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CtrContractItem> listContractItemByHeader(CtrContractHeader contractHeader) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(CtrContractItem.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("admUom", FetchMode.JOIN);
		crit.add(Restrictions.eq("contractHeader", contractHeader));
		crit.addOrder(Order.asc("id"));
		return (List<CtrContractItem>) getListByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CtrContractComment> listCommentByContractHeader(CtrContractHeader contractHeader) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(CtrContractComment.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("contractId", new BigDecimal(contractHeader.getContractId())));
		crit.addOrder(Order.asc("createdDate"));
		return (List<CtrContractComment>) getListByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CtrContractPb> listContractPbByHeader(CtrContractHeader contractHeader) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(CtrContractPb.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("ctrContractHeader", contractHeader));
		return (List<CtrContractPb>) getListByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CtrContractTop> listContractTopByHeader(CtrContractHeader contractHeader) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(CtrContractTop.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("ctrContractHeader", contractHeader));
		return (List<CtrContractTop>) getListByDetachedCiteria(crit);
	}

	@Override
	public void savePemilihanPembuatKontrak(CtrContractHeader contractHeader, CtrContractComment ctrContractComment,
			File docsKomentar, String docsKomentarFileName) throws Exception {
		try {
			startTransaction();
			CtrContractHeader chf = getContractHeaderById(contractHeader);
			chf.setAdmUserByContractCreator(contractHeader.getAdmUserByContractCreator());
			chf.setAdmUserByCurrentUser(new AdmServiceImpl().getUserById(contractHeader.getAdmUserByContractCreator()));
			chf.setContractProsesLevel(2);
			AdmProsesHirarki ahp = SessionGetter.getHirarkiProsesJenis(chf.getPrcMainHeader());
			if (ahp != null) {
				chf.setAdmRole(ahp.getAdmRoleByCurrentPos());
			}
			saveEntity(chf);

			// upload doc
			if (SessionGetter.isNotNull(docsKomentar)) {
				ctrContractComment.setContractId(new BigDecimal(chf.getContractId()));
				ctrContractComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}
			// save history
			SessionGetter.saveHistoryGlobalContract(ctrContractComment, chf, session);

			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public CtrContractTop getTopItemById(int id) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(CtrContractTop.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.idEq(Long.valueOf(String.valueOf(id))));
		return (CtrContractTop) getEntityByDetachedCiteria(crit);
	}

	@Override
	public void deleteTopItem(Object topItemById) throws Exception {
		try {
			startTransaction();
			deleteEntity(topItemById);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public CtrContractPb getPbItemById(int id) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(CtrContractPb.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.idEq(Long.valueOf(String.valueOf(id))));
		return (CtrContractPb) getEntityByDetachedCiteria(crit);
	}

	@Override
	public void deletePbItem(CtrContractPb pbItemById) throws Exception {
		try {
			startTransaction();
			deleteEntity(pbItemById);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CtrContractHeader> listContractPembuatanDraftKontrak(Object object) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(CtrContractHeader.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("prcMainHeader", FetchMode.JOIN);
		crit.setFetchMode("admUserByCurrentUser", FetchMode.JOIN);
		crit.add(Restrictions.eq("admProses", SessionGetter.KONTRAK_PROSES_ID));
		crit.add(Restrictions.eq("admUserByCurrentUser", SessionGetter.getUser()));
		List<AdmDelegasi> del = new AdmServiceImpl()
				.getListDelegasiByUserKepadaAllAktifNotAktif(SessionGetter.getUser());
		if (del != null && del.size() > 0) {
			Disjunction dj = Restrictions.disjunction();
			dj.add(Restrictions.eq("admUserByContractCreator", SessionGetter.getUser()));
			dj.add(Restrictions.eq("admUserByContractCreator", del.get(0).getDari()));
			crit.add(dj);
		} else
			crit.add(Restrictions.eq("admUserByContractCreator", SessionGetter.getUser()));
		crit.add(Restrictions.eq("contractProsesLevel", 2));
		return (List<CtrContractHeader>) getListByDetachedCiteria(crit);
	}

	@Override
	public void saveDraftKontrak(CtrContractHeader contractHeader, CtrContractComment ctrContractComment,
			File docsKomentar, String docsKomentarFileName, List<CtrContractTop> listTop, List<CtrContractPb> listPb)
			throws Exception {
		try {
			startTransaction();
			CtrContractHeader chf = getContractHeaderById(contractHeader);
			AdmProsesHirarki ahp = SessionGetter.getHirarkiProsesJenisKontrak(chf);
			if (ahp != null) {
				if (ahp.getAdmRoleByCurrentPos() != null) {
					chf.setAdmRole(ahp.getAdmRoleByCurrentPos());
					chf.setAdmUserByCurrentUser(
							SessionGetter.getEmployeeByRole(ahp.getAdmRoleByCurrentPos()).getAdmUser());
					/*if (ahp.getAdmRoleByCurrentPos().getRoleName().trim().equalsIgnoreCase("pengawas pp")) {
						chf.setAdmRole(ahp.getAdmRoleByCurrentPos());
						chf.setAdmUserByCurrentUser(
								new AdmServiceImpl().getUserById(chf.getPrcMainHeader().getPengawasPp()));
					} else {
						chf.setAdmRole(ahp.getAdmRoleByCurrentPos());
						chf.setAdmUserByCurrentUser(
								SessionGetter.getEmployeeByRole(ahp.getAdmRoleByCurrentPos()).getAdmUser());
					}*/
					
				}
				chf.setContractProsesLevel(ahp.getTingkat());
			} else {
				throw new Exception("Kesalahan dalam hirarki .. cek hirarki");
			}
			chf.setContractSubject(contractHeader.getContractSubject());
			chf.setContractDescription(contractHeader.getContractDescription());
			chf.setContractType(contractHeader.getContractType());
			chf.setContractStart(contractHeader.getContractStart());
			chf.setContractEnd(contractHeader.getContractEnd());
			chf.setTglPenyerahan(contractHeader.getTglPenyerahan());
			chf.setPaymentTerm(contractHeader.getPaymentTerm());
			chf.setPurchasingOrganization(contractHeader.getPurchasingOrganization());
			chf.setPurchasingGroup(contractHeader.getPurchasingGroup());
			saveEntity(chf);

			// insert top
			if (listTop != null && listTop.size() > 0) {
				for (CtrContractTop ctop : listTop) {
					ctop.setCtrContractHeader(chf);
					saveEntity(ctop);
				}
			}

			// insert pb
			if (listPb != null && listPb.size() > 0) {
				for (CtrContractPb ctop : listPb) {
					ctop.setCtrContractHeader(chf);
					saveEntity(ctop);
				}
			}
			// upload doc
			if (SessionGetter.isNotNull(docsKomentar)) {
				ctrContractComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}

			// insert History
			SessionGetter.saveHistoryGlobalContract(ctrContractComment, chf, session);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			e.printStackTrace();
			throw e;
		}
	}

	@Override
	public void saveDraftKontrakLegal(CtrContractHeader contractHeader, CtrContractComment ctrContractComment,
			File docsKomentar, String docsKomentarFileName, List<CtrContractTop> listTop, List<CtrContractPb> listPb)
			throws Exception {
		try {
			startTransaction();
			CtrContractHeader chf = getContractHeaderById(contractHeader);
			AdmUser userLegal = new AdmServiceImpl().getUserById(contractHeader.getUserLegal());
			chf.setAdmRole(userLegal.getAdmEmployee().getAdmRoleByEmpRole1());
			chf.setAdmUserByCurrentUser(userLegal);
			chf.setLegal(1);// dalam proses legal
			chf.setUserLegal(userLegal);
			chf.setContractSubject(contractHeader.getContractSubject());
			chf.setContractDescription(contractHeader.getContractDescription());
			chf.setContractType(contractHeader.getContractType());
			chf.setContractStart(contractHeader.getContractStart());
			chf.setContractEnd(contractHeader.getContractEnd());
			chf.setContractProsesLevel(chf.getContractProsesLevel() + 1);
			chf.setTglPenyerahan(contractHeader.getTglPenyerahan());
			chf.setPaymentTerm(contractHeader.getPaymentTerm());
			chf.setPurchasingOrganization(contractHeader.getPurchasingOrganization());
			chf.setPurchasingGroup(contractHeader.getPurchasingGroup());
			saveEntity(chf);

			// insert top
			if (listTop != null && listTop.size() > 0) {
				for (CtrContractTop ctop : listTop) {
					ctop.setCtrContractHeader(chf);
					saveEntity(ctop);
				}
			}

			// insert pb
			if (listPb != null && listPb.size() > 0) {
				for (CtrContractPb ctop : listPb) {
					ctop.setCtrContractHeader(chf);
					saveEntity(ctop);
				}
			}
			// upload doc
			if (SessionGetter.isNotNull(docsKomentar)) {
				ctrContractComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}

			// insert History
			SessionGetter.saveHistoryGlobalContract(ctrContractComment, chf, session);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			e.printStackTrace();
		}

	}

	@Override
	public void saveApprovalKontrak(CtrContractHeader contractHeader, CtrContractComment ctrContractComment,
			File docsKomentar, String docsKomentarFileName) throws Exception {
		try {
			startTransaction();
			CtrContractHeader chf = getContractHeaderById(contractHeader);
			//AdmProsesHirarki ahp = SessionGetter.getHirarkiProsesJenisKontrak(chf);
			AdmProsesHirarki ahp = SessionGetter.getHirarkiProsesByDistrict(chf.getAdmProses(),
					chf.getContractProsesLevel() + 1, chf.getAdmDistrict(), chf.getContractAmount().doubleValue());
			if (ahp != null) {
				if (ahp.getAdmRoleByCurrentPos() != null) {
					
						chf.setAdmRole(ahp.getAdmRoleByCurrentPos());
						/*chf.setAdmUserByCurrentUser(
								SessionGetter.getEmployeeByRole(ahp.getAdmRoleByCurrentPos()).getAdmUser());*/
						chf.setAdmUserByCurrentUser(chf.getAdmUserByContractCreator());
					
				}
				chf.setContractProsesLevel(ahp.getTingkat());
				saveEntity(chf);

				// upload doc
				if (SessionGetter.isNotNull(docsKomentar)) {
					ctrContractComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
				}

				// insert History
				SessionGetter.saveHistoryGlobalContract(ctrContractComment, chf, session);
			} else {
				ahp = SessionGetter.getHirarkiProsesByDistrict(chf.getAdmProses(),
						chf.getContractProsesLevel() + 1, chf.getAdmDistrict(), chf.getContractAmount().doubleValue());
				if(ahp==null) {
					// langsung selesai
					chf.setAdmUserByCurrentUser(null);
					chf.setAdmRole(null);
					chf.setContractProsesLevel(SessionGetter.SELESAI);
					saveEntity(chf);

					// upload doc
					if (SessionGetter.isNotNull(docsKomentar)) {
						ctrContractComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
					}

					// insert History
					ctrContractComment.setProsesAksi("Finalisasi Draft Kontrak");
					ctrContractComment.setAksi("Finalisasi Draft Kontrak");
					SessionGetter.saveHistoryGlobalKontrakFinish(ctrContractComment, chf, session);
				}else {
					if (ahp.getAdmRoleByCurrentPos().getRoleName().trim().equalsIgnoreCase("pengawas pp")) {
						chf.setAdmRole(ahp.getAdmRoleByCurrentPos());
						chf.setAdmUserByCurrentUser(chf.getPrcMainHeader().getPengawasPp());
					}
					chf.setContractProsesLevel(ahp.getTingkat());
					saveEntity(chf);
					if (SessionGetter.isNotNull(docsKomentar)) {
						ctrContractComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
					}
					
					SessionGetter.saveHistoryGlobalKontrakFinish(ctrContractComment, chf, session);
				}
				
			}

			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CtrContractHeader> listContractApprovalDraftKontrak(Object object) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(CtrContractHeader.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("prcMainHeader", FetchMode.JOIN);
		crit.setFetchMode("admUserByCurrentUser", FetchMode.JOIN);
		crit.add(Restrictions.eq("admProses", SessionGetter.KONTRAK_PROSES_ID));
		crit.add(Restrictions.eq("admUserByCurrentUser", SessionGetter.getUser()));
		crit.add(Restrictions.gt("contractProsesLevel", 2));
		return (List<CtrContractHeader>) getListByDetachedCiteria(crit);
	}

	@Override
	public void saveRevisiKontrak(CtrContractHeader contractHeader, CtrContractComment ctrContractComment,
			File docsKomentar, String docsKomentarFileName) throws Exception {
		try {
			startTransaction();
			CtrContractHeader chf = getContractHeaderById(contractHeader);
			AdmUser contractCreator = new AdmServiceImpl().getUserById(chf.getAdmUserByContractCreator());
			chf.setAdmRole(contractCreator.getAdmEmployee().getAdmRoleByEmpRole1());
			chf.setAdmUserByCurrentUser(contractCreator);
			chf.setContractProsesLevel(2);
			chf.setLegal(null);
			saveEntity(chf);

			// upload doc
			if (SessionGetter.isNotNull(docsKomentar)) {
				ctrContractComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}

			// insert History
			SessionGetter.saveHistoryGlobalContract(ctrContractComment, chf, session);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			e.printStackTrace();
		}
	}

	@Override
	public void rebidPengadaan(PrcMainHeader mainHeader, HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName, List<PrcMainVendor> listVendor) throws Exception {
		try {
			startTransaction();
			PrcMainHeader prcMainHeader = getPrcMainHeaderSPPHByPpmId(mainHeader.getPpmId());

			// create new
			Integer no = 1;
			String noRfq = prcMainHeader.getPpmNomorRfq();
			/*
			 * if(prcMainHeader.getKeterangan()!=null &&
			 * prcMainHeader.getKeterangan().length()>0){ no =
			 * Integer.valueOf(prcMainHeader.getKeterangan()); no = no+1; }else{
			 * noRfq = noRfq+"-"+no; }
			 */

			if (prcMainHeader.getKeterangan() != null && prcMainHeader.getKeterangan().trim().length() > 0) {
				no = Integer.valueOf(prcMainHeader.getKeterangan().trim());
				no = no + 1;
				noRfq = StringUtils.substring(noRfq, 0, StringUtils.lastIndexOf(noRfq, "-")).concat("-")
						.concat(String.valueOf(no));

			} else {
				noRfq = noRfq.concat("-").concat(String.valueOf(no));

			}

			PrcMainHeader mainHeaderNew = new PrcMainHeader();
			mainHeaderNew.setAdmDept(prcMainHeader.getAdmDept());
			mainHeaderNew.setAdmDistrict(prcMainHeader.getAdmDistrict());
			mainHeaderNew.setAdmProses(SessionGetter.PP_PROSES_ID);
			mainHeaderNew.setAdmUserByPpmAdmBuyer(prcMainHeader.getAdmUserByPpmAdmBuyer());
			mainHeaderNew.setAdmUserByPpmBuyer(prcMainHeader.getAdmUserByPpmBuyer());
			mainHeaderNew.setAdmUserByPpmCreator(prcMainHeader.getAdmUserByPpmCreator());
			mainHeaderNew.setAdmUserByPpmCurrentUser(prcMainHeader.getAdmUserByPpmBuyer());
			mainHeaderNew.setAdmUserByPpmEstimator(prcMainHeader.getAdmUserByPpmEstimator());
			mainHeaderNew.setAttachment(prcMainHeader.getAttachment());
			mainHeaderNew.setGroupCode(prcMainHeader.getGroupCode());
			mainHeaderNew.setKeterangan(String.valueOf(no));
			mainHeaderNew.setLokasiPengadaan(prcMainHeader.getLokasiPengadaan());
			mainHeaderNew.setOeOpen(prcMainHeader.getOeOpen());
			mainHeaderNew.setPembelian(prcMainHeader.getPembelian());
			mainHeaderNew.setPengawasPp(prcMainHeader.getPengawasPp());
			mainHeaderNew.setPpmDescription(prcMainHeader.getPpmDescription());
			mainHeaderNew.setPpmJenis(prcMainHeader.getPpmJenis());
			mainHeaderNew.setPpmJumlahEe(prcMainHeader.getPpmJumlahEe());
			mainHeaderNew.setPpmJumlahOe(prcMainHeader.getPpmJumlahOe());
			mainHeaderNew.setPpmNomorPr(prcMainHeader.getPpmNomorPr());
			mainHeaderNew.setPpmNomorRfq(noRfq);
			mainHeaderNew.setPpmPrioritas(prcMainHeader.getPpmPrioritas());
			mainHeaderNew.setPpmProsesLevel(4);
			mainHeaderNew.setPpmSubject(prcMainHeader.getPpmSubject());
			mainHeaderNew.setPrcMethod(prcMainHeader.getPrcMethod());
			mainHeaderNew.setTglDibutuhkan(prcMainHeader.getTglDibutuhkan());
			mainHeaderNew.setAdmRole(prcMainHeader.getAdmRole());
			mainHeaderNew.setSatkerPeminta(prcMainHeader.getSatkerPeminta());
			mainHeaderNew.setDistrictKeuangan(prcMainHeader.getDistrictKeuangan());
		    mainHeaderNew.setDistrictUser(prcMainHeader.getDistrictUser());
		   //mainHeaderNew.setPembuatJde(prcMainHeader.getPembuatJde());
		    mainHeaderNew.setLokasiPenyerahan(prcMainHeader.getLokasiPenyerahan());
		    mainHeaderNew.setPpmJenisDetail(prcMainHeader.getPpmJenisDetail());
			mainHeaderNew.setPrcMainHeader(prcMainHeader);
			mainHeaderNew.setPpmNomorPrSap(prcMainHeader.getPpmNomorPrSap());
			mainHeaderNew.setPembuatPr(prcMainHeader.getPembuatPr());
			mainHeaderNew.setNamaPembuatPr(prcMainHeader.getPembuatPr());
			mainHeaderNew.setMataUang(prcMainHeader.getMataUang());
			saveEntity(mainHeaderNew);
			// Clone items

			List<PrcMainItem> listItem = new PrcServiceImpl().listPrcMainItemByHeader(prcMainHeader);
			if (listItem != null && listItem.size() > 0) {
				for (PrcMainItem it : listItem) {
					PrcMainItem itn = new PrcMainItem();
					itn.setAdmUom(it.getAdmUom());
					itn.setAttachment(it.getAttachment());
					itn.setGroupCode(it.getGroupCode());
					itn.setItemCode(it.getItemCode());
					itn.setItemCostCenter(it.getItemCostCenter());
					itn.setItemDescription(it.getItemDescription());
					itn.setItemPrice(it.getItemPrice());
					itn.setItemPriceOe(it.getItemPriceOe());
					itn.setItemQuantity(it.getItemQuantity());
					itn.setItemQuantityOe(it.getItemQuantityOe());
					itn.setItemTax(it.getItemTax());
					itn.setItemTaxOe(it.getItemTaxOe());
					itn.setItemTipe(it.getItemTipe());
					itn.setItemTotalPrice(it.getItemTotalPrice());
					itn.setItemTotalPriceOe(it.getItemTotalPriceOe());
					itn.setPrcMainHeader(mainHeaderNew);
					itn.setLineId(it.getLineId());
					itn.setStorageLocation(it.getStorageLocation());
					itn.setKeterangan(it.getKeterangan());
					itn.setDeliveryDate(it.getDeliveryDate());
					itn.setItemCategory(it.getItemCategory());
					itn.setMaterialGroup(it.getMaterialGroup());
					itn.setPurchasingGroup(it.getPurchasingGroup());
					saveEntity(itn);
				}
			}

			// clone anggaran
			PrcAnggaranJdeHeader ahead = new PrcServiceImpl().getJdeAnggaranHeaderByPpmId(prcMainHeader);
			if (ahead != null) {
				PrcAnggaranJdeHeader anggaran = new PrcAnggaranJdeHeader(ahead);
				anggaran.setPrcMainHeader(mainHeaderNew);
				saveEntity(anggaran);

				// clone list detail
				List<PrcAnggaranJdeDetail> lst = new PrcServiceImpl().listAnggaranDetailByPPmId(prcMainHeader);
				if (lst != null) {
					for (PrcAnggaranJdeDetail d : lst) {
						PrcAnggaranJdeDetail dd = new PrcAnggaranJdeDetail(d);
						dd.setPrcAnggaranJdeHeader(anggaran);
						dd.setPrcMainHeader(mainHeaderNew);
						saveEntity(dd);
					}
				}

			}

			// cek panitia
			if (prcMainHeader.getPanitia() != null) {
				PanitiaPengadaan tmppan = new PrcServiceImpl().getPanitiaPengadaan(prcMainHeader.getPanitia());
				// create new PAN
				PanitiaPengadaan pan = new PanitiaPengadaan();
				pan.setKeterangan(tmppan.getKeterangan());
				pan.setNamaPanitia(tmppan.getNamaPanitia());
				pan.setTanggalBerakhir(tmppan.getTanggalBerakhir());
				pan.setTanggalMulai(tmppan.getTanggalMulai());
				saveOnlyEntity(pan);

				List<PanitiaPengadaanDetail> lpp = new PrcServiceImpl()
						.getListPanitiaPengadaanDetail(prcMainHeader.getPanitia());
				if (lpp != null && lpp.size() > 0) {
					List<Long> insertedUser = new ArrayList<Long>();
					for (PanitiaPengadaanDetail pp : lpp) {
						PanitiaPengadaanDetail p = new PanitiaPengadaanDetail();
						p.setPanitia(pan);
						p.setAnggota(pp.getAnggota());
						p.setPosisi(pp.getPosisi());
						p.setStatus(null);
						if (!insertedUser.contains(pp.getAnggota().getId())) {
							insertedUser.add(pp.getAnggota().getId());
							saveOnlyEntity(p);
						}
					}
				}

				mainHeaderNew.setPanitia(pan);
				saveEntity(mainHeaderNew);
			}

			// cek delegasi
			boolean delegasi = false;
			AdmDelegasi del = new AdmServiceImpl().getDelegasiByUserDari(mainHeaderNew.getAdmUserByPpmCurrentUser());
			if (del != null) {
				mainHeaderNew.setAdmUserByPpmCurrentUser(del.getKepada());
				session.saveOrUpdate(mainHeaderNew);
				delegasi = true;
			}

			HistMainComment comment = new HistMainComment();
			comment.setAksi("Pemilihan Mitra Kerja");
			if (!delegasi) {
				comment.setUsername(SessionGetter.getUser().getCompleteName());
				comment.setPosisi(SessionGetter.getUser().getAdmEmployee().getEmpStructuralPos());
			} else {
				comment.setUsername(del.getKepada().getCompleteName());
				comment.setPosisi(del.getKepada().getAdmEmployee().getEmpStructuralPos());
			}
			comment.setPrcMainHeader(mainHeaderNew);
			comment.setProses(SessionGetter.PP_PROSES_ID.getId());
			comment.setProsesAksi("Pemilihan Mitra Kerja");
			saveEntity(comment);

			prcMainHeader.setAdmRole(null);
			prcMainHeader.setAdmUserByPpmCurrentUser(null);
			prcMainHeader.setPpmProsesLevel(SessionGetter.REBID);
			saveEntity(prcMainHeader);

			// set history
			if (SessionGetter.isNotNull(docsKomentar)) {
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}
			histMainComment.setAksi("Rebid");
			SessionGetter.saveHistoryGlobalFinish(histMainComment, prcMainHeader, session);

			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}

	}

	@Override
	public void revisiPengadaan(PrcMainHeader prcMainHeader, HistMainComment histMainComment, File docsKomentar,
			String docsKomentarFileName, List<PrcMainVendor> listVendor) throws Exception {
		try {
			startTransaction();
			prcMainHeader = getPrcMainHeaderLite(prcMainHeader.getPpmId());
			prcMainHeader.setPpmProsesLevel(prcMainHeader.getPpmProsesLevel() - 1);
			saveEntity(prcMainHeader);

			// set history
			if (SessionGetter.isNotNull(docsKomentar)) {
				histMainComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}
			SessionGetter.saveHistoryGlobalPrOE(histMainComment, prcMainHeader, session);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public void saveSetujuKontrakLegal(CtrContractHeader contractHeader, CtrContractComment ctrContractComment,
			File docsKomentar, String docsKomentarFileName) throws Exception {
		try {
			startTransaction();
			CtrContractHeader chf = getContractHeaderById(contractHeader);
			AdmUser contractCreator = new AdmServiceImpl().getUserById(chf.getAdmUserByContractCreator());
			chf.setAdmRole(contractCreator.getAdmEmployee().getAdmRoleByEmpRole1());
			chf.setAdmUserByCurrentUser(contractCreator);
			chf.setContractProsesLevel(2);
			chf.setLegal(2);// setuju legal
			saveEntity(chf);

			// upload doc
			if (SessionGetter.isNotNull(docsKomentar)) {
				ctrContractComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}

			// insert History
			SessionGetter.saveHistoryGlobalContract(ctrContractComment, chf, session);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			e.printStackTrace();
		}
	}

	@Override
	public void createAdendum(CtrContractHeader contractHeaderAdendum, CtrContractHeader contractHeader,
			List<CtrContractItem> listContractItem, CtrContractComment ctrContractComment) throws Exception {
		startTransaction();
		try {
			contractHeader = getContractHeaderById(contractHeader);
			// create header
			contractHeaderAdendum.setAdmDistrict(contractHeader.getAdmDistrict());
			contractHeaderAdendum.setAdmProses(SessionGetter.KONTRAK_PROSES_ID);
			contractHeaderAdendum.setContractProsesLevel(1);
			contractHeaderAdendum.setPrcMainHeader(contractHeader.getPrcMainHeader());
			contractHeaderAdendum.setVndHeader(contractHeader.getVndHeader());
			contractHeaderAdendum.setContractDescription(contractHeaderAdendum.getContractDescription()
					.concat("<br/>ADENDUM :").concat(contractHeaderAdendum.getContractAmmendReason()));

			AdmProsesHirarki ahp = SessionGetter.getHirarkiProsesByDistrict(contractHeaderAdendum.getAdmProses(), 1,
					contractHeaderAdendum.getAdmDistrict(), contractHeaderAdendum.getContractAmount().doubleValue());
			if (ahp != null) {
				contractHeaderAdendum.setAdmRole(ahp.getAdmRoleByCurrentPos());
				AdmUser user = new AdmServiceImpl().getAdmUser(contractHeader.getPrcMainHeader().getPengawasPp());
				contractHeaderAdendum.setAdmUserByCurrentUser(user);
			}

			saveEntity(contractHeaderAdendum);

			// update status header
			contractHeader.setContractProsesLevel(SessionGetter.REBID);// ini
																		// jadi
																		// adendum
			saveEntity(contractHeader);
			// insert item
			if (listContractItem != null) {
				for (CtrContractItem it : listContractItem) {
					it.setContractHeader(contractHeaderAdendum);
					saveEntity(it);
				}
			}

			CtrContractComment comment = new CtrContractComment();
			comment.setAksi("");
			comment.setContractId(new BigDecimal(contractHeaderAdendum.getContractId()));
			comment.setUsername(new AdmServiceImpl().getUserById(contractHeaderAdendum.getAdmUserByCurrentUser())
					.getCompleteName());
			comment.setPosisi(contractHeaderAdendum.getAdmUserByCurrentUser().getAdmEmployee().getEmpStructuralPos());
			comment.setProses(contractHeaderAdendum.getAdmProses().getId());
			comment.setProsesAksi("Pemilihan Pembuat Kontrak");
			saveEntity(comment);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}

	}

	@Override
	public List<MainMasterJdeCommand> listJdeMasterMain(Object object) throws Exception {
		List<MainMasterJdeCommand> retMe = new ArrayList<MainMasterJdeCommand>();
		Session ses = HibernateUtil.getSessionJde();
		String query = "";
		String tipe = String.valueOf(object);
		if (tipe.equalsIgnoreCase("BS")) {
			query = "select mcmcu, mcdl01, mcstyl  from " + SessionGetter.valueProperty("jde.schema")
					+ ".f0006 where mcstyl='BS' and mcldm=1";
		} else if (tipe.equalsIgnoreCase("CURR")) {
			query = "select cvcrcd, cvdl01, 'currency'  from " + SessionGetter.valueProperty("jde.schema") + ".f0013";
		} else if (tipe.equalsIgnoreCase("SHIPTO")) {
			query = "select aban8, abalph, abat1  from " + SessionGetter.valueProperty("jde.schema")
					+ ".f0101 where abat1='O'";
		} else if (tipe.equalsIgnoreCase("BUYER")) {
			query = "select aban8, abalph, abat1  from " + SessionGetter.valueProperty("jde.schema")
					+ ".f0101 where abat1='UK'";
		} else if (tipe.equalsIgnoreCase("CARRIER")) {
			query = "select aban8, abalph, abat1  from " + SessionGetter.valueProperty("jde.schema")
					+ ".f0101 where abat1='UK'";
		} else if (tipe.equalsIgnoreCase("PAYTERM")) {
			query = "select pnptc, pnptd, 'PTERMS'  from " + SessionGetter.valueProperty("jde.schema") + ".f0014";
		} else if (tipe.equalsIgnoreCase("HC")) {
			query = "select drky, drdl01, drrt from " + SessionGetter.valueProperty("jde.schema.ctl")
					+ ".f0005 where drrt='HC' and DRSY='42'";
		} else if (tipe.equalsIgnoreCase("PM")) {
			query = "select drky, drdl01, drrt from " + SessionGetter.valueProperty("jde.schema.ctl")
					+ ".f0005 where drrt='PM' and DRSY='40'";
		} else if (tipe.equalsIgnoreCase("AIA")) {
			query = "select drky, drdl01, drrt from " + SessionGetter.valueProperty("jde.schema.ctl")
					+ ".f0005 where drrt='FU' and DRSY='H40'";
		} else if (tipe.equalsIgnoreCase("SUBLEDGER")) {
			query = "select drky, drdl01, drrt from " + SessionGetter.valueProperty("jde.schema.ctl")
					+ ".f0005 where drrt='ST' and DRSY='00'";
		} else if (tipe.equalsIgnoreCase("UOM")) {
			query = "select drky, drdl01, drrt from " + SessionGetter.valueProperty("jde.schema.ctl")
					+ ".f0005 where drrt='UM' and DRSY='00'";
		} else if (tipe.equalsIgnoreCase("LINETYPE")) {
			query = "select drky, drdl01, drrt from " + SessionGetter.valueProperty("jde.schema.ctl")
					+ ".f0005 where drrt='UM' and DRSY='00'";
		} else if (tipe.equalsIgnoreCase("REPORT1")) {
			query = "select drky, drdl01, drrt from " + SessionGetter.valueProperty("jde.schema.ctl")
					+ ".f0005 where drrt='P1' and DRSY='41'";
		} else if (tipe.equalsIgnoreCase("REPORT2")) {
			query = "select drky, drdl01, drrt from " + SessionGetter.valueProperty("jde.schema.ctl")
					+ ".f0005 where drrt='P2' and DRSY='41'";
		} else if (tipe.equalsIgnoreCase("REPORT3")) {
			query = "select drky, drdl01, drrt from " + SessionGetter.valueProperty("jde.schema.ctl")
					+ ".f0005 where drrt='P3' and DRSY='41'";
		} else if (tipe.equalsIgnoreCase("REPORT4")) {
			query = "select drky, drdl01, drrt from " + SessionGetter.valueProperty("jde.schema.ctl")
					+ ".f0005 where drrt='P4' and DRSY='41'";
		} else if (tipe.equalsIgnoreCase("TAXABLE")) {
			query = "select drky, drdl01, drrt from " + SessionGetter.valueProperty("jde.schema.ctl")
					+ ".f0005 where drrt='TV' and DRSY='H00'";
		}
		Query q = ses.createSQLQuery(query);
		@SuppressWarnings("unchecked")
		List<Object[]> lobj = q.list();
		if (lobj != null) {
			Iterator<Object[]> it = lobj.iterator();
			while (it.hasNext()) {
				Object[] o = (Object[]) it.next();
				it.remove();
				MainMasterJdeCommand jde = new MainMasterJdeCommand();
				jde.setKey(String.valueOf(o[0]).trim());
				jde.setValue(String.valueOf(o[1]));
				jde.setTipe(String.valueOf(o[2]).trim());
				retMe.add(jde);
			}
		}
		ses.clear();
		ses.close();
		return retMe;
	}
	
	//THIS METHOD IS NO LONGER USED
	@Override
	public void savePOJde(POHeader poHeader, List<PODetail> listPoDetail, CtrContractHeader contractHeader,
			Integer sessionID, UserSession userSession) throws Exception {
		try {
			startTransaction();
			if (poHeader != null) {
				//JdeTransaction jta = JdeTransaction.getInstance("GeneratePO");
				// proses dulu headernya
				poHeader.setSzProgramID("EPROC");
				poHeader.setSzComputerId("MUSI");
				poHeader.setcEvaluatedReceiptsFlag("N");
				List<PODetail> listFinal = new ArrayList<PODetail>();
				String orderNumber = "0";
				if (listPoDetail != null && listPoDetail.size() > 0) {
					int line = 1000;
					for (PODetail pd : listPoDetail) {
						pd.setMnUnitPrice(pd.getMnUnitPrice());
						pd.setSzOrderCompany(poHeader.getSzOrderCompany());
						pd.setMnOrderNumber(poHeader.getMnOrderNumber());
						pd.setSzOrderType(poHeader.getSzOrderType());
						pd.setSzOrderSuffix(poHeader.getSzOrderSuffix());
						// pd.setMnOrderLineNumber(line);
						pd.setSzProgramID("EPROC");
						pd.setSzComputerID(poHeader.getSzComputerId());
						pd.setJdRequestedDate(poHeader.getJdRequestedDate());
						pd.setJdTransactionDate(new Date());
						pd.setJdPromisedDate(poHeader.getJdPromisedDate());
						pd.setJdGLDate(new Date());
						pd.setJdDatePromisedShipJu(poHeader.getJdPromisedDate());
						pd.setMnSupplierNumber(poHeader.getMnSupplierNumber());
						pd.setMnBuyerNumber(poHeader.getMnBuyerNumber());
						pd.setMnShipToNumber(poHeader.getMnShipToNumber());
						pd.setMnCarrierNumber(poHeader.getMnCarrierNumber());
						pd.setSzTransactionCurrencyCode(poHeader.getSzTransactionCurrencyCode());
						pd.setIdentifierShortItem(getShortItem(pd.getSzUnformattedItemNumber()));
						pd.setSzBranchPlant(poHeader.getSzBranchPlant());
						pd.setMnCurrencyExchangeRate(poHeader.getMnCurrencyExchangeRate().floatValue());

						// buat nampung text spek
						pd.setSzElevation(pd.getSzElevation());

						listFinal.add(pd);
						if (orderNumber.equalsIgnoreCase("0"))
							if (pd.getSzOriginalOrderNumber() != null)
								orderNumber = pd.getSzOriginalOrderNumber();
						line = line + 1000;
					}
				}

				CtrContractHeader cth = getContractHeaderById(contractHeader);
				String orderType = cth.getPrcMainHeader().getPpmNomorPrSap().contains("ZMN") ? "ZMN" : "ZST";

				//String jdeNumber = JdeService.insertPOFromModel(sessionID, userSession, poHeader, listFinal, orderType);

				// update OR nextstatus 999/ last status=130
				poHeader.setMnOrderNumber(Integer.valueOf(orderNumber));

				updateOrStatus(poHeader, orderType);

				// update contract header

				//cth.setJdeNumber(jdeNumber);
				cth.setStatusJde(1);
				saveEntity(cth);
			}
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			e.printStackTrace();
			throw e;
		}

	}

	private void updateOrStatus(POHeader poHeader, String orderType) throws Exception {
		Session ses = HibernateUtil.getSessionJde();
		try {
			ses.beginTransaction();
			List<PODetail> lpd = getListPoDetail(poHeader, orderType);
			if (lpd != null && lpd.size() > 0) {
				System.out.println(">> UPDATE OR STATUS");
				for (PODetail p : lpd) {
					String query = "update " + SessionGetter.valueProperty("jde.schema")
							+ ".f4311 a set a.pdnxtr='999', a.pdlttr='130', a.pdurec=a.pduopn, a.pduopn=0, a.pdarec=a.pdaopn, a.pdaopn=0 "
							+ " where a.pdkcoo='" + poHeader.getSzOrderCompany() + "' and a.pdlnid="
							+ p.getMnOrderLineNumber() + " and a.pddoco=" + p.getSzOriginalOrderNumber()
							+ " and a.pddcto='OR'";
					Query q = ses.createSQLQuery(query);
					q.executeUpdate();
				}
			}
			ses.getTransaction().commit();
		} catch (Exception e) {
			ses.getTransaction().rollback();
			ses.clear();
			ses.close();
			e.printStackTrace();
			throw e;
		}

	}

	private Integer getShortItem(String szUnformattedItemNumber) throws Exception {
		Integer retMe = 0;
		try {
			Session ses = HibernateUtil.getSessionJde();
			Query q = ses.createSQLQuery("select imitm from " + SessionGetter.valueProperty("jde.schema")
					+ ".f4101 where imlitm='" + szUnformattedItemNumber + "'");
			Object obj = q.uniqueResult();
			if (obj != null) {
				retMe = Integer.valueOf(String.valueOf(obj));
			}
			ses.clear();
			ses.close();

		} catch (Exception e) {
			throw e;
		}
		return retMe;
	}

	@Override
	public String getDeskripsiAnggaran(String kode) throws Exception {
		Session ses = HibernateUtil.getSessionJde();
		try {
			String query = "";
			if (kode != null && kode != "" && kode.length() > 0)
				query = "select glexa from " + SessionGetter.valueProperty("jde.schema") + ".f550911a where glaid ="
						+ kode + "";
			else {
				ses.clear();
				ses.close();
				return "";
			}
			Query q = ses.createSQLQuery(query);
			Object obj = q.uniqueResult();
			ses.clear();
			ses.close();
			if (obj != null) {
				return String.valueOf(obj);
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			ses.clear();
			ses.close();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MainMasterJdeCommand> listJdeMasterAnggaran(Integer tahunAnggaran, String kodeAnggaran)
			throws Exception {
		System.out.println(">>> QUERY ANGGARAN");
		Session ses = HibernateUtil.getSessionJde();
		List<MainMasterJdeCommand> retMe = new ArrayList<MainMasterJdeCommand>();
		try {
			String query = "";
			if (tahunAnggaran != null && kodeAnggaran != null) {
				if (tahunAnggaran != null && String.valueOf(tahunAnggaran).length() >= 4) {
					String th = String.valueOf(tahunAnggaran).substring(2, 3);
					query = "select glaid, glexa, glaa, glmcu, globj, glsub from "
							+ SessionGetter.valueProperty("jde.schema") + ".f550911a where glaid like '%" + kodeAnggaran
							+ "%' and glfy=" + th + " ";
				} else {
					query = "select glaid, glexa, glaa, glmcu, globj, glsub from "
							+ SessionGetter.valueProperty("jde.schema") + ".f550911a where glaid like '%" + kodeAnggaran
							+ "%' and glfy=" + tahunAnggaran + " ";
				}
			} else {
				ses.clear();
				ses.close();
				return retMe;
			}
			Query qq = ses.createSQLQuery(query);
			List<Object[]> lst = qq.list();
			if (lst != null && lst.size() > 0) {
				Iterator<Object[]> it = lst.iterator();
				while (it.hasNext()) {
					Object[] o = (Object[]) it.next();
					it.remove();
					MainMasterJdeCommand jde = new MainMasterJdeCommand();
					jde.setKey(String.valueOf(o[0]));
					jde.setValue(String.valueOf(o[1]));
					String saldo = "0";
					if (o[2] != null) {
						Double d = Double.valueOf(String.valueOf(o[2]));
						d = d / 100;
						saldo = String.valueOf(d);
					}
					jde.setSaldoAwal(saldo);
					jde.setTransaksi(getJumlahTransaksiAnggaran(jde.getKey(), ses));
					// rekening
					String costCenter = String.valueOf(o[3] != null ? o[3] : "");
					String rekening = String.valueOf(o[4] != null ? o[4] : "");
					String subRekening = String.valueOf(o[5] != null ? o[5] : "");
					jde.setRekening(costCenter.concat(".").concat(rekening).concat(".").concat(subRekening));
					jde.setValue(jde.getValue().concat("<br>").concat("Rekening : " + jde.getRekening()));
					retMe.add(jde);
				}
			}
			ses.clear();
			ses.close();
		} catch (Exception e) {
			ses.clear();
			ses.close();
			e.printStackTrace();
			throw e;
		}
		return retMe;
	}

	private String getJumlahTransaksiAnggaran(String kodeAnggaran, Session ses) throws Exception {
		String jumlah = "";
		try {
			Double anggaranReserved = getAnggaranReserved(kodeAnggaran);
			/**
			 * kahst = C --> complete, take a.rkaa kahst != C --> incomplete ,
			 * take a.rkacm1
			 */
			String query = "select sum(decode(b.kahst,'C',a.rkaa, a.rkacm1) ) from "
					+ SessionGetter.valueProperty("jde.schema") + ".f559911 a left join "
					+ SessionGetter.valueProperty("jde.schema")
					+ ".f559901 b on a.RKKCOO = b.KAKCOO and a.RKDCTO = b.KADCTO and a.RKDOCO = b.KADOCO where a.rkapfl='A' and trim(a.RKAID)= '"
					+ kodeAnggaran.trim() + "'";
			// Query q = ses.createSQLQuery("select sum(rkacm1) from
			// "+SessionGetter.valueProperty("jde.schema")+".f559911 where
			// rkapfl='A' and rkaid='"+kodeAnggaran+"' ");
			Query q = ses.createSQLQuery(query);
			Object o = q.uniqueResult();
			if (o != null) {
				String data = String.valueOf(o);
				Double db = Double.valueOf(data) / 100;
				db = db + anggaranReserved;
				DecimalFormat df = new DecimalFormat("###.##");
				jumlah = String.valueOf(df.format(db));
			} else {
				DecimalFormat df = new DecimalFormat("###.##");
				jumlah = String.valueOf(df.format(anggaranReserved));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jumlah;
	}

	private Double getAnggaranReserved(String kodeAnggaran) {
		Double jumlah = 0.0;
		String query = "select nvl(sum(a.rkacm1),0) from prc_anggaran_jde_detail a left join prc_main_header b on a.ppm_id = b.ppm_id where trim(a.rkaid) = '"
				+ kodeAnggaran.trim() + "' and b.ppm_proses=" + SessionGetter.ANGGARAN_PROSES_ID.getId()
				+ " and b.ppm_proses_level <> -99 and b.ppm_proses_Level<>-98 and b.ppm_proses_level<>99 ";
		try {
			startTransaction();
			Query q = session.createSQLQuery(query);
			Object o = q.uniqueResult();
			if (o != null) {
				jumlah = Double.valueOf(String.valueOf(o));
			}
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			e.printStackTrace();
		}
		return jumlah;
	}

	@SuppressWarnings("unchecked")
	@Override
	public POHeader setPoHeaderByOr(CtrContractHeader contractHeader, String orderType) throws Exception {
		Session ses = HibernateUtil.getSessionJde();
		POHeader poHeader = new POHeader();
		try {
			String tmp[] = contractHeader.getContractNumber().contains("OP")
					? contractHeader.getContractNumber().trim().split("OP")
					: contractHeader.getContractNumber().trim().split("ZP");
			String com = tmp[1];
			if (com.split("-").length > 1) {
				com = com.split("-")[0].trim();
			}
			String query = "select " + "" + POHeader.SZ_ORDER_COMPANY + "," + // 0
					"" + POHeader.MN_ORDER_NUMBER + ", " + "" + POHeader.SZ_ORDER_TYPE + ", " + ""
					+ POHeader.SZ_ORDER_SUFFIX + "," + "" + POHeader.SZ_BRANCH_PLANT + ", " + ""
					+ POHeader.SZ_ORIGINAL_ORDER_COMPANY + ", " + "" + POHeader.SZ_ORIGINAL_ORDER_NUMBER + ", " + ""
					+ POHeader.SZ_ORIGINAL_ORDER_TYPE + ", " + "" + POHeader.SZ_RELATED_ORDER_COMPANY + "," + ""
					+ POHeader.SZ_RELATED_ORDER_NUMBER + ", " + "" + POHeader.SZ_RELATED_ORDER_TYPE + "," + // 10
					"" + POHeader.MN_SUPPLIER_NUMBER + "," + "" + POHeader.MN_SHIP_TO_NUMBER + "," + ""
					+ POHeader.JD_REQUESTED_DATE + "," + "" + POHeader.JD_ORDER_DATE + ", " + ""
					+ POHeader.JD_PROMISED_DATE + "," + "" + POHeader.JD_CANCEL_DATE + "," + ""
					+ POHeader.SZ_REFERENCE_01 + "," + "" + POHeader.SZ_REFERENCE_02 + "," + ""
					+ POHeader.SZ_DELIVERY_INSTRUCTIONS_01 + "," + "" + POHeader.SZ_DELIVERY_INSTRUCTIONS_02 + "," + // 20
					"" + POHeader.SZ_PRINT_MESSAGE + "," + "" + POHeader.SZ_SUPPLIER_PRICE_GROUP + "," + ""
					+ POHeader.SZ_PAYMENT_TERMS + "," + "" + POHeader.SZ_TAX_EXPLANATION_CODE + "," + ""
					+ POHeader.SZ_TAX_RATE_AREA + "," + "" + POHeader.SZ_TAX_CERTIFICATE + "," + ""
					+ POHeader.C_ASSOCIATED_TEXT + "," + "" + POHeader.SZ_HOLD_CODE + "," + ""
					+ POHeader.SZ_FREIGHT_HANDLING_CODE + "," + "" + POHeader.MN_BUYER_NUMBER + "," + // 30
					"" + POHeader.MN_CARRIER_NUMBER + "," + "" + POHeader.C_EVALUATED_RECEIPTS_FLAG + "," + ""
					+ POHeader.C_SEND_METHOD + "," + "" + POHeader.SZ_LANDED_COST_RULE + "," + ""
					+ POHeader.SZ_APPROVAL_ROUTE_CODE + "," + "" + POHeader.MN_CHANGE_ORDER_NUMBER + "," + ""
					+ POHeader.C_CURRENYC_MODE + "," + "" + POHeader.SZ_TRANSACTION_CURRENCY_CODE + "," + ""
					+ POHeader.MN_CURRENCY_EXCHANGE_RATE + "," + "" + POHeader.SZ_ORDERED_PLACED_BY + "," + // 40
					"" + POHeader.SZ_ORDER_TAKEN_BY + "," + "" + POHeader.SZ_PROGRAM_ID + "," + "" + POHeader.SZ_USER_ID
					+ "," + "" + POHeader.MN_RETAINAGE + "," + "" + POHeader.SZ_DESRIPTION + "," + ""
					+ POHeader.SZ_REMARK + "," + "" + POHeader.JD_EFFECTIVE_DATE + "," + ""
					+ POHeader.JD_PHYSICAL_COMPLETION_DATE + "," + "" + POHeader.SZ_PRICE_ADJUSTMENT_SCHEDULE_N + ","
					+ "" + POHeader.C_AIA_DOCUMENT + "," + // 50
					"" + POHeader.MN_RMA_DOC_NUMBER + "," + "" + POHeader.SZ_RMA_DOC_TYPE + "," + ""
					+ POHeader.JD_USER_RESERVED_DATE + "," + "" + POHeader.MN_USER_RESERVED_AMOUNT + "," + ""
					+ POHeader.SZ_USER_RESERVED_CODE + "," + "" + POHeader.SZ_USER_RESERVED_REFERENCE + "," + ""
					+ POHeader.SZ_USER_RESERVED_NUMBER + "" + " from " + SessionGetter.valueProperty("jde.schema")
					+ ".f4301 " + " where " + POHeader.SZ_ORDER_COMPANY + " = '" + com.trim() + "' and "
					+ POHeader.MN_ORDER_NUMBER + " = " + tmp[0].trim() + " and " + POHeader.SZ_ORDER_TYPE + " = '"
					+ orderType + "' ";
			Query q = ses.createSQLQuery(query);
			List<Object[]> lst = q.list();
			if (lst != null && lst.size() > 0) {
				Iterator<Object[]> it = lst.iterator();
				while (it.hasNext()) {
					Object[] o = it.next();
					it.remove();
					poHeader.setSzOrderCompany(SessionGetter.nullObjectToEmptyString(o[0]));
					poHeader.setMnOrderNumber(Integer.valueOf(SessionGetter.nullNumberToEmptyString(o[1])));
					poHeader.setSzOrderType("O1");
					poHeader.setSzOrderSuffix(SessionGetter.nullObjectToEmptyString(o[3]));
					poHeader.setSzBranchPlant(SessionGetter.nullObjectToEmptyString(o[4]));
					poHeader.setSzOriginalOrderCompany(SessionGetter.nullObjectToEmptyString(o[5]));
					poHeader.setSzOriginalOrderNumber(SessionGetter.nullObjectToEmptyString(o[6]));
					poHeader.setSzOriginalOrderType(SessionGetter.nullObjectToEmptyString(o[7]));
					poHeader.setSzRelatedOrderCompany(SessionGetter.nullObjectToEmptyString(o[8]));
					poHeader.setSzRelatedOrderNumber(SessionGetter.nullObjectToEmptyString(o[9]));
					poHeader.setSzRelatedOrderType(SessionGetter.nullObjectToEmptyString(o[10]));
					poHeader.setMnSupplierNumber(
							Integer.valueOf(contractHeader.getVndHeader().getVendorSmkNo().trim()));// 11
					poHeader.setMnShipToNumber(Integer.valueOf(com.trim()));// 12
					poHeader.setJdRequestedDate(JulianToGregorianViceVersa
							.getDateFromJulian7(SessionGetter.nullObjectToEmptyString(o[13])));
					poHeader.setJdOrderDate(JulianToGregorianViceVersa
							.getDateFromJulian7(SessionGetter.nullObjectToEmptyString(o[14])));
					poHeader.setJdPromisedDate(contractHeader.getTglPenyerahan());// 15
					poHeader.setJdCancelDate(JulianToGregorianViceVersa
							.getDateFromJulian7(SessionGetter.nullObjectToEmptyString(o[16])));
					poHeader.setSzReference01(SessionGetter.nullObjectToEmptyString(o[17]));
					poHeader.setSzReference02(SessionGetter.nullObjectToEmptyString(o[18]));
					poHeader.setSzDeliveryInstructions01(SessionGetter.nullObjectToEmptyString(o[19]));
					poHeader.setSzDeliveryInstructions02(SessionGetter.nullObjectToEmptyString(o[20]));
					poHeader.setSzPrintMessage("");// 21
					poHeader.setMnBuyerNumber(Integer.valueOf(SessionGetter.nullNumberToEmptyString(o[30])));
					poHeader.setMnCarrierNumber(Integer.valueOf(SessionGetter.nullNumberToEmptyString(o[31])));
					poHeader.setSzOrderedPlacedBy("");
				}
			}
			ses.clear();
			ses.close();
		} catch (Exception e) {
			ses.clear();
			ses.close();
			e.printStackTrace();
			throw e;
		}
		return poHeader;
	}

	@Override
	public List<PODetail> getListPoDetail(POHeader poHeader, String orderType) throws Exception {
		Session ses = HibernateUtil.getSessionJde();
		List<PODetail> retMe = new ArrayList<PODetail>();
		try {
			String query = "select  " + "" + PODetail.MN_ORDER_LINE_NUMBER + "," + // 0
					"" + PODetail.SZ_ORDER_COMPANY + "," + "" + PODetail.MN_ORDER_NUMBER + "," + ""
					+ PODetail.SZ_ORDER_TYPE + "," + "" + PODetail.SZ_ORDER_SUFFIX + "," + ""
					+ PODetail.SZ_ORDER_BRANCH_PLANT + "," + "" + PODetail.SZ_ORIGINAL_ORDER_COMPANY + "," + ""
					+ PODetail.SZ_ORIGINAL_ORDER_NUMBER + "," + "" + PODetail.SZ_ORIGINAL_ORDER_TYPE + "," + ""
					+ PODetail.MN_ORIGINAL_LINE_NUMBER + "," + "" + PODetail.SZ_RELATED_ORDER_COMPANY + "," + // 10
					"" + PODetail.SZ_RELATED_ORDER_NUMBER + "," + "" + PODetail.SZ_RELATED_ORDER_TYPE + "," + ""
					+ PODetail.MN_RELATED_ORDER_LINE + "," + "" + PODetail.MN_SUPPLIER_NUMBER + "," + ""
					+ PODetail.MN_SHIP_TO_NUMBER + "," + "" + PODetail.JD_REQUESTED_DATE + "," + ""
					+ PODetail.JD_TRANSACTION_DATE + "," + "" + PODetail.JD_PROMISED_DATE + "," + ""
					+ PODetail.JD_CANCEL_DATE + "," + "" + PODetail.JD_GL_DATE + "," + // 20
					"" + PODetail.SZ_UNFORMATED_ITEM_NUMBER + "," + "" + PODetail.MN_QUANTITY_ORDERED + "," + ""
					+ PODetail.MN_UNIT_PRICE + "," + "" + PODetail.C_PRICE_OVERRIDE_FLAG + "," + ""
					+ PODetail.MN_EXTENDED_PRICE + "," + "" + PODetail.SZ_LINE_TYPE + "," + ""
					+ PODetail.SZ_DESCRIPTION_1 + "," + "" + PODetail.SZ_DESCRIPTION_2 + "," + "" + PODetail.SZ_ASSET_ID
					+ "," + "" + PODetail.SZ_LOCATION + "," + // 30
					"" + PODetail.SZ_LOT_NUMBER + "," + "" + PODetail.SZ_TRANSACTION_UOM + "," + ""
					+ PODetail.SZ_PURCHASING_UOM + "," + "" + PODetail.SZ_LAST_STATUS + "," + ""
					+ PODetail.SZ_NEXT_STATUS + "," + "" + PODetail.SZ_UNFORMATTED_ACCOUNT_NUMBER + "," + // 36
					"" + PODetail.SZ_SUBLEDGER + "," + "" + PODetail.C_SUBLEDGER_TYPE + "," + ""
					+ PODetail.MN_DISCOUNT_FACTOR + "," + "" + PODetail.SZ_CATALOG_NUMBER + "," + // 40
					"" + PODetail.SZ_INVENTORY_PRICE_RULE + "," + "" + PODetail.SZ_PRICE_LEVEL + "," + ""
					+ PODetail.SZ_PRINT_MESSAGE + "," + "" + PODetail.C_TAXABLE + "," + ""
					+ PODetail.SZ_TAX_EXPLANATION_CODE + "," + "" + PODetail.SZ_TAX_RATE_AREA + "," + ""
					+ PODetail.MN_BUYER_NUMBER + "," + "" + PODetail.MN_CARRIER_NUMBER + "," + ""
					+ PODetail.SZ_PURCHASING_CATEGORY_CODE_1 + "," + "" + PODetail.SZ_PURCHASING_CATEGORY_CODE_2 + "," + // 50
					"" + PODetail.SZ_PURCHASING_CATEGORY_CODE_3 + "," + "" + PODetail.SZ_PURCHASING_CATEGORY_CODE_4
					+ "," + "" + PODetail.MN_WEIGHT + "," + "" + PODetail.SZ_WEIGHT_UOM + "," + // 54
					"" + PODetail.SZ_VOLUME_UOM + "," + // 55
					"" + PODetail.MN_VOLUME + "," + "" + PODetail.SZ_REFERENCE_1 + "," + "" + PODetail.SZ_REFERENCE_2
					+ "," + "" + PODetail.C_SEND_METHOD + "," + "" + PODetail.C_FREEZE_CODE + "," + // 60
					"" + PODetail.C_EVALUATED_RECEIPTS + "," + "" + PODetail.SZ_TRANSACTION_CURRENCY_CODE + "," + ""
					+ PODetail.MN_CURRENCY_EXCHANGE_RATE + "," + "" + PODetail.SZ_PROGRAM_ID + "," + ""
					+ PODetail.SZ_USER_ID + "," + "" + PODetail.MN_UNIQUE_KEY + "," + ""
					+ PODetail.MN_SOURCE_OF_DATA_UNIT_OPEN + "," + "" + PODetail.SZ_PAYMENT_TERMS + "," + ""
					+ PODetail.SZ_FREIGHT_HANDLING_CODE + "," + "" + PODetail.SZ_USER_RESERVED_CODE + "," + // 70
					"" + PODetail.JD_USER_RESERVED_DATE + "," + "" + PODetail.MN_USER_RESERVED_AMOUNT + "," + ""
					+ PODetail.MN_USER_RESERVED_NUMBER + "," + "" + PODetail.SZ_USER_RESERVED_REFERENCE + "," + ""
					+ PODetail.SZ_AGREEMENT_NUMBER + "," + "" + PODetail.MN_AGREEMENT_SUPPLEMENT + "," + ""
					+ PODetail.JD_EFFECTIVE_DATE + "," + "" + PODetail.JD_PHYSICAL_COMPLETION_DATE + "," + ""
					+ PODetail.SZ_PURCHASING_COST_CENTER + "," + "" + PODetail.SZ_OBJECT_ACCOUNT + "," + // 80
					"" + PODetail.SZ_SUBSIDIARY + "," + "" + PODetail.SZ_ITEM_NUMBER_RELATED_KIT + "," + ""
					+ PODetail.SZ_ADJUSTMENT_SCHEDULE + "," + "" + PODetail.C_TRANSFER_ORDER + "," + ""
					+ PODetail.MN_SHIPMENT_NUMBER + "," + "" + PODetail.MN_IDENTIFIER_SHORT_ITEM + "," + ""
					+ PODetail.MN_SECONDARY_QTY + "," + "" + PODetail.SZ_SECONDARY_UOM + "," + ""
					+ PODetail.JD_LOT_EFFECTIVITY_DATE + "," + "" + PODetail.SZ_PROMOTION_ID + "," + // 90
					"" + PODetail.MN_MATRIX_CONTROL_LINE + "," + "" + PODetail.MN_PROJECT_NUMBER + "," + ""
					+ PODetail.SZ_MODE_TRANSPORT + "," + "" + PODetail.SZ_PLAN + "," + "" + PODetail.SZ_ELEVATION + ","
					+ "" + PODetail.SZ_UNSPSC_CODE + "," + "" + PODetail.SZ_COMMODITY_CODE + "," + ""
					+ PODetail.JD_DATE_PROMISED_SHIP_JU + "," + "" + PODetail.C_MULTIPLE_ACCOUNT_EXIST_MACT + "," + ""
					+ PODetail.SZ_TRANSACTION_ORIGINATOR_TORG + "," + // 100
					"" + PODetail.SZ_RELATED_ORDER_SUFFIX + "," + "" + PODetail.MN_CONTRACT_ID + "," + ""
					+ PODetail.MN_CONTRACT_DETAIL_ID + "," + "" + PODetail.C_MANUAL_OVERIDE + "," + ""
					+ PODetail.MN_VESSEL_ID + "," + "" + PODetail.SZ_GL_CLASS_CODE + "," + ""
					+ PODetail.IDENTIFIER_SHORT_ITEM + "," + "" + PODetail.MN_SOURCE_OF_DATA_PRICE_OPEN + "" + // 108
					" from  " + SessionGetter.valueProperty("jde.schema") + ".f4311 " + " where "
					+ PODetail.SZ_ORDER_COMPANY + "='" + poHeader.getSzOrderCompany() + "' and "
					+ PODetail.MN_ORDER_NUMBER + "=" + poHeader.getMnOrderNumber() + " and " + PODetail.SZ_ORDER_TYPE
					+ "='" + orderType + "' " + "and " + PODetail.SZ_NEXT_STATUS + "='130' and "
					+ PODetail.SZ_LAST_STATUS + "='125' " + "ORDER BY pdlnid asc ";
			// + " pddsc1 asc,pddsc2 asc";
			Query q = ses.createSQLQuery(query);
			List<Object[]> obj = q.list();
			if (obj != null && obj.size() > 0) {
				Iterator<Object[]> it = obj.iterator();
				while (it.hasNext()) {
					Object o[] = it.next();
					it.remove();
					PODetail pod = new PODetail();
					pod.setSzOriginalOrderNumber(SessionGetter.nullNumberToEmptyString(o[2]));
					pod.setMnOrderLineNumber(Integer.valueOf(SessionGetter.nullNumberToEmptyString(o[0])));
					pod.setJdGLDate(JulianToGregorianViceVersa
							.getDateFromJulian7(SessionGetter.nullObjectToEmptyString(o[20])));
					pod.setSzUnformattedItemNumber(SessionGetter.nullNumberToEmptyString(o[21]));
					pod.setMnQuantityOrdered(Double.valueOf(SessionGetter.nullNumberToEmptyString(o[22])));
					pod.setSzLineType(SessionGetter.nullObjectToEmptyString(o[26]));
					pod.setSzDescription1(SessionGetter.nullObjectToEmptyString(o[27]));
					pod.setSzDescription2(SessionGetter.nullObjectToEmptyString(o[28]));
					pod.setSzAssetID(SessionGetter.nullObjectToEmptyString(o[29]));
					pod.setSzLocation(SessionGetter.nullObjectToEmptyString(o[30]));
					pod.setSzLotNumber(SessionGetter.nullObjectToEmptyString(o[31]));
					pod.setSzTransactionUoM(SessionGetter.nullObjectToEmptyString(o[32]));
					pod.setSzPurchasingUoM(SessionGetter.nullObjectToEmptyString(o[33]));
					pod.setSzUnformattedAccountNumber(SessionGetter.nullObjectToEmptyString(o[36]));
					pod.setSzSubledger(SessionGetter.nullObjectToEmptyString(o[37]));
					pod.setSubledgerType(SessionGetter.nullObjectToEmptyString(o[38]));
					pod.setMnDiscountFactor(Double.valueOf(SessionGetter.nullObjectToEmptyString(o[39])));
					pod.setSzCatalogNumber(SessionGetter.nullObjectToEmptyString(o[40]));
					pod.setSzInventoryPriceRule(SessionGetter.nullObjectToEmptyString(o[41]));
					pod.setSzPriceLevel(SessionGetter.nullObjectToEmptyString(o[42]));
					pod.setSzPrintMessage(SessionGetter.nullObjectToEmptyString(o[43]));
					pod.setTaxable(SessionGetter.nullObjectToEmptyString(o[44]));
					pod.setSzPurchasingCategoryCode1(SessionGetter.nullObjectToEmptyString(o[49]));
					pod.setSzPurchasingCategoryCode2(SessionGetter.nullObjectToEmptyString(o[50]));
					pod.setSzPurchasingCategoryCode3(SessionGetter.nullObjectToEmptyString(o[51]));
					pod.setSzPurchasingCategoryCode4(SessionGetter.nullObjectToEmptyString(o[52]));
					pod.setSzWeightUoM(SessionGetter.nullObjectToEmptyString(o[54]));
					pod.setSzVolumeUoM(SessionGetter.nullObjectToEmptyString(o[55]));
					pod.setSzPurchasingCostCenter(SessionGetter.nullObjectToEmptyString(o[79]));
					pod.setSzObjectAccount(SessionGetter.nullObjectToEmptyString(o[80]));
					pod.setSzSubsidiary(SessionGetter.nullObjectToEmptyString(o[81]));
					pod.setSzGLClassCode(SessionGetter.nullObjectToEmptyString(o[106]));
					retMe.add(pod);
				}
			}
			ses.clear();
			ses.close();
		} catch (Exception e) {
			ses.clear();
			ses.close();
			e.printStackTrace();
			throw e;
		}
		return retMe;
	}

	@Override
	public Integer countMessage(Integer status) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMsgChat.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setProjection(Projections.count("id"));
		crit.add(Restrictions.eq("prcMainHeader", new PrcMainHeader(Long.valueOf(status))));
		Object obj = getEntityByDetachedCiteria(crit);
		return Integer.valueOf(String.valueOf(obj));
	}

	@Override
	public void updateTanggalKontrak(CtrContractHeader contractHeader) throws Exception {
		try {
			startTransaction();
			CtrContractHeader ch = getContractHeaderById(contractHeader);
			ch.setTglPenyerahan(contractHeader.getTglPenyerahan());
			ch.setContractStart(contractHeader.getContractStart());
			ch.setContractEnd(contractHeader.getContractEnd());
			saveEntity(ch);

			// update ke jde
			if (ch.getJdeNumber().contains("O1")) {
				String tmp[] = ch.getJdeNumber().split("O1");
				String nomorOp = tmp[0];
				String kantor = tmp[1];
				//JdeService.updateTanggalKontrak(ch, nomorOp, "O1", kantor);
			} else if (ch.getJdeNumber().contains("O2")) {
				String tmp[] = ch.getJdeNumber().split("O2");
				String nomorOp = tmp[0];
				String kantor = tmp[1];
				//JdeService.updateTanggalKontrak(ch, nomorOp, "O2", kantor);
			} else if (ch.getJdeNumber().contains("O4")) {
				String tmp[] = ch.getJdeNumber().split("O4");
				String nomorOp = tmp[0];
				String kantor = tmp[1];
				//JdeService.updateTanggalKontrak(ch, nomorOp, "O4", kantor);
			} else if (ch.getJdeNumber().contains("O9")) {
				String tmp[] = ch.getJdeNumber().split("O9");
				String nomorOp = tmp[0];
				String kantor = tmp[1];
				//JdeService.updateTanggalKontrak(ch, nomorOp, "O9", kantor);
			}
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}

	}

	@Override
	public void saveCloseKontrak(CtrContractHeader contractHeader, CtrContractComment ctrContractComment,
			File docsKomentar, String docsKomentarFileName) throws Exception {
		try {
			startTransaction();
			CtrContractHeader chf = getContractHeaderById(contractHeader);

			// langsung selesai
			chf.setAdmUserByCurrentUser(null);
			chf.setAdmRole(null);
			chf.setContractProsesLevel(SessionGetter.CLOSE);
			saveEntity(chf);

			// upload doc
			if (SessionGetter.isNotNull(docsKomentar)) {
				ctrContractComment.setDocument(UploadUtil.uploadDocsFile(docsKomentar, docsKomentarFileName));
			}

			// insert History
			ctrContractComment.setProsesAksi("Close Kontrak");

			CtrContractComment last = new CtrContractComment();
			last.setContractId(new BigDecimal(chf.getContractId()));
			last.setAksi("Close Kontrak");
			last.setProses(chf.getAdmProses().getId());
			last.setDocument(ctrContractComment.getDocument());
			last.setComment(ctrContractComment.getComment());
			last.setProsesAksi("Close Kontrak");
			last.setUpdatedDate(new Date());
			last.setUsername(SessionGetter.getUser().getCompleteName());
			last.setPosisi(SessionGetter.getUser().getAdmEmployee().getEmpStructuralPos());
			saveEntity(last);

			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			e.printStackTrace();
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<HistoryHargaNegosiasi> listHargaNegosiasi(PrcMainHeader prcMainHeader) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(HistoryHargaNegosiasi.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("ppmId", prcMainHeader.getPpmId()));
		crit.addOrder(Order.asc("vendorName"));
		crit.addOrder(Order.asc("createdDate"));
		return (List<HistoryHargaNegosiasi>) getListByDetachedCiteria(crit);
	}
	
	@Override
	public void sendEmailToVendor(PrcMainHeader prcMainHeader) throws Exception{
		List<Long> listVendor = getListPemenang(prcMainHeader.getPpmId(), session);
		if (listVendor != null) {
			String recipients[] = new String[listVendor.size()];
			int i = 0;
			List<VndHeader> lVh = new ArrayList<VndHeader>();
			for (Long id : listVendor) {
				VndHeader vh = new VendorServiceImpl().getVndHeaderById(id);
				recipients[i] = vh.getVendorEmail();
				lVh.add(vh);
				i++;
			}
			Object o[] = new Object[3];
			o[0] = prcMainHeader.getPpmNomorRfq();
			o[1] = prcMainHeader.getPpmSubject();
			o[2] = prcMainHeader.getPpmNomorRfq();
			String message = SessionGetter.getPropertiesValue("email.eproc.calonpemenang", o);
			EmailSessionBean.sendMail("eProc PT. Semen Baturaja (Persero)", message, recipients);

			List<PrcMainVendor> listNotWinner = getListMainVendorNotWinner(lVh, prcMainHeader);
			if (listNotWinner != null) {
				String rcpt[] = new String[listNotWinner.size()];
				int k = 0;
				for (PrcMainVendor d : listNotWinner) {
					rcpt[k] = d.getVndHeader().getVendorEmail();
					k++;
				}
				Object o2[] = new Object[3];
				o2[0] = prcMainHeader.getPpmNomorRfq();
				o2[1] = prcMainHeader.getPpmSubject();
				o2[2] = prcMainHeader.getPpmNomorRfq();
				String msg = SessionGetter.getPropertiesValue("email.eproc.bukanpemenang", o2);
				EmailSessionBean.sendMail("eProc PT. Semen Baturaja (Persero)", msg, rcpt);
			}
		}
	}
	
	@Override
	public AdmProsesHirarki getHirarkiProsesJenisKontrak(CtrContractHeader ctrContractHeader) throws Exception {
		try{
			AdmProsesHirarki ahp = new AdmProsesHirarki();
			if(ctrContractHeader.getPrcMainHeader().getPpmJenis().getId() == 1){
				ahp = SessionGetter.getHirarkiProsesByJenis(ctrContractHeader.getAdmProses(), ctrContractHeader.getContractProsesLevel()+1, ctrContractHeader.getAdmDistrict(), ctrContractHeader.getContractAmount().doubleValue(), 1);
			}else if(ctrContractHeader.getPrcMainHeader().getPpmJenis().getId() == 2){
				ahp = SessionGetter.getHirarkiProsesByJenis(ctrContractHeader.getAdmProses(), ctrContractHeader.getContractProsesLevel()+1, ctrContractHeader.getAdmDistrict(), ctrContractHeader.getContractAmount().doubleValue(), 2);
			}else{
				throw new Exception("Jenis OR tidak terdaftar");
			}
			

			if(ahp==null){
				return null;
			}else{
				return ahp;
			}
			
		}catch(Exception e){
			throw e;
		}
		
		
	}
	
	@Override
	public AdmProsesHirarki getHirarkiProsesJenisByDistrict(PrcMainHeader prcMainHeader) throws Exception{
		try {
			AdmProsesHirarki ahp = new AdmProsesHirarki();
			AdmDistrict kantor = new AdmDistrict();
			
			if(prcMainHeader.getPpmNomorPrSap().contains("1201")) {
				kantor = new AdmDistrict(104);
				kantor = new AdmServiceImpl().getDistrictById(kantor);
			}else if(prcMainHeader.getPpmNomorPrSap().contains("1202")  || prcMainHeader.getPpmNomorPrSap().contains("1203")  || prcMainHeader.getPpmNomorPrSap().contains("1601")) {
				kantor = new AdmDistrict(107);
				kantor = new AdmServiceImpl().getDistrictById(kantor);
			}else if(prcMainHeader.getPpmNomorPrSap().contains("1204")) {
				kantor = new AdmDistrict(110);
				kantor = new AdmServiceImpl().getDistrictById(kantor);
			}
			
			
			if(prcMainHeader.getPpmJenis().getId()== 1){
				ahp = SessionGetter.getHirarkiProsesByJenis(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel()+1, kantor, prcMainHeader.getPpmJumlahOe().doubleValue(), 1);
			}else if(prcMainHeader.getPpmJenis().getId()==2){
				ahp = SessionGetter.getHirarkiProsesByJenis(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel()+1, kantor, prcMainHeader.getPpmJumlahOe().doubleValue(), 2);
			}else{
				throw new Exception("Jenis OR tidak terdaftar");
			}
			if(ahp==null) {
				ahp = SessionGetter.getHirarkiProsesByDistrict(prcMainHeader.getAdmProses(), prcMainHeader.getPpmProsesLevel()+1, 
						kantor, prcMainHeader.getPpmJumlahOe().doubleValue());
			}
			

			if(ahp==null){
				return null;
			}else{
				return ahp;
			}
			
		}catch(Exception e){
			throw e;
		}
		}
	
	public AdmUser getManagerPengadaanByJenis(PrcMainHeader prcMainHeader) throws Exception {
		AdmUser manager = new AdmUser();
		manager = SessionGetter.getManagerPengadaanByJenis(prcMainHeader);
		return manager;
	}
	
	@Override
	public void saveContractNumberSap(CtrContractHeader contractHeader, String contractNumber) throws Exception {
		try {
			startTransaction();
			CtrContractHeader chf = getContractHeaderById(contractHeader);
			chf.setContractNumberSap(contractNumber);
			saveEntity(chf);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<AdmPurchasingOrganization> listAdmPurchOrgByPlant(String kodePlant){
		DetachedCriteria crit = DetachedCriteria.forClass(AdmPurchasingOrganization.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("kodePlant", kodePlant.trim()));
		System.out.println(kodePlant);
//		if(kodePlant=="" || kodePlant.equalsIgnoreCase("")){
//			crit.add(Restrictions.or(Restrictions.like("kodePurcOrg", "1001"), Restrictions.like("kodePlant", kodePlant)));
//			crit.add(Restrictions.sqlRestriction("rownum <=1"));
//		}else{
//			crit.add(Restrictions.and(Restrictions.like("kodePurcOrg", "1001"), Restrictions.like("kodePlant", kodePlant)));
//		}
		//crit.setProjection(Projections.distinct(Projections.property("kodePurchOrg")));
		return  (List<AdmPurchasingOrganization>) getListByDetachedCiteria(crit);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public AdmPurchasingOrganization getAdmPurchOrgByKode(String kodePurchOrg, String kodePlant){
		DetachedCriteria crit = DetachedCriteria.forClass(AdmPurchasingOrganization.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.and(Restrictions.like("kodePurcOrg", kodePurchOrg), Restrictions.like("kodePlant", kodePlant.trim())));
		//crit.setProjection(Projections.distinct(Projections.property("kodePurchOrg")));
		return  (AdmPurchasingOrganization) getEntityByDetachedCiteria(crit);
	}
	
	@Override
	public ResultDto saveContractSap(CtrContractHeader ctrContractHeader, List<CtrContractItem> listContractItem)  {
		PoHeaderDto poSap = new PoHeaderDto();
		ResultDto res = new ResultDto();
		try {
			IntegrationService integrationService = new IntegrationServiceImpl();
			ResultDto r =  integrationService.pushPO(ctrContractHeader, listContractItem);
			if(r.getData()!=null) {
				poSap  = (PoHeaderDto)r.getData();
			}else {
				throw new Exception(r.getMessage());
			}
			
			VendorService vendorService = new VendorServiceImpl();
			VndHeader vnd = vendorService.getVndHeaderById(Long.valueOf(poSap.getELIFN()));
			
			if(poSap.getEBELN()!=null) {
				if (poSap.getEBELN() != "" || !poSap.getEBELN().equals("")) {
					saveContractNumberSap(ctrContractHeader, poSap.getEBELN());
					Long vendorIdSap = Long.valueOf(poSap.getLIFNR());
					System.out.println(">>> PARSED ID" + vendorIdSap);
					vendorService.saveVendorIdSap(vnd, vendorIdSap);
					res.setStatus(200);
					res.setData(poSap);
					res.setMessage("Sukses");
				}else {
					res.setData(poSap);
					res.setStatus(500);
					res.setMessage("Terjadi Kesalahan");
				}
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return res;
	}

}
	
	
	

