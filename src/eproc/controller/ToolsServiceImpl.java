package eproc.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.hibernate.FetchMode;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.type.StandardBasicTypes;

import com.eproc.utils.SessionGetter;
import com.orm.model.AdmDistrict;
import com.orm.model.PrcMainHeader;
import com.orm.model.PrcMainPreparation;
import com.orm.model.PrcMainVendor;
import com.orm.model.PrcMsgSanggahan;
import com.orm.model.TmpVndHeader;
import com.orm.model.VndHeader;
import com.orm.tools.GenericDao;

public class ToolsServiceImpl extends GenericDao implements ToolsService {

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> searchPrcMainHeader(PrcMainHeader o, Integer page, Integer size) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMainHeader.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);
		crit.createAlias("admUserByPpmCurrentUser", "us");
		crit.setFetchMode("admDept", FetchMode.JOIN);
		crit.add(Restrictions.or(
				Restrictions.ilike("us.completeName", o.getPpmNomorPr().toLowerCase(), MatchMode.ANYWHERE),
				Restrictions.or(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE),
						Restrictions.like("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE))));
		if (o.getDistrictUser() != null) {
			crit.add(Restrictions.eq("districtUser", o.getDistrictUser()));
		}

		return (List<PrcMainHeader>) getListByDetachedCiteriaPaging(crit, page, size);
	}

	@Override
	public boolean editTanggal(PrcMainPreparation o) throws Exception {
		boolean ret = false;
		try {
			startTransaction();
			ret = updateEntity(o);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			e.printStackTrace();
		}
		return ret;
	}

	@Override
	public PrcMainPreparation getPrcMainPreparation(PrcMainHeader o) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMainPreparation.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.idEq(o.getPpmId()));
		return (PrcMainPreparation) getEntityByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> getListSanggahanPq(Object o) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMainHeader.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -4);

		crit.add(Restrictions.ge("updatedDate", cal.getTime()));
		crit.add(Restrictions.or(
				Restrictions.and(Restrictions.eq("admProses", SessionGetter.PQ_PROSES_ID),
						Restrictions.eq("ppmProsesLevel", 3)),
				Restrictions.and(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID),
						Restrictions.gt("ppmProsesLevel", 3))));
		DetachedCriteria se = crit.createCriteria("prcMainVendors", JoinType.INNER_JOIN);
		se.add(Restrictions.eq("vndHeader", o));

		return (List<PrcMainHeader>) getListByDetachedCiteria(crit);
	}

	@Override
	public PrcMainHeader getPrcMainHeader(PrcMainHeader o) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMainHeader.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("ppmId", o.getPpmId()));

		return (PrcMainHeader) getEntityByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMsgSanggahan> getListSanggahan(PrcMainHeader h, VndHeader v) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMsgSanggahan.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("prcMainHeader", h));
		if (v != null)
			crit.add(Restrictions.eq("vndHeader", v));

		return (List<PrcMsgSanggahan>) getListByDetachedCiteria(crit);
	}

	@Override
	public boolean saveSanggahan(PrcMsgSanggahan o) {
		boolean ret = false;
		try {
			startTransaction();
			ret = saveEntity(o);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			e.printStackTrace();
		}
		return ret;
	}

	@Override
	public VndHeader getVndHeader(TmpVndHeader o) {
		DetachedCriteria crit = DetachedCriteria.forClass(VndHeader.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("vendorId", o.getVendorId()));

		return (VndHeader) getEntityByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainVendor> getListPrcVendor(PrcMainHeader o) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMainVendor.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("vndHeader", FetchMode.JOIN);
		crit.add(Restrictions.eq("prcMainHeader", o));

		return (List<PrcMainVendor>) getListByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> searchPrcMainHeaderAll(PrcMainHeader o, Integer year) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMainHeader.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("admDept", FetchMode.JOIN);
		crit.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);

		if (o.getDistrictUser() != null) {
			crit.add(Restrictions.eq("districtUser", o.getDistrictUser()));
		}
		if (o != null && o.getPpmProsesLevel() != null) {
			if (o.getPpmProsesLevel() == 0) {
				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					crit.createAlias("admUserByPpmCurrentUser", "us", JoinType.LEFT_OUTER_JOIN);
					crit.add(Restrictions.or(
							Restrictions.ilike("us.completeName", o.getPpmNomorPr().toLowerCase(), MatchMode.ANYWHERE),
							Restrictions.or(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE),
									Restrictions.like("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE))));
				}
				crit.add(Restrictions.ne("ppmProsesLevel", 99));
				crit.add(Restrictions.ne("ppmProsesLevel", -99));
				crit.add(Restrictions.ne("ppmProsesLevel", -98));
				crit.add(Restrictions.ne("ppmProsesLevel", -100));

			} else if (o.getPpmProsesLevel() == -1) {
				// all
				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
			} else if (o.getPpmProsesLevel() == 1) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.PR_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 1));
			} else if (o.getPpmProsesLevel() == 2) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.PR_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 2));
			} else if (o.getPpmProsesLevel() == 3) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.PR_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", -1));
			} else if (o.getPpmProsesLevel() == 4) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.OE_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 2));
			} else if (o.getPpmProsesLevel() == 5) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.OE_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 1));
			} else if (o.getPpmProsesLevel() == 6) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.OE_PROSES_ID));
				crit.add(Restrictions.or(Restrictions.eq("ppmProsesLevel", 3), Restrictions.eq("ppmProsesLevel", 4),
						Restrictions.eq("ppmProsesLevel", 5), Restrictions.eq("ppmProsesLevel", 6)));
			} else if (o.getPpmProsesLevel() == 7) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.OE_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", -2));
			} else if (o.getPpmProsesLevel() == 8) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.PP_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 1));
			} else if (o.getPpmProsesLevel() == 9) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.PP_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 2));
			} else if (o.getPpmProsesLevel() == 10) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.PP_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 3));
			} else if (o.getPpmProsesLevel() == 11) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.PP_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 4));
			} else if (o.getPpmProsesLevel() == 12) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.PP_PROSES_ID));
				crit.add(Restrictions.or(Restrictions.eq("ppmProsesLevel", 5), Restrictions.eq("ppmProsesLevel", 6),
						Restrictions.eq("ppmProsesLevel", 7), Restrictions.eq("ppmProsesLevel", 8),
						Restrictions.eq("ppmProsesLevel", 9)));
			} else if (o.getPpmProsesLevel() == 13) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.PP_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", -3));
			} else if (o.getPpmProsesLevel() == 14) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.ANGGARAN_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 1));
			} else if (o.getPpmProsesLevel() == 15) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.ANGGARAN_PROSES_ID));
				crit.add(Restrictions.or(Restrictions.eq("ppmProsesLevel", 2), Restrictions.eq("ppmProsesLevel", 3),
						Restrictions.eq("ppmProsesLevel", 4), Restrictions.eq("ppmProsesLevel", 5)));
			} else if (o.getPpmProsesLevel() == 16) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.ANGGARAN_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", -1));
			} else if (o.getPpmProsesLevel() == 17) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 1));
			} else if (o.getPpmProsesLevel() == 18) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 2));
			} else if (o.getPpmProsesLevel() == 19) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 3));
			} else if (o.getPpmProsesLevel() == 20) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 4));
			} else if (o.getPpmProsesLevel() == 21) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 5));
			} else if (o.getPpmProsesLevel() == 22) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 6));
			} else if (o.getPpmProsesLevel() == 23) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
				crit.add(Restrictions.or(Restrictions.eq("ppmProsesLevel", 7), Restrictions.eq("ppmProsesLevel", 8),
						Restrictions.eq("ppmProsesLevel", 9), Restrictions.eq("ppmProsesLevel", 10),
						Restrictions.eq("ppmProsesLevel", 11), Restrictions.eq("ppmProsesLevel", 12),
						Restrictions.eq("ppmProsesLevel", 13), Restrictions.eq("ppmProsesLevel", 14),
						Restrictions.eq("ppmProsesLevel", 15), Restrictions.eq("ppmProsesLevel", 16)));
			} else if (o.getPpmProsesLevel() == 24) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 17));
			} else if (o.getPpmProsesLevel() == 25) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.VENDOR_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 1));
			} else if (o.getPpmProsesLevel() == 26) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.VENDOR_PROSES_ID));
				crit.add(Restrictions.or(Restrictions.eq("ppmProsesLevel", 2), Restrictions.eq("ppmProsesLevel", 3),
						Restrictions.eq("ppmProsesLevel", 4)));
			} else if (o.getPpmProsesLevel() == 27) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.PQ_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 1));
			} else if (o.getPpmProsesLevel() == 28) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.PQ_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 2));
			} else if (o.getPpmProsesLevel() == 29) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.PQ_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 3));
			} else if (o.getPpmProsesLevel() == 30) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.KONTRAK_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 1));
			} else if (o.getPpmProsesLevel() == 31) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.KONTRAK_PROSES_ID));
				crit.add(Restrictions.or(Restrictions.eq("ppmProsesLevel", 2), Restrictions.eq("ppmProsesLevel", 3),
						Restrictions.eq("ppmProsesLevel", 4), Restrictions.eq("ppmProsesLevel", 5),
						Restrictions.eq("ppmProsesLevel", 6), Restrictions.eq("ppmProsesLevel", 7)));
			} else if (o.getPpmProsesLevel() == 32) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.KONTRAK_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 8));
			} else if (o.getPpmProsesLevel() == 33) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.PANITIA_PP_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 1));
			} else if (o.getPpmProsesLevel() == 34) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.REVISI_EE));
				crit.add(Restrictions.eq("ppmProsesLevel", 1));

			} else if (o.getPpmProsesLevel() == 35) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.REVISI_EE));
				crit.add(Restrictions.eq("ppmProsesLevel", 2));

			} else if (o.getPpmProsesLevel() == 36) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.REALOKASI));
				crit.add(Restrictions.eq("ppmProsesLevel", 1));
			} else {
				// crit.add(Restrictions.isNull("admUserByPpmCurrentUser"));
				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("ppmProsesLevel", o.getPpmProsesLevel()));
			}
		}
		/*
		 * crit.add(Restrictions.sqlRestriction("to_char({alias}." +
		 * "CREATED_DATE, 'yyyy') = ?", year, StandardBasicTypes.INTEGER));
		 */

		crit.addOrder(Order.desc("updatedDate"));
		List<PrcMainHeader> retMe = new ArrayList<PrcMainHeader>();
		retMe = ((List<PrcMainHeader>) getListByDetachedCiteria(crit));
		return retMe;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> searchPrcMainHeaderAllByYear(PrcMainHeader o, Integer year) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMainHeader.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("admDept", FetchMode.JOIN);
		crit.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);

		if (o.getDistrictUser() != null) {
			crit.add(Restrictions.eq("districtUser", o.getDistrictUser()));
		}
		if (o != null && o.getPpmProsesLevel() != null) {
			if (o.getPpmProsesLevel() == 0) {
				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					crit.createAlias("admUserByPpmCurrentUser", "us", JoinType.LEFT_OUTER_JOIN);
					crit.add(Restrictions.or(
							Restrictions.ilike("us.completeName", o.getPpmNomorPr().toLowerCase(), MatchMode.ANYWHERE),
							Restrictions.or(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE),
									Restrictions.like("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE))));
				}
				crit.add(Restrictions.ne("ppmProsesLevel", 99));
				crit.add(Restrictions.ne("ppmProsesLevel", -99));
				crit.add(Restrictions.ne("ppmProsesLevel", -98));
				crit.add(Restrictions.ne("ppmProsesLevel", -100));

			} else if (o.getPpmProsesLevel() == -1) {
				// all
				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
			} else if (o.getPpmProsesLevel() == 1) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.PR_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 1));
			} else if (o.getPpmProsesLevel() == 2) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.PR_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 2));
			} else if (o.getPpmProsesLevel() == 3) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.PR_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", -1));
			} else if (o.getPpmProsesLevel() == 4) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.OE_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 2));
			} else if (o.getPpmProsesLevel() == 5) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.OE_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 1));
			} else if (o.getPpmProsesLevel() == 6) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.OE_PROSES_ID));
				crit.add(Restrictions.or(Restrictions.eq("ppmProsesLevel", 3), Restrictions.eq("ppmProsesLevel", 4),
						Restrictions.eq("ppmProsesLevel", 5), Restrictions.eq("ppmProsesLevel", 6)));
			} else if (o.getPpmProsesLevel() == 7) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.OE_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", -2));
			} else if (o.getPpmProsesLevel() == 8) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.PP_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 1));
			} else if (o.getPpmProsesLevel() == 9) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.PP_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 2));
			} else if (o.getPpmProsesLevel() == 10) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.PP_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 3));
			} else if (o.getPpmProsesLevel() == 11) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.PP_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 4));
			} else if (o.getPpmProsesLevel() == 12) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.PP_PROSES_ID));
				crit.add(Restrictions.or(Restrictions.eq("ppmProsesLevel", 5), Restrictions.eq("ppmProsesLevel", 6),
						Restrictions.eq("ppmProsesLevel", 7), Restrictions.eq("ppmProsesLevel", 8),
						Restrictions.eq("ppmProsesLevel", 9)));
			} else if (o.getPpmProsesLevel() == 13) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.PP_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", -3));
			} else if (o.getPpmProsesLevel() == 14) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.ANGGARAN_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 1));
			} else if (o.getPpmProsesLevel() == 15) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.ANGGARAN_PROSES_ID));
				crit.add(Restrictions.or(Restrictions.eq("ppmProsesLevel", 2), Restrictions.eq("ppmProsesLevel", 3),
						Restrictions.eq("ppmProsesLevel", 4), Restrictions.eq("ppmProsesLevel", 5)));
			} else if (o.getPpmProsesLevel() == 16) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.ANGGARAN_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", -1));
			} else if (o.getPpmProsesLevel() == 17) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 1));
			} else if (o.getPpmProsesLevel() == 18) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 2));
			} else if (o.getPpmProsesLevel() == 19) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 3));
			} else if (o.getPpmProsesLevel() == 20) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 4));
			} else if (o.getPpmProsesLevel() == 21) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 5));
			} else if (o.getPpmProsesLevel() == 22) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 6));
			} else if (o.getPpmProsesLevel() == 23) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
				crit.add(Restrictions.or(Restrictions.eq("ppmProsesLevel", 7), Restrictions.eq("ppmProsesLevel", 8),
						Restrictions.eq("ppmProsesLevel", 9), Restrictions.eq("ppmProsesLevel", 10),
						Restrictions.eq("ppmProsesLevel", 11), Restrictions.eq("ppmProsesLevel", 12),
						Restrictions.eq("ppmProsesLevel", 13), Restrictions.eq("ppmProsesLevel", 14),
						Restrictions.eq("ppmProsesLevel", 15), Restrictions.eq("ppmProsesLevel", 16)));
			} else if (o.getPpmProsesLevel() == 24) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 17));
			} else if (o.getPpmProsesLevel() == 25) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.VENDOR_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 1));
			} else if (o.getPpmProsesLevel() == 26) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.VENDOR_PROSES_ID));
				crit.add(Restrictions.or(Restrictions.eq("ppmProsesLevel", 2), Restrictions.eq("ppmProsesLevel", 3),
						Restrictions.eq("ppmProsesLevel", 4)));
			} else if (o.getPpmProsesLevel() == 27) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.PQ_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 1));
			} else if (o.getPpmProsesLevel() == 28) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.PQ_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 2));
			} else if (o.getPpmProsesLevel() == 29) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.PQ_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 3));
			} else if (o.getPpmProsesLevel() == 30) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.KONTRAK_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 1));
			} else if (o.getPpmProsesLevel() == 31) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.KONTRAK_PROSES_ID));
				crit.add(Restrictions.or(Restrictions.eq("ppmProsesLevel", 2), Restrictions.eq("ppmProsesLevel", 3),
						Restrictions.eq("ppmProsesLevel", 4), Restrictions.eq("ppmProsesLevel", 5),
						Restrictions.eq("ppmProsesLevel", 6), Restrictions.eq("ppmProsesLevel", 7)));
			} else if (o.getPpmProsesLevel() == 32) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.KONTRAK_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 8));
			} else if (o.getPpmProsesLevel() == 33) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.PANITIA_PP_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 1));
			} else if (o.getPpmProsesLevel() == 34) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.REVISI_EE));
				crit.add(Restrictions.eq("ppmProsesLevel", 1));

			} else if (o.getPpmProsesLevel() == 35) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.REVISI_EE));
				crit.add(Restrictions.eq("ppmProsesLevel", 2));

			} else if (o.getPpmProsesLevel() == 36) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.REALOKASI));
				crit.add(Restrictions.eq("ppmProsesLevel", 1));
			} else if (o.getPpmProsesLevel() == -100) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("ppmProsesLevel", -100));
			} else {
				// crit.add(Restrictions.isNull("admUserByPpmCurrentUser"));
				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("ppmProsesLevel", o.getPpmProsesLevel()));
			}
		}
		crit.add(Restrictions.sqlRestriction("to_char({alias}." + "CREATED_DATE, 'yyyy') = ?", year,
				StandardBasicTypes.INTEGER));

		crit.addOrder(Order.desc("updatedDate"));
		List<PrcMainHeader> retMe = new ArrayList<PrcMainHeader>();
		retMe = ((List<PrcMainHeader>) getListByDetachedCiteria(crit));
		return retMe;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> searchPrcMainHeaderRfq(PrcMainHeader o, Integer year) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMainHeader.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("admDept", FetchMode.JOIN);
		crit.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);

		if (o.getDistrictUser() != null) {
			crit.add(Restrictions.eq("districtUser", o.getDistrictUser()));
		}
		if (o != null && o.getPpmProsesLevel() != null) {
			if (o.getPpmProsesLevel() == 0) {
				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					crit.createAlias("admUserByPpmCurrentUser", "us", JoinType.LEFT_OUTER_JOIN);
					crit.add(Restrictions.or(
							Restrictions.ilike("us.completeName", o.getPpmNomorPr().toLowerCase(), MatchMode.ANYWHERE),
							Restrictions.or(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE),
									Restrictions.like("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE))));
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
				crit.add(Restrictions.ne("ppmProsesLevel", 99));
				crit.add(Restrictions.ne("ppmProsesLevel", -99));
				crit.add(Restrictions.ne("ppmProsesLevel", -98));
				crit.add(Restrictions.ne("ppmProsesLevel", -100));

			} else if (o.getPpmProsesLevel() == -1) {
				// all
				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
					crit.add(dj);
				}
			} else if (o.getPpmProsesLevel() == 1) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 1));
			} else if (o.getPpmProsesLevel() == 2) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 2));
			} else if (o.getPpmProsesLevel() == 3) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 3));
			} else if (o.getPpmProsesLevel() == 4) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 4));
			} else if (o.getPpmProsesLevel() == 5) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 5));
			} else if (o.getPpmProsesLevel() == 6) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 6));
			} else if (o.getPpmProsesLevel() == 7) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
				crit.add(Restrictions.or(Restrictions.eq("ppmProsesLevel", 7), Restrictions.eq("ppmProsesLevel", 8),
						Restrictions.eq("ppmProsesLevel", 9), Restrictions.eq("ppmProsesLevel", 10)));
			} else if (o.getPpmProsesLevel() == 8) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", 17));
			} else if (o.getPpmProsesLevel() == -100) {

				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("ppmProsesLevel", -100));
			} else {
				// crit.add(Restrictions.isNull("admUserByPpmCurrentUser"));
				if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
					Disjunction dj = Restrictions.disjunction();
					dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
					crit.add(dj);
				}
				crit.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
				crit.add(Restrictions.eq("ppmProsesLevel", o.getPpmProsesLevel()));
				crit.add(Restrictions.eq("admProses", SessionGetter.RFQ_PROSES_ID));
				crit.add(Restrictions.ne("ppmProsesLevel", 99));
				crit.add(Restrictions.ne("ppmProsesLevel", -99));
				crit.add(Restrictions.ne("ppmProsesLevel", -98));
				crit.add(Restrictions.ne("ppmProsesLevel", -100));

			}
		}
		crit.add(Restrictions.eq("isWatched", 1));
		crit.addOrder(Order.desc("updatedDate"));
		crit.add(Restrictions.sqlRestriction("to_char({alias}." + "CREATED_DATE, 'yyyy') = ?", year,
				StandardBasicTypes.INTEGER));

		crit.addOrder(Order.desc("updatedDate"));
		List<PrcMainHeader> retMe = new ArrayList<PrcMainHeader>();
		retMe = ((List<PrcMainHeader>) getListByDetachedCiteria(crit));
		return retMe;
	}

	@Override
	public List<Integer> getListOfYears() {
		// TODO Auto-generated method stub
		int yearStart = 2013;
		Calendar today = Calendar.getInstance();
		List<Integer> listOfYears = new ArrayList<Integer>();
		int yearNow = today.get(Calendar.YEAR);
		for (int i = yearStart; i <= yearNow; i++) {
			listOfYears.add(i);
		}
		return listOfYears;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> searchPrcMainHeaderManual(PrcMainHeader o) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcMainHeader.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("admDept", FetchMode.JOIN);
		crit.setFetchMode("admUserByPpmCurrentUser", FetchMode.JOIN);

		if (o.getDistrictUser() != null) {
			crit.add(Restrictions.eq("districtUser", o.getDistrictUser()));
		}

		if (o.getPpmNomorPr() != null && o.getPpmNomorPr().trim().length() > 0) {
			Disjunction dj = Restrictions.disjunction();
			dj.add(Restrictions.ilike("ppmSubject", o.getPpmNomorPr(), MatchMode.ANYWHERE));
			dj.add(Restrictions.ilike("ppmNomorPr", o.getPpmNomorPr(), MatchMode.ANYWHERE));
			crit.add(dj);
		}
		crit.add(Restrictions.ne("ppmProsesLevel", 99));
		crit.add(Restrictions.ne("ppmProsesLevel", -99));
		crit.add(Restrictions.ne("ppmProsesLevel", -98));
		crit.addOrder(Order.desc("updatedDate"));
		List<PrcMainHeader> retMe = new ArrayList<PrcMainHeader>();
		retMe = ((List<PrcMainHeader>) getListByDetachedCiteria(crit));
		return retMe;
	}

}
