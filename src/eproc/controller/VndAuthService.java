package eproc.controller;

import com.orm.model.TmpVndHeader;

public interface VndAuthService {
	/**
	 * Login Vendor
	 * @param vndHeader
	 * @return
	 */
	public TmpVndHeader doLogin(TmpVndHeader vndHeader);

	public TmpVndHeader getVndHeaderByMail(String vendorEmail);

	public TmpVndHeader getVndHeaderByLogin(String vendorLogin);
	
	public TmpVndHeader getVndHeaderByNpwp(String vendorLogin);

	public boolean saveOrUpdateTmpVndHeader(TmpVndHeader vndHeader); 
	
	public TmpVndHeader getVndHeaderByMailAndLogin(String email, String login);
	
	public TmpVndHeader getTmpVndHeaderById(long l);
	
	public boolean setVendorActivation(String login, String email);

	public void resetPassword(TmpVndHeader vndHeader) throws Exception;

	public void aktivasiManual(TmpVndHeader vndHeader) throws Exception;

	boolean isExistNpwp(String npwp) throws Exception;
}
