package eproc.controller;

import java.util.List;

import com.eproc.x.verification.command.BidderDetailModel;
import com.eproc.x.verification.command.BidderListModel;
import com.eproc.x.verification.command.VendorExpiredModel;
import com.orm.model.TmpVndAddress;
import com.orm.model.TmpVndDoc;
import com.orm.model.TmpVndFinRpt;
import com.orm.model.TmpVndHeader;
import com.orm.model.UsulanVnd;
import com.orm.model.UsulanVndDetail;
import com.orm.model.UsulanVndHistory;
import com.orm.model.VndAddress;
import com.orm.model.VndAgent;
import com.orm.model.VndAkta;
import com.orm.model.VndBank;
import com.orm.model.VndBoard;
import com.orm.model.VndCert;
import com.orm.model.VndCompanyType;
import com.orm.model.VndCv;
import com.orm.model.VndDistrict;
import com.orm.model.VndDoc;
import com.orm.model.VndDomisili;
import com.orm.model.VndEquip;
import com.orm.model.VndFinRpt;
import com.orm.model.VndHeader;
import com.orm.model.VndIjin;
import com.orm.model.VndProduct;
import com.orm.model.VndSdm;
import com.orm.model.VndSuppdoc;
import com.orm.model.VndTambahan;

public interface VendorService {
	//tmp
	public TmpVndHeader getTmpVndHeaderById(long id);
	public List<TmpVndHeader> listTmpVndHeaderByStatus(String status);
	public List<TmpVndDoc> listTmpVndDocByTmpVndHeader(long vendorId);
	public boolean saveOrUpdateTmpVndDoc(TmpVndDoc tmpVndDoc);
	
	public TmpVndHeader copyTmpToVnd(TmpVndHeader tmpVndHeader);
	
	//Vendor
	public List<VndAddress> listVndAddress(VndHeader vndHeader);
	public List<VndAgent> listVndAgent(VndHeader vndHeader);
	public List<VndAkta> listVndAkta(VndHeader vndHeader);
	public List<VndBank> listVndBank(VndHeader vndHeader);
	public List<VndBoard> listVndBard(VndHeader vndHeader);
	public List<VndCert> listVndCerts(VndHeader vndHader);
	public List<VndCompanyType> listVndCompanyType(VndHeader vndHeader);
	public List<VndCv> listVndCv(VndHeader vndHader);
	public List<VndDistrict> listVndDistric(VndHeader vndHeader);
	public List<VndDoc> listVndDoc(VndHeader vndHeader);
	public List<VndDomisili> listVndDomisili(VndHeader vndHeader);
	public List<VndEquip> listVndEquip(VndHeader vndHeader);
	public List<VndFinRpt> listVndFinRpt(VndHeader vndHeader);	
	public List<VndProduct> listVndProduct(VndHeader vndHeader);
	public List<VndSdm> listVndSdm(VndHeader vndHeader);
	public List<VndSuppdoc> listVndSuppDoc(VndHeader vndHeader);
	public List<VndTambahan> listVndTambahan(VndHeader vndHeader) throws Exception;
	public List<VndIjin> listVndIjin(VndHeader vndHeader) throws Exception;	
	
	public VndHeader getVndHeaderById(long id);
	public List<TmpVndHeader> listTmpVndHeaderByStatusVerifikasi(String string) throws Exception;
	public List<UsulanVnd> getListUsulan(Object object, Integer stat) throws Exception;
	public UsulanVnd getUsulanById(UsulanVnd usulan) throws Exception;
	public List<UsulanVndDetail> getListUsulanDetailByUsulan(UsulanVnd usulan) throws Exception;
	public List<UsulanVndHistory> getListUsulanHistoryByUsulan(UsulanVnd usulan) throws Exception;
	public void deleteUsulan(UsulanVnd usulan) throws Exception;
	public void saveUsulan(UsulanVnd usulan,
			List<UsulanVndDetail> listUsulanDetail,
			UsulanVndHistory usulanHistory) throws Exception;
	public void saveApprovalUsulan(UsulanVnd usulan,
			List<UsulanVndDetail> listUsulanDetail,
			UsulanVndHistory usulanHistory, String aksi) throws Exception;
	public void saveFinalisasiUsulan(UsulanVnd usulan,
			List<UsulanVndDetail> listUsulanDetail,
			UsulanVndHistory usulanHistory, String setuju) throws Exception;
	public void aktivasiDanPenomoran(TmpVndHeader tmp) throws Exception;
	public List<BidderListModel> listBidder(String query) throws Exception;
	public List<TmpVndHeader> listTmpVndHeaderByStatusCari(String vendorStatus) throws Exception;
	public List<BidderDetailModel> listDetailBidder(String bidderDetail) throws Exception;
	public List<VendorExpiredModel> getExpiredVendor(String expiredDokumen) throws Exception;
	public void setStatusVendor(String statusVendor) throws Exception;
	public List<VndBoard> listVndBardKomisaris(VndHeader vndHeader) throws Exception;
	TmpVndAddress getTmpVndAddByTmpVndHeader(TmpVndHeader vndHeader);
	TmpVndFinRpt getTmpVndFinRptByHeader(TmpVndHeader vndHeader);
	UsulanVndDetail getUsulanDetailById(UsulanVndDetail usulanVndDetail) throws Exception;
	void saveVendorIdSap(VndHeader vndHeader, Long vendorNumber);
	List<UsulanVnd> getListUsulanByYear(Object object, Integer status, Integer year) throws Exception;
	void backToVerifikator(UsulanVnd usulanVnd, List<UsulanVndDetail> listUsulanDetail,UsulanVndHistory usulanHistory);

	/*
	public boolean saveOrUpdateVndAddress(VndAddress vndAddress);
	public boolean saveOrUpdateVndAgent(VndAgent vndAgent);
	public boolean saveOrUpdateVndAkta(VndAkta vndAkta);
	public boolean saveOrUpdateVndBank(VndBank vndBank);
	public boolean saveOrUpdateVndBoard(VndBoard vndBoard);
	public boolean saveOrUpdateVndCert(VndCert vndCert);
	public boolean saveOrUpdateVndCompanyType(VndCompanyType vndCompanyType);
	public boolean saveOrUpdateVndCv(VndCv vndCv);
	public boolean saveOrUpdateVndDistrict(VndDistrict vndDistrict);
	public boolean saveOrUpdateVndDoc(VndDoc vndDoc);*/
	
	
	
}
