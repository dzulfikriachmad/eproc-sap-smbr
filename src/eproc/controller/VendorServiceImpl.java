package eproc.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StandardBasicTypes;

import com.eproc.interceptor.OrmInterceptor;
import com.eproc.sap.controller.IntegrationService;
import com.eproc.sap.controller.IntegrationServiceImpl;
import com.eproc.sap.dto.ResultDto;
import com.eproc.utils.EmailSessionBean;
import com.eproc.utils.SessionGetter;
import com.eproc.x.verification.command.BidderDetailModel;
import com.eproc.x.verification.command.BidderListModel;
import com.eproc.x.verification.command.VendorExpiredModel;
import com.orm.model.AdmCompanyType;
import com.orm.model.AdmDistrict;
import com.orm.model.AdmProsesHirarki;
import com.orm.model.AdmUser;
import com.orm.model.TmpVndAddress;
import com.orm.model.TmpVndAgent;
import com.orm.model.TmpVndAkta;
import com.orm.model.TmpVndBank;
import com.orm.model.TmpVndBoard;
import com.orm.model.TmpVndCert;
import com.orm.model.TmpVndCompanyType;
import com.orm.model.TmpVndCv;
import com.orm.model.TmpVndDistrict;
import com.orm.model.TmpVndDoc;
import com.orm.model.TmpVndDomisili;
import com.orm.model.TmpVndEquip;
import com.orm.model.TmpVndFinRpt;
import com.orm.model.TmpVndHeader;
import com.orm.model.TmpVndProduct;
import com.orm.model.TmpVndSdm;
import com.orm.model.TmpVndSuppdoc;
import com.orm.model.TmpVndTambahan;
import com.orm.model.UsulanVnd;
import com.orm.model.UsulanVndDetail;
import com.orm.model.UsulanVndHistory;
import com.orm.model.VndAddress;
import com.orm.model.VndAgent;
import com.orm.model.VndAkta;
import com.orm.model.VndBank;
import com.orm.model.VndBoard;
import com.orm.model.VndCert;
import com.orm.model.VndCompanyType;
import com.orm.model.VndCompanyTypeId;
import com.orm.model.VndCv;
import com.orm.model.VndDistrict;
import com.orm.model.VndDistrictId;
import com.orm.model.VndDoc;
import com.orm.model.VndDomisili;
import com.orm.model.VndEquip;
import com.orm.model.VndFinRpt;
import com.orm.model.VndHeader;
import com.orm.model.VndIjin;
import com.orm.model.VndProduct;
import com.orm.model.VndSdm;
import com.orm.model.VndSuppdoc;
import com.orm.model.VndTambahan;
import com.orm.tools.GenericDao;

public class VendorServiceImpl extends GenericDao implements VendorService {
	

	@Override
	public TmpVndHeader getTmpVndHeaderById(long id) {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.add(Restrictions.idEq(id));
		dc.setFetchMode("admCurrencyByVendorCurrInCap", FetchMode.JOIN);
		dc.setFetchMode("admCurrencyByVendorCurrBaseCap", FetchMode.JOIN);
		return (TmpVndHeader) getEntityByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TmpVndHeader> listTmpVndHeaderByStatus(String status) {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		
		if( status != null && !status.equals("")){
			dc.add(Restrictions.eq("vendorStatus", status));
		}
		
		return (List<TmpVndHeader>) getListByDetachedCiteria(dc);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TmpVndHeader> listTmpVndHeaderByStatusCari(String status) {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		
		if( status != null && !status.equals("")){
			if(status.equals("EE")){//menungu verifikasi
				dc.add(Restrictions.or(Restrictions.eq("vendorStatus", status), Restrictions.and(Restrictions.eq("vendorStatus", "P"), Restrictions.eq("vendorNextPage", 10))));
			}else if(status.equals("P")){//sedang di proses belum selesai
				dc.add(Restrictions.eq("vendorStatus", status));
				dc.add(Restrictions.lt("vendorNextPage", 10));
			}else if(status.equals("0")){
				//all do nothing
			}else
				dc.add(Restrictions.eq("vendorStatus", status));
		}
		
		return (List<TmpVndHeader>) getListByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TmpVndDoc> listTmpVndDocByTmpVndHeader(long vendorId) {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndDoc.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.add(Restrictions.eq("tmpVndHeader.vendorId", vendorId));
		return (List<TmpVndDoc>) getListByDetachedCiteria(dc);
	}

	@Override
	public boolean saveOrUpdateTmpVndDoc(TmpVndDoc tmpVndDoc) {
		boolean ret = false;
		try {
			startTransaction(new OrmInterceptor());
			ret = saveEntity(tmpVndDoc);
			commitAndClear();
		} catch (Exception e) {
			e.printStackTrace();
			RollbackAndClear();
			return ret;
		}
		return ret;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<VndAddress> listVndAddress(VndHeader vndHeader) {
		DetachedCriteria crit = DetachedCriteria.forClass(VndAddress.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("admAddType", FetchMode.JOIN);
		crit.add(Restrictions.eq("vndHeader", vndHeader));
		return (List<VndAddress>) getListByDetachedCiteria(crit);
	}
	
	

	@SuppressWarnings("unchecked")
	@Override
	public List<VndAgent> listVndAgent(VndHeader vndHeader) {
		DetachedCriteria crit = DetachedCriteria.forClass(VndAgent.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("vndHeader", vndHeader));
		return (List<VndAgent>) getListByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<VndAkta> listVndAkta(VndHeader vndHeader) {
		DetachedCriteria crit = DetachedCriteria.forClass(VndAkta.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("vndHeader", vndHeader));
		return (List<VndAkta>) getListByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<VndBank> listVndBank(VndHeader vndHeader) {
		DetachedCriteria crit = DetachedCriteria.forClass(VndBank.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("vndHeader", vndHeader));
		return (List<VndBank>) getListByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<VndBoard> listVndBard(VndHeader vndHeader) {
		DetachedCriteria crit = DetachedCriteria.forClass(VndBoard.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("vndHeader", vndHeader));
		crit.add(Restrictions.eq("boardVipe", 1));
		return (List<VndBoard>) getListByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<VndCert> listVndCerts(VndHeader vndHader) {
		DetachedCriteria crit = DetachedCriteria.forClass(VndCert.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("vndHeader", vndHader));
		return (List<VndCert>) getListByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<VndCompanyType> listVndCompanyType(VndHeader vndHeader) {
		DetachedCriteria crit = DetachedCriteria.forClass(VndCompanyType.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("admCompanyType", FetchMode.JOIN);
		crit.add(Restrictions.eq("vndHeader", vndHeader));
		return (List<VndCompanyType>) getListByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<VndCv> listVndCv(VndHeader vndHader) {
		DetachedCriteria crit = DetachedCriteria.forClass(VndCv.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("vndHeader", vndHader));
		return (List<VndCv>) getListByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<VndDistrict> listVndDistric(VndHeader vndHeader) {
		DetachedCriteria crit = DetachedCriteria.forClass(VndDistrict.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("admDistrict", FetchMode.JOIN);
		crit.add(Restrictions.eq("vndHeader", vndHeader));
		return (List<VndDistrict>) getListByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<VndDoc> listVndDoc(VndHeader vndHeader) {
		DetachedCriteria crit = DetachedCriteria.forClass(VndDoc.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("vndHeader", vndHeader));
		return (List<VndDoc>) getListByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<VndDomisili> listVndDomisili(VndHeader vndHeader) {
		DetachedCriteria crit = DetachedCriteria.forClass(VndDomisili.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("vndHeader", vndHeader));
		return (List<VndDomisili>) getListByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<VndEquip> listVndEquip(VndHeader vndHeader) {
		DetachedCriteria crit = DetachedCriteria.forClass(VndEquip.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("vndHeader", vndHeader));
		return (List<VndEquip>) getListByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<VndFinRpt> listVndFinRpt(VndHeader vndHeader) {
		DetachedCriteria crit = DetachedCriteria.forClass(VndFinRpt.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("admCurrency", FetchMode.JOIN);
		crit.add(Restrictions.eq("vndHeader", vndHeader));
		return (List<VndFinRpt>) getListByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<VndProduct> listVndProduct(VndHeader vndHeader) {
		DetachedCriteria crit = DetachedCriteria.forClass(VndProduct.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("comGroup", FetchMode.JOIN);
		crit.add(Restrictions.eq("vndHeader", vndHeader));
		return (List<VndProduct>) getListByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<VndSdm> listVndSdm(VndHeader vndHeader) {
		DetachedCriteria crit = DetachedCriteria.forClass(VndSdm.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("vndHeader", vndHeader));
		return (List<VndSdm>) getListByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<VndSuppdoc> listVndSuppDoc(VndHeader vndHeader) {
		DetachedCriteria crit = DetachedCriteria.forClass(VndSuppdoc.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("vndHeader", vndHeader));
		return (List<VndSuppdoc>) getListByDetachedCiteria(crit);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<VndBoard> listVndBardKomisaris(VndHeader vndHeader) {
		DetachedCriteria crit = DetachedCriteria.forClass(VndBoard.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("vndHeader", vndHeader));
		crit.add(Restrictions.eq("boardVipe", 2));
		return (List<VndBoard>) getListByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<VndTambahan> listVndTambahan(VndHeader vndHeader)
			throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(VndTambahan.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("vendorId", vndHeader));
		return (List<VndTambahan>) getListByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<VndIjin> listVndIjin(VndHeader vndHeader) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(VndIjin.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("vndHeader", vndHeader));
		return (List<VndIjin>) getListByDetachedCiteria(crit);
	}

	@Override
	public VndHeader getVndHeaderById(long id) {
		DetachedCriteria dc = DetachedCriteria.forClass(VndHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admCurrencyByVendorCurrInCap", FetchMode.JOIN);
		dc.setFetchMode("admCurrencyByVendorCurrBaseCap", FetchMode.JOIN);
		dc.add(Restrictions.eq("vendorId", id));
		
		return (VndHeader) getEntityByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public TmpVndHeader copyTmpToVnd(TmpVndHeader tmpVndHeader) {
		boolean ret = false;
		TmpVndHeader retMe = tmpVndHeader;
		try {
						
			//copy VndHeader
			if(tmpVndHeader != null){
				VndHeader vh = new VndHeader(tmpVndHeader);
				saveEntity(vh);
			}
			
			VndHeader vndHeader = (VndHeader)session.get(VndHeader.class, tmpVndHeader.getVendorId());
			
			//Copy vndaddress
			DetachedCriteria dcAddress = DetachedCriteria.forClass(TmpVndAddress.class);
			dcAddress.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			dcAddress.add(Restrictions.eq("tmpVndHeader.vendorId", retMe.getVendorId()));
			List<TmpVndAddress> listTmpVndAddress = (List<TmpVndAddress>) getListByDetachedCriteria(dcAddress, session);
			
			DetachedCriteria dcVndAddress = DetachedCriteria.forClass(VndAddress.class);
			dcVndAddress.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			dcVndAddress.add(Restrictions.eq("vndHeader.vendorId", retMe.getVendorId()));
			List<VndAddress> listVndAddress = (List<VndAddress>) getListByDetachedCriteria(dcVndAddress, session);
			
			if(listTmpVndAddress != null){
				if(listVndAddress.size() != 0){
					for(VndAddress vad:listVndAddress){
						deleteEntity(vad);
					}
				}
				
				for(TmpVndAddress tad:listTmpVndAddress){
					VndAddress vndAddress = new VndAddress(tad);
					vndAddress.setVndHeader(vndHeader);
					saveEntity(vndAddress);
				}
			}
			
			//Copy Vnd Agent
			DetachedCriteria dcTmpVndAgent = DetachedCriteria.forClass(TmpVndAgent.class);
			dcTmpVndAgent.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			dcTmpVndAgent.add(Restrictions.eq("tmpVndHeader.vendorId", retMe.getVendorId()));
			List<TmpVndAgent> listTmpVndAgent = (List<TmpVndAgent>) getListByDetachedCriteria(dcTmpVndAgent, session);
			
			DetachedCriteria dcVndAgent = DetachedCriteria.forClass(VndAgent.class);
			dcVndAgent.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			dcVndAgent.add(Restrictions.eq("vndHeader.vendorId", retMe.getVendorId()));
			List<VndAgent> listVndAgent = (List<VndAgent>) getListByDetachedCriteria(dcVndAgent, session);
			
			if(listTmpVndAgent != null){
				if(listVndAgent.size() != 0){
					for(VndAgent vag:listVndAgent){
						deleteEntity(vag);
					}
				}
				
				for(TmpVndAgent tag:listTmpVndAgent){
					VndAgent agent = new VndAgent(tag);
					agent.setVndHeader(vndHeader);
					saveEntity(agent);
				}
			}
			
			//Copy VNdAkta
			DetachedCriteria dcTmpVndAkta = DetachedCriteria.forClass(TmpVndAkta.class);
			dcTmpVndAkta.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			dcTmpVndAkta.add(Restrictions.eq("tmpVndHeader.vendorId", retMe.getVendorId()));
			List<TmpVndAkta> listTmpVndAkta = (List<TmpVndAkta>) getListByDetachedCriteria(dcTmpVndAkta, session);
			
			DetachedCriteria dcVndAkta = DetachedCriteria.forClass(VndAkta.class);
			dcVndAkta.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			dcVndAkta.add(Restrictions.eq("vndHeader.vendorId", retMe.getVendorId()));
			List<VndAkta> listVndAkta = (List<VndAkta>) getListByDetachedCriteria(dcVndAkta, session);
			
			if(listTmpVndAkta != null){
				if(listVndAkta.size() != 0){
					for(VndAkta vak:listVndAkta){
						deleteEntity(vak);
					}
				}
				
				for(TmpVndAkta tag:listTmpVndAkta){
					VndAkta akta = new VndAkta(tag);
					akta.setVndHeader(vndHeader);
					saveEntity(akta);
				}
			}
			
			///Copy VndBank
			DetachedCriteria dcTmpVndBank = DetachedCriteria.forClass(TmpVndBank.class);
			dcTmpVndBank.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			dcTmpVndBank.add(Restrictions.eq("tmpVndHeader.vendorId", retMe.getVendorId()));
			List<TmpVndBank> listTmpVndBank = (List<TmpVndBank>) getListByDetachedCriteria(dcTmpVndBank, session);
			
			DetachedCriteria dcVndBank = DetachedCriteria.forClass(VndBank.class);
			dcVndBank.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			dcVndBank.add(Restrictions.eq("vndHeader.vendorId", retMe.getVendorId()));
			List<VndBank> listVndBank = (List<VndBank>) getListByDetachedCriteria(dcVndBank, session);
			
			if(listTmpVndBank != null){
				if(listVndBank.size() != 0){
					for(VndBank vba:listVndBank){
						deleteEntity(vba);
					}
				}
				
				for(TmpVndBank tba:listTmpVndBank){
					VndBank bank = new VndBank(tba);
					bank.setVndHeader(vndHeader);
					saveEntity(bank);
				}
			}
			
			//Copy Vnd Board
			DetachedCriteria dcTmpVndBoard = DetachedCriteria.forClass(TmpVndBoard.class);
			dcTmpVndBoard.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			dcTmpVndBoard.add(Restrictions.eq("tmpVndHeader.vendorId", retMe.getVendorId()));
			List<TmpVndBoard> listTmpVndBoard = (List<TmpVndBoard>) getListByDetachedCriteria(dcTmpVndBoard, session);
			
			DetachedCriteria dcVndBoard = DetachedCriteria.forClass(VndBoard.class);
			dcVndBoard.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			dcVndBoard.add(Restrictions.eq("vndHeader.vendorId", retMe.getVendorId()));
			List<VndBoard> listVndBoard = (List<VndBoard>) getListByDetachedCriteria(dcVndBoard, session);
			
			if(listTmpVndBoard != null){
				if(listVndBoard.size() != 0){
					for(VndBoard vbo:listVndBoard){
						deleteEntity(vbo);
					}
				}
				
				for(TmpVndBoard tbo:listTmpVndBoard){
					VndBoard board = new VndBoard(tbo);
					board.setVndHeader(vndHeader);
					saveEntity(board);
				}
			}
			
			//Copy VndCert
			DetachedCriteria dcTmpVndCert = DetachedCriteria.forClass(TmpVndCert.class);
			dcTmpVndCert.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			dcTmpVndCert.add(Restrictions.eq("tmpVndHeader.vendorId", retMe.getVendorId()));
			List<TmpVndCert> listTmpVndCert = (List<TmpVndCert>) getListByDetachedCriteria(dcTmpVndCert, session);
			
			DetachedCriteria dcVndCert = DetachedCriteria.forClass(VndCert.class);
			dcVndCert.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			dcVndCert.add(Restrictions.eq("vndHeader.vendorId", retMe.getVendorId()));
			List<VndCert> listVndCert = (List<VndCert>) getListByDetachedCriteria(dcVndCert, session);
			
			if(listTmpVndCert != null){
				if(listVndCert.size() != 0){
					for(VndCert vce:listVndCert){
						deleteEntity(vce);
					}
				}
				
				for(TmpVndCert tce:listTmpVndCert){
					VndCert cert = new VndCert(tce);
					cert.setVndHeader(vndHeader);
					saveEntity(cert);
				}
			}			
			
			//Copy companytype
			DetachedCriteria dcTmpVndCom = DetachedCriteria.forClass(TmpVndCompanyType.class);
			dcTmpVndCom.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			dcTmpVndCom.add(Restrictions.eq("tmpVndHeader.vendorId", retMe.getVendorId()));
			List<TmpVndCompanyType> listTmpVndCt = (List<TmpVndCompanyType>) getListByDetachedCriteria(dcTmpVndCom, session);
			
			DetachedCriteria dcVndCt = DetachedCriteria.forClass(VndCompanyType.class);
			dcVndCt.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			dcVndCt.add(Restrictions.eq("vndHeader.vendorId", retMe.getVendorId()));
			List<VndCompanyType> listVndCt = (List<VndCompanyType>) getListByDetachedCriteria(dcVndCt, session);
			
			if(listTmpVndCt != null){
				if(listVndCt.size() != 0){
					for(VndCompanyType vct:listVndCt){
						deleteEntity(vct);
					}
				}
				
				for(TmpVndCompanyType tct:listTmpVndCt){
					AdmCompanyType type = new AdmCompanyType(tct.getAdmCompanyType().getId());
					
					VndCompanyTypeId id = new VndCompanyTypeId();
					id.setCompanyVype(type.getId());
					id.setVendorId(vndHeader.getVendorId());
					
					VndCompanyType ct = new VndCompanyType();
					ct.setId(id);
					ct.setVndHeader(vndHeader);
					saveEntity(ct);
				}
			}
			
			//Copy VndCv
			DetachedCriteria dcTmpVndCv = DetachedCriteria.forClass(TmpVndCv.class);
			dcTmpVndCv.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			dcTmpVndCv.add(Restrictions.eq("tmpVndHeader.vendorId", retMe.getVendorId()));
			List<TmpVndCv> listTmpVndCv = (List<TmpVndCv>) getListByDetachedCriteria(dcTmpVndCv, session);
			
			DetachedCriteria dcVndCv = DetachedCriteria.forClass(VndCv.class);
			dcVndCv.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			dcVndCv.add(Restrictions.eq("vndHeader.vendorId", retMe.getVendorId()));
			List<VndCv> listVndCv = (List<VndCv>) getListByDetachedCriteria(dcVndCv, session);
			
			if(listTmpVndCv != null){
				if(listVndCv.size() != 0){
					for(VndCv vcv:listVndCv){
						deleteEntity(vcv);
					}
				}
				
				for(TmpVndCv tcv:listTmpVndCv){
					VndCv cv  = new VndCv(tcv);
					cv.setVndHeader(vndHeader);
					saveEntity(cv);
				}
			}
			
			//Copy Vnd District
			DetachedCriteria dcTmpVndDis = DetachedCriteria.forClass(TmpVndDistrict.class);
			dcTmpVndDis.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			dcTmpVndDis.add(Restrictions.eq("tmpVndHeader.vendorId", retMe.getVendorId()));
			List<TmpVndDistrict> listTmpVndDis = (List<TmpVndDistrict>) getListByDetachedCriteria(dcTmpVndDis, session);
			
			DetachedCriteria dcVndDis = DetachedCriteria.forClass(VndDistrict.class);
			dcVndDis.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			dcVndDis.add(Restrictions.eq("vndHeader.vendorId", retMe.getVendorId()));
			List<VndDistrict> listVndDis= (List<VndDistrict>) getListByDetachedCriteria(dcVndDis, session);
			
			if(listTmpVndDis != null){
				if(listVndDis.size() != 0){
					for(VndDistrict vdi:listVndDis){
						deleteEntity(vdi);
					}
				}
				
				for(TmpVndDistrict tdi:listTmpVndDis){
					AdmDistrict district = new AdmDistrict(tdi.getAdmDistrict().getId());
					
					VndDistrictId id = new VndDistrictId();
					id.setDistrictId(district.getId());
					id.setVendorId(vndHeader.getVendorId());
					
					VndDistrict di = new VndDistrict();
					di.setId(id);
					di.setVndHeader(vndHeader);
					di.setAdmDistrict(district);
					saveEntity(di);
				}
			}
			
			//Copy VndDoc
			DetachedCriteria dcTmpVndDoc = DetachedCriteria.forClass(TmpVndDoc.class);
			dcTmpVndDoc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			dcTmpVndDoc.add(Restrictions.eq("tmpVndHeader.vendorId", retMe.getVendorId()));
			List<TmpVndDoc> listTmpVndDoc = (List<TmpVndDoc>) getListByDetachedCriteria(dcTmpVndDoc, session);
			
			DetachedCriteria dcVndDoc = DetachedCriteria.forClass(VndDoc.class);
			dcVndDoc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			dcVndDoc.add(Restrictions.eq("vndHeader.vendorId", retMe.getVendorId()));
			List<VndDoc> listVndDoc = (List<VndDoc>) getListByDetachedCriteria(dcVndDoc, session);
			
			if(listTmpVndDoc != null){
				if(listVndDoc.size() != 0){
					for(VndDoc vdo:listVndDoc){
						deleteEntity(vdo);
					}
				}
				
				for(TmpVndDoc tdo:listTmpVndDoc){
					VndDoc doc  = new VndDoc(tdo);
					doc.setVndHeader(vndHeader);
					saveEntity(doc);
				}
			}
			
			//Copy Vnd Domisili
			DetachedCriteria dcTmpVndDom = DetachedCriteria.forClass(TmpVndDomisili.class);
			dcTmpVndDom.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			dcTmpVndDom.add(Restrictions.eq("tmpVndHeader.vendorId", retMe.getVendorId()));
			List<TmpVndDomisili> listTmpVndDom = (List<TmpVndDomisili>) getListByDetachedCriteria(dcTmpVndDom, session);
			
			DetachedCriteria dcVndDom = DetachedCriteria.forClass(VndDomisili.class);
			dcVndDom.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			dcVndDom.add(Restrictions.eq("vndHeader.vendorId", retMe.getVendorId()));
			List<VndDomisili> listVndDom = (List<VndDomisili>) getListByDetachedCriteria(dcVndDom, session);
			
			if(listTmpVndDom != null){
				if(listVndDom.size() != 0){
					for(VndDomisili vdom:listVndDom){
						deleteEntity(vdom);
					}
				}
				
				for(TmpVndDomisili tdom:listTmpVndDom){
					VndDomisili dom  = new VndDomisili(tdom);
					dom.setVndHeader(vndHeader);
					saveEntity(dom);
				}
			}
			
			//Copy Vnd Equip
			DetachedCriteria dcTmpVndEquip = DetachedCriteria.forClass(TmpVndEquip.class);
			dcTmpVndEquip.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			dcTmpVndEquip.add(Restrictions.eq("tmpVndHeader.vendorId", retMe.getVendorId()));
			List<TmpVndEquip> listTmpVndEquip = (List<TmpVndEquip>) getListByDetachedCriteria(dcTmpVndEquip, session);
			
			DetachedCriteria dcVndEquip = DetachedCriteria.forClass(VndEquip.class);
			dcVndEquip.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			dcVndEquip.add(Restrictions.eq("vndHeader.vendorId", retMe.getVendorId()));
			List<VndEquip> listVndEquip = (List<VndEquip>) getListByDetachedCriteria(dcVndEquip, session);
			
			if(listTmpVndEquip != null){
				if(listVndEquip.size() != 0){
					for(VndEquip veq:listVndEquip){
						deleteEntity(veq);
					}
				}
				
				for(TmpVndEquip teq:listTmpVndEquip){
					VndEquip equip  = new VndEquip(teq);
					equip.setVndHeader(vndHeader);
					saveEntity(equip);
				}
			}
			
			//Copy VndFinRpt
			DetachedCriteria dcTmpVndFin = DetachedCriteria.forClass(TmpVndFinRpt.class);
			dcTmpVndFin.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			dcTmpVndFin.add(Restrictions.eq("tmpVndHeader.vendorId", retMe.getVendorId()));
			List<TmpVndFinRpt> listTmpVndFin = (List<TmpVndFinRpt>) getListByDetachedCriteria(dcTmpVndFin, session);
			
			DetachedCriteria dcVndFin = DetachedCriteria.forClass(VndFinRpt.class);
			dcVndFin.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			dcVndFin.add(Restrictions.eq("vndHeader.vendorId", retMe.getVendorId()));
			List<VndFinRpt> listVndFin = (List<VndFinRpt>) getListByDetachedCriteria(dcVndFin, session);
			
			if(listTmpVndFin != null){
				if(listVndFin.size() != 0){
					for(VndFinRpt vfi:listVndFin){
						deleteEntity(vfi);
					}
				}
				
				for(TmpVndFinRpt teq:listTmpVndFin){
					VndFinRpt fin  = new VndFinRpt(teq);
					fin.setVndHeader(vndHeader);
					saveEntity(fin);
				}
			}
			
			//Copy VndProduct
			DetachedCriteria dcTmpVndPro = DetachedCriteria.forClass(TmpVndProduct.class);
			dcTmpVndPro.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			dcTmpVndPro.add(Restrictions.eq("tmpVndHeader.vendorId", retMe.getVendorId()));
			List<TmpVndProduct> listTmpVndPro = (List<TmpVndProduct>) getListByDetachedCriteria(dcTmpVndPro, session);
			
			DetachedCriteria dcVndPro = DetachedCriteria.forClass(VndProduct.class);
			dcVndPro.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			dcVndPro.add(Restrictions.eq("vndHeader.vendorId", retMe.getVendorId()));
			List<VndProduct> listVndPro = (List<VndProduct>) getListByDetachedCriteria(dcVndPro, session);
			
			if(listTmpVndPro != null){
				if(listVndPro.size() != 0){
					for(VndProduct vpr:listVndPro){
						deleteEntity(vpr);
					}
				}
				
				for(TmpVndProduct tpr:listTmpVndPro){
					VndProduct pro = new VndProduct(tpr);
					pro.setVndHeader(vndHeader);
					saveEntity(pro);
				}
			}
			
			//Copy VndSdm
			DetachedCriteria dcTmpVndSdm = DetachedCriteria.forClass(TmpVndSdm.class);
			dcTmpVndSdm.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			dcTmpVndSdm.add(Restrictions.eq("tmpVndHeader.vendorId", retMe.getVendorId()));
			List<TmpVndSdm> listTmpVndSdm = (List<TmpVndSdm>) getListByDetachedCriteria(dcTmpVndSdm, session);
			
			DetachedCriteria dcVndSdm = DetachedCriteria.forClass(VndSdm.class);
			dcVndSdm.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			dcVndSdm.add(Restrictions.eq("vndHeader.vendorId", retMe.getVendorId()));
			List<VndSdm> listVndSdm = (List<VndSdm>) getListByDetachedCriteria(dcVndSdm, session);
			
			if(listTmpVndSdm != null){
				if(listVndSdm.size() != 0){
					for(VndSdm vsd:listVndSdm){
						deleteEntity(vsd);
					}
				}
				
				for(TmpVndSdm tsd:listTmpVndSdm){
					VndSdm sdm = new VndSdm(tsd);
					sdm.setVndHeader(vndHeader);
					saveEntity(sdm);
				}
			}
			
			//Copy VndSupdoc
			DetachedCriteria dcTmpVndSupDoc = DetachedCriteria.forClass(TmpVndSuppdoc.class);
			dcTmpVndSupDoc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			dcTmpVndSupDoc.add(Restrictions.eq("tmpVndHeader.vendorId", retMe.getVendorId()));
			List<TmpVndSuppdoc> listTmpVndSupDoc = (List<TmpVndSuppdoc>) getListByDetachedCriteria(dcTmpVndSupDoc, session);
			
			DetachedCriteria dcVndSupDoc = DetachedCriteria.forClass(VndSuppdoc.class);
			dcVndSupDoc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			dcVndSupDoc.add(Restrictions.eq("vndHeader.vendorId", retMe.getVendorId()));
			List<VndSuppdoc> listVndSupDoc = (List<VndSuppdoc>) getListByDetachedCriteria(dcVndSupDoc, session);
			
			if(listTmpVndSupDoc != null){
				if(listVndSupDoc.size() != 0){
					for(VndSuppdoc vsu:listVndSupDoc){
						deleteEntity(vsu);
					}
				}
				
				for(TmpVndSuppdoc tsud:listTmpVndSupDoc){
					VndSuppdoc sdoc = new VndSuppdoc(tsud);
					sdoc.setVndHeader(vndHeader);
					saveEntity(sdoc);
				}
			}
			
			//copy vndtambahan
			DetachedCriteria dcTmpVndTambahan = DetachedCriteria.forClass(TmpVndTambahan.class);
			dcTmpVndTambahan.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			dcTmpVndTambahan.add(Restrictions.eq("vendorId", retMe));
			List<TmpVndTambahan> listTmpVndTambahan = (List<TmpVndTambahan>)getListByDetachedCriteria(dcTmpVndTambahan, session);
			
			DetachedCriteria dcVndTambahan = DetachedCriteria.forClass(VndTambahan.class);
			dcVndTambahan.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			dcVndTambahan.add(Restrictions.eq("vendorId", retMe));
			List<VndTambahan> listVndTambahan = (List<VndTambahan>)getListByDetachedCriteria(dcVndTambahan, session);
			if(listTmpVndTambahan!=null){
				if(listVndTambahan.size()!=0){
					for(VndTambahan v :listVndTambahan){
						deleteEntity(v);
					}
				}
				for(TmpVndTambahan vt:listTmpVndTambahan){
					VndTambahan v = new VndTambahan(vt);
					v.setVendorId(vndHeader);
					saveEntity(v);
				}
			}
			
			ret = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(!ret){
			retMe.setVendorStatus(null);
		}
		return retMe ;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TmpVndHeader> listTmpVndHeaderByStatusVerifikasi(String status)
			throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		
		if( status != null && !status.equals("")){
			dc.add(Restrictions.eq("vendorStatus", status));
			dc.add(Restrictions.eq("vendorNextPage", 10));
		}
		
		/** jika anggota paniata drt, harus yang drt saja **/
		if(SessionGetter.isMemberPanitiaDrt(SessionGetter.getUser())){
			dc.add(Restrictions.eq("drt", 1));
		}else{
			dc.add(Restrictions.isNull("drt"));
		}
		
		/** tambah default district , disesuaikan dengan yang akan membuat**/
//		dc.add(Restrictions.eq("district", SessionGetter.getUser().getAdmEmployee().getAdmDept().getAdmDistrict()));
		
		return (List<TmpVndHeader>) getListByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UsulanVnd> getListUsulan(Object object, Integer status) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(UsulanVnd.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		if(status!=null){
//			crit.add(Restrictions.eq("tingkat", status)); //pembuatan or edit only
			if(status==3){
				crit.add(Restrictions.le("tingkat", status));
			}else if(status==1){
				crit.add(Restrictions.eq("tingkat", 1));
			}else if(status==4) {
				crit.add(Restrictions.eq("tingkat", 4));
			}
			
		}else{
			crit.add(Restrictions.ne("tingkat", 1));//persetujuan Only
		}
		if(object!=null){
			if(object instanceof AdmUser){
				crit.add(Restrictions.eq("nextUser", (AdmUser)object));
			}
		}
		return (List<UsulanVnd>) getListByDetachedCiteria(crit);
	}
	
	@Override
	public UsulanVndDetail getUsulanDetailById(UsulanVndDetail usulanVndDetail) throws Exception{
		DetachedCriteria crit = DetachedCriteria.forClass(UsulanVndDetail.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.idEq(usulanVndDetail.getId()));
		return (UsulanVndDetail) getEntityByDetachedCiteria(crit);
	}

	@Override
	public UsulanVnd getUsulanById(UsulanVnd usulan) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(UsulanVnd.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.idEq(usulan.getId()));
		return (UsulanVnd) getEntityByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UsulanVndDetail> getListUsulanDetailByUsulan(UsulanVnd usulan)
			throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(UsulanVndDetail.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("vendor", FetchMode.JOIN);
		crit.setFetchMode("usulan", FetchMode.JOIN);
		crit.add(Restrictions.eq("usulan", usulan));
		return (List<UsulanVndDetail>) getListByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UsulanVndHistory> getListUsulanHistoryByUsulan(UsulanVnd usulan)
			throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(UsulanVndHistory.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("usulan", FetchMode.JOIN);
		crit.add(Restrictions.eq("usulan", usulan));
		crit.addOrder(Order.asc("id"));
		return (List<UsulanVndHistory>) getListByDetachedCiteria(crit);
	}

	@Override
	public void deleteUsulan(UsulanVnd usulan) throws Exception {
		try {
			startTransaction();
			deleteEntity(usulan);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public void saveUsulan(UsulanVnd usulan,
			List<UsulanVndDetail> listUsulanDetail,
			UsulanVndHistory usulanHistory) throws Exception {
		try {
			startTransaction();
			if(usulan.getTingkat()==null || usulan.getTingkat()<0 || usulan.getTingkat()==1)
				usulan.setTingkat(2);
			
			if(SessionGetter.isMemberPanitiaDrt(SessionGetter.getUser())){
				usulan.setTipe(SessionGetter.DRT);
			}else{
				usulan.setTipe(SessionGetter.NONDRT);
			}
			usulan.setCreator(SessionGetter.getUser());
			usulan.setKantor(SessionGetter.getUser().getAdmEmployee().getAdmDept().getAdmDistrict());
			AdmUser nextUser = new AdmServiceImpl().getUserById(usulan.getNextUser());
			usulan.setNextUser(nextUser);
			usulan.setNextPos(nextUser.getAdmEmployee().getAdmRoleByEmpRole1());
			saveEntity(usulan);
			
			if(SessionGetter.isNotEmptyList(listUsulanDetail)){
				for(UsulanVndDetail d:listUsulanDetail){
					TmpVndHeader vnd = getTmpVndHeaderById(d.getVendor().getVendorId());
					d.setUsulan(usulan);
					saveEntity(d);
					//update Vendor Status
					if(d.getStatus()!=null && d.getStatus()==1){
						vnd.setVendorStatus("U");//lagi diusulkan
					}else{
						vnd.setVendorStatus("P");//Harus diverifikasi lagi
						deleteEntity(d);
					}
					
					saveEntity(vnd);
					
				}
			}
			//insert history
			usulanHistory.setUsulan(usulan);
			SessionGetter.saveHistoryUsulanVendorNew(usulanHistory,session,"Persetujuan Usulan");
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
		
	}
	
	@Override 
	public void backToVerifikator(UsulanVnd usulanVnd, List<UsulanVndDetail> listUsulanDetail,UsulanVndHistory usulanHistory) {
		try {
			startTransaction();
			usulanVnd.setTingkat(-99);
			usulanVnd.setNextUser(null);
			usulanVnd.setNextPos(null);
			saveEntity(usulanVnd);
			SessionGetter.saveHistoryUsulanVendorRevisi(usulanHistory,session,"Revisi Data Vendor");

			if(SessionGetter.isNotEmptyList(listUsulanDetail)){
				for(UsulanVndDetail d:listUsulanDetail){
					//update Vendor Status
					TmpVndHeader vnd = getTmpVndHeaderById(d.getVendor().getVendorId());
					vnd.setVendorStatus("P");//Harus diverifikasi lagi
					saveEntity(vnd);
					commitAndClear();
				}
			}
		}catch(Exception e) {
			RollbackAndClear();
			e.printStackTrace();
		}
		
	}

	public UsulanVndHistory getLastUsulanVndHistory(
			UsulanVndHistory usulanHistory) throws Exception {
		DetachedCriteria sr = DetachedCriteria.forClass(UsulanVndHistory.class);
		sr.setProjection(Projections.max("id"));
		sr.add(Restrictions.eq("usulan", usulanHistory.getUsulan()));
		Integer id = (Integer) getEntityByDetachedCiteria(sr);
		
		DetachedCriteria crit = DetachedCriteria.forClass(UsulanVndHistory.class);
		crit.setResultTransformer(DetachedCriteria.ROOT_ENTITY);
		crit.add(Restrictions.eq("id", id));
		crit.add(Restrictions.eq("usulan", usulanHistory.getUsulan()));
		return (UsulanVndHistory) getEntityByDetachedCiteria(crit);
	}

	@Override
	public void saveApprovalUsulan(UsulanVnd usulan,
			List<UsulanVndDetail> listUsulanDetail,
			UsulanVndHistory usulanHistory, String aksi) throws Exception {
		try{
			startTransaction();
			String aks="";
			
			if(aksi.equalsIgnoreCase(SessionGetter.SETUJU)){
				AdmProsesHirarki ahp = SessionGetter.getHirarkiProsesByDistrict(SessionGetter.VENDOR_PROSES_ID, usulan.getTingkat()+1, SessionGetter.getUser().getAdmEmployee().getAdmDept().getAdmDistrict(), 0.0);
				if(ahp!=null){
					if(ahp.getAdmRoleByCurrentPos().getRoleName().equalsIgnoreCase("vendor spesialis")){
						aks = "Persetujuan Usulan";
						usulan.setTingkat(usulan.getTingkat()+1);
						usulan.setNextUser(usulan.getCreator());
						usulan.setCreator(new AdmServiceImpl().getUserById(usulan.getCreator()));
						usulan.setNextUser(usulan.getCreator());
						for(UsulanVndDetail uv : listUsulanDetail) {
							UsulanVndDetail usulanDetail = getUsulanDetailById(uv);
							usulanDetail.setVendorAccountGroup(uv.getVendorAccountGroup());
							saveEntity(usulanDetail);
						}
					}else{
						aks = "Persetujuan Usulan";
						usulan.setTingkat(usulan.getTingkat()+1);
						AdmUser us = new AdmServiceImpl().getAdmEmployeeByRole(ahp.getAdmRoleByCurrentPos()).getAdmUser();
						usulan.setNextUser(us);
						usulan.setCreator(new AdmServiceImpl().getUserById(usulan.getCreator()));
						usulan.setNextUser(us);
					}
				}
			}else if(aksi.equalsIgnoreCase(SessionGetter.REVISI)){
				usulan.setTingkat(1);
				aks = "Revisi Usulan";
				usulan.setCreator(new AdmServiceImpl().getUserById(usulan.getCreator()));
				usulan.setNextUser(usulan.getCreator());
			}
			saveEntity(usulan);

			//insert history
			usulanHistory.setUsulan(usulan);
			SessionGetter.saveHistoryUsulanVendorNew(usulanHistory,session,aks);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public void saveFinalisasiUsulan(UsulanVnd usulan,
			List<UsulanVndDetail> listUsulanDetail,
			UsulanVndHistory usulanHistory, String aksi) throws Exception {
		try{
			startTransaction();
			String aks="";
			if(aksi.equalsIgnoreCase(SessionGetter.SETUJU)){
				usulan.setTingkat(99);
				aks = "Finalisasi dan Umumkan Usulan";
			}
			saveEntity(usulan);

			//insert history
			usulanHistory.setUsulan(usulan);
			SessionGetter.saveHistoryUsulanVendor(usulanHistory,session,aks);
			//create Vendor Number
			List<UsulanVndDetail> lst = getListUsulanDetailByUsulan(usulan);
			if(lst!=null && lst.size()>0){
				for(UsulanVndDetail v:lst){
					TmpVndHeader head = (TmpVndHeader)getTmpVndHeaderById(v.getVendor().getVendorId());
					UsulanVndDetail usulVnd = getUsulanDetailById(v);
//					head.setVendorSmkNo(SessionGetter.smkNo(head.getVendorId(), "000000000"));
					head.setVendorStatus("F");//Selesai Finalisasi
					head.setVendorAccountGroup(usulVnd.getVendorAccountGroup());
					saveEntity(head);
//					copyTmpToVnd(head);
				}
			}
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
		
	}

	@Override
	public void aktivasiDanPenomoran(TmpVndHeader tmp) throws Exception {
		try {
			startTransaction();
			saveEntity(tmp);
			copyTmpToVnd(tmp);
			//send mail to vendor
			String msg = "";
			msg = "Selamat, Perusahaan <b>"+tmp.getVendorName().toUpperCase()+"</b> Telah Terdaftar Menjadi Rekanan di PT Semen Baturaja (Persero) <br/>";
			msg = msg.concat("<tr>");
				msg = msg.concat("<td>");
					msg = msg.concat("No Mitra Kerja :");
				msg = msg.concat("</td>");
				msg = msg.concat("<td>");
				msg = msg.concat("</td>");
			msg = msg.concat("</tr>");
			msg = msg.concat("<br/>");
			msg = msg.concat("Hormat Kami");
			msg = msg.concat("<br/>");
			msg = msg.concat("<br/>");
			msg = msg.concat("eProc PT Semen Baturaja (Persero)");
			EmailSessionBean.sendMail("eProc PT. Semen Baturaja", msg, tmp.getVendorEmail());
			IntegrationService integrationService = new IntegrationServiceImpl();
			ResultDto res = integrationService.pushVendor(tmp);
			if(res.getStatus()!=200) {
				RollbackAndClear();
				throw new Exception(res.getMessage());
				
			}
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BidderListModel> listBidder(String query) throws Exception {
		List<BidderListModel> retMe = new ArrayList<BidderListModel>();
		try {
			startTransaction();
			Query q = session.createSQLQuery(query);
			List<Object[]> lst = q.list();
			if(lst!=null && lst.size()>0){
				Iterator<Object[]> it = lst.iterator();
				while(it.hasNext()){
					Object [] o = it.next();
					it.remove();
					BidderListModel m = new BidderListModel();
					m.setVendorId(Integer.valueOf(String.valueOf(o[0])));
					m.setNamaVendor(String.valueOf(o[1]));
					m.setDiundang(Integer.valueOf(String.valueOf(o[2])));
					m.setMendaftar(Integer.valueOf(String.valueOf(o[3])));
					m.setLulusAdm(Integer.valueOf(String.valueOf(o[4])));
					m.setLulusTeknis(Integer.valueOf(String.valueOf(o[5])));
					m.setLulusHarga(Integer.valueOf(String.valueOf(o[6])));
					m.setMenang(Integer.valueOf(String.valueOf(o[7])));
					retMe.add(m);
				}
			}
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
		return retMe;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<BidderDetailModel> listDetailBidder(String bidderDetail)
			throws Exception {
		List<BidderDetailModel> retMe = new ArrayList<BidderDetailModel>();
		try {
			startTransaction();
			Query q = session.createSQLQuery(bidderDetail);
			List<Object[]> lst = q.list();
			if(lst!=null && lst.size()>0){
				Iterator<Object[]> it = lst.iterator();
				while(it.hasNext()){
					Object o[] = it.next();
					it.remove();
					BidderDetailModel d =  new BidderDetailModel();
					d.setPpmId(Integer.valueOf(String.valueOf(o[0])));
					d.setNomorRfq(String.valueOf(o[1]));
					d.setJudul(String.valueOf(o[2]));
					d.setOe(String.valueOf(o[3]));
					d.setRealisasi(String.valueOf(o[4]));
					retMe.add(d);
				}
			}
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
		return retMe;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<VendorExpiredModel> getExpiredVendor(String expiredDokumen)
			throws Exception {
		List<VendorExpiredModel> retMe = new ArrayList<VendorExpiredModel>();
		try {
			startTransaction();
			Query q = session.createSQLQuery(expiredDokumen);
			List<Object[]> lst = q.list();
			if(lst!=null && lst.size()>0){
				Iterator<Object[]> it = lst.iterator();
				while(it.hasNext()){
					Object o[] = it.next();
					it.remove();
					VendorExpiredModel m =  new VendorExpiredModel();
					m.setVendorId(Integer.valueOf(String.valueOf(o[0])));
					m.setVendorName(String.valueOf(o[1]));
					m.setSiup(Integer.valueOf(String.valueOf(o[2])));
					m.setTdp(Integer.valueOf(String.valueOf(o[3])));
					m.setDomisili(Integer.valueOf(String.valueOf(o[4])));
					m.setIjin(Integer.valueOf(String.valueOf(o[5])));
					m.setSertifikat(Integer.valueOf(String.valueOf(o[6])));
					m.setEmail(String.valueOf(o[7]));
					m.setStatus(String.valueOf(o[8]));
					retMe.add(m);
				}
			}
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
		return retMe;
	}

	@Override
	public void setStatusVendor(String statusVendor) throws Exception {
		try {
			startTransaction();
			Query q = session.createSQLQuery(statusVendor);
			q.executeUpdate();
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
	}

	@Override
	public TmpVndAddress getTmpVndAddByTmpVndHeader(TmpVndHeader vndHeader) {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndAddress.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("tmpVndHeader", FetchMode.JOIN);
		dc.add(Restrictions.eq("tmpVndHeader", vndHeader));
		
		return (TmpVndAddress) getEntityByDetachedCiteria(dc);
	}
	
	@Override
	public TmpVndFinRpt getTmpVndFinRptByHeader(TmpVndHeader vndHeader) {
		DetachedCriteria dc = DetachedCriteria.forClass(TmpVndAddress.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("tmpVndHeader", FetchMode.JOIN);
		dc.setFetchMode("admCurrency", FetchMode.JOIN);
		dc.add(Restrictions.eq("tmpVndHeader", vndHeader));
		
		return (TmpVndFinRpt) getEntityByDetachedCiteria(dc);
	}
	
	@Override
	public void saveVendorIdSap(VndHeader vndHeader, Long vendorNumber) {
		try {
			startTransaction();
			
			VndHeader vnd = getVndHeaderById(vndHeader.getVendorId());
			TmpVndHeader tmpVnd = getTmpVndHeaderById(vndHeader.getVendorId());
			if(vnd.getVendorSapId()==null) {
				vnd.setVendorSapId(vendorNumber);
				saveEntity(vnd);
			}
			if(tmpVnd.getVendorSapId()==null) {
				tmpVnd.setVendorSapId(vendorNumber);
				saveEntity(tmpVnd);
			}
			
			commitAndClear();
			
		}catch(Exception e) {
			RollbackAndClear();
			e.printStackTrace();
		}
		
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<UsulanVnd> getListUsulanByYear(Object object, Integer status, Integer year) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(UsulanVnd.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		if(status!=null){
//			crit.add(Restrictions.eq("tingkat", status)); //pembuatan or edit only
			if(status==3){
				crit.add(Restrictions.ge("tingkat", status));
			}else if(status==1){
				crit.add(Restrictions.eq("tingkat", 1));
			}
		}else{
			crit.add(Restrictions.ne("tingkat", 1));//persetujuan Only
		}
		if(object!=null){
			if(object instanceof AdmUser){
				crit.add(Restrictions.eq("nextUser", (AdmUser)object));
			}
		}
		crit.add(Restrictions.sqlRestriction("to_char({alias}."
				+ "CREATED_DATE, 'yyyy') = ?", year, StandardBasicTypes.INTEGER));
		return (List<UsulanVnd>) getListByDetachedCiteria(crit);
	}
	
}
