package eproc.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.type.StandardBasicTypes;

import com.eproc.utils.SessionGetter;
import com.eproc.x.report.command.DataSheetPengadaanCommand;
import com.eproc.x.report.command.ProcurementProsesCommand;
import com.eproc.x.report.command.ProcurementValueCommand;
import com.eproc.x.report.command.RekapPembelianCommand;
import com.orm.model.AdmDept;
import com.orm.model.AdmDistrict;
import com.orm.model.AdmProses;
import com.orm.model.CtrContractHeader;
import com.orm.model.PrcAnggaranJdeDetail;
import com.orm.model.PrcMainHeader;
import com.orm.tools.GenericDao;
import com.orm.tools.HibernateUtil;

public class RptServiceImpl extends GenericDao implements RptService {

	@Override
	public List<List<String>> getStatisticVendor() {
		Session sess = HibernateUtil.getSessionFactory().openSession();
		List<List<String>> returnMe = new ArrayList<List<String>>();
		String queryStr = "SELECT VND.VENDOR_STATUS, COUNT(DIST.VENDOR_ID) " +
				"FROM TMP_VND_HEADER VND INNER JOIN TMP_VND_DISTRICT DIST " +
				"ON VND.VENDOR_ID = DIST.VENDOR_ID " +
				"GROUP BY VND.VENDOR_STATUS";
		Query q = sess.createSQLQuery(queryStr);
		List tmp = q.list();
		if(tmp!=null && tmp.size()>0){
			Iterator it =tmp.iterator();
			while(it.hasNext()){
				Object[] obj = (Object[])it.next();
				List<String> lst = new ArrayList<String>();

				lst.add(String.valueOf(obj[0]));

				lst.add(String.valueOf(obj[1]));
				/*System.out.println(">>>>>>>>" + String.valueOf(obj[0]));
				System.out.println(">>>>>>>>" + String.valueOf(obj[1]));*/
				returnMe.add(lst);
			}
		}
		return returnMe;
	}

	@Override
	public List<List<String>> getStatisticProc(AdmDept dept) {
		Session sess = HibernateUtil.getSessionFactory().openSession();
		List<List<String>> returnMe = new ArrayList<List<String>>();
		
		String queryStr = "";
		if(dept != null && dept.getId() != 0){
			queryStr = "SELECT ADM.PROSES_NAME, NVL(COUNT(PRC.PPM_ID), 0) " +
					"FROM PRC_MAIN_HEADER PRC LEFT JOIN ADM_PROSES ADM " +
					"ON PRC.PPM_PROSES = ADM.ID " +
					"WHERE PRC.DEPT_ID = '" + dept.getId() +"'" +
					"GROUP BY ADM.PROSES_NAME, ADM.ID " +
					"ORDER BY ADM.ID ASC";
					
		}else{
			queryStr = "SELECT ADM.PROSES_NAME, NVL(COUNT(PRC.PPM_ID), 0) " +
					"FROM PRC_MAIN_HEADER PRC RIGHT JOIN ADM_PROSES ADM " +
					"ON PRC.PPM_PROSES = ADM.ID " +
					"GROUP BY ADM.PROSES_NAME, ADM.ID " +
					"ORDER BY ADM.ID ASC";
		}
		
		Query q = sess.createSQLQuery(queryStr);
		List tmp = q.list();
		if(tmp!=null && tmp.size()>0){
			Iterator it =tmp.iterator();
			while(it.hasNext()){
				Object[] obj = (Object[])it.next();
				List<String> lst = new ArrayList<String>();

				lst.add(String.valueOf(obj[0]));

				lst.add(String.valueOf(obj[1]));
				/*System.out.println(">>>>>>>>" + String.valueOf(obj[0]));
				System.out.println(">>>>>>>>" + String.valueOf(obj[1]));*/
				returnMe.add(lst);
			}
		}
		return returnMe;
	}

	@Override
	public List<List<String>> getStatisticVendor(AdmDistrict dist) {
		Session sess = HibernateUtil.getSessionFactory().openSession();
		List<List<String>> returnMe = new ArrayList<List<String>>();
		
		String queryStr = "";
		if(dist != null && dist.getId() != 0){
			queryStr = "SELECT VND.VENDOR_STATUS, COUNT(DIST.VENDOR_ID) " +
					"FROM TMP_VND_HEADER VND INNER JOIN TMP_VND_DISTRICT DIST " +
					"ON VND.VENDOR_ID = DIST.VENDOR_ID " +
					"WHERE DIST.DISTRICT_ID = '" + dist.getId() +"'" +
					"GROUP BY VND.VENDOR_STATUS ";
					
		}else{
			queryStr = "SELECT VND.VENDOR_STATUS, COUNT(DIST.VENDOR_ID) " +
					"FROM TMP_VND_HEADER VND INNER JOIN TMP_VND_DISTRICT DIST " +
					"ON VND.VENDOR_ID = DIST.VENDOR_ID " +
					"GROUP BY VND.VENDOR_STATUS";
		}
		
		Query q = sess.createSQLQuery(queryStr);
		List tmp = q.list();
		if(tmp!=null && tmp.size()>0){
			Iterator it =tmp.iterator();
			while(it.hasNext()){
				Object[] obj = (Object[])it.next();
				List<String> lst = new ArrayList<String>();

				lst.add(String.valueOf(obj[0]));

				lst.add(String.valueOf(obj[1]));
				/*System.out.println(">>>>>>>>" + String.valueOf(obj[0]));
				System.out.println(">>>>>>>>" + String.valueOf(obj[1]));*/
				returnMe.add(lst);
			}
		}
		return returnMe;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CtrContractHeader> searchKontrakHeader(
			CtrContractHeader contractHeader, Integer year) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(CtrContractHeader.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("admUserByCurrentUser", FetchMode.JOIN);
		crit.setFetchMode("vndHeader", FetchMode.JOIN);
		if(contractHeader!=null){
			if(contractHeader.getContractNumber()!=null && contractHeader.getContractNumber()!=""){
				crit.add(Restrictions.ilike("contractNumber", contractHeader.getContractNumber(), MatchMode.ANYWHERE));
			}
			if(contractHeader.getContractProsesLevel()!=null)
				crit.add(Restrictions.eq("contractProsesLevel",contractHeader.getContractProsesLevel()));
			if(contractHeader.getContractAmount()!=null && contractHeader.getContractAmmendReason()!=null){
				BigDecimal start = new BigDecimal(contractHeader.getContractAmmendReason());
				crit.add(Restrictions.between("contractAmount", start, contractHeader.getContractAmount()));
			}
			crit.add(Restrictions.sqlRestriction("to_char({alias}."
					+ "CREATED_DATE, 'yyyy') = ?", year, StandardBasicTypes.INTEGER));
			
		}
		
		
		List<CtrContractHeader> retMe = (List<CtrContractHeader>) getListByDetachedCiteria(crit);
		List<CtrContractHeader> returnMe = new ArrayList<CtrContractHeader>();
		if(retMe!=null && retMe.size()>0){
			for(CtrContractHeader c:retMe){
				//c.setStatusGlobal(new RfqVendorServiceImpl().getStatusPembayaran(c.getJdeNumber()));
				returnMe.add(c);
			}
		}
		return returnMe;
	}

	@Override
	public int countContractHeader(Object object) throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(CtrContractHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setProjection(Projections.rowCount());
		if(object!=null && object instanceof CtrContractHeader){
			CtrContractHeader ch = (CtrContractHeader)object;
			if(ch.getContractProsesLevel()!=null)
				dc.add(Restrictions.eq("contractProsesLevel",ch.getContractProsesLevel()));
			
			if(ch.getContractNumber()!=null && ch.getContractNumber()!=""){
				dc.add(Restrictions.ilike("contractNumber", ch.getContractNumber(), MatchMode.ANYWHERE));
			}
			
			if(ch.getContractAmount()!=null && ch.getContractAmmendReason()!=null){
				BigDecimal start = new BigDecimal(ch.getContractAmmendReason());
				dc.add(Restrictions.between("contractAmount", start, ch.getContractAmount()));
			}
			
		}
		Object obj = getEntityByDetachedCiteria(dc);
		return Integer.valueOf(String.valueOf(obj)).intValue();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CtrContractHeader> listCountContractHeaderReport(Object object,
			int page, int i) throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(CtrContractHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("admUserByCurrentUser", FetchMode.JOIN);
		dc.setFetchMode("vndHeader", FetchMode.JOIN);
		dc.setFetchMode("prcMainHeader", FetchMode.JOIN);
		if(object!=null && object instanceof CtrContractHeader){
			CtrContractHeader ch = (CtrContractHeader)object;
			if(ch.getContractProsesLevel()!=null)
				dc.add(Restrictions.eq("contractProsesLevel",ch.getContractProsesLevel()));
			
			if(ch.getContractNumber()!=null && ch.getContractNumber()!=""){
				dc.add(Restrictions.ilike("contractNumber", ch.getContractNumber(), MatchMode.ANYWHERE));
			}
			
			if(ch.getContractAmount()!=null && ch.getContractAmmendReason()!=null){
				BigDecimal start = new BigDecimal(ch.getContractAmmendReason());
				dc.add(Restrictions.between("contractAmount", start, ch.getContractAmount()));
			}
		}
			
		
		List<CtrContractHeader> retMe = ((List<CtrContractHeader>) getListByDetachedCiteriaPaging(dc, page, i));
		List<CtrContractHeader> returnMe = new ArrayList<CtrContractHeader>();
		if(retMe!=null && retMe.size()>0){
			for(CtrContractHeader c:retMe){
				Date  dt2 = c.getContractEnd();
				Date dt1 = new Date();
				
				Calendar c2 = Calendar.getInstance();
				c2.setTime(dt2);
				
				Calendar c1 = Calendar.getInstance();
				c1.setTime(dt1);
				
				int tahun = c2.get(Calendar.YEAR) - c1.get(Calendar.YEAR);
				tahun = tahun * 365;
				
				int bulan = c2.get(Calendar.MONTH) - c1.get(Calendar.MONTH);
				bulan = bulan * 30;
				
				int hari = c2.get(Calendar.DATE) - c1.get(Calendar.DATE);
				int total = tahun+ bulan + hari;
				
				if(total>90){
					c.setStatusGlobal("1");
				}else if(total<=90 && total>60){
					c.setStatusGlobal("2");
				}else if(total<=60 && total>30){
					c.setStatusGlobal("3");
				}else if(total<=30){
					c.setStatusGlobal("4");
				}
				
				
				returnMe.add(c);
			}
		}
		return returnMe;
	}

	@Override
	public List<ProcurementValueCommand> getListProcurementValue(
			String procurementValue, Date startDate, Date endDate)
			throws Exception {
		List<ProcurementValueCommand> retMe = new  ArrayList<ProcurementValueCommand>();
		startTransaction();
		try {
			Query q = session.createSQLQuery(procurementValue);
			if(startDate!=null){
				q.setTimestamp(0, startDate);
				//q.setTimestamp(2, startDate);
			}else{
				q.setTimestamp(0, new Date());
				//q.setTimestamp(2, new Date());
			}
			
			if(endDate!=null){
				q.setTimestamp(1, endDate);
				//q.setTimestamp(3, endDate);
			}else{
				q.setTimestamp(1, new Date());
				//q.setTimestamp(3, new Date());
			}
			@SuppressWarnings("unchecked")
			List<Object[]> obj = q.list();
			if(obj!=null){
				Iterator<Object[]> it = obj.iterator();
				while(it.hasNext()){
					Object o[] = it.next();
					it.remove();
					ProcurementValueCommand p = new ProcurementValueCommand();
					p.setId(Integer.valueOf(SessionGetter.nullNumberToEmptyString(o[0])));
					p.setMetode(SessionGetter.nullObjectToEmptyString(o[1]));
					p.setEE(SessionGetter.nullObjectToEmptyString(o[2]));
					p.setOE(SessionGetter.nullObjectToEmptyString(o[3]));
					p.setKontrak(SessionGetter.nullObjectToEmptyString(o[4]));
					p.setFrequensi(SessionGetter.nullObjectToEmptyString(o[5]));
					retMe.add(p);
				}
			}
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
		return retMe;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProcurementValueCommand> listProcurementValueDetail(
			String procurementValueDetail) throws Exception {
		List<ProcurementValueCommand> retMe = new ArrayList<ProcurementValueCommand>();
		try {
			startTransaction();
			Query q = session.createSQLQuery(procurementValueDetail);
			List<Object[]> lst = q.list();
			if(lst!=null && lst.size()>0){
				Iterator<Object[]> it = lst.iterator();
				while(it.hasNext()){
					Object o[] = it.next();
					it.remove();
					ProcurementValueCommand p = new ProcurementValueCommand();
					p.setPpmId(Integer.valueOf(String.valueOf(o[0])));
					p.setNomorSpph(SessionGetter.nullObjectToEmptyString(o[1]));
					p.setHoe(SessionGetter.nullObjectToEmptyString(o[2]));
					p.setHang(SessionGetter.nullObjectToEmptyString(o[3]));
					p.setHpp(SessionGetter.nullObjectToEmptyString(o[4]));
					p.setHpq(SessionGetter.nullObjectToEmptyString(o[5]));
					p.setHspph(SessionGetter.nullObjectToEmptyString(o[6]));
					p.setNamaBuyer(SessionGetter.nullObjectToEmptyString(o[7]));
					retMe.add(p);
				}
			}
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
		return retMe;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProcurementProsesCommand> getListProcurementProses(
			String queryProsesPengadaan) throws Exception {
		List<ProcurementProsesCommand> retMe = new ArrayList<ProcurementProsesCommand>();
		try {
			startTransaction();
			Query q = session.createSQLQuery(queryProsesPengadaan);
			List<Object[]> obj = q.list();
			if(obj!=null && obj.size()>0){
				Iterator<Object[]> it = obj.iterator();
				while (it.hasNext()){
					Object [] o = it.next();
					it.remove();
					ProcurementProsesCommand pr = new ProcurementProsesCommand();
					pr.setProsesId(Integer.valueOf(String.valueOf(o[0])));
					pr.setProsesName(String.valueOf(o[1]));
					pr.setJumlah(Integer.valueOf(String.valueOf(o[2])));
					pr.setTingkat(Integer.valueOf(String.valueOf(o[3])));
					retMe.add(pr);
				}
			}
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			e.printStackTrace();
			throw e;
		}
		return retMe;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcMainHeader> getListPengadaanByProses(Integer prosesId, Integer tingkat)
			throws Exception {
		DetachedCriteria dc= DetachedCriteria.forClass(PrcMainHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		if(prosesId!=108 && prosesId!=105)
			dc.add(Restrictions.eq("admProses", new AdmProses(prosesId)));
		else if(prosesId==108)
			dc.add(Restrictions.eq("ppmProsesLevel", 99));
		else if(prosesId==105){
			dc.add(Restrictions.eq("admProses", new AdmProses(prosesId)));
			dc.add(Restrictions.eq("ppmProsesLevel", tingkat));
		}
		return (List<PrcMainHeader>) getListByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DataSheetPengadaanCommand> getDataSheetPengadaan(
			String queryDataSheetPengadaan, Object object, Object object2)
			throws Exception {
		List<DataSheetPengadaanCommand> retMe = new ArrayList<DataSheetPengadaanCommand>();
		try {
			startTransaction();
			Query q = session.createSQLQuery(queryDataSheetPengadaan);
			if(object!=null && object2!=null){
				queryDataSheetPengadaan = queryDataSheetPengadaan.concat(" and a.created_date between ? and ? ");
				q = session.createSQLQuery(queryDataSheetPengadaan);
				Date start = (Date)object;
				Date end = (Date)object2;
				q.setTimestamp(0, start);
				q.setTimestamp(1, end);
			}
			List<Object[]> obj = q.list();
			if(obj!=null && obj.size()>0){
				Iterator<Object[]> it = obj.iterator();
				while (it.hasNext()){
					Object [] o = it.next();
					it.remove();
					DataSheetPengadaanCommand pr = new DataSheetPengadaanCommand();
					pr.setNomorPr(SessionGetter.nullToEmptyString(String.valueOf(o[0])));
					pr.setJudul(SessionGetter.nullToEmptyString(String.valueOf(o[1])));
					pr.setDeskripsi(SessionGetter.nullToEmptyString(String.valueOf(o[2])));
					pr.setTanggalOe(SessionGetter.nullToEmptyString(String.valueOf(o[3])));
					pr.setTanggalRks(SessionGetter.nullToEmptyString(String.valueOf(o[4])));
					pr.setTanggalMitra(SessionGetter.nullToEmptyString(String.valueOf(o[5])));
					pr.setTanggalBukaSpph(SessionGetter.nullToEmptyString(String.valueOf(o[6])));
					pr.setTanggalKonfirmasiAnggaran(SessionGetter.nullToEmptyString(String.valueOf(o[7])));
					pr.setTanggalNegosiasi(SessionGetter.nullToEmptyString(String.valueOf(o[8])));
					pr.setTanggalEvaluasi(SessionGetter.nullToEmptyString(String.valueOf(o[9])));
					retMe.add(pr);
				}
			}
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			e.printStackTrace();
			throw e;
		}
		return retMe;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RekapPembelianCommand> getRekapPembelian(String rekapPembelian,
			Date startDate, Date endDate) throws Exception {
		List<RekapPembelianCommand> retMe = new ArrayList<RekapPembelianCommand>();
		try {
			startTransaction();
			Query q = session.createSQLQuery(rekapPembelian);
			if(startDate!=null && endDate!=null){
				rekapPembelian = rekapPembelian.concat(" where contract_start between ? and ? ");
				q = session.createSQLQuery(rekapPembelian);
				q.setTimestamp(0, startDate);
				q.setTimestamp(1, endDate);
			}
			List<Object[]> obj = q.list();
			if(obj!=null && obj.size()>0){
				Iterator<Object[]> it = obj.iterator();
				while (it.hasNext()){
					Object [] o = it.next();
					it.remove();
					RekapPembelianCommand pr = new RekapPembelianCommand();
					pr.setPpmId(Integer.valueOf(SessionGetter.nullToEmptyString(String.valueOf(o[0]))));
					pr.setNomorPr(SessionGetter.nullToEmptyString(String.valueOf(o[1])));
					pr.setTanggalOe(SessionGetter.nullToEmptyString(String.valueOf(o[2])));
					pr.setJumlah(SessionGetter.nullToEmptyString(String.valueOf(o[3])));
					pr.setSatuan(SessionGetter.nullToEmptyString(String.valueOf(o[4])));
					pr.setNamaBarang(SessionGetter.nullToEmptyString(String.valueOf(o[5])));
					pr.setHarga(SessionGetter.nullToEmptyString(String.valueOf(o[6])));
					pr.setHargaTotal(SessionGetter.nullToEmptyString(String.valueOf(o[7])));
					pr.setNomorPo(SessionGetter.nullToEmptyString(String.valueOf(o[8])));
					pr.setTanggalPo(SessionGetter.nullToEmptyString(String.valueOf(o[9])));
					pr.setTanggalAkhirPo(SessionGetter.nullToEmptyString(String.valueOf(o[10])));
					pr.setNamaVendor(SessionGetter.nullToEmptyString(String.valueOf(o[11])));
					pr.setTanggalPenyerahan(SessionGetter.nullToEmptyString(String.valueOf(o[12])));
					retMe.add(pr);
				}
			}
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			e.printStackTrace();
			throw e;
		}
		return retMe;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrcAnggaranJdeDetail> getListAnggaranDetail(Object object)
			throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcAnggaranJdeDetail.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("prcMainHeader", FetchMode.JOIN);
		if(object!=null){
			if(object instanceof PrcAnggaranJdeDetail){
				PrcAnggaranJdeDetail jd = (PrcAnggaranJdeDetail)object;
				crit.add(Restrictions.ilike("rkaid", jd.getRkaid(), MatchMode.ANYWHERE));
			}
		}
		return (List<PrcAnggaranJdeDetail>) getListByDetachedCiteria(crit);
	}

	@Override
	public void updateMataAnggaran(PrcAnggaranJdeDetail anggaranJdeDetail)
			throws Exception {
		try {
			startTransaction();
			PrcAnggaranJdeDetail jde = getAnggaranJdeDetailById(anggaranJdeDetail);
			jde.setRkaid(anggaranJdeDetail.getRkaid());
			saveEntity(jde);
			commitAndClear();
		} catch (Exception e) {
			e.printStackTrace();
			RollbackAndClear();
			throw e;
		}
		
	}

	public PrcAnggaranJdeDetail getAnggaranJdeDetailById(
			PrcAnggaranJdeDetail anggaranJdeDetail) {
		DetachedCriteria crit = DetachedCriteria.forClass(PrcAnggaranJdeDetail.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.idEq(anggaranJdeDetail.getId()));
		return (PrcAnggaranJdeDetail) getEntityByDetachedCiteria(crit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CtrContractHeader> searchKontrakHeaderAktif(
			CtrContractHeader contractHeader, Integer periode) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(CtrContractHeader.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("admUserByCurrentUser", FetchMode.JOIN);
		crit.setFetchMode("vndHeader", FetchMode.JOIN);
		crit.setFetchMode("prcMainHeader", FetchMode.JOIN);
		crit.setFetchMode("admCurrency", FetchMode.JOIN);
		crit.addOrder(Order.asc("createdDate"));
		if(contractHeader!=null){
			if(contractHeader.getContractNumber()!=null && contractHeader.getContractNumber()!=""){
				crit.add(Restrictions.ilike("contractNumber", contractHeader.getContractNumber(), MatchMode.ANYWHERE));
			}
			if(contractHeader.getContractProsesLevel()!=null)
				crit.add(Restrictions.eq("contractProsesLevel",contractHeader.getContractProsesLevel()));
			if(contractHeader.getContractAmount()!=null && contractHeader.getContractAmmendReason()!=null){
				BigDecimal start = new BigDecimal(contractHeader.getContractAmmendReason());
				crit.add(Restrictions.between("contractAmount", start, contractHeader.getContractAmount()));
			}
			
			
			
		}
		
		if(periode!=null && periode!=0){
			if(periode==1){
				crit.add(Restrictions.sqlRestriction(" (trunc(contract_end)-trunc(sysdate)) > 90 "));
			}else if(periode==2){
				crit.add(Restrictions.sqlRestriction(" (trunc(contract_end)-trunc(sysdate)) between 60 and 90 "));
			}else if(periode==3){
				crit.add(Restrictions.sqlRestriction(" (trunc(contract_end)-trunc(sysdate)) between 30 and 60 "));
			}else if(periode==4){
				crit.add(Restrictions.sqlRestriction(" (trunc(contract_end)-trunc(sysdate)) < 30 "));
			}
		}
		List<CtrContractHeader> retMe = (List<CtrContractHeader>) getListByDetachedCiteria(crit);
		List<CtrContractHeader> returnMe = new ArrayList<CtrContractHeader>();
		if(retMe!=null && retMe.size()>0){
			for(CtrContractHeader c:retMe){
				//c.setStatusGlobal(new RfqVendorServiceImpl().getStatusPembayaran(c.getJdeNumber()));
				c.setStatusGlobal(String.valueOf(periode));
				returnMe.add(c);
			}
		}
		return returnMe;
	}

}
