package eproc.controller;

import java.util.Date;
import java.util.List;

import com.eproc.x.report.command.DataSheetPengadaanCommand;
import com.eproc.x.report.command.ProcurementProsesCommand;
import com.eproc.x.report.command.ProcurementValueCommand;
import com.eproc.x.report.command.RekapPembelianCommand;
import com.orm.model.AdmDept;
import com.orm.model.AdmDistrict;
import com.orm.model.CtrContractHeader;
import com.orm.model.PrcAnggaranJdeDetail;
import com.orm.model.PrcMainHeader;

public interface RptService {
	public List<List<String>> getStatisticVendor();
	public List<List<String>> getStatisticVendor(AdmDistrict dist);
	public List<List<String>> getStatisticProc(AdmDept dept);
	public List<CtrContractHeader> searchKontrakHeader(
			CtrContractHeader contractHeader,  Integer year) throws Exception;
	public int countContractHeader(Object object) throws Exception;
	public List<CtrContractHeader> listCountContractHeaderReport(Object object,
			int page, int i) throws Exception;
	public List<ProcurementValueCommand> getListProcurementValue(
			String procurementValue, Date startDate, Date endDate) throws Exception;
	public List<ProcurementValueCommand> listProcurementValueDetail(
			String procurementValueDetail) throws Exception;
	public List<ProcurementProsesCommand> getListProcurementProses(
			String queryProsesPengadaan) throws Exception;
	public List<PrcMainHeader> getListPengadaanByProses(Integer prosesId, Integer tingkat) throws Exception;
	public List<DataSheetPengadaanCommand> getDataSheetPengadaan(
			String queryDataSheetPengadaan, Object object, Object object2) throws Exception;
	public List<RekapPembelianCommand> getRekapPembelian(String rekapPembelian,
			Date startDate, Date endDate) throws Exception;
	public List<PrcAnggaranJdeDetail> getListAnggaranDetail(Object object) throws Exception;
	public void updateMataAnggaran(PrcAnggaranJdeDetail anggaranJdeDetail) throws Exception;
	public List<CtrContractHeader> searchKontrakHeaderAktif(
			CtrContractHeader contractHeader, Integer periode) throws Exception;
}
