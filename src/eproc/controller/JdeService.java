package eproc.controller;

import java.net.InetAddress;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.Session;

import com.eproc.utils.JulianToGregorianViceVersa;
import com.eproc.utils.SessionGetter;
import com.jde.model.PODetail;
import com.jde.model.POHeader;
import com.jdedwards.system.connector.Connector;
import com.jdedwards.system.connector.dynamic.OneworldTransaction;
import com.jdedwards.system.connector.dynamic.UserSession;
import com.jdedwards.system.connector.dynamic.callmethod.BSFNExecutionWarning;
import com.jdedwards.system.connector.dynamic.callmethod.ExecutableMethod;
import com.jdedwards.system.connector.dynamic.spec.source.BSFNMethod;
import com.jdedwards.system.connector.dynamic.spec.source.BSFNSpecSource;
import com.jdedwards.system.connector.dynamic.spec.source.OneworldBSFNSpecSource;
import com.orm.model.CtrContractHeader;
import com.orm.model.CtrContractItem;
import com.orm.model.PrcAnggaranJdeDetail;
import com.orm.model.PrcAnggaranJdeHeader;
import com.orm.model.PrcMainHeader;
import com.orm.model.TmpVndAddress;
import com.orm.model.TmpVndHeader;
import com.orm.tools.HibernateUtil;

public class JdeService {

	/**
	 * insert new vendor
	 * @author kusumah
	 * 
	 * @param head
	 * @throws Exception
	 */
	public static void updateVendor(TmpVndHeader head, Integer sessionID,UserSession session, TmpVndAddress alamat)
			throws Exception {
		try {
			OneworldTransaction transaction = session.createOneworldTransaction(true);
			transaction.begin();
			System.out.print("Reading BSFNSpecSource ... ");
			BSFNSpecSource specSource = new OneworldBSFNSpecSource(sessionID);
			System.out.print("OK! (" + specSource.getName() + ")\n");

			System.out.println("Getting BSFNMethod ... ");
			BSFNMethod bsfnMethod = specSource.getBSFNMethod("AddressBookMasterMBF");
			ExecutableMethod ab = bsfnMethod.createExecutable();
			
			//insert new record 
			ab.setValue("cActionCode", "C");//artinya add data (I=INQUERY, A=ADD, C= UPDATE, D=DELETE)
			ab.setValue("szTaxId", head.getVendorNpwpNo());//nomor npwp
			
			ab.setValue("szSearchType", "V");//tipe vendor
			ab.setValue("cUpdateMasterFile", "1");//write
			ab.setValue("mnAddressBookNumber", head.getVendorSmkNo());
			ab.setValue("szAlphaName", head.getVendorName().toUpperCase());
			ab.setValue("szMailingName", head.getVendorName().toUpperCase());
			ab.setValue("szDescriptionCompressed", head.getVendorName().toUpperCase().trim());
			ab.setValue("szBusinessUnit", "1201");
			String address = alamat.getAddAddress();
			if(address!=null){
				if(address.length()<=40){
					ab.setValue("szAddressLine1", address);
					ab.setValue("szAddressLine2", "");
					ab.setValue("szAddressLine3", "");
					ab.setValue("szAddressLine4", "");
				}else if(address.length()>40 && address.length()<=80){
					ab.setValue("szAddressLine1", address.substring(0, 40));
					ab.setValue("szAddressLine2", address.substring(41));
					ab.setValue("szAddressLine3", "");
					ab.setValue("szAddressLine4", "");
				}else if(address.length()>80 && address.length()<=120){
					ab.setValue("szAddressLine1", address.substring(0, 40));
					ab.setValue("szAddressLine2", address.substring(41,80));
					ab.setValue("szAddressLine3", address.substring(81));
					ab.setValue("szAddressLine4", "");
				}else if(address.length()>120){
					ab.setValue("szAddressLine1", address.substring(0, 40));
					ab.setValue("szAddressLine2", address.substring(41,80));
					ab.setValue("szAddressLine3", address.substring(81,120));
					ab.setValue("szAddressLine4", address.substring(121,160));
				}
			}
			ab.setValue("szPostalCode", alamat.getAddPostCode());
			ab.setValue("szCity", alamat.getAddCity().toUpperCase());
			ab.setValue("szState", "");//look up kemana ?
			ab.setValue("szCountry", "ID");//look up kemana ??
			ab.setValue("szPrefix1", "");
			ab.setValue("szPhoneNumber1", alamat.getAddPhone1());
			ab.setValue("szPhoneNumberType2", "FAX");
			ab.setValue("szPhoneNumber2", alamat.getAddFax());
			ab.setValue("cPayablesYNM", "Y");
			ab.setValue("cReceivablesYN", "N");
			ab.setValue("cEmployeeYN", "N");
			ab.setValue("cUserCode", "N");
			ab.setValue("cARAPNettingY", "Y");
			ab.setValue("szAccountRepresentative", "B");
			ab.setValue("szProgramId", "EPROC");
			@SuppressWarnings("unused")
			BSFNExecutionWarning warn = ab.execute(sessionID);
			transaction.commit();
		} catch (Exception e) {
			throw e;
		}
	
	}

	/**
	 * update vendor
	 * 
	 * @param head
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public static String insertVendor(TmpVndHeader head, Integer sessionID,UserSession session, TmpVndAddress alamat)
			throws Exception {
			Map output = new HashMap();
			String nomorVendor=null;
			try {
				OneworldTransaction transaction = session.createOneworldTransaction(true);
				transaction.begin();
				System.out.print("Reading BSFNSpecSource ... ");
				BSFNSpecSource specSource = new OneworldBSFNSpecSource(sessionID);
				System.out.print("OK! (" + specSource.getName() + ")\n");

				System.out.println("Getting BSFNMethod ... ");
				BSFNMethod bsfnMethod = specSource.getBSFNMethod("AddressBookMasterMBF");
				ExecutableMethod ab = bsfnMethod.createExecutable();
				
				//insert new record 
				ab.setValue("cActionCode", "A");//artinya add data (I=INQUERY, A=ADD, C= UPDATE, D=DELETE)
				ab.setValue("szTaxId", head.getVendorNpwpNo());//nomor npwp
				ab.setValue("szSearchType", "V");//tipe vendor
				ab.setValue("cUpdateMasterFile", "1");//write
				ab.setValue("szAlphaName", head.getVendorName().toUpperCase());
				ab.setValue("szMailingName", head.getVendorName().toUpperCase());
				ab.setValue("szDescriptionCompressed", head.getVendorName().toUpperCase().trim());
				ab.setValue("szBusinessUnit", "1201");
				String address = alamat.getAddAddress();
				if(address!=null){
					if(address.length()<=40){
						ab.setValue("szAddressLine1", address);
						ab.setValue("szAddressLine2", "");
						ab.setValue("szAddressLine3", "");
						ab.setValue("szAddressLine4", "");
					}else if(address.length()>40 && address.length()<=80){
						ab.setValue("szAddressLine1", address.substring(0, 40));
						ab.setValue("szAddressLine2", address.substring(41));
						ab.setValue("szAddressLine3", "");
						ab.setValue("szAddressLine4", "");
					}else if(address.length()>80 && address.length()<=120){
						ab.setValue("szAddressLine1", address.substring(0, 40));
						ab.setValue("szAddressLine2", address.substring(41,80));
						ab.setValue("szAddressLine3", address.substring(81));
						ab.setValue("szAddressLine4", "");
					}else if(address.length()>120){
						ab.setValue("szAddressLine1", address.substring(0, 40));
						ab.setValue("szAddressLine2", address.substring(41,80));
						ab.setValue("szAddressLine3", address.substring(81,120));
						ab.setValue("szAddressLine4", address.substring(121,160));
					}
				}
				ab.setValue("szPostalCode", alamat.getAddPostCode());
				ab.setValue("szCity", alamat.getAddCity().toUpperCase());
				ab.setValue("szState", "");//look up kemana ?
				ab.setValue("szCountry", "ID");//look up kemana ??
				ab.setValue("szPrefix1", "");
				ab.setValue("szPhoneNumber1", alamat.getAddPhone1());
				ab.setValue("szPhoneNumberType2", "FAX");
				ab.setValue("szPhoneNumber2", alamat.getAddFax());
				ab.setValue("cPayablesYNM", "Y");
				ab.setValue("cReceivablesYN", "N");
				ab.setValue("cEmployeeYN", "N");
				ab.setValue("cUserCode", "N");
				ab.setValue("cARAPNettingY", "Y");
				ab.setValue("szAccountRepresentative", "B");
				ab.setValue("szProgramId", "EPROC");
				@SuppressWarnings("unused")
				BSFNExecutionWarning warn = ab.execute(sessionID);
				transaction.commit();
				output = ab.getValues();
				nomorVendor = String.valueOf(output.get("mnAddressBookNumber"));
			} catch (Exception e) {
				throw e;
			}
		
		return nomorVendor;
	}
	
	public static void insertEmail(TmpVndHeader header, String nomorVendor) throws Exception{
		Session session = HibernateUtil.getSessionJde();
		try {
			session.beginTransaction();
			String query ="insert into "+SessionGetter.valueProperty("jde.schema")+".f01151 (eaan8, earck7, eaemal, eauser, eapid, eaetp, eaupmj, eaupmt, eajobn ) "
					+ "values ("+nomorVendor+",1,'"+header.getVendorEmail()+"','ALAM','EPROC','E',"+JulianToGregorianViceVersa.getJulian7FromDate(new Date())+","+JulianToGregorianViceVersa.timeString(new Date())+",'SEMBARA')";
			Query q = session.createSQLQuery(query);
			q.executeUpdate();
			
			session.getTransaction().commit();
			session.clear();
			session.close();
		} catch (Exception e) {
			session.getTransaction().rollback();
			session.clear();
			session.close();
			e.printStackTrace();
			throw e;
		}
	}
	
	/**
	 * insert anggaran via db to db
	 * @param prcHeader
	 * @param listDetail
	 * @throws Exception
	 */
	public static void insertAnggaran(PrcAnggaranJdeHeader prcHeader, List<PrcAnggaranJdeDetail> listDetail) throws Exception{
		Session session = HibernateUtil.getSessionJde();
		try {
			session.beginTransaction();
			String query = "";
			query = "insert into "+SessionGetter.valueProperty("jde.schema")+".f559901 " +
					"(kakcoo,kadcto,kadoco,kaaid,katrdj,kaapdj,kaco,kakco,kadct,kadoc,kabasedj,kamcu,kammcu," +
					"kaan8,kaan82,kaan83,kadsc1,kajj,kaapfl,kaska,kahst,kaaap,kaea,kaacm1,kaaa,katorg,kauser," +
					"kajobn,kaupmj,kaupmt,kapid) " +
					"values " +
					"('"+prcHeader.getKakcoo()+"','"+prcHeader.getKadcto()+"',"+prcHeader.getKadoco()+",'"+SessionGetter.nullToEmptyString(prcHeader.getKaaid())+"'," +
					""+JulianToGregorianViceVersa.getJulian7FromDate(prcHeader.getKatrdj())+","+prcHeader.getKaapdj()+",'"+prcHeader.getKaco()+"','"+prcHeader.getKakco()+"'," +
					"'"+SessionGetter.nullToEmptyString(prcHeader.getKadct())+"',"+prcHeader.getKadoc()+","+prcHeader.getKabasedj()+",'"+SessionGetter.nullToEmptyString(prcHeader.getKamcu())+"'," +
					"'"+prcHeader.getKammcu()+"',"+prcHeader.getKaan8()+","+prcHeader.getKaan82()+","+prcHeader.getKaan83()+"," +
					"'"+SessionGetter.nullToEmptyString(prcHeader.getKadsc1().replaceAll("'", " "))+"','"+SessionGetter.nullToEmptyString(prcHeader.getKajj())+"','A','"+SessionGetter.nullToEmptyString(prcHeader.getKaska())+"','"+SessionGetter.nullToEmptyString(prcHeader.getKahst())+"'," +
					""+prcHeader.getKaaap()+","+(prcHeader.getKaea().doubleValue()*100)+","+(prcHeader.getKaacm1().doubleValue()*100)+","+prcHeader.getKaaa()+",'"+prcHeader.getKatorg()+"'," +
					"'"+prcHeader.getKauser()+"','"+prcHeader.getKajobn()+"',"+JulianToGregorianViceVersa.getJulian7FromDate(prcHeader.getKupmj())+","+JulianToGregorianViceVersa.timeString(prcHeader.getKupmt())+",'"+SessionGetter.nullToEmptyString(prcHeader.getKapid())+"' )";
			Query q = session.createSQLQuery(query);
			q.executeUpdate();
			
			//insert detail
			if(listDetail!=null && listDetail.size()>0){
				for(PrcAnggaranJdeDetail d:listDetail){
					String kquery ="";
					System.out.println("linnnne>>>>>>>"+d.getRklnid());
					kquery = "insert into "+SessionGetter.valueProperty("jde.schema")+".f559911 " +
							"(rkkcoo,rkdcto,rkdoco,rklnid,rkaid,rkaap,rkea,rkacm1,rkaa,rkdl01,rkdl02,rkapdj," +
							"rkapfl,rktorg,rkuser,rkjobn,rkupmj,rkupmt,rkpid) " +
							"values " +
							"('"+prcHeader.getKakcoo()+"','"+prcHeader.getKadcto()+"',"+prcHeader.getKadoco()+","+d.getRklnid()+",'"+SessionGetter.nullToEmptyString(d.getRkaid())+"',"+d.getRkaap()+"," +
							" "+d.getRkaea()+","+(d.getRkacm1().doubleValue()*100)+","+d.getRkaa()+",'"+SessionGetter.nullToEmptyString(d.getRkdl01())+"','"+SessionGetter.nullToEmptyString(d.getRkdl02())+"',"+JulianToGregorianViceVersa.getJulian7FromDate(new Date())+"," +
							" 'A','"+SessionGetter.nullToEmptyString(d.getRktorg())+"','"+d.getRkuser()+"','"+d.getRkjobn()+"',"+JulianToGregorianViceVersa.getJulian7FromDate(new Date())+","+JulianToGregorianViceVersa.timeString(new Date())+",'"+SessionGetter.nullToEmptyString(d.getRkapid())+"') ";
					Query qq = session.createSQLQuery(kquery);
					qq.executeUpdate();
				}
			}
			session.getTransaction().commit();
			session.clear();
			session.close();
		} catch (Exception e) {
			session.clear();
			session.close();
			throw e;
		}
	}
	
	/**
	 * delete anggaran
	 * @param prcHeader
	 * @throws Exception
	 */
	public static void deleteAnggaranDetail(PrcAnggaranJdeHeader prcHeader) throws Exception{
		Session session = HibernateUtil.getSessionJde();
		try {
			session.beginTransaction();
			String query = "";
			
			//delete dulu
			query = "delete from "+SessionGetter.valueProperty("jde.schema")+".f559911 " +
					" where rkkcoo='"+prcHeader.getKakco()+"' and rkdcto='"+prcHeader.getKadcto()+"' and rkdoco='"+prcHeader.getKadoco()+"' ";
			Query q = session.createSQLQuery(query);
			q.executeUpdate();
			
			session.getTransaction().commit();
			session.clear();
			session.close();
		} catch (Exception e) {
			session.clear();
			session.close();
			throw e;
		}
	}
	
	/**
	 * update anggaran via db to db
	 * @param prcHeader
	 * @param listDetail
	 * @throws Exception
	 */
	public static void updateAnggaran(PrcAnggaranJdeHeader prcHeader, List<PrcAnggaranJdeDetail> listDetail) throws Exception{
		Session session = HibernateUtil.getSessionJde();
		try {
			session.beginTransaction();
			String query = "";
			query = "update "+SessionGetter.valueProperty("jde.schema")+".f559901 " +
					" set kaea="+(prcHeader.getKaea().doubleValue()*100)+", kaacm1="+(prcHeader.getKaacm1().doubleValue()*100)+" where kakcoo='"+prcHeader.getKakcoo()+"' and kadcto='"+prcHeader.getKadcto()+"' and kadoco='"+prcHeader.getKadoco()+"'";
			Query q = session.createSQLQuery(query);
			q.executeUpdate();
			
			//insert detail
			if(listDetail!=null && listDetail.size()>0){
				for(PrcAnggaranJdeDetail d:listDetail){
						String kquery ="";
						kquery = "insert into "+SessionGetter.valueProperty("jde.schema")+".f559911 " +
								"(rkkcoo,rkdcto,rkdoco,rklnid,rkaid,rkaap,rkea,rkacm1,rkaa,rkdl01,rkdl02,rkapdj," +
								"rkapfl,rktorg,rkuser,rkjobn,rkupmj,rkupmt,rkpid) " +
								"values " +
								"('"+prcHeader.getKakcoo()+"','"+prcHeader.getKadcto()+"',"+prcHeader.getKadoco()+","+d.getRklnid()+",'"+SessionGetter.nullToEmptyString(d.getRkaid())+"',"+d.getRkaap()+"," +
								" "+d.getRkaea()+","+(d.getRkacm1().doubleValue()*100)+","+d.getRkaa()+",'"+SessionGetter.nullToEmptyString(d.getRkdl01())+"','"+SessionGetter.nullToEmptyString(d.getRkdl02())+"',"+JulianToGregorianViceVersa.getJulian7FromDate(new Date())+"," +
								" 'A','"+SessionGetter.nullToEmptyString(d.getRktorg())+"','"+d.getRkuser()+"','"+d.getRkjobn()+"',"+JulianToGregorianViceVersa.getJulian7FromDate(new Date())+","+JulianToGregorianViceVersa.timeString(new Date())+",'"+SessionGetter.nullToEmptyString(d.getRkapid())+"') ";
						Query qq = session.createSQLQuery(kquery);
						qq.executeUpdate();
				}
			}
			session.getTransaction().commit();
			session.clear();
			session.close();
		} catch (Exception e) {
			session.clear();
			session.close();
			throw e;
		}
	}
	
	/**
	 * 
	 * @param sessionID
	 * @param session
	 * @param head
	 * @param listItem
	 * @throws Exception
	 */
	public static void createPo(Integer sessionID, UserSession session,CtrContractHeader head, List<CtrContractItem> listItem) throws Exception{
		try {
			//execute begin doc
			OneworldTransaction transaction = session.createOneworldTransaction(true);
			transaction.begin();
			System.out.print("Reading BSFNSpecSource ... ");
			BSFNSpecSource specSource = new OneworldBSFNSpecSource(sessionID);
			System.out.print("OK! (" + specSource.getName() + ")\n");
			String tmp [] = head.getContractNumber().trim().split("OP");
			

			System.out.println("Getting BSFNMethod ... ");
			BSFNMethod bsfnMethod = specSource.getBSFNMethod("F4311FSBeginDoc");
			ExecutableMethod ab = bsfnMethod.createExecutable();
			ab.setValue("cHeaderActionCode", "A");//add
	        ab.setValue("cProcessEdits", "1");
	        ab.setValue("cUpdateOrWriteToWorkFile", "2");
	        ab.setValue("szProgramID", "EPROC");
	        ab.setValue("szPurchaseOrderPrOptVersion", "ZJDE0001");
	        ab.setValue("szComputerID", InetAddress.getLocalHost().getHostName().substring(0, 10));
	        ab.setValue("szOrderCOmpany", tmp[1].trim());
	        ab.setValue("mnOrderNumber", tmp[0].trim());
	        ab.setValue("szOrderType", "OP");
	        ab.setValue("szOrderSuffix", "000");
	        ab.setValue("szBranchPlant", tmp[1].trim());
	        ab.setValue("szOriginalOrderCompany", "");//OKCO
	        ab.setValue("szOriginalOrderNumber", "");//OORN
	        ab.setValue("szOriginalOrderType", "");//OKTO
	        ab.setValue("szRelatedOrderCompany", "");//RKCO
	        ab.setValue("szRelatedOrderNumber", "");//RORN
	        ab.setValue("szRelatedOrderType", "");//RCTO
	        ab.setValue("mnSupplierNumber", "");//AN8
	        ab.setValue("mnShipToNumber", "");//SHAN
	        ab.setValue("jdRequestedDate", "");
	        ab.setValue("jdOrderDate", "");
	        
	        ab.setValue("jdPromisedDate", "");
	        ab.setValue("jdCancelDate", "");
	        ab.setValue("szReference01", "");
	        ab.setValue("szReference02", "");
	        ab.setValue("szDeliveryInstructions01", "");
	        ab.setValue("szDeliveryInstructions02", "");
	        ab.setValue("szPrintMessage", "");
	        ab.setValue("szSupplierPriceGroup", "");
	        ab.setValue("szPaymentTerms", "");
	        ab.setValue("szTaxExplanationCode", "");
	        ab.setValue("szTaxRateArea", "");
	        ab.setValue("szTaxCertificate", "");
	        ab.setValue("cAssociatedText", "");
	        ab.setValue("szHoldCode", "");
	        ab.setValue("szFreightHandlingCode", "");
	        ab.setValue("mnBuyerNumber", "");
	        ab.setValue("mnCarrierNumber", "");
	        ab.setValue("cEvaluatedReceiptsFlag", "");
	        ab.setValue("cSendMethod", "");
	        ab.setValue("szLandedCostRule", "");
	        ab.setValue("szApprovalRouteCode", "");
	        ab.setValue("mnChangeOrderNumber", "");
	        ab.setValue("cCurrencyMode", "");
	        ab.setValue("szTransactionCurrencyCode", "");
	        ab.setValue("mnCurrencyExchangeRate", "");
	        ab.setValue("szOrderedPlacedBy", "");
	        ab.setValue("szOrderTakenBy", "");
	        ab.setValue("szProgramID", "");
	        ab.setValue("szApprovalRoutePO", "");
	        ab.setValue("szPurchaseOrderPrOptVersion", "");
	        
	        ab.setValue("szBaseCurrencyCode", "");
	        ab.setValue("szUserID", "");
	        ab.setValue("cAddNewLineToExistingOrder", "");
	        ab.setValue("idInternalVariables", "");
	        ab.setValue("cSourceOfData", "");
	        ab.setValue("mnSODOrderNumber", "");
	        ab.setValue("szSODOrderType", "");
	        ab.setValue("szSODOrderCompany", "");
	        ab.setValue("szSODOrderSuffix", "");
	        ab.setValue("mnRetainage", "");
	        ab.setValue("szDescription", "");
	        ab.setValue("szRemark", "");
	        ab.setValue("jdEffectiveDate", "");
	        ab.setValue("jdPhysicalCompletionDate", "");
	        ab.setValue("mnTriangulationRateFromCurrenc", "");
	        ab.setValue("mnTriangulationRateToCurrency", "");
	        ab.setValue("cCurrencyConversionMethod", "");
	        ab.setValue("szPriceAdjustmentScheduleN", "");
	        ab.setValue("cAIADocument", "");
	        ab.setValue("mnProcessID", "");
	        ab.setValue("mnTransactionID", "");
	        ab.setValue("mnRMADocNumber", "");
	        ab.setValue("szRMADocType", "");
	        ab.setValue("szPOEMBFInternalFlags", "");
	        ab.setValue("jdUserReservedDate", "");
	        ab.setValue("mnUserReservedAmount", "");
	        ab.setValue("szUserReservedCode", "");
	        ab.setValue("szUserReservedReference", "");
	        ab.setValue("mnUserReservedNumber", "");
	        @SuppressWarnings("unused")
			BSFNExecutionWarning warn = ab.execute(sessionID);
	        transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	public static String insertPOFromModel(Integer sessionID, UserSession session, POHeader po,List<PODetail> listDetail, String orderType) throws Exception{
		String retMe = "";
		try {
			//execute begin doc
			int data = Calendar.getInstance().get(Calendar.YEAR);
			String fiscal = String.valueOf(data).substring(2);
			String nomor = getNextNumberAnggaran(SessionGetter.nullToEmptyString(po.getSzOrderCompany()).trim(), String.valueOf(po.getSzOrderType()), "20", fiscal, session, sessionID);
			
			OneworldTransaction transaction = session.createOneworldTransaction(true);
			transaction.begin();
			System.out.print("Reading BSFNSpecSource ... ");
			BSFNSpecSource specSource = new OneworldBSFNSpecSource(sessionID);
			System.out.print("OK! (" + specSource.getName() + ")\n");
			
			System.out.println("Getting BSFNMethod ... ");
			BSFNMethod bsfnMethod = specSource.getBSFNMethod("PurchaseOrderInit");
			ExecutableMethod ab = bsfnMethod.createExecutable();
			ab.setValue("cHeaderActionCode", "1");//add
	        ab.setValue("szProgramID", "EPROC");
	        ab.setValue("szPurchaseOrderPrOptVersion", "ZJDE0001");
	        if(InetAddress.getLocalHost().getHostName().length()>=10)
	        	ab.setValue("szComputerID", InetAddress.getLocalHost().getHostName().substring(0, 10));
	        else
	        	ab.setValue("szComputerID", InetAddress.getLocalHost().getHostName());
	        ab.setValue("szOrderCOmpany", SessionGetter.nullToEmptyString(po.getSzOrderCompany()).trim());
//	        ab.setValue("mnOrderNumber", String.valueOf(po.getMnOrderNumber())); //automatic
	        ab.setValue("mnOrderNumber", String.valueOf(nomor)); //automatic
	        ab.setValue("szOrderType", String.valueOf(po.getSzOrderType()));
	        ab.setValue("szOrderSuffix", "000");
	        ab.setValue("szBranchPlant", SessionGetter.nullToEmptyString(po.getSzBranchPlant().trim()));
	        ab.setValue("mnSupplierNumber", String.valueOf(po.getMnSupplierNumber()));//AN8
	        ab.setValue("mnShipToNumber", String.valueOf(po.getMnShipToNumber()));//SHAN
	        ab.setValue("cCurrencyMode", "D");
	        ab.setValue("szTransactionCurrencyCode", SessionGetter.nullToEmptyString(po.getSzTransactionCurrencyCode()));
	        ab.setValue("szDeliveryInstructions01", SessionGetter.nullToEmptyString(po.getSzDeliveryInstructions01()));
	        ab.setValue("szDeliveryInstructions02", SessionGetter.nullToEmptyString(po.getSzDeliveryInstructions02()));
	        ab.setValue("szRemark", po.getSzRemark());
	        ab.setValue("szDescription", po.getSzDescription());
	        ab.setValue("mnBuyerNumber", SessionGetter.nullToEmptyString(String.valueOf(po.getMnBuyerNumber())));
	        ab.setValue("mnCarrierNumber", SessionGetter.nullToEmptyString(String.valueOf(po.getMnCarrierNumber())));
	        ab.setValue("szPrintMessage", SessionGetter.nullToEmptyString(po.getSzPrintMessage()));
	        ab.setValue("szHoldCode", SessionGetter.nullToEmptyString(po.getSzHoldCode()));
	        ab.setValue("szPaymentTerms", SessionGetter.nullToEmptyString(po.getSzPaymentTerms()));
	        ab.setValue("cAIADocument", SessionGetter.nullToEmptyString(po.getSzPOEMBFInternalFlags()));
	        
	        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
	        ab.setValue("jdRequestedDate", sdf.format(po.getJdRequestedDate()));
	        ab.setValue("jdOrderDate", sdf.format(po.getJdOrderDate()));
	        ab.setValue("jdPromisedDate", sdf.format(po.getJdPromisedDate()));
	        BSFNExecutionWarning warn = ab.execute(sessionID);
	        
	        int i=1;
	        if(listDetail!=null && listDetail.size()>0){
	        	for(PODetail p:listDetail){
	        		//insert line
	    	        ExecutableMethod line = specSource.getBSFNMethod("PurchaseOrderLineProcess").createExecutable();
	    	        line.setValue("mnOrderNumber", String.valueOf(nomor));
//	    	        line.setValue("mnOrderNumber", ab.getValue("mnOrderNumber"));
	    	        line.setValue("szOrderCompany", ab.getValue("szOrderCOmpany"));
	    	        line.setValue("szOrderType", ab.getValue("szOrderType"));
	    	        line.setValue("mnJobNumber", ab.getValue("mnJobNumber"));
	    	        line.setValue("szDetailLineBranchPlant","       "+ab.getValue("szBranchPlant"));
	    	        line.setValue("szComputerID", ab.getValue("szComputerID"));
	                line.setValue("szOrderType", ab.getValue("szOrderType"));
	                line.setValue("cDetailActionCode", "1");
	                line.setValue("mnShipToNumber", ab.getValue("mnShipToNumber"));//SHAN
	                line.setValue("szOrderSuffix", "000");
	                line.setValue("mnProcessID", ab.getValue("mnProcessID"));
	                line.setValue("mnTransactionID", ab.getValue("mnTransactionID"));
	                line.setValue("mnSupplierNumber", ab.getValue("mnSupplierNumber"));
	                line.setValue("szLineType", p.getSzLineType().trim());
	                line.setValue("szUnformattedItemNumber", p.getSzUnformattedItemNumber().trim());
	    			line.setValue("mnIdentifierShortItem", String.valueOf(p.getIdentifierShortItem()));
	                line.setValue("mnQuantityOrdered", String.valueOf(p.getMnQuantityOrdered()));
	                DecimalFormat df = new DecimalFormat("###.##");
	                line.setValue("mnUnitPrice", String.valueOf(df.format(p.getMnUnitPrice())));
	                line.setValue("szLastStatus", "220");
	                line.setValue("szNextStatus", "280");
	                line.setValue("szTransactionUoM", p.getSzTransactionUoM().trim());
	                line.setValue("szPurchasingUoM", SessionGetter.nullToEmptyString(p.getSzPurchasingUoM()).trim());
	                line.setValue("szTransactionCurrencyCode", ab.getValue("szTransactionCurrencyCode"));
	                line.setValue("szDescription1", p.getSzDescription1().trim());
	                line.setValue("szDescription2", SessionGetter.nullToEmptyString(p.getSzDescription2()).trim());
	                line.setValue("cPriceOverrideFlag", "1");
	                line.setValue("cTaxable", p.getTaxable());
	                line.setValue("cSubledgerType", SessionGetter.nullToEmptyString(p.getSubledgerType()).trim());
	                line.setValue("szSubledger", SessionGetter.nullToEmptyString(p.getSzSubledger()).trim());
	                line.setValue("szPurchasingCategoryCode1", SessionGetter.nullToEmptyString(p.getSzPurchasingCategoryCode1()).trim());
	                line.setValue("szPurchasingCategoryCode2", SessionGetter.nullToEmptyString(p.getSzPurchasingCategoryCode2()).trim());
	                line.setValue("szPurchasingCategoryCode3", SessionGetter.nullToEmptyString(p.getSzPurchasingCategoryCode3()).trim());
	                line.setValue("szPurchasingCategoryCode4", SessionGetter.nullToEmptyString(p.getSzPurchasingCategoryCode4()).trim());
	                line.setValue("szGLClassCode", SessionGetter.nullToEmptyString(p.getSzGLClassCode()).trim());
	                line.setValue("jdGLDate", sdf.format(p.getJdGLDate()));
	                line.setValue("szAssetID", SessionGetter.nullToEmptyString(p.getSzAssetID()).trim());
	                line.setValue("szLocation", SessionGetter.nullToEmptyString(p.getSzLocation()).trim());
	                line.setValue("szLotNumber", SessionGetter.nullToEmptyString(p.getSzLotNumber()).trim());
	                line.setValue("szUnformattedAccountNumber", SessionGetter.nullToEmptyString(p.getSzUnformattedAccountNumber()).trim());
	                line.setValue("szPurchasingCostCenter", SessionGetter.nullToEmptyString(p.getSzPurchasingCostCenter()));
	                line.setValue("szObjectAccount", SessionGetter.nullToEmptyString(p.getSzObjectAccount()).trim());
	                line.setValue("szSubsidiary", SessionGetter.nullToEmptyString(p.getSzSubsidiary()).trim());
	                line.setValue("szWeightUoM", SessionGetter.nullToEmptyString(p.getSzWeightUoM()).trim());
	                line.setValue("szVolumeUoM", SessionGetter.nullToEmptyString(p.getSzVolumeUoM()).trim());
	                
	                //set line
	                line.setValue("mnOrderLineNumber", SessionGetter.nullObjectToEmptyString((p.getMnOrderLineNumber()/1000)));
	                
	                //set original
	                line.setValue("mnOriginalLineNumber", line.getValue("mnOrderLineNumber"));
	                line.setValue("szOriginalOrderCompany", line.getValue("szOrderCompany"));
	                line.setValue("szOriginalOrderNumber", SessionGetter.nullNumberToEmptyString(p.getSzOriginalOrderNumber()));
	                line.setValue("szOriginalOrderType", orderType);
	                
	                warn = line.execute(sessionID);
	                i++;
	                
	                //insert text
	                if(p.getSzElevation()!=null && p.getSzElevation().trim().length()>0){
	                	ExecutableMethod text = specSource.getBSFNMethod("GT4311_InsertGenericText").createExecutable();
	                	text.setValue("mnDocumentOrderInvoiceE", nomor);
	                	text.setValue("szCompanyKeyOrderNo", ab.getValue("szOrderCOmpany"));
	                	text.setValue("szOrderType", ab.getValue("szOrderType"));
	                	text.setValue("szOrderSuffix", "000");
	                	text.setValue("mnLineNumber", SessionGetter.nullObjectToEmptyString((p.getMnOrderLineNumber()/1000)).concat(".000"));
	                	String txt = p.getSzElevation().trim();
	                	if(txt!=null && txt.length()>61){
	                		txt = txt.substring(0, 60);
	                	}else{
	                		txt = "";
	                	}
	                	text.setValue("szKanbanTextString", txt);
	                	warn = text.execute(sessionID);
	                }
	        	}
	        }
	        
	        //end doc
            ExecutableMethod poeEndDoc = specSource.getBSFNMethod("PurchaseOrderCommit").createExecutable();
            poeEndDoc.setValue("mnJobNumber",ab.getValue("mnJobNumber"));
            poeEndDoc.setValue("mnOrderNumberAssigned",ab.getValue("mnOrderNumber"));
            poeEndDoc.setValue("szRelatedOrderType",ab.getValue("szOrderType"));
            poeEndDoc.setValue("szComputerID",ab.getValue("szComputerID"));
            poeEndDoc.setValue("mnProcessID",ab.getValue("mnProcessID"));
            poeEndDoc.setValue("mnTransactionID",ab.getValue("mnTransactionID"));
            warn=poeEndDoc.execute(sessionID);
            retMe = poeEndDoc.getValueString("mnOrderNumberAssigned").concat(po.getSzOrderType().trim()).concat(po.getSzOrderCompany().trim()).trim();
            transaction.commit();
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return retMe;
	}

	@SuppressWarnings("rawtypes")
	public static String getNextNumberAnggaran(String kantor, String kodeTipe,
			String century, String fiscal, UserSession session,
			Integer sessionID) throws Exception {
			String retMe = "";
			Map output = new HashMap();
			try {
				//execute begin doc
				OneworldTransaction transaction = session.createOneworldTransaction(true);
				transaction.begin();
				System.out.print("Reading BSFNSpecSource ... ");
				BSFNSpecSource specSource = new OneworldBSFNSpecSource(sessionID);
				System.out.print("OK! (" + specSource.getName() + ")\n");
				
				System.out.println("Getting BSFNMethod ... ");
				BSFNMethod bsfnMethod = specSource.getBSFNMethod("X0010GetNextNumber");
				ExecutableMethod ab = bsfnMethod.createExecutable();
				ab.setValue("szCompanyKey", kantor);
				ab.setValue("szDocumentType", kodeTipe);
				ab.setValue("mnCentury", century);
				ab.setValue("mnFiscalYear1", fiscal);
				
				BSFNExecutionWarning warn = ab.execute(sessionID);
				
				output = ab.getValues();
	        	Set ss = output.keySet();
	        	int i = 0;
	        	for(Object o:output.values()){
	        		if(ss.toArray()[i].toString().equalsIgnoreCase("mnNextNumber001")){
	        			retMe = String.valueOf(o);
	        			break;
	        		}
	        		i++;
	        	}
	        	transaction.commit();
				
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return retMe;
	}
	
	public static void updateTanggalKontrak(CtrContractHeader contract, String nomorOp, String tipeData, String kantor) throws Exception{
		Session ses = HibernateUtil.getSessionJde();
		try {
			ses.beginTransaction();
			
			String query = "update "+SessionGetter.valueProperty("jde.schema")+".f4301 set phdrqj='"+JulianToGregorianViceVersa.getJulian7FromDate(contract.getTglPenyerahan())+"' , "
					+ " phtrdj='"+JulianToGregorianViceVersa.getJulian7FromDate(contract.getContractStart())+"' , phpddj='"+JulianToGregorianViceVersa.getJulian7FromDate(contract.getContractEnd())+"' "
					+ " where phdoco='"+nomorOp+"' and phdcto='"+tipeData+"' and phkcoo='"+kantor+"'";
			
			Query q = ses.createSQLQuery(query);
			q.executeUpdate();
			
			ses.getTransaction().commit();
			ses.clear();
			ses.close();
		} catch (Exception e) {
			ses.getTransaction().rollback();
			ses.clear();
			ses.close();
			throw e;
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static List<Object> generateAnggaranFromON(PrcMainHeader ppm, Session session) throws Exception{
		List retMe = new ArrayList();
		Session ses = HibernateUtil.getSessionJde();
		try {
			String tmp [] = ppm.getPpmNomorPr().trim().split("ZMN");
			String doco = tmp[0].trim();
			String kaco = tmp[1].trim();
			
			/** ambil kode ON **/
			String query = "select distinct pdocto, pdoorn , pdokco from "+SessionGetter.valueProperty("jde.schema")+".f4311 where trim(pddoco) ='"+doco+"' and trim(pddcto)='OR' and trim(pdkcoo) ='"+kaco+"'";
			Query q = ses.createSQLQuery(query);
			Object noOn = q.uniqueResult();
			if(noOn!=null){
				Object o[] = (Object[])noOn;
				query = "select distinct ydaid, ydaa2 from "+SessionGetter.valueProperty("jde.schema")+".f55ck1a "
						+ " where trim(yddoco) ='"+String.valueOf(o[1]).trim()+"' and trim(yddcto)='"+String.valueOf(o[0]).trim()+"' and trim(ydkcoo)='"+String.valueOf(o[2]).trim()+"'";
				
				q =ses.createSQLQuery(query);
				retMe= q.list(); 
			}
		} catch (Exception e) {
			ses.clear();
			ses.close();
			throw e;
		}
		
		return retMe;
	}

	@SuppressWarnings("rawtypes")
	public static String getRekeningAnggaran(String kAID) throws Exception {
		String retMe = "";
		Session ses =  HibernateUtil.getSessionJde();
		try {
			String query = "select glaid, glexa, glaa, glmcu, globj, glsub from "+SessionGetter.valueProperty("jde.schema")+".f550911a where glaid like '%"+kAID.trim()+"%'";
			Query q = ses.createSQLQuery(query);
			List lst = q.list();
			Object [] o =(Object[]) lst.get(0);
			String costCenter = String.valueOf(o[3]!=null?o[3]:"");
			String rekening = String.valueOf(o[4]!=null?o[4]:"");
			String subRekening = String.valueOf(o[5]!=null?o[5]:"");
			
			retMe = costCenter.concat(".").concat(rekening).concat(".").concat(subRekening);
			retMe = String.valueOf(o[1]).concat("<br>").concat("Rekening : "+retMe);
			ses.clear();
			ses.close();
		} catch (Exception e) {
			ses.clear();
			ses.close();
			throw e;
		}
		return retMe;
	}
	
	
}
