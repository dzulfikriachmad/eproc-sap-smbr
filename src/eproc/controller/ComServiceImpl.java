package eproc.controller;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;

import com.orm.model.ComCatalog;
import com.orm.model.ComGroup;
import com.orm.tools.GenericDao;

public class ComServiceImpl extends GenericDao implements ComService {

	@SuppressWarnings("unchecked")
	@Override
	public List<ComCatalog> listComCatalog() {
		DetachedCriteria dc = DetachedCriteria.forClass(ComCatalog.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		
		return (List<ComCatalog>) getListByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ComGroup> listComGroup(Object object) throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(ComGroup.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		return (List<ComGroup>) getListByDetachedCiteria(dc);
	}

}
