package eproc.controller;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.eproc.utils.EmailSessionBean;
import com.eproc.utils.SessionGetter;
import com.eproc.utils.UploadUtil;
import com.orm.model.AdmDelegasi;
import com.orm.model.AdmDept;
import com.orm.model.AdmDistrict;
import com.orm.model.AdmJenis;
import com.orm.model.AdmProses;
import com.orm.model.AdmProsesHirarki;
import com.orm.model.AdmUom;
import com.orm.model.AdmUser;
import com.orm.model.ComGroup;
import com.orm.model.HistMainComment;
import com.orm.model.OrDetail;
import com.orm.model.OrHeader;
import com.orm.model.OrHeaderId;
import com.orm.model.PrcMainHeader;
import com.orm.model.PrcMainItem;
import com.orm.model.PrcManualDoc;
import com.orm.tools.GenericDao;
import com.orm.tools.HibernateUtil;

public class OrServiceImpl extends GenericDao implements OrService{

	@Override
	public int countActiveOr(Object object) throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(OrHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setProjection(Projections.rowCount());
		dc.add(Restrictions.or(Restrictions.isNull("status"), Restrictions.eq("status", 0)));
		if(object instanceof OrHeader){
			OrHeader oh = (OrHeader)object;
			if(oh.getId()!=null){
				if(oh.getId().getNomorOr()!=null){
					dc.add(Restrictions.like("id.nomorOr", oh.getId().getNomorOr()));
				}
				if(oh.getId().getHeaderSite()!=null){
					dc.add(Restrictions.like("id.headerSite", oh.getId().getHeaderSite(),MatchMode.ANYWHERE));
				}
			}
		}
		Object obj = getEntityByDetachedCiteria(dc);
		return Integer.valueOf(String.valueOf(obj)).intValue();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<OrHeader> listActiveOrHeader(Object object, int page, int i)
			throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(OrHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.add(Restrictions.or(Restrictions.isNull("status"), Restrictions.eq("status", 0)));
		if(object instanceof OrHeader){
			OrHeader oh = (OrHeader)object;
			if(oh.getId()!=null){
				if(oh.getId().getNomorOr()!=null){
					dc.add(Restrictions.like("id.nomorOr", oh.getId().getNomorOr()));
				}
				if(oh.getId().getHeaderSite()!=null){
					dc.add(Restrictions.like("id.headerSite", oh.getId().getHeaderSite(),MatchMode.ANYWHERE));
				}
			}
		}
		return (List<OrHeader>) getListByDetachedCiteriaPaging(dc, page, i);
	}
	
	@SuppressWarnings("unchecked")
	public List<OrHeader> listActiveOrHeader(Object object)
			throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(OrHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.add(Restrictions.or(Restrictions.isNull("status"), Restrictions.eq("status", 0)));
		if(object instanceof OrHeader){
			OrHeader oh = (OrHeader)object;
			if(oh.getId()!=null){
				if(oh.getId().getNomorOr()!=null){
					dc.add(Restrictions.like("id.nomorOr", oh.getId().getNomorOr()));
				}
				if(oh.getId().getHeaderSite()!=null){
					dc.add(Restrictions.like("id.headerSite", oh.getId().getHeaderSite(),MatchMode.ANYWHERE));
				}
			}
		}
		dc.addOrder(Order.desc("createdDate"));
		return (List<OrHeader>) getListByDetachedCiteria(dc);
	}
	
	@SuppressWarnings("unchecked")
	public AdmUser getAdmUserByUsername(String string) throws Exception{
		DetachedCriteria dc = DetachedCriteria.forClass(AdmUser.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.add(Restrictions.like("username", string));
		return (AdmUser) getEntityByDetachedCiteria(dc);
	}

	@Override
	public OrHeader getOrHeaderById(OrHeaderId id) throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(OrHeader.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.add(Restrictions.idEq(id));
		return (OrHeader) getEntityByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<OrDetail> getOrDetailByOrHeader(OrHeader orHeader)
			throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(OrDetail.class);
		dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		dc.setFetchMode("orHeader", FetchMode.JOIN);
		dc.add(Restrictions.eq("orHeader", orHeader));
		dc.addOrder(Order.asc("id.lineId"));
		return (List<OrDetail>) getListByDetachedCiteria(dc);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ComGroup> getListComGrup(Object object) throws Exception {
		DetachedCriteria crit = DetachedCriteria.forClass(ComGroup.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		return (List<ComGroup>) getListByDetachedCiteria(crit);
	}

	@Override
	public void generatePr(OrHeader orHeader, PrcMainHeader prcMainHeader)
			throws Exception {
		try {
			startTransaction();
			OrHeader oh = getOrHeaderById(orHeader.getId());
			List<OrDetail> lod = getOrDetailByOrHeader(oh);
			List<String> namaPembuatPr = new ArrayList<String>();
			String pembuatOrJasa;
			AdmJenis jenis = SessionGetter.getAdmJenisById(prcMainHeader.getPpmJenis().getId());
			//create PR HEADER
			Double ee = 0.0;
			//calculate EE
			if(lod!=null && lod.size()>0){
				for(OrDetail o:lod){
					ee = ee+o.getHargaSatuan();
					if(o.getLineType().trim().equalsIgnoreCase("J")){
						namaPembuatPr.add(oh.getPembuatOrJasa());
					}else if(o.getLineType().trim().equalsIgnoreCase("S")){
						prcMainHeader.setPembuatPr(oh.getNamaPembuatOm());
					}
				}
			}
			
			AdmProsesHirarki ap = SessionGetter.getHirarkiProsesByDistrict(new AdmProses(102), 1, SessionGetter.getUser().getAdmEmployee().getAdmDept().getAdmDistrict(), 0.0);
//			prcMainHeader.setAdmDept(getAdmDeptByDeptCode(orHeader.getUnitKerja()));
			prcMainHeader.setAdmDept(SessionGetter.getUser().getAdmEmployee().getAdmDept());
			prcMainHeader.setAdmDistrict(SessionGetter.getUser().getAdmEmployee().getAdmDept().getAdmDistrict());
			prcMainHeader.setAdmProses(new AdmProses(102));//langsung ke OE
			prcMainHeader.setAdmRole(ap.getAdmRoleByCurrentPos());
			prcMainHeader.setAdmUserByPpmCurrentUser(SessionGetter.getEmployeeByRole(ap.getAdmRoleByCurrentPos()).getAdmUser());
			prcMainHeader.setLokasiPengadaan(prcMainHeader.getAdmDistrict());
			prcMainHeader.setPpmJenis(jenis.getParentId());//1 = Barang 2 = Jasa
			prcMainHeader.setPpmJenisDetail(jenis);
			prcMainHeader.setPpmJumlahEe(new BigDecimal(ee));
			prcMainHeader.setPpmNomorPr(orHeader.getId().getNomorOr().toString().concat(" ").concat(orHeader.getId().getOrH().concat(" ").concat(orHeader.getId().getHeaderSite())));
			prcMainHeader.setPpmProsesLevel(1);
			prcMainHeader.setPpmTipe(1);
			prcMainHeader.setAttachment(oh.getAttachment());
			prcMainHeader.setMataUang(oh.getMataUang());
			prcMainHeader.setFileName(oh.getFileName());
			
			if(prcMainHeader.getPembuatPr() == null || prcMainHeader.getPembuatPr().equalsIgnoreCase("")){
				pembuatOrJasa = namaPembuatPr.get(0).trim();
				AdmUser userOrJasa = getAdmUserByUsername(pembuatOrJasa);
				prcMainHeader.setPembuatPr(userOrJasa.getCompleteName());
			}
			
			
			AdmDistrict kantor = null;
//			if(prcMainHeader.getPpmNomorPrSap().contains("1201")){//palembang
//				kantor = new AdmDistrict(104);
//			}else if(prcMainHeader.getPpmNomorPrSap().contains("1202") || prcMainHeader.getPpmNomorPrSap().contains("11202")){//baturaja
//				kantor = new AdmDistrict(107);
//			}else if(prcMainHeader.getPpmNomorPrSap().contains("1204")){//panjang
//				kantor = new AdmDistrict(110);
//			}else if(prcMainHeader.getPpmNomorPrSap().contains("1203")){//baturaja2
//				kantor = new AdmDistrict(111);
//			}
			prcMainHeader.setDistrictUser(kantor);
			
			//get satuan kerja by code
			AdmDept satker = new AdmServiceImpl().getAdmDeptByCode(oh.getKodeSatker());
			prcMainHeader.setSatkerPeminta(satker);
			saveEntity(prcMainHeader);
			
			//create ITEM
			if(lod!=null && lod.size()>0){
				for(OrDetail o:lod){
					if(o.getKodeBarang()!=null){
						PrcMainItem item = new PrcMainItem();
						item.setAdmUom(getAdmUomByCode(o.getSatuan()));
						//item.setGroupCode(prcMainHeader.getGroupCode());
						item.setItemCode(o.getKodeBarang());
						item.setItemCostCenter(o.getAccuountNumber());
						item.setItemDescription(o.getNamaBarang()+o.getNamaBarangExt());
						item.setItemPrice(new BigDecimal(o.getHargaSatuan()));
						item.setItemQuantity(new BigDecimal(o.getJumlah()));
						item.setItemTax(new BigDecimal(0));
						item.setItemTipe(o.getLineType().equals("S")?"M":"S");
						item.setItemTotalPrice(item.getItemQuantity().multiply(item.getItemPrice()));
						item.setPrcMainHeader(prcMainHeader);
						item.setAttachment(o.getAttachment());
						item.setFileName(o.getFileName());
						saveEntity(item);
					}
				}
			}
			//insert History
			
			HistMainComment comment = new HistMainComment();
			comment.setPosisi(SessionGetter.getUser().getAdmEmployee().getEmpStructuralPos());
			comment.setPrcMainHeader(prcMainHeader);
			comment.setProses(102);
			comment.setUsername(SessionGetter.getUser().getCompleteName());
			comment.setProsesAksi("Generate PR ke OE");
			if(oh.getCreatedDate()!=null){
				comment.setCreatedDate(oh.getCreatedDate());
			}else{
				comment.setCreatedDate(new Date());
			}
			comment.setUpdatedDate(new Date());
			comment.setAksi("Generate PR ke OE");
			comment.setComment("Generate PR ke OE");
			saveEntity(comment);
			
			//cek delegasi 
			AdmDelegasi del = new AdmServiceImpl().getDelegasiByUserDari(prcMainHeader.getAdmUserByPpmCurrentUser());
			if(del!=null){
				prcMainHeader.setAdmUserByPpmCurrentUser(del.getKepada());
				saveEntity(prcMainHeader);
			}
			comment = new HistMainComment();
			comment.setPosisi(prcMainHeader.getAdmUserByPpmCurrentUser().getAdmEmployee().getEmpStructuralPos());
			comment.setPrcMainHeader(prcMainHeader);
			comment.setProses(102);
			comment.setUsername(prcMainHeader.getAdmUserByPpmCurrentUser().getCompleteName());
			comment.setProsesAksi(SessionGetter.getPropertiesValue("oe.1", null));
			saveEntity(comment);
			
			//send mail
			Object o[] = new Object[2];
			o[0] = prcMainHeader.getPpmNomorPr();
			o[1] = comment.getProsesAksi(); 
			String message = SessionGetter.getPropertiesValue("eproc.email.notif.internal", o);
			EmailSessionBean.sendMail("eProc PT. Semen Baturaja Persero ", message, prcMainHeader.getAdmUserByPpmCurrentUser().getAdmEmployee().getEmpEmail());
			
			//update orheader status
			orHeader.setStatus(1);//completed
			saveEntity(orHeader);
			
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			e.printStackTrace();
			throw e;
		}
		
	}
	
	@Override
	public void generatePrSap(OrHeader orHeader, PrcMainHeader prcMainHeader)
			throws Exception {
		try {
			startTransaction();
			OrHeader oh = getOrHeaderById(orHeader.getId());
			List<OrDetail> lod = getOrDetailByOrHeader(oh);
			List<String> namaPembuatPr = new ArrayList<String>();
			String pembuatOrJasa;
			AdmJenis jenis = SessionGetter.getAdmJenisById(prcMainHeader.getPpmJenis().getId());
			//create PR HEADER
			Double ee = 0.0;
			if(lod!=null && lod.size()>0){
				for(OrDetail o:lod){
					ee = ee+o.getHargaSatuan();
				}
			}
			AdmProsesHirarki ap = SessionGetter.getHirarkiProsesByDistrict(new AdmProses(102), 1, SessionGetter.getUser().getAdmEmployee().getAdmDept().getAdmDistrict(), 0.0);
//			prcMainHeader.setAdmDept(getAdmDeptByDeptCode(orHeader.getUnitKerja()));
			prcMainHeader.setAdmDept(SessionGetter.getUser().getAdmEmployee().getAdmDept());
			prcMainHeader.setAdmDistrict(SessionGetter.getUser().getAdmEmployee().getAdmDept().getAdmDistrict());
			prcMainHeader.setAdmProses(new AdmProses(102));//langsung ke OE
			prcMainHeader.setAdmRole(ap.getAdmRoleByCurrentPos());
			prcMainHeader.setAdmUserByPpmCurrentUser(SessionGetter.getEmployeeByRole(ap.getAdmRoleByCurrentPos()).getAdmUser());
			
			prcMainHeader.setLokasiPengadaan(prcMainHeader.getAdmDistrict());			
			prcMainHeader.setPpmJenis(jenis.getParentId());//1 = Barang 2 = Jasa
			prcMainHeader.setPpmJenisDetail(jenis);
			prcMainHeader.setPpmJumlahEe(new BigDecimal(ee));
			prcMainHeader.setPpmNomorPr(orHeader.getId().getNomorOr().toString().concat(" ").concat(orHeader.getId().getOrH().concat(" ").concat(orHeader.getId().getHeaderSite())));
			prcMainHeader.setPpmNomorPr(String.valueOf(oh.getId().getNomorOr()));
			prcMainHeader.setPpmNomorPrSap(orHeader.getId().getNomorOr().toString().concat(" ").concat(orHeader.getId().getOrH().concat(" ").concat(orHeader.getId().getHeaderSite())));
			prcMainHeader.setPpmProsesLevel(1);
			prcMainHeader.setPpmTipe(1);
			prcMainHeader.setAttachment(oh.getAttachment());
			prcMainHeader.setMataUang(oh.getMataUang());
			prcMainHeader.setFileName(oh.getFileName() );
			prcMainHeader.setPembuatPr(oh.getPembuatOm());
			prcMainHeader.setNamaPembuatPr(oh.getNamaPembuatOm());
			/*if(prcMainHeader.getPpmJenisDetail()!=null) {
				AdmUser manager = SessionGetter.getUserByJenisKodeSatker(prcMainHeader.getPpmJenisDetail(),prcMainHeader.getPpmJumlahOe().doubleValue(), 1).getAdmUser();
				prcMainHeader.setAdmUserByPpmCurrentUser(manager);
			}else {
				throw new Exception("Untuk menyesuaikan SOP yang baru, Jenis Permintaan harus diupdate agar bisa melanjutkan");
			}*/
//			if(prcMainHeader.getPembuatJde() == null || prcMainHeader.getPembuatJde().equalsIgnoreCase("")){
//				pembuatOrJasa = namaPembuatPr.get(0).trim();
//				AdmUser userOrJasa = getAdmUserByUsername(pembuatOrJasa);
//				prcMainHeader.setPembuatJde(userOrJasa.getCompleteName());
//			}
			
			
			AdmDistrict kantor = null;
			if(prcMainHeader.getPpmNomorPrSap().contains("1201")){//palembang
				kantor = new AdmDistrict(104);
			}else if(prcMainHeader.getPpmNomorPrSap().contains("1202") || prcMainHeader.getPpmNomorPrSap().contains("11202")){//baturaja
				kantor = new AdmDistrict(107);
			}else if(prcMainHeader.getPpmNomorPrSap().contains("1204")){//panjang
				kantor = new AdmDistrict(110);
			}else if(prcMainHeader.getPpmNomorPrSap().contains("1203")){//baturaja2
				kantor = new AdmDistrict(111);
			}
			prcMainHeader.setDistrictUser(kantor);
			
			//get satuan kerja by code
			AdmDept satker = new AdmServiceImpl().getAdmDeptByCode(oh.getPurchasingGroup());
			prcMainHeader.setSatkerPeminta(satker);
			saveEntity(prcMainHeader);
			
			//create ITEM
			int cek = 0;
			if(lod!=null && lod.size()>0){
				for(OrDetail o:lod){
					//if(o.getKodeBarang()!=null){
						PrcMainItem item = new PrcMainItem();
						item.setAdmUom(getAdmUomByCode(o.getSatuan()));
						//item.setGroupCode(prcMainHeader.getGroupCode());
						item.setItemCode(o.getKodeBarang());
						item.setItemCostCenter(o.getAccuountNumber());
						item.setItemDescription(o.getNamaBarang());
						
						if(o.getItemCategory().equals("9")) {
							item.setItemPrice((o.getServicePricePr()));
							cek++;
						}else if(o.getItemCategory().equals("0")){
							item.setItemPrice(new BigDecimal(o.getHargaSatuan()));
						}else {
							if(prcMainHeader.getPpmJenis().getId() == 2) {
								item.setItemPrice((o.getServicePricePr()));
								cek++;
							}else {
								item.setItemPrice(new BigDecimal(o.getHargaSatuan()));
							}
						}
						/*if(prcMainHeader.getPpmJenis().getId() == 2) {
							item.setItemPrice((o.getServicePricePr()));
							cek++;
						}else {
							item.setItemPrice(new BigDecimal(o.getHargaSatuan()));
						}*/

						item.setItemQuantity(new BigDecimal(o.getJumlah()));
						item.setItemTax(new BigDecimal(0));
						//item.setItemTipe(o.getLineType().equals("S")?"M":"S");
						item.setItemTotalPrice(item.getItemQuantity().multiply(item.getItemPrice()));
						item.setPrcMainHeader(prcMainHeader);
						item.setAttachment(o.getAttachment());
						item.setFileName(o.getFileName());
						item.setLineId(o.getId().getLineId());
						if(item.getLineId()==0 || item.getLineId()==null) {
							item.setLineId(o.getLine());	
						}
						System.out.println("LINE>>" + item.getLineId());
						item.setItemCategory(o.getItemCategory());
						item.setMaterialGroup(o.getMaterialGroup());
						item.setStorageLocation(o.getStorageLocation());
						item.setPurchasingGroup(oh.getPurchasingGroup());
						item.setDeliveryDate(o.getDeliveryDate());
						item.setKeterangan(o.getDescriptionText());
						saveEntity(item);
					}
				}
			//}
			
			//Jika jenis permintaan tidak sesuai, maka proses tidak bisa dilanjutkan
			if(cek>0) {
				if(prcMainHeader.getPpmJenis().getId() != 2) {
					throw new Exception("Jenis Permintaan tidak sesuai");
				}
			}
			//insert History
			HistMainComment comment = new HistMainComment();
			comment.setPosisi(SessionGetter.getUser().getAdmEmployee().getEmpStructuralPos());
			comment.setPrcMainHeader(prcMainHeader);
			comment.setProses(102);
			comment.setUsername(SessionGetter.getUser().getCompleteName());
			comment.setProsesAksi("Generate PR ke OE");
			if(oh.getCreatedDate()!=null){
				comment.setCreatedDate(oh.getCreatedDate());
			}else{
				comment.setCreatedDate(new Date());
			}
			comment.setUpdatedDate(new Date());
			comment.setAksi("Generate PR ke OE");
			comment.setComment("Generate PR ke OE");
			saveEntity(comment);
			
			//cek delegasi 
			AdmDelegasi del = new AdmServiceImpl().getDelegasiByUserDari(prcMainHeader.getAdmUserByPpmCurrentUser());
			if(del!=null){
				prcMainHeader.setAdmUserByPpmCurrentUser(del.getKepada());
				saveEntity(prcMainHeader);
			}
			comment = new HistMainComment();
			comment.setPosisi(prcMainHeader.getAdmUserByPpmCurrentUser().getAdmEmployee().getEmpStructuralPos());
			comment.setPrcMainHeader(prcMainHeader);
			comment.setProses(102);
			comment.setUsername(prcMainHeader.getAdmUserByPpmCurrentUser().getCompleteName());
			comment.setProsesAksi(SessionGetter.getPropertiesValue("oe.1", null));
			saveEntity(comment);
			
			//send mail
			Object o[] = new Object[2];
			o[0] = prcMainHeader.getPpmNomorPr();
			o[1] = comment.getProsesAksi(); 
			String message = SessionGetter.getPropertiesValue("eproc.email.notif.internal", o);
			EmailSessionBean.sendMail("eProc PT. Semen Baturaja Persero ", message, prcMainHeader.getAdmUserByPpmCurrentUser().getAdmEmployee().getEmpEmail());
			
			//update orheader status
			orHeader.setStatus(1);//completed
			saveEntity(orHeader);
			
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			e.printStackTrace();
			throw e;
		}
		
	}
	
	@Override
	public void prManualDispatcher(OrHeader orHeader, PrcMainHeader prcMainHeader, List<File> prcManDocument,
			List<String> prcManDocumentCategory, HistMainComment histMainComment, List<String> prcManDocumentDescription,
			List<String> prcManDocumentFileName, File docsKomentar,
			String docsKomentarFileName)
			throws Exception {
		try {
			startTransaction();
			OrHeader oh = getOrHeaderById(orHeader.getId());
			List<OrDetail> lod = getOrDetailByOrHeader(oh);
			
			AdmJenis jenis = SessionGetter.getAdmJenisById(prcMainHeader.getPpmJenis().getId());
			//create PR HEADER
			Double ee = 0.0;
			if(lod!=null && lod.size()>0){
				for(OrDetail o:lod){
					ee = ee+o.getHargaSatuan();
				}
			}

			prcMainHeader.setAdmDept(SessionGetter.getUser().getAdmEmployee().getAdmDept());
			prcMainHeader.setAdmDistrict(SessionGetter.getUser().getAdmEmployee().getAdmDept().getAdmDistrict());
			prcMainHeader.setAdmProses(null);//langsung ke OE
			
			prcMainHeader.setLokasiPengadaan(prcMainHeader.getAdmDistrict());			
			prcMainHeader.setPpmJenis(jenis.getParentId());//1 = Barang 2 = Jasa
			prcMainHeader.setPpmJenisDetail(jenis);
			prcMainHeader.setPpmJumlahEe(new BigDecimal(ee));
			prcMainHeader.setPpmNomorPr(orHeader.getId().getNomorOr().toString().concat(" ").concat(orHeader.getId().getOrH().concat(" ").concat(orHeader.getId().getHeaderSite())));
			prcMainHeader.setPpmNomorPr(String.valueOf(oh.getId().getNomorOr()));
			prcMainHeader.setPpmNomorPrSap(orHeader.getId().getNomorOr().toString().concat(" ").concat(orHeader.getId().getOrH().concat(" ").concat(orHeader.getId().getHeaderSite())));
			prcMainHeader.setPpmTipe(1);
			prcMainHeader.setAttachment(oh.getAttachment());
			prcMainHeader.setMataUang(oh.getMataUang());
			prcMainHeader.setFileName(oh.getFileName() );
			prcMainHeader.setPembuatPr(oh.getPembuatOm());
			prcMainHeader.setNamaPembuatPr(oh.getNamaPembuatOm());
			prcMainHeader.setPpmProsesLevel(-100);//pengadaan manual
			prcMainHeader.setAdmUserByPpmCurrentUser(null);
			prcMainHeader.setAdmRole(null);
			
			
			AdmDistrict kantor = null;
			if(prcMainHeader.getPpmNomorPrSap().contains("1201")){//palembang
				kantor = new AdmDistrict(104);
			}else if(prcMainHeader.getPpmNomorPrSap().contains("1202") || prcMainHeader.getPpmNomorPrSap().contains("11202")){//baturaja
				kantor = new AdmDistrict(107);
			}else if(prcMainHeader.getPpmNomorPrSap().contains("1204")){//panjang
				kantor = new AdmDistrict(110);
			}else if(prcMainHeader.getPpmNomorPrSap().contains("1203")){//baturaja2
				kantor = new AdmDistrict(111);
			}
			prcMainHeader.setDistrictUser(kantor);
			
			//get satuan kerja by code
			AdmDept satker = new AdmServiceImpl().getAdmDeptByCode(oh.getPurchasingGroup());
			prcMainHeader.setSatkerPeminta(satker);
			saveEntity(prcMainHeader);
			
			//create ITEM
			int cek = 0;
			if(lod!=null && lod.size()>0){
				for(OrDetail o:lod){
					//if(o.getKodeBarang()!=null){
						PrcMainItem item = new PrcMainItem();
						item.setAdmUom(getAdmUomByCode(o.getSatuan()));
						//item.setGroupCode(prcMainHeader.getGroupCode());
						item.setItemCode(o.getKodeBarang());
						item.setItemCostCenter(o.getAccuountNumber());
						item.setItemDescription(o.getNamaBarang());
						
						if(o.getItemCategory().equals("9")) {
							item.setItemPrice((o.getServicePricePr()));
							cek++;
						}else if(o.getItemCategory().equals("0")){
							item.setItemPrice(new BigDecimal(o.getHargaSatuan()));
						}else {
							if(prcMainHeader.getPpmJenis().getId() == 2) {
								item.setItemPrice((o.getServicePricePr()));
								cek++;
							}else {
								item.setItemPrice(new BigDecimal(o.getHargaSatuan()));
							}
						}
						/*if(prcMainHeader.getPpmJenis().getId() == 2) {
							item.setItemPrice((o.getServicePricePr()));
							cek++;
						}else {
							item.setItemPrice(new BigDecimal(o.getHargaSatuan()));
						}*/

						item.setItemQuantity(new BigDecimal(o.getJumlah()));
						item.setItemTax(new BigDecimal(0));
						//item.setItemTipe(o.getLineType().equals("S")?"M":"S");
						item.setItemTotalPrice(item.getItemQuantity().multiply(item.getItemPrice()));
						item.setPrcMainHeader(prcMainHeader);
						item.setAttachment(o.getAttachment());
						item.setFileName(o.getFileName());
						item.setLineId(o.getId().getLineId());
						if(item.getLineId()==0 || item.getLineId()==null) {
							item.setLineId(o.getLine());	
						}
						System.out.println("LINE>>" + item.getLineId());
						item.setItemCategory(o.getItemCategory());
						item.setMaterialGroup(o.getMaterialGroup());
						item.setStorageLocation(o.getStorageLocation());
						item.setPurchasingGroup(oh.getPurchasingGroup());
						item.setDeliveryDate(o.getDeliveryDate());
						item.setKeterangan(o.getDescriptionText());
						saveEntity(item);
					}
				}
					
			//upload dokumen
			if(SessionGetter.isNotNull(prcManDocument)){
				int i = 0;
				for(File f:prcManDocument){
					PrcManualDoc doc = new PrcManualDoc();
					doc.setPrcMainHeader(prcMainHeader);
					doc.setDocCategory(prcManDocumentCategory.get(i));
					doc.setDocName(prcManDocumentDescription.get(i));
					doc.setDocPath(UploadUtil.uploadDocsFile(f, prcManDocumentFileName.get(i)));
					saveEntity(doc);
					i++;
				}
			}
			

			//insert history
			HistMainComment comment = new HistMainComment();
			comment.setAksi("Pengadaan Manual");
			comment.setComment(histMainComment.getComment());
			comment.setPosisi(SessionGetter.getUser().getAdmEmployee().getEmpStructuralPos());
			comment.setPrcMainHeader(prcMainHeader);
			comment.setProses(null);
			comment.setProsesAksi("Pengadaan Manual");
			comment.setUpdatedDate(new Date());
			comment.setUsername(SessionGetter.getUser().getCompleteName());;
			comment.setDocument(histMainComment.getDocument());
			saveEntity(comment);
			//update orheader status
			orHeader.setStatus(1);//completed
			saveEntity(orHeader);
			
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			e.printStackTrace();
			throw e;
		}
		
	}

	private AdmUom getAdmUomByCode(String satuan) {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmUom.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("uomName", satuan.trim()));
		AdmUom retMe = (AdmUom) getEntityByDetachedCiteria(crit);
		if(retMe==null){
			Session ses = HibernateUtil.getSession();
			ses.beginTransaction();
			AdmUom uom =  new AdmUom();
			uom.setUomName(satuan.trim());
			uom.setUomDescription(satuan);
			ses.saveOrUpdate(uom);
			ses.flush();
			ses.getTransaction().commit();
			ses.clear();
			ses.close();
			retMe =  uom;
		}
		return retMe;
	}

	private AdmDept getAdmDeptByDeptCode(String unitKerja) {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmDept.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.eq("deptCode", unitKerja));
		return (AdmDept) getEntityByDetachedCiteria(crit);
	}

	@Override
	public void batalOR(OrHeader orHeader, PrcMainHeader prcMainHeader)
			throws Exception {
		try {
			startTransaction();
			OrHeader oh = getOrHeaderById(orHeader.getId());
			//update orheader status
			oh.setStatus(1);//completed or di batalkan
			saveEntity(oh);
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			e.printStackTrace();
			throw e;
		}
	}
	
	@Override
	public List<AdmJenis> getListAdmJenis(String itemCategory){
		DetachedCriteria crit = DetachedCriteria.forClass(AdmJenis.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		if(itemCategory.equals("0")) {
			crit.add(Restrictions.eq("parentId.id", 1));
		}else if(itemCategory.equals("9")) {
			crit.add(Restrictions.eq("parentId.id", 2));
		}
		crit.add(Restrictions.and(Restrictions.ne("id", 1), Restrictions.ne("id", 2)));
	
		return (List<AdmJenis>) getListByDetachedCiteria(crit);
	}
	
	@Override
	public void getDocument(OrHeader orHeader, List<OrDetail> listOrDetail) throws IOException{
		if(orHeader.getAttachment()!=null) {
			UploadUtil.uploadDocsFileSAP(orHeader.getAttachment());
		}
		
		for(OrDetail detail:listOrDetail) {
			System.out.println(detail.getAttachment());
			if(detail.getAttachment()!=null) {
				UploadUtil.uploadDocsFileSAP(detail.getAttachment());
			}
		}
		
	}


}
