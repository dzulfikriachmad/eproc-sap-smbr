package id.co.adw.smbr.api.dto;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;


/**
 * 
 * @author Root-arief chris pramudyo
 * @date 2017-Apr-12 2:08:15 PM
 * @version 1.0
 *
 */

public class AdmCurrencyDto{
	private int id;
	private String currencyCode;
	private String currencyName;
	private BigDecimal currentKurs;
	private Date createdDate;
	private String createdBy;
	private Date updatedDate;
	private String updatedBy;
}
