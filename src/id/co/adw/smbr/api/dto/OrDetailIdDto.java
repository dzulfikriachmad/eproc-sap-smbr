package id.co.adw.smbr.api.dto;
import javax.persistence.Column;


public class OrDetailIdDto {
	private Integer nomorOr;
	private String orH;
	private String headerSite;
	private Integer lineId;
}
