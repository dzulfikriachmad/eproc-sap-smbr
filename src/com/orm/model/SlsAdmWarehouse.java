package com.orm.model;

// Generated Feb 26, 2012 1:56:14 PM by Hibernate Tools 3.4.0.Beta1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * SlsAdmWarehouse generated by hbm2java
 */
@Entity
@Table(name = "SLS_ADM_WAREHOUSE")
public class SlsAdmWarehouse implements java.io.Serializable {

	private int id;
	private AdmDistrict admDistrict;
	private String warehouseName;
	private String warehouseDescription;
	private String warehouseAddress;
	private String warehousePhoto;
	private Date createdDate;
	private String createdBy;
	private Date updatedDate;
	private String updatedBy;
	private Set<SlsProductStock> slsProductStocks = new HashSet<SlsProductStock>(
			0);
	private Set<SlsProduct> slsProducts = new HashSet<SlsProduct>(0);

	public SlsAdmWarehouse() {
	}

	public SlsAdmWarehouse(int id) {
		this.id = id;
	}

	public SlsAdmWarehouse(int id, AdmDistrict admDistrict,
			String warehouseName, String warehouseDescription,
			String warehouseAddress, String warehousePhoto, Date createdDate,
			String createdBy, Date updatedDate, String updatedBy,
			Set<SlsProductStock> slsProductStocks, Set<SlsProduct> slsProducts) {
		this.id = id;
		this.admDistrict = admDistrict;
		this.warehouseName = warehouseName;
		this.warehouseDescription = warehouseDescription;
		this.warehouseAddress = warehouseAddress;
		this.warehousePhoto = warehousePhoto;
		this.createdDate = createdDate;
		this.createdBy = createdBy;
		this.updatedDate = updatedDate;
		this.updatedBy = updatedBy;
		this.slsProductStocks = slsProductStocks;
		this.slsProducts = slsProducts;
	}

	@Id
	@Column(name = "ID", unique = true, nullable = false, precision = 5, scale = 0)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "DISTRICT_ID")
	public AdmDistrict getAdmDistrict() {
		return this.admDistrict;
	}

	public void setAdmDistrict(AdmDistrict admDistrict) {
		this.admDistrict = admDistrict;
	}

	@Column(name = "WAREHOUSE_NAME", length = 100)
	public String getWarehouseName() {
		return this.warehouseName;
	}

	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}

	@Column(name = "WAREHOUSE_DESCRIPTION", length = 200)
	public String getWarehouseDescription() {
		return this.warehouseDescription;
	}

	public void setWarehouseDescription(String warehouseDescription) {
		this.warehouseDescription = warehouseDescription;
	}

	@Column(name = "WAREHOUSE_ADDRESS", length = 200)
	public String getWarehouseAddress() {
		return this.warehouseAddress;
	}

	public void setWarehouseAddress(String warehouseAddress) {
		this.warehouseAddress = warehouseAddress;
	}

	@Column(name = "WAREHOUSE_PHOTO", length = 200)
	public String getWarehousePhoto() {
		return this.warehousePhoto;
	}

	public void setWarehousePhoto(String warehousePhoto) {
		this.warehousePhoto = warehousePhoto;
	}

	@Column(name = "CREATED_DATE")
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "CREATED_BY", length = 100)
	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "UPDATED_DATE")
	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Column(name = "UPDATED_BY", length = 100)
	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "slsAdmWarehouse")
	public Set<SlsProductStock> getSlsProductStocks() {
		return this.slsProductStocks;
	}

	public void setSlsProductStocks(Set<SlsProductStock> slsProductStocks) {
		this.slsProductStocks = slsProductStocks;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "slsAdmWarehouse")
	public Set<SlsProduct> getSlsProducts() {
		return this.slsProducts;
	}

	public void setSlsProducts(Set<SlsProduct> slsProducts) {
		this.slsProducts = slsProducts;
	}

}
