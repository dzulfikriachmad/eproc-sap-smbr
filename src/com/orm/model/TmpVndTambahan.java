package com.orm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="TMP_VND_TAMBAHAN")
public class TmpVndTambahan implements Serializable {

	private Integer id;
	private TmpVndHeader vendorId;
	private String tipe;
	private String nama;
	private String alamat;
	private AdmCountry negara;
	private String kodePos;
	private String hubunganKerja;
	private String kualifikasi;
	
	public TmpVndTambahan() {
		// TODO Auto-generated constructor stub
	}

	@Id
	@SequenceGenerator(name="TMP_VND_TAMB_SEQ",sequenceName="TMP_VND_TAMB_SEQ")
	@GeneratedValue(generator="TMP_VND_TAMB_SEQ",strategy=GenerationType.SEQUENCE)
	@Column(name="ID")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VENDOR_ID")
	public TmpVndHeader getVendorId() {
		return vendorId;
	}

	public void setVendorId(TmpVndHeader vendorId) {
		this.vendorId = vendorId;
	}

	@Column(name="TIPE")
	public String getTipe() {
		return tipe;
	}

	public void setTipe(String tipe) {
		this.tipe = tipe;
	}

	@Column(name="NAMA")
	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	@Column(name="ALAMAT")
	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="NEGARA")
	public AdmCountry getNegara() {
		return negara;
	}

	public void setNegara(AdmCountry negara) {
		this.negara = negara;
	}

	@Column(name="KODE_POS")
	public String getKodePos() {
		return kodePos;
	}

	public void setKodePos(String kodePos) {
		this.kodePos = kodePos;
	}

	@Column(name="HUBUNGAN_KERJA")
	public String getHubunganKerja() {
		return hubunganKerja;
	}

	public void setHubunganKerja(String hubunganKerja) {
		this.hubunganKerja = hubunganKerja;
	}

	@Column(name="KUALIFIKASI")
	public String getKualifikasi() {
		return kualifikasi;
	}

	public void setKualifikasi(String kualifikasi) {
		this.kualifikasi = kualifikasi;
	}
	
}
