package com.orm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class OrHeaderId implements Serializable {

	@Column(name="NOMOR_OR")
	private Long nomorOr;//nomorPr -> sap
	
	@Column(name="ORH")
	private String orH;//nomor prh -> sap
	
	@Column(name="HEADER_SITE")
	private String headerSite;
	
	public OrHeaderId() {
		// TODO Auto-generated constructor stub
	}
	
	public Long getNomorOr() {
		return nomorOr;
	}
	public void setNomorOr(Long nomorOr) {
		this.nomorOr = nomorOr;
	}
	
	public String getHeaderSite() {
		return headerSite;
	}
	public void setHeaderSite(String headerSite) {
		this.headerSite = headerSite;
	}

	

	public String getOrH() {
		return orH;
	}

	public void setOrH(String orH) {
		this.orH = orH;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((headerSite == null) ? 0 : headerSite.hashCode());
		result = prime * result + ((nomorOr == null) ? 0 : nomorOr.hashCode());
		result = prime * result + ((orH == null) ? 0 : orH.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof OrHeaderId))
			return false;
		OrHeaderId other = (OrHeaderId) obj;
		if (headerSite == null) {
			if (other.headerSite != null)
				return false;
		} else if (!headerSite.equals(other.headerSite))
			return false;
		if (nomorOr == null) {
			if (other.nomorOr != null)
				return false;
		} else if (!nomorOr.equals(other.nomorOr))
			return false;
		if (orH == null) {
			if (other.orH != null)
				return false;
		} else if (!orH.equals(other.orH))
			return false;
		return true;
	}
	
	
}
