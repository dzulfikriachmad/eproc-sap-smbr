package com.orm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="CTR_CONTRACT_COMMENT")
public class CtrContractComment implements Serializable {

	private Long id;
	private PrcMainHeader prcMainHeader;
	private BigDecimal contractId;
	private String username;
	private String posisi;
	private String comment;
	private String document;
	private Integer proses;
	private String aksi;
	private String prosesAksi;
	private Date createdDate;
	private String createdBy;
	private Date updatedDate;
	private String updatedBy;

	public CtrContractComment() {
	}

	public CtrContractComment(Long id) {
		this.id = id;
	}

	public CtrContractComment(Long id, PrcMainHeader prcMainHeader,
			BigDecimal contractId, String username, String comment,
			String document, Integer proses, Date createdDate,
			String createdBy, Date updatedDate, String updatedBy) {
		this.id = id;
		this.prcMainHeader = prcMainHeader;
		this.contractId = contractId;
		this.username = username;
		this.comment = comment;
		this.document = document;
		this.proses = proses;
		this.createdDate = createdDate;
		this.createdBy = createdBy;
		this.updatedDate = updatedDate;
		this.updatedBy = updatedBy;
	}

	@Id
	@SequenceGenerator(name = "CTR_MAIN_COMMENT_SEQ", sequenceName = "CTR_MAIN_COMMENT_SEQ")
	@GeneratedValue(generator = "CTR_MAIN_COMMENT_SEQ")
	@Column(name = "ID", unique = true, nullable = false, precision = 35, scale = 0)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PPM_ID")
	public PrcMainHeader getPrcMainHeader() {
		return this.prcMainHeader;
	}

	public void setPrcMainHeader(PrcMainHeader prcMainHeader) {
		this.prcMainHeader = prcMainHeader;
	}

	@Column(name = "CONTRACT_ID", precision = 30, scale = 0)
	public BigDecimal getContractId() {
		return this.contractId;
	}

	public void setContractId(BigDecimal contractId) {
		this.contractId = contractId;
	}

	@Column(name = "USERNAME", length = 100)
	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Column(name = "KOMENTAR", length = 1000)
	public String getComment() {
		return this.comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Column(name = "DOCUMENT", length = 200)
	public String getDocument() {
		return this.document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	@Column(name = "PROSES", precision = 5, scale = 0)
	public Integer getProses() {
		return this.proses;
	}

	public void setProses(Integer proses) {
		this.proses = proses;
	}

	@Column(name = "CREATED_DATE")
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "CREATED_BY", length = 100)
	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "UPDATED_DATE")
	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Column(name = "UPDATED_BY", length = 100)
	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Column(name="POSISI")
	public String getPosisi() {
		return posisi;
	}

	public void setPosisi(String posisi) {
		this.posisi = posisi;
	}

	@Column(name="AKSI")
	public String getAksi() {
		return aksi;
	}

	public void setAksi(String aksi) {
		this.aksi = aksi;
	}

	@Column(name="PROSES_AKSI")
	public String getProsesAksi() {
		return prosesAksi;
	}

	public void setProsesAksi(String prosesAksi) {
		this.prosesAksi = prosesAksi;
	}
}
