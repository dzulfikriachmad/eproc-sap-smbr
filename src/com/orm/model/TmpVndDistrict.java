package com.orm.model;

// Generated Feb 26, 2012 1:56:14 PM by Hibernate Tools 3.4.0.Beta1

import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * TmpVndDistrict generated by hbm2java
 */
@Entity
@Table(name = "TMP_VND_DISTRICT")
public class TmpVndDistrict implements java.io.Serializable {

	private TmpVndDistrictId id;
	private TmpVndHeader tmpVndHeader;
	private AdmDistrict admDistrict;
	private Date createdDate;
	private String createdBy;
	private Date updatedDate;
	private String updatedBy;

	public TmpVndDistrict() {
	}

	public TmpVndDistrict(TmpVndDistrictId id, TmpVndHeader tmpVndHeader,
			AdmDistrict admDistrict) {
		this.id = id;
		this.tmpVndHeader = tmpVndHeader;
		this.admDistrict = admDistrict;
	}

	public TmpVndDistrict(TmpVndDistrictId id, TmpVndHeader tmpVndHeader,
			AdmDistrict admDistrict, Date createdDate, String createdBy,
			Date updatedDate, String updatedBy) {
		this.id = id;
		this.tmpVndHeader = tmpVndHeader;
		this.admDistrict = admDistrict;
		this.createdDate = createdDate;
		this.createdBy = createdBy;
		this.updatedDate = updatedDate;
		this.updatedBy = updatedBy;
	}

	@EmbeddedId
	@AttributeOverrides({
			@AttributeOverride(name = "vendorId", column = @Column(name = "VENDOR_ID", nullable = false, precision = 15, scale = 0)),
			@AttributeOverride(name = "districtId", column = @Column(name = "DISTRICT_ID", nullable = false, precision = 5, scale = 0)) })
	public TmpVndDistrictId getId() {
		return this.id;
	}

	public void setId(TmpVndDistrictId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "VENDOR_ID", nullable = false, insertable = false, updatable = false)
	public TmpVndHeader getTmpVndHeader() {
		return this.tmpVndHeader;
	}

	public void setTmpVndHeader(TmpVndHeader tmpVndHeader) {
		this.tmpVndHeader = tmpVndHeader;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "DISTRICT_ID", nullable = false, insertable = false, updatable = false)
	public AdmDistrict getAdmDistrict() {
		return this.admDistrict;
	}

	public void setAdmDistrict(AdmDistrict admDistrict) {
		this.admDistrict = admDistrict;
	}

	@Column(name = "CREATED_DATE")
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "CREATED_BY", length = 100)
	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "UPDATED_DATE")
	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Column(name = "UPDATED_BY", length = 100)
	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

}
