package com.orm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Table(name="EAUCTION_HEADER")
public class EauctionHeader implements Serializable {

	@GenericGenerator(name = "generator", strategy = "foreign", parameters = @Parameter(name = "property", value = "prcMainHeader"))
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "PPM_ID", unique = true, nullable = false, precision = 30, scale = 0)
	private Long ppmId;
	
	@OneToOne(fetch = FetchType.LAZY)
	@PrimaryKeyJoinColumn
	private PrcMainHeader prcMainHeader;
	
	@Column(name="JUDUL")
	private String judul;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TANGGAL_MULAI", columnDefinition="DATE")
	private Date tanggalMulai;
	
	@Column(name="WAKTU")
	private Long waktu;
	
	@Column(name="DESKRIPSI")
	private String deskripsi;
	
	/*
	 * 1 = paket, 2 item
	 */
	@Column(name="TIPE")
	private Integer tipe;
	
	@Column(name="tipeName")
	private String tipeNama;
	
	@Column(name="HPS")
	private BigDecimal Hps;
	
	@Column(name="CREATED_BY")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATED_DATE")
	private Date createdDate;
	
	/**
	 * 1= mulai, 0= selesai
	 */
	@Column(name="STATUS")
	private int status;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TANGGAL_BERAKHIR")
	private Date tanggalBerakhir;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CURR_ID")
	private AdmCurrency admCurr;
	
	@Column(name="BATAS_ATAS")
	private BigDecimal batasAtas;
	
	@Column(name ="BATAS_BAWAH")
	private BigDecimal batasBawah;
	
	@Column(name="MINIMAL_PENURUNAN")
	private BigDecimal minimumPenurunan;

	/**
	 * 1= latihan, 0= bukan latihan
	 */
	@Column(name="LATIHAN")
	private Integer latihan;

	public Integer getLatihan() {
		return latihan;
	}

	public void setLatihan(Integer latihan) {
		this.latihan = latihan;
	}

	@OneToMany(fetch=FetchType.LAZY)
	@JoinColumn(name="EAUCTION_HEADER")
	private Set<EauctionVendor> eauctionVendors = new HashSet<EauctionVendor>();
	
	public EauctionHeader() {
		// TODO Auto-generated constructor stub
	}

	public Long getPpmId() {
		return ppmId;
	}

	public void setPpmId(Long ppmId) {
		this.ppmId = ppmId;
	}

	public String getJudul() {
		return judul;
	}

	public void setJudul(String judul) {
		this.judul = judul;
	}

	public Date getTanggalMulai() {
		return tanggalMulai;
	}

	public void setTanggalMulai(Date tanggalMulai) {
		this.tanggalMulai = tanggalMulai;
	}

	public Long getWaktu() {
		return waktu;
	}

	public void setWaktu(Long waktu) {
		this.waktu = waktu;
	}

	public String getDeskripsi() {
		return deskripsi;
	}

	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}

	public Integer getTipe() {
		return tipe;
	}

	public void setTipe(Integer tipe) {
		this.tipe = tipe;
	}

	public String getTipeNama() {
		return tipeNama;
	}

	public void setTipeNama(String tipeNama) {
		this.tipeNama = tipeNama;
	}

	public BigDecimal getHps() {
		return Hps;
	}

	public void setHps(BigDecimal hps) {
		Hps = hps;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Date getTanggalBerakhir() {
		return tanggalBerakhir;
	}

	public void setTanggalBerakhir(Date tanggalBerakhir) {
		this.tanggalBerakhir = tanggalBerakhir;
	}

	public AdmCurrency getAdmCurr() {
		return admCurr;
	}

	public void setAdmCurr(AdmCurrency admCurr) {
		this.admCurr = admCurr;
	}

	public BigDecimal getBatasAtas() {
		return batasAtas;
	}

	public void setBatasAtas(BigDecimal batasAtas) {
		this.batasAtas = batasAtas;
	}

	public BigDecimal getBatasBawah() {
		return batasBawah;
	}

	public void setBatasBawah(BigDecimal batasBawah) {
		this.batasBawah = batasBawah;
	}

	public BigDecimal getMinimumPenurunan() {
		return minimumPenurunan;
	}

	public void setMinimumPenurunan(BigDecimal minimumPenurunan) {
		this.minimumPenurunan = minimumPenurunan;
	}

	public PrcMainHeader getPrcMainHeader() {
		return prcMainHeader;
	}

	public void setPrcMainHeader(PrcMainHeader prcMainHeader) {
		this.prcMainHeader = prcMainHeader;
	}

	public Set<EauctionVendor> getEauctionVendors() {
		return eauctionVendors;
	}

	public void setEauctionVendors(Set<EauctionVendor> eauctionVendors) {
		this.eauctionVendors = eauctionVendors;
	}
	
	
}
