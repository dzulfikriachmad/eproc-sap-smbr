package com.orm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "ADM_PURCHASING_ORGANIZATION")
public class AdmPurchasingOrganization {
	private int id;
	private String kodePurcOrg;
	private String deskripsiPurchOrg;
	private String kodePlant;
	private String deskripsiPlant;
	
	public AdmPurchasingOrganization() {
		
	}
	
	public AdmPurchasingOrganization(int id, String kodePurcOrg, String deskripsiPurchOrg, String deskripsiPlant) {
		this.id = id;
		this.kodePurcOrg = kodePurcOrg;
		this.deskripsiPurchOrg = deskripsiPurchOrg;
		this.deskripsiPlant = deskripsiPlant;
		
	}
	
	@Id
	@SequenceGenerator(name="ADM_PURCH_ORG_SEQ", sequenceName="ADM_PURCH_ORG_SEQ")
	@GeneratedValue(generator="ADM_PURCH_ORG_SEQ")
	@Column(name = "ID", unique = true, nullable = false, precision = 5, scale = 0)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	@Column(name = "KODE_PURC_ORG", length = 10)
	public String getKodePurcOrg() {
		return kodePurcOrg;
	}

	public void setKodePurcOrg(String kodePurcOrg) {
		this.kodePurcOrg = kodePurcOrg;
	}

	@Column(name = "DESKRIPSI_PURC_ORG", length = 100)
	public String getDeskripsiPurchOrg() {
		return deskripsiPurchOrg;
	}

	public void setDeskripsiPurchOrg(String deskripsiPurchOrg) {
		this.deskripsiPurchOrg = deskripsiPurchOrg;
	}
	
	@Column(name = "KODE_PLANT", length = 10)
	public String getKodePlant() {
		return kodePlant;
	}

	public void setKodePlant(String kodePlant) {
		this.kodePlant = kodePlant;
	}

	@Column(name = "DESKRIPSI_PLANT", length = 100)
	public String getDeskripsiPlant() {
		return deskripsiPlant;
	}

	public void setDeskripsiPlant(String deskripsiPlant) {
		this.deskripsiPlant = deskripsiPlant;
	}
	
	
}
