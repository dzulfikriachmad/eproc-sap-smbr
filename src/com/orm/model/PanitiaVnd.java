package com.orm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="PANITIA_VND")
public class PanitiaVnd implements Serializable {

	private Integer id;
	private String nomorSurat;
	private String namaPanitia;
	private Date tanggalMulai;
	private Date tanggalBerakhir;
	private AdmDept satker;
	private Integer status;
	private Date createdDate;
	private String createdBy;
	private Date updatedDate;
	private String updatedBy;
	
	public PanitiaVnd() {
		// TODO Auto-generated constructor stub
	}

	@Id
	@SequenceGenerator(name="PANITIA_VND_SEQ",sequenceName="PANITIA_VND_SEQ")
	@GeneratedValue(generator="PANITIA_VND_SEQ",strategy=GenerationType.SEQUENCE)
	@Column(name="ID")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="NOMOR_SURAT")
	public String getNomorSurat() {
		return nomorSurat;
	}

	public void setNomorSurat(String nomorSurat) {
		this.nomorSurat = nomorSurat;
	}

	@Column(name="NAMA_PANITIA")
	public String getNamaPanitia() {
		return namaPanitia;
	}

	public void setNamaPanitia(String namaPanitia) {
		this.namaPanitia = namaPanitia;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TANGGAL_MULAI")
	public Date getTanggalMulai() {
		return tanggalMulai;
	}

	public void setTanggalMulai(Date tanggalMulai) {
		this.tanggalMulai = tanggalMulai;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TANGGAL_BERAKHIR")
	public Date getTanggalBerakhir() {
		return tanggalBerakhir;
	}

	public void setTanggalBerakhir(Date tanggalBerakhir) {
		this.tanggalBerakhir = tanggalBerakhir;
	}

	@Column(name="STATUS")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATED_DATE")
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name="CREATED_BY")
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UPDATED_DATE")
	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Column(name="UPDATED_BY")
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SATKER")
	public AdmDept getSatker() {
		return satker;
	}

	public void setSatker(AdmDept satker) {
		this.satker = satker;
	}
	
	
}
