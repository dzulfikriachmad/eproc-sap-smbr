package com.orm.model;

import java.io.Serializable;
import java.sql.Blob;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="OR_HEADER")
public class OrHeader implements Serializable {

	@EmbeddedId
	@AttributeOverrides({
		@AttributeOverride(name = "nomorOr", column = @Column(name = "NOMOR_OR", nullable = false)),
		@AttributeOverride(name = "orH", column = @Column(name = "ORH", nullable = false)),
		@AttributeOverride(name = "headerSite", column = @Column(name = "HEADER_SITE", nullable = false))
	})
	private OrHeaderId id;
	
	@Column(name="LAST_STATUS")
	private String lastStatus;
	
	@Column(name="NEXT_STATUS")
	private String nextStatus;
	
	@Column(name="TGL_OR")
	private Date tglOr;
	
	@Column(name="TGL_APPROVE")
	private Date tglApprove;
	
	@Column(name="PEMBUAT_OM")
	private String pembuatOm; //pembuatOR->kode_user ->SAP
	
	@Column(name="NAMA_PEMBUAT_OM")
	private String namaPembuatOm;//namapembuatOR->SAP
	
	@Column(name="NOMOR_OM")
	private String nomorOm;
	
	@Column(name="OM_ON")
	private String omOn;
	
	@Column(name="SITE")
	private String site;
	
	@Column(name="UNIT_KERJA")
	private String unitKerja;
	
	@Column(name="NOMOR_WO")
	private String nomorWo;
	
	@Column(name="ATTACHMENT",length=4000)
	private String attachment;
	
	@Column(name="FILE_NAME")
	private String fileName;
	
	@Column(name="SERVER_FILE_NAME")
	private String serverFileName;
	

	@Column(name="PURCHASING_GROUP")
	private String purchasingGroup;
	
	
	//@Column(name="FUND_CENTER")
	//private String fundCenter;
	
	//@Column(name="COMMITMENT_ITEM")
//	private String commitmentItem;
	
	//@Column(name="FUNCTIONAL_AREA")
	//private String functionalArea;
	
	
	/**
	 * default 0 = not processed
	 * 1 = completed
	 */
	@Column(name="STATUS")
	private Integer status;
	
	
	@Column(name="CONTENT", length=4000)
	@Lob
	private byte [] content;
	
	@Column(name="CONTENT_TYPE")
	private String contentType;
	
	@Column(name="MATA_UANG")
	private String mataUang;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATED_DATE")
	private Date createdDate;
	
	@Column(name="KODE_GENERATE")
	private String kodeGenerate;
	
	@Column(name="KODE_SATKER")
	private String kodeSatker;
	
	@Column(name="PEMBUAT_OR_JASA")
	private String pembuatOrJasa;
	
	
	public OrHeader() {
		// TODO Auto-generated constructor stub
	}
	
	public OrHeader(OrHeaderId id){
		this.id = id;
	}

	public OrHeaderId getId() {
		return id;
	}

	public void setId(OrHeaderId id) {
		this.id = id;
	}

	public String getLastStatus() {
		return lastStatus;
	}

	public void setLastStatus(String lastStatus) {
		this.lastStatus = lastStatus;
	}

	public String getNextStatus() {
		return nextStatus;
	}

	public void setNextStatus(String nextStatus) {
		this.nextStatus = nextStatus;
	}

	public Date getTglOr() {
		return tglOr;
	}

	public void setTglOr(Date tglOr) {
		this.tglOr = tglOr;
	}

	public Date getTglApprove() {
		return tglApprove;
	}

	public void setTglApprove(Date tglApprove) {
		this.tglApprove = tglApprove;
	}

	public String getPembuatOm() {
		return pembuatOm;
	}

	public void setPembuatOm(String pembuatOm) {
		this.pembuatOm = pembuatOm;
	}

	public String getNamaPembuatOm() {
		return namaPembuatOm;
	}

	public void setNamaPembuatOm(String namaPembuatOm) {
		this.namaPembuatOm = namaPembuatOm;
	}

	public String getNomorOm() {
		return nomorOm;
	}

	public void setNomorOm(String nomorOm) {
		this.nomorOm = nomorOm;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String getUnitKerja() {
		return unitKerja;
	}

	public void setUnitKerja(String unitKerja) {
		this.unitKerja = unitKerja;
	}

	public String getNomorWo() {
		return nomorWo;
	}

	public void setNomorWo(String nomorWo) {
		this.nomorWo = nomorWo;
	}

	public String getOmOn() {
		return omOn;
	}

	public void setOmOn(String omOn) {
		this.omOn = omOn;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getServerFileName() {
		return serverFileName;
	}

	public void setServerFileName(String serverFileName) {
		this.serverFileName = serverFileName;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getMataUang() {
		return mataUang;
	}

	public void setMataUang(String mataUang) {
		this.mataUang = mataUang;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getKodeGenerate() {
		return kodeGenerate;
	}

	public void setKodeGenerate(String kodeGenerate) {
		this.kodeGenerate = kodeGenerate;
	}

	public String getKodeSatker() {
		return kodeSatker;
	}

	public void setKodeSatker(String kodeSatker) {
		this.kodeSatker = kodeSatker;
	}

	public String getPembuatOrJasa() {
		return pembuatOrJasa;
	}

	public void setPembuatOrJasa(String pembuatOrJasa) {
		this.pembuatOrJasa = pembuatOrJasa;
	}
	
	public String getPurchasingGroup() {
		return purchasingGroup;
	}

	public void setPurchasingGroup(String purchasingGroup) {
		this.purchasingGroup = purchasingGroup;
	}
	
	
}
