package com.orm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="USULAN_VND_HISTORY")
public class UsulanVndHistory implements Serializable {

	@Id
	@SequenceGenerator(name="USULAN_VND_HIST_SEQ",sequenceName="USULAN_VND_HIST_SEQ")
	@GeneratedValue(generator="USULAN_VND_HIST_SEQ",strategy=GenerationType.SEQUENCE)
	@Column(name="ID")
	private Integer id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="USULAN")
	private UsulanVnd usulan;
	
	@Column(name="NAMA")
	private String nama;
	
	@Column(name="POSISI")
	private String posisi;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATED_DATE")
	private Date createdDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UPDATED_DATE")
	private Date updatedDate;
	
	@Column(name="PROSES")
	private String proses;
	
	@Column(name="KOMENTAR")
	private String komentar;
	
	@Column(name="AKSI")
	private String aksi;
	
	@Column(name="DOKUMEN")
	private String dokumen;
	
	public UsulanVndHistory() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public UsulanVnd getUsulan() {
		return usulan;
	}

	public void setUsulan(UsulanVnd usulan) {
		this.usulan = usulan;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getPosisi() {
		return posisi;
	}

	public void setPosisi(String posisi) {
		this.posisi = posisi;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getProses() {
		return proses;
	}

	public void setProses(String proses) {
		this.proses = proses;
	}

	public String getKomentar() {
		return komentar;
	}

	public void setKomentar(String komentar) {
		this.komentar = komentar;
	}

	public String getAksi() {
		return aksi;
	}

	public void setAksi(String aksi) {
		this.aksi = aksi;
	}

	public String getDokumen() {
		return dokumen;
	}

	public void setDokumen(String dokumen) {
		this.dokumen = dokumen;
	}
	
}
