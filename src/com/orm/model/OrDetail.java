package com.orm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="OR_DETAIL")
public class OrDetail implements Serializable {

	@EmbeddedId
	@AttributeOverrides({
		@AttributeOverride(name = "nomorOr", column = @Column(name = "NOMOR_OR", nullable = false)),
		@AttributeOverride(name = "orH", column = @Column(name = "ORH", nullable = false)),
		@AttributeOverride(name = "headerSite", column = @Column(name = "HEADER_SITE", nullable = false)),
		@AttributeOverride(name = "lineId", column = @Column(name = "LINE_ID", nullable = false))
	})
	private OrDetailId id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumns({
		@JoinColumn(name="NOMOR_OR_H"),
		@JoinColumn(name="OR_H"),
		@JoinColumn(name="HEADER_SITE_H")
	})
	private OrHeader orHeader;
	
	@Column(name="LINE")
	private Integer line;
	@Column(name="KODE_BARANG")
	private String kodeBarang;
	
	@Column(name="NAMA_BARANG")
	private String namaBarang;
	
	@Column(name="NAMA_BARANG_EXT")
	private String namaBarangExt;
	
	@Column(name="JUMLAH")
	private Double jumlah;
	
	@Column(name="SATUAN")
	private String satuan;
	
	@Column(name="HARGA_SATUAN")
	private Double hargaSatuan;
	
	@Column(name="LINE_TYPE")
	private String lineType;
	
	@Column(name="ACCOUNT_NUMBER")
	private String accuountNumber;
	
	@Column(name="ATTACHMENT",length=4000)
	private String attachment;
	
	@Column(name="FILE_NAME")
	private String fileName;
	
	@Column(name="SERVER_FILE_NAME")
	private String serverFileName;
	
	@Column(name="SERVICE_PRICE_PR")
	private BigDecimal servicePricePr;
	
	@Column(name="ITEM_CATEGORY")
	private String itemCategory;
	
	@Column(name="DELIVERY_DATE")
	private Date deliveryDate;
	
	@Column(name="MATERIAL_GROUP")
	private String materialGroup;
	
	@Column(name="STORAGE_LOCATION")
	private String storageLocation;
	

	@Column(name="CONTENT", length=4000)
	@Lob
	private byte[] content;
	
	@Column(name="CONTENT_TYPE")
	private String contentType;
	
	@Column(name="DESCRIPTION_TEXT")
	private String descriptionText;
	
	
	public OrDetail() {
		// TODO Auto-generated constructor stub
	}
	
	public OrDetail(OrDetailId id) {
		this.id = id;
	}

	public OrDetailId getId() {
		return id;
	}

	public void setId(OrDetailId id) {
		this.id = id;
	}

	public OrHeader getOrHeader() {
		return orHeader;
	}

	public void setOrHeader(OrHeader orHeader) {
		this.orHeader = orHeader;
	}

	public Integer getLine() {
		return line;
	}

	public void setLine(Integer line) {
		this.line = line;
	}

	public String getKodeBarang() {
		return kodeBarang;
	}

	public void setKodeBarang(String kodeBarang) {
		this.kodeBarang = kodeBarang;
	}

	public String getNamaBarang() {
		return namaBarang;
	}

	public void setNamaBarang(String namaBarang) {
		this.namaBarang = namaBarang;
	}

	public String getNamaBarangExt() {
		return namaBarangExt;
	}

	public void setNamaBarangExt(String namaBarangExt) {
		this.namaBarangExt = namaBarangExt;
	}

	public Double getJumlah() {
		return jumlah;
	}

	public void setJumlah(Double jumlah) {
		this.jumlah = jumlah;
	}

	public String getSatuan() {
		return satuan;
	}

	public void setSatuan(String satuan) {
		this.satuan = satuan;
	}

	public Double getHargaSatuan() {
		return hargaSatuan;
	}

	public void setHargaSatuan(Double hargaSatuan) {
		this.hargaSatuan = hargaSatuan;
	}

	public String getLineType() {
		return lineType;
	}

	public void setLineType(String lineType) {
		this.lineType = lineType;
	}

	public String getAccuountNumber() {
		return accuountNumber;
	}

	public void setAccuountNumber(String accuountNumber) {
		this.accuountNumber = accuountNumber;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getServerFileName() {
		return serverFileName;
	}

	public void setServerFileName(String serverFileName) {
		this.serverFileName = serverFileName;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public BigDecimal getServicePricePr() {
		return servicePricePr;
	}

	public void setServicePricePr(BigDecimal servicePricePr) {
		this.servicePricePr = servicePricePr;
	}

	public String getItemCategory() {
		return itemCategory;
	}

	public void setItemCategory(String itemCategory) {
		this.itemCategory = itemCategory;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getMaterialGroup() {
		return materialGroup;
	}

	public void setMaterialGroup(String materialGroup) {
		this.materialGroup = materialGroup;
	}

	public String getStorageLocation() {
		return storageLocation;
	}

	public void setStorageLocation(String storageLocation) {
		this.storageLocation = storageLocation;
	}
	public String getDescriptionText() {
		return descriptionText;
	}

	public void setDescriptionText(String descriptionText) {
		this.descriptionText = descriptionText;
	}
	
	
}
