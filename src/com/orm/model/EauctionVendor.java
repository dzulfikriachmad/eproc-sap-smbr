package com.orm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="EAUCTION_VENDOR")
public class EauctionVendor implements Serializable {

	@Id
	@SequenceGenerator(name="EAUCTION_VENDOR_SEQ",sequenceName="EAUCTION_VENDOR_SEQ")
	@GeneratedValue(generator="EAUCTION_VENDOR_SEQ",strategy=GenerationType.SEQUENCE)
	@Column(name="ID")
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="EAUCTION_HEADER")
	private EauctionHeader eauctionHeader;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VND_HEADER")
	private VndHeader vndHeader;
	
	@Column(name="LAST_BID")
	private BigDecimal lastBid;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TGL_BID")
	private Date tglBid;
	
	/**
	 * -1 = diskualifikasi
	 */
	@Column(name="STATUS")
	private Integer status;
	
	public EauctionVendor() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public EauctionHeader getEauctionHeader() {
		return eauctionHeader;
	}

	public void setEauctionHeader(EauctionHeader eauctionHeader) {
		this.eauctionHeader = eauctionHeader;
	}

	public VndHeader getVndHeader() {
		return vndHeader;
	}

	public void setVndHeader(VndHeader vndHeader) {
		this.vndHeader = vndHeader;
	}

	public BigDecimal getLastBid() {
		return lastBid;
	}

	public void setLastBid(BigDecimal lastBid) {
		this.lastBid = lastBid;
	}

	public Date getTglBid() {
		return tglBid;
	}

	public void setTglBid(Date tglBid) {
		this.tglBid = tglBid;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
}
