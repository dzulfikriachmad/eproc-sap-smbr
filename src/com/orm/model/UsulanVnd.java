package com.orm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="USULAN_VND")
public class UsulanVnd implements Serializable {

	private Integer id;
	private String nomorUsulan;
	private String namaUsulan;
	private String keterangan;
	private AdmRole nextPos;
	private AdmUser nextUser;
	private AdmUser creator;
	private AdmDistrict kantor;
	private String tipe;
	private Integer tingkat;
	private Date createdDate;
	private String createdBy;
	private Date updatedDate;
	private String updatedBy;
	
	public UsulanVnd() {
		// TODO Auto-generated constructor stub
	}

	@Id
	@SequenceGenerator(name="USULAN_VND_SEQ",sequenceName="USULAN_VND_SEQ")
	@GeneratedValue(generator="USULAN_VND_SEQ",strategy=GenerationType.SEQUENCE)
	@Column(name="ID")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="NOMOR_USULAN")
	public String getNomorUsulan() {
		return nomorUsulan;
	}

	public void setNomorUsulan(String nomorUsulan) {
		this.nomorUsulan = nomorUsulan;
	}

	@Column(name="NAMA_USULAN")
	public String getNamaUsulan() {
		return namaUsulan;
	}

	public void setNamaUsulan(String namaUsulan) {
		this.namaUsulan = namaUsulan;
	}

	@Column(name="KETERANGAN")
	public String getKeterangan() {
		return keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATED_DATE")
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name="CREATED_BY")
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UPDATED_DATE")
	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Column(name="UPDATED_BY")
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="POSISI")
	public AdmRole getNextPos() {
		return nextPos;
	}

	public void setNextPos(AdmRole nextPos) {
		this.nextPos = nextPos;
	}

	@Column(name="TINGKAT")
	public Integer getTingkat() {
		return tingkat;
	}

	public void setTingkat(Integer tingkat) {
		this.tingkat = tingkat;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="NEXT_USER")
	public AdmUser getNextUser() {
		return nextUser;
	}

	public void setNextUser(AdmUser nextUser) {
		this.nextUser = nextUser;
	}

	@Column(name="TIPE")
	public String getTipe() {
		return tipe;
	}

	public void setTipe(String tipe) {
		this.tipe = tipe;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CREATOR")
	public AdmUser getCreator() {
		return creator;
	}

	public void setCreator(AdmUser creator) {
		this.creator = creator;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="KANTOR")
	public AdmDistrict getKantor() {
		return kantor;
	}

	public void setKantor(AdmDistrict kantor) {
		this.kantor = kantor;
	}
	
}
