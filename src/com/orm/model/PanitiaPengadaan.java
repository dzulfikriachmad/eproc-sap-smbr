package com.orm.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@SuppressWarnings("serial")
@Entity
@Table(name="PANITIA_PENGADAAN")
public class PanitiaPengadaan implements Serializable {

	@Id
	@SequenceGenerator(sequenceName="PANITIA_PENGADAAN_SEQ",name="PANITIA_PENGADAAN_SEQ")
	@GeneratedValue(generator="PANITIA_PENGADAAN_SEQ",strategy=GenerationType.SEQUENCE)
	@Column(name="ID")
	private Integer id;
	
	@Column(name="NAMA_PANITIA",length=250)
	private String namaPanitia;
	
	@Column(name="KETERANGAN",length=500)
	private String keterangan;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TANGGAL_MULAI")
	private Date tanggalMulai;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TANGGAL_BERAKHIR")
	private Date tanggalBerakhir;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATED_DATE")
	private Date createdDate;
	
	@Column(name="CREATED_BY")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UPDATED_DATE")
	private Date updatedDate;
	
	@Column(name="UPDATED_BY")
	private String updatedBy;
	
	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name="PANITIA")
	private Set<PanitiaPengadaanDetail> panitiaPengadaanDetails = new HashSet<PanitiaPengadaanDetail>();
	
	public PanitiaPengadaan() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNamaPanitia() {
		return namaPanitia;
	}

	public void setNamaPanitia(String namaPanitia) {
		this.namaPanitia = namaPanitia;
	}

	public String getKeterangan() {
		return keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}

	public Date getTanggalMulai() {
		return tanggalMulai;
	}

	public void setTanggalMulai(Date tanggalMulai) {
		this.tanggalMulai = tanggalMulai;
	}

	public Date getTanggalBerakhir() {
		return tanggalBerakhir;
	}

	public void setTanggalBerakhir(Date tanggalBerakhir) {
		this.tanggalBerakhir = tanggalBerakhir;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Set<PanitiaPengadaanDetail> getPanitiaPengadaanDetails() {
		return panitiaPengadaanDetails;
	}

	public void setPanitiaPengadaanDetails(
			Set<PanitiaPengadaanDetail> panitiaPengadaanDetails) {
		this.panitiaPengadaanDetails = panitiaPengadaanDetails;
	}
	
}
