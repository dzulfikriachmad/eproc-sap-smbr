package com.orm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="EAUCTION_HISTORY_LATIHAN")
public class EauctionHistoryLatihan implements Serializable{

	private Integer id;
	private EauctionVendor eauctionVendor;
	private Date tglBid;
	private Double jumlahBid;
	
	public EauctionHistoryLatihan() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Method getId.
	 * @return Integer
	 */
	@Id
	@SequenceGenerator(sequenceName="EAUCTION_HISTORY_LATIHAN_SEQ",name="EAUCTION_HISTORY_LATIHAN_SEQ")
	@GeneratedValue(generator="EAUCTION_HISTORY_LATIHAN_SEQ",strategy=GenerationType.SEQUENCE)
	@Column(name="ID",unique=true,nullable=false)
	public Integer getId() {
		return id;
	}

	/**
	 * Method setId.
	 * @param id Integer
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Method getEauctionVendor.
	 * @return EauctionVendor
	 */
	@Column(name="EAUCTION_VENDOR")
	public EauctionVendor getEauctionVendor() {
		return eauctionVendor;
	}

	/**
	 * Method setEauctionVendor.
	 * @param eauctionVendor EauctionVendor
	 */
	public void setEauctionVendor(EauctionVendor eauctionVendor) {
		this.eauctionVendor = eauctionVendor;
	}

	/**
	 * Method getTglBid.
	 * @return Date
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TGL_BID")
	public Date getTglBid() {
		return tglBid;
	}

	/**
	 * Method setTglBid.
	 * @param tglBid Date
	 */
	public void setTglBid(Date tglBid) {
		this.tglBid = tglBid;
	}

	/**
	 * Method getJumlahBid.
	 * @return Double
	 */
	@Column(name="JUMLAH_BID")
	public Double getJumlahBid() {
		return jumlahBid;
	}

	/**
	 * Method setJumlahBid.
	 * @param jumlahBid Double
	 */
	public void setJumlahBid(Double jumlahBid) {
		this.jumlahBid = jumlahBid;
	}
}
