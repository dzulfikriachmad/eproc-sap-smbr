package com.orm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="PRC_MAPPING_KEUANGAN")
public class PrcMappingKeuangan implements Serializable {

	@Id
	@SequenceGenerator(name="PRC_MAPPING_KEU_SEQ", sequenceName="PRC_MAPPING_KEU_SEQ")
	@GeneratedValue(generator="PRC_MAPPING_KEU_SEQ",strategy=GenerationType.SEQUENCE)
	@Column(name="ID")
	private Integer id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ADM_USER")
	private AdmUser admUser;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="KANTOR")
	private AdmDistrict district;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="SATKER")
	private AdmDept admDept;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATED_DATE")
	private Date createdDate;
	
	@Column(name="CREATED_BY")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UPDATED_DATE")
	private Date updatedDate;
	
	@Column(name="UPDATED_BY")
	private String updatedBy;
	
	public PrcMappingKeuangan() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public AdmUser getAdmUser() {
		return admUser;
	}

	public void setAdmUser(AdmUser admUser) {
		this.admUser = admUser;
	}

	public AdmDistrict getDistrict() {
		return district;
	}

	public void setDistrict(AdmDistrict district) {
		this.district = district;
	}

	public AdmDept getAdmDept() {
		return admDept;
	}

	public void setAdmDept(AdmDept admDept) {
		this.admDept = admDept;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
}
