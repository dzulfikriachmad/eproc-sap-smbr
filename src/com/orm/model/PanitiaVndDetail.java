package com.orm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="PANITIA_VND_DETAIL")
public class PanitiaVndDetail implements Serializable {

	private Integer id;
	private PanitiaVnd panitia;
	private AdmUser anggota;
	private String posisi;
	private boolean ketua;
	private Date createdDate;
	private String createdBy;
	private Date updatedDate;
	private String updatedBy;
	
	public PanitiaVndDetail() {
		// TODO Auto-generated constructor stub
	}

	@Id
	@SequenceGenerator(name="PANITIA_VND_DET_SEQ",sequenceName="PANITIA_VND_DET_SEQ")
	@GeneratedValue(generator="PANITIA_VND_DET_SEQ",strategy=GenerationType.SEQUENCE)
	@Column(name="ID")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PANITIA")
	public PanitiaVnd getPanitia() {
		return panitia;
	}

	public void setPanitia(PanitiaVnd panitia) {
		this.panitia = panitia;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ANGGOTA")
	public AdmUser getAnggota() {
		return anggota;
	}

	public void setAnggota(AdmUser anggota) {
		this.anggota = anggota;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATED_DATE")
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name="CREATED_BY")
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UPDATED_DATE")
	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Column(name="UPDATED_BY")
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Column(name="POSISI")
	public String getPosisi() {
		return posisi;
	}

	public void setPosisi(String posisi) {
		this.posisi = posisi;
	}

	@Column(name="KETUA")
	public boolean isKetua() {
		return ketua;
	}

	public void setKetua(boolean ketua) {
		this.ketua = ketua;
	}
	
}
