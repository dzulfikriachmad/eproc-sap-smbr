package com.orm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ADM_ERP_CONFIG")
public class AdmErpConfig implements Serializable {

	@Id
	@Column(name="ID",nullable=false)
	private Integer id;
	
	@Column(name="SERVER")
	private String server;
	
	@Column(name="PORT")
	private String port;
	
	@Column(name="USERNAME")
	private String username;
	@Column(name="PASSWORD")
	private String password;
	
	@Column(name="USERNAME_ERP")
	private String usernameErp;
	@Column(name="PASSWORD_ERP")
	private String passwordErp;
	
	@Column(name="ROLE_ERP")
	private String roleErp;
	
	@Column(name="DISTRICT_ERP")
	private String districtErp;
	
	@Column(name="TEMP1")
	private String temp1;
	@Column(name="TEMP2")
	private String temp2;
	@Column(name="TEMP3")
	private String temp3;
	
	private Date createdDate;
	private String createdBy;
	private Date updatedDate;
	private String updatedBy;
	
	public AdmErpConfig() {
		// TODO Auto-generated constructor stub
	}
	
	public AdmErpConfig(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsernameErp() {
		return usernameErp;
	}

	public void setUsernameErp(String usernameErp) {
		this.usernameErp = usernameErp;
	}

	public String getPasswordErp() {
		return passwordErp;
	}

	public void setPasswordErp(String passwordErp) {
		this.passwordErp = passwordErp;
	}

	public String getRoleErp() {
		return roleErp;
	}

	public void setRoleErp(String roleErp) {
		this.roleErp = roleErp;
	}

	public String getDistrictErp() {
		return districtErp;
	}

	public void setDistrictErp(String districtErp) {
		this.districtErp = districtErp;
	}

	public String getTemp1() {
		return temp1;
	}

	public void setTemp1(String temp1) {
		this.temp1 = temp1;
	}

	public String getTemp2() {
		return temp2;
	}

	public void setTemp2(String temp2) {
		this.temp2 = temp2;
	}

	public String getTemp3() {
		return temp3;
	}

	public void setTemp3(String temp3) {
		this.temp3 = temp3;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
}
