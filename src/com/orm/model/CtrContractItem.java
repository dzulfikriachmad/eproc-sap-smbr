package com.orm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="CTR_CONTRACT_ITEM")
public class CtrContractItem implements Serializable {

	@Id
	@SequenceGenerator(name="CTR_CONTRACT_ITEM_SEQ",sequenceName="CTR_CONTRACT_ITEM_SEQ")
	@GeneratedValue(generator="CTR_CONTRACT_ITEM_SEQ",strategy=GenerationType.SEQUENCE)
	@Column(name="ID",nullable=false)
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CONTRACT_HEADER")
	private CtrContractHeader contractHeader;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="UOM")
	private AdmUom admUom;
	
	@Column(name="GROUP_CODE")
	private String groupCode;
	
	@Column(name="ITEM_TIPE")
	private String itemTipe;
	
	@Column(name="ITEM_CODE")
	private String itemCode;
	
	@Column(name="ITEM_DESCRIPTION")
	private String itemDescription;
	
	@Column(name="ITEM_COST_CENTER")
	private String itemCostCenter;
	
	@Column(name="ITEM_QUANTITY_OE")
	private BigDecimal itemQuantityOe;
	
	@Column(name="ITEM_TAX_OE")
	private BigDecimal itemTaxOe;
	
	@Column(name="ITEM_PRICE_OE")
	private BigDecimal itemPriceOe;
	
	@Column(name="ITEM_TOTAL_PRICE_OE")
	private BigDecimal itemTotalPriceOe;
	
	@Column(name="ATTACHMENT",length=4000)
	private String attachment;
	
	@Column(name="SERVICE_PRICE_OE")
	private BigDecimal servicePriceOe;
	
	@Column(name="LINE_ID")
	private Integer lineId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATED_DATE")
	private Date createdDate;
	
	@Column(name="CREATED_BY")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UPDATED_DATE")
	private Date updatedDate;
	
	@Column(name="UPDATED_BY")
	private String updatedBy;
	
	@Column(name="STORAGE_LOCATION")
	private String storageLocation;
	
	public CtrContractItem() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CtrContractHeader getContractHeader() {
		return contractHeader;
	}

	public void setContractHeader(CtrContractHeader contractHeader) {
		this.contractHeader = contractHeader;
	}

	public AdmUom getAdmUom() {
		return admUom;
	}

	public void setAdmUom(AdmUom admUom) {
		this.admUom = admUom;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getItemTipe() {
		return itemTipe;
	}

	public void setItemTipe(String itemTipe) {
		this.itemTipe = itemTipe;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemDescription() {
		return itemDescription;
	}

	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}

	public String getItemCostCenter() {
		return itemCostCenter;
	}

	public void setItemCostCenter(String itemCostCenter) {
		this.itemCostCenter = itemCostCenter;
	}

	public BigDecimal getItemQuantityOe() {
		return itemQuantityOe;
	}

	public void setItemQuantityOe(BigDecimal itemQuantityOe) {
		this.itemQuantityOe = itemQuantityOe;
	}

	public BigDecimal getItemTaxOe() {
		return itemTaxOe;
	}

	public void setItemTaxOe(BigDecimal itemTaxOe) {
		this.itemTaxOe = itemTaxOe;
	}

	public BigDecimal getItemPriceOe() {
		return itemPriceOe;
	}

	public void setItemPriceOe(BigDecimal itemPriceOe) {
		this.itemPriceOe = itemPriceOe;
	}

	public BigDecimal getItemTotalPriceOe() {
		return itemTotalPriceOe;
	}

	public void setItemTotalPriceOe(BigDecimal itemTotalPriceOe) {
		this.itemTotalPriceOe = itemTotalPriceOe;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public BigDecimal getServicePriceOe() {
		return servicePriceOe;
	}

	public void setServicePriceOe(BigDecimal servicePriceOe) {
		this.servicePriceOe = servicePriceOe;
	}

	public Integer getLineId() {
		return lineId;
	}

	public void setLineId(Integer lineId) {
		this.lineId = lineId;
	}

	public String getStorageLocation() {
		return storageLocation;
	}

	public void setStorageLocation(String storageLocation) {
		this.storageLocation = storageLocation;
	}
	
	
	
}
