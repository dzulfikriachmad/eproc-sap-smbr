package com.orm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="PRC_MSG_CHAT_VENDOR")
public class PrcMsgChatVendor implements Serializable {

	@Id
	@SequenceGenerator(name="PRC_MSG_VCHAT_SEQ",sequenceName="PRC_MSG_VCHAT_SEQ")
	@GeneratedValue(generator="PRC_MSG_VCHAT_SEQ",strategy=GenerationType.SEQUENCE)
	@Column(name="ID")
	private Long id;
	
	@Column(name="PPM_ID")
	private Long ppmId;
	
	@Column(name="VENDOR_ID")
	private Long vendorId;
	
	@Column(name="DARI")
	private String dari;
	
	@Column(name="KEPADA")
	private String kepada;
	
	@Column(name="PESAN")
	private String pesan;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATED_DATE")
	private Date createdDate;
	
	@Column(name="CREATED_BY")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UPDATED_DATE")
	private Date updatedDate;
	
	@Column(name="UPDATED_BY")
	private String updatedBy;
	
	public PrcMsgChatVendor() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPpmId() {
		return ppmId;
	}

	public void setPpmId(Long ppmId) {
		this.ppmId = ppmId;
	}

	public Long getVendorId() {
		return vendorId;
	}

	public void setVendorId(Long vendorId) {
		this.vendorId = vendorId;
	}

	public String getDari() {
		return dari;
	}

	public void setDari(String dari) {
		this.dari = dari;
	}

	public String getKepada() {
		return kepada;
	}

	public void setKepada(String kepada) {
		this.kepada = kepada;
	}

	public String getPesan() {
		return pesan;
	}

	public void setPesan(String pesan) {
		this.pesan = pesan;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	
}
