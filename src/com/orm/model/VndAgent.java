package com.orm.model;

// Generated Feb 26, 2012 1:56:14 PM by Hibernate Tools 3.4.0.Beta1

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * VndAgent generated by hbm2java
 */
@Entity
@Table(name = "VND_AGENT")
public class VndAgent implements java.io.Serializable {

	private long id;
	private VndHeader vndHeader;
	private Integer agnVype;
	private String agnNo;
	private String agnIssued;
	private Date agnFrom;
	private Date agnVo;
	private Date createdDate;
	private String createdBy;
	private Date updatedDate;
	private String updatedBy;

	public VndAgent() {
	}
	
	public VndAgent(TmpVndAgent t){
		this.id = t.getId();
		this.agnVype = t.getAgnType();
		this.agnFrom = t.getAgnFrom();
		this.agnIssued = t.getAgnIssued();
		this.agnNo = t.getAgnNo();
		this.agnVo = t.getAgnTo();
	}

	public VndAgent(long id, VndHeader vndHeader) {
		this.id = id;
		this.vndHeader = vndHeader;
	}

	public VndAgent(long id, VndHeader vndHeader, Integer agnVype,
			String agnNo, String agnIssued, Date agnFrom, Date agnVo,
			Date createdDate, String createdBy, Date updatedDate,
			String updatedBy) {
		this.id = id;
		this.vndHeader = vndHeader;
		this.agnVype = agnVype;
		this.agnNo = agnNo;
		this.agnIssued = agnIssued;
		this.agnFrom = agnFrom;
		this.agnVo = agnVo;
		this.createdDate = createdDate;
		this.createdBy = createdBy;
		this.updatedDate = updatedDate;
		this.updatedBy = updatedBy;
	}

	@Id
	@Column(name = "ID", unique = true, nullable = false, precision = 15, scale = 0)
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "VENDOR_ID", nullable = false)
	public VndHeader getVndHeader() {
		return this.vndHeader;
	}

	public void setVndHeader(VndHeader vndHeader) {
		this.vndHeader = vndHeader;
	}

	@Column(name = "AGN_VYPE", precision = 5, scale = 0)
	public Integer getAgnVype() {
		return this.agnVype;
	}

	public void setAgnVype(Integer agnVype) {
		this.agnVype = agnVype;
	}

	@Column(name = "AGN_NO", length = 100)
	public String getAgnNo() {
		return this.agnNo;
	}

	public void setAgnNo(String agnNo) {
		this.agnNo = agnNo;
	}

	@Column(name = "AGN_ISSUED", length = 100)
	public String getAgnIssued() {
		return this.agnIssued;
	}

	public void setAgnIssued(String agnIssued) {
		this.agnIssued = agnIssued;
	}

	@Column(name = "AGN_FROM")
	public Date getAgnFrom() {
		return this.agnFrom;
	}

	public void setAgnFrom(Date agnFrom) {
		this.agnFrom = agnFrom;
	}

	@Column(name = "AGN_VO")
	public Date getAgnVo() {
		return this.agnVo;
	}

	public void setAgnVo(Date agnVo) {
		this.agnVo = agnVo;
	}

	@Column(name = "CREATED_DATE")
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "CREATED_BY", length = 100)
	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "UPDATED_DATE")
	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Column(name = "UPDATED_BY", length = 100)
	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

}
