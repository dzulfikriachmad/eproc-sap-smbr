package com.orm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "ADM_JENIS")
public class AdmJenis implements java.io.Serializable {
	private int id;
	private String namaJenis;
	private AdmJenis parentId;
	
	public AdmJenis() {
		
	}
	public AdmJenis(int id) {
		this.setId(id);
	}
	
	public AdmJenis(int id, String namaJenis, AdmJenis parentId) {
		this.setId(id);
		this.setNamaJenis(namaJenis);
		this.setParentId(parentId);
	}
	
	@Id
	@SequenceGenerator(name="ADM_JENIS_SEQ", sequenceName="ADM_JENIS_SEQ")
	@GeneratedValue(generator="ADM_JENIS_SEQ")
	@Column(name = "ID", unique = true, nullable = false, precision = 5, scale = 0)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	@Column(name = "NAMA_JENIS", length = 100)
	public String getNamaJenis() {
		return namaJenis;
	}

	public void setNamaJenis(String namaJenis) {
		this.namaJenis = namaJenis;
	}
	

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "PARENT_JENIS")
	public AdmJenis getParentId() {
		return parentId;
	}

	public void setParentId(AdmJenis parentId) {
		this.parentId = parentId;
	}
}
