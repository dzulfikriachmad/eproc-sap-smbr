package com.orm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="ADM_JADWAL_DRT")
public class AdmJadwalDrt implements Serializable {

	@Id
	@SequenceGenerator(sequenceName="ADM_JADWAL_DRT_SEQ",name="ADM_JADWAL_DRT_SEQ")
	@GeneratedValue(generator="ADM_JADWAL_DRT_SEQ",strategy=GenerationType.SEQUENCE)
	@Column(name="ID",nullable=false,unique=true)
	private Integer id;
	
	@Column(name="NOMOR_JADWAL",length=200)
	private String nomorJadwal;
	
	@Column(name="NAMA_JADWAL",length=300)
	private String namaJadwal;
	
	@Column(name="DESKRIPSI_JADWAL",length=300)
	private String deskripsiJadwal;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TANGGAL_PEMBUKAAN")
	private Date tanggalPembukaan;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TANGGAL_PENUTUPAN")
	private Date tanggalPenutupan;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="DISTRICT")
	private AdmDistrict district;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATED_DATE")
	private Date createdDate;
	
	@Column(name="CREATED_BY")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UPDATED_DATE")
	private Date updatedDate;
	
	@Column(name="UPDATED_BY")
	private String updatedBy;
	
	public AdmJadwalDrt() {
		// TODO Auto-generated constructor stub
	}
	
	public AdmJadwalDrt(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNomorJadwal() {
		return nomorJadwal;
	}

	public void setNomorJadwal(String nomorJadwal) {
		this.nomorJadwal = nomorJadwal;
	}

	public String getNamaJadwal() {
		return namaJadwal;
	}

	public void setNamaJadwal(String namaJadwal) {
		this.namaJadwal = namaJadwal;
	}

	public String getDeskripsiJadwal() {
		return deskripsiJadwal;
	}

	public void setDeskripsiJadwal(String deskripsiJadwal) {
		this.deskripsiJadwal = deskripsiJadwal;
	}

	public Date getTanggalPembukaan() {
		return tanggalPembukaan;
	}

	public void setTanggalPembukaan(Date tanggalPembukaan) {
		this.tanggalPembukaan = tanggalPembukaan;
	}

	public Date getTanggalPenutupan() {
		return tanggalPenutupan;
	}

	public void setTanggalPenutupan(Date tanggalPenutupan) {
		this.tanggalPenutupan = tanggalPenutupan;
	}

	public AdmDistrict getDistrict() {
		return district;
	}

	public void setDistrict(AdmDistrict district) {
		this.district = district;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
}
