package com.orm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="VND_IJIN")
public class VndIjin implements Serializable {

	@Id
	@SequenceGenerator(name="TMP_VND_IJIN_SEQ",sequenceName="TMP_VND_IJIN_SEQ")
	@GeneratedValue(generator="TMP_VND_IJIN_SEQ",strategy=GenerationType.SEQUENCE)
	@Column(name="ID",nullable=false)
	private Integer id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VENDOR_ID")
	private VndHeader vndHeader;
	
	@Column(name="VND_IJIN_TIPE")
	private Integer vndIjinTipe;
	
	@Column(name="VND_IJIN_TIPE_NAME")
	private String vndIjinTipeName;
	
	@Column(name="VND_IJIN_ISSUED")
	private String vndIjinIssued;
	
	@Column(name="VND_IJIN_NO")
	private String vndIjinNo;
	
	@Temporal(TemporalType.DATE)
	@Column(name="VND_IJIN_START")
	private Date vndIjinStart;
	
	@Temporal(TemporalType.DATE)
	@Column(name="VND_IJIN_END")
	private Date vndIjinEnd;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATED_DATE")
	private Date createdDate;
	
	@Column(name="CREATED_BY")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UPDATED_DATE")
	private Date updatedDate;
	
	@Column(name="UPDATED_BY")
	private String updatedBy;
	
	public VndIjin() {
		// TODO Auto-generated constructor stub
	}
	
	public VndIjin(Integer id) {
		this.id = id;
	}
	
	public VndIjin(TmpVndIjin ijin) {
		this.id = ijin.getId();
		this.vndIjinTipe = ijin.getVndIjinTipe();
		this.vndIjinEnd = ijin.getVndIjinEnd();
		this.vndIjinIssued = ijin.getVndIjinIssued();
		this.vndIjinNo = ijin.getVndIjinNo();
		this.vndIjinStart = ijin.getVndIjinStart();
		this.createdBy = ijin.getCreatedBy();
		this.createdDate = ijin.getCreatedDate();
		this.updatedBy = ijin.getUpdatedBy();
		this.updatedDate = ijin.getUpdatedDate();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public VndHeader getVndHeader() {
		return vndHeader;
	}

	public void setVndHeader(VndHeader vndHeader) {
		this.vndHeader = vndHeader;
	}

	public Integer getVndIjinTipe() {
		return vndIjinTipe;
	}

	public void setVndIjinTipe(Integer vndIjinTipe) {
		this.vndIjinTipe = vndIjinTipe;
	}

	public String getVndIjinTipeName() {
		return vndIjinTipeName;
	}

	public void setVndIjinTipeName(String vndIjinTipeName) {
		this.vndIjinTipeName = vndIjinTipeName;
	}

	public String getVndIjinIssued() {
		return vndIjinIssued;
	}

	public void setVndIjinIssued(String vndIjinIssued) {
		this.vndIjinIssued = vndIjinIssued;
	}

	public String getVndIjinNo() {
		return vndIjinNo;
	}

	public void setVndIjinNo(String vndIjinNo) {
		this.vndIjinNo = vndIjinNo;
	}

	public Date getVndIjinStart() {
		return vndIjinStart;
	}

	public void setVndIjinStart(Date vndIjinStart) {
		this.vndIjinStart = vndIjinStart;
	}

	public Date getVndIjinEnd() {
		return vndIjinEnd;
	}

	public void setVndIjinEnd(Date vndIjinEnd) {
		this.vndIjinEnd = vndIjinEnd;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	
	
}
