package com.orm.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="ADM_USER_MANAGER")
public class AdmUserManager {
	
	private Long id;
	private AdmUser admUser;
	private AdmJenis admJenis;
	private BigDecimal minimalValue;
	private Integer jabatan;
	private Integer kodeSatker1;
	private Integer kodeSatker2;
	private Integer kodeSatker3;
	private Integer kodeSatker4;
	private Integer kodeSatker5;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	
	
	
	@Id
	@SequenceGenerator(name="ADM_USER_MANAGER_SEQ", sequenceName="ADM_USER_MANAGER_SEQ")
	@GeneratedValue(generator="ADM_USER_MANAGER_SEQ",strategy=GenerationType.SEQUENCE)
	@Column(name="ID")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ADM_USER")
	public AdmUser getAdmUser() {
		return admUser;
	}
	public void setAdmUser(AdmUser admUser) {
		this.admUser = admUser;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ADM_JENIS")
	public AdmJenis getAdmJenis() {
		return admJenis;
	}
	public void setAdmJenis(AdmJenis admJenis) {
		this.admJenis = admJenis;
	}

	@Column(name = "CREATED_BY", length=100)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column(name = "CREATED_DATE")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name = "UPDATED_BY", length=100)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Column(name = "UPDATED_DATE")
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	@Column(name = "MINIMAL_VALUE", precision = 30, scale = 5)
	public BigDecimal getMinimalValue() {
		return minimalValue;
	}
	public void setMinimalValue(BigDecimal minimalValue) {
		this.minimalValue = minimalValue;
	}
	
	/*
	 * 1 = Senior Manager
	 * 2 = Manager
	 * 3 = Specialist
	 */
	@Column(name = "JABATAN", length=1)
	public Integer getJabatan() {
		return jabatan;
	}
	public void setJabatan(Integer jabatan) {
		this.jabatan = jabatan;
	}
	
	@Column(name = "KODE_SATKER_1", length=10)
	public Integer getKodeSatker1() {
		return kodeSatker1;
	}
	public void setKodeSatker1(Integer kodeSatker1) {
		this.kodeSatker1 = kodeSatker1;
	}
	
	@Column(name = "KODE_SATKER_2", length=10)
	public Integer getKodeSatker2() {
		return kodeSatker2;
	}
	public void setKodeSatker2(Integer kodeSatker2) {
		this.kodeSatker2 = kodeSatker2;
	}
	
	@Column(name = "KODE_SATKER_3", length=10)
	public Integer getKodeSatker3() {
		return kodeSatker3;
	}
	public void setKodeSatker3(Integer kodeSatker3) {
		this.kodeSatker3 = kodeSatker3;
	}
	
	@Column(name = "KODE_SATKER_4", length=10)
	public Integer getKodeSatker4() {
		return kodeSatker4;
	}
	public void setKodeSatker4(Integer kodeSatker4) {
		this.kodeSatker4 = kodeSatker4;
	}
	
	@Column(name = "KODE_SATKER_5", length=10)
	public Integer getKodeSatker5() {
		return kodeSatker5;
	}
	public void setKodeSatker5(Integer kodeSatker5) {
		this.kodeSatker5 = kodeSatker5;
	}
	
	

}
