package com.orm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="PRC_ANGGARAN_JDE_DETAIL")
public class PrcAnggaranJdeDetail implements Serializable {

	@Id
	@SequenceGenerator(name="PRC_ANGGARAN_JDE_SEQ",sequenceName="PRC_ANGGARAN_JDE_SEQ")
	@GeneratedValue(generator="PRC_ANGGARAN_JDE_SEQ",strategy=GenerationType.SEQUENCE)
	@Column(name="ID")
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PPM_ID")
	private PrcMainHeader prcMainHeader;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ANGGARAN_HEADER")
	private PrcAnggaranJdeHeader prcAnggaranJdeHeader;
	
	@Column(name="RKKCOO")
	private String rkkcoo;
	
	@Column(name="RKDCTO")
	private String rkdcto;
	
	@Column(name="RKDOCO")
	private String rkdoco;
	
	@Column(name="RKLNID")
	private Integer rklnid;
	
	/**
	 * budget id
	 */
	@Column(name="RKAID")
	private String rkaid;
	
	/**
	 * nilai permintaan anggaran
	 */
	@Column(name="RKAAP")
	private BigDecimal rkaap;
	
	@Column(name="RKAEA")
	private BigDecimal rkaea;
	
	@Column(name="RKACM1")
	private BigDecimal rkacm1;
	
	@Column(name="RKAA")
	private BigDecimal rkaa;
	
	@Column(name="RKDL01")
	private String rkdl01;
	
	@Column(name="RKDL02")
	private String rkdl02;
	
	@Column(name="RKAPDJ")
	private Date rkapdj;
	
	@Column(name="RKAPFL")
	private String rkapfl;
	
	@Column(name="RKTORG")
	private String rktorg;
	
	@Column(name="RKUSER")
	private String rkuser;
	
	@Column(name="RKJOBN")
	private String rkjobn;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="RKUPMJ")
	private Date rkupmj;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="RKUPMT")
	private Date rkupmt;
	
	@Column(name="RKAPID")
	private String rkapid;
	
	@Column(name="KETERANGAN")
	private String keterangan;
	
	/**
	 * r=revisi
	 */
	@Column(name="STATUS")
	private String status;
	
	public PrcAnggaranJdeDetail() {
		// TODO Auto-generated constructor stub
	}
	
	public PrcAnggaranJdeDetail(PrcAnggaranJdeDetail d) {
		this.keterangan = d.getKeterangan();
		this.rkaa = d.getRkaa();
		this.rkaap = d.getRkaap();
		this.rkacm1 = d.getRkacm1();
		this.rkaea = d.getRkaea();
		this.rkaid = d.getRkaid();
		this.rkapdj = d.getRkapdj();
		this.rkapfl = d.getRkapfl();
		this.rkapid = d.getRkapid();
		this.rkdcto = d.getRkdcto();
		this.rkdl01 = d.getRkdl01();
		this.rkdl02 = d.getRkdl02();
		this.rkdoco = d.getRkdoco();
		this.rkjobn = d.getRkjobn();
		this.rkkcoo = d.getRkkcoo();
		this.rklnid = d.getRklnid();
		this.rktorg = d.getRktorg();
		this.rkupmj = d.getRkupmj();
		this.rkupmt = d.getRkupmt();
		this.rkuser = d.getRkuser();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PrcMainHeader getPrcMainHeader() {
		return prcMainHeader;
	}

	public void setPrcMainHeader(PrcMainHeader prcMainHeader) {
		this.prcMainHeader = prcMainHeader;
	}

	public PrcAnggaranJdeHeader getPrcAnggaranJdeHeader() {
		return prcAnggaranJdeHeader;
	}

	public void setPrcAnggaranJdeHeader(PrcAnggaranJdeHeader prcAnggaranJdeHeader) {
		this.prcAnggaranJdeHeader = prcAnggaranJdeHeader;
	}

	public String getRkkcoo() {
		return rkkcoo;
	}

	public void setRkkcoo(String rkkcoo) {
		this.rkkcoo = rkkcoo;
	}

	public String getRkdcto() {
		return rkdcto;
	}

	public void setRkdcto(String rkdcto) {
		this.rkdcto = rkdcto;
	}

	public String getRkdoco() {
		return rkdoco;
	}

	public void setRkdoco(String rkdoco) {
		this.rkdoco = rkdoco;
	}

	public Integer getRklnid() {
		return rklnid;
	}

	public void setRklnid(Integer rklnid) {
		this.rklnid = rklnid;
	}

	public String getRkaid() {
		return rkaid;
	}

	public void setRkaid(String rkaid) {
		this.rkaid = rkaid;
	}

	public BigDecimal getRkaap() {
		return rkaap;
	}

	public void setRkaap(BigDecimal rkaap) {
		this.rkaap = rkaap;
	}

	public BigDecimal getRkaea() {
		return rkaea;
	}

	public void setRkaea(BigDecimal rkaea) {
		this.rkaea = rkaea;
	}

	public BigDecimal getRkacm1() {
		return rkacm1;
	}

	public void setRkacm1(BigDecimal rkacm1) {
		this.rkacm1 = rkacm1;
	}

	public BigDecimal getRkaa() {
		return rkaa;
	}

	public void setRkaa(BigDecimal rkaa) {
		this.rkaa = rkaa;
	}

	public String getRkdl01() {
		return rkdl01;
	}

	public void setRkdl01(String rkdl01) {
		this.rkdl01 = rkdl01;
	}

	public String getRkdl02() {
		return rkdl02;
	}

	public void setRkdl02(String rkdl02) {
		this.rkdl02 = rkdl02;
	}

	public Date getRkapdj() {
		return rkapdj;
	}

	public void setRkapdj(Date rkapdj) {
		this.rkapdj = rkapdj;
	}

	public String getRkapfl() {
		return rkapfl;
	}

	public void setRkapfl(String rkapfl) {
		this.rkapfl = rkapfl;
	}

	public String getRktorg() {
		return rktorg;
	}

	public void setRktorg(String rktorg) {
		this.rktorg = rktorg;
	}

	public String getRkuser() {
		return rkuser;
	}

	public void setRkuser(String rkuser) {
		this.rkuser = rkuser;
	}

	public String getRkjobn() {
		return rkjobn;
	}

	public void setRkjobn(String rkjobn) {
		this.rkjobn = rkjobn;
	}

	public Date getRkupmj() {
		return rkupmj;
	}

	public void setRkupmj(Date rkupmj) {
		this.rkupmj = rkupmj;
	}

	public Date getRkupmt() {
		return rkupmt;
	}

	public void setRkupmt(Date rkupmt) {
		this.rkupmt = rkupmt;
	}

	public String getRkapid() {
		return rkapid;
	}

	public void setRkapid(String rkapid) {
		this.rkapid = rkapid;
	}

	public String getKeterangan() {
		return keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
