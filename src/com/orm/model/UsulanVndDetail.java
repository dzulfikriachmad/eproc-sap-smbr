package com.orm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="USULAN_VND_DETAIL")
public class UsulanVndDetail implements Serializable {

	private Integer id;
	private UsulanVnd usulan;
	private TmpVndHeader vendor;
	private Integer status;
	private String vendorAccountGroup;

	public UsulanVndDetail() {
		// TODO Auto-generated constructor stub
	}

	@Id
	@SequenceGenerator(name="USULAN_VND_DET_SEQ",sequenceName="USULAN_VND_DET_SEQ")
	@GeneratedValue(generator="USULAN_VND_DET_SEQ",strategy=GenerationType.SEQUENCE)
	@Column(name="ID")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="USULAN")
	public UsulanVnd getUsulan() {
		return usulan;
	}

	public void setUsulan(UsulanVnd usulan) {
		this.usulan = usulan;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VENDOR_ID")
	public TmpVndHeader getVendor() {
		return vendor;
	}

	public void setVendor(TmpVndHeader vendor) {
		this.vendor = vendor;
	}

	@Column(name="STATUS")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Column(name="VENDOR_ACCOUNT_GROUP")
	public String getVendorAccountGroup() {
		return vendorAccountGroup;
	}

	public void setVendorAccountGroup(String vendorAccountGroup) {
		this.vendorAccountGroup = vendorAccountGroup;
	}
}
