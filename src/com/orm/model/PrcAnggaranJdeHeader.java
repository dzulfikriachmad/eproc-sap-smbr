package com.orm.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
@Table(name="PRC_ANGGARAN_JDE_HEADER")
public class PrcAnggaranJdeHeader implements Serializable {

	@GenericGenerator(name = "generator", strategy = "foreign", parameters = @Parameter(name = "property", value = "prcMainHeader"))
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "PPM_ID", unique = true, nullable = false, precision = 30, scale = 0)
	private Long ppmId;
	
	@OneToOne(fetch=FetchType.LAZY)
	@PrimaryKeyJoinColumn
	private PrcMainHeader prcMainHeader;
	
	@Column(name="KAKCOO")
	private String kakcoo;
	
	@Column(name="KADCTO")
	private String kadcto;
	
	@Column(name="KADOCO")
	private String kadoco;
	
	@Column(name="KAAID")
	private String kaaid;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="KATRDJ")
	private Date katrdj;
	
	@Column(name="KAAPDJ")
	private String kaapdj;
	
	@Column(name="KACO")
	private String kaco;
	
	@Column(name="KAKCO")
	private String kakco;
	
	@Column(name="KADCT")
	private String kadct;
	
	@Column(name="KADOC")
	private Long kadoc;
	
	@Column(name="KABASEDJ")
	private String kabasedj;
	
	@Column(name="KAMCU")
	private String kamcu;
	
	@Column(name="KAMMCU")
	private String kammcu;
	
	@Column(name="KAAN8")
	private String kaan8;
	
	@Column(name="KAAN82")
	private String kaan82;
	
	@Column(name="KAAN83")
	private String kaan83;
	
	@Column(name="KADSC1")
	private String kadsc1;
	
	@Column(name="KAJJ")
	private String kajj;
	
	@Column(name="KAAPFL")
	private String kaapfl;
	
	@Column(name="KASKA")
	private String kaska;
	
	@Column(name="KAHST")
	private String kahst;
	
	@Column(name="KAAAP")
	private Integer kaaap;
	
	@Column(name="KAEA")
	private BigDecimal kaea;
	
	@Column(name="KAACM1")
	private BigDecimal kaacm1;
	
	@Column(name="KAAA")
	private BigDecimal kaaa;
	
	@Column(name="KATORG")
	private String katorg;
	
	@Column(name="KAUSER")
	private String kauser;
	
	@Column(name="KAJOBN")
	private String kajobn;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="KUPMJ")
	private Date kupmj;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="KUPMT")
	private Date kupmt;
	
	@Column(name="KAPID")
	private String kapid;
	
	@Column(name="CREATED_BY")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATED_DATE")
	private Date createdDate;
	
	@Column(name="UPDATED_BY")
	private String updatedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UPDATED_DATE")
	private Date updatedDate;
	
	public PrcAnggaranJdeHeader() {
		// TODO Auto-generated constructor stub
	}
	
	public PrcAnggaranJdeHeader(PrcAnggaranJdeHeader a){
		this.kaaa = a.getKaaa();
		this.kaaap = a.getKaaap();
		this.kaacm1 = a.getKaacm1();
		this.kaaid = a.getKaaid();
		this.kaan8 = a.getKaan8();
		this.kaan82 = a.getKaan82();
		this.kaan83 = a.getKaan83();
		this.kaapdj = a.getKaapdj();
		this.kaapfl = a.getKaapfl();
		this.kabasedj = a.getKabasedj();
		this.kaco = a.getKaco();
		this.kadct = a.getKadct();
		this.kadcto = a.getKadcto();
		this.kadoc = a.getKadoc();
		this.kadoco = a.getKadoco();
		this.kadsc1 = a.getKadsc1();
		this.kaea = a.getKaea();
		this.kahst = a.getKahst();
		this.kajj = a.getKajj();
		this.kajobn = a.getKajobn();
		this.kakco = a.getKakco();
		this.kakcoo = a.getKakcoo();
		this.kamcu = a.getKammcu();
		this.kammcu = a.getKammcu();
		this.kapid = a.getKapid();
		this.kaska = a.getKaska();
		this.katorg = a.getKatorg();
		this.katrdj = a.getKatrdj();
		this.kauser = a.getKauser();
		this.kupmj = a.getKupmj();
		this.kupmt = a.getKupmt();
	}
	
	public PrcAnggaranJdeHeader(Long ppmId) {
		this.ppmId = ppmId;
	}

	public Long getPpmId() {
		return ppmId;
	}

	public void setPpmId(Long ppmId) {
		this.ppmId = ppmId;
	}

	public String getKakcoo() {
		return kakcoo;
	}

	public void setKakcoo(String kakcoo) {
		this.kakcoo = kakcoo;
	}

	public String getKadcto() {
		return kadcto;
	}

	public void setKadcto(String kadcto) {
		this.kadcto = kadcto;
	}

	public String getKadoco() {
		return kadoco;
	}

	public void setKadoco(String kadoco) {
		this.kadoco = kadoco;
	}

	public String getKaaid() {
		return kaaid;
	}

	public void setKaaid(String kaaid) {
		this.kaaid = kaaid;
	}

	public Date getKatrdj() {
		return katrdj;
	}

	public void setKatrdj(Date katrdj) {
		this.katrdj = katrdj;
	}

	public String getKaapdj() {
		return kaapdj;
	}

	public void setKaapdj(String kaapdj) {
		this.kaapdj = kaapdj;
	}

	public String getKaco() {
		return kaco;
	}

	public void setKaco(String kaco) {
		this.kaco = kaco;
	}

	public String getKakco() {
		return kakco;
	}

	public void setKakco(String kakco) {
		this.kakco = kakco;
	}

	public String getKadct() {
		return kadct;
	}

	public void setKadct(String kadct) {
		this.kadct = kadct;
	}

	public Long getKadoc() {
		return kadoc;
	}

	public void setKadoc(Long kadoc) {
		this.kadoc = kadoc;
	}

	public String getKabasedj() {
		return kabasedj;
	}

	public void setKabasedj(String kabasedj) {
		this.kabasedj = kabasedj;
	}

	public String getKamcu() {
		return kamcu;
	}

	public void setKamcu(String kamcu) {
		this.kamcu = kamcu;
	}

	public String getKammcu() {
		return kammcu;
	}

	public void setKammcu(String kammcu) {
		this.kammcu = kammcu;
	}

	public String getKaan8() {
		return kaan8;
	}

	public void setKaan8(String kaan8) {
		this.kaan8 = kaan8;
	}

	public String getKaan82() {
		return kaan82;
	}

	public void setKaan82(String kaan82) {
		this.kaan82 = kaan82;
	}

	public String getKaan83() {
		return kaan83;
	}

	public void setKaan83(String kaan83) {
		this.kaan83 = kaan83;
	}

	public String getKadsc1() {
		return kadsc1;
	}

	public void setKadsc1(String kadsc1) {
		this.kadsc1 = kadsc1;
	}

	public String getKajj() {
		return kajj;
	}

	public void setKajj(String kajj) {
		this.kajj = kajj;
	}

	public String getKaapfl() {
		return kaapfl;
	}

	public void setKaapfl(String kaapfl) {
		this.kaapfl = kaapfl;
	}

	public String getKaska() {
		return kaska;
	}

	public void setKaska(String kaska) {
		this.kaska = kaska;
	}

	public String getKahst() {
		return kahst;
	}

	public void setKahst(String kahst) {
		this.kahst = kahst;
	}

	public Integer getKaaap() {
		return kaaap;
	}

	public void setKaaap(Integer kaaap) {
		this.kaaap = kaaap;
	}

	public BigDecimal getKaea() {
		return kaea;
	}

	public void setKaea(BigDecimal kaea) {
		this.kaea = kaea;
	}

	public BigDecimal getKaacm1() {
		return kaacm1;
	}

	public void setKaacm1(BigDecimal kaacm1) {
		this.kaacm1 = kaacm1;
	}

	public BigDecimal getKaaa() {
		return kaaa;
	}

	public void setKaaa(BigDecimal kaaa) {
		this.kaaa = kaaa;
	}

	public String getKatorg() {
		return katorg;
	}

	public void setKatorg(String katorg) {
		this.katorg = katorg;
	}

	public String getKauser() {
		return kauser;
	}

	public void setKauser(String kauser) {
		this.kauser = kauser;
	}

	public String getKajobn() {
		return kajobn;
	}

	public void setKajobn(String kajobn) {
		this.kajobn = kajobn;
	}

	public Date getKupmj() {
		return kupmj;
	}

	public void setKupmj(Date kupmj) {
		this.kupmj = kupmj;
	}

	public Date getKupmt() {
		return kupmt;
	}

	public void setKupmt(Date kupmt) {
		this.kupmt = kupmt;
	}

	public String getKapid() {
		return kapid;
	}

	public void setKapid(String kapid) {
		this.kapid = kapid;
	}

	public PrcMainHeader getPrcMainHeader() {
		return prcMainHeader;
	}

	public void setPrcMainHeader(PrcMainHeader prcMainHeader) {
		this.prcMainHeader = prcMainHeader;
	}
	
	
}
