package com.orm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="ADM_DELEGASI")
public class AdmDelegasi implements Serializable {

	@Id
	@SequenceGenerator(name="ADM_DELEGASI_SEQ", sequenceName="ADM_DELEGASI_SEQ")
	@GeneratedValue(generator="ADM_DELEGASI_SEQ",strategy=GenerationType.SEQUENCE)
	@Column(name="ID",unique=true)
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="DARI")
	private AdmUser dari;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="KEPADA")
	private AdmUser kepada;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TGL_MULAI")
	private Date tglMulai;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TGL_BERAKHIR")
	private Date tglBerakhir;
	
	@Column(name="STATUS")
	private Integer status;
	
	@Column(name="KETERANGAN")
	private String keterangan;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATED_DATE")
	private Date createdDate;
	
	@Column(name="CREATED_BY")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UPDATED_DATE")
	private Date updatedDate;
	
	@Column(name="UPDATED_BY")
	private String updatedBy;
	
	public AdmDelegasi() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AdmUser getDari() {
		return dari;
	}

	public void setDari(AdmUser dari) {
		this.dari = dari;
	}

	public AdmUser getKepada() {
		return kepada;
	}

	public void setKepada(AdmUser kepada) {
		this.kepada = kepada;
	}

	public Date getTglMulai() {
		return tglMulai;
	}

	public void setTglMulai(Date tglMulai) {
		this.tglMulai = tglMulai;
	}

	public Date getTglBerakhir() {
		return tglBerakhir;
	}

	public void setTglBerakhir(Date tglBerakhir) {
		this.tglBerakhir = tglBerakhir;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getKeterangan() {
		return keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
}
