package com.orm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="PANITIA_PENGADAAN_DETAIL")
public class PanitiaPengadaanDetail implements Serializable {

	@Id
	@SequenceGenerator(name="PANITIA_PENGDET_SEQ",sequenceName="PANITIA_PENGDET_SEQ")
	@GeneratedValue(generator="PANITIA_PENGDET_SEQ",strategy=GenerationType.SEQUENCE)
	@Column(name="ID")
	private Integer id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="PANITIA")
	private PanitiaPengadaan panitia;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ANGGOTA")
	private AdmUser anggota;
	
	@Column(name="POSISI")
	private Integer posisi;
	
	@Column(name="STATUS")
	private Integer status;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATED_DATE")
	private Date createdDate;
	
	@Column(name="CREATED_BY")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UPDATED_DATE")
	private Date updatedDate;
	
	@Column(name="UPDATED_BY")
	private String updatedBy;
	
	public PanitiaPengadaanDetail() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public PanitiaPengadaan getPanitia() {
		return panitia;
	}

	public void setPanitia(PanitiaPengadaan panitia) {
		this.panitia = panitia;
	}

	public AdmUser getAnggota() {
		return anggota;
	}

	public void setAnggota(AdmUser anggota) {
		this.anggota = anggota;
	}

	public Integer getPosisi() {
		return posisi;
	}

	public void setPosisi(Integer posisi) {
		this.posisi = posisi;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
}
