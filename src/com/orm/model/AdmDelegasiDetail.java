package com.orm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="ADM_DELEGASI_DETAIL")
public class AdmDelegasiDetail implements Serializable {

	@Id
	@SequenceGenerator(name="ADM_DEL_DET_SEQ",sequenceName="ADM_DEL_DET_SEQ")
	@GeneratedValue(generator="ADM_DEL_DET_SEQ",strategy=GenerationType.SEQUENCE)
	@Column(name="ID",unique=true)
	private Long id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="DELEGASI")
	private AdmDelegasi admDelegasi;
	
	@Column(name="PPM_ID")
	private Long ppmId;
	
	@Column(name="CONTRACT_ID")
	private Long contractId;
	
	public AdmDelegasiDetail() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AdmDelegasi getAdmDelegasi() {
		return admDelegasi;
	}

	public void setAdmDelegasi(AdmDelegasi admDelegasi) {
		this.admDelegasi = admDelegasi;
	}

	public Long getPpmId() {
		return ppmId;
	}

	public void setPpmId(Long ppmId) {
		this.ppmId = ppmId;
	}

	public Long getContractId() {
		return contractId;
	}

	public void setContractId(Long contractId) {
		this.contractId = contractId;
	}
	
}
