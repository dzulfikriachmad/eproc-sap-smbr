package com.orm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="VND_TAMBAHAN")
public class VndTambahan implements Serializable {

	private Integer id;
	private VndHeader vendorId;
	private String tipe;
	private String nama;
	private String alamat;
	private AdmCountry negara;
	private String kodePos;
	private String hubunganKerja;
	private String kualifikasi;
	
	public VndTambahan() {
		// TODO Auto-generated constructor stub
	}

	public VndTambahan(TmpVndTambahan vt) {
		this.id = vt.getId();
		this.tipe = vt.getTipe();
		this.nama = vt.getNama();
		this.alamat = vt.getAlamat();
		this.negara = vt.getNegara();
		this.kodePos = vt.getKodePos();
		this.hubunganKerja = vt.getHubunganKerja();
		this.kualifikasi = vt.getKualifikasi();
	}

	@Id
	@Column(name="ID")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="VENDOR_ID")
	public VndHeader getVendorId() {
		return vendorId;
	}

	public void setVendorId(VndHeader vendorId) {
		this.vendorId = vendorId;
	}

	@Column(name="TIPE")
	public String getTipe() {
		return tipe;
	}

	public void setTipe(String tipe) {
		this.tipe = tipe;
	}

	@Column(name="NAMA")
	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	@Column(name="ALAMAT")
	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="NEGARA")
	public AdmCountry getNegara() {
		return negara;
	}

	public void setNegara(AdmCountry negara) {
		this.negara = negara;
	}

	@Column(name="KODE_POS")
	public String getKodePos() {
		return kodePos;
	}

	public void setKodePos(String kodePos) {
		this.kodePos = kodePos;
	}

	@Column(name="HUBUNGAN_KERJA")
	public String getHubunganKerja() {
		return hubunganKerja;
	}

	public void setHubunganKerja(String hubunganKerja) {
		this.hubunganKerja = hubunganKerja;
	}

	@Column(name="KUALIFIKASI")
	public String getKualifikasi() {
		return kualifikasi;
	}

	public void setKualifikasi(String kualifikasi) {
		this.kualifikasi = kualifikasi;
	}
	
}
