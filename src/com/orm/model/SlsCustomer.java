package com.orm.model;

// Generated Feb 26, 2012 1:56:14 PM by Hibernate Tools 3.4.0.Beta1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * SlsCustomer generated by hbm2java
 */
@Entity
@Table(name = "SLS_CUSTOMER")
public class SlsCustomer implements java.io.Serializable {

	private String customerNo;
	private AdmCurrency admCurrency;
	private String customerName;
	private String companyCode;
	private String custContactName;
	private String custContactPhone;
	private String custTelp;
	private String custFax;
	private String custEmail;
	private String custInvoiceAddress;
	private String custShipmentAddress;
	private String defaultInvoiceTitle;
	private String custRemark;
	private String defaultSalesNotes;
	private String defaultShipmentMark;
	private Date createdDate;
	private String createdBy;
	private Date updatedDate;
	private String updatedBy;
	private Set<SlsOrderRequest> slsOrderRequests = new HashSet<SlsOrderRequest>(
			0);
	private Set<SlsSalesOrder> slsSalesOrders = new HashSet<SlsSalesOrder>(0);
	private Set<SlsDeliveryOrder> slsDeliveryOrders = new HashSet<SlsDeliveryOrder>(
			0);

	public SlsCustomer() {
	}

	public SlsCustomer(String customerNo) {
		this.customerNo = customerNo;
	}

	public SlsCustomer(String customerNo, AdmCurrency admCurrency,
			String customerName, String companyCode, String custContactName,
			String custContactPhone, String custTelp, String custFax,
			String custEmail, String custInvoiceAddress,
			String custShipmentAddress, String defaultInvoiceTitle,
			String custRemark, String defaultSalesNotes,
			String defaultShipmentMark, Date createdDate, String createdBy,
			Date updatedDate, String updatedBy,
			Set<SlsOrderRequest> slsOrderRequests,
			Set<SlsSalesOrder> slsSalesOrders,
			Set<SlsDeliveryOrder> slsDeliveryOrders) {
		this.customerNo = customerNo;
		this.admCurrency = admCurrency;
		this.customerName = customerName;
		this.companyCode = companyCode;
		this.custContactName = custContactName;
		this.custContactPhone = custContactPhone;
		this.custTelp = custTelp;
		this.custFax = custFax;
		this.custEmail = custEmail;
		this.custInvoiceAddress = custInvoiceAddress;
		this.custShipmentAddress = custShipmentAddress;
		this.defaultInvoiceTitle = defaultInvoiceTitle;
		this.custRemark = custRemark;
		this.defaultSalesNotes = defaultSalesNotes;
		this.defaultShipmentMark = defaultShipmentMark;
		this.createdDate = createdDate;
		this.createdBy = createdBy;
		this.updatedDate = updatedDate;
		this.updatedBy = updatedBy;
		this.slsOrderRequests = slsOrderRequests;
		this.slsSalesOrders = slsSalesOrders;
		this.slsDeliveryOrders = slsDeliveryOrders;
	}

	@Id
	@Column(name = "CUSTOMER_NO", unique = true, nullable = false, length = 100)
	public String getCustomerNo() {
		return this.customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "DEFAULT_CURRENCY")
	public AdmCurrency getAdmCurrency() {
		return this.admCurrency;
	}

	public void setAdmCurrency(AdmCurrency admCurrency) {
		this.admCurrency = admCurrency;
	}

	@Column(name = "CUSTOMER_NAME", length = 200)
	public String getCustomerName() {
		return this.customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	@Column(name = "COMPANY_CODE", length = 100)
	public String getCompanyCode() {
		return this.companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	@Column(name = "CUST_CONTACT_NAME", length = 200)
	public String getCustContactName() {
		return this.custContactName;
	}

	public void setCustContactName(String custContactName) {
		this.custContactName = custContactName;
	}

	@Column(name = "CUST_CONTACT_PHONE", length = 100)
	public String getCustContactPhone() {
		return this.custContactPhone;
	}

	public void setCustContactPhone(String custContactPhone) {
		this.custContactPhone = custContactPhone;
	}

	@Column(name = "CUST_TELP", length = 100)
	public String getCustTelp() {
		return this.custTelp;
	}

	public void setCustTelp(String custTelp) {
		this.custTelp = custTelp;
	}

	@Column(name = "CUST_FAX", length = 100)
	public String getCustFax() {
		return this.custFax;
	}

	public void setCustFax(String custFax) {
		this.custFax = custFax;
	}

	@Column(name = "CUST_EMAIL", length = 100)
	public String getCustEmail() {
		return this.custEmail;
	}

	public void setCustEmail(String custEmail) {
		this.custEmail = custEmail;
	}

	@Column(name = "CUST_INVOICE_ADDRESS", length = 500)
	public String getCustInvoiceAddress() {
		return this.custInvoiceAddress;
	}

	public void setCustInvoiceAddress(String custInvoiceAddress) {
		this.custInvoiceAddress = custInvoiceAddress;
	}

	@Column(name = "CUST_SHIPMENT_ADDRESS", length = 500)
	public String getCustShipmentAddress() {
		return this.custShipmentAddress;
	}

	public void setCustShipmentAddress(String custShipmentAddress) {
		this.custShipmentAddress = custShipmentAddress;
	}

	@Column(name = "DEFAULT_INVOICE_TITLE", length = 200)
	public String getDefaultInvoiceTitle() {
		return this.defaultInvoiceTitle;
	}

	public void setDefaultInvoiceTitle(String defaultInvoiceTitle) {
		this.defaultInvoiceTitle = defaultInvoiceTitle;
	}

	@Column(name = "CUST_REMARK", length = 200)
	public String getCustRemark() {
		return this.custRemark;
	}

	public void setCustRemark(String custRemark) {
		this.custRemark = custRemark;
	}

	@Column(name = "DEFAULT_SALES_NOTES", length = 100)
	public String getDefaultSalesNotes() {
		return this.defaultSalesNotes;
	}

	public void setDefaultSalesNotes(String defaultSalesNotes) {
		this.defaultSalesNotes = defaultSalesNotes;
	}

	@Column(name = "DEFAULT_SHIPMENT_MARK", length = 100)
	public String getDefaultShipmentMark() {
		return this.defaultShipmentMark;
	}

	public void setDefaultShipmentMark(String defaultShipmentMark) {
		this.defaultShipmentMark = defaultShipmentMark;
	}

	@Column(name = "CREATED_DATE")
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "CREATED_BY", length = 100)
	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "UPDATED_DATE")
	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Column(name = "UPDATED_BY", length = 100)
	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "slsCustomer")
	public Set<SlsOrderRequest> getSlsOrderRequests() {
		return this.slsOrderRequests;
	}

	public void setSlsOrderRequests(Set<SlsOrderRequest> slsOrderRequests) {
		this.slsOrderRequests = slsOrderRequests;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "slsCustomer")
	public Set<SlsSalesOrder> getSlsSalesOrders() {
		return this.slsSalesOrders;
	}

	public void setSlsSalesOrders(Set<SlsSalesOrder> slsSalesOrders) {
		this.slsSalesOrders = slsSalesOrders;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "slsCustomer")
	public Set<SlsDeliveryOrder> getSlsDeliveryOrders() {
		return this.slsDeliveryOrders;
	}

	public void setSlsDeliveryOrders(Set<SlsDeliveryOrder> slsDeliveryOrders) {
		this.slsDeliveryOrders = slsDeliveryOrders;
	}

}
