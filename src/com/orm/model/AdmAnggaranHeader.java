package com.orm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="ADM_ANGGARAN_HEADER")
public class AdmAnggaranHeader implements Serializable {

	private Integer id;
	private String nomorAnggaran;
	private Integer tahunAnggaran;
	private AdmDept admDept;
	private Integer status;
	private String description;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	
	public AdmAnggaranHeader() {
		// TODO Auto-generated constructor stub
	}
	
	public AdmAnggaranHeader(Integer id){
		this.id = id;
	}

	@Id
	@SequenceGenerator(name="ADM_ANGGARAN_HEAD_SEQ",sequenceName="ADM_ANGGARAN_HEAD_SEQ")
	@GeneratedValue(generator="ADM_ANGGARAN_HEAD_SEQ",strategy=GenerationType.SEQUENCE)
	@Column(name="ID",nullable=false,unique=true)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="NOMOR_ANGGARAN")
	public String getNomorAnggaran() {
		return nomorAnggaran;
	}

	public void setNomorAnggaran(String nomorAnggaran) {
		this.nomorAnggaran = nomorAnggaran;
	}

	@Column(name="TAHUN_ANGGARAN")
	public Integer getTahunAnggaran() {
		return tahunAnggaran;
	}

	public void setTahunAnggaran(Integer tahunAnggaran) {
		this.tahunAnggaran = tahunAnggaran;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ADM_DEPT")
	public AdmDept getAdmDept() {
		return admDept;
	}

	public void setAdmDept(AdmDept admDept) {
		this.admDept = admDept;
	}

	@Column(name="STATUS")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Column(name="DESCRIPTION")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@Column(name="CREATED_BY")
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATED_DATE")
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}


	@Column(name="UPDATED_BY")
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UPDATED_DATE")
	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}
