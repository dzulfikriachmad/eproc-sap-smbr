package com.orm.model;

// Generated Feb 26, 2012 1:56:14 PM by Hibernate Tools 3.4.0.Beta1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * AdmCompanyType generated by hbm2java
 */
@Entity
@Table(name = "ADM_COMPANY_TYPE")
public class AdmCompanyType implements java.io.Serializable {

	private int id;
	private String companyTypeName;
	private Date createdDate;
	private String createdBy;
	private Date updatedDate;
	private String updatedBy;
	private Set<TmpVndCompanyType> tmpVndCompanyTypes = new HashSet<TmpVndCompanyType>(
			0);

	public AdmCompanyType() {
	}

	public AdmCompanyType(int id) {
		this.id = id;
	}

	public AdmCompanyType(int id, String companyTypeName, Date createdDate,
			String createdBy, Date updatedDate, String updatedBy,
			Set<TmpVndCompanyType> tmpVndCompanyTypes) {
		this.id = id;
		this.companyTypeName = companyTypeName;
		this.createdDate = createdDate;
		this.createdBy = createdBy;
		this.updatedDate = updatedDate;
		this.updatedBy = updatedBy;
		this.tmpVndCompanyTypes = tmpVndCompanyTypes;
	}

	@Id
	@SequenceGenerator(name="ADM_COMPANY_TYPE_SEQ", sequenceName="ADM_COMPANY_TYPE_SEQ")
	@GeneratedValue(generator="ADM_COMPANY_TYPE_SEQ")
	@Column(name = "ID", unique = true, nullable = false, precision = 5, scale = 0)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "COMPANY_TYPE_NAME", length = 100)
	public String getCompanyTypeName() {
		return this.companyTypeName;
	}

	public void setCompanyTypeName(String companyTypeName) {
		this.companyTypeName = companyTypeName;
	}

	@Column(name = "CREATED_DATE")
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "CREATED_BY", length = 100)
	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "UPDATED_DATE")
	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Column(name = "UPDATED_BY", length = 100)
	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "admCompanyType")
	public Set<TmpVndCompanyType> getTmpVndCompanyTypes() {
		return this.tmpVndCompanyTypes;
	}

	public void setTmpVndCompanyTypes(Set<TmpVndCompanyType> tmpVndCompanyTypes) {
		this.tmpVndCompanyTypes = tmpVndCompanyTypes;
	}

}
