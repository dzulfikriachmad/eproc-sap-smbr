package com.orm.tools;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.eproc.utils.SchedulerBatch;

public class HibernateListener implements ServletContextListener {

	/**
	 * added by ahmad on 4 july 2008
	 */
    public void contextInitialized(ServletContextEvent event) {
        HibernateUtil.getSessionFactory();
        HibernateUtil.getSessionFactoryJde();
        try{
	         new SchedulerBatch();
	       }
	       catch(Exception e)
	       {
	           e.printStackTrace();
	       }
    }

    public void contextDestroyed(ServletContextEvent event) {
        HibernateUtil.getSessionFactory().close();
        HibernateUtil.getSessionFactoryJde().close();
    }
}
