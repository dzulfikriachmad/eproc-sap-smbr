package com.orm.tools;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.IdentifierGenerator;

public class PrcPrNumberSeq implements IdentifierGenerator {

	@Override
	public Serializable generate(SessionImplementor session, Object obj)
			throws HibernateException {
		String suffix = "/III/2012";
		
		Connection connection = session.connection();
        try {
            PreparedStatement ps = connection.prepareStatement("SELECT nextval ('PRC_PR_NUMBER_SEQ') as nextval");

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                int id = rs.getInt("nextval");
                String code = Integer.toString(id) + suffix;
                System.out.println(">>>>>>>>>>>>>>>>>>>"+code); 
                return code;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new HibernateException(
                    "Unable to generate Stock Code Sequence");
        }
		return null;
	}

}
