package com.orm.tools;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import com.orm.model.AdmMenu;

public class AbstractCommand {

	public String xCommand;
	public Integer create;
	public Integer retrieve;
	public Integer update;
	public Integer delete;

	private int resultSize;
	private int size;
	private int page;

	/**
	 * get page from display tag
	 * 
	 * @param request
	 * @return
	 * @author kusumah
	 */
	public int getPage(HttpServletRequest request) {
		int page = 0;
		@SuppressWarnings("rawtypes")
		Enumeration paramNames = request.getParameterNames();
		while (paramNames.hasMoreElements()) {
			String name = (String) paramNames.nextElement();
			if (name != null && name.startsWith("d-") && name.endsWith("-p")) {
				String pageValue = request.getParameter(name);
				if (pageValue != null) {
					page = Integer.parseInt(pageValue) - 1;
				}
			}
		}
		return page;
	}

	public AdmMenu menu;

	public String getxCommand() {
		return xCommand;
	}

	public void setxCommand(String xCommand) {
		this.xCommand = xCommand;
	}

	public Integer getCreate() {
		return create;
	}

	public void setCreate(Integer create) {
		this.create = create;
	}

	public Integer getRetrieve() {
		return retrieve;
	}

	public void setRetrieve(Integer retrieve) {
		this.retrieve = retrieve;
	}

	public Integer getUpdate() {
		return update;
	}

	public void setUpdate(Integer update) {
		this.update = update;
	}

	public Integer getDelete() {
		return delete;
	}

	public void setDelete(Integer delete) {
		this.delete = delete;
	}

	public AdmMenu getMenu() {
		return menu;
	}

	public void setMenu(AdmMenu menu) {
		this.menu = menu;
	}

	public int getResultSize() {
		return resultSize;
	}

	public void setResultSize(int resultSize) {
		this.resultSize = resultSize;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

}
