package com.orm.tools;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Interceptor;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;

public class GenericDao {

	public Session session;
	
	/**
	 * saving or update entity
	 * @param o
	 * @return
	 * @throws Exception 
	 */
	public boolean saveEntity(Object o) throws Exception{
		boolean cek = false;
		try{
			session.saveOrUpdate(o);
			session.flush();
			cek = true;
		}catch (Exception e) {
			//session.getTransaction().rollback();
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		return cek;
	}
	
	
	/**
	 * saving only , typically used after deletion 
	 * @param o
	 * @return
	 * @throws Exception
	 */
	public boolean saveOnlyEntity(Object o) throws Exception{
		boolean cek = false;
		try{
			session.save(o);
			session.flush();
			cek = true;
		}catch (Exception e) {
			session.getTransaction().rollback();
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		return cek;
	}
	
	/**
	 * updating entity using merge
	 * @param o
	 * @return
	 * @throws Exception 
	 */
	public boolean updateEntity(Object o) throws Exception{
		boolean cek = false;
		try{
			session.merge(o);
			session.flush();			
			cek = true;
		}catch (Exception e) {
			session.getTransaction().rollback();
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		return cek;
	}
	
	/**
	 * delete entity
	 * @param o
	 * @return
	 * @throws Exception 
	 */
	public boolean deleteEntity(Object o) throws Exception{
		boolean cek = false;
		try{
			session.delete(o);
			session.flush();
			cek = true;
		}catch (Exception e) {
			session.getTransaction().rollback();
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		return cek;
	}
	
	public boolean executeByHql(String hql) throws Exception{
		boolean cek = false;
		try{
		
		Object lst = null;
		Query q	= session.createQuery(hql);
		q.executeUpdate();
		cek = true;
		}catch (Exception e) {
			session.getTransaction().rollback();
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		return cek;
	}
	
	/**
	 * get entity using detache criteria 
	 * @param crit
	 * @return
	 */
	public Object getEntityByDetachedCiteria(DetachedCriteria crit){
		Session ses = HibernateUtil.getSessionFactory().openSession();
		Object obj =  crit.getExecutableCriteria(ses).uniqueResult();
		ses.clear();
		ses.close();
		return obj;
	}
	
	/**
	 * get list of object using detached criteria
	 * @param crit
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Object getListByDetachedCiteria(DetachedCriteria crit){
		Session ses = HibernateUtil.getSessionFactory().openSession();
		List<Object> lst = new ArrayList<Object>();
		lst =(List<Object>)crit.getExecutableCriteria(ses).list();
		Object obj =  null;
		if(lst!=null && lst.size()>0){
				obj = lst;
		}
		ses.clear();
		ses.close();
		return lst;
	}
	
	/**
	 * get list of object using detached criteria
	 * Pastikan session sudah di open sebelumnya...
	 * @param crit
	 * @param ses
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Object getListByDetachedCriteria(DetachedCriteria crit, Session ses){
		List<Object> lst = new ArrayList<Object>();
		lst = (List<Object>) crit.getExecutableCriteria(ses).list();
		Object obj =  null;
		if(lst!=null && lst.size()>0){
				obj = lst;
		}
		return lst;
	}
	
	public Object getListByHql(String hql){
		Session ses = HibernateUtil.getSessionFactory().openSession();
		List<Object> lst = new ArrayList<Object>();
		Query q	= ses.createQuery(hql);
		
		lst = (List<Object>)q.list();
		Object obj =  null;
		if(lst!=null && lst.size()>0){
				obj = lst;
		}
		
		ses.clear();
		ses.close();
		return lst;
		
	}
	
	public Object getEntityByHql(String hql){
		Session ses = HibernateUtil.getSessionFactory().openSession();
		Object lst = null;
		Query q	= ses.createQuery(hql);
		lst = q.uniqueResult();
		ses.clear();
		ses.close();
		return lst;
		
	}
	
	/**
	 * paging using hibernate
	 * @param crit
	 * @param page
	 * @param size
	 * @param aClass
	
	 * @return Object
	 */
	@SuppressWarnings("unchecked")
	public Object getListByDetachedCiteriaPaging(DetachedCriteria crit,int page, int size){
		Session ses = HibernateUtil.getSession();
		List<Object> lst = new ArrayList<Object>();
		Criteria cr = crit.getExecutableCriteria(ses);
		cr.setFirstResult(page*size).setMaxResults(size);
		lst =cr.list();
		ses.clear();
		ses.close();
		return lst;
	}
	
	
	/**
	 * start transaction
	 */
	public void startTransaction(){
		session =  HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
	}
	/**
	 * jika pake intetceptors
	 * @param interceptor
	 */
	public void startTransaction(Interceptor interceptor){
		session =  HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
	}
	
	/**
	 * commit and clear
	 */
	public void commitAndClear(){
		session.getTransaction().commit();
		session.clear();
		session.close();
	}
	
	
	/**
	 * rollback and close
	 */
	public void RollbackAndClear(){
		if(session.isConnected() && session.isOpen()){
			session.getTransaction().rollback();
			session.clear();
			session.close();
		}
	}
	
	/**
	 * start transaction
	 */
	public void startTransactionJDE(){
		session =  HibernateUtil.getSessionFactoryJdeLocal().openSession();
		session.beginTransaction();
	}

}
