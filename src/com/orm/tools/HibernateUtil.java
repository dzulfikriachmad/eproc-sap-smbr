package com.orm.tools;

import com.eproc.interceptor.OrmInterceptor;
import com.jwebs.orm.tools.ClassScanner;
import java.io.PrintStream;
import java.util.Properties;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class HibernateUtil
{
  private static final SessionFactory sessionFactory = buildSessionFactory();
  private static final SessionFactory sessionFactoryJde = buildSessionFactoryJde();
  private static final SessionFactory sessionFactoryJdeLocal = buildSessionFactoryJdeLocal();
  private static ServiceRegistry serviceRegistry;
  
  private static SessionFactory buildSessionFactory()
  {
    try
    {
      Configuration conf = new Configuration();
      Class[] aClasses = ClassScanner.getClassesInPackage("com.orm.model", null);
      if ((aClasses != null) && (aClasses.length > 0)) {
        for (Class<?> a : aClasses) {
          conf.addAnnotatedClass(a);
        }
      }
      conf.setInterceptor(new OrmInterceptor());
      
      Properties prop = new Properties();
      prop.put("hibernate.connection.username", "eproc");
      //prop.put("hibernate.connection.password", "eproc2013jwebs");
      prop.put("hibernate.connection.password", "s4p_Eproc2019jWebs");
      prop.put("hibernate.connection.url", "jdbc:oracle:thin:@localhost:1525:orcl");
      //prop.put("hibernate.connection.url", "jdbc:oracle:thin:@localhost:1524:orcl");
   //   prop.put("hibernate.connection.url", "jdbc:oracle:thin:@apps.semenbaturaja.co.id:1515:orcl");
      //prop.put("hibernate.connection.url", "jdbc:oracle:thin:@10.10.2.14:1521:orcl");
      //prop.put("hibernate.connection.url", "jdbc:oracle:thin:@localhost:1525:orcl");
//      prop.put("hibernate.connection.url", "jdbc:oracle:thin:@10.10.2.11:1521:orcl");
//      prop.put("hibernate.default_schema", "eproc");
      /*prop.put("hibernate.connection.username", "EPROC");
      prop.put("hibernate.connection.password", "EPROC");*/
     // prop.put("hibernate.connection.url", "jdbc:oracle:thin:@localhost:1521:xe");
      prop.put("hibernate.default_schema", "eproc");
      //prop.put("hibernate.hbm2ddl.auto", "update");

      prop.put("hibernate.bytecode.use_reflection_optimizer", "false");
      prop.put("hibernate.connection.driver_class", "oracle.jdbc.driver.OracleDriver");
      prop.put("hibernate.dialect", "org.hibernate.dialect.OracleDialect");
      prop.put("hibernate.format_sql", "true");
      prop.put("hibernate.search.autoregister_listeners", "false");
      prop.put("hibernate.show_sql", "true");
      prop.put("hibernate.show_statistics", "true");
      
      prop.put("hibernate.temp.use_jdbc_metadata_defaults", "false");
      conf.addProperties(prop);
      serviceRegistry = new ServiceRegistryBuilder().applySettings(conf.getProperties()).buildServiceRegistry();
      return conf.buildSessionFactory(serviceRegistry);
    }
    catch (Throwable ex)
    {
      System.err.println("Initial SessionFactory creation failed." + ex);
      ex.printStackTrace();
      throw new ExceptionInInitializerError(ex);
    }
  }
  
  private static SessionFactory buildSessionFactoryJdeLocal()
  {
    try
    {
      Configuration conf = new Configuration();
      Class[] aClasses = ClassScanner.getClassesInPackage("com.orm.model", null);
      if ((aClasses != null) && (aClasses.length > 0)) {
        for (Class<?> a : aClasses) {
          conf.addAnnotatedClass(a);
        }
      }
      Properties prop = new Properties();
      prop.put("hibernate.connection.username", "eproc");
      prop.put("hibernate.connection.password", "s4p_Eproc2019jWebs");
      prop.put("hibernate.connection.url", "jdbc:oracle:thin:@localhost:1525:orcl");
      //prop.put("hibernate.connection.url", "jdbc:oracle:thin:@10.10.2.15:1521:orcl");
     //prop.put("hibernate.connection.url", "jdbc:oracle:thin:@10.10.2.14:1521:orcl");
     // prop.put("hibernate.connection.url", "jdbc:oracle:thin:@10.10.2.14:1521:orcl");
    //  prop.put("hibernate.connection.url", "jdbc:oracle:thin:@10.10.2.15:1521:orcl");
      //      prop.put("hibernate.connection.url", "jdbc:oracle:thin:@10.10.2.11:1521:orcl");
//      prop.put("hibernate.default_schema", "eproc");
      /*prop.put("hibernate.connection.username", "EPROC");
      prop.put("hibernate.connection.password", "EPROC");*/
    //  prop.put("hibernate.connection.url", "jdbc:oracle:thin:@localhost:1521:xe");
      prop.put("hibernate.default_schema", "eproc");
    
      prop.put("hibernate.bytecode.use_reflection_optimizer", "false");
      prop.put("hibernate.connection.driver_class", "oracle.jdbc.driver.OracleDriver");
      prop.put("hibernate.dialect", "org.hibernate.dialect.OracleDialect");
      prop.put("hibernate.format_sql", "true");
      prop.put("hibernate.search.autoregister_listeners", "false");
      prop.put("hibernate.show_sql", "true");
      prop.put("hibernate.show_statistics", "true");
      prop.put("hibernate.temp.use_jdbc_metadata_defaults", "false");
      conf.addProperties(prop);
      serviceRegistry = new ServiceRegistryBuilder().applySettings(conf.getProperties()).buildServiceRegistry();
      return conf.buildSessionFactory(serviceRegistry);
    }
    catch (Throwable ex)
    {
      System.err.println("Initial SessionFactory creation failed." + ex);
      ex.printStackTrace();
      throw new ExceptionInInitializerError(ex);
    }
  }
  
  private static SessionFactory buildSessionFactoryJde()
  {
    try
    {
      Configuration conf = new Configuration();
      Properties prop = new Properties();
//      prop.put("hibernate.connection.username", "adw");
//      prop.put("hibernate.connection.password", "4dwk0nsu1t4n");
//      prop.put("hibernate.connection.url", "jdbc:oracle:thin:@10.10.2.18:1521:jdeora");
//      prop.put("hibernate.default_schema", "crpdta");
      prop.put("hibernate.bytecode.use_reflection_optimizer", "false");
      prop.put("hibernate.connection.driver_class", "oracle.jdbc.driver.OracleDriver");
      prop.put("hibernate.dialect", "org.hibernate.dialect.OracleDialect");
      prop.put("hibernate.format_sql", "true");
      prop.put("hibernate.search.autoregister_listeners", "false");
      prop.put("hibernate.show_sql", "true");
      prop.put("hibernate.temp.use_jdbc_metadata_defaults", "false");
      conf.addProperties(prop);
      serviceRegistry = new ServiceRegistryBuilder().applySettings(conf.getProperties()).buildServiceRegistry();
      return conf.buildSessionFactory(serviceRegistry);
    }
    catch (Throwable ex)
    {
      System.err.println("Initial SessionFactory creation failed." + ex);
      ex.printStackTrace();
      
      throw new ExceptionInInitializerError(ex);
    }
  }
  
  public static SessionFactory getSessionFactory()
  {
    return sessionFactory;
  }
  
  public static Session getSession()
  {
    return sessionFactory.openSession();
  }
  
  public static SessionFactory getSessionFactoryJde()
  {
    return sessionFactoryJde;
  }
  
  public static Session getSessionJde()
  {
    return sessionFactoryJde.openSession();
  }
  
  public static SessionFactory getSessionFactoryJdeLocal()
  {
    return sessionFactoryJdeLocal;
  }
  
  public static Session getSessionJdeLocal()
  {
    return sessionFactoryJdeLocal.openSession();
  }
}
