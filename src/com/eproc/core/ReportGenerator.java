package com.eproc.core;

import java.util.Iterator;
import java.util.List;

import org.apache.struts2.ServletActionContext;

import com.eproc.utils.FusionClass;


public class ReportGenerator {
	public String statistikVendor(List<List<String>> data){
		String strXml;
		
		strXml="";
   	 	strXml="<chart caption='Data Statistic Status Vendor' xAxisName='Status Vendor' yAxisName='Jumlah Vendor'  shownames='1' showvalues='1' showBorder='1' formatNumberScale='0'>";
   	 	
   	 	String vndStatus = "";
   	 	String jmlVendor = "";
   	 	
   	 	strXml+="<categories>";
   	 	
   	 	Iterator<List<String>> it = data.iterator();
   	 	while(it.hasNext()){
			List<String> tmp = (List<String>)it.next();
			vndStatus = String.valueOf(tmp.get(0));
			strXml+="<category label='"+vndStatus+"'/>";		
		}
   	 	
   	 	
   	 	strXml+="</categories>";
   	 	
   	 	strXml+="<dataset seriesName='Data Vendor' color='8BBA00'>";
   	 	it = data.iterator();
   	 	
   	 	
   	 	double total = 0;
   	 	while(it.hasNext()){
	   	 	List<String> tmp = (List<String>)it.next();
	   	 	jmlVendor = String.valueOf(tmp.get(1));
			strXml+="<set value='"+jmlVendor+"'  />";
			
			//cal total;
			total = total+Double.valueOf(jmlVendor);
   	 	}
   	 	
   	 	strXml+="</dataset>";	 	
   	 	strXml+="</chart>";

   	 	FusionClass fs = new FusionClass();
   	 	
   	   	String chartCode = fs.createChart(ServletActionContext.getRequest().getContextPath()+"/assets/chart/MSColumn3D.swf", "", strXml, 
   		       "vendor", 906, 300, false, false);
		return chartCode;
	}
	
	public String statistikPrc(List<List<String>> data){
		String strXml;
		
		strXml="";
   	 	strXml="<chart caption='Data Statistic Proses Pengadaan' xAxisName='Status Proses Pengadaan' yAxisName='Jumlah'  shownames='1' showvalues='1' showBorder='1' formatNumberScale='0'>";
   	 	
   	 	String vndStatus = "";
   	 	String jmlVendor = "";
   	 	
   	 	strXml+="<categories>";
   	 	
   	 	Iterator<List<String>> it = data.iterator();
   	 	while(it.hasNext()){
			List<String> tmp = (List<String>)it.next();
			vndStatus = String.valueOf(tmp.get(0));
			strXml+="<category label='"+vndStatus+"'/>";		
		}
   	 	
   	 	
   	 	strXml+="</categories>";
   	 	
   	 	strXml+="<dataset seriesName='Data Pengadaan' color='8BBA00'>";
   	 	it = data.iterator();
   	 	
   	 	
   	 	double total = 0;
   	 	while(it.hasNext()){
	   	 	List<String> tmp = (List<String>)it.next();
	   	 	jmlVendor = String.valueOf(tmp.get(1));
			strXml+="<set value='"+jmlVendor+"'  />";
			
			//cal total;
			total = total+Double.valueOf(jmlVendor);
   	 	}
   	 	
   	 	strXml+="</dataset>";	 	
   	 	strXml+="</chart>";

   	 	FusionClass fs = new FusionClass();
   	 	
   	   	String chartCode = fs.createChart(ServletActionContext.getRequest().getContextPath()+"/assets/chart/MSColumn3D.swf", "", strXml, 
   		       "procurement", 906, 300, false, false);
		return chartCode;
	}
}
