package com.eproc.core;

import org.apache.struts2.interceptor.ServletRequestAware;
import org.springframework.context.ApplicationContextAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

public abstract class ActionController extends ActionSupport implements ModelDriven<Object>,
Preparable,ServletRequestAware,ApplicationContextAware {

/**
* 
*/
private static final long serialVersionUID = 1L;

}
