package com.eproc.core;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.opensymphony.xwork2.util.LocalizedTextUtil;

public class GlobalListener implements ServletContextListener {
	private final String DEFAULT_RESOURCE = "lang";

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		LocalizedTextUtil.addDefaultResourceBundle(DEFAULT_RESOURCE);
	}
}
