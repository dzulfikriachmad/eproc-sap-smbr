package com.eproc.utils;

import java.text.SimpleDateFormat;

import org.joda.time.DateTime;
import org.joda.time.Seconds;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class TimerJob implements Job {

	@Override
	public void execute(JobExecutionContext ctx) throws JobExecutionException {
		int remaining = Seconds.secondsBetween(new DateTime(ctx.getFireTime()),
				new DateTime(ctx.getTrigger().getEndTime())).getSeconds();
		TimerEauction.setFireTime("Eauction Akan Berakhir Pada :: "
				+ new SimpleDateFormat("dd/MM/yyyy HH:mm").format(ctx
						.getTrigger().getEndTime()));
		TimerEauction.setInterval(remaining);
	}

}
