package com.eproc.utils;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletContext;

import org.apache.commons.io.FileUtils;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.struts2.ServletActionContext;

public class UploadUtil {
	/**
	 * 
	 * @param srcFile
	 * @param fileName
	 * @return
	 * @throws IOException
	 */
	public static String uploadDocsFile(File srcFile, String fileName) throws IOException{
		ServletContext context = ServletActionContext.getRequest().getSession().getServletContext();
		String path = context.getRealPath("").concat(System.getProperty("file.separator")).concat("upload").concat(System.getProperty("file.separator").concat("file").concat(System.getProperty("file.separator")));
		long idUpload = Calendar.getInstance().getTimeInMillis();
		fileName = String.valueOf(idUpload).concat("-").concat(fileName.replace(" ", "_"));
		//FileUtils.copyFile(srcFile, new File(path.concat(fileName)));
		/*FileChannel srcChannel = new FileInputStream(srcFile).getChannel();
		FileChannel destChannel = new FileOutputStream(new File(path.concat(fileName))).getChannel();
		destChannel.transferFrom(srcChannel, 0, srcChannel.size());*/
		Files.copy(srcFile.toPath(), new File(path.concat(fileName)).toPath());
		
		return fileName;
		
	}
	
	public static void uploadDocsFileSAP(String attachment) throws IOException {
		final String  SOURCE = SessionGetter.getPropertiesValue("file.source.path", null);
		ServletContext context = ServletActionContext.getRequest().getSession().getServletContext();
		final String DESTINATION = context.getRealPath("").concat(System.getProperty("file.separator")).concat("upload")
				.concat(System.getProperty("file.separator").concat("file").concat(System.getProperty("file.separator")));
		File destFile = new File(DESTINATION+attachment);
		System.out.println(DESTINATION+attachment);
		if(!destFile.exists()) {
			File sourceFile = new File(SOURCE+attachment);
			System.out.println(SOURCE+attachment);
			FileUtils.copyFile(sourceFile, destFile);
		}
		
		
																				}
	
	/*public static void uploadDocsFileSAP(String attachmentUrl, String file) throws IOException{
		ServletContext context = ServletActionContext.getRequest().getSession().getServletContext();
		String path = context.getRealPath("").concat(System.getProperty("file.separator")).concat("upload")
				.concat(System.getProperty("file.separator").concat("file").concat(System.getProperty("file.separator")));
		long idUpload = Calendar.getInstance().getTimeInMillis();
		System.out.println("opening connection");
		System.out.println(path);
		URL url = new URL(attachmentUrl);
		InputStream in = url.openStream();
		String filename = file;
		filename = filename.replace(" ", "_");
		FileOutputStream fos = new FileOutputStream(new File(path+filename));
		System.out.println("reading from resource and writing to file...");
		int length = -1;
		byte[] buffer = new byte[1024];// buffer for portion of data from connection
		while ((length = in.read(buffer)) > -1) {
		    fos.write(buffer, 0, length);
		}
		fos.close();
		in.close();
		System.out.println("File downloaded");
		//FileUtils.copyFile(srcFile, new File(path.concat(filename)));
	}*/
	
	/*public static void uploadDocsFileSAP(String attachment) {
		try {
//			String SFTPHOST = "app.semenbaturaja.co.id";
			
			ServletContext context = ServletActionContext.getRequest().getSession().getServletContext();
			String path = context.getRealPath("").concat(System.getProperty("file.separator")).concat("upload").concat(System.getProperty("file.separator").concat("file").concat(System.getProperty("file.separator")));
			String SFTPHOST = SessionGetter.getPropertiesValue("sftp.sap.server", null);
	        Integer SFTPPORT = Integer.valueOf(SessionGetter.getPropertiesValue("sftp.sap.port", null));
	        String SFTPUSER = SessionGetter.getPropertiesValue("sftp.sap.user", null);;
	        String SFTPPASS = SessionGetter.getPropertiesValue("sftp.sap.password", null);
	        String SFTPWORKINGDIR = SessionGetter.getPropertiesValue("sftp.sap.dir", null);;

	        Session session = null;
	        Channel channel = null;
	        ChannelSftp channelSftp = null;
	        System.out.println("preparing the host information for sftp.");
	        try {
	        	System.out.println(">>>>>>>>>>>>>>>" + attachment );
	            JSch jsch = new JSch();
	           // JSch.setLogger(new MyLogger());
	            session = jsch.getSession(SFTPUSER, SFTPHOST, SFTPPORT);
	            System.out.println(SFTPUSER + SFTPHOST + SFTPPORT);
	            session.setPassword(SFTPPASS);
	            System.out.println(SFTPPASS);
	            java.util.Properties config = new java.util.Properties();
	            System.out.println("config");
	            config.put("kex", "diffie-hellman-group-exchange-sha256, hmac-sha2-256");
	           config.put("cipher.s2c", 
	                    "aes128-ctr,aes128-cbc,3des-ctr,3des-cbc,blowfish-cbc,aes192-ctr,aes192-cbc,aes256-ctr,aes256-cbc");
	            
	            config.put("StrictHostKeyChecking", "no");
	            System.out.println("StrictHostKeyChecking");
	            session.setConfig(config);
	            System.out.println("setConfig");
	            session.connect();
	            System.out.println("Host connected.");
	            channel = session.openChannel("sftp");
	            channel.connect();
	            System.out.println("sftp channel opened and connected.");
	            channelSftp = (ChannelSftp) channel;
	            channelSftp.cd(SFTPWORKINGDIR);
	            System.out.println("FILE >> " + SFTPWORKINGDIR+ "/" + attachment);
	            System.out.println(path + attachment);
	            channelSftp.get(SFTPWORKINGDIR+ "/" + attachment, path);
	            
	        } catch (Exception ex) {
	        	 ex.printStackTrace();
	             System.out.println("Exception found while tranfer the response.");
	             
	        }
	        finally{

	            channelSftp.exit();
	            System.out.println("sftp Channel exited.");
	            channel.disconnect();
	            System.out.println("Channel disconnected.");
	            session.disconnect();
	            System.out.println("Host Session disconnected.");
	        }
			
			
		}catch (HttpClientErrorException exception) {
			exception.printStackTrace();
		     exception.getResponseBodyAsString().pr
		      System.out.println(exception.getRawStatusCode());
		      System.out.println(exception.getMessage());
	  }
	}*/
		
	
	
	public static String uploadDocsFileOriginal(File srcFile, String fileName) throws IOException{
		ServletContext context = ServletActionContext.getRequest().getSession().getServletContext();
		String path = context.getRealPath("").concat(System.getProperty("file.separator")).concat("upload").concat(System.getProperty("file.separator").concat("file").concat(System.getProperty("file.separator")));
		FileUtils.copyFile(srcFile, new File(path.concat(fileName)));
		return fileName;
		
	}
	
	public static String uploadFileByBinary(byte[] fileBytes, String fileType, String fileName) throws Exception{
		ServletContext context = ServletActionContext.getRequest().getSession().getServletContext();
		String path = context.getRealPath("").concat(System.getProperty("file.separator")).concat("upload").concat(System.getProperty("file.separator").concat("file").concat(System.getProperty("file.separator")));
		fileName = fileName.replace(" ", "_");
		fileType = fileType.toLowerCase();
		System.out.println(path+fileName+"."+fileType);
		
		if(fileType.equalsIgnoreCase("docx") || fileType.equalsIgnoreCase("xlsx") || 
				fileType.equalsIgnoreCase("pptx") || fileType.equalsIgnoreCase("doc") || fileType.equalsIgnoreCase("xls") || fileType.equalsIgnoreCase("ppt")) {
			 XWPFDocument xWPFDocument = new XWPFDocument(new ByteArrayInputStream(fileBytes)); 
			 FileOutputStream fos = new FileOutputStream(new File(path+fileName+"."+fileType));   
			 xWPFDocument.write(fos);
			 xWPFDocument.close();
		}else {
			ByteArrayOutputStream bOut = new ByteArrayOutputStream();
			bOut.write(fileBytes);
			FileOutputStream out = new FileOutputStream(new File(path+fileName+"."+fileType));
			out.write(fileBytes);
			out.close();
		}
		
		return fileName;
	}
	
	public static void writeIntegrationLog(String logname, String nomorPr, String content) throws IOException {
		ServletContext context = ServletActionContext.getRequest().getSession().getServletContext();
		String path = context.getRealPath("").concat(System.getProperty("file.separator")).concat("upload").concat(System.getProperty("file.separator").concat("file").concat(System.getProperty("file.separator")));
		long idUpload = Calendar.getInstance().getTimeInMillis();
		String fileName = nomorPr +"_"+ logname + "_"+ String.valueOf(idUpload) + ".log";
		File statText = new File(path+fileName);
        FileOutputStream is = new FileOutputStream(statText);
        OutputStreamWriter osw = new OutputStreamWriter(is);    
        Writer w = new BufferedWriter(osw);
        w.write(content);
        w.close();
	}
	
}
