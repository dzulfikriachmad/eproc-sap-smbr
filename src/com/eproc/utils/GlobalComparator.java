package com.eproc.utils;

import java.util.Comparator;

import com.eproc.x.rfq.command.TabulasiPenilaian;
import com.orm.model.AdmMenu;

public class GlobalComparator<T> implements Comparator<T> {

	private Object obj;
	public GlobalComparator(Object obj) {
		this.obj = obj;
	}
	@Override
	public int compare(T a, T b) {
		if(obj instanceof AdmMenu){
			int x = ((AdmMenu)a).getId();
			int y = ((AdmMenu)b).getId();
			if (x > y ){
				return 1;
			} else if (x < y){
				return -1;
			}
		}else if(obj instanceof TabulasiPenilaian){
			double x = ((TabulasiPenilaian)a).getTotalPenawaran().doubleValue();
			double y = ((TabulasiPenilaian)b).getTotalPenawaran().doubleValue();
			if (x > y ){
				return 1;
			} else if (x < y){
				return -1;
			}
		}
		return 0;
	}

}
