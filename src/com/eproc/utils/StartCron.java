package com.eproc.utils;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class StartCron implements ServletContextListener {

	/**
	 * Method contextDestroyed.
	 * 
	 * @param arg0
	 *            ServletContextEvent
	 * @see javax.servlet.ServletContextListener#contextDestroyed(ServletContextEvent)
	 */
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		System.out.println("Stopping Application successfully");

	}

	/**
	 * Method contextInitialized.
	 * 
	 * @param arg0
	 *            ServletContextEvent
	 * @see javax.servlet.ServletContextListener#contextInitialized(ServletContextEvent)
	 */
	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		System.out.println("Initializing Application successfully");
		try {
			new SchedulerBatch();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
