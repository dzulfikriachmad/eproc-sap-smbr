package com.eproc.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

public class FtpUtilEproc {

	public static InputStream getFtpFile(String username, String password, String server, String port, String remoteDirectory, String fileName) throws Exception{
		InputStream retMe;
		try {
			System.out.println(">>>> LOGIN FTP");
			FTPClient client = new FTPClient();
			client.connect(server);
			if(!client.login(username, password)){
				client.logout();
				return null;
			}
			
			int replyCode = client.getReplyCode();
			if (!FTPReply.isPositiveCompletion(replyCode))
            {
                client.disconnect();
                return null;
            }
			
			client.enterLocalPassiveMode();
			client.changeWorkingDirectory(remoteDirectory);
			System.out.println(">> "+remoteDirectory+" <><><><><>> "+fileName);
			
			//retrieve file
			retMe = client.retrieveFileStream(fileName);

			if(retMe!=null)
				return retMe;
			client.logout();
			client.disconnect();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return retMe;
	}
	
	public static void main(String[] args) {
		try {
			FtpUtilEproc.getFtpFile("JDE", "k3rb3ros.", "10.10.2.254", null, "mediaOBJ/HTMLUpload", "FILE-10-10-2-253-5136814468217884-1369017495625.jpg");
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	
	public static final String PREFIX = "stream2file";
    public static final String SUFFIX = ".tmp";

    public static File stream2file (InputStream in) throws IOException {
        final File tempFile = File.createTempFile(PREFIX, SUFFIX);
        tempFile.deleteOnExit();
        try {
        	FileOutputStream out = new FileOutputStream(tempFile);
            IOUtils.copy(in, out);
        }catch(Exception e){
        	e.printStackTrace();
        }
        return tempFile;
    }
}
