package com.eproc.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.joda.time.Chronology;
import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.joda.time.Seconds;
import org.joda.time.chrono.JulianChronology;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.matchers.GroupMatcher;

public class TimerEauction {

	private static int waktu;
	public static int ellapsed;
	private static String fireTime;
	private Date startDate;
	private Long ppmid;
	private Scheduler scheduler;

	public TimerEauction(Date startDate, int waktu, Long ppmid) {
		this.startDate = startDate;
		TimerEauction.waktu = waktu;
		this.ppmid = ppmid;
	}

	public void startTimer() throws Exception {
		try {

			JobDetail job = JobBuilder.newJob(TimerJob.class)
					.withIdentity("Eaution - " + ppmid).build();

			Trigger trigger = TriggerBuilder
					.newTrigger()
					.startAt(new DateTime(startDate).plusSeconds(1).toDate())
					.endAt(new DateTime(startDate).plusMinutes(waktu).toDate())
					.withIdentity("Eauction Trigger - " + ppmid)
					.withSchedule(
							SimpleScheduleBuilder.simpleSchedule()
									.withIntervalInSeconds(1).repeatForever())
					.build();

			fireTime = "Eauction Akan Mulai Pada :: "
					+ new SimpleDateFormat("dd/MM/yyyy HH:mm")
							.format(startDate);
			scheduler = new StdSchedulerFactory().getScheduler();
			scheduler.start();
			scheduler.scheduleJob(job, trigger);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public void paused(){
		try{
		
		 for (String groupName : scheduler.getJobGroupNames()) {
		     
			for (JobKey jobKey : scheduler.getJobKeys(GroupMatcher.jobGroupEquals(groupName))) {

				  String jobName = jobKey.getName();
				  //String jobGroup = jobKey.getGroup();

				  System.out.println("jobName:"+jobName);
				     if(jobName.startsWith("Eaution")){
				    	 scheduler.pauseJob(jobKey);
				     }
				}
		 }  
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void resume(){
		try{
			Trigger trigger = TriggerBuilder
					.newTrigger()
					.startAt(new DateTime(startDate).plusSeconds(1).toDate())
					.endAt(new DateTime(startDate).plusMinutes(getEllapsed()).toDate())
					.withIdentity("Eauction Trigger - " + ppmid)
					.withSchedule(
							SimpleScheduleBuilder.simpleSchedule()
									.withIntervalInSeconds(1).repeatForever())
					.build();
			scheduler = new StdSchedulerFactory().getScheduler();
			scheduler.start();
		 
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	public static void setInterval(int remaining) {
		ellapsed = remaining;
	}

	public static void setFireTime(String fire) {
		fireTime = fire;
	}

	public String getFireTime() {
		return fireTime;
	}

	public void cancelTimer() {
		
	}

	public int getEllapsed() {
		return ellapsed;
	}

	public static void main(String[] args) throws Exception {
		DateTime dt = new DateTime(new Date());
		DateTime dt2 = dt.plusMinutes(2);

		Minutes minu = Minutes.minutesBetween(new DateTime(new Date()), dt2);
		Seconds sc = Seconds.secondsBetween(new DateTime(new Date()), dt2);
		System.out.println(sc.getSeconds() + "<><><><>");

		Chronology cr = JulianChronology.getInstance();
		DateTime st = new DateTime(new Date(), cr);
		System.out.println(st);

		TimerEauction test = new TimerEauction(new Date(), 30,
				Long.valueOf(123));
		test.startTimer();

	}

}
