package com.eproc.utils;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.HtmlEmail;

import com.orm.model.AdmMailConfig;



public class EmailSessionBean {

	/** 
	 * sending mail
	 * @param subject
	 * @param message
	 * @param recipients
	 * @throws Exception
	 */
	public static void sendMail(String subject,String message, String [] recipients) throws Exception{
		try {
			EmailSessionBean esb = new EmailSessionBean();
			SendMailMulti sm = esb.new SendMailMulti(subject, message, recipients);
			new Thread(sm).start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/** 
	 * sending mail
	 * @param subject
	 * @param message
	 * @param recipients
	 * @throws Exception
	 */
	public static void sendMail(String subject,String message, String recipient) throws Exception{
		try {
			EmailSessionBean esb = new EmailSessionBean();
			SendMailSingle sm = esb.new SendMailSingle(subject, message, recipient);
			new Thread(sm).start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/** 
	 * sending mail
	 * @param subject
	 * @param message
	 * @param recipients
	 * @throws Exception
	 */
	public static void sendMail(String subject,String message, String recipient,AdmMailConfig config) throws Exception{
		try {
			EmailSessionBean esb = new EmailSessionBean();
			SendMailSingleConfig sm = esb.new SendMailSingleConfig(subject, message, recipient, config);
			new Thread(sm).start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	class SendMailSingleConfig implements Runnable{
		private String subject;
		private String message;
		private String recipient;
		private AdmMailConfig config;
		public SendMailSingleConfig(String subject,String message, String recipient, AdmMailConfig config) {
			this.subject = subject;
			this.message = message;
			this.recipient = recipient;
			this.config = config;
		}
		@Override
		public void run() {
			try {
				HtmlEmail email = new HtmlEmail();
				email.setSmtpPort(config.getSmtpPort());
				email.setHostName(config.getSmtpServer());
				if(config.getSsl()!=null && config.getSsl()==1){
					email.setSSL(true);
					email.setSslSmtpPort(config.getSmtpPort().toString());
				}
				email.setAuthenticator(new DefaultAuthenticator(config.getUsername(), config.getPassword()));
				email.addTo(recipient);
				email.setFrom(config.getUsername(), "eProc PT Semen Baturaja (Persero)");
				email.setSubject(subject);
				// set the html message
				email.setHtmlMsg(message);
				// set the alternative message if the client doesn't support html message
				email.setTextMsg("Your email client does not support HTML messages");
				// send the email
				email.send();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}
	
	class SendMailSingle implements Runnable{
		private String subject;
		private String message;
		private String recipient;
		public SendMailSingle(String subject,String message, String recipient) {
			this.subject = subject;
			this.message = message;
			this.recipient = recipient;
		}
		@Override
		public void run() {
			try {
				AdmMailConfig config = SessionGetter.getMailConfig();
				HtmlEmail email = new HtmlEmail();
				email.setSmtpPort(config.getSmtpPort());
				email.setHostName(config.getSmtpServer());
				email.setTLS(true);
				/*if(config.getSsl()!=null && config.getSsl()==1){
					email.setSSL(true);
					email.setSslSmtpPort(config.getSmtpPort().toString());
				}*/
				email.setAuthenticator(new DefaultAuthenticator(config.getUsername(), config.getPassword()));
				email.addTo(recipient);
				email.setFrom(config.getUsername(), "eProc PT Semen Baturaja (Persero)");
				email.setSubject(subject);
				// set the html message
				email.setHtmlMsg(message);
				// set the alternative message if the client doesn't support html message
				email.setTextMsg("Your email client does not support HTML messages");
				// send the email
				email.send();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}
	
	class SendMailMulti implements Runnable{
		private String subject;
		private String message;
		private String recipients[];
		public SendMailMulti(String subject,String message, String recipients[]) {
			this.subject = subject;
			this.message = message;
			this.recipients = recipients;
		}
		@Override
		public void run() {
			try {
				AdmMailConfig config = SessionGetter.getMailConfig();
				HtmlEmail email = new HtmlEmail();
				email.setSmtpPort(config.getSmtpPort());
				email.setHostName(config.getSmtpServer());
				if(config.getSsl()!=null && config.getSsl()==1){
					email.setSSL(true);
					email.setSslSmtpPort(config.getSmtpPort().toString());
					email.setTLS(true);
				}
				email.setAuthenticator(new DefaultAuthenticator(config.getUsername(), config.getPassword()));
				for(int i=0;i<recipients.length;i++){
//					email.addTo(recipients[i]);
					email.addBcc(recipients[i]);
				}
				email.setFrom(config.getUsername(), "eProc PT Semen Baturaja (Persero)");
				email.setSubject(subject);
				// set the html message
				email.setHtmlMsg(message);
				// set the alternative message if the client doesn't support html message
				email.setTextMsg("Your email client does not support HTML messages");
				// send the email
				email.send();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public static void main(String[] args) {
		try {
			HtmlEmail email = new HtmlEmail();
			email.setSmtpPort(25);
			email.setHostName("mail.semenbaturaja.co.id");
//			email.setSSL(true);
//			email.setSslSmtpPort("465");
			email.setAuthenticator(new DefaultAuthenticator("eproc@semenbaturaja.co.id", "eproc123456"));
			email.addTo("ramamonriex@gmail.com");
			email.setFrom("eproc@semenbaturaja.co.id", "eProc PT Semen Baturaja (Persero)");
			email.setSubject("TEST EMAIl");
			// set the html message
			email.setHtmlMsg("Test Email");
			// set the alternative message if the client doesn't support html message
			email.setTextMsg("Your email client does not support HTML messages");
			// send the email
			email.send();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
