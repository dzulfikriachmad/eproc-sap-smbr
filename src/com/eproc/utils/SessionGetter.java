package com.eproc.utils;

import java.math.BigDecimal;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import com.opensymphony.xwork2.ActionSupport;
import com.orm.model.AdmDelegasi;
import com.orm.model.AdmDept;
import com.orm.model.AdmDistrict;
import com.orm.model.AdmEmployee;
import com.orm.model.AdmJenis;
import com.orm.model.AdmMailConfig;
import com.orm.model.AdmProses;
import com.orm.model.AdmProsesHirarki;
import com.orm.model.AdmRole;
import com.orm.model.AdmRoleMenu;
import com.orm.model.AdmUser;
import com.orm.model.AdmUserManager;
import com.orm.model.CtrContractComment;
import com.orm.model.CtrContractHeader;
import com.orm.model.HistMainComment;
import com.orm.model.PanitiaPengadaanDetail;
import com.orm.model.PrcMainHeader;
import com.orm.model.PrcMainItem;
import com.orm.model.TmpVndHeader;
import com.orm.model.UsulanVndDetail;
import com.orm.model.UsulanVndHistory;

import eproc.controller.AdmServiceImpl;
import eproc.controller.PrcService;
import eproc.controller.PrcServiceImpl;
import eproc.controller.RfqServiceImpl;
import eproc.controller.VendorServiceImpl;

public class SessionGetter {

	public static final String USER_SESSION = "user";
	public static final String VENDOR_SESSION = "vendor";
	public static final String MENU_SESSION = "menu";
	public static final String ACTIVEMENU = "aktivemenu";
	public static final String SETUJU = "setuju";
	public static final String REVISI = "revisi";
	public static final String DRT = "DRT";
	public static final String NONDRT = "NON DRT";
	public static final Object PANITIADRT = "PANITIA DRT";

	public static final AdmProses PR_PROSES_ID = new AdmProses(100);
	public static final AdmProses OE_PROSES_ID = new AdmProses(102);
	public static final AdmProses PP_PROSES_ID = new AdmProses(103);
	public static final AdmProses ANGGARAN_PROSES_ID = new AdmProses(104);
	public static final AdmProses RFQ_PROSES_ID = new AdmProses(105);
	public static final AdmProses VENDOR_PROSES_ID = new AdmProses(106);
	public static final AdmProses PQ_PROSES_ID = new AdmProses(107);
	public static final AdmProses KONTRAK_PROSES_ID = new AdmProses(108);
	public static final AdmProses PANITIA_PP_PROSES_ID = new AdmProses(109);

	public static TimerEauction TIMER_EAUCTION = null;

	public static final Integer CLOSE = 100;
	public static final Integer SELESAI = 99;
	public static final Integer BATAL = -99;
	public static final Integer REBID = -98;
	public static final Integer MANUAL = -100;
	public static final AdmProses REVISI_EE = new AdmProses(113);
	public static final AdmProses REALOKASI = new AdmProses(114);
	
	//public static final String MIDDLEWARE_URL = "10.10.2.172:7070";
	public static final String MIDDLEWARE_URL = "10.10.2.23:9090";
	//public static final String SAP_URL = valueProperty("sap.server.local");
	public static final String SAP_URL = valueProperty("sap.server");
	public static final String SAP_USERNAME = valueProperty("sap.username");
	public static final String SAP_PASSWORD = valueProperty("sap.password");
	public static final String SAP_CLIENT = valueProperty("sap.client");
	public static final String SAP_AUTH = SAP_USERNAME + ":" + SAP_PASSWORD;
	//public static final String MIDDLEWARE_URL = "10.10.2.23:9090";
	//public static final String MIDDLEWARE_URL = "localhost";
	//public static final String SAP_URL = "http://smbrerpdev.semenbaturaja.co.id:8011/sap/zsmbrwebservice/IPROC/";


	public static Object getSessionValue(String attribute) {
		Object obj = ServletActionContext.getRequest().getSession()
				.getAttribute(attribute);
		return obj;
	}

	public static void setSessionValue(Object data, String attribute) {
		ServletActionContext.getRequest().getSession()
				.setAttribute(attribute, data);
	}

	public static void destroySessionValue(String attribute) {
		ServletActionContext.getRequest().getSession()
				.removeAttribute(attribute);
	}

	public static AdmUser getUser() {
		return (AdmUser) getSessionValue(SessionGetter.USER_SESSION);
	}

	public static boolean isNotNull(Object obj) {
		if (obj != null)
			return true;
		return false;
	}

	public static boolean isEdit(String getxCommand) {
		if (getxCommand.equalsIgnoreCase("edit")) {
			return true;
		}
		return false;
	}

	public static boolean isDelete(String getxCommand) {
		if (getxCommand.equalsIgnoreCase("delete")) {
			return true;
		}
		return false;
	}

	public static boolean isAddChild(String getxCommand) {
		if (getxCommand.equalsIgnoreCase("child")) {
			return true;
		}
		return false;
	}

	public static AdmRoleMenu getAdmRoleMenuByAction(String aksi,
			AdmEmployee admEmployee) throws Exception {
		return new AdmServiceImpl().getAdmRoleMenuByAction(aksi, admEmployee);
	}

	public static boolean isDrtJadwal(AdmDistrict district) {
		return new AdmServiceImpl().isDrtJadwal(district);
	}

	public static boolean isMemberPanitiaDrt(AdmUser user) {
		return new AdmServiceImpl().isMemberPanitiaDrt(user);
	}

	public static List<UsulanVndDetail> convertUsulanDetail(
			List<TmpVndHeader> listVendor) {
		List<UsulanVndDetail> retMe = new ArrayList<UsulanVndDetail>();
		if (SessionGetter.isNotEmptyList(listVendor)) {
			for (TmpVndHeader h : listVendor) {
				UsulanVndDetail d = new UsulanVndDetail();
				d.setVendor(h);
				d.setStatus(1);
				retMe.add(d);
			}
		}
		return retMe;
	}

	@SuppressWarnings("rawtypes")
	public static boolean isNotEmptyList(List list) {
		if (list != null && list.size() > 0) {
			return true;
		}
		return false;
	}

	public static void saveHistoryUsulanVendor(UsulanVndHistory usulanHistory,
			Session session, String aksi) throws Exception {
		try {
			UsulanVndHistory lastHist = new VendorServiceImpl()
					.getLastUsulanVndHistory(usulanHistory);
			if (lastHist == null) {
				usulanHistory
						.setNama(SessionGetter.getUser().getCompleteName());
				usulanHistory.setPosisi(SessionGetter.getUser()
						.getAdmEmployee().getEmpStructuralPos());
				usulanHistory.setAksi("Pembuatan Usulan");
				usulanHistory.setProses("Usulan Mitra Kerja");
				usulanHistory.setUpdatedDate(new Date());
				session.saveOrUpdate(usulanHistory);

				// get next proses usulan dan role
				AdmProsesHirarki ahp = SessionGetter
						.getHirarkiProsesByDistrict(
								SessionGetter.VENDOR_PROSES_ID, 2,
								SessionGetter.getUser().getAdmEmployee()
										.getAdmDept().getAdmDistrict(), 0.0);
				AdmEmployee emp = new AdmServiceImpl().getAdmEmployeeByRole(ahp
						.getAdmRoleByCurrentPos());
				if (ahp.getAdmRoleByCurrentPos().getRoleName()
						.equals(SessionGetter.PANITIADRT)) {
					UsulanVndHistory hist = new UsulanVndHistory();
					hist.setProses("Usulan Mitra Kerja");
					hist.setNama("Panitia DRT");
					hist.setPosisi("Panitia DRT");
					hist.setUsulan(usulanHistory.getUsulan());
					session.saveOrUpdate(hist);

					/** update usulan **/
					usulanHistory.getUsulan().setNextPos(
							ahp.getAdmRoleByCurrentPos());
					usulanHistory.getUsulan().setNextUser(null);
					session.saveOrUpdate(usulanHistory.getUsulan());
				} else {
					UsulanVndHistory hist = new UsulanVndHistory();
					hist.setProses(usulanHistory.getProses());
					hist.setNama(emp.getEmpFullName());
					hist.setPosisi(emp.getEmpStructuralPos());
					hist.setUsulan(usulanHistory.getUsulan());
					session.saveOrUpdate(hist);

					/** update usulan **/
					usulanHistory.getUsulan().setNextPos(
							ahp.getAdmRoleByCurrentPos());
					usulanHistory.getUsulan().setNextUser(emp.getAdmUser());
					session.saveOrUpdate(usulanHistory.getUsulan());
				}

			} else {
				lastHist.setAksi(aksi);
				lastHist.setProses("Persetujuan");
				lastHist.setKomentar(usulanHistory.getKomentar());
				session.saveOrUpdate(lastHist);

				// get next proses usulan dan role
				AdmProsesHirarki ahp = SessionGetter.getHirarkiProses(
						SessionGetter.VENDOR_PROSES_ID, usulanHistory
								.getUsulan().getTingkat(), SessionGetter
								.getUser().getAdmEmployee().getAdmDept(), 0.0);
				if (ahp != null) {// bukan last proses
					if (ahp.getAdmRoleByCurrentPos().getRoleName()
							.equals(SessionGetter.PANITIADRT)) {
						UsulanVndHistory hist = new UsulanVndHistory();
						hist.setProses("Usulan Mitra Kerja");
						hist.setNama("Panitia DRT");
						hist.setPosisi("Panitia DRT");
						hist.setUsulan(usulanHistory.getUsulan());
						session.saveOrUpdate(hist);

						/** update usulan **/
						usulanHistory.getUsulan().setNextPos(
								ahp.getAdmRoleByCurrentPos());
						usulanHistory.getUsulan().setNextUser(null);
						session.saveOrUpdate(usulanHistory.getUsulan());
					} else {
						AdmEmployee emp = new AdmServiceImpl()
								.getAdmEmployeeByRole(ahp
										.getAdmRoleByCurrentPos());
						UsulanVndHistory hist = new UsulanVndHistory();
						if (ahp.getAdmRoleByNextPos() == null)
							hist.setProses("Finalisasi Usulan Mitra Kerja");
						else
							hist.setProses("Usulan Mitra Kerja");
						hist.setNama(emp.getEmpFullName());
						hist.setPosisi(emp.getEmpStructuralPos());
						hist.setUsulan(usulanHistory.getUsulan());
						session.saveOrUpdate(hist);

						/** update usulan **/
						usulanHistory.getUsulan().setNextPos(
								ahp.getAdmRoleByCurrentPos());
						usulanHistory.getUsulan().setNextUser(emp.getAdmUser());
						session.saveOrUpdate(usulanHistory.getUsulan());
					}
				} else {
					lastHist.setAksi(aksi);
					lastHist.setProses("Finalisasi Usulan dan Umumkan");
					lastHist.setKomentar(usulanHistory.getKomentar());
					session.saveOrUpdate(lastHist);
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * history vendor baru
	 * 
	 * @param usulanHistory
	 * @param session
	 * @param aksi
	 * @throws Exception
	 */
	public static void saveHistoryUsulanVendorNew(
			UsulanVndHistory usulanHistory, Session session, String aksi)
			throws Exception {
		try {
			UsulanVndHistory lastHist = new VendorServiceImpl()
					.getLastUsulanVndHistory(usulanHistory);
			if (lastHist == null) {
				usulanHistory
						.setNama(SessionGetter.getUser().getCompleteName());
				usulanHistory.setPosisi(SessionGetter.getUser()
						.getAdmEmployee().getEmpStructuralPos());
				usulanHistory.setAksi("Pembuatan Usulan");
				usulanHistory.setProses("Usulan Mitra Kerja");
				usulanHistory.setUpdatedDate(new Date());
				session.saveOrUpdate(usulanHistory);

				// get next proses usulan dan role
				UsulanVndHistory hist = new UsulanVndHistory();
				hist.setProses(usulanHistory.getProses());
				hist.setNama(usulanHistory.getUsulan().getNextUser()
						.getCompleteName());
				hist.setPosisi(usulanHistory.getUsulan().getNextUser()
						.getAdmEmployee().getEmpStructuralPos());
				hist.setUsulan(usulanHistory.getUsulan());
				hist.setProses(aksi);
				session.saveOrUpdate(hist);

			} else {
				lastHist.setAksi(aksi);
				lastHist.setProses("Persetujuan");
				lastHist.setKomentar(usulanHistory.getKomentar());
				session.saveOrUpdate(lastHist);

				// get next proses usulan dan role
				UsulanVndHistory hist = new UsulanVndHistory();
				hist.setProses(usulanHistory.getProses());
				hist.setNama(usulanHistory.getUsulan().getNextUser()
						.getCompleteName());
				hist.setPosisi(usulanHistory.getUsulan().getNextUser()
						.getAdmEmployee().getEmpStructuralPos());
				hist.setUsulan(usulanHistory.getUsulan());
				hist.setProses(aksi);
				session.saveOrUpdate(hist);

			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	public static void saveHistoryUsulanVendorRevisi(
			UsulanVndHistory usulanHistory, Session session, String aksi)
			throws Exception {
		try {
			
				usulanHistory
						.setNama(SessionGetter.getUser().getCompleteName());
				usulanHistory.setPosisi(SessionGetter.getUser()
						.getAdmEmployee().getEmpStructuralPos());
				usulanHistory.setAksi(aksi);
				usulanHistory.setProses("BATAL");
				usulanHistory.setUpdatedDate(new Date());
				session.saveOrUpdate(usulanHistory);


		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * 
	 * @param admProses
	 * @param level
	 * @param admDept
	 * @return
	 */
	public static AdmProsesHirarki getHirarkiProses(AdmProses admProses,
			int level, AdmDept admDept, Double limit) throws Exception {
		return new AdmServiceImpl().getHirarkiProsesGlobal(admProses, level,
				admDept, limit);
	}

	public static String smkNo(long id, String format) {
		String ids = String.valueOf(id);
		int fmt = format.length();
		int idfmt = ids.length();
		String returnMe = format.substring(0, fmt - idfmt);
		return returnMe.concat(ids);
	}

	/**
	 * Get Total EE from Items
	 * 
	 * @param listPrcMainItem
	 * @return
	 */
	public static BigDecimal getEeFromItems(List<PrcMainItem> listPrcMainItem) {
		Double retMe = 0.0;
		if (listPrcMainItem != null && listPrcMainItem.size() > 0) {
			for (PrcMainItem t : listPrcMainItem) {
				retMe = retMe
						+ (t.getItemTotalPrice().doubleValue() * ((t
								.getItemTax().doubleValue() + 100) / 100));
			}
		}
		return new BigDecimal(retMe);
	}

	/**
	 * get Total OE from ITEMS
	 * 
	 * @param listPrcMainItem
	 * @return
	 */
	public static BigDecimal getOEFromItems(List<PrcMainItem> listPrcMainItem) {
		Double retMe = 0.0;
		if (listPrcMainItem != null && listPrcMainItem.size() > 0) {
			for (PrcMainItem t : listPrcMainItem) {
				retMe = retMe
						+ (t.getItemTotalPriceOe().doubleValue() * ((t
								.getItemTaxOe().doubleValue() + 100) / 100));
			}
		}
		return new BigDecimal(retMe);
	}

	public static HistMainComment getLastCommentByHeader(
			PrcMainHeader prcMainHeader) {
		return new PrcServiceImpl().getLastCommentByHeader(prcMainHeader);
	}

	public static CtrContractComment getLastCommentContractByHeader(
			CtrContractHeader mainHeader) {
		return new PrcServiceImpl().getLastCommentByContractHeader(mainHeader);
	}

	public static AdmEmployee getEmployeeByRole(AdmRole role) throws Exception {
		return new AdmServiceImpl().getAdmEmployeeByRole(role);
	}
	
	public static AdmEmployee getEmployeeByRoleName(String roleName) throws Exception {
		return new AdmServiceImpl().getAdmEmployeeByRoleName(roleName);
	}
	public static AdmProsesHirarki getHirarkiProsesJenis(PrcMainHeader prcMainHeader) throws Exception{
		return new PrcServiceImpl().getHirarkiProsesJenis(prcMainHeader);
	}
	public static AdmProsesHirarki getHirarkiProsesJenisKontrak(CtrContractHeader ctrContractHeader) throws Exception{
		return new RfqServiceImpl().getHirarkiProsesJenisKontrak(ctrContractHeader);
	}
	public static AdmProsesHirarki getHirarkiProsesJenisByDistrict(PrcMainHeader prcMainHeader) throws Exception{
		return new RfqServiceImpl().getHirarkiProsesJenisByDistrict(prcMainHeader);
	}
	public static AdmJenis getAdmJenisById(Integer id) throws Exception{
		return new AdmServiceImpl().getAdmJenisById(id);
	}
	public static AdmUser getManagerPengadaanByJenis(PrcMainHeader prcMainHeader) throws Exception{
		return new RfqServiceImpl().getManagerPengadaanByJenis(prcMainHeader);
	}

	
	

	/**
	 * Save Global History for PR and OE
	 * 
	 * @param histMainComment
	 * @param prcMainHeader
	 * @param session
	 * @throws Exception
	 */
	public static void saveHistoryGlobalPrOE(HistMainComment histMainComment,
			PrcMainHeader prcMainHeader, Session session) throws Exception {
		HistMainComment last = SessionGetter.getLastCommentByHeader(prcMainHeader);
		if(last!=null){
			last.setAksi(histMainComment.getAksi());
			last.setProses(prcMainHeader.getAdmProses().getId());
			last.setDocument(histMainComment.getDocument());
			last.setComment(histMainComment.getComment());
			last.setUsername(SessionGetter.getUser().getCompleteName());
			last.setPosisi(SessionGetter.getUser().getAdmEmployee().getEmpStructuralPos());
			session.saveOrUpdate(last);
		}
		
		System.out.print(">>>>>>>>>>>>");
		System.out.println(prcMainHeader.getPpmId());
		//next user
		histMainComment.setPrcMainHeader(prcMainHeader);
		histMainComment.setPosisi(prcMainHeader.getAdmUserByPpmCurrentUser().getAdmEmployee().getEmpStructuralPos());
		// cek delegasi
		AdmDelegasi del = new AdmServiceImpl().getDelegasiByUserDari(prcMainHeader.getAdmUserByPpmCurrentUser());
		if (del != null) {
			prcMainHeader.setAdmUserByPpmCurrentUser(del.getKepada());
			session.merge(prcMainHeader);
		}
		// 
		histMainComment.setUsername(prcMainHeader.getAdmUserByPpmCurrentUser().getCompleteName());
		histMainComment.setProses(prcMainHeader.getAdmProses().getId());
		if (SessionGetter.isNotNull(histMainComment.getCreatedBy())) {
			histMainComment.setProsesAksi(histMainComment.getCreatedBy());
			histMainComment.setCreatedBy("");
		}
		histMainComment.setAksi("");
		histMainComment.setDocument("");
		histMainComment.setComment("");
		session.save(histMainComment);

		// SEND MAIL
		// send mail
		Object o[] = new Object[2];
		if (prcMainHeader.getPpmNomorRfq() != null) {
			o[0] = prcMainHeader.getPpmNomorRfq();
		} else {
			o[0] = prcMainHeader.getPpmNomorPr();
		}
		o[1] = histMainComment.getProsesAksi();
		String message = SessionGetter.getPropertiesValue(
				"eproc.email.notif.internal", o);
		EmailSessionBean.sendMail("eProc PT. Semen Baturaja (Persero)",
				message, prcMainHeader.getAdmUserByPpmCurrentUser()
						.getAdmEmployee().getEmpEmail());
		System.out.println("eProc PT. Semen Baturaja (Persero)" + " " +
				message + " " + prcMainHeader.getAdmUserByPpmCurrentUser()
				.getAdmEmployee().getEmpEmail());
		
		
			if(prcMainHeader.getPanitia()!=null){
				
				if(StringUtils.equalsIgnoreCase(prcMainHeader.getAdmRole().getRoleName(), "panitia pengadaan")){
					PrcService prcService = new PrcServiceImpl();
					List<PanitiaPengadaanDetail> listPanitia = prcService.getListPanitiaPengadaanDetail(prcMainHeader.getPanitia());
					for(PanitiaPengadaanDetail pan: listPanitia){
						Object obj[] = new Object[2];
						if (prcMainHeader.getPpmNomorRfq() != null) {
							obj[0] = prcMainHeader.getPpmNomorRfq();
						} else {
							obj[0] = prcMainHeader.getPpmNomorPr();
						}
						obj[1] = histMainComment.getProsesAksi();
						message = SessionGetter.getPropertiesValue(
								"eproc.email.notif.internal", obj);
						EmailSessionBean.sendMail("eProc PT. Semen Baturaja (Persero)",
								message, pan.getAnggota().getEmail());
					}
				}	
			}
		
		
	}

	/**
	 * Save Global History for PR and OE
	 * 
	 * @param histMainComment
	 * @param prcMainHeader
	 * @param session
	 * @throws Exception
	 */
	public static void saveHistoryGlobalPanitia(
			HistMainComment histMainComment, PrcMainHeader prcMainHeader,
			Session session) throws Exception {
		try{
			HistMainComment last = SessionGetter
					.getLastCommentByHeader(prcMainHeader);
			last.setUsername(SessionGetter.getUser().getCompleteName());
			last.setPosisi(SessionGetter.getUser().getAdmEmployee()
					.getEmpStructuralPos());
			last.setAksi(histMainComment.getAksi());
			last.setProses(prcMainHeader.getAdmProses().getId());
			last.setDocument(histMainComment.getDocument());
			last.setComment(histMainComment.getComment());
			session.saveOrUpdate(last);

			// next user
			histMainComment.setPrcMainHeader(prcMainHeader);
			histMainComment.setUsername(prcMainHeader.getAdmUserByPpmCurrentUser()
					.getCompleteName());
			histMainComment.setPosisi(prcMainHeader.getAdmUserByPpmCurrentUser()
					.getAdmEmployee().getEmpStructuralPos());
			histMainComment.setProses(prcMainHeader.getAdmProses().getId());
			if (SessionGetter.isNotNull(histMainComment.getCreatedBy())) {
				histMainComment.setProsesAksi(histMainComment.getCreatedBy());
				histMainComment.setCreatedBy("");
			}
			histMainComment.setAksi("");
			histMainComment.setDocument("");
			histMainComment.setComment("");
			session.save(histMainComment);

			// SEND MAIL
			// send mail
			
					Object o[] = new Object[2];
					if (prcMainHeader.getPpmNomorRfq() != null) {
						o[0] = prcMainHeader.getPpmNomorRfq();
					} else {
						o[0] = prcMainHeader.getPpmNomorPr();
					}
					o[1] = histMainComment.getProsesAksi();
					String message = SessionGetter.getPropertiesValue(
							"eproc.email.notif.internal", o);
					EmailSessionBean.sendMail("eProc PT. Semen Baturaja (Persero)",
							message, prcMainHeader.getAdmUserByPpmCurrentUser().getEmail());
			
		} catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		
	}

	/**
	 * Save Global History for PR and OE
	 * 
	 * @param histMainComment
	 * @param prcMainHeader
	 * @param session
	 * @throws Exception
	 */
	public static void saveHistoryGlobalContract(
			CtrContractComment histMainComment,
			CtrContractHeader prcMainHeader, Session session) throws Exception {
		CtrContractComment last = SessionGetter
				.getLastCommentContractByHeader(prcMainHeader);
		last.setAksi(histMainComment.getAksi());
		last.setProses(prcMainHeader.getAdmProses().getId());
		last.setDocument(histMainComment.getDocument());
		last.setComment(histMainComment.getComment());
		System.out.println(last.getAksi() + " " + last.getComment());
		session.saveOrUpdate(last);
		System.out.println(last.getAksi() + " " + last.getComment());

		// next user
		// cek delegasi
		AdmDelegasi del = new AdmServiceImpl()
				.getDelegasiByUserDari(prcMainHeader.getAdmUserByCurrentUser());
		if (del != null) {
			prcMainHeader.setAdmUserByCurrentUser(del.getKepada());
			session.saveOrUpdate(prcMainHeader);
		}

		histMainComment.setPrcMainHeader(prcMainHeader.getPrcMainHeader());
		histMainComment.setContractId(new BigDecimal(prcMainHeader
				.getContractId()));
		histMainComment.setUsername(prcMainHeader.getAdmUserByCurrentUser()
				.getCompleteName());
		histMainComment.setPosisi(prcMainHeader.getAdmUserByCurrentUser()
				.getAdmEmployee().getEmpStructuralPos());
		histMainComment.setProses(prcMainHeader.getAdmProses().getId());
		if (SessionGetter.isNotNull(histMainComment.getCreatedBy())) {
			histMainComment.setProsesAksi(histMainComment.getCreatedBy());
			histMainComment.setCreatedBy("");
		}
		histMainComment.setAksi("");
		histMainComment.setDocument("");
		histMainComment.setComment("");
		session.save(histMainComment);

		// SEND MAIL
		// send mail
		Object o[] = new Object[2];
		if (prcMainHeader.getContractNumber() != null) {
			o[0] = prcMainHeader.getContractNumber();
		} else {
			o[0] = prcMainHeader.getContractNumber();
		}
		o[1] = histMainComment.getProsesAksi();
		String message = SessionGetter.getPropertiesValue(
				"eproc.email.notif.internal", o);
		EmailSessionBean.sendMail("eProc PT. Semen Baturaja (Persero)",
				message, prcMainHeader.getAdmUserByCurrentUser()
						.getAdmEmployee().getEmpEmail());
	}

	public static void saveHistoryGlobalKontrakFinish(
			CtrContractComment histMainComment,
			CtrContractHeader prcMainHeader, Session session) {
		CtrContractComment last = SessionGetter
				.getLastCommentContractByHeader(prcMainHeader);
		last.setProsesAksi(histMainComment.getProsesAksi());
		last.setAksi(histMainComment.getAksi());
		last.setProses(prcMainHeader.getAdmProses().getId());
		last.setDocument(histMainComment.getDocument());
		last.setComment(histMainComment.getComment());
		session.saveOrUpdate(last);
	}

	/**
	 * gte properties based on properties name
	 * 
	 * @param prop
	 * @return
	 */
	public static String getPropertiesValue(String prop, Object[] param) {
		ResourceReader rs = new SessionGetter().new ResourceReader();
		MessageFormat msgFormat = new MessageFormat(rs.getText(prop));
		if (param != null)
			return msgFormat.format(param);
		else
			return rs.getText(prop);
	}

	class ResourceReader extends ActionSupport {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public ResourceReader() {

		}
	}

	/**
	 * Get proses berdasarkan ID
	 * 
	 * @param i
	 * @return
	 * @throws Exception
	 */
	public static AdmProses getProsesFlowByLevel(int i) throws Exception {
		return new AdmServiceImpl().getProsesFlowByLevel(new AdmProses(i));
	}

	/**
	 * khusus proses pengadaan asumsi : satu district hanya 1 pelaku satker
	 * proses pengadaan
	 * 
	 * @param proses
	 * @param level
	 * @param admDistrict
	 * @param limit
	 * @return
	 * @throws Exception
	 */
	public static AdmProsesHirarki getHirarkiProsesByDistrict(AdmProses proses,
			int level, AdmDistrict admDistrict, double doubleValue)
			throws Exception {
		return new AdmServiceImpl().getHirarkiProsesPengadaanGlobal(proses,
				level, admDistrict, doubleValue);
	}
	
	
	public static AdmUserManager getUserByJenisKodeSatker(AdmJenis jenis, Integer jabatan, Integer kodeSatker)
			throws Exception {
		return new AdmServiceImpl().getUserByJenisKodeSatker(jenis, jabatan, kodeSatker);
	}
	
	public static AdmUserManager getUserByJenis(AdmJenis jenis,Integer jabatan)
			throws Exception {
		return new AdmServiceImpl().getUserByJenis(jenis,jabatan);
	}
	
	
	public static AdmUser getManagerByPr(PrcMainHeader prcMainHeader) throws Exception{
		return new AdmServiceImpl().getManagerByPr(prcMainHeader);
	}
	
	public static AdmProsesHirarki getHirarkiProsesByJenis(AdmProses proses,
			int level, AdmDistrict admDistrict, double doubleValue, Integer jenis)
			throws Exception {
		return new AdmServiceImpl().getHirarkiProsesPengadaanJenis(proses,
				level, admDistrict, doubleValue, jenis);
	}

	public static AdmRole EstimatorRole() {
		return new AdmRole(108);
	}

	public static void saveHistoryGlobalFinish(HistMainComment histMainComment,
			PrcMainHeader prcMainHeader, Session session) {
		HistMainComment last = SessionGetter
				.getLastCommentByHeader(prcMainHeader);
		last.setAksi(histMainComment.getAksi());
		last.setProses(prcMainHeader.getAdmProses().getId());
		last.setDocument(histMainComment.getDocument());
		last.setComment(histMainComment.getComment());
		session.saveOrUpdate(last);
	}

	public static String objectToString(Object object) {
		return String.valueOf(object);
	}

	public static String objectToStringNumber(Object object) {
		if (object != null) {
			return String.valueOf(object);
		} else {
			return "0";
		}
	}

	public static AdmMailConfig getMailConfig() {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmMailConfig.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		return (AdmMailConfig) new AdmServiceImpl()
				.getEntityByDetachedCiteria(crit);
	}

	public static String nullToEmptyString(String obj) {
		if (obj == null)
			return "";
		else {
			if (obj.equalsIgnoreCase("null"))
				return "";
			else
				return obj;
		}
	}

	public static String nullObjectToEmptyString(Object obj) {
		if (obj == null)
			return "";
		else
			return String.valueOf(obj);
	}

	public static String nullNumberToEmptyString(Object obj) {
		if (obj == null)
			return "0";
		else
			return String.valueOf(obj);
	}

	public static String valueProperty(String key) {
		return new AdmServiceImpl().getPropertyByKey(key);
	}

	/**
	 * Generate random string
	 * 
	 * @param len
	 * @return
	 */
	public static String randomString(int len) {
		String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		Random rnd = new Random();
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++)
			sb.append(AB.charAt(rnd.nextInt(AB.length())));
		return sb.toString();
	}

	public static AdmEmployee getEmployeeByRoleByDistrict(
			AdmRole admRoleByCurrentPos, AdmDistrict admDistrict) {
		DetachedCriteria crit = DetachedCriteria.forClass(AdmEmployee.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.setFetchMode("admUser", FetchMode.JOIN);
		crit.add(Restrictions.or(Restrictions.or(
				Restrictions.eq("admRoleByEmpRole1", admRoleByCurrentPos),
				Restrictions.eq("admRoleByEmpRole2", admRoleByCurrentPos)),
				Restrictions.or(Restrictions.eq("admRoleByEmpRole3",
						admRoleByCurrentPos), Restrictions.eq(
						"admRoleByEmpRole4", admRoleByCurrentPos))));
		DetachedCriteria sb = crit.createCriteria("admDept",
				JoinType.LEFT_OUTER_JOIN);
		sb.add(Restrictions.eq("admDistrict", admDistrict));
		return (AdmEmployee) new AdmServiceImpl()
				.getEntityByDetachedCiteria(crit);
	}

	/**
	 * cek if local address
	 * 
	 * @param ipAddress
	 * @return
	 * @throws Exception
	 */
	public static boolean localIpAddress(String ipAddress) throws Exception {
		boolean retMe = false;
		Inet4Address inet4Address;

		try {
			inet4Address = (Inet4Address) InetAddress.getByName(ipAddress);
			retMe = inet4Address.isSiteLocalAddress();
			if (ipAddress.equalsIgnoreCase("127.0.0.1")
					|| ipAddress.equalsIgnoreCase("127.0.1.1")) {
				retMe = true;
			}
		} catch (UnknownHostException exception) {
			System.out.println("UnknownHostException");
		}
		return retMe;
	}

}