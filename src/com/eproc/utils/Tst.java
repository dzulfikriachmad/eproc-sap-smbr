package com.eproc.utils;

public class Tst { 
 private final String foo = "Boiled Eggs"; 
 
 public static double SquareRoot( double value ) throws ArithmeticException 
 { 
  if (value >= 0) return Math.sqrt( value ); 
  else throw new ArithmeticException(); 
 } 

 public static double func(int x) { 
   double y = (double) x; 
   try { 
     y = SquareRoot( y ); 
   } 
   catch (ArithmeticException e) { y = 0; } 
   finally { --y; }   
   return y; 
 }
 
 public static void main(String[] args) throws Exception { 
	 
	 System.out.println(func(4));
 } 

 public String toString() { 
   return foo; 
 } 
}