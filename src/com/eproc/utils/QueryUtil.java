package com.eproc.utils;

import com.orm.model.AdmUser;
import com.orm.model.OrDetail;
import com.orm.model.OrHeader;
import com.orm.model.OrHeaderId;

public class QueryUtil {

	/**
	 * Query Jumlah Pengadaan Vendor based On Status
	 * 
	 * @param vendorId
	 * @param status
	 * @return
	 */
	public static String jumlahDashboardVendor(Long vendorId, Integer status) {
		String query = " select count(*) from prc_main_vendor a "
				+ " inner join prc_main_header b on a.ppm_id=b.ppm_id "
				+ " inner join prc_main_preparation c on a.ppm_id = c.ppm_id"
				+ "	left join prc_main_vendor_quote d on a.ppm_id = d.ppm_id"
				+ " where a.pmp_status= " + status + " " + " and a.vendor_id="
				+ vendorId + " and (b.ppm_proses_level !="
				+ SessionGetter.SELESAI + " and " + " b.ppm_proses_level !="
				+ SessionGetter.BATAL + " and b.ppm_proses_level !="
				+ SessionGetter.REBID + " and b.ppm_proses_level !="
				+ SessionGetter.MANUAL + ")";
		return query;
	}
	
	

	/**
	 * 
	 * @param vendorId
	 * @param status
	 * @return
	 */
	
	public static String todoListVendorSQL(Long vendorId, Integer status) {
		String query = " select c.ppm_id as ppmId, a.vendor_id as vendorId, c.ppm_nomor_rfq as nomorRfq, "
				+ " b.registrastion_opening as daftarBuka, b.registrastion_closing as daftarTutup, "
				+ " b.aanwijzing_date as aanwijzingDate, b.quotation_opening as quotationDate"
				+ " from prc_main_vendor a inner join prc_main_preparation b on a.ppm_id = b.ppm_id "
				+ " inner join prc_main_header c on c.ppm_id = b.ppm_id "
				+ " where a.vendor_id="
				+ vendorId
				+ " and a.pmp_status="
				+ status
				+ " and (c.ppm_proses_level !="
				+ SessionGetter.SELESAI
				+ " and "
				+ " c.ppm_proses_level !="
				+ SessionGetter.BATAL
				+ " and c.ppm_proses_level !="
				+ SessionGetter.REBID 
				+ " and c.ppm_proses_level !="
				+ SessionGetter.MANUAL + ") ";
		return query;
	}

	/**
	 * 
	 * @param vendorId
	 * @param status
	 * @param status2
	 * @return
	 */
	public static String todoListVendorSQL(Long vendorId, Integer status,
			Integer status2) {
		String query = "select c.ppm_id as ppmId, a.vendor_id as vendorId, c.ppm_nomor_rfq as nomorRfq, "
				+ " b.registrastion_opening as daftarBuka, b.registrastion_closing as daftarTutup, "
				+ " b.aanwijzing_date as aanwijzingDate, b.quotation_opening as quotationDate"
				+ " from prc_main_vendor a inner join prc_main_preparation b on a.ppm_id = b.ppm_id "
				+ " inner join prc_main_header c on c.ppm_id = b.ppm_id "
				+ " where a.vendor_id= "
				+ vendorId
				+ " and (a.pmp_status= "
				+ status
				+ " or a.pmp_status= "
				+ status2
				+ ") and (c.ppm_proses_level != "
				+ SessionGetter.SELESAI
				+ " and "
				+ " c.ppm_proses_level != "
				+ SessionGetter.BATAL
				+ " and c.ppm_proses_level != " + SessionGetter.REBID 
				+ " and c.ppm_proses_level != " + SessionGetter.MANUAL
				+ ")";
		return query;
	}

	/**
	 * 
	 * @param vendorId
	 * @param status
	 * @param status2
	 * @return
	 */
	public static String todoListVendorSQLNego(Long vendorId) {
		String query = "select c.ppm_id as ppmId, a.vendor_id as vendorId, c.ppm_nomor_rfq as nomorRfq, "
				+ " b.registrastion_opening as daftarBuka, b.registrastion_closing as daftarTutup, "
				+ " b.aanwijzing_date as aanwijzingDate, b.quotation_opening as quotationDate"
				+ " from prc_main_vendor a inner join prc_main_preparation b on a.ppm_id = b.ppm_id "
				+ " inner join prc_main_header c on c.ppm_id = b.ppm_id "
				+ " where a.vendor_id="
				+ vendorId
				+ " and (a.pmp_is_negotatiate=1) and (c.ppm_proses_level !="
				+ SessionGetter.SELESAI
				+ " and "
				+ " c.ppm_proses_level !="
				+ SessionGetter.BATAL
				+ " and c.ppm_proses_level !="
				+ SessionGetter.REBID 
				+ " and c.ppm_proses_level !="
				+ SessionGetter.MANUAL
				+ ") and c.ppm_proses_level=5";
		return query;
	}

	/**
	 * 
	 * @param vendorId
	 * @param status
	 * @return
	 */
	public static String todoListVendorHQL(Long vendorId, Integer status) {
		String query = " select c.ppmId as ppmId, a.vendorId as vendorId, c.ppmNomorRfq as nomorRfq, "
				+ " b.registrastionOpening as daftarBuka, b.registrastionClosing as daftarTutup, "
				+ " b.aanwijzingDate as aanwijzingDate, b.quotationOpening as quotationDate"
				+ " from PrcMainVendor a inner join PrcMainPreparation b on a.prcMainHeader = b.prcMainHeader "
				+ " inner join PrcMainHeader c on c.ppmId = b.prcMainHeader.ppmId "
				+ " where a.vendorId="
				+ vendorId
				+ " and a.pmpStatus="
				+ status
				+ " and (c.ppmProsesLevel !="
				+ SessionGetter.SELESAI
				+ " and "
				+ " c.ppmProsesLevel !="
				+ SessionGetter.BATAL
				+ " c.ppmProsesLevel !="
				+ SessionGetter.MANUAL
				+ " and c.ppmProsesLevel !=" + SessionGetter.REBID + ") ";
		return query;
	}

	/**
	 * Query tabulasi penilaian
	 * 
	 * @param ppmId
	 * @return
	 */
	public static String tabulasiEvaluasi(Long ppmId) {
		String query = "select "
				+ " b.vendor_id as vendorId,"
				+ " b.vendor_name as vendorName,"
				+ " a.pmp_adm_status as pmpAdmStatus,"
				+ " e.template_passing_grade as passingGrade,"
				+ " e.template_tech_weight as bobotTeknis,"
				+ " a.pmp_technical_value as nilaiTeknis,"
				+ " e.template_price_weight as bobotHarga,"
				+ " a.pmp_price_value as nilaiHarga,"
				+ " c.pqm_total_penawaran as totalPenawaran,"
				+ " a.pmp_technical_status as statusTeknis,"
				+ " a.pmp_technical_notes as catatanTeknis,"
				+ " a.pmp_price_status as statusHarga,"
				+ " a.pmp_price_notes as catatanHarga, "
				+ " a.pmp_eval_total as totalNilai, "
				+ " a.ppm_id as ppmId, "
				+ " c.pqm_total_penawaran_nego as totalPenawaranNego,"
				+ " c.pqm_total_eauction as totalEauction,"
				+ " a.price_evaluated as priceEvaluated,"
				+ " a.tech_evaluated as techEvaluated"
				+ " from "
				+ " prc_main_vendor a "
				+ " inner join vnd_header b on a.vendor_id = b.vendor_id "
				+ " inner join prc_main_vendor_quote c on c.ppm_id = a.ppm_id and c.vendor_id=a.vendor_id "
				+ " inner join prc_main_header d on d.ppm_id=a.ppm_id "
				+ " inner join prc_template e on e.id = d.prc_template "
				+ " where "
				+ " (a.pmp_status=5 or a.pmp_status=6 or a.pmp_status=7 or a.pmp_status=-6 or a.pmp_status=-7) and a.ppm_id="
				+ ppmId
				+ " "
				+ "order by a.pmp_eval_total, a.pmp_technical_value,a.pmp_price_value asc ";
		return query;
	}

	public static String tabulasiHarga(Long ppmId) {
		String query = "select "
				+ " a.id as id,"
				+ " a.ppm_id as ppmId,"
				+ " a.item_description as deskripsi,"
				+ " a.item_quantity_oe as jumlah,"
				+ " a.item_price_oe as harga,"
				+ " bb.vendor_name as vendorName,"
				+ " (select x.item_quantity from prc_main_vendor_quote_item x where x.ppm_id = b.ppm_id and x.vendor_id = b.vendor_id) as jumlahVendor,"
				+ " (select x.item_price from prc_main_vendor_quote_item x where x.ppm_id = b.ppm_id and x.vendor_id = b.vendor_id) as hargaVendor,"
				+ " (select x.pqm_total_penawaran from prc_main_vendor_quote x where x.ppm_id = b.ppm_id and x.vendor_id = b.vendor_id) as totalPenawaran "
				+ " from "
				+ " prc_main_item a left join "
				+ " prc_main_vendor b on a.ppm_id = b.ppm_id  left join vnd_header bb on b.vendor_id = bb.vendor_id"
				+ " left join prc_main_header c on c.ppm_id = b.ppm_id"
				+ " left join prc_main_vendor_quote_item d on d.ppm_id = b.ppm_id and d.vendor_id = b.vendor_id"
				+ " left join prc_main_vendor_quote e on e.ppm_id = b.ppm_id and e.vendor_id=b.vendor_id"
				+ " where b.pmp_status=5 or b.pmp_status=6 or b.pmp_status=7 or b.pmp_status=-6 or b.pmp_status=-7 and a.ppm_id="
				+ ppmId + " "
				+ " order by a.ppm_id, e.pqm_total_penawaran, b.vendor_id asc";
		return query;
	}

	public static String jumlahNegosiasi(long vendorId, int nego) {
		String query = " select count(*) from prc_main_vendor a "
				+ " inner join prc_main_header b on a.ppm_id=b.ppm_id "
				+ " inner join prc_main_preparation c on a.ppm_id = c.ppm_id"
				+ " where a.pmp_is_negotatiate=" + nego + " "
				+ " and a.vendor_id=" + vendorId
				+ " and (b.ppm_proses_level !=" + SessionGetter.SELESAI
				+ " and " + " b.ppm_proses_level !=" + SessionGetter.BATAL
				+ " and b.ppm_proses_level !=" + SessionGetter.REBID
				+ " and b.ppm_proses_level !=" + SessionGetter.MANUAL
				+ ") and b.ppm_proses_level=5";
		return query;
	}

	/**
	 * default proddta. <-- crpdta.
	 * 
	 * @param obj
	 * @return
	 */
	public static String headerOr(Object obj) {
		String query = "select distinct "
				+ " a.pddoco as nomorOr, "
				+ // 0
				" a.pdkcoo as siteHeader, "
				+ // 1
				" a.pddcto as orh, "
				+ // 2
				" a.pdnxtr as nextStatus, "
				+ // 3
				" a.pdlttr as lastStatus, "
				+ // 4
				// " a.pdtrdj as tglOr, " +
				// " a.pdrdj tglAprove, " +
				" c.aban8 as pembuatOm, "
				+ " c.abalph as namaPembuatOm, "
				+ " a.pdoorn as nomorOm, "
				+ " a.pdocto omOn, "
				+ " a.pdokco as site, "
				+ " a.pdanby as unitKerja, "
				+ " a.pdsbl as wo,"
				+ " a.pddrqj as tglOr,"
				+ " a.pdcrcd as mataUang,"
				+ " i.phancr as kodeSatker, "
				+ " i.phartg as pembuatOrjasa "
				+ " from "
				+ SessionGetter.valueProperty("jde.schema")
				+ ".f4311 a "
				+ " left outer join "
				+ SessionGetter.valueProperty("jde.schema")
				+ ".f4311 d on a.pdoorn = to_char(d.pddoco) and d.pddcto='ON' and d.pdkcoo=a.pdkcoo "
				+ " left join sy812.f0092 b on d.pdtorg=b.uluser left join "
				+ SessionGetter.valueProperty("jde.schema")
				+ ".f0101 c on c.aban8 = b.ulan8  "
				+
				// " left join proddta.f4209 h on h.hokcoo = a.pdkcoo and h.hodoco = a.pddoco and h.hodcto=a.pddcto "
				// +
				" left join "
				+ SessionGetter.valueProperty("jde.schema")
				+ ".f4301 i on a.pddoco = i.phdoco and a.pdkcoo=i.phkcoo and a.pddcto = i.phdcto "
				+ " where a.pdnxtr='130' and a.pdlttr='125' and a.PDDCTO='OR' "
				+ " and trunc(to_date(substr(i.phtrdj,2,5),'YYDDD')) >= trunc(to_date('02/10/2013','DD/MM/YYYY'))";
		return query;
	}
	
	public static String headerZr(Object obj) {
		String query = "select distinct "
				+ " a.pddoco as nomorOr, "
				+ // 0
				" a.pdkcoo as siteHeader, "
				+ // 1
				" a.pddcto as orh, "
				+ // 2
				" a.pdnxtr as nextStatus, "
				+ // 3
				" a.pdlttr as lastStatus, "
				+ // 4
				// " a.pdtrdj as tglOr, " +
				// " a.pdrdj tglAprove, " +
				" c.aban8 as pembuatOm, "
				+ " c.abalph as namaPembuatOm, "
				+ " a.pdoorn as nomorOm, "
				+ " a.pdocto omOn, "
				+ " a.pdokco as site, "
				+ " a.pdanby as unitKerja, "
				+ " a.pdsbl as wo,"
				+ " a.pddrqj as tglOr,"
				+ " a.pdcrcd as mataUang,"
				+ " i.phancr as kodeSatker,"
				+ " i.phartg as pembuatOrjasa "
				+ " from "
				+ SessionGetter.valueProperty("jde.schema")
				+ ".f4311 a "
				+ " left outer join "
				+ SessionGetter.valueProperty("jde.schema")
				+ ".f4311 d on a.pdoorn = to_char(d.pddoco) and d.pddcto='ON' and d.pdkcoo=a.pdkcoo "
				+ " left join sy812.f0092 b on d.pdtorg=b.uluser left join "
				+ SessionGetter.valueProperty("jde.schema")
				+ ".f0101 c on c.aban8 = b.ulan8  "
				+
				// " left join proddta.f4209 h on h.hokcoo = a.pdkcoo and h.hodoco = a.pddoco and h.hodcto=a.pddcto "
				// +
				" left join "
				+ SessionGetter.valueProperty("jde.schema")
				+ ".f4301 i on a.pddoco = i.phdoco and a.pdkcoo=i.phkcoo and a.pddcto = i.phdcto "
				+ " where a.pdnxtr='130' and a.pdlttr='125' and a.PDDCTO='ZR' "
				+ " and trunc(to_date(substr(i.phtrdj,2,5),'YYDDD')) >= trunc(to_date('02/10/2013','DD/MM/YYYY'))";
		return query;
	}

	public static String headerOrConcat(Object obj) {
		String query = "select distinct "
				+ " a.pddoco as nomorOr, "
				+ // 0
				" a.pdkcoo as siteHeader, "
				+ // 1
				" a.pddcto as orh, "
				+ // 2
				" a.pdnxtr as nextStatus, "
				+ // 3
				" a.pdlttr as lastStatus, "
				+ // 4
				// " a.pdtrdj as tglOr, " +
				// " a.pdrdj tglAprove, " +
				" c.aban8 as pembuatOm, "
				+ " c.abalph as namaPembuatOm, "
				+ " a.pdoorn as nomorOm, "
				+ " a.pdocto omOn, "
				+ " a.pdokco as site, "
				+ " a.pdanby as unitKerja, "
				+ " a.pdsbl as wo,"
				+ " a.pddrqj as tglOr,"
				+ " a.pdcrcd as mataUang, "
				+ " i.phancr as kodeSatker "
				+ " from "
				+ SessionGetter.valueProperty("jde.schema")
				+ ".f4311 a "
				+ " inner join "
				+ SessionGetter.valueProperty("jde.schema")
				+ ".f4311 d on a.pdoorn = concat('0',to_char(d.pddoco)) and d.pddcto='ON' and d.pdkcoo=a.pdkcoo "
				+ " left join sy812.f0092 b on d.pdtorg=b.uluser left join "
				+ SessionGetter.valueProperty("jde.schema")
				+ ".f0101 c on c.aban8 = b.ulan8  "
				+
				// " left join proddta.f4209 h on h.hokcoo = a.pdkcoo and h.hodoco = a.pddoco and h.hodcto=a.pddcto "
				// +
				" left join "
				+ SessionGetter.valueProperty("jde.schema")
				+ ".f4301 i on a.pddoco = i.phdoco and a.pdkcoo=i.phkcoo and a.pddcto = i.phdcto "
				+ " where a.pdnxtr='130' and a.pdlttr='125' "
				+ " and trunc(to_date(substr(i.phtrdj,2,5),'YYDDD')) >= trunc(to_date('02/10/2013','DD/MM/YYYY'))";
		return query;
	}

	public static String detailOr(OrHeaderId headerId) {
		String query = "select distinct "
				+ " a.pddoco as nomorOr, "
				+ // 0
				" a.pdkcoo as siteHeader, "
				+ // 1
				" a.pddcto as orh, "
				+ // 2
				" a.pdlnid as lineId, "
				+ // 3
				" a.pdlitm as kodeBarang, "
				+ // 4
				" a.pddsc1 as namaBarang, "
				+ // 5
				" a.pddsc2 as namaBarangExt, "
				+ // 6
				" a.pduorg as jumlah, "
				+ // 7
				" a.pduom as satuan, "
				+ // 8
				" a.pdprrc as hargaSatuan, "
				+ // 9
				" a.pdlnty as lineType, "
				+ // 10
				" a.pdani as  accountNumber "
				+ // 11
				" from " + SessionGetter.valueProperty("jde.schema")
				+ ".f4311 a where a.pdnxtr='130' and a.pdlttr='125' "
				+ " and a.pddoco ='" + headerId.getNomorOr()
				+ "' and a.pdkcoo='" + headerId.getHeaderSite()
				+ "' and a.pddcto='" + headerId.getOrH() + "' ";
		return query;
	}

	public static String getAttachmentOr(OrHeader orHeader) {
		String gdtxky = orHeader.getId().getNomorOr() + "|"
				+ orHeader.getId().getOrH().trim() + "|"
				+ orHeader.getId().getHeaderSite().trim() + "|000";
		String query = "select UTL_RAW.CAST_TO_VARCHAR2(DBMS_LOB.SUBSTR(a.gdtxft, 2000,1)) as attachment  from "
				+ SessionGetter.valueProperty("jde.schema")
				+ ".f00165 a "
				+ "where a.gdtxky='" + gdtxky + "'";
		return query;
	}

	public static String getContentHeaderData(OrHeader orHeader) {
		String gdtxky = orHeader.getId().getNomorOr() + "|"
				+ orHeader.getId().getOrH() + "|"
				+ orHeader.getId().getHeaderSite() + "|000";
		String query = "select a.gdtxft as attachment  from "
				+ SessionGetter.valueProperty("jde.schema") + ".f00165 a "
				+ "where a.gdtxky='" + gdtxky + "'";
		return query;
	}

	public static String getContenerHeadrName(OrHeader orHeader) {
		String gdtxky = orHeader.getId().getNomorOr() + "|"
				+ orHeader.getId().getOrH().trim() + "|"
				+ orHeader.getId().getHeaderSite().trim() + "|000";
		String query = "select trim(replace(a.gdgtfilenm,'\\','/')) as attachment  from "
				+ SessionGetter.valueProperty("jde.schema")
				+ ".f00165 a "
				+ "where a.gdtxky='" + gdtxky + "'";
		return query;
	}

	public static String getAttachmentOrDetail(OrDetail orHeader) {
		int n = 0;
		if (orHeader.getLine() > 9000) {
			n = orHeader.getLine() / 1000;
		} else {
			n = orHeader.getLine() / 1000;
		}
		String gdtxky = orHeader.getId().getNomorOr() + "|"
				+ orHeader.getId().getHeaderSite().trim() + "|"
				+ orHeader.getId().getOrH().trim() + "|000|"
				+ String.valueOf(n).trim() + "%";
		String query = "select UTL_RAW.CAST_TO_VARCHAR2(DBMS_LOB.SUBSTR(a.gdtxft, 2000,1)) as attachment  from "
				+ SessionGetter.valueProperty("jde.schema")
				+ ".f00165 a "
				+ "where a.gdtxky like '" + gdtxky + "'";
		return query;
	}

	public static String getContentDetailData(OrDetail orHeader) {
		int n = 0;
		if (orHeader.getLine() > 9000) {
			n = orHeader.getLine() / 1000;
		} else {
			n = orHeader.getLine() / 1000;
		}
		String gdtxky = orHeader.getId().getNomorOr() + "|"
				+ orHeader.getId().getHeaderSite() + "|"
				+ orHeader.getId().getOrH() + "|000|" + String.valueOf(n) + "%";
		String query = "select a.gdtxft as attachment  from "
				+ SessionGetter.valueProperty("jde.schema") + ".f00165 a "
				+ "where a.gdtxky like '" + gdtxky + "'";
		return query;
	}

	public static String getContentDetailName(OrDetail orHeader) {
		int n = 0;
		if (orHeader.getLine() > 9000) {
			n = orHeader.getLine() / 1000;
		} else {
			n = orHeader.getLine() / 1000;
		}
		String gdtxky = orHeader.getId().getNomorOr() + "|"
				+ orHeader.getId().getHeaderSite().trim() + "|"
				+ orHeader.getId().getOrH().trim() + "|000|"
				+ String.valueOf(n).trim() + "%";
		String query = "select trim(replace(a.gdgtfilenm,'\\','/')) as attachment  from "
				+ SessionGetter.valueProperty("jde.schema")
				+ ".f00165 a "
				+ "where a.gdtxky like '" + gdtxky + "'";
		return query;
	}

	public static String todoListUserSpphPr(AdmUser user) {
		String query = "select distinct a.ppm_id as ppmId, "
				+ " a.ppm_nomor_pr as nomorPr, "
				+ " a.ppm_nomor_rfq as nomorSpph, "
				+ " a.ppm_current_user as currentUser, "
				+ " c.complete_name as completeName, "
				+ " a.updated_date as updatedDate, "
				+ " b.keterangan as keterangan, "
				+ " b.url as url, "
				+ " a.ppm_subject as subject, "
				+ " a.ppm_prioritas as prioritas"
				+ " from "
				+ " prc_main_header a left join adm_proses_hirarki b on a.ppm_proses = b.adm_proses and a.ppm_proses_level = b.tingkat "
				+ " inner join adm_user c on a.ppm_current_user = c.id and a.ppm_current_user="
				+ user.getId() + " and (a.ppm_proses_level !="
				+ SessionGetter.SELESAI + " and " + " a.ppm_proses_level !="
				+ SessionGetter.BATAL 
				+ " and a.ppm_proses_level !="
				+ SessionGetter.REBID
				+ " and a.ppm_proses_level !="
				+ SessionGetter.MANUAL
				+ ")";
		return query;
	}

	public static String todoListPanitiaPP(AdmUser user) {
		String query = "select distinct "
				+ " a.ppm_id, "
				+ " a.ppm_nomor_pr,"
				+ " a.ppm_nomor_rfq,"
				+ " a.ppm_current_user,"
				+ " b.complete_name,"
				+ " a.updated_date,"
				+ " e.keterangan,"
				+ " e.url,"
				+ " a.ppm_subject ,"
				+ " a.ppm_prioritas "
				+ " from "
				+ " prc_main_header a, "
				+ " adm_user b, "
				+ " panitia_pengadaan c,"
				+ " panitia_pengadaan_detail d,"
				+ " adm_proses_hirarki e "
				+ " where "
				+ " a.ppm_current_user=b.id "
				+ " and a.panitia=c.id "
				+ " and c.id=d.panitia "
				+ " and ((a.ppm_proses="
				+ SessionGetter.PANITIA_PP_PROSES_ID.getId()
				+ " and (d.status is null or d.status=0) and a.ppm_proses_level=1) or (a.ppm_proses="
				+ SessionGetter.RFQ_PROSES_ID.getId()
				+ " and (d.status=1 or d.status is null) and lower(b.complete_name) like '%panitia pengadaan%' )) "
				+ " and d.anggota="
				+ user.getId()
				+ " "
				+ " and a.ppm_proses = e.adm_proses "
				+ " and a.ppm_proses_level = e.tingkat and (a.ppm_proses_level !="
				+ SessionGetter.SELESAI + " and " + " a.ppm_proses_level !="
				+ SessionGetter.BATAL 
				+ " and a.ppm_proses_level !="
				+ SessionGetter.REBID 
				+ " and a.ppm_proses_level !="
				+ SessionGetter.MANUAL
				+ ")";
		return query;
	}

	public static String todoListLelangOpen() {
		String query = "select distinct a.ppm_id as ppmId, "
				+ " a.ppm_nomor_pr as nomorPr, "
				+ " a.ppm_nomor_rfq as nomorSpph, "
				+ " a.ppm_current_user as currentUser, "
				+ " c.complete_name as completeName, "
				+ " a.updated_date as updatedDate, "
				+ " b.keterangan as keterangan, "
				+ " b.url as url, "
				+ " a.ppm_subject as subject"
				+ " from "
				+ " prc_main_header a left join adm_proses_hirarki b on a.ppm_proses = b.adm_proses and a.ppm_proses_level = b.tingkat "
				+ " inner join adm_user c on a.ppm_current_user = c.id inner join prc_main_preparation d on d.ppm_id = a.ppm_id"
				+ " where (a.ppm_proses_level !="
				+ SessionGetter.SELESAI
				+ " and "
				+ " a.ppm_proses_level !="
				+ SessionGetter.BATAL
				+ " and a.ppm_proses_level !="
				+ SessionGetter.REBID
				+ " and a.ppm_proses_level !="
				+ SessionGetter.MANUAL
				+ ") and a.ppm_proses="
				+ SessionGetter.PQ_PROSES_ID.getId()
				+ " "
				+ " and a.ppm_proses_level=2 and d.registrastion_closing > ? and d.registrastion_opening < ?";
		return query;
	}

	/**
	 * list Eauction Vendor
	 * 
	 * @param ppmId
	 * @return
	 */
	public static String listVendorEauction(Long ppmId) {
		String query = "select a.id,b.ppm_id, c.vendor_name, a.last_bid, a.tgl_bid, to_char(a.tgl_bid,'dd Mon yyyy HH:mi:ss') as tglString from "
				+ " eauction_vendor a left join eauction_header b on a.eauction_header = b.ppm_id "
				+ " inner join vnd_header c on a.vnd_header = c.vendor_id "
				+ " where b.ppm_id =" + ppmId + " order by a.id ";
		return query;
	}

	public static String listHistoryEauction(Long ppmId) {
		String query = "select a.id, c.ppm_id, d.vendor_name, a.jumlah_bid, a.tgl_bid, to_char(a.tgl_bid,'dd Mon yyyy HH:mi:ss') as tglString from "
				+ " eauction_history a left join eauction_vendor b on a.eauction_vendor = b.id "
				+ " left join eauction_header c on b.eauction_header = c.ppm_id "
				+ " inner join vnd_header d on d.vendor_id = b.vnd_header "
				+ " where c.ppm_id=" + ppmId + " order by a.tgl_bid asc";
		return query;
	}

	public static String todoListKontrak(AdmUser user) {
		String query = "select distinct"
				+ " a.contract_id, "
				+ " b.ppm_nomor_rfq,"
				+ " a.contract_number,"
				+ " a.current_user,"
				+ " d.complete_name,"
				+ " a.updated_date,"
				+ " c.keterangan,"
				+ " c.url,"
				+ " b.ppm_subject,"
				+ " b.ppm_prioritas,"
				+ " e.vendor_name "
				+ " from "
				+ " ctr_contract_header a "
				+ " left join prc_main_header b on a.ppm_id = b.ppm_id "
				+ " left join adm_proses_hirarki c on a.contract_proses = c.adm_proses and a.contract_proses_level = c.tingkat "
				+ " left join adm_user d on d.id = a.current_user "
				+ " left join vnd_header e on e.vendor_id = a.vendor_id "
				+ " where a.current_user=" + user.getId() + " "
				+ " and (a.contract_proses_level !=" + SessionGetter.SELESAI
				+ " and " + " a.contract_proses_level !=" + SessionGetter.BATAL
				+ " and a.contract_proses_level !=" + SessionGetter.REBID + ")";
		return query;
	}

	public static String jumlahEauction(long vendorId) {
		String query = "select count(*) from "
				+ " eauction_header a "
				+ " left join eauction_vendor b on a.ppm_id = b.eauction_header "
				+ " where " + " a.tanggal_berakhir >= ? "
				+ " and b.vnd_header =" + vendorId + " ";

		return query;
	}

	public static String jumlahHistory(long vendorId) {
		String query = "select count(*) from "
				+ " prc_main_vendor a "
				+ " where a.vendor_id ="
				+ vendorId
				+ " "
				+ " and (a.pmp_status =2 or a.pmp_status =3 or a.pmp_status =4 or a.pmp_status =5 or a.pmp_status =6 or a.pmp_status =7 "
				+ " or a.pmp_status =-3 or a.pmp_status =-5 or a.pmp_status =-6) ";
		return query;
	}

	public static String historyPengadaan(long vendorId) {
		String query = "select b.ppm_id, a.vendor_id, b.ppm_nomor_rfq, a.pmp_status,  "
				+ "a.pmp_adm_status, a.pmp_technical_status, a.pmp_price_status,  "
				+ "CASE   WHEN (select count(1) from prc_main_item c where c.ppm_id = b.ppm_id and c.pemenang = a.vendor_id) >= 1 THEN 'Pemenang'   "
				+ "WHEN (select count(1) from prc_main_item c where c.ppm_id = b.ppm_id and c.pemenang <> a.vendor_id) >= 1 THEN 'Kalah'  ELSE ''  END "
				+ " from  prc_main_vendor a  inner join prc_main_header b on a.ppm_id = b.ppm_id  where a.vendor_id ="
				+ vendorId
				+ " "
				+ " and (a.pmp_status =2 or a.pmp_status =3 or a.pmp_status =4 or a.pmp_status =5 or a.pmp_status =6 or a.pmp_status =7 "
				+ " or a.pmp_status =-3 or a.pmp_status =-5 or a.pmp_status =-6 or a.pmp_status=-7)";
		return query;
	}

	public static String historyPo(long vendorId) {
		String query = "select contract_id, contract_number, contract_amount, jde_number, contract_subject, contract_start, contract_end "
				+ "from ctr_contract_header where vendor_id=" + vendorId;
		return query;
	}

	/**
	 * detail efisiensi
	 * 
	 * @return
	 */
	public static String detailEfisiensi() {
		String query = "select "
				+ " a.ppm_id, "
				+ " a.ppm_nomor_pr,"
				+ " a.ppm_jumlah_ee, "
				+ " a.ppm_jumlah_oe, "
				+ " (select sum(b.contract_amount) from ctr_contract_header b where b.ppm_id= a.ppm_id) as kontrak "
				+ " from prc_main_header a ";
		return query;
	}

	/*public static String procurementValue() {
		String query = "select "
				+ " c.id, "
				+ " c.method_name, "
				+ " sum(a.ppm_jumlah_ee) as EE, "
				+ " sum(a.ppm_jumlah_oe) as OE, "
				+ " sum((select sum(b.contract_amount) from ctr_contract_header b where b.ppm_id= a.ppm_id)) as kontrak,"
				+ " (select sum(dt) from (select count(dt) as dt,"
				+ "mt from (select 1 as dt, prc_method as mt from prc_main_header where ppm_proses_level=99 and created_date between ? and ?) group by mt) "
				+ "where mt=c.id group by mt) as frequensi "
				+ " from prc_main_header a inner join prc_method c on a.prc_method = c.id where a.created_date between ? and ? "
				+ " and 1=1 and a.ppm_proses_level=99 " + // buat tambahan
															// disini
				" group by c.id, c.method_name order by c.id asc ";
		return query;
	}*/
	
	public static String procurementValue() {
		String query = "select id , method_name, "
				+ "sum(EE) as EE, "
				+ "sum(OE) as OE,"
				+ "sum(kontrak) as kontrak, "
				+ "count(id) as frequensi from mtz_vw_procurement_value where created_date " 
				+ "between ? and ? group by id, method_name order by id asc";
		return query;
	}

	/*public static String procurementValueDetail(Integer metode) {
		String query = "select "
				+ " a.ppm_id, "
				+ " a.ppm_nomor_rfq, "
				+ " (select (max(trunc(updated_date))-min(trunc(created_date))) as hari from hist_main_comment where proses=102 and ppm_id=a.ppm_id) as h_oe, "
				+ " (select (max(trunc(updated_date))-min(trunc(created_date))) as hari from hist_main_comment where proses=104 and ppm_id=a.ppm_id) as h_ang, "
				+ " (select (max(trunc(updated_date))-min(trunc(created_date))) as hari from hist_main_comment where proses=103 and ppm_id=a.ppm_id) as h_pp, "
				+ " (select (max(trunc(updated_date))-min(trunc(created_date))) as hari from hist_main_comment where proses=107 and ppm_id=a.ppm_id) as h_pq, "
				+ " (select (max(trunc(updated_date))-min(trunc(created_date))) as hari from hist_main_comment where proses=105 and ppm_id=a.ppm_id) as h_spph, "
				+ " b.complete_name " + " from prc_main_header a "
				+ " left join adm_user b on a.ppm_adm_buyer = b.id "
				+ " where a.ppm_proses_level=99 and a.prc_method=" + metode;
		return query;
	}*/

	public static String procurementValueDetail(Integer metode) {
		/*	String query = "select "
					+ " a.ppm_id, "
					+ " a.ppm_nomor_rfq, "
					+ " (select (max(trunc(updated_date))-min(trunc(created_date))) as hari from hist_main_comment where proses=102 and ppm_id=a.ppm_id) as h_oe, "
					+ " (select (max(trunc(updated_date))-min(trunc(created_date))) as hari from hist_main_comment where proses=104 and ppm_id=a.ppm_id) as h_ang, "
					+ " (select (max(trunc(updated_date))-min(trunc(created_date))) as hari from hist_main_comment where proses=103 and ppm_id=a.ppm_id) as h_pp, "
					+ " (select (max(trunc(updated_date))-min(trunc(created_date))) as hari from hist_main_comment where proses=107 and ppm_id=a.ppm_id) as h_pq, "
					+ " (select (max(trunc(updated_date))-min(trunc(created_date))) as hari from hist_main_comment where proses=105 and ppm_id=a.ppm_id) as h_spph, "
					+ " b.complete_name " + " from prc_main_header a "
					+ " left join adm_user b on a.ppm_adm_buyer = b.id "
					+ " where a.ppm_proses_level=99 and a.prc_method=" + metode;*/
			String query = "select * from mtz_vw_proc_detail_all where prc_method = " + metode;
			return query;
		}
	public static String bidderList(Integer kantor, String komoditi,
			Integer klasifikasi, String namaVendor) {
		String query = "select distinct "
				+ " a.vendor_id, "
				+ " a.vendor_name as vendor_name, "
				+ " (select count(*) from prc_main_vendor where pmp_status>=2 and vendor_id=a.vendor_id) as diundang,"
				+ " (select count(*) from prc_main_vendor where pmp_status>=3 and vendor_id=a.vendor_id) as mendaftar,"
				+ " (select count(*) from prc_main_vendor where pmp_status>=5 and vendor_id=a.vendor_id) as lulusadm,"
				+ " (select count(*) from prc_main_vendor where pmp_status>=6 and vendor_id=a.vendor_id) as lulusteknis,"
				+ " (select count(*) from prc_main_vendor where pmp_status>=7 and vendor_id=a.vendor_id) as lulusharga, "
				+ " (select count(*) from (select distinct ppm_id,pemenang from prc_main_item) where pemenang=a.vendor_id) as menang "
				+ " from vnd_header a "
				+ " left join vnd_product b on a.vendor_id = b.vendor_id "
				+ " left join vnd_district c on a.vendor_id = c.vendor_id where 1=1 ";
		if (kantor != null && kantor != 0) {
			query = query.concat(" and c.district_id=" + kantor + " ");
		}

		if (klasifikasi != null && klasifikasi != 0) {
			query = query.concat(" and (a.vendor_fin_class =" + klasifikasi
					+ " or a.vendor_fin_class1 = " + klasifikasi
					+ " or a.vendor_fin_class2=" + klasifikasi + ") ");
		}

		if (SessionGetter.isNotNull(komoditi)) {
			query = query.concat(" and trim(b.group_code)='" + komoditi.trim()
					+ "' ");
		}

		if (SessionGetter.isNotNull(namaVendor)) {
			query = query.concat(" and lower(a.vendor_name) like '%"
					+ namaVendor + "%' ");
		}
		query = query.concat(" order by a.vendor_id asc");
		return query;
	}

	/**
	 * 
	 * @param vendorId
	 * @param tipe
	 *            , 1=diundang 2, mendaftar, 3 lulus adm, 4 lulus teknis, 5
	 *            lulus harga, 6 menang
	 * @return
	 */
	public static String bidderDetail(Integer vendorId, Integer tipe) {
		String query = "";
		switch (tipe) {
		case 1:
			query = " select distinct b.ppm_id, b.ppm_nomor_rfq, b.ppm_subject, b.ppm_jumlah_oe, (select avg(c.contract_amount) from ctr_contract_header c where c.ppm_id=a.ppm_id and a.vendor_id = c.vendor_id) as realisasi from prc_main_vendor a inner join prc_main_header b on a.ppm_id = b.ppm_id "
					+ " where a.pmp_status>=2 and a.vendor_id=" + vendorId;
			break;
		case 2:
			query = " select distinct b.ppm_id, b.ppm_nomor_rfq, b.ppm_subject, b.ppm_jumlah_oe, (select avg(c.contract_amount) from ctr_contract_header c where c.ppm_id=a.ppm_id and a.vendor_id = c.vendor_id) as realisasi from prc_main_vendor a inner join prc_main_header b on a.ppm_id = b.ppm_id "
					+ " where a.pmp_status>=3 and a.vendor_id=" + vendorId;
			break;
		case 3:
			query = " select distinct b.ppm_id, b.ppm_nomor_rfq, b.ppm_subject, b.ppm_jumlah_oe, (select avg(c.contract_amount) from ctr_contract_header c where c.ppm_id=a.ppm_id and a.vendor_id = c.vendor_id) as realisasi from prc_main_vendor a inner join prc_main_header b on a.ppm_id = b.ppm_id "
					+ " where a.pmp_status>=5 and a.vendor_id=" + vendorId;
			break;
		case 4:
			query = " select distinct b.ppm_id, b.ppm_nomor_rfq, b.ppm_subject, b.ppm_jumlah_oe, (select avg(c.contract_amount) from ctr_contract_header c where c.ppm_id=a.ppm_id and a.vendor_id = c.vendor_id) as realisasi from prc_main_vendor a inner join prc_main_header b on a.ppm_id = b.ppm_id "
					+ " where a.pmp_status>=6 and a.vendor_id=" + vendorId;
			break;
		case 5:
			query = " select distinct b.ppm_id, b.ppm_nomor_rfq, b.ppm_subject, b.ppm_jumlah_oe, (select avg(c.contract_amount) from ctr_contract_header c where c.ppm_id=a.ppm_id and a.vendor_id = c.vendor_id) as realisasi from prc_main_vendor a inner join prc_main_header b on a.ppm_id = b.ppm_id "
					+ " where a.pmp_status>=7 and a.vendor_id=" + vendorId;
			break;
		case 6:
			query = " select distinct b.ppm_id, b.ppm_nomor_rfq , b.ppm_subject, b.ppm_jumlah_oe, (select avg(c.contract_amount) from ctr_contract_header c where c.ppm_id=a.ppm_id and a.pemenang = c.vendor_id) as realisasi from prc_main_item a inner join prc_main_header b on a.ppm_id = b.ppm_id "
					+ " where a.pemenang=" + vendorId;
			break;
		default:
			break;
		}
		return query;
	}

	/**
	 * Expired Document
	 * 
	 * @param vendorId
	 * @return
	 */
	public static String expiredDokumen(Integer vendorId) {
		String query = "select "
				+ " a.vendor_id, "
				+ " a.vendor_name, "
				+ " (case when trunc(a.vendor_siup_to) < trunc(sysdate) then '1' else '0' end) as statsiup, "
				+ " (case when trunc(a.vendor_tdp_to) < trunc(sysdate) then '1' else '0' end) as stattdp, "
				+ " (select count(*) from (select  vendor_id,dom_tgl_kadaluarsa from tmp_vnd_domisili where trunc(dom_tgl_kadaluarsa) < trunc(sysdate)) where vendor_id=a.vendor_id) as domisili, "
				+ " (select count(*) from (select  vendor_id,vnd_ijin_end from tmp_vnd_ijin where trunc(vnd_ijin_end) < trunc(sysdate)) where vendor_id=a.vendor_id) as ijin, "
				+ " (select count(*) from (select  vendor_id,cert_to from tmp_vnd_cert where trunc(cert_to) < trunc(sysdate)) where vendor_id=a.vendor_id) as cert, "
				+ " a.vendor_email,"
				+ " a.vendor_status "
				+ " from "
				+ " tmp_vnd_header a inner join vnd_header b on a.vendor_id = b.vendor_id ";
		if (vendorId != null) {
			query = query.concat(" where a.vendor_id=" + vendorId);
		}
		return query;
	}

	public static String statusVendor(Integer vendorId, String statusVendor,
			String table) {
		String query = "update " + table + " set vendor_status='"
				+ statusVendor + "' where vendor_id=" + vendorId;
		return query;
	}

	public static String queryKpi() {
		String query = " select distinct "
				+ " a.ppm_id, "
				+ " c.ppm_estimator, "
				+ " c.ppm_buyer, "
				+ " c.ppm_nomor_pr, "
				+ " a.proses, "
				+ " b.proses_name, "
				+ " (select (max(trunc(created_date))-min(trunc(created_date))) from hist_main_comment where  ppm_id = a.ppm_id and proses = a.proses) as jml, "
				+ " (select complete_name from adm_user where id = c.ppm_estimator) as estimator, "
				+ " (select complete_name from adm_user where id = c.ppm_buyer) as pembeli, "
				+ " to_number(to_char(c.created_date,'yyyy')) as tahun "
				+ " from hist_main_comment a left join adm_proses b on proses = b.id "
				+ " left join prc_main_header c on a.ppm_id = c.ppm_id "
				+ " where 1=1 ";
		return query;
	}

	public static String queryProsesPengadaan(Object obj) {
		String query = "select * from ( "
				+ " select "
				+ " a.id, "
				+ " a.proses_name, "
				+ " (select count(*) from prc_main_header b where b.ppm_proses=a.id and b.ppm_proses_level <>-99 and b.ppm_proses_level<>-98 and b.ppm_proses_level<>99) as jumlah,"
				+ " 0 as tingkat "
				+ " from adm_proses a "
				+ " where a.id<>100 and a.id<>106 and a.id<>108 and a.id<>105"
				+ " union "
				+ " select "
				+ " 108 as id, "
				+ " 'Selesai / Kontrak' as proses_name,"
				+ " (select count(*) from prc_main_header b where b.ppm_proses_level=99 ) as jumlah, "
				+ " 0 as tingkat "
				+ " from dual "
				+ " union "
				+ " select "
				+ " 105 as id, "
				+ " 'Undang Vendor' as proses_name,"
				+ " (select count(*) from prc_main_header b where b.ppm_proses=105 and ppm_proses_level=1 ) as jumlah, "
				+ " 1 as tingkat "
				+ " from dual "
				+ " union "
				+ " select "
				+ " 105 as id, "
				+ " 'Aanwijzing' as proses_name,"
				+ " (select count(*) from prc_main_header b where b.ppm_proses=105 and ppm_proses_level=2 ) as Aanwijzing, "
				+ " 2 as tingkat "
				+ " from dual "
				+ " union "
				+ " select "
				+ " 105 as id, "
				+ " 'Pembukaan Penawaran' as proses_name,"
				+ " (select count(*) from prc_main_header b where b.ppm_proses=105 and ppm_proses_level=3 ) as Pembukaan_Penawaran, "
				+ " 3 as tingkat "
				+ " from dual "
				+ " union "
				+ " select "
				+ " 105 as id, "
				+ " 'Evaluasi Penawaran' as proses_name,"
				+ " (select count(*) from prc_main_header b where b.ppm_proses=105 and ppm_proses_level=4 ) as Evaluasi_Penawaran, "
				+ " 4 as tingkat "
				+ " from dual "
				+ " union "
				+ " select "
				+ " 105 as id, "
				+ " 'Negosiasi' as proses_name,"
				+ " (select count(*) from prc_main_header b where b.ppm_proses=105 and ppm_proses_level=5 ) as Negosiasi, "
				+ " 5 as tingkat " + " from dual "
				+ " ) x order by x.id asc, x.tingkat asc";
		return query;
	}

	public static String queryDataSheetPengadaan() {
		String query = "select distinct "
				+ " a.ppm_nomor_pr, "
				+ " upper(a.ppm_subject), "
				+ " upper(a.ppm_description), "
				+ " (select to_char(min(created_date),'dd/MM/yyyy') from hist_main_comment where ppm_id = a.ppm_id and proses_aksi='Pembuatan OE') as tgl_oe, "
				+ " (select to_char(min(created_date),'dd/MM/yyyy') from hist_main_comment where ppm_id = a.ppm_id and proses_aksi='Persiapan Pengadaan') as tgl_rks,"
				+ " (select to_char(min(created_date),'dd/MM/yyyy') from hist_main_comment where ppm_id = a.ppm_id and proses_aksi='Pemilihan Mitra Kerja') as tgl_mitra, "
				+ " (select to_char(min(created_date),'dd/MM/yyyy') from hist_main_comment where ppm_id = a.ppm_id and proses_aksi='Bid Opening') as tgl_buka_spph, "
				+ " (select to_char(min(created_date),'dd/MM/yyyy') from hist_main_comment where ppm_id = a.ppm_id and proses_aksi='Persetujuan Anggaran') as tgl_konfirmasi_anggaran, "
				+ " (select to_char(min(created_date),'dd/MM/yyyy') from hist_main_comment where ppm_id = a.ppm_id and proses_aksi='Negosiasi') as tgl_negosiasi, "
				+ " (select to_char(min(created_date),'dd/MM/yyyy') from hist_main_comment where ppm_id = a.ppm_id and proses_aksi='Evaluasi Penawaran') as tgl_evaluasi "
				+ " from " + " prc_main_header a where 1=1 ";
		return query;
	}

	/*public static String rekapPembelian() {
		String query = "select "
				+ " b.ppm_id, "
				+ " b.ppm_nomor_pr, "
				+ " (select to_char(min(created_date),'dd/MM/yyyy') from hist_main_comment where ppm_id = a.ppm_id and proses_aksi='Pemilihan Estimator') as tgl_oe, "
				+ " a.item_quantity, "
				+ " c.uom_name, "
				+ " a.item_description, "
				+ " decode(a.item_price_oe,null,a.item_price,a.item_price_oe) as harga, "
				+ " decode(a.item_total_price_oe,null,a.item_total_price,a.item_total_price_oe) as harga_total, "
				+ " d.contract_number as nomorPO, "
				+ " to_char(d.contract_start,'dd/MM/yyyy') as tanggalMulaiKontrak, "
				+ " to_char(d.contract_end,'dd/MM/yyyy') as tanggalMulaiBerakhir, "
				+ " upper(e.vendor_name) as mitraKerja, "
				+ " to_char(d.tgl_penyerahan,'dd/MM/yyyy') as tanggalPenyerahan "
				+ " from "
				+ " prc_main_item a left join prc_main_header b on a.ppm_id = b.ppm_id left join adm_uom c on a.item_uom = c.uom_id "
				+ " left join ctr_contract_header d on a.ppm_id = d.ppm_id left join vnd_header e on d.vendor_id = e.vendor_id where 1=1 ";
		return query;
	}*/
	
	public static String rekapPembelian() {
		String query = "select * from mtz_vw_rekap_pembelian";
		return query;
	}

	public static String expiredKontrakPenyerahan(Object object) {
		String query = "select a.contract_number, a.contract_subject, a.contract_description, to_char(a.tgl_penyerahan,'dd/MM/yyyy') as penyerahan, b.vendor_name, b.vendor_email "
				+ "from ctr_contract_header a inner join vnd_header b on a.vendor_id=b.vendor_id "
				+ "where trunc(a.tgl_penyerahan)-trunc(sysdate) <10 and trunc(a.tgl_penyerahan)-trunc(sysdate) >0";
		return query;
	}
	
	public static String vndCheckNpwp(String npwp){
		String query = "select * from ( " + 
				"SELECT REGEXP_REPLACE(vendor_npwp_no, '[^0-9A-Za-z]', '') as npwp, vendor_id FROM tmp_vnd_header ) npwp_vendor " + 
				"where npwp like '%"+npwp+"%'";
		return query;
	}
}
