package com.eproc.utils;

import java.io.InputStream;
import java.sql.Blob;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.BlobType;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.eproc.sap.controller.IntegrationService;
import com.eproc.sap.controller.IntegrationServiceImpl;
import com.eproc.x.rfq.command.ContractPenyerahanExpired;
import com.eproc.x.verification.command.VendorExpiredModel;
import com.orm.model.AdmEmployee;
import com.orm.model.AdmMailConfig;
import com.orm.model.AdmRole;
import com.orm.model.OrDetail;
import com.orm.model.OrDetailId;
import com.orm.model.OrHeader;
import com.orm.model.OrHeaderId;
import com.orm.tools.GenericDao;
import com.orm.tools.HibernateUtil;

public class SchedulerOrBatch extends GenericDao implements Job{
	IntegrationService integrationService = new IntegrationServiceImpl();
	/**
	 * here all jobs will be execute
	 */
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		try {
			/*prosesBatchHeaderOr();
			prosesBatchHeaderZr();
			notifikasiVendor();
			notifikasiPengirimanBarangVendor();*/
			//integrationService.pushVendorAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*public void prosesBatchHeaderZr() throws Exception{
		Session ses = HibernateUtil.getSessionJde();
		startTransactionJDE();
		try {
			Query q = ses.createSQLQuery(QueryUtil.headerZr(null));
			List<Object[]> lst = q.list();
			boolean flag = true;
			if(lst!=null && lst.size()>0){
				System.out.println("Execute ZR <<<<<<");
				Iterator<Object[]> it = lst.iterator();
				while(it.hasNext()){
					
					Object o[] = it.next();
					it.remove();
					OrHeaderId id = new OrHeaderId();
					id.setNomorOr(Long.valueOf(String.valueOf(o[0])));
					id.setHeaderSite(String.valueOf(o[1]));
					id.setOrH(String.valueOf(o[2]));
					if(!existOrHeader(id)){
						OrHeader orh = new OrHeader();
						orh.setId(id);
						orh.setNextStatus(String.valueOf(o[3]));
						orh.setLastStatus(String.valueOf(o[4]));
						orh.setPembuatOm(String.valueOf(o[5]));
						orh.setNamaPembuatOm(String.valueOf(o[6]));
						orh.setNomorOm(String.valueOf(o[7]));
						orh.setOmOn(String.valueOf(o[8]));
						orh.setSite(String.valueOf(o[9]));
						orh.setUnitKerja(String.valueOf(o[10]));
						orh.setNomorWo(String.valueOf(o[11]));
						orh.setTglOr(JulianToGregorianViceVersa.getDateFromJulian7(String.valueOf(o[12]).trim()));
						orh.setMataUang(SessionGetter.nullObjectToEmptyString(o[13]));
						orh.setKodeSatker(SessionGetter.nullObjectToEmptyString(o[14]));
						orh.setPembuatOrJasa(SessionGetter.nullObjectToEmptyString(o[15]));
						//get attchment
						orh.setAttachment(getAttchmentOr(orh,ses));
						orh.setFileName(getContentName(orh,ses));
						String username = SessionGetter.valueProperty("ftp.jde.user");
						String password = SessionGetter.valueProperty("ftp.jde.password");
						String server = SessionGetter.valueProperty("ftp.jde.server");
						String remoteDirectory = SessionGetter.valueProperty("ftp.jde.dir");
						InputStream is = FtpUtilEproc.getFtpFile(username, password, server, null, remoteDirectory, orh.getFileName());
						if(is!=null){
							UploadUtil.uploadDocsFileOriginal(FtpUtilEproc.stream2file(is), orh.getFileName());
						}else{
							orh.setFileName("");
						}
						
						orh.setStatus(0);
						orh.setCreatedDate(new Date());
						orh.setKodeGenerate(new SimpleDateFormat("ddMMyyyy").format(new Date()));
						session.merge(orh);
						
						//save Detail
						flag = getAndSaveDetailZR(id,session,ses);
						if(!flag){
							orh.setStatus(1);//dibatalkan
							session.merge(orh);
						}
						
					}
					
				}
			}
			
			ses.close();
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			e.printStackTrace();
			throw e;
		}
		
	}

	@SuppressWarnings({"unchecked" })
	private void notifikasiVendor() throws Exception {
		try {
			startTransactionJDE();
			//mail config
			DetachedCriteria cr = DetachedCriteria.forClass(AdmMailConfig.class);
			cr.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			AdmMailConfig config = (AdmMailConfig) cr.getExecutableCriteria(session).uniqueResult();
			
			//user vendor spesialis
			DetachedCriteria cr2 = DetachedCriteria.forClass(AdmRole.class);
			cr2.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			cr2.add(Restrictions.ilike("roleName", "vendor spesialis"));
			List<AdmRole> roles = cr2.getExecutableCriteria(session).list();
			
			DetachedCriteria cr3 = DetachedCriteria.forClass(AdmEmployee.class);
			cr3.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			cr3.add(Restrictions.or(Restrictions.or(Restrictions.in("admRoleByEmpRole1", roles), Restrictions.in("admRoleByEmpRole2", roles)), Restrictions.or(Restrictions.in("admRoleByEmpRole3", roles), Restrictions.in("admRoleByEmpRole4", roles))));
			List<AdmEmployee> lsemp = cr3.getExecutableCriteria(session).list();
			
			Query q = session.createSQLQuery(QueryUtil.expiredDokumen(null));
			List<Object[]> lst = q.list();
			if(lst!=null && lst.size()>0){
				Iterator<Object[]> it = lst.iterator();
				while(it.hasNext()){
					Object o[] = it.next();
					it.remove();
					List<String> listData = new ArrayList<String>();
					VendorExpiredModel m =  new VendorExpiredModel();
					m.setVendorId(Integer.valueOf(String.valueOf(o[0])));
					m.setVendorName(String.valueOf(o[1]));
					m.setSiup(Integer.valueOf(String.valueOf(o[2])));
					if(m.getSiup()!=0){
						listData.add("Siup");
					}
					m.setTdp(Integer.valueOf(String.valueOf(o[3])));
					if(m.getTdp()!=0){
						listData.add("Tdp");
					}
					m.setDomisili(Integer.valueOf(String.valueOf(o[4])));
					if(m.getDomisili()!=0){
						listData.add("Domisili");
					}
					m.setIjin(Integer.valueOf(String.valueOf(o[5])));
					if(m.getIjin()!=0){
						listData.add("Ijin");
					}
					m.setEmail(String.valueOf(o[7]));
					m.setStatus(String.valueOf(o[8]));
					
					if(listData!=null && listData.size()>0){
						String msg = "";
						msg = msg.concat("Dengan Hormat, <br/>");
						msg = msg.concat("Dokumen Perusahaan <b>"+m.getVendorName().toUpperCase()+" </b>, Sudah atau akan Expired. Dokumen tersebut adalah : <br/>");
						for(String s:listData){
							msg = msg.concat("<table>");
								msg = msg.concat("<tr>");
									msg = msg.concat("<td>");
										msg = msg.concat(s);
									msg = msg.concat("</td>");
								msg = msg.concat("</tr>");
							msg = msg.concat("</table>");
						}
						msg = msg.concat("-----------------<br/><br/><br/>");
						msg = msg.concat("eProcurement PT Semen Baturaja (Persero)");
						EmailSessionBean.sendMail("Dokumen Expired", msg, m.getEmail(),config);
						
						if(lsemp!=null && lsemp.size()>0){
							for(AdmEmployee e:lsemp){
								EmailSessionBean.sendMail("Dokumen Expired", msg, e.getEmpEmail(),config);
							}
						}
					}
					
				}
			}
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
		
	}
	
	private void notifikasiPengirimanBarangVendor() throws Exception {
		try {
			startTransactionJDE();
			//mail config
			DetachedCriteria cr = DetachedCriteria.forClass(AdmMailConfig.class);
			cr.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			AdmMailConfig config = (AdmMailConfig) cr.getExecutableCriteria(session).uniqueResult();

			Query q = session.createSQLQuery(QueryUtil.expiredKontrakPenyerahan(null));
			List<Object[]> lst = q.list();
			if(lst!=null && lst.size()>0){
				Iterator<Object[]> it = lst.iterator();
				while(it.hasNext()){
					Object o[] = it.next();
					it.remove();
					ContractPenyerahanExpired exp = new ContractPenyerahanExpired();
					exp.setNomorKontrak(SessionGetter.nullObjectToEmptyString(o[0]));
					exp.setJudulKontrak(SessionGetter.nullObjectToEmptyString(o[1]));
					exp.setDeskripsiKontrak(SessionGetter.nullObjectToEmptyString(o[2]));
					exp.setTanggalPenyerahan(SessionGetter.nullObjectToEmptyString(o[3]));
					exp.setMitraKerja(SessionGetter.nullObjectToEmptyString(o[4]));
					exp.setEmail(SessionGetter.nullObjectToEmptyString(o[5]));
						String msg = "";
						msg = msg.concat("Dengan Hormat, <br/>");
						msg = msg.concat("Perusahaan <b>"+exp.getMitraKerja().toUpperCase()+" </b>, Memiliki Pekerjaan yang batas Penyerahannnya akan Berakhir, yaitu : <br/>");
							msg = msg.concat("<table>");
								msg = msg.concat("<tr>");
									msg = msg.concat("<td>");
										msg = msg.concat("Nomor Kontrak / OP");
									msg = msg.concat("</td>");
								msg = msg.concat("</tr>");
								msg = msg.concat("<tr>");
									msg = msg.concat("<td>");
										msg = msg.concat(exp.getNomorKontrak());
									msg = msg.concat("</td>");
								msg = msg.concat("</tr>");
								
								msg = msg.concat("<tr>");
									msg = msg.concat("<td>");
										msg = msg.concat("Deskripsi");
									msg = msg.concat("</td>");
								msg = msg.concat("</tr>");
								msg = msg.concat("<tr>");
									msg = msg.concat("<td>");
										msg = msg.concat(exp.getDeskripsiKontrak());
									msg = msg.concat("</td>");
								msg = msg.concat("</tr>");
								
								msg = msg.concat("<tr>");
									msg = msg.concat("<td>");
										msg = msg.concat("Tanggal Akhir Penyerahan");
									msg = msg.concat("</td>");
								msg = msg.concat("</tr>");
								msg = msg.concat("<tr>");
									msg = msg.concat("<td>");
										msg = msg.concat(exp.getTanggalPenyerahan());
									msg = msg.concat("</td>");
								msg = msg.concat("</tr>");
							msg = msg.concat("</table>");
						msg = msg.concat("<br/><br/>");
						msg = msg.concat("============================");
						msg = msg.concat("Abaikan Email Ini jika, Barang sudah di kirim / di deliver");
						msg = msg.concat("-----------------<br/><br/><br/>");
						msg = msg.concat("eProcurement PT Semen Baturaja (Persero)");
						EmailSessionBean.sendMail("Kontrak Expired", msg, exp.getEmail(),config);
				}
			}
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			throw e;
		}
		
	}

	*//**
	 * get OR HEADER
	 * nextstatus = 130
	 * last status = 125
	 * @throws Exception
	 *//*
	@SuppressWarnings("unchecked")
	public void prosesBatchHeaderOr() throws Exception{
		Session ses = HibernateUtil.getSessionJde();
		startTransactionJDE();
		try {
			Query q = ses.createSQLQuery(QueryUtil.headerOr(null));
			List<Object[]> lst = q.list();
			if(lst!=null && lst.size()>0){
				System.out.println(">> 1 <<<<<<<<<<<<<<<<<");
				Iterator<Object[]> it = lst.iterator();
				while(it.hasNext()){
					
					Object o[] = it.next();
					it.remove();
					OrHeaderId id = new OrHeaderId();
					id.setNomorOr(Long.valueOf(String.valueOf(o[0])));
					id.setHeaderSite(String.valueOf(o[1]));
					id.setOrH(String.valueOf(o[2]));
					if(!existOrHeader(id)){
						OrHeader orh = new OrHeader();
						orh.setId(id);
						orh.setNextStatus(String.valueOf(o[3]));
						orh.setLastStatus(String.valueOf(o[4]));
						orh.setPembuatOm(String.valueOf(o[5]));
						orh.setNamaPembuatOm(String.valueOf(o[6]));
						orh.setNomorOm(String.valueOf(o[7]));
						orh.setOmOn(String.valueOf(o[8]));
						orh.setSite(String.valueOf(o[9]));
						orh.setUnitKerja(String.valueOf(o[10]));
						orh.setNomorWo(String.valueOf(o[11]));
						orh.setTglOr(JulianToGregorianViceVersa.getDateFromJulian7(String.valueOf(o[12]).trim()));
						orh.setMataUang(SessionGetter.nullObjectToEmptyString(o[13]));
						orh.setKodeSatker(SessionGetter.nullObjectToEmptyString(o[14]));
						orh.setPembuatOrJasa(SessionGetter.nullObjectToEmptyString(o[15]));
						//get attchment
						orh.setAttachment(getAttchmentOr(orh,ses));
						orh.setFileName(getContentName(orh,ses));
						String username = SessionGetter.valueProperty("ftp.jde.user");
						String password = SessionGetter.valueProperty("ftp.jde.password");
						String server = SessionGetter.valueProperty("ftp.jde.server");
						String remoteDirectory = SessionGetter.valueProperty("ftp.jde.dir");
						InputStream is = FtpUtilEproc.getFtpFile(username, password, server, null, remoteDirectory, orh.getFileName());
						if(is!=null){
							UploadUtil.uploadDocsFileOriginal(FtpUtilEproc.stream2file(is), orh.getFileName());
						}else{
							orh.setFileName("");
						}
						
						orh.setStatus(0);
						orh.setCreatedDate(new Date());
						orh.setKodeGenerate(new SimpleDateFormat("ddMMyyyy").format(new Date()));
						session.merge(orh);
						
						//save Detail
						getAndSaveDetailOr(id,session,ses);
					}
					
				}
			}
			
			Query q2 = ses.createSQLQuery(QueryUtil.headerOrConcat(null));
			List<Object[]> lst2 = q2.list();
			if(lst2!=null && lst2.size()>0){
				System.out.println(">> 2 <<<<<<<<<<<<<<<<<");
				Iterator<Object[]> it = lst2.iterator();
				while(it.hasNext()){
					Object o[] = it.next();
					it.remove();
					OrHeaderId id = new OrHeaderId();
					id.setNomorOr(Long.valueOf(String.valueOf(o[0])));
					id.setHeaderSite(String.valueOf(o[1]));
					id.setOrH(String.valueOf(o[2]));
					if(!existOrHeader(id)){
						OrHeader orh = new OrHeader();
						orh.setId(id);
						orh.setNextStatus(String.valueOf(o[3]));
						orh.setLastStatus(String.valueOf(o[4]));
						orh.setPembuatOm(String.valueOf(o[5]));
						orh.setNamaPembuatOm(String.valueOf(o[6]));
						orh.setNomorOm(String.valueOf(o[7]));
						orh.setOmOn(String.valueOf(o[8]));
						orh.setSite(String.valueOf(o[9]));
						orh.setUnitKerja(String.valueOf(o[10]));
						orh.setNomorWo(String.valueOf(o[11]));
						orh.setTglOr(JulianToGregorianViceVersa.getDateFromJulian7(String.valueOf(o[12]).trim()));
						orh.setMataUang(SessionGetter.nullObjectToEmptyString(o[13]));
						orh.setKodeSatker(SessionGetter.nullObjectToEmptyString(o[14]));
						
						//get attchment
						orh.setAttachment(getAttchmentOr(orh,ses));
						orh.setFileName(getContentName(orh,ses));
						String username = SessionGetter.valueProperty("ftp.jde.user");
						String password = SessionGetter.valueProperty("ftp.jde.password");
						String server = SessionGetter.valueProperty("ftp.jde.server");
						String remoteDirectory = SessionGetter.valueProperty("ftp.jde.dir");
						InputStream is = FtpUtilEproc.getFtpFile(username, password, server, null, remoteDirectory, orh.getFileName());
						if(is!=null){
							UploadUtil.uploadDocsFileOriginal(FtpUtilEproc.stream2file(is), orh.getFileName());
						}else{
							orh.setFileName("");
						}
						orh.setStatus(0);
						orh.setCreatedDate(new Date());
						orh.setKodeGenerate(new SimpleDateFormat("ddMMyyyy").format(new Date()));
						session.merge(orh);
						
						//save Detail
						getAndSaveDetailOr(id,session,ses);
					}
				}
			}
			ses.close();
			commitAndClear();
		} catch (Exception e) {
			RollbackAndClear();
			e.printStackTrace();
			throw e;
		}
		
	}

	private String getContentName(OrHeader orh, Session ses) {
		Query q = ses.createSQLQuery(QueryUtil.getContenerHeadrName(orh));
		String retMe = null;
		@SuppressWarnings("unchecked")
		List<Object> lst = q.list();
		if(lst!=null && lst.size()>0){
			retMe = String.valueOf(lst.get(0)).trim();
			if(retMe.lastIndexOf("/")!=-1){
				retMe = retMe.substring(retMe.lastIndexOf("/")).replaceAll("/", "");
			}
			
		}
		return retMe;
	}

	@SuppressWarnings("unused")
	private Blob getContentData(OrHeader orh, Session ses) {
		try {
			SQLQuery q = ses.createSQLQuery(QueryUtil.getContentHeaderData(orh));
			q.addScalar("attachment", BlobType.INSTANCE);
			@SuppressWarnings("unchecked")
			List<Object> lst = q.list();
			if(lst!=null && lst.size()>0){
				return (Blob) lst.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private boolean existOrHeader(OrHeaderId id){
		DetachedCriteria crit = DetachedCriteria.forClass(OrHeader.class);
		crit.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
		crit.add(Restrictions.idEq(id));
		OrHeader oh = (OrHeader) getEntityByDetachedCiteria(crit);
		if(oh==null){
			System.out.println(">>>> NOT EXIST");
			return false;
		}else{
			System.out.println(">>>>> EXIST");
			return true;
		}
	}

	private String getAttchmentOr(OrHeader orh,Session sesjde) throws Exception{
		Query q = sesjde.createSQLQuery(QueryUtil.getAttachmentOr(orh));
		@SuppressWarnings("unchecked")
		List<Object> lst = q.list();
		if(lst!=null && lst.size()>0){
			Object obj = lst.get(0);
			return ConvertRtfToText.to_text(String.valueOf(obj));
		}
		return "";
	}
	
	private String getAttchmentOrDetail(OrDetail orh,Session sesjde) throws Exception{
		Query q = sesjde.createSQLQuery(QueryUtil.getAttachmentOrDetail(orh));
		@SuppressWarnings("unchecked")
		List<Object> lst = q.list();
		if(lst!=null && lst.size()>0){
			Object obj = lst.get(0);
			return ConvertRtfToText.to_text(String.valueOf(obj));
		}
		return "";
	}

	@SuppressWarnings("unchecked")
	private void getAndSaveDetailOr(OrHeaderId id, Session session, Session sesjde) throws Exception{
		try {
			Query q = sesjde.createSQLQuery(QueryUtil.detailOr(id));
			List<Object[]> lst = q.list();
			if(lst!=null && lst.size()>0){
				Iterator<Object[]> it = lst.iterator();
				while(it.hasNext()){
					Object o[] = it.next();
					it.remove();
					OrDetailId idi = new OrDetailId();
					idi.setNomorOr(Long.valueOf(String.valueOf(o[0])));
					idi.setHeaderSite(String.valueOf(o[1]));
					idi.setOrH(String.valueOf(o[2]));
					idi.setLineId(Integer.valueOf(String.valueOf(o[3])));
					
					OrDetail ord = new OrDetail(idi);
					ord.setLine(idi.getLineId());
					ord.setKodeBarang(String.valueOf(o[4]));
					ord.setNamaBarang(String.valueOf(o[5]));
					ord.setNamaBarangExt(String.valueOf(o[6]));
					ord.setJumlah(Double.valueOf(String.valueOf(o[7])));
					ord.setSatuan(String.valueOf(String.valueOf(o[8])));
					ord.setHargaSatuan((Double.valueOf(o[9]!=null?String.valueOf(o[9]):"0")/10000));
					ord.setLineType(String.valueOf(o[10]));
					ord.setAccuountNumber(String.valueOf(o[11]));
					ord.setOrHeader(new OrHeader(id));
					ord.setAttachment(getAttchmentOrDetail(ord, sesjde));
					ord.setFileName(getCOntentDetailName(ord,sesjde));
					session.merge(ord);

					String username = SessionGetter.valueProperty("ftp.jde.user");
					String password = SessionGetter.valueProperty("ftp.jde.password");
					String server = SessionGetter.valueProperty("ftp.jde.server");
					String remoteDirectory = SessionGetter.valueProperty("ftp.jde.dir");
					InputStream is = FtpUtilEproc.getFtpFile(username, password, server, null, remoteDirectory, ord.getFileName());
					if(is!=null){
						System.out.println(">>> NOT NULL INPUT STREAM");
						UploadUtil.uploadDocsFileOriginal(FtpUtilEproc.stream2file(is), ord.getFileName());
					}else{
						System.out.println(">>> NULL INPUT STREAM");
						ord.setFileName("");
					}
					session.merge(ord);
					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@SuppressWarnings("unchecked")
	private boolean getAndSaveDetailZR(OrHeaderId id, Session session, Session sesjde) throws Exception{
		try {
			Query q = sesjde.createSQLQuery(QueryUtil.detailOr(id));
			List<Object[]> lst = q.list();
			Double threshold = Double.valueOf("250000000");//250 juta
			Double currentData=Double.valueOf("0");
			
			if(lst!=null && lst.size()>0){
				Iterator<Object[]> it = lst.iterator();
				while(it.hasNext()){
					Object o[] = it.next();
					it.remove();
					OrDetailId idi = new OrDetailId();
					idi.setNomorOr(Long.valueOf(String.valueOf(o[0])));
					idi.setHeaderSite(String.valueOf(o[1]));
					idi.setOrH(String.valueOf(o[2]));
					idi.setLineId(Integer.valueOf(String.valueOf(o[3])));
					
					OrDetail ord = new OrDetail(idi);
					ord.setLine(idi.getLineId());
					ord.setKodeBarang(String.valueOf(o[4]));
					ord.setNamaBarang(String.valueOf(o[5]));
					ord.setNamaBarangExt(String.valueOf(o[6]));
					ord.setJumlah(Double.valueOf(String.valueOf(o[7])));
					ord.setSatuan(String.valueOf(String.valueOf(o[8])));
					ord.setHargaSatuan((Double.valueOf(o[9]!=null?String.valueOf(o[9]):"0")/10000));
					ord.setLineType(String.valueOf(o[10]));
					ord.setAccuountNumber(String.valueOf(o[11]));
					ord.setOrHeader(new OrHeader(id));
					ord.setAttachment(getAttchmentOrDetail(ord, sesjde));
					ord.setFileName(getCOntentDetailName(ord,sesjde));
					session.merge(ord);
					
					currentData = currentData + (ord.getHargaSatuan() * ord.getJumlah());

					String username = SessionGetter.valueProperty("ftp.jde.user");
					String password = SessionGetter.valueProperty("ftp.jde.password");
					String server = SessionGetter.valueProperty("ftp.jde.server");
					String remoteDirectory = SessionGetter.valueProperty("ftp.jde.dir");
					InputStream is = FtpUtilEproc.getFtpFile(username, password, server, null, remoteDirectory, ord.getFileName());
					if(is!=null){
						System.out.println(">>> NOT NULL INPUT STREAM");
						UploadUtil.uploadDocsFileOriginal(FtpUtilEproc.stream2file(is), ord.getFileName());
					}else{
						System.out.println(">>> NULL INPUT STREAM");
						ord.setFileName("");
					}
					session.merge(ord);
					
				}
			}
			
			if(currentData < threshold){
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return true;
	}

	private String getCOntentDetailName(OrDetail ord, Session sesjde) {
		String retMe="";
		Query q = sesjde.createSQLQuery(QueryUtil.getContentDetailName(ord));
		@SuppressWarnings("unchecked")
		List<Object> lst = q.list();
		if(lst!=null && lst.size()>0){
			retMe = String.valueOf(lst.get(0)).trim();
			if(retMe.lastIndexOf("/")!=-1){
				retMe = retMe.substring(retMe.lastIndexOf("/")).replaceAll("/", "");
			}
		}
		return retMe;
	}

	@SuppressWarnings("unused")
	private Blob getCOntentDetail(OrDetail ord, Session sesjde) {
		try {
			SQLQuery q = sesjde.createSQLQuery(QueryUtil.getContentDetailData(ord));
			q.addScalar("attachment", BlobType.INSTANCE);
			@SuppressWarnings("unchecked")
			List<Object> lst = q.list();
			if(lst!=null && lst.size()>0){
				System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>> BLOB NOT NULL");
				System.out.println( ((Blob) lst.get(0)).length()+"<><><><>< LENG TH BLOB");
				return (Blob) lst.get(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}*/

}
