package com.eproc.utils;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 
 * @author kusumah
 *Kelas converter julin date to gregorian date vice versa
 */
public class JulianToGregorianViceVersa {
	
	/**
	 * format back julian to gregorian
	 * @param julianDate
	 * @return
	 * @throws ParseException
	 */
	public static Date getDateFromJulian7(String julianDate)
			throws ParseException {
		System.out.println(julianDate.trim().length()+"<><><><><><><LENGTH DATE");
		if(julianDate.trim()!=null && julianDate.trim().length()>0 && julianDate.length()>1 && julianDate.trim()!="")
			return new SimpleDateFormat("yyDDD").parse(julianDate.substring(1));
		else
			return new Date();
	}

	/**
	 * format julian from gregoria
	 * @param date
	 * @return
	 */
	public static String getJulian7FromDate(Date date) {
		StringBuilder sb = new StringBuilder();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		String tahun = "0";
		if (cal.get(Calendar.YEAR) >= 2000)
			tahun = "1";


		return sb.append(tahun)
				.append(String.valueOf(cal.get(Calendar.YEAR)).substring(2))
				.append(String.format("%03d", cal.get(Calendar.DAY_OF_YEAR)))
				.toString();
	}
	
	/**
	 * time from date
	 * @param date
	 * @return
	 */
	public static String timeString(Date date){
		StringBuilder sb = new StringBuilder();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		String format = new  SimpleDateFormat("HHmmss").format(cal.getTime());
		return sb.append(format).toString();
	}

	public static void main(String[] args) throws Exception {
		 SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String test = "2017-12-18 16:29:30";
		Date datee = formatter.parse(test);
		System.out.println(test.split("-").length);
		Date d = JulianToGregorianViceVersa.getDateFromJulian7(test);
		System.out.println(">>> TANGGAL ==" + d);
		System.out.println(">>>>>JULIAN "
				+ JulianToGregorianViceVersa.getJulian7FromDate(datee));
		System.out.println(JulianToGregorianViceVersa.timeString(datee));
		
		DecimalFormat df = new DecimalFormat("###.##");
		Double harga = 4000000.25;
		String dt = "12121";
		System.out.println(dt.trim().length());
		System.out.println(String.valueOf(df.format(harga)));
		
		String data = "/FILE-10-10-2-253-5136814468217884-1369017495625.jpg";
		System.out.println(">>>> "+data.lastIndexOf("/"));
		System.out.println(">>>> "+data.substring(data.lastIndexOf("/")).replaceAll("/", ""));
		
	}

}
