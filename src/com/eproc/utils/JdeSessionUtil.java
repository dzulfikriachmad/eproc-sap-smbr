package com.eproc.utils;

import com.jdedwards.system.connector.dynamic.Connector;
import com.jdedwards.system.connector.dynamic.UserSession;
import com.jdedwards.system.lib.JdeProperty;

public class JdeSessionUtil {

	protected String username = "";
	protected String password = "";
	protected String env = "";
	protected String role = "*ALL";
	
	/**
	 * login to JDE
	 * @param username
	 * @param password
	 * @param env
	 * @param role
	 * @return
	 * @throws Exception
	 */
	public static Integer loginJde(String username, String password, String env, String role) throws Exception{
		Integer sessionID = null;
		try {
			username = JdeProperty.getProperty("EPROC", "username", username);
			password = JdeProperty.getProperty("EPROC", "password",password);
			env = JdeProperty.getProperty("EPROC", "env", env);
			role = JdeProperty.getProperty("EPROC", "role", "*ALL");
			sessionID = Connector.getInstance().login(username, password, env, role);
			if (Connector.getInstance().isLoggedIn(sessionID))
			{
			     System.out.print("(Login Success) ="+sessionID);
			}else{
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		System.out.println(">>>> SESSION ID "+sessionID);
		return sessionID;
	}
	
	/**
	 * log off from JDE
	 * @param sessionID
	 * @throws Exception
	 */
	public static void loggofJDE(Integer sessionID) throws Exception{
		try {
			Connector.getInstance().logoff(sessionID);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
