package com.eproc.utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

import org.apache.commons.lang.RandomStringUtils;

public class RandomCode {
	 private static final char[] symbols = new char[36];

	  static {
		  for (int idx = 0; idx < 10; ++idx)
			  symbols[idx] = (char) ('0' + idx);
		  for (int idx = 10; idx < 36; ++idx)
			  symbols[idx] = (char) ('a' + idx - 10);
	  }

	  private final Random random = new Random();

	  private final char[] buf;
	  
	  public RandomCode(int length){
		  if (length < 1)
			  throw new IllegalArgumentException("length < 1: " + length);
		  buf = new char[length];
	  }
	  
	  public String getCode(){
		  for (int idx = 0; idx < buf.length; ++idx) 
			  buf[idx] = symbols[random.nextInt(symbols.length)];
		  return new String(buf);
	  }
	  
	  public static String stringToMd5(String t) {
			try {
			   MessageDigest m = MessageDigest.getInstance("MD5");
		       m.reset();
		       m.update(t.getBytes());
		       byte[] digest = m.digest();
		       
		       BigInteger bigInt = new BigInteger(1, digest);
		       String hastText = bigInt.toString(16);
		       
		       while(hastText.length() < 32 ){
		    	   hastText = "0"+hastText;
		       }
		       
		       return hastText; 
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}
	  
	  public static void main(String[] args) {
		System.out.println(RandomCode.stringToMd5("Sem3nB4tvRaj4"));
	}
}
