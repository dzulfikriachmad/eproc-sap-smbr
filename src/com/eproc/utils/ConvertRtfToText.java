package com.eproc.utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.text.DecimalFormat;

import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.rtf.RTFEditorKit;

public class ConvertRtfToText extends Object {

	public static void convertToText(java.lang.String p_in,
			java.lang.String[] p_out) throws IOException, BadLocationException {
		// test for null inputs to avoid java.lang.NullPointerException when
		// input is null
		if (p_in != null) {
			RTFEditorKit kit = new RTFEditorKit();
			Document doc = kit.createDefaultDocument();
			kit.read(new StringReader(p_in), doc, 0);
			p_out[0] = doc.getText(0, doc.getLength());
		} else
			p_out[0] = null;
	}

	public static String to_text(String p_in) throws IOException,
			BadLocationException {
		// test for null inputs to avoid java.lang.NullPointerException when
		// input is null
		if (p_in != null) {
			RTFEditorKit kit = new RTFEditorKit();
			Document doc = kit.createDefaultDocument();
			kit.read(new ByteArrayInputStream(p_in.getBytes()), doc, 0);
			return doc.getText(0, doc.getLength());
		} else
			return null;
	}
	
	public static void main(String[] args) {
		
	}
}
