package com.eproc.utils;


import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SchedulerListener implements ServletContextListener,JobListener {

private static Logger _log = LoggerFactory.getLogger(SchedulerListener.class);
	
	/**
	 * Method contextDestroyed.
	 * @param event ServletContextEvent
	 * @see javax.servlet.ServletContextListener#contextDestroyed(ServletContextEvent)
	 */
	@Override
	public void contextDestroyed(ServletContextEvent event) {
		//
	}

	/**
	 * Method contextInitialized.
	 * @param event ServletContextEvent
	 * @see javax.servlet.ServletContextListener#contextInitialized(ServletContextEvent)
	 */
	@Override
	public void contextInitialized(ServletContextEvent event) {
		
	}

	 /**
	  * Method getName.
	  * @return String
	  * @see org.quartz.JobListener#getName()
	  */
	 public String getName() {
	        return "job1_to_job2";
	    }

	    /**
	     * Method jobToBeExecuted.
	     * @param inContext JobExecutionContext
	     * @see org.quartz.JobListener#jobToBeExecuted(JobExecutionContext)
	     */
	    public void jobToBeExecuted(JobExecutionContext inContext) {
	        _log.info("Job1Listener says: Job Is about to be executed.");
	    }

	    /**
	     * Method jobExecutionVetoed.
	     * @param inContext JobExecutionContext
	     * @see org.quartz.JobListener#jobExecutionVetoed(JobExecutionContext)
	     */
	    public void jobExecutionVetoed(JobExecutionContext inContext) {
	        _log.info("Job1Listener says: Job Execution was vetoed.");
	    }

	    /**
	     * Method jobWasExecuted.
	     * @param inContext JobExecutionContext
	     * @param inException JobExecutionException
	     * @see org.quartz.JobListener#jobWasExecuted(JobExecutionContext, JobExecutionException)
	     */
	    public void jobWasExecuted(JobExecutionContext inContext,
	            JobExecutionException inException) {
	        _log.info("Job1Listener says: Job was executed.");
	        
	        // Simple job #2
//	        JobDetail job2 = newJob(SchedulerOrBatch.class)
//	            .withIdentity("S")
//	            .build();
//	        
//	        Trigger trigger = newTrigger() 
//	            .withIdentity("masterTrigger")
//	            .startNow()
//	            .build();
	        
//	        try {
//	            // schedule the job to run!
////	            inContext.getScheduler().scheduleJob(job2, trigger);
//	        } catch (SchedulerException e) {
//	            _log.warn("Unable to schedule Master!");
//	            e.printStackTrace();
//	        }
	        
	    }
}
