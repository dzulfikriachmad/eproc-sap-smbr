package com.eproc.sap.controller;

import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;

import com.eproc.sap.dto.PoHeaderDto;
import com.eproc.sap.dto.ResultDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.orm.model.CtrContractHeader;
import com.orm.model.CtrContractItem;
import com.orm.model.PrcMainHeader;
import com.orm.model.PrcMainItem;
import com.orm.model.TmpVndHeader;

public interface IntegrationService {
	//public ResultDto generateOR();
	public ResultDto pushPO(CtrContractHeader ctr, List<CtrContractItem> contractItemList) throws JsonProcessingException;
	public ResultDto pushVendor(TmpVndHeader vnd)throws JsonProcessingException;
	/*public ResponseEntity<String> generateToken();
	public ResultDto pushVendorOne(TmpVndHeader vnd) throws JsonProcessingException;*/
	public ResultDto changePr(List<PrcMainItem> listPrcMainItem,  PrcMainHeader prcMainHeader) throws Exception;
	Map<String, String> generateTokenAndCookies();
	//ResultDto pushVendorAll() throws JsonProcessingException;
	ResponseEntity<String> generateToken();
	}
