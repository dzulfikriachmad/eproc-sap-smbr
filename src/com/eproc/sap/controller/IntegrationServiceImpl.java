
package com.eproc.sap.controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.net.util.Base64;
import org.hibernate.Query;
import org.springframework.boot.devtools.remote.client.HttpHeaderInterceptor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.eproc.sap.dto.ChangePrDto;
import com.eproc.sap.dto.PoHeaderDto;
import com.eproc.sap.dto.PoItemDto;
import com.eproc.sap.dto.ResultDto;
import com.eproc.sap.dto.ZVendorSapDto;
import com.eproc.utils.SessionGetter;
import com.eproc.utils.UploadUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.fasterxml.jackson.databind.ObjectWriter;
//import com.fasterxml.jackson.databind.type.CollectionType;
import com.orm.model.CtrContractHeader;
import com.orm.model.CtrContractItem;
import com.orm.model.PrcMainHeader;
import com.orm.model.PrcMainItem;
import com.orm.model.TmpVndHeader;
import com.orm.tools.GenericDao;

import eproc.controller.PrcService;
import eproc.controller.PrcServiceImpl;
import eproc.controller.RfqService;
import eproc.controller.RfqServiceImpl;
import eproc.controller.VendorService;
import eproc.controller.VendorServiceImpl;

public class IntegrationServiceImpl extends GenericDao implements IntegrationService {
	private VendorService vndService = new VendorServiceImpl();
	private RfqService rfqService = new RfqServiceImpl();
	private PrcService prcService = new PrcServiceImpl();
	
	@Override
	public ResponseEntity<String> generateToken() {
		RestTemplate templatet = new RestTemplate();
		// String urla =
		// "http://10.10.203.42:8021/sap/zsmbrwebservice/IPROC/CSRF-Token?sap-client=220";
		String url = "http://10.10.203.42:8035/sap/zsmbrwebservice/IPROC/CSRF-Token?sap-client=500";
		//String url = "http://10.10.203.31:8031/sap/zsmbrwebservice/IPROC/CSRF-Token?sap-client=300";
		//String url = "http://smbrerpprd1.semenbaturaja.co.id:8031/sap/zsmbrwebservice/IPROC/CSRF-Token?sap-client=300";
		HttpHeaders headersa = new HttpHeaders();
		headersa.set("X-CSRF-Token", "Fetch");
		//String auth = "sap-batch:Smbr#2018";
		String auth = "tlkm-kemal:jakarta";
		byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
		String authHeader = "Basic " + new String(encodedAuth);
		headersa.add("Authorization", authHeader);
		headersa.add("user-agent",
				"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
		List<ClientHttpRequestInterceptor> interceptors = new ArrayList<ClientHttpRequestInterceptor>();
		//interceptors.add(new HttpHeaderInterceptor("username", "sap-batch"));
		//interceptors.add(new HttpHeaderInterceptor("password", "Smbr#2018"));
		 interceptors.add(new HttpHeaderInterceptor("username", "tlkm-kemal"));
		 interceptors.add(new HttpHeaderInterceptor("password", "jakarta"));
		templatet.setInterceptors(interceptors);
		templatet.getMessageConverters().add(new 
		           MappingJackson2HttpMessageConverter());
		HttpEntity<Object> requesta = new HttpEntity<Object>(headersa);
		ResponseEntity<String> sapSecurity = templatet.exchange(url, HttpMethod.GET, requesta, String.class);
		return sapSecurity;
	}

	@Override
	public ResultDto pushPO(CtrContractHeader ctr,
			List<CtrContractItem> contractItemList)  throws JsonProcessingException  {
		ResultDto dto = new ResultDto();
		try {
			RestTemplate template = new RestTemplate();
			
			// ResponseEntity<String> sapSecurity = generateToken();
			// String url = "http://"+SessionGetter.MIDDLEWARE_URL+":7070/PO/pushPO";
			// String url =
			// "http://10.10.203.21:8021/sap/zsmbrwebservice/IPROC/CREATEPO?sap-client=220";
			
			//String url = "http://10.10.203.31:8031/sap/zsmbrwebservice/IPROC/CREATEPO?sap-client=300";
			//String url = "https://localhost:44331/sap/zsmbrwebservice/IPROC/CREATEPO?sap-client=300";
			String url = "http://localhost:8031/sap/zsmbrwebservice/IPROC/CREATEPO?sap-client=300";
			//String url = "http://10.10.203.42:8035/sap/zsmbrwebservice/IPROC/CREATEPO?sap-client=500";
			DateFormat df = new SimpleDateFormat("yyyyMMdd");
			Date today = new Date();

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);

			String ZEBELN = ctr.getContractNumber().split("PO")[0];
			//String BEDAT = df.format(ctr.getCreatedDate());
			String BEDAT = df.format(ctr.getContractStart());
			
			String ELIFN = String.valueOf(ctr.getVndHeader().getVendorId());
			// String ELIFN = "6200020";
			String WAERS = ctr.getAdmCurrency() == null ? "" : ctr.getAdmCurrency().getCurrencyCode();
			String EKORG = ctr.getPurchasingOrganization();
			String BKGRP = ctr.getPurchasingGroup();
			String ZTERM = ctr.getPaymentTerm();
			String AEDAT = df.format(today);
			String ERNAM = ctr.getAdmUserByContractCreator().getCompleteName();
			//String KDATB = df.format(ctr.getContractStart());
			String KDATB = df.format(ctr.getTglPenyerahan());
			String KDATE = df.format(ctr.getContractEnd());
			String ITEM = "";
			PoHeaderDto poHeader = new PoHeaderDto();
			poHeader.setZEBELN(ZEBELN);
			poHeader.setEBELN("");
			poHeader.setBEDAT(BEDAT);
			poHeader.setELIFN(ELIFN);
			poHeader.setWAERS(WAERS);
			poHeader.setEKORG(EKORG);
			poHeader.setBKGRP(BKGRP);
			poHeader.setZTERM(ZTERM);
			poHeader.setINCO1("");
			poHeader.setAEDAT(AEDAT);
			poHeader.setERNAM(ERNAM);
			poHeader.setKDATB(KDATB);
			poHeader.setKDATE(KDATE);
			List<PoItemDto> tmp = new ArrayList<PoItemDto>();

			for (CtrContractItem itm : contractItemList) {
				String ebelp = itm.getLineId() == null ? "" : itm.getLineId().toString();
				String matnr = itm.getItemCode() == null ? "" : String.valueOf(itm.getItemCode());
				String ewerk = ctr.getContractNumber().split("PO")[1].trim();
				String txz = itm.getItemDescription();
				String lgort = itm.getStorageLocation() == null ? "" : itm.getStorageLocation();
				String bstme = itm.getAdmUom().getUomName();
				String bstmg = String.valueOf(itm.getItemQuantityOe());
				String itemPrice = String.valueOf(itm.getItemPriceOe());
				String epein = itemPrice;
				String afnam = ctr.getPrcMainHeader().getNamaPembuatPr() != null
						? ctr.getPrcMainHeader().getNamaPembuatPr()
						: "";
				String banfn = ctr.getPrcMainHeader().getPpmNomorPr();
				String bnfpo = itm.getLineId() == null ? "" : itm.getLineId().toString();
				String bcommitmnt = itm.getServicePriceOe() != null
						? String.valueOf(itm.getServicePriceOe())
						: "0";
				PoItemDto item = new PoItemDto();
				item.setEBELP(ebelp);
				item.setMATNR(matnr);
				item.setEWERK(ewerk);
				item.setLGORT(lgort);
				item.setTXZ01(txz);
				item.setBSTMG(bstmg);
				item.setBSTME(bstme);
				item.setEPEIN(epein);
				item.setAFNAM(afnam);
				item.setBANFN(banfn);
				item.setBNFPO(bnfpo);
				item.setBSUMLIMIT("0.0");
				item.setBCOMMITMNT(bcommitmnt);
				tmp.add(item);
			}
			poHeader.setITEMS(tmp);

			ObjectMapper mapper = new ObjectMapper();

			// Object to JSON in file
			ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
			String json = ow.writeValueAsString(poHeader);
			System.out.println(">>>> JSON <<<<<");
			System.out.println(json);
			System.out.println(">>>>======<<<<<");


			Map<String, String> csrf = (Map<String, String>) generateTokenAndCookies();
			HttpHeaders headerSap = new HttpHeaders();
			String auth = "sap-batch:Smbr#2018";
			//String auth = "tlkm-kemal:jakarta";
			byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
			String authHeader = "Basic " + new String(encodedAuth);
			headerSap.add("Authorization", authHeader);
			headerSap.add("X-CSRF-Token", csrf.get("x-csrf-token"));
			headerSap.add("Content-Type", "application/json");
			// headerSap.add("Cookie", "MYSAPSSO2=" + csrf.get("MYSAPSSO2"));
			headerSap.add("Cookie", "SAP_SESSIONID_SEP_300=" + csrf.get("SAP_SESSIONID_SEP_300"));
			//headerSap.add("Cookie", "SAP_SESSIONID_SPP_500=" + (String)csrf.get("SAP_SESSIONID_SPP_500"));
			HttpEntity<Object> request = new HttpEntity<Object>(poHeader, headerSap);
			PoHeaderDto po = template.postForObject(url, request, PoHeaderDto.class);
			
			json = ow.writeValueAsString(po);
			System.out.println(po);
			System.out.println(">>>> JSON AFTER PUSH<<<<<");
			System.out.println(json);
			System.out.println(">>>>======<<<<<");
			
			String vendorIdSap = po.getLIFNR();
			String contractNumber = po.getEBELN();
			// System.out.println(contractNumber);
			System.out.println(">>>>> VENDOR >>>>>" + po.getLIFNR());
			System.out.println(">>>> KONTRAK >>>>>" + po.getEBELN());
			System.out.println(">>>> NOMOR PR >>>>>" + po.getZEBELN());
			dto.setStatus(200);
			dto.setMessage("Message");
			dto.setData(po);
			return dto;
		} catch (Exception e) {
			dto.setStatus(500);
			dto.setMessage(e.toString());
			dto.setData(null);
			e.printStackTrace();
		}

		return dto;

		// TODO Auto-generated method stub
	}

	@Override
	public ResultDto pushVendor(TmpVndHeader vnd) throws JsonProcessingException  {
		startTransaction();

		ResultDto resultDto = new ResultDto();
		RestTemplate template = new RestTemplate();
		ZVendorSapDto vndDto = new ZVendorSapDto();

		List<Long> vendorIdList = new ArrayList<Long>();
		Query q = session.createSQLQuery("select * from vw_push_vendor where vendor_id = ?");
		q.setParameter(0, vnd.getVendorId());

		List<Object[]> obj = q.list();
		if (obj != null && obj.size() > 0) {
			Iterator<Object[]> it = obj.iterator();
			while (it.hasNext()) {
				Object o[] = it.next();
				it.remove();
				vndDto.setBpext(String.valueOf(o[0]) == null ? "" : String.valueOf(o[0]));
				vndDto.setName1(String.valueOf(o[1]) == null ? "" : String.valueOf(o[1]));
				vndDto.setLifnr("");
				vndDto.setStreet(String.valueOf(o[2]) == null ? "" : String.valueOf(o[2]).replaceAll("(\r\n|\n)", " "));
				vndDto.setPstlz(String.valueOf(o[3]) == null ? "" : String.valueOf(o[3]));
				vndDto.setOrt01(String.valueOf(o[4]) == null ? "" : String.valueOf(o[4]));
				vndDto.setRegio(String.valueOf(o[5]) == null ? "" : String.valueOf(o[5]));
				String idCountry = String.valueOf(o[6]) == null ? ""
						: String.valueOf(o[6]).equalsIgnoreCase("1") ? "ID"
								: String.valueOf(o[6]).equalsIgnoreCase("100") ? "SG"
										: String.valueOf(o[6]).equalsIgnoreCase("102") ? "US"
												: String.valueOf(o[6]).equalsIgnoreCase("103") ? "GB"
														: String.valueOf(o[6]).equalsIgnoreCase("105") ? "DE" : "";

				vndDto.setLand1(idCountry);
				vndDto.setStcd1(String.valueOf(o[7]) == null ? "" : String.valueOf(o[7]));
				vndDto.setTelf1(String.valueOf(o[8]) == null ? "" : String.valueOf(o[8]));
				vndDto.setZterm("");
				vndDto.setTelfx(String.valueOf(o[9]) == null ? "" : String.valueOf(o[9]));
				vndDto.setBstwa(String.valueOf(o[10]) == null ? "" : String.valueOf(o[10]));
				vndDto.setEverk(String.valueOf(o[11]) == null ? "" : String.valueOf(o[11]));
				vndDto.setTelfe(String.valueOf(o[12]) == null ? "" : String.valueOf(o[12]));
				vndDto.setSmtpadr(String.valueOf(o[13]) == null ? "" : String.valueOf(o[13]));
				vndDto.setKtokk(String.valueOf(o[14]) == null ? "" : String.valueOf(o[14]));
			}
			ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
			String json = ow.writeValueAsString(vndDto);
			String u = "http://" + SessionGetter.MIDDLEWARE_URL + "/vendor/sendZVendorToSap";
			HttpHeaders header = new HttpHeaders();
			header.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<Object> request = new HttpEntity<Object>(vndDto, header);

			// Object to JSON in file

			System.out.println(json);
			ResultDto response = template.postForObject(u, request, ResultDto.class);
			System.out.println(response.getStatus());

			if (response.getStatus() == 200) {
				resultDto.setData(response.getData());
				resultDto.setMessage("SUCCESS");
				resultDto.setStatus(200);
			} else {
				resultDto.setData(response.getData());
				resultDto.setMessage(response.getMessage());
				resultDto.setStatus(500);
			}

		}

		return resultDto;

	}

	
	@SuppressWarnings("unchecked")
	@Override
	public ResultDto changePr(List<PrcMainItem> listPrcMainItem, PrcMainHeader prcMainHeader) throws Exception {
		ResultDto resultDto = new ResultDto();
		RestTemplate template = new RestTemplate();

		ChangePrDto dto = new ChangePrDto();
		Double quantity = 0.0;
		Double jumlahOe = 0.0;
		PrcMainHeader prcMain = prcService.getPrcMainHeaderSPPHById(prcMainHeader.getPpmId());

		for (PrcMainItem item : listPrcMainItem) {
			BigDecimal total = item.getItemQuantityOe().multiply(item.getItemPriceOe());
			jumlahOe = jumlahOe + total.doubleValue();
			dto.setBNFPO(String.valueOf(item.getLineId()));
			dto.setAMOUNT(String.valueOf(item.getItemPriceOe()));
			dto.setBANFN(String.valueOf(prcMain.getPpmNomorPr()));

			// WRITE THE LOG
			System.out.println(">>>>>> JSON SAP LOG VALIDATE ANGGARAN <<<<");
			ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
			String json = ow.writeValueAsString(dto);
			UploadUtil.writeIntegrationLog("change_pr_"+String.valueOf(item.getLineId()), prcMain.getPpmNomorPr(), json);
			System.out.println(json);

			Map<String, String> csrf = (Map<String, String>) generateTokenAndCookies();
			
			if(csrf!=null) {
			
				HttpHeaders headerSap = new HttpHeaders();
				for (Map.Entry<String, String> entry : csrf.entrySet())
				{
				     System.out.println(entry.getKey() + "/" + entry.getValue());
				     if(!entry.getKey().contains("x-csrf-token")) {
						 headerSap.add("Cookie", entry.getKey() + "=" + entry.getValue());
				     }
				}
				String auth = SessionGetter.SAP_AUTH;
				byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
				String authHeader = "Basic " + new String(encodedAuth);
				headerSap.add("Authorization", authHeader);
				headerSap.add("X-CSRF-Token", csrf.get("x-csrf-token"));
				headerSap.setContentType(MediaType.APPLICATION_JSON);
			    String u = "http://"+SessionGetter.SAP_URL+"/sap/zsmbrwebservice/IPROC/CHANGEPR?sap-client="+SessionGetter.SAP_CLIENT;
				HttpEntity<Object> request = new HttpEntity<Object>( ow.writeValueAsString(dto), headerSap);
				Object response = template.postForObject(u, request, Object.class);
				Map<String, String> test = (Map<String, String>) response;

				if (test.get("TYPE_MSG").equalsIgnoreCase("200")) {
					resultDto.setData(response);
					resultDto.setMessage(test.get("MESSAGE"));
					resultDto.setStatus(Integer.valueOf(test.get("TYPE_MSG")));
					System.out.println("CODE : " + resultDto.getStatus());
					System.out.println("MESSAGE : " + resultDto.getMessage());
				} else {
					resultDto.setData(response);
					resultDto.setMessage(test.get("MESSAGE"));
					resultDto.setStatus(Integer.valueOf(test.get("TYPE_MSG")));
					System.out.println(test.get("TYPE_MSG"));
					throw new Exception( "ERROR CODE: " + test.get("TYPE_MSG") + " " + resultDto.getMessage() + " ITEM : " + test.get("BNFPO") + ", NILAI OE : "
							+ NumberFormat.getCurrencyInstance(new Locale("id", "ID")).format(total));
				}
			}else {
				throw new Exception("Terjadi kesalahan sambungan SAP");
			}
		}
		
		return resultDto;

			
			

	}

	@Override
	public Map<String, String> generateTokenAndCookies(){
		System.out.println("GENEATE COOKIES AND TOKEN");
		try {
			
			// "http://10.10.203.21:8021/sap/zsmbrwebservice/IPROC/CSRF-Token?sap-client=220";
			//String url = "http://10.10.203.31:8031/sap/zsmbrwebservice/IPROC/CSRF-Token?sap-client=300";
			//String url = "http://localhost:8035/sap/zsmbrwebservice/IPROC/CSRF-Token?sap-client=500";
			//String url = "http://smbrerpprd1.semenbaturaja.co.id:8031/sap/zsmbrwebservice/IPROC/CSRF-Token?sap-client=300";
			
			
			RestTemplate templatet = new RestTemplate();
			///String url = "http://"+SessionGetter.SAP_URL+"/sap/zsmbrwebservice/IPROC/CSRF-Token?sap-client="+SessionGetter.SAP_CLIENT;
			String url = "http://localhost:8031/sap/zsmbrwebservice/IPROC/CSRF-Token?sap-client=300";
			HttpHeaders headersa = new HttpHeaders();
			System.out.println("header with url : "+ url);
			headersa.set("X-CSRF-Token", "Fetch");
			String auth = "sap-batch:Smbr#2018";
			 //String auth = SessionGetter.SAP_AUTH;
			byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
			String authHeader = "Basic " + new String(encodedAuth);
			headersa.add("Authorization", authHeader);
			headersa.setContentType(MediaType.APPLICATION_JSON);
			headersa.add("user-agent",
					"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
			System.out.println("headersadd");
			List<ClientHttpRequestInterceptor> interceptors = new ArrayList<ClientHttpRequestInterceptor>();
			/*interceptors.add(new HttpHeaderInterceptor("username", SessionGetter.SAP_USERNAME));
		    interceptors.add(new HttpHeaderInterceptor("password", SessionGetter.SAP_PASSWORD));*/
			interceptors.add(new HttpHeaderInterceptor("username", "sap-batch"));
		    interceptors.add(new HttpHeaderInterceptor("password", "Smbr#2018"));
			templatet.setInterceptors(interceptors);

		    
			/*interceptors.add(new HttpHeaderInterceptor("username", "sap-batch"));
			interceptors.add(new HttpHeaderInterceptor("password", "Smbr#2018"));*/
			
			HttpEntity<Object> requesta = new HttpEntity<Object>(headersa);
			System.out.println("request httpentity");
			System.out.println("declare response entity");
			ResponseEntity sapSecurity = null;
			System.out.println("exchange start");
			templatet.getMessageConverters().add(new 
			           MappingJackson2HttpMessageConverter());
			sapSecurity = templatet.exchange(url, HttpMethod.GET, requesta, String.class);
			System.out.println("declare");
			HashMap<String, String> sap = new HashMap<String, String>();
			sap.put("x-csrf-token", sapSecurity.getHeaders().get("x-csrf-token").get(0));
			String[] val = sapSecurity.getHeaders().get("set-cookie").get(1).split(";")[0].split("=");
			sap.put(val[0], val[1]);
			for (Map.Entry<String, String> entry : sap.entrySet())
			{
			    System.out.println(entry.getKey() + "/" + entry.getValue());
			}
			return sap;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
		
	}
	

}



//TRASH
//Iterator<String> it = csrf.keySet().iterator();
/*while (it.hasNext()) {
	System.out.println(it.next() + "" + );
}*/
//String auth = "tlkm-kemal:jakarta";
//String auth = "sap-batch:Smbr#2018";

//headerSap.add("Cookie", "SAP_SESSIONID_SEP_300=" + csrf.get("SAP_SESSIONID_SEP_300"));
// headerSap.add("Cookie", "sap-ssolist=" + csrf.get("sap-ssolist"));
  
//String u = "http://10.10.203.31:8031/sap/zsmbrwebservice/IPROC/CHANGEPR?sap-client=300";
//String u = "http://localhost:8031/sap/zsmbrwebservice/IPROC/CHANGEPR?sap-client=300";
// template.getMessageConverters().add(new
// MappingJackson2HttpMessageConverter());

// String u = "http://"+SessionGetter.MIDDLEWARE_URL+":7070/OR/changePR";
// smbrerpdev.semenbaturaja.co.id:8011/sap/zsmbrwebservice/IPROC/CHANGEPR?sap-client=120