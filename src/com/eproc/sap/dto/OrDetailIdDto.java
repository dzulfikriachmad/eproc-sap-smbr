package com.eproc.sap.dto;
import javax.persistence.Column;

public class OrDetailIdDto {
	private Integer nomorOr;
	private String orH;
	private String headerSite;
	private Integer lineId;
	public Integer getNomorOr() {
		return nomorOr;
	}
	public void setNomorOr(Integer nomorOr) {
		this.nomorOr = nomorOr;
	}
	public String getOrH() {
		return orH;
	}
	public void setOrH(String orH) {
		this.orH = orH;
	}
	public String getHeaderSite() {
		return headerSite;
	}
	public void setHeaderSite(String headerSite) {
		this.headerSite = headerSite;
	}
	public Integer getLineId() {
		return lineId;
	}
	public void setLineId(Integer lineId) {
		this.lineId = lineId;
	}
	
	
}
