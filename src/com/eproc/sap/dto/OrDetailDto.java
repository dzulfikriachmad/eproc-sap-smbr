package com.eproc.sap.dto;
import javax.persistence.Column;
import javax.persistence.Lob;


public class OrDetailDto extends OrDetailIdDto{
	private Object orHeader;
	private Integer line;
	private String kodeBarang;
	private String namaBarang;
	private String namaBarangExt;
	private Double jumlah;
	private String satuan;
	private Double hargaSatuan;
	private String lineType;
	private String accuountNumber;
	private String attachment;
	private String fileName;
	private String serverFileName;
	private byte[] content;
	private String contentType;
	public Object getOrHeader() {
		return orHeader;
	}
	public void setOrHeader(Object orHeader) {
		this.orHeader = orHeader;
	}
	public Integer getLine() {
		return line;
	}
	public void setLine(Integer line) {
		this.line = line;
	}
	public String getKodeBarang() {
		return kodeBarang;
	}
	public void setKodeBarang(String kodeBarang) {
		this.kodeBarang = kodeBarang;
	}
	public String getNamaBarang() {
		return namaBarang;
	}
	public void setNamaBarang(String namaBarang) {
		this.namaBarang = namaBarang;
	}
	public String getNamaBarangExt() {
		return namaBarangExt;
	}
	public void setNamaBarangExt(String namaBarangExt) {
		this.namaBarangExt = namaBarangExt;
	}
	public Double getJumlah() {
		return jumlah;
	}
	public void setJumlah(Double jumlah) {
		this.jumlah = jumlah;
	}
	public String getSatuan() {
		return satuan;
	}
	public void setSatuan(String satuan) {
		this.satuan = satuan;
	}
	public Double getHargaSatuan() {
		return hargaSatuan;
	}
	public void setHargaSatuan(Double hargaSatuan) {
		this.hargaSatuan = hargaSatuan;
	}
	public String getLineType() {
		return lineType;
	}
	public void setLineType(String lineType) {
		this.lineType = lineType;
	}
	public String getAccuountNumber() {
		return accuountNumber;
	}
	public void setAccuountNumber(String accuountNumber) {
		this.accuountNumber = accuountNumber;
	}
	public String getAttachment() {
		return attachment;
	}
	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getServerFileName() {
		return serverFileName;
	}
	public void setServerFileName(String serverFileName) {
		this.serverFileName = serverFileName;
	}
	public byte[] getContent() {
		return content;
	}
	public void setContent(byte[] content) {
		this.content = content;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	
	
}
