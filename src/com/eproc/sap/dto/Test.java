package com.eproc.sap.dto;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.rtf.RTFEditorKit;

import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.eproc.sap.controller.IntegrationService;
import com.eproc.sap.controller.IntegrationServiceImpl;
import com.orm.model.OrHeader;
import com.orm.model.OrHeaderId;
import com.orm.tools.GenericDao;

public class Test extends GenericDao {

	 /**
	  * @param args
	 * @throws Exception 
	  */
	 @SuppressWarnings("unchecked")
	public static void main(String[] args) throws Exception {

		 IntegrationService in = new IntegrationServiceImpl();
			System.out.println(in.generateTokenAndCookies());
		    
	 }
	 
	 public  OrHeader getOrHeaderById(OrHeaderId id) throws Exception {
			DetachedCriteria dc = DetachedCriteria.forClass(OrHeader.class);
			dc.setResultTransformer(DetachedCriteria.DISTINCT_ROOT_ENTITY);
			dc.add(Restrictions.idEq(id));
			return (OrHeader) getEntityByDetachedCiteria(dc);
	}
	 public static String to_text(String p_in) throws IOException,
		BadLocationException {
	// test for null inputs to avoid java.lang.NullPointerException when
	// input is null
	if (p_in != null) {
		RTFEditorKit kit = new RTFEditorKit();
		Document doc = kit.createDefaultDocument();
		kit.read(new ByteArrayInputStream(p_in.getBytes()), doc, 0);
		return doc.getText(0, doc.getLength());
	} else
		return null;
}
	
}
