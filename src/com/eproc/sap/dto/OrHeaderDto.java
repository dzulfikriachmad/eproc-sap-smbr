package com.eproc.sap.dto;

import java.util.Date;
import java.util.List;


public class OrHeaderDto extends OrHeaderIdDto{
	private String lastStatus;
	private String nextStatus;
	private Date tglOr;
	private Date tglApprove;
	private String pembuatOm;
	private String namaPembuatOm;
	private String nomorOm;
	private String omOn;
	private String site;
	private String unitKerja;
	private String nomorWo;
	private String attachment;
	private String fileName;
	private String serverFileName;
	private Integer status;
	private byte [] content;
	private String contentType;	
	private String mataUang;
	private Date createdDate;
	private String kodeGenerate;
	private String kodeSatker;
	private String pembuatOrJasa;
	private List<OrDetailDto> listOrDetail;
	public String getLastStatus() {
		return lastStatus;
	}
	public void setLastStatus(String lastStatus) {
		this.lastStatus = lastStatus;
	}
	public String getNextStatus() {
		return nextStatus;
	}
	public void setNextStatus(String nextStatus) {
		this.nextStatus = nextStatus;
	}
	public Date getTglOr() {
		return tglOr;
	}
	public void setTglOr(Date tglOr) {
		this.tglOr = tglOr;
	}
	public Date getTglApprove() {
		return tglApprove;
	}
	public void setTglApprove(Date tglApprove) {
		this.tglApprove = tglApprove;
	}
	public String getPembuatOm() {
		return pembuatOm;
	}
	public void setPembuatOm(String pembuatOm) {
		this.pembuatOm = pembuatOm;
	}
	public String getNamaPembuatOm() {
		return namaPembuatOm;
	}
	public void setNamaPembuatOm(String namaPembuatOm) {
		this.namaPembuatOm = namaPembuatOm;
	}
	public String getNomorOm() {
		return nomorOm;
	}
	public void setNomorOm(String nomorOm) {
		this.nomorOm = nomorOm;
	}
	public String getOmOn() {
		return omOn;
	}
	public void setOmOn(String omOn) {
		this.omOn = omOn;
	}
	public String getSite() {
		return site;
	}
	public void setSite(String site) {
		this.site = site;
	}
	public String getUnitKerja() {
		return unitKerja;
	}
	public void setUnitKerja(String unitKerja) {
		this.unitKerja = unitKerja;
	}
	public String getNomorWo() {
		return nomorWo;
	}
	public void setNomorWo(String nomorWo) {
		this.nomorWo = nomorWo;
	}
	public String getAttachment() {
		return attachment;
	}
	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getServerFileName() {
		return serverFileName;
	}
	public void setServerFileName(String serverFileName) {
		this.serverFileName = serverFileName;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public byte[] getContent() {
		return content;
	}
	public void setContent(byte[] content) {
		this.content = content;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public String getMataUang() {
		return mataUang;
	}
	public void setMataUang(String mataUang) {
		this.mataUang = mataUang;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getKodeGenerate() {
		return kodeGenerate;
	}
	public void setKodeGenerate(String kodeGenerate) {
		this.kodeGenerate = kodeGenerate;
	}
	public String getKodeSatker() {
		return kodeSatker;
	}
	public void setKodeSatker(String kodeSatker) {
		this.kodeSatker = kodeSatker;
	}
	public String getPembuatOrJasa() {
		return pembuatOrJasa;
	}
	public void setPembuatOrJasa(String pembuatOrJasa) {
		this.pembuatOrJasa = pembuatOrJasa;
	}
	public List<OrDetailDto> getListOrDetail() {
		return listOrDetail;
	}
	public void setListOrDetail(List<OrDetailDto> listOrDetail) {
		this.listOrDetail = listOrDetail;
	}
	
	
}

