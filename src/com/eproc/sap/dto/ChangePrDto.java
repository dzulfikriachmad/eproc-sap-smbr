package com.eproc.sap.dto;

import java.util.List;

public class ChangePrDto {
	private String BANFN;
	private String BNFPO;
	private String AMOUNT;
	private List<MessagesDto> MESSAGE;
	public String getBANFN() {
		return BANFN;
	}
	public void setBANFN(String bANFN) {
		BANFN = bANFN;
	}
	public String getBNFPO() {
		return BNFPO;
	}
	public void setBNFPO(String bNFPO) {
		BNFPO = bNFPO;
	}
	public String getAMOUNT() {
		return AMOUNT;
	}
	public void setAMOUNT(String aMOUNT) {
		AMOUNT = aMOUNT;
	}
	public List<MessagesDto> getMESSAGE() {
		return MESSAGE;
	}
	public void setMESSAGE(List<MessagesDto> mESSAGE) {
		MESSAGE = mESSAGE;
	}
	
	
	
}
