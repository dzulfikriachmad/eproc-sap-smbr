package com.eproc.sap.dto;


public class XCsrfTokenDto {
	private String xcsrftoken ;
	private String contentLength;
	private String sapPerfFesrec;
	public String getXcsrftoken() {
		return xcsrftoken;
	}

	public void setXcsrftoken(String xcsrftoken) {
		this.xcsrftoken = xcsrftoken;
	}

	public String getContentLength() {
		return contentLength;
	}

	public void setContentLength(String contentLength) {
		this.contentLength = contentLength;
	}

	public String getSapPerfFesrec() {
		return sapPerfFesrec;
	}

	public void setSapPerfFesrec(String sapPerfFesrec) {
		this.sapPerfFesrec = sapPerfFesrec;
	}
}
