package com.eproc.sap.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class PoItemDto {
	
	
	private String EBELP;
	
	
	private String MATNR;
	

	private String EWERK;
	

	private String LGORT;
	
	
	private String TXZ01;
	
	
	private String BSTMG;
	
	
	private String BSTME;
	
	
	private String EPEIN;
	
	
	private String AFNAM;
	
	
	private String BANFN;
	
	
	private String BNFPO;
	
	
	private String BSUMLIMIT;
	
	
	private String BCOMMITMNT;
	public String getEBELP() {
		return EBELP;
	}
	public void setEBELP(String eBELP) {
		EBELP = eBELP;
	}
	public String getMATNR() {
		return MATNR;
	}
	public void setMATNR(String mATNR) {
		MATNR = mATNR;
	}
	public String getEWERK() {
		return EWERK;
	}
	public void setEWERK(String eWERK) {
		EWERK = eWERK;
	}
	public String getLGORT() {
		return LGORT;
	}
	public void setLGORT(String lGORT) {
		LGORT = lGORT;
	}
	public String getTXZ01() {
		return TXZ01;
	}
	public void setTXZ01(String tXZ01) {
		TXZ01 = tXZ01;
	}
	public String getBSTMG() {
		return BSTMG;
	}
	public void setBSTMG(String bSTMG) {
		BSTMG = bSTMG;
	}
	public String getBSTME() {
		return BSTME;
	}
	public void setBSTME(String bSTME) {
		BSTME = bSTME;
	}
	public String getEPEIN() {
		return EPEIN;
	}
	public void setEPEIN(String ePEIN) {
		EPEIN = ePEIN;
	}
	public String getAFNAM() {
		return AFNAM;
	}
	public void setAFNAM(String aFNAM) {
		AFNAM = aFNAM;
	}
	public String getBANFN() {
		return BANFN;
	}
	public void setBANFN(String bANFN) {
		BANFN = bANFN;
	}
	public String getBNFPO() {
		return BNFPO;
	}
	public void setBNFPO(String bNFPO) {
		BNFPO = bNFPO;
	}
	public String getBSUMLIMIT() {
		return BSUMLIMIT;
	}
	public void setBSUMLIMIT(String bSUMLIMIT) {
		BSUMLIMIT = bSUMLIMIT;
	}
	public String getBCOMMITMNT() {
		return BCOMMITMNT;
	}
	public void setBCOMMITMNT(String bCOMMITMNT) {
		BCOMMITMNT = bCOMMITMNT;
	}
	
	
	
}
