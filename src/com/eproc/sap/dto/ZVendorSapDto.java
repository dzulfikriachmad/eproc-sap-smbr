package com.eproc.sap.dto;


public class ZVendorSapDto {
	//External BP Number
	private String bpext;
	//Vendor number	
	private String lifnr;
	//Name
	private String name1;
	//House number and street
	private String street;
	//Postal Code
	private String pstlz;
	//City
	private String ort01;
	//Region (State, Province, County)
	private String regio;
	//Country of company
	private String land1;
	//Tax Number 1
	private String stcd1;
	//First telephone number
	private String telf1;
	//Fax Number
	private String telfx;
	//Terms of Payment Key
	private String zterm;
	//Purchase order currency
	private String bstwa;
	//Salesperson
	private String everk;
	//Telephone
	private String telfe;

	private String smtpadr;

	private String ktokk;

	public String getBpext() {
		return bpext;
	}

	public void setBpext(String bpext) {
		this.bpext = bpext;
	}

	public String getLifnr() {
		return lifnr;
	}

	public void setLifnr(String lifnr) {
		this.lifnr = lifnr;
	}

	public String getName1() {
		return name1;
	}

	public void setName1(String name1) {
		this.name1 = name1;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getPstlz() {
		return pstlz;
	}

	public void setPstlz(String pstlz) {
		this.pstlz = pstlz;
	}

	public String getOrt01() {
		return ort01;
	}

	public void setOrt01(String ort01) {
		this.ort01 = ort01;
	}

	public String getRegio() {
		return regio;
	}

	public void setRegio(String regio) {
		this.regio = regio;
	}

	public String getLand1() {
		return land1;
	}

	public void setLand1(String land1) {
		this.land1 = land1;
	}

	public String getStcd1() {
		return stcd1;
	}

	public void setStcd1(String stcd1) {
		this.stcd1 = stcd1;
	}

	public String getTelf1() {
		return telf1;
	}

	public void setTelf1(String telf1) {
		this.telf1 = telf1;
	}

	public String getTelfx() {
		return telfx;
	}

	public void setTelfx(String telfx) {
		this.telfx = telfx;
	}

	public String getZterm() {
		return zterm;
	}

	public void setZterm(String zterm) {
		this.zterm = zterm;
	}

	public String getBstwa() {
		return bstwa;
	}

	public void setBstwa(String bstwa) {
		this.bstwa = bstwa;
	}

	public String getEverk() {
		return everk;
	}

	public void setEverk(String everk) {
		this.everk = everk;
	}

	public String getTelfe() {
		return telfe;
	}

	public void setTelfe(String telfe) {
		this.telfe = telfe;
	}

	public String getSmtpadr() {
		return smtpadr;
	}

	public void setSmtpadr(String smtpadr) {
		this.smtpadr = smtpadr;
	}

	public String getKtokk() {
		return ktokk;
	}

	public void setKtokk(String ktokk) {
		this.ktokk = ktokk;
	}
	
	

}
