package com.eproc.sap.dto;

public class VendorDto {
	Integer vendorId;
	String czActivationCode;
	String szTaxId;
	String szSearchType;
	String cUpdateMasterFile;
	String szAlphaName;
	String szMailingName;
	String szDescriptionCompressed;
	String szBusinessUnit;
	String szAddressLine1;
	String szAddressLine2;
	String szAddressLine3;
	String szAddressLine4;
	String szPostalCode;
	String szCity;
	String szState;
	String szCountry;
	String szPrefix1;
	String szPhoneNumber1;
	String szPhoneNumberType2;
	String szPhoneNumber2;
	String cPayablesYNM;
	String cReceivablesYN;
	String cEmployeeYN;
	String cUserCode;
	String cARAPNettingY;
	String szAccountRepresentative;
	String szProgramId;
	public Integer getVendorId() {
		return vendorId;
	}
	public void setVendorId(Integer vendorId) {
		this.vendorId = vendorId;
	}
	public String getCzActivationCode() {
		return czActivationCode;
	}
	public void setCzActivationCode(String czActivationCode) {
		this.czActivationCode = czActivationCode;
	}
	public String getSzTaxId() {
		return szTaxId;
	}
	public void setSzTaxId(String szTaxId) {
		this.szTaxId = szTaxId;
	}
	public String getSzSearchType() {
		return szSearchType;
	}
	public void setSzSearchType(String szSearchType) {
		this.szSearchType = szSearchType;
	}
	public String getcUpdateMasterFile() {
		return cUpdateMasterFile;
	}
	public void setcUpdateMasterFile(String cUpdateMasterFile) {
		this.cUpdateMasterFile = cUpdateMasterFile;
	}
	public String getSzAlphaName() {
		return szAlphaName;
	}
	public void setSzAlphaName(String szAlphaName) {
		this.szAlphaName = szAlphaName;
	}
	public String getSzMailingName() {
		return szMailingName;
	}
	public void setSzMailingName(String szMailingName) {
		this.szMailingName = szMailingName;
	}
	public String getSzDescriptionCompressed() {
		return szDescriptionCompressed;
	}
	public void setSzDescriptionCompressed(String szDescriptionCompressed) {
		this.szDescriptionCompressed = szDescriptionCompressed;
	}
	public String getSzBusinessUnit() {
		return szBusinessUnit;
	}
	public void setSzBusinessUnit(String szBusinessUnit) {
		this.szBusinessUnit = szBusinessUnit;
	}
	public String getSzAddressLine1() {
		return szAddressLine1;
	}
	public void setSzAddressLine1(String szAddressLine1) {
		this.szAddressLine1 = szAddressLine1;
	}
	public String getSzAddressLine2() {
		return szAddressLine2;
	}
	public void setSzAddressLine2(String szAddressLine2) {
		this.szAddressLine2 = szAddressLine2;
	}
	public String getSzAddressLine3() {
		return szAddressLine3;
	}
	public void setSzAddressLine3(String szAddressLine3) {
		this.szAddressLine3 = szAddressLine3;
	}
	public String getSzAddressLine4() {
		return szAddressLine4;
	}
	public void setSzAddressLine4(String szAddressLine4) {
		this.szAddressLine4 = szAddressLine4;
	}
	public String getSzPostalCode() {
		return szPostalCode;
	}
	public void setSzPostalCode(String szPostalCode) {
		this.szPostalCode = szPostalCode;
	}
	public String getSzCity() {
		return szCity;
	}
	public void setSzCity(String szCity) {
		this.szCity = szCity;
	}
	public String getSzState() {
		return szState;
	}
	public void setSzState(String szState) {
		this.szState = szState;
	}
	public String getSzCountry() {
		return szCountry;
	}
	public void setSzCountry(String szCountry) {
		this.szCountry = szCountry;
	}
	public String getSzPrefix1() {
		return szPrefix1;
	}
	public void setSzPrefix1(String szPrefix1) {
		this.szPrefix1 = szPrefix1;
	}
	public String getSzPhoneNumber1() {
		return szPhoneNumber1;
	}
	public void setSzPhoneNumber1(String szPhoneNumber1) {
		this.szPhoneNumber1 = szPhoneNumber1;
	}
	public String getSzPhoneNumberType2() {
		return szPhoneNumberType2;
	}
	public void setSzPhoneNumberType2(String szPhoneNumberType2) {
		this.szPhoneNumberType2 = szPhoneNumberType2;
	}
	public String getSzPhoneNumber2() {
		return szPhoneNumber2;
	}
	public void setSzPhoneNumber2(String szPhoneNumber2) {
		this.szPhoneNumber2 = szPhoneNumber2;
	}
	public String getcPayablesYNM() {
		return cPayablesYNM;
	}
	public void setcPayablesYNM(String cPayablesYNM) {
		this.cPayablesYNM = cPayablesYNM;
	}
	public String getcReceivablesYN() {
		return cReceivablesYN;
	}
	public void setcReceivablesYN(String cReceivablesYN) {
		this.cReceivablesYN = cReceivablesYN;
	}
	public String getcEmployeeYN() {
		return cEmployeeYN;
	}
	public void setcEmployeeYN(String cEmployeeYN) {
		this.cEmployeeYN = cEmployeeYN;
	}
	public String getcUserCode() {
		return cUserCode;
	}
	public void setcUserCode(String cUserCode) {
		this.cUserCode = cUserCode;
	}
	public String getcARAPNettingY() {
		return cARAPNettingY;
	}
	public void setcARAPNettingY(String cARAPNettingY) {
		this.cARAPNettingY = cARAPNettingY;
	}
	public String getSzAccountRepresentative() {
		return szAccountRepresentative;
	}
	public void setSzAccountRepresentative(String szAccountRepresentative) {
		this.szAccountRepresentative = szAccountRepresentative;
	}
	public String getSzProgramId() {
		return szProgramId;
	}
	public void setSzProgramId(String szProgramId) {
		this.szProgramId = szProgramId;
	}
	
	
}
