package com.eproc.sap.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class PoHeaderDto {
	
	
	private String ZEBELN;
	
	
	private String EBELN;
	
	
	private String LIFNR;
	
	
	private String BEDAT;
	
	
	private String ELIFN;
	
	
	private String WAERS;
	
	
	private String AEDAT;
	
	
	private String ERNAM;
	
	
	private String EKORG;
	
	
	private String BKGRP;
	
	
	private String ZTERM;
	
	
	private String INCO1;
	
	
	private String KDATB;
	
	
	private String KDATE;
	
	
	private List<PoItemDto> ITEMS;
	
	
	private List<MessagesDto> MESSAGES;
	
	public String getZEBELN() {
		return ZEBELN;
	}
	public void setZEBELN(String zEBELN) {
		ZEBELN = zEBELN;
	}
	public String getEBELN() {
		return EBELN;
	}
	public void setEBELN(String eBELN) {
		EBELN = eBELN;
	}
	public String getBEDAT() {
		return BEDAT;
	}
	public void setBEDAT(String bEDAT) {
		BEDAT = bEDAT;
	}
	public String getELIFN() {
		return ELIFN;
	}
	public void setELIFN(String eLIFN) {
		ELIFN = eLIFN;
	}
	public String getAEDAT() {
		return AEDAT;
	}
	public void setAEDAT(String aEDAT) {
		AEDAT = aEDAT;
	}
	public String getERNAM() {
		return ERNAM;
	}
	public void setERNAM(String eRNAM) {
		ERNAM = eRNAM;
	}
	public String getEKORG() {
		return EKORG;
	}
	public void setEKORG(String eKORG) {
		EKORG = eKORG;
	}
	public String getBKGRP() {
		return BKGRP;
	}
	public void setBKGRP(String bKGRP) {
		BKGRP = bKGRP;
	}
	public String getZTERM() {
		return ZTERM;
	}
	public void setZTERM(String zTERM) {
		ZTERM = zTERM;
	}
	public String getINCO1() {
		return INCO1;
	}
	public void setINCO1(String iNCO1) {
		INCO1 = iNCO1;
	}
	public String getKDATB() {
		return KDATB;
	}
	public void setKDATB(String kDATB) {
		KDATB = kDATB;
	}
	public String getKDATE() {
		return KDATE;
	}
	public void setKDATE(String kDATE) {
		KDATE = kDATE;
	}
	public List<PoItemDto> getITEMS() {
		return ITEMS;
	}
	public void setITEMS(List<PoItemDto> iTEMS) {
		ITEMS = iTEMS;
	}
	public String getWAERS() {
		return WAERS;
	}
	public void setWAERS(String wAERS) {
		WAERS = wAERS;
	}
	public List<MessagesDto> getMESSAGES() {
		return MESSAGES;
	}
	public void setMESSAGES(List<MessagesDto> mESSAGES) {
		MESSAGES = mESSAGES;
	}
	public String getLIFNR() {
		return LIFNR;
	}
	public void setLIFNR(String lIFNR) {
		LIFNR = lIFNR;
	}
	
	

}
