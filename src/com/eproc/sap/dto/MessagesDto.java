package com.eproc.sap.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MessagesDto {
	@JsonProperty("TYPE")
	String TYPE;
	
	@JsonProperty("ID")
    String ID;
	
	@JsonProperty("NUMBER")
    String NUMBER;
	
	@JsonProperty("MESSAGE")
    String MESSAGE;
	
	@JsonProperty("LOG_NO")
    String LOG_NO;
	
	@JsonProperty("LOG_MSG_NO")
    String LOG_MSG_NO;
	
	@JsonProperty("MESSAGE_V1")
    String MESSAGE_V1;
	
	@JsonProperty("MESSAGE_V2")
    String MESSAGE_V2;
	
	@JsonProperty("MESSAGE_V3")
    String MESSAGE_V3;
	
	@JsonProperty("MESSAGE_V4")
    String MESSAGE_V4;
	
	@JsonProperty("PARAMETER")
    String PARAMETER;
	
	@JsonProperty("ROW")
    String ROW;
	
	@JsonProperty("FIELD")
    String FIELD;
	
	@JsonProperty("SYSTEM")
    String SYSTEM;
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getNUMBER() {
		return NUMBER;
	}
	public void setNUMBER(String nUMBER) {
		NUMBER = nUMBER;
	}
	public String getMESSAGE() {
		return MESSAGE;
	}
	public void setMESSAGE(String mESSAGE) {
		MESSAGE = mESSAGE;
	}
	public String getLOG_NO() {
		return LOG_NO;
	}
	public void setLOG_NO(String lOG_NO) {
		LOG_NO = lOG_NO;
	}
	public String getLOG_MSG_NO() {
		return LOG_MSG_NO;
	}
	public void setLOG_MSG_NO(String lOG_MSG_NO) {
		LOG_MSG_NO = lOG_MSG_NO;
	}
	public String getMESSAGE_V1() {
		return MESSAGE_V1;
	}
	public void setMESSAGE_V1(String mESSAGE_V1) {
		MESSAGE_V1 = mESSAGE_V1;
	}
	public String getMESSAGE_V2() {
		return MESSAGE_V2;
	}
	public void setMESSAGE_V2(String mESSAGE_V2) {
		MESSAGE_V2 = mESSAGE_V2;
	}
	public String getMESSAGE_V3() {
		return MESSAGE_V3;
	}
	public void setMESSAGE_V3(String mESSAGE_V3) {
		MESSAGE_V3 = mESSAGE_V3;
	}
	public String getMESSAGE_V4() {
		return MESSAGE_V4;
	}
	public void setMESSAGE_V4(String mESSAGE_V4) {
		MESSAGE_V4 = mESSAGE_V4;
	}
	public String getPARAMETER() {
		return PARAMETER;
	}
	public void setPARAMETER(String pARAMETER) {
		PARAMETER = pARAMETER;
	}
	public String getROW() {
		return ROW;
	}
	public void setROW(String rOW) {
		ROW = rOW;
	}
	public String getFIELD() {
		return FIELD;
	}
	public void setFIELD(String fIELD) {
		FIELD = fIELD;
	}
	public String getSYSTEM() {
		return SYSTEM;
	}
	public void setSYSTEM(String sYSTEM) {
		SYSTEM = sYSTEM;
	}
    
    
}
