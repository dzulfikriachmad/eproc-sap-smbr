package com.eproc.v.global.action;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.eproc.core.BaseController;
import com.eproc.v.global.command.TodoListVendorCommand;

import eproc.controller.RfqVendorService;
import eproc.controller.RfqVendorServiceImpl;

@Namespace("/")
@ParentPackage("eprocVnd")
public class VndGlobalJson extends BaseController {

	private TodoListVendorCommand model;
	private RfqVendorService service;
	@Override
	public Object getModel() {
		return this.model;
	}
	
	public void prepare() throws Exception {
		model = new TodoListVendorCommand();
		service = new RfqVendorServiceImpl();
	};
	
	@Action(
			results={
					@Result(name = SUCCESS, type="json")
			}, value = "daftarHistoryJson"
			)
	public String daftarHistoryJson() throws Exception{
		System.out.println(">>>> "+request.getParameter("iDisplayStart"));
		return SUCCESS;
	}
}
