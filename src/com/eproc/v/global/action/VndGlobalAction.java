package com.eproc.v.global.action;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.web.util.WebUtils;

import com.eproc.core.BaseController;
import com.eproc.utils.EmailSessionBean;
import com.eproc.utils.QueryUtil;
import com.eproc.utils.SessionGetter;
import com.eproc.utils.UploadUtil;
import com.eproc.v.global.command.VndGlobalCommand;
import com.orm.model.AdmUser;
import com.orm.model.EauctionHeader;
import com.orm.model.PrcMainHeader;
import com.orm.model.PrcMainItem;
import com.orm.model.PrcMainVendor;
import com.orm.model.PrcMainVendorQuote;
import com.orm.model.PrcMainVendorQuoteId;
import com.orm.model.PrcMsgSanggahan;
import com.orm.model.TmpVndHeader;
import com.orm.model.VndHeader;

import eproc.controller.AdmService;
import eproc.controller.AdmServiceImpl;
import eproc.controller.EauctionService;
import eproc.controller.EauctionServiceImpl;
import eproc.controller.PrcService;
import eproc.controller.PrcServiceImpl;
import eproc.controller.RfqVendorService;
import eproc.controller.RfqVendorServiceImpl;
import eproc.controller.ToolsService;
import eproc.controller.ToolsServiceImpl;

@Namespace("/")
@ParentPackage("eprocVnd")
public class VndGlobalAction extends BaseController {
	private VndGlobalCommand model;
	private RfqVendorService service;
	private PrcService prcService;
	private ToolsService toolsService;
	private EauctionService eauctionService;
	private AdmService admService;
	@Override
	public Object getModel() {
		return model;
	}

	@Override
	public void prepare() throws Exception {
		model = new VndGlobalCommand();
		service = new RfqVendorServiceImpl();
		prcService = new PrcServiceImpl();
		toolsService = new ToolsServiceImpl();
		eauctionService = new EauctionServiceImpl();
		admService = new AdmServiceImpl();
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/dashboard.jsp")
			}, value="vnd/dashboard"
	)
	public String vndDashboard() throws Exception{
		try {
			TmpVndHeader headr = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
			model.setUndangan(service.getJumlahDashboardVendor(QueryUtil.jumlahDashboardVendor(headr.getVendorId(), 2),2));
			model.setMenungguPenawaran(service.getJumlahDashboardVendor(QueryUtil.jumlahDashboardVendor(headr.getVendorId(), 3),3));
			model.setMenungguPenawaran(String.valueOf(Integer.parseInt(model.getMenungguPenawaran()) + Integer.parseInt(service.getJumlahDashboardVendor(QueryUtil.jumlahDashboardVendor(headr.getVendorId(), 6).concat("and d.pqm_total_penawaran is null"),5))));
			model.setSudahMenawarkan(service.getJumlahDashboardVendor(QueryUtil.jumlahDashboardVendor(headr.getVendorId(), 4),4));
			model.setSudahMenawarkan(String.valueOf(Integer.parseInt(model.getSudahMenawarkan()) + Integer.parseInt(service.getJumlahDashboardVendor(QueryUtil.jumlahDashboardVendor(headr.getVendorId(), 6).concat("and d.pqm_total_penawaran is not null"),5))));
			model.setNegosiasiPengadan(service.getJumlahNegosiasiVendor(QueryUtil.jumlahNegosiasi(headr.getVendorId(),1)));
			model.setEauction(service.getJumlahDashboardEauction(QueryUtil.jumlahEauction(headr.getVendorId())));
			model.setHistory(service.getJumlahDashboardHistory(QueryUtil.jumlahHistory(headr.getVendorId())));
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	//Sanggahan
	@Action(
			results={
					@Result(name=SUCCESS,location="vnd/proc/listPrcMainHeader.jsp")
			},value="vnd/sanggahan"
			)
	public String listSpph() throws Exception{
		try {
			TmpVndHeader headr = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
			VndHeader hdr = new VndHeader();
			hdr.setVendorId(headr.getVendorId());
			model.setListPrcMainHeader(toolsService.getListSanggahanPq(hdr));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="vnd/proc/ajax/sanggahan.jsp")
			},value="ajax/vnd/listSanggahan"
			)
	public String sanggahan() throws Exception{
		model.setPrcMainHeader(toolsService.getPrcMainHeader(model.getPrcMainHeader()));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="vnd/proc/ajax/listSanggahan.jsp")
			},value="ajax/vnd/addSanggahan"
			)
	public String listSanggahan() throws Exception{
		try {
			TmpVndHeader headTemp = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
			VndHeader head = toolsService.getVndHeader(headTemp) ;
			if(model.getMsgSanggahan() != null){
				System.out.println("lewat sini");
				PrcMsgSanggahan ms = model.getMsgSanggahan();
				ms.setVndHeader(head);
				ms.setMsgTo("PEMBELI");
				ms.setMsgFrom(head.getVendorName());
				boolean save = toolsService.saveSanggahan(ms);
				if (!save) {
					addActionError("Sanggahan gagal dikirim, mohon ulangi kembali");
				}else{
					PrcMainHeader prch = prcService.getPrcMainHeaderSPPHById(model.getPrcMainHeader().getPpmId());
					AdmUser usr = admService.getAdmUser(prch.getAdmUserByPpmBuyer());
					EmailSessionBean.sendMail("eProc PT Semen Baturaja (Persero)", "Vendor "
							+head.getVendorName()+" mengirim sanggahan pada PR NO."+prch.getPpmNomorPr()+
							" "+prch.getPpmSubject(), usr.getAdmEmployee().getEmpEmail());
				}
			}
			model.setListSanggahan(toolsService.getListSanggahan(model.getPrcMainHeader(), head));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="vnd/proc/viewDetailSpph.jsp")
			},value="ajax/proc/viewDetailSpph"
			)
	public String ajaxDetailSpph() throws Exception{
		PrcMainHeader prcMainHeader = prcService.getPrcMainHeaderSPPHById(model.getPrcMainHeader().getPpmId());
		model.setPrcMainHeader(prcMainHeader);
		List<PrcMainItem> listItem = prcService.listPrcMainItemByHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId()));
		model.setListPrcMainItem(listItem);
		model.setListPrcMainDoc(prcService.listPrcMainDocByHeader(prcMainHeader));
		model.setPrcMainPreparation(prcService.getPrcMainPreparationByHeader(prcMainHeader));
		return SUCCESS;
	}
	@Action(
			results={
					@Result(name=SUCCESS,location="vnd/proc/listDaftar.jsp"),
					@Result(name="view",location="vnd/proc/viewDaftar.jsp"),
					@Result(name="ok",location="vnd/daftarRfq",type="redirectAction")
			},value="vnd/daftarRfq"
			)
	public String vndDaftar() throws Exception{
		TmpVndHeader headr = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		try {
			if(WebUtils.hasSubmitParameter(request, "proses")){
				PrcMainHeader prcMainHeader = prcService.getPrcMainHeaderSPPHById(model.getPrcMainHeader().getPpmId());
				model.setPrcMainHeader(prcMainHeader);
				List<PrcMainItem> listItem = prcService.listPrcMainItemByHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId()));
				model.setListPrcMainItem(listItem);
				model.setListPrcMainDoc(prcService.listPrcMainDocByHeader(prcMainHeader));
				model.setPrcMainPreparation(prcService.getPrcMainPreparationByHeader(prcMainHeader));
				return "view";
			}else if(WebUtils.hasSubmitParameter(request, "daftar")){
				PrcMainVendor prcMainVendor = model.getPrcMainVendor();
				prcMainVendor.setPmpStatus(3);
				service.daftarPengadaan(prcMainVendor);
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			}else if(WebUtils.hasSubmitParameter(request, "tidakdaftar")){
				PrcMainVendor prcMainVendor = model.getPrcMainVendor();
				prcMainVendor.setPmpStatus(-3);
				service.daftarPengadaan(prcMainVendor);
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
			return "ok";
		}
		model.setListTodo(service.listTodoListDaftar(QueryUtil.todoListVendorSQL(headr.getVendorId(), 2)));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="vnd/proc/listPenawaran.jsp"),
					@Result(name="view",location="vnd/proc/viewPenawaran.jsp"),
					@Result(name="ok",location="vnd/penawaranRfq",type="redirectAction")
			},value="vnd/penawaranRfq"
			)
	public String vndPenawaran() throws Exception{
		TmpVndHeader headr = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		try {
			if(WebUtils.hasSubmitParameter(request, "proses")){
				PrcMainHeader prcMainHeader = prcService.getPrcMainHeaderSPPHById(model.getPrcMainHeader().getPpmId());
				model.setPrcMainHeader(prcMainHeader);
				List<PrcMainItem> listItem = prcService.listPrcMainItemByHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId()));
				model.setQuote(prcService.getQuoteByVendor(model.getPrcMainVendor()));
				model.setPrcMainVendor(prcService.getPrcMainVendorById(model.getPrcMainVendor().getId()));
				model.setListAdministrasi(prcService.listItemAdministrasi(model.getQuote(),prcMainHeader));
				model.setListTeknis(prcService.listItemTeknis(model.getQuote(),prcMainHeader));
				model.setListQuoteItem(prcService.listQuoteItem(model.getQuote(),listItem));
				model.setListCurrency(new AdmServiceImpl().listAdmCurr());
				return "view";
			}else if(WebUtils.hasSubmitParameter(request, "saveAndFinish")){
				if(model.getDocsPenawaran() != null && model.getDocsPenawaran().length() > 5097152){
					addActionError("Ukuran file terlalu besar dari batas minimal yang ditetapkan");
					return SUCCESS;
				}
				PrcMainVendorQuote quo = model.getQuote();
				if (model.getDocsPenawaran() != null) {
					quo.setPqmAttachment(UploadUtil.uploadDocsFile(model.getDocsPenawaran(), model.getDocsPenawaranFileName()));
				}
				
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DATE, -4);
				quo.setPqmValidTo(cal.getTime());
				
				if(model.getQuote().getPqmType().equalsIgnoreCase("A"))
					service.savePenawaran(model.getPrcMainVendor(),model.getPrcMainHeader(),quo,model.getListAdministrasi(),model.getListTeknis(),model.getListQuoteItem());
				else if(model.getQuote().getPqmType().equalsIgnoreCase("B"))
					service.savePenawaran(model.getPrcMainVendor(),model.getPrcMainHeader(),quo,model.getListAdministrasi(),model.getListTeknis(),model.getListQuoteItemB());
				else if(model.getQuote().getPqmType().equalsIgnoreCase("C"))
					service.savePenawaran(model.getPrcMainVendor(),model.getPrcMainHeader(),quo,model.getListAdministrasi(),model.getListTeknis(),model.getListQuoteItemC());
				addActionMessage(getText("notif.save"));
				return "ok";
			}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
			return "ok";
		}
		model.setListTodo(service.listTodoListPenawaran(QueryUtil.todoListVendorSQL(headr.getVendorId(), 3,4),QueryUtil.todoListVendorSQL(headr.getVendorId(), 3,4), QueryUtil.todoListVendorSQL(headr.getVendorId(), 6,3)));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name="view",location="vnd/proc/ajax/ubahPenawaran.jsp"),
					@Result(name="ok",location="vnd/negosiasi",type="redirectAction",params={"a998yajhsbzgfakjdkaskd","kmkjhdgtqrgsyaehcgdtyebhcmnashg","prcMainHeader.ppmId","%{prcMainHeader.ppmId}","prcMainVendor.id.ppmId","%{prcMainHeader.ppmId}","prcMainVendor.id.vendorId","%{prcMainVendor.id.vendorId}","gahgsdyt16253yutyqegaksdj76564ghqwryhg","124fgtr1092010837167ljhasgd41fcvavJhasnmnf13241fvkj"})
			},value="vnd/ubahPenawaranRfq"
			)
	public String UbahvndPenawaran() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "saveAndFinish")){
				if(model.getQuote().getPqmType().equalsIgnoreCase("A"))
					service.savePenawaranNego(model.getPrcMainVendor(),model.getPrcMainHeader(),model.getQuote(),model.getListQuoteItem());
				else if(model.getQuote().getPqmType().equalsIgnoreCase("B"))
					service.savePenawaranNego(model.getPrcMainVendor(),model.getPrcMainHeader(),model.getQuote(),model.getListQuoteItemB());
				else if(model.getQuote().getPqmType().equalsIgnoreCase("C"))
					service.savePenawaranNego(model.getPrcMainVendor(),model.getPrcMainHeader(),model.getQuote(),model.getListQuoteItemC());
				addActionMessage(getText("notif.save"));
				return "ok";
			}else {
				PrcMainHeader prcMainHeader = prcService.getPrcMainHeaderSPPHById(model.getPrcMainHeader().getPpmId());
				model.setPrcMainHeader(prcMainHeader);
				List<PrcMainItem> listItem = prcService.listPrcMainItemByHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId()));
				model.setQuote(prcService.getQuoteByVendor(model.getPrcMainVendor()));
				model.setListQuoteItem(prcService.listQuoteItem(model.getQuote(),listItem));
				return "view";
			}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
			return "ok";
		}

	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="vnd/proc/listNegosiasi.jsp"),
					@Result(name="view",location="vnd/proc/viewNegosiasi.jsp"),
					@Result(name="ok",location="vnd/negosiasi",type="redirectAction")
			},value="vnd/negosiasi"
			)
	public String negosiasi() throws Exception{
		TmpVndHeader headr = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		try {
			if(WebUtils.hasSubmitParameter(request, "proses")){
				PrcMainHeader prcMainHeader = prcService.getPrcMainHeaderSPPHById(model.getPrcMainHeader().getPpmId());
				model.setPrcMainHeader(prcMainHeader);
				model.setListNegosiasi(service.getListNegosiasiByVendor(model.getPrcMainVendor()));
				return "view";
			}else if(WebUtils.hasSubmitParameter(request, "saveAndFinish")){
				service.saveNegosiasi(model.getPrcMainVendor(),model.getNegosiasi());
				return "ok";
			}else if(request.getParameter("gahgsdyt16253yutyqegaksdj76564ghqwryhg")!=null){
				PrcMainHeader prcMainHeader = prcService.getPrcMainHeaderSPPHById(model.getPrcMainHeader().getPpmId());
				model.setPrcMainHeader(prcMainHeader);
				model.setListNegosiasi(service.getListNegosiasiByVendor(model.getPrcMainVendor()));
				return "view";
			}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
			return "ok";
		}
		model.setListTodo(service.listTodoListNego(QueryUtil.todoListVendorSQLNego(headr.getVendorId())));
		return SUCCESS;
	}
	
	/** lelang **/
	@Action(
			results={
					@Result(name=SUCCESS,location="vnd/proc/listLelang.jsp"),
					@Result(name="view",location="vnd/proc/viewLelang.jsp"),
					@Result(name="ok",location="vnd/daftarLelang",type="redirectAction")
			},value="vnd/daftarLelang",
			interceptorRefs={
					@InterceptorRef("loginInterceptorStack")
			}
			)
	public String lelang() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "proses")){
				PrcMainHeader prcMainHeader = prcService.getPrcMainHeaderSPPHById(model.getPrcMainHeader().getPpmId());
				model.setPrcMainHeader(prcMainHeader);
				List<PrcMainItem> listItem = prcService.listPrcMainItemByHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId()));
				model.setListPrcMainItem(listItem);
				model.setListPrcMainDoc(prcService.listPrcMainDocByHeader(prcMainHeader));
				model.setPrcMainPreparation(prcService.getPrcMainPreparationByHeader(prcMainHeader));
				return "view";
			}else if(WebUtils.hasSubmitParameter(request, "daftar")){
				service.daftarLelang(model.getPrcMainHeader(),model.getVndHeader());
				addActionMessage("Selamat Anda Terdaftar Dalam Pengadaan ini");
				return "ok";
			}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
			return "ok";
		}
		model.setListLelang(service.listLelang(QueryUtil.todoListLelangOpen()));
		return SUCCESS;
	}
	
	
	@Action(
			results={
					@Result(name=SUCCESS,location="vnd/proc/listEauction.jsp"),
					@Result(name="form",location="vnd/proc/viewEauction.jsp"),
					@Result(name="confirm",location="vnd/proc/eauctionConfirm.jsp"),
			},value="vnd/eauction"
			)
	public String formEauction() throws Exception{
		TmpVndHeader headr = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		try {
			if(WebUtils.hasSubmitParameter(request, "save")){
				Date now = new EauctionServiceImpl().getDatabaseTime();
				EauctionHeader eauctionHeader = eauctionService.eauctionHeaderByPpm(model.getPrcMainHeader());
				model.getEauctionVendor().setEauctionHeader(model.getEauctionHeader());
				if(now.before(eauctionHeader.getTanggalMulai())) {
					addActionError("Eauction Belum Dimulai");
				}else if(now.after(eauctionHeader.getTanggalBerakhir())) {
					addActionError("Waktu Eauction sudah habis");
				}else {
					service.savePenawaranEauction(model.getQuote(),model.getEauctionVendor(),model.getListQuoteItem());
				}
				
				//service.savePenawaranEauction(model.getQuote(),model.getEauctionVendor(),model.getListQuoteItem());
				model.setPrcMainHeader(new PrcServiceImpl().getPrcMainIHeaderById(model.getPrcMainHeader().getPpmId()));
				model.setEauctionHeader(eauctionHeader);
				model.setListQuoteItem(prcService.listQuoteItem(model.getQuote(),null));
				VndHeader vh = new VndHeader();
				vh.setVendorId(headr.getVendorId());
				model.setEauctionVendor(eauctionService.eauctionVendorByHeaderByVendorId(eauctionHeader,vh));
				return "form";
			}else if(WebUtils.hasSubmitParameter(request, "setuju")){
				EauctionHeader eauctionHeader = eauctionService.eauctionHeaderByPpm(model.getPrcMainHeader());
				model.setPrcMainHeader(new PrcServiceImpl().getPrcMainIHeaderById(model.getPrcMainHeader().getPpmId()));
				model.setEauctionHeader(eauctionHeader);
				PrcMainVendorQuote quote = new PrcMainVendorQuote();
				PrcMainVendorQuoteId id = new PrcMainVendorQuoteId(new BigDecimal(model.getPrcMainHeader().getPpmId()), headr.getVendorId());
				quote.setId(id);
				model.setQuote(quote);
				model.setListQuoteItem(prcService.listQuoteItem(quote,null));
				VndHeader vh = new VndHeader();
				vh.setVendorId(headr.getVendorId());
				model.setEauctionVendor(eauctionService.eauctionVendorByHeaderByVendorId(eauctionHeader,vh));
				return "form";
			}else if(WebUtils.hasSubmitParameter(request, "proses")) {
				model.setPrcMainHeader(new PrcServiceImpl().getPrcMainIHeaderById(model.getPrcMainHeader().getPpmId()));
				return "confirm";
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
			e.printStackTrace();
		}
		model.setListEauctionHeader(eauctionService.listEauctionHeaderByVendorActive(headr));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="vnd/proc/listDaftarHistory.jsp"),
					@Result(name="view",location="vnd/proc/viewDaftarHistory.jsp"),
					@Result(name="viewpenawaran",location="vnd/proc/viewPenawaran.jsp"),
					@Result(name="ok",location="vnd/daftarHistory",type="redirectAction")
			},value="vnd/daftarHistory"
			)
	public String vndDaftarHistory() throws Exception{
		TmpVndHeader headr = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		try {
			if(WebUtils.hasSubmitParameter(request, "proses")){
				PrcMainHeader prcMainHeader = prcService.getPrcMainHeaderSPPHById(model.getPrcMainHeader().getPpmId());
				model.setPrcMainHeader(prcMainHeader);
				List<PrcMainItem> listItem = prcService.listPrcMainItemByHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId()));
				model.setListPrcMainItem(listItem);
				model.setListPrcMainDoc(prcService.listPrcMainDocByHeader(prcMainHeader));
				model.setPrcMainPreparation(prcService.getPrcMainPreparationByHeader(prcMainHeader));
				return "view";
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
			return "ok";
		}
		model.setListTodo(service.listTodoListHistory(QueryUtil.historyPengadaan(headr.getVendorId())));
		model.setListKontrak(service.listTodoListContract(QueryUtil.historyPo(headr.getVendorId())));
		return SUCCESS;
	}

	@Action(
			results={
					@Result(name="viewpenawaran",location="vnd/proc/ajax/viewPenawaran.jsp")
			},value="ajax/viewPenawaranRead",
			interceptorRefs={
					@InterceptorRef("defaultInterceptorStack")
			}
			)
	public String ajaxViewPenawaran() throws Exception{
		PrcMainHeader prcMainHeader = prcService.getPrcMainHeaderSPPHById(model.getPrcMainHeader().getPpmId());
		model.setPrcMainHeader(prcMainHeader);
		List<PrcMainItem> listItem = prcService.listPrcMainItemByHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId()));
		model.setQuote(prcService.getQuoteByVendor(model.getPrcMainVendor()));
		model.setListAdministrasi(prcService.listItemAdministrasi(model.getQuote(),prcMainHeader));
		model.setListTeknis(prcService.listItemTeknis(model.getQuote(),prcMainHeader));
		model.setListQuoteItem(prcService.listQuoteItem(model.getQuote(),listItem));
		return "viewpenawaran";
	}
	
	@Action(
			results={
					@Result(name="profil",location="vnd/profil/viewProfile.jsp"),
					@Result(name="edit",location="vnd/companyInformation",type="redirectAction")
			},value="vnd/viewProfile"
			)
	public String ajaxViewProfil() throws Exception{
		System.out.println(">>> MASUK SINI >>>>");
		try {
			if(WebUtils.hasSubmitParameter(request, "edit")){
				TmpVndHeader headr = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
				service.editProfile(headr);
				return "edit";
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return "profil";
	}

}
