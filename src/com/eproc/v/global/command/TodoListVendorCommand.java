package com.eproc.v.global.command;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class TodoListVendorCommand implements Serializable {

	private Long ppmId;
	private Long vendorId;
	private String nomorRfq;
	private Date daftarBuka;
	private Date daftarTutup;
	private Date aanwijzingDate;
	private Date quotationDate;
	
	//history section
	private String status;
	private String administrasi;
	private String teknis;
	private String harga;
	
	//contract
	private Long contractId;
	private String contractNumber;
	private String contractAmount;
	private String nomorPembayaran;
	private String judulKontrak;
	private Date tglKontrakAwal;
	private Date tglKontrakAkhir;
	private String statusPembayaran;
	
	public TodoListVendorCommand() {
		// TODO Auto-generated constructor stub
	}

	public Long getPpmId() {
		return ppmId;
	}

	public void setPpmId(Long ppmId) {
		this.ppmId = ppmId;
	}

	public String getNomorRfq() {
		return nomorRfq;
	}

	public void setNomorRfq(String nomorRfq) {
		this.nomorRfq = nomorRfq;
	}

	public Date getDaftarBuka() {
		return daftarBuka;
	}

	public void setDaftarBuka(Date daftarBuka) {
		this.daftarBuka = daftarBuka;
	}

	public Date getDaftarTutup() {
		return daftarTutup;
	}

	public void setDaftarTutup(Date daftarTutup) {
		this.daftarTutup = daftarTutup;
	}

	public Date getAanwijzingDate() {
		return aanwijzingDate;
	}

	public void setAanwijzingDate(Date aanwijzingDate) {
		this.aanwijzingDate = aanwijzingDate;
	}

	public Date getQuotationDate() {
		return quotationDate;
	}

	public void setQuotationDate(Date quotationDate) {
		this.quotationDate = quotationDate;
	}

	public Long getVendorId() {
		return vendorId;
	}

	public void setVendorId(Long vendorId) {
		this.vendorId = vendorId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAdministrasi() {
		return administrasi;
	}

	public void setAdministrasi(String administrasi) {
		this.administrasi = administrasi;
	}

	public String getTeknis() {
		return teknis;
	}

	public void setTeknis(String teknis) {
		this.teknis = teknis;
	}

	public String getHarga() {
		return harga;
	}

	public void setHarga(String harga) {
		this.harga = harga;
	}

	public Long getContractId() {
		return contractId;
	}

	public void setContractId(Long contractId) {
		this.contractId = contractId;
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public String getContractAmount() {
		return contractAmount;
	}

	public void setContractAmount(String contractAmount) {
		this.contractAmount = contractAmount;
	}

	public String getNomorPembayaran() {
		return nomorPembayaran;
	}

	public void setNomorPembayaran(String nomorPembayaran) {
		this.nomorPembayaran = nomorPembayaran;
	}

	public String getJudulKontrak() {
		return judulKontrak;
	}

	public void setJudulKontrak(String judulKontrak) {
		this.judulKontrak = judulKontrak;
	}

	public Date getTglKontrakAwal() {
		return tglKontrakAwal;
	}

	public void setTglKontrakAwal(Date tglKontrakAwal) {
		this.tglKontrakAwal = tglKontrakAwal;
	}

	public Date getTglKontrakAkhir() {
		return tglKontrakAkhir;
	}

	public void setTglKontrakAkhir(Date tglKontrakAkhir) {
		this.tglKontrakAkhir = tglKontrakAkhir;
	}

	public String getStatusPembayaran() {
		return statusPembayaran;
	}

	public void setStatusPembayaran(String statusPembayaran) {
		this.statusPembayaran = statusPembayaran;
	}
	
}
