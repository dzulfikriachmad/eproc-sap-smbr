package com.eproc.v.global.command;

import java.io.File;
import java.io.Serializable;
import java.util.List;

import com.eproc.x.global.command.TodoUserCommand;
import com.orm.model.AdmCurrency;
import com.orm.model.EauctionHeader;
import com.orm.model.EauctionVendor;
import com.orm.model.PrcMainDoc;
import com.orm.model.PrcMainHeader;
import com.orm.model.PrcMainItem;
import com.orm.model.PrcMainPreparation;
import com.orm.model.PrcMainVendor;
import com.orm.model.PrcMainVendorQuote;
import com.orm.model.PrcMainVendorQuoteAdmtech;
import com.orm.model.PrcMainVendorQuoteItem;
import com.orm.model.PrcMsgNegotiation;
import com.orm.model.PrcMsgSanggahan;
import com.orm.model.TmpVndAddress;
import com.orm.model.TmpVndAgent;
import com.orm.model.TmpVndAkta;
import com.orm.model.TmpVndBank;
import com.orm.model.TmpVndBoard;
import com.orm.model.TmpVndCert;
import com.orm.model.TmpVndCompanyType;
import com.orm.model.TmpVndCv;
import com.orm.model.TmpVndDistrict;
import com.orm.model.TmpVndDomisili;
import com.orm.model.TmpVndEquip;
import com.orm.model.TmpVndFinRpt;
import com.orm.model.TmpVndHeader;
import com.orm.model.TmpVndIjin;
import com.orm.model.TmpVndProduct;
import com.orm.model.TmpVndSdm;
import com.orm.model.TmpVndTambahan;
import com.orm.model.VndHeader;

@SuppressWarnings("serial")
public class VndGlobalCommand implements Serializable {

	private String undangan;
	private String menungguPenawaran;
	private String sudahMenawarkan;
	private String negosiasiPengadan;
	private String eauction;
	private String history;
	private VndHeader vndHeader;
	
	private EauctionHeader eauctionHeader;
	private List<EauctionHeader> listEauctionHeader;
	private EauctionVendor eauctionVendor;
	
	private List<TodoListVendorCommand> listTodo;
	private List<TodoListVendorCommand> listKontrak;
	private PrcMainHeader prcMainHeader;
	private PrcMainVendor prcMainVendor;
	
	private List<PrcMainHeader> listPrcMainHeader;
	private List<PrcMainItem> listPrcMainItem;
	private List<PrcMainDoc> listPrcMainDoc;
	private PrcMainPreparation prcMainPreparation;
	
	private List<PrcMainVendorQuoteAdmtech> listAdministrasi;
	private List<PrcMainVendorQuoteAdmtech> listTeknis;
	private PrcMainVendorQuote quote;
	private List<PrcMainVendorQuoteItem> listQuoteItem;
	private List<PrcMainVendorQuoteItem> listQuoteItemB;
	private List<PrcMainVendorQuoteItem> listQuoteItemC;
	
	private List<TodoUserCommand> listLelang;
	
	private PrcMsgNegotiation negosiasi;
	private List<PrcMsgNegotiation> listNegosiasi;
	private List<PrcMsgSanggahan> listSanggahan;
	private PrcMsgSanggahan msgSanggahan;
	private List<AdmCurrency> listCurrency;
	
	private File docsPenawaran;
	private String docsPenawaranFileName;
	private String docsPenawaranContentType;

	public File getDocsPenawaran() {
		return docsPenawaran;
	}
	public void setDocsPenawaran(File docsPenawaran) {
		this.docsPenawaran = docsPenawaran;
	}
	public String getDocsPenawaranFileName() {
		return docsPenawaranFileName;
	}
	public void setDocsPenawaranFileName(String docsPenawaranFileName) {
		this.docsPenawaranFileName = docsPenawaranFileName;
	}
	public String getDocsPenawaranContentType() {
		return docsPenawaranContentType;
	}
	public void setDocsPenawaranContentType(String docsPenawaranContentType) {
		this.docsPenawaranContentType = docsPenawaranContentType;
	}
	public PrcMsgSanggahan getMsgSanggahan() {
		return msgSanggahan;
	}
	public void setMsgSanggahan(PrcMsgSanggahan msgSanggahan) {
		this.msgSanggahan = msgSanggahan;
	}
	public List<PrcMsgSanggahan> getListSanggahan() {
		return listSanggahan;
	}
	public void setListSanggahan(List<PrcMsgSanggahan> listSanggahan) {
		this.listSanggahan = listSanggahan;
	}
	public String getUndangan() {
		return undangan;
	}
	public void setUndangan(String undangan) {
		this.undangan = undangan;
	}
	public String getMenungguPenawaran() {
		return menungguPenawaran;
	}
	public void setMenungguPenawaran(String menungguPenawaran) {
		this.menungguPenawaran = menungguPenawaran;
	}
	public String getSudahMenawarkan() {
		return sudahMenawarkan;
	}
	public void setSudahMenawarkan(String sudahMenawarkan) {
		this.sudahMenawarkan = sudahMenawarkan;
	}
	public List<TodoListVendorCommand> getListTodo() {
		return listTodo;
	}
	public void setListTodo(List<TodoListVendorCommand> listTodo) {
		this.listTodo = listTodo;
	}
	public PrcMainHeader getPrcMainHeader() {
		return prcMainHeader;
	}
	public void setPrcMainHeader(PrcMainHeader prcMainHeader) {
		this.prcMainHeader = prcMainHeader;
	}
	public PrcMainVendor getPrcMainVendor() {
		return prcMainVendor;
	}
	public void setPrcMainVendor(PrcMainVendor prcMainVendor) {
		this.prcMainVendor = prcMainVendor;
	}
	public List<PrcMainHeader> getListPrcMainHeader() {
		return listPrcMainHeader;
	}
	public void setListPrcMainHeader(List<PrcMainHeader> listPrcMainHeader) {
		this.listPrcMainHeader = listPrcMainHeader;
	}
	public List<PrcMainItem> getListPrcMainItem() {
		return listPrcMainItem;
	}
	public void setListPrcMainItem(List<PrcMainItem> listPrcMainItem) {
		this.listPrcMainItem = listPrcMainItem;
	}
	public List<PrcMainDoc> getListPrcMainDoc() {
		return listPrcMainDoc;
	}
	public void setListPrcMainDoc(List<PrcMainDoc> listPrcMainDoc) {
		this.listPrcMainDoc = listPrcMainDoc;
	}
	public PrcMainPreparation getPrcMainPreparation() {
		return prcMainPreparation;
	}
	public void setPrcMainPreparation(PrcMainPreparation prcMainPreparation) {
		this.prcMainPreparation = prcMainPreparation;
	}
	public List<PrcMainVendorQuoteAdmtech> getListAdministrasi() {
		return listAdministrasi;
	}
	public void setListAdministrasi(List<PrcMainVendorQuoteAdmtech> listAdministrasi) {
		this.listAdministrasi = listAdministrasi;
	}
	public List<PrcMainVendorQuoteAdmtech> getListTeknis() {
		return listTeknis;
	}
	public void setListTeknis(List<PrcMainVendorQuoteAdmtech> listTeknis) {
		this.listTeknis = listTeknis;
	}
	public PrcMainVendorQuote getQuote() {
		return quote;
	}
	public void setQuote(PrcMainVendorQuote quote) {
		this.quote = quote;
	}
	public List<PrcMainVendorQuoteItem> getListQuoteItem() {
		return listQuoteItem;
	}
	public void setListQuoteItem(List<PrcMainVendorQuoteItem> listQuoteItem) {
		this.listQuoteItem = listQuoteItem;
	}
	public List<PrcMainVendorQuoteItem> getListQuoteItemB() {
		return listQuoteItemB;
	}
	public void setListQuoteItemB(List<PrcMainVendorQuoteItem> listQuoteItemB) {
		this.listQuoteItemB = listQuoteItemB;
	}
	public List<PrcMainVendorQuoteItem> getListQuoteItemC() {
		return listQuoteItemC;
	}
	public void setListQuoteItemC(List<PrcMainVendorQuoteItem> listQuoteItemC) {
		this.listQuoteItemC = listQuoteItemC;
	}
	
	public void setNegosiasiPengadan(String negosiasiPengadan) {
		this.negosiasiPengadan = negosiasiPengadan;
	}
	public List<PrcMsgNegotiation> getListNegosiasi() {
		return listNegosiasi;
	}
	public void setListNegosiasi(List<PrcMsgNegotiation> listNegosiasi) {
		this.listNegosiasi = listNegosiasi;
	}
	public void setNegosiasi(PrcMsgNegotiation negosiasi) {
		this.negosiasi = negosiasi;
	}
	public String getNegosiasiPengadan() {
		return negosiasiPengadan;
	}
	public PrcMsgNegotiation getNegosiasi() {
		return negosiasi;
	}
	public List<TodoUserCommand> getListLelang() {
		return listLelang;
	}
	public void setListLelang(List<TodoUserCommand> listLelang) {
		this.listLelang = listLelang;
	}
	public VndHeader getVndHeader() {
		return vndHeader;
	}
	public void setVndHeader(VndHeader vndHeader) {
		this.vndHeader = vndHeader;
	}
	public EauctionHeader getEauctionHeader() {
		return eauctionHeader;
	}
	public void setEauctionHeader(EauctionHeader eauctionHeader) {
		this.eauctionHeader = eauctionHeader;
	}
	public List<EauctionHeader> getListEauctionHeader() {
		return listEauctionHeader;
	}
	public void setListEauctionHeader(List<EauctionHeader> listEauctionHeader) {
		this.listEauctionHeader = listEauctionHeader;
	}
	public EauctionVendor getEauctionVendor() {
		return eauctionVendor;
	}
	public void setEauctionVendor(EauctionVendor eauctionVendor) {
		this.eauctionVendor = eauctionVendor;
	}
	public String getEauction() {
		return eauction;
	}
	public void setEauction(String eauction) {
		this.eauction = eauction;
	}
	public String getHistory() {
		return history;
	}
	public void setHistory(String history) {
		this.history = history;
	}
	public List<TodoListVendorCommand> getListKontrak() {
		return listKontrak;
	}
	public void setListKontrak(List<TodoListVendorCommand> listKontrak) {
		this.listKontrak = listKontrak;
	}
	public List<AdmCurrency> getListCurrency() {
		return listCurrency;
	}
	public void setListCurrency(List<AdmCurrency> listCurrency) {
		this.listCurrency = listCurrency;
	}
}
