package com.eproc.v.reg.command;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.orm.model.AdmCountry;
import com.orm.model.TmpVndAkta;
import com.orm.model.TmpVndDomisili;
import com.orm.model.TmpVndHeader;
import com.orm.model.TmpVndIjin;

public class LegalInformationCommand implements Serializable {
	private TmpVndHeader tmpVndHeader;
	private TmpVndAkta tmpVndAkta;
	private List<TmpVndAkta> listTmpVndAkta;
	
	private TmpVndDomisili tmpVndDom;
	private List<TmpVndDomisili> listTmpVndDom;
	
	private Map<Integer, String> listAktaType;
	private Map<String, String> listPkp;
	private List<AdmCountry> listAdmCountry;
	
	private List<TmpVndIjin> listIjin;
	private TmpVndIjin ijin;

	private int pageId;
	
	public TmpVndHeader getTmpVndHeader() {
		return tmpVndHeader;
	}
	
	public void setTmpVndHeader(TmpVndHeader tmpVndHeader) {
		this.tmpVndHeader = tmpVndHeader;
	}
	
	public TmpVndAkta getTmpVndAkta() {
		return tmpVndAkta;
	}
	
	public void setTmpVndAkta(TmpVndAkta tmpVndAkta) {
		this.tmpVndAkta = tmpVndAkta;
	}
	
	public List<TmpVndAkta> getListTmpVndAkta() {
		return listTmpVndAkta;
	}
	
	public void setListTmpVndAkta(List<TmpVndAkta> listTmpVndAkta) {
		this.listTmpVndAkta = listTmpVndAkta;
	}

	public Map<Integer, String> getListAktaType() {
		if(listAktaType == null){
			Map<Integer, String> ta = new TreeMap<Integer, String>();
			ta.put(1, "PENDIRIAN (ESTABLISHMENT)");
			ta.put(2, "PERUBAHAN (CHANGES)");
			listAktaType = ta;
		}
		return listAktaType;
	}

	public void setListAktaType(Map<Integer, String> listAktaType) {
		this.listAktaType = listAktaType;
	}

	public TmpVndDomisili getTmpVndDom() {
		return tmpVndDom;
	}

	public void setTmpVndDom(TmpVndDomisili tmpVndDom) {
		this.tmpVndDom = tmpVndDom;
	}

	public List<TmpVndDomisili> getListTmpVndDom() {
		return listTmpVndDom;
	}

	public void setListTmpVndDom(List<TmpVndDomisili> listTmpVndDom) {
		this.listTmpVndDom = listTmpVndDom;
	}

	public List<AdmCountry> getListAdmCountry() {
		return listAdmCountry;
	}

	public void setListAdmCountry(List<AdmCountry> listAdmCountry) {
		this.listAdmCountry = listAdmCountry;
	}

	public int getPageId() {
		return pageId;
	}

	public void setPageId(int pageId) {
		this.pageId = pageId;
	}

	public Map<String, String> getListPkp() {
		return listPkp;
	}

	public void setListPkp(Map<String, String> listPkp) {
		this.listPkp = listPkp;
	}

	public List<TmpVndIjin> getListIjin() {
		return listIjin;
	}

	public void setListIjin(List<TmpVndIjin> listIjin) {
		this.listIjin = listIjin;
	}

	public TmpVndIjin getIjin() {
		return ijin;
	}

	public void setIjin(TmpVndIjin ijin) {
		this.ijin = ijin;
	}
	
}
