package com.eproc.v.reg.command;

import java.io.Serializable;
import java.util.List;

import com.orm.model.AdmDistrict;
import com.orm.model.ComCatalog;
import com.orm.model.ComGroup;
import com.orm.model.TmpVndHeader;
import com.orm.model.TmpVndProduct;

public class ComWorkInformationCommand implements Serializable {
	private TmpVndHeader tmpVndHeader;
	private TmpVndProduct tmpVndProduct;
	
	private List<TmpVndProduct> listTmpVndProduct;
	private List<AdmDistrict> listAdmDistrict;
	
	private int pageId;
	private List<ComCatalog> listComCatalog;
	private List<ComGroup> listComGroup;
	private Integer [] comDistrict;
	
	public TmpVndHeader getTmpVndHeader() {
		return tmpVndHeader;
	}
	public void setTmpVndHeader(TmpVndHeader tmpVndHeader) {
		this.tmpVndHeader = tmpVndHeader;
	}
	public TmpVndProduct getTmpVndProduct() {
		return tmpVndProduct;
	}
	public void setTmpVndProduct(TmpVndProduct tmpVndProduct) {
		this.tmpVndProduct = tmpVndProduct;
	}
	public List<TmpVndProduct> getListTmpVndProduct() {
		return listTmpVndProduct;
	}
	public void setListTmpVndProduct(List<TmpVndProduct> listTmpVndProduct) {
		this.listTmpVndProduct = listTmpVndProduct;
	}
	public int getPageId() {
		return pageId;
	}
	public void setPageId(int pageId) {
		this.pageId = pageId;
	}
	public List<ComCatalog> getListComCatalog() {
		return listComCatalog;
	}
	public void setListComCatalog(List<ComCatalog> listComCatalog) {
		this.listComCatalog = listComCatalog;
	}
	public List<AdmDistrict> getListAdmDistrict() {
		return listAdmDistrict;
	}
	public void setListAdmDistrict(List<AdmDistrict> listAdmDistrict) {
		this.listAdmDistrict = listAdmDistrict;
	}
	public Integer[] getComDistrict() {
		return comDistrict;
	}
	public void setComDistrict(Integer[] comDistrict) {
		this.comDistrict = comDistrict;
	}
	public List<ComGroup> getListComGroup() {
		return listComGroup;
	}
	public void setListComGroup(List<ComGroup> listComGroup) {
		this.listComGroup = listComGroup;
	}
	
}
