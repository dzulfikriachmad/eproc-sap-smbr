package com.eproc.v.reg.command;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.orm.model.TmpVndEquip;
import com.orm.model.TmpVndHeader;

public class CompanyFacilityCommand implements Serializable {
	private TmpVndHeader tmpVndHeader;
	private TmpVndEquip tmpVndEquip;
	private List<TmpVndEquip> listTmpVndEquip;
	
	private int pageId;
	private Map<Integer, String> listEquipType;

	public TmpVndHeader getTmpVndHeader() {
		return tmpVndHeader;
	}

	public void setTmpVndHeader(TmpVndHeader tmpVndHeader) {
		this.tmpVndHeader = tmpVndHeader;
	}

	public TmpVndEquip getTmpVndEquip() {
		return tmpVndEquip;
	}

	public void setTmpVndEquip(TmpVndEquip tmpVndEquip) {
		this.tmpVndEquip = tmpVndEquip;
	}

	public List<TmpVndEquip> getListTmpVndEquip() {
		return listTmpVndEquip;
	}

	public void setListTmpVndEquip(List<TmpVndEquip> listTmpVndEquip) {
		this.listTmpVndEquip = listTmpVndEquip;
	}

	public int getPageId() {
		return pageId;
	}

	public void setPageId(int pageId) {
		this.pageId = pageId;
	}

	public Map<Integer, String> getListEquipType() {
		return listEquipType;
	}

	public void setListEquipType(Map<Integer, String> listEquipType) {
		this.listEquipType = listEquipType;
	}
	
}
