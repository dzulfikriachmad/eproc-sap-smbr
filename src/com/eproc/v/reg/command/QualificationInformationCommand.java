package com.eproc.v.reg.command;

import java.io.Serializable;
import java.util.List;

import com.orm.model.TmpVndCert;
import com.orm.model.TmpVndHeader;

public class QualificationInformationCommand implements Serializable {
	private TmpVndHeader tmpVndHeader;
	private TmpVndCert tmpVndCert;
	private List<TmpVndCert> listTmpVndCert;
	
	private int pageId;

	public TmpVndHeader getTmpVndHeader() {
		return tmpVndHeader;
	}

	public void setTmpVndHeader(TmpVndHeader tmpVndHeader) {
		this.tmpVndHeader = tmpVndHeader;
	}

	public TmpVndCert getTmpVndCert() {
		return tmpVndCert;
	}

	public void setTmpVndCert(TmpVndCert tmpVndCert) {
		this.tmpVndCert = tmpVndCert;
	}

	public List<TmpVndCert> getListTmpVndCert() {
		return listTmpVndCert;
	}

	public void setListTmpVndCert(List<TmpVndCert> listTmpVndCert) {
		this.listTmpVndCert = listTmpVndCert;
	}

	public int getPageId() {
		return pageId;
	}

	public void setPageId(int pageId) {
		this.pageId = pageId;
	}
	
	
}
