package com.eproc.v.reg.command;

import java.io.Serializable;
import java.util.List;

import com.orm.model.AdmCurrency;
import com.orm.model.TmpVndBank;
import com.orm.model.TmpVndFinRpt;
import com.orm.model.TmpVndHeader;

public class FinanceInformationCommand implements Serializable {
	private TmpVndHeader tmpVndHeader;
	private TmpVndBank tmpVndBank;
	private TmpVndFinRpt tmpVndFinRpt;
	
	private List<TmpVndBank> listTmpVndBank;
	private List<TmpVndFinRpt> listTmpVndFinRpt;
	
	private int pageId;
	private List<AdmCurrency> listAdmCurr;

	public TmpVndHeader getTmpVndHeader() {
		return tmpVndHeader;
	}

	public void setTmpVndHeader(TmpVndHeader tmpVndHeader) {
		this.tmpVndHeader = tmpVndHeader;
	}

	public TmpVndBank getTmpVndBank() {
		return tmpVndBank;
	}

	public void setTmpVndBank(TmpVndBank tmpVndBank) {
		this.tmpVndBank = tmpVndBank;
	}

	public TmpVndFinRpt getTmpVndFinRpt() {
		return tmpVndFinRpt;
	}

	public void setTmpVndFinRpt(TmpVndFinRpt tmpVndFinRpt) {
		this.tmpVndFinRpt = tmpVndFinRpt;
	}

	public List<TmpVndBank> getListTmpVndBank() {
		return listTmpVndBank;
	}

	public void setListTmpVndBank(List<TmpVndBank> listTmpVndBank) {
		this.listTmpVndBank = listTmpVndBank;
	}

	public List<TmpVndFinRpt> getListTmpVndFinRpt() {
		return listTmpVndFinRpt;
	}

	public void setListTmpVndFinRpt(List<TmpVndFinRpt> listTmpVndFinRpt) {
		this.listTmpVndFinRpt = listTmpVndFinRpt;
	}

	public int getPageId() {
		return pageId;
	}

	public void setPageId(int pageId) {
		this.pageId = pageId;
	}

	public List<AdmCurrency> getListAdmCurr() {
		return listAdmCurr;
	}

	public void setListAdmCurr(List<AdmCurrency> listAdmCurr) {
		this.listAdmCurr = listAdmCurr;
	}
}
