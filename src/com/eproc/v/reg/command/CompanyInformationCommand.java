package com.eproc.v.reg.command;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.orm.model.AdmAddType;
import com.orm.model.AdmCompanyType;
import com.orm.model.AdmCountry;
import com.orm.model.AdmDistrict;
import com.orm.model.TmpVndAddress;
import com.orm.model.TmpVndHeader;
import com.orm.model.TmpVndTambahan;

public class CompanyInformationCommand implements Serializable {
	private TmpVndHeader vndHeader;
	private AdmCompanyType admCompanyType;
	private TmpVndAddress tmpVndAdd;
	private List<AdmDistrict> listKantor;

	private List<TmpVndAddress> listTmpVndAddress;
	private List<AdmCompanyType> listCompanyType;
	private List<AdmAddType> listAddType;
	private List<AdmCountry> listAdmCountry;
	private List<TmpVndTambahan> listTmpVndTambahan;
	private TmpVndTambahan tmpVndTambahan;
	private Map<String, String> listTipeAfiliasi;
	
	private Integer [] companyType;
	private int pageId;
	private String status;
	private String statusText;
	private String oldPassword;
	
	
	
	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public Integer[] getCompanyType() {
		return companyType;
	}

	public void setCompanyType(Integer[] companyType) {
		this.companyType = companyType;
	}

	private String lastUrl;

	public TmpVndHeader getVndHeader() {
		return vndHeader;
	}

	public void setVndHeader(TmpVndHeader vndHeader) {
		this.vndHeader = vndHeader;
	}
	
	public AdmCompanyType getAdmCompanyType() {
		return admCompanyType;
	}

	public void setAdmCompanyType(AdmCompanyType admCompanyType) {
		this.admCompanyType = admCompanyType;
	}

	public List<TmpVndAddress> getListTmpVndAddress() {
		return listTmpVndAddress;
	}

	public void setListTmpVndAddress(List<TmpVndAddress> listTmpVndAddress) {
		this.listTmpVndAddress = listTmpVndAddress;
	}

	public String getLastUrl() {
		return lastUrl;
	}

	public void setLastUrl(String lastUrl) {
		this.lastUrl = lastUrl;
	}

	public List<AdmCompanyType> getListCompanyType() {
		return listCompanyType;
	}

	public void setListCompanyType(List<AdmCompanyType> listCompanyType) {
		this.listCompanyType = listCompanyType;
	}

	public List<AdmAddType> getListAddType() {
		return listAddType;
	}

	public void setListAddType(List<AdmAddType> listAddType) {
		this.listAddType = listAddType;
	}

	public TmpVndAddress getTmpVndAdd() {
		return tmpVndAdd;
	}

	public void setTmpVndAdd(TmpVndAddress tmpVndAdd) {
		this.tmpVndAdd = tmpVndAdd;
	}

	public List<AdmCountry> getListAdmCountry() {
		return listAdmCountry;
	}

	public void setListAdmCountry(List<AdmCountry> listAdmCountry) {
		this.listAdmCountry = listAdmCountry;
	}

	public int getPageId() {
		return pageId;
	}

	public void setPageId(int pageId) {
		this.pageId = pageId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusText() {
		return statusText;
	}

	public void setStatusText(String statusText) {
		this.statusText = statusText;
	}

	public List<AdmDistrict> getListKantor() {
		return listKantor;
	}

	public void setListKantor(List<AdmDistrict> listKantor) {
		this.listKantor = listKantor;
	}

	public List<TmpVndTambahan> getListTmpVndTambahan() {
		return listTmpVndTambahan;
	}

	public void setListTmpVndTambahan(List<TmpVndTambahan> listTmpVndTambahan) {
		this.listTmpVndTambahan = listTmpVndTambahan;
	}

	public TmpVndTambahan getTmpVndTambahan() {
		return tmpVndTambahan;
	}

	public void setTmpVndTambahan(TmpVndTambahan tmpVndTambahan) {
		this.tmpVndTambahan = tmpVndTambahan;
	}

	public Map<String, String> getListTipeAfiliasi() {
		if(listTipeAfiliasi==null){
			listTipeAfiliasi = new HashMap<String, String>();
			listTipeAfiliasi.put("PRINCIPAL", "PRINCIPAL");
			listTipeAfiliasi.put("SUBKONTRAKTOR", "SUBKONTRAKTOR");
			listTipeAfiliasi.put("LAINNYA", "LAINNYA");
		}
		return listTipeAfiliasi;
	}
	
}
