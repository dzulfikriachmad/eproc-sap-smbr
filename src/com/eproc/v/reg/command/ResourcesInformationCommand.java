package com.eproc.v.reg.command;

import java.io.Serializable;
import java.util.List;

import com.orm.model.TmpVndHeader;
import com.orm.model.TmpVndSdm;

public class ResourcesInformationCommand implements Serializable {
	private TmpVndHeader tmpVndHeader;
	private TmpVndSdm tmpVndSdm;
	private List<TmpVndSdm> listTmpVndSdm;
	
	private int pageId;

	public TmpVndHeader getTmpVndHeader() {
		return tmpVndHeader;
	}

	public void setTmpVndHeader(TmpVndHeader tmpVndHeader) {
		this.tmpVndHeader = tmpVndHeader;
	}

	public TmpVndSdm getTmpVndSdm() {
		return tmpVndSdm;
	}

	public void setTmpVndSdm(TmpVndSdm tmpVndSdm) {
		this.tmpVndSdm = tmpVndSdm;
	}

	public List<TmpVndSdm> getListTmpVndSdm() {
		return listTmpVndSdm;
	}

	public void setListTmpVndSdm(List<TmpVndSdm> listTmpVndSdm) {
		this.listTmpVndSdm = listTmpVndSdm;
	}

	public int getPageId() {
		return pageId;
	}

	public void setPageId(int pageId) {
		this.pageId = pageId;
	}
	
}
