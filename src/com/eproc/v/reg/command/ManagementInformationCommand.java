package com.eproc.v.reg.command;

import java.io.Serializable;
import java.util.List;

import com.orm.model.AdmCountry;
import com.orm.model.TmpVndBoard;
import com.orm.model.TmpVndHeader;

public class ManagementInformationCommand implements Serializable {
	private TmpVndHeader tmpVndHeader;
	private TmpVndBoard tmpVndBoard;
	private List<TmpVndBoard> listTmpVndBoardDireksi;
	private List<TmpVndBoard> listTmpVndBoardKomisaris;
	
	private List<AdmCountry> listAdmCountry;
	private int pageId;

	public TmpVndHeader getTmpVndHeader() {
		return tmpVndHeader;
	}

	public void setTmpVndHeader(TmpVndHeader tmpVndHeader) {
		this.tmpVndHeader = tmpVndHeader;
	}

	public TmpVndBoard getTmpVndBoard() {
		return tmpVndBoard;
	}

	public void setTmpVndBoard(TmpVndBoard tmpVndBoard) {
		this.tmpVndBoard = tmpVndBoard;
	}

	public int getPageId() {
		return pageId;
	}

	public void setPageId(int pageId) {
		this.pageId = pageId;
	}

	public List<TmpVndBoard> getListTmpVndBoardDireksi() {
		return listTmpVndBoardDireksi;
	}

	public void setListTmpVndBoardDireksi(List<TmpVndBoard> listTmpVndBoardDireksi) {
		this.listTmpVndBoardDireksi = listTmpVndBoardDireksi;
	}

	public List<TmpVndBoard> getListTmpVndBoardKomisaris() {
		return listTmpVndBoardKomisaris;
	}

	public void setListTmpVndBoardKomisaris(
			List<TmpVndBoard> listTmpVndBoardKomisaris) {
		this.listTmpVndBoardKomisaris = listTmpVndBoardKomisaris;
	}

	public List<AdmCountry> getListAdmCountry() {
		return listAdmCountry;
	}

	public void setListAdmCountry(List<AdmCountry> listAdmCountry) {
		this.listAdmCountry = listAdmCountry;
	}
	
}
