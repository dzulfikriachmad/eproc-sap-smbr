package com.eproc.v.reg.command;

import java.io.Serializable;
import java.util.List;

import com.orm.model.AdmCurrency;
import com.orm.model.TmpVndCv;
import com.orm.model.TmpVndHeader;

public class ProjectExperienceCommand implements Serializable {
	private TmpVndHeader tmpVndHeader;
	private TmpVndCv tmpVndCv;
	private List<TmpVndCv> listTmpVndCv;
	
	private int pageId;
	private List<AdmCurrency> listAdmCurrency;
	public TmpVndHeader getTmpVndHeader() {
		return tmpVndHeader;
	}
	public void setTmpVndHeader(TmpVndHeader tmpVndHeader) {
		this.tmpVndHeader = tmpVndHeader;
	}
	public TmpVndCv getTmpVndCv() {
		return tmpVndCv;
	}
	public void setTmpVndCv(TmpVndCv tmpVndCv) {
		this.tmpVndCv = tmpVndCv;
	}
	public List<TmpVndCv> getListTmpVndCv() {
		return listTmpVndCv;
	}
	public void setListTmpVndCv(List<TmpVndCv> listTmpVndCv) {
		this.listTmpVndCv = listTmpVndCv;
	}
	public int getPageId() {
		return pageId;
	}
	public void setPageId(int pageId) {
		this.pageId = pageId;
	}
	public List<AdmCurrency> getListAdmCurrency() {
		return listAdmCurrency;
	}
	public void setListAdmCurrency(List<AdmCurrency> listAdmCurrency) {
		this.listAdmCurrency = listAdmCurrency;
	}
}
