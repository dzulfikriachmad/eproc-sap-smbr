package com.eproc.v.reg.command;

import java.io.Serializable;

import com.orm.model.TmpVndHeader;

public class AuthCommand implements Serializable {
	private TmpVndHeader vndHeader;
	
	private String lastUrl;

	public TmpVndHeader getVndHeader() {
		return vndHeader;
	}

	public void setVndHeader(TmpVndHeader vndHeader) {
		this.vndHeader = vndHeader;
	}

	public String getLastUrl() {
		return lastUrl;
	}

	public void setLastUrl(String lastUrl) {
		this.lastUrl = lastUrl;
	}
}
