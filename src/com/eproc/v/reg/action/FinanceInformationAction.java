package com.eproc.v.reg.action;

import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.web.util.WebUtils;

import com.eproc.core.BaseController;
import com.eproc.utils.SessionGetter;
import com.eproc.v.reg.command.FinanceInformationCommand;
import com.orm.model.TmpVndBank;
import com.orm.model.TmpVndFinRpt;
import com.orm.model.TmpVndHeader;

import eproc.controller.AdmService;
import eproc.controller.AdmServiceImpl;
import eproc.controller.VndAuthService;
import eproc.controller.VndAuthServiceImpl;
import eproc.controller.VndRegService;
import eproc.controller.VndRegServiceImpl;

@ParentPackage("eprocVnd")
@Namespace("/")
public class FinanceInformationAction extends BaseController {
	private FinanceInformationCommand model;
	private VndAuthService authService;
	private VndRegService regService;
	private AdmService admService;

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return model;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		model = new FinanceInformationCommand();
		authService = new VndAuthServiceImpl();
		regService = new VndRegServiceImpl();
		admService = new AdmServiceImpl();
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/reg/financeInformation/finInf.jsp"),
					@Result(name=ERROR, location="vnd/reg/financeInformation/finInf.jsp"),
					@Result(name="restricted", location="vnd/reg/restrictedPage.jsp"),
					@Result(name="next", location="vnd/qualificationInformation", type="redirectAction"),
			},value="vnd/companyFinanceInformation"
	)
	public String companyFinanceInformation() throws Exception{
		TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		TmpVndHeader tmp = authService.getTmpVndHeaderById(vndHeader.getVendorId());
		List<TmpVndBank> bank = regService.listTmpVndBank(tmp);
		List<TmpVndFinRpt> finRpt = regService.listTmpVndFinRpt(tmp);
		
		if(tmp.getVendorNextPage() >= 4){
			model.setPageId(tmp.getVendorNextPage());
			
			if(WebUtils.hasSubmitParameter(request, "save") || WebUtils.hasSubmitParameter(request, "saveAndNext")){
				tmp.setVendorBaseCap(model.getTmpVndHeader().getVendorBaseCap());
				tmp.setVendorInCap(model.getTmpVndHeader().getVendorInCap());
				tmp.setAdmCurrencyByVendorCurrBaseCap(model.getTmpVndHeader().getAdmCurrencyByVendorCurrBaseCap());
				tmp.setAdmCurrencyByVendorCurrInCap(model.getTmpVndHeader().getAdmCurrencyByVendorCurrInCap());
				
				//Klasifikasi Perusahaan
				tmp.setVendorFinClass(model.getTmpVndHeader().getVendorFinClass());
				tmp.setVendorFinClass1(model.getTmpVndHeader().getVendorFinClass1());
				tmp.setVendorFinClass2(model.getTmpVndHeader().getVendorFinClass2());
				
				boolean cek = authService.saveOrUpdateTmpVndHeader(tmp);
				if(cek){
					addActionMessage(getText("eproc.alert.success"));
				}else{
					addActionError(getText("eproc.alert.failed"));
					
					model.setTmpVndHeader(tmp);
					model.setListTmpVndBank(bank);
					model.setListTmpVndFinRpt(finRpt);
					model.setListAdmCurr(admService.listAdmCurr());
					return ERROR;
				}
				
				if(WebUtils.hasSubmitParameter(request, "saveAndNext")){
					if(bank.size() == 0){
						clearMessages();
						addActionError(getText("fin.inf.bank.minimal"));
					}else if(finRpt.size() == 0){
						clearMessages();
						addActionError(getText("fin.inf.financial.minimal"));
					}else{
						if(tmp.getVendorNextPage() == 4)regService.saveNextPage(tmp.getVendorNextPage()+1, tmp);
						return "next";
					}	
				}
			}
			
			model.setTmpVndHeader(tmp);
			model.setListTmpVndBank(bank);
			model.setListTmpVndFinRpt(finRpt);
			model.setListAdmCurr(admService.listAdmCurr());
			return SUCCESS;
		}else{
			return "restricted";
		}
	}
	
	//-=---------------------REKENING BANK---------------------------------------------------------//
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/reg/financeInformation/listBank.jsp")
			},value="ajax/listTmpVndBank"
	)
	public String listTmpVndBank() throws Exception{
		TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		TmpVndHeader tmp = authService.getTmpVndHeaderById(vndHeader.getVendorId());
		model.setListTmpVndBank(regService.listTmpVndBank(tmp));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/reg/financeInformation/setBank.jsp"),
					@Result(name=ERROR, location="error/wrong", type="redirectAction"),
					@Result(name="delete", location="ajax/listTmpVndBank", type="redirectAction"),
					@Result(name="edit", location="vnd/reg/financeInformation/setBank.jsp")
			},value="ajax/setTmpVndBank"
	)
	public String setTmpVndBank() throws Exception{
		TmpVndHeader tmpVndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
				
		if(request.getParameter("id") != null && request.getParameter("id") != "" && request.getParameter("act") != null && request.getParameter("act").equals("edit")){
			int id = Integer.parseInt(request.getParameter("id"));
			model.setTmpVndBank(regService.getTmpVndBankById(id, tmpVndHeader));
			return "edit";
			
		}else if (request.getParameter("id") != null && request.getParameter("id") != "" && request.getParameter("act") != null && request.getParameter("act").equals("delete")) {
			int id = Integer.parseInt(request.getParameter("id"));
			boolean cek = regService.deleteTmpVndBank(regService.getTmpVndBankById(id, tmpVndHeader));
			if(cek){
				addActionMessage(getText("eproc.alert.delete"));
				return "delete";
			}else{
				addActionError(getText("eproc.alert.deletefail"));
				return ERROR;
			}
		}else{
			return SUCCESS;
		}
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="ajax/listTmpVndBank", type="redirectAction")
			},value="ajax/submitTmpVndBank"
	)
	public String submitTmpVndBank() throws Exception{
		TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		TmpVndBank bank = model.getTmpVndBank();
		bank.setTmpVndHeader(vndHeader);
						
		boolean cek  = regService.saveOrUpdateTmpVndBank(bank);
		if(cek){
			addActionMessage(getText("eproc.alert.success"));
		}else{
			addActionError(getText("eproc.alert.failed"));
		}
		return SUCCESS;
	}
	
	//-=---------------------FIANANSIAL REPORT---------------------------------------------------------//
		@Action(
				results={
						@Result(name=SUCCESS, location="vnd/reg/financeInformation/listFinRpt.jsp")
				},value="ajax/listTmpVndFinRpt"
		)
		public String listTmpVndFinRpt() throws Exception{
			TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
			TmpVndHeader tmp = authService.getTmpVndHeaderById(vndHeader.getVendorId());
			model.setListTmpVndFinRpt(regService.listTmpVndFinRpt(tmp));
			return SUCCESS;
		}
		
		@Action(
				results={
						@Result(name=SUCCESS, location="vnd/reg/financeInformation/setFinRpt.jsp"),
						@Result(name=ERROR, location="error/wrong", type="redirectAction"),
						@Result(name="delete", location="ajax/listTmpVndFinRpt", type="redirectAction"),
						@Result(name="edit", location="vnd/reg/financeInformation/setFinRpt.jsp")
				},value="ajax/setTmpVndFinRpt"
		)
		public String setTmpVndFinRpt() throws Exception{
			TmpVndHeader tmpVndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
			model.setListAdmCurr(admService.listAdmCurr());
			
			if(request.getParameter("id") != null && request.getParameter("id") != "" && request.getParameter("act") != null && request.getParameter("act").equals("edit")){
				int id = Integer.parseInt(request.getParameter("id"));
				model.setTmpVndFinRpt(regService.getTmpVndFinRptById(id, tmpVndHeader));
				return "edit";
				
			}else if (request.getParameter("id") != null && request.getParameter("id") != "" && request.getParameter("act") != null && request.getParameter("act").equals("delete")) {
				int id = Integer.parseInt(request.getParameter("id"));
				boolean cek = regService.deleteTmpVndFinRpt(regService.getTmpVndFinRptById(id, tmpVndHeader));
				if(cek){
					addActionMessage(getText("eproc.alert.delete"));
					return "delete";
				}else{
					addActionError(getText("eproc.alert.deletefail"));
					return ERROR;
				}
			}else{
				return SUCCESS;
			}
		}
		
		@Action(
				results={
						@Result(name=SUCCESS, location="ajax/listTmpVndFinRpt", type="redirectAction")
				},value="ajax/submitTmpVndFinRpt"
		)
		public String submitTmpVndFinRpt() throws Exception{
			TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
			TmpVndFinRpt fin = model.getTmpVndFinRpt();
			fin.setTmpVndHeader(vndHeader);
							
			boolean cek  = regService.saveOrUpdateTmpVndFinRpt(fin);
			if(cek){
				addActionMessage(getText("eproc.alert.success"));
			}else{
				addActionError(getText("eproc.alert.failed"));
			}
			return SUCCESS;
		}
}
