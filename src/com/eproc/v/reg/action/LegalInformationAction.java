package com.eproc.v.reg.action;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.web.util.WebUtils;

import com.eproc.core.BaseController;
import com.eproc.utils.SessionGetter;
import com.eproc.v.reg.command.LegalInformationCommand;
import com.orm.model.TmpVndAddress;
import com.orm.model.TmpVndAkta;
import com.orm.model.TmpVndDomisili;
import com.orm.model.TmpVndHeader;
import com.orm.model.TmpVndIjin;

import eproc.controller.VndAuthService;
import eproc.controller.VndAuthServiceImpl;
import eproc.controller.VndRegService;
import eproc.controller.VndRegServiceImpl;

@ParentPackage("eprocVnd")
@Namespace("/")
public class LegalInformationAction extends BaseController {
	private LegalInformationCommand model;
	private VndAuthService authService;
	private VndRegService regSevice;

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return model;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		model = new LegalInformationCommand();
		authService = new VndAuthServiceImpl();
		regSevice = new VndRegServiceImpl();
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/reg/legalInformation/legInf.jsp"),
					@Result(name=ERROR, location="vnd/reg/legalInformation/legInf.jsp"),
					@Result(name="restricted", location="vnd/reg/restrictedPage.jsp"),
					@Result(name="next", location="vnd/managementInformation", type="redirectAction"),
			},value="vnd/legalInformation"
	)
	public String vndLegalInformation() throws Exception{
		TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		TmpVndHeader tmp = authService.getTmpVndHeaderById(vndHeader.getVendorId());
		
		List<TmpVndAkta> akta = regSevice.listTmpVndAkta(tmp);
		List<TmpVndDomisili> dom = regSevice.listTmpVndDom(tmp);
		model.setListIjin(regSevice.listTmpVndIjin(tmp));
				
		if(tmp.getVendorNextPage() >= 2){
			model.setPageId(tmp.getVendorNextPage());
			
			if(WebUtils.hasSubmitParameter(request, "save") || WebUtils.hasSubmitParameter(request, "saveAndNext")){
				//NPWP
				tmp.setVendorNpwpNo(model.getTmpVndHeader().getVendorNpwpNo());
				tmp.setVendorNpwpProvince(model.getTmpVndHeader().getVendorNpwpProvince());
				tmp.setVendorNpwpCity(model.getTmpVndHeader().getVendorNpwpCity());
				tmp.setVendorNpwpAddress(model.getTmpVndHeader().getVendorNpwpAddress());
				tmp.setVendorNpwpPostCode(model.getTmpVndHeader().getVendorNpwpPostCode());
				tmp.setVendorNpwpPkp(model.getTmpVndHeader().getVendorNpwpPkp());
				tmp.setVendorNpwpPkpNo(model.getTmpVndHeader().getVendorNpwpPkpNo());
				
				//SIUP
				tmp.setVendorSiupTipe(model.getTmpVndHeader().getVendorSiupTipe());
				tmp.setVendorSiupIssued(model.getTmpVndHeader().getVendorSiupIssued());
				tmp.setVendorSiupNo(model.getTmpVndHeader().getVendorSiupNo());
				tmp.setVendorSiupFrom(model.getTmpVndHeader().getVendorSiupFrom());
				tmp.setVendorSiupTo(model.getTmpVndHeader().getVendorSiupTo());
				
				//TDP
				tmp.setVendorTdpIssued(model.getTmpVndHeader().getVendorTdpIssued());
				tmp.setVendorTdpNo(model.getTmpVndHeader().getVendorTdpNo());
				tmp.setVendorTdpFrom(model.getTmpVndHeader().getVendorTdpFrom());
				tmp.setVendorTdpTo(model.getTmpVndHeader().getVendorTdpTo());
				
				//Simpan NPWP, TDP, SIUP
				boolean cek = authService.saveOrUpdateTmpVndHeader(tmp);
				if(cek){
					addActionMessage(getText("eproc.alert.success"));
				}else{
					addActionError(getText("eproc.alert.failed"));
					model.setListTmpVndAkta(akta);
					model.setListTmpVndDom(dom);
					
					Map<String, String> l = new TreeMap<String, String>();
						l.put("Y", getText("yes"));
						l.put("N", getText("no"));
					model.setListPkp(l);
					return ERROR;
				}
				
				if(WebUtils.hasSubmitParameter(request, "saveAndNext")){
					if(akta.size() == 0){
						clearMessages();
						addActionError(getText("leg.inf.akta.minimal"));
					}else if(dom.size() == 0){
						clearMessages();
						addActionError(getText("leg.inf.domisili.minimal"));
					}else{
						if(tmp.getVendorNextPage() == 2)regSevice.saveNextPage(tmp.getVendorNextPage()+1, tmp);
						return "next";
					}	
				}
			}
			
			model.setTmpVndHeader(tmp);
			model.setListTmpVndAkta(akta);
			model.setListTmpVndDom(dom);
			
			Map<String, String> l = new TreeMap<String, String>();
				l.put("Y", getText("yes"));
				l.put("N", getText("no"));
			model.setListPkp(l);
			return SUCCESS;
		}else{
			return "restricted";
		}
		
	}
	
	//----------------------------------AKTA PERUSAHAAN ----------------------------------------------//	
		@Action(
				results={
						@Result(name=SUCCESS, location="vnd/reg/legalInformation/listAktaPerusahaan.jsp")
				},value="ajax/listTmpVndAkta"
		)
		public String listTmpVndAkta() throws Exception{
			TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
			TmpVndHeader tmp = authService.getTmpVndHeaderById(vndHeader.getVendorId());
			model.setListTmpVndAkta(regSevice.listTmpVndAkta(tmp));
			return SUCCESS;
		}
		
		@Action(
				results={
						@Result(name=SUCCESS, location="vnd/reg/legalInformation/setAktaPerusahaan.jsp"),
						@Result(name=ERROR, location="error/wrong", type="redirectAction"),
						@Result(name="delete", location="ajax/listTmpVndAkta", type="redirectAction"),
						@Result(name="edit", location="vnd/reg/legalInformation/setAktaPerusahaan.jsp")
				},value="ajax/setTmpVndAkta"
		)
		public String setTmpVndAkta() throws Exception{
			TmpVndHeader tmpVndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
			
			if(request.getParameter("id") != null && request.getParameter("id") != "" && request.getParameter("act") != null && request.getParameter("act").equals("edit")){
				int id = Integer.parseInt(request.getParameter("id"));
				model.setTmpVndAkta(regSevice.getTmpVndAktaById(id, tmpVndHeader));
				return "edit";
				
			}else if (request.getParameter("id") != null && request.getParameter("id") != "" && request.getParameter("act") != null && request.getParameter("act").equals("delete")) {
				int id = Integer.parseInt(request.getParameter("id"));
				boolean cek = regSevice.deleteTmpVndAkta(regSevice.getTmpVndAktaById(id, tmpVndHeader));
				if(cek){
					addActionMessage(getText("eproc.alert.delete"));
					return "delete";
				}else{
					addActionError(getText("eproc.alert.deletefail"));
					return ERROR;
				}
			}else{
				return SUCCESS;
			}
		}
		
		@Action(
				results={
						@Result(name=SUCCESS, location="ajax/listTmpVndAkta", type="redirectAction")
				},value="ajax/submitTmpVndAkta"
		)
		public String submitTmpVndAkta() throws Exception{
			TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
			TmpVndAkta tmpAkta = model.getTmpVndAkta();
			tmpAkta.setTmpVndHeader(vndHeader);
			
			boolean cek  = regSevice.saveOrUpdateTmpVndAkta(tmpAkta);
			if(cek){
				addActionMessage(getText("eproc.alert.success"));
			}else{
				addActionError(getText("eproc.alert.failed"));
			}
			return SUCCESS;
		}
		
		//----------------------------------DOMISILI PERUSAHAAN ----------------------------------------------//	
		@Action(
				results={
						@Result(name=SUCCESS, location="vnd/reg/legalInformation/listDomPerusahaan.jsp")
				},value="ajax/listTmpVndDom"
		)
		public String listTmpVndDom() throws Exception{
			TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
			TmpVndHeader tmp = authService.getTmpVndHeaderById(vndHeader.getVendorId());
			model.setListTmpVndDom(regSevice.listTmpVndDom(tmp));
			return SUCCESS;
		}
		
		@Action(
				results={
						@Result(name=SUCCESS, location="vnd/reg/legalInformation/setDomPerusahaan.jsp"),
						@Result(name=ERROR, location="error/wrong", type="redirectAction"),
						@Result(name="delete", location="ajax/listTmpVndDom", type="redirectAction"),
						@Result(name="edit", location="vnd/reg/legalInformation/setDomPerusahaan.jsp")
				},value="ajax/setTmpVndDom"
		)
		public String setTmpVndDom() throws Exception{
			TmpVndHeader tmpVndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
			model.setListAdmCountry(regSevice.listAdmCountry());
			
			if(request.getParameter("id") != null && request.getParameter("id") != "" && request.getParameter("act") != null && request.getParameter("act").equals("edit")){
				int id = Integer.parseInt(request.getParameter("id"));
				model.setTmpVndDom(regSevice.getTmpVndDomById(id, tmpVndHeader));
				return "edit";
				
			}else if (request.getParameter("id") != null && request.getParameter("id") != "" && request.getParameter("act") != null && request.getParameter("act").equals("delete")) {
				int id = Integer.parseInt(request.getParameter("id"));
				boolean cek = regSevice.deleteTmpVndDom(regSevice.getTmpVndDomById(id, tmpVndHeader));
				if(cek){
					addActionMessage(getText("eproc.alert.delete"));
					return "delete";
				}else{
					addActionError(getText("eproc.alert.deletefail"));
					return ERROR;
				}
			}else{
				return SUCCESS;
			}
		}
		
		@Action(
				results={
						@Result(name=SUCCESS, location="ajax/listTmpVndDom", type="redirectAction")
				},value="ajax/submitTmpVndDom"
		)
		public String submitTmpVndDom() throws Exception{
			TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
			TmpVndDomisili tmpVndDom = model.getTmpVndDom();
			tmpVndDom.setTmpVndHeader(vndHeader);
			
			boolean cek  = regSevice.saveOrUpdateTmpVndDom(tmpVndDom);
			if(cek){
				addActionMessage(getText("eproc.alert.success"));
			}else{
				addActionError(getText("eproc.alert.failed"));
			}
			return SUCCESS;
		}
		
		//----------------------------------IJIN LAIN PERUSAHAAN ----------------------------------------------//	
				@Action(
						results={
								@Result(name=SUCCESS, location="vnd/reg/legalInformation/listIjinPerusahaan.jsp")
						},value="ajax/listTmpVndIjin"
				)
				public String listTmpVndIjin() throws Exception{
					TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
					TmpVndHeader tmp = authService.getTmpVndHeaderById(vndHeader.getVendorId());
					model.setListIjin(regSevice.listTmpVndIjin(tmp));
					return SUCCESS;
				}
				
				@Action(
						results={
								@Result(name=SUCCESS, location="vnd/reg/legalInformation/setIjinPerusahaan.jsp"),
								@Result(name=ERROR, location="error/wrong", type="redirectAction"),
								@Result(name="delete", location="ajax/listTmpVndIjin", type="redirectAction"),
								@Result(name="edit", location="vnd/reg/legalInformation/setIjinPerusahaan.jsp")
						},value="ajax/setTmpVndIjin"
				)
				public String setTmpVndIjin() throws Exception{
					TmpVndHeader tmpVndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
					
					if(request.getParameter("id") != null && request.getParameter("id") != "" && request.getParameter("act") != null && request.getParameter("act").equals("edit")){
						int id = Integer.parseInt(request.getParameter("id"));
						model.setTmpVndDom(regSevice.getTmpVndDomById(id, tmpVndHeader));
						model.setIjin(regSevice.getTmpVndIjinById(id));
						return "edit";
						
					}else if (request.getParameter("id") != null && request.getParameter("id") != "" && request.getParameter("act") != null && request.getParameter("act").equals("delete")) {
						int id = Integer.parseInt(request.getParameter("id"));
						boolean cek = regSevice.deleteTmpVndIjin(regSevice.getTmpVndIjinById(id));
						if(cek){
							addActionMessage(getText("eproc.alert.delete"));
							return "delete";
						}else{
							addActionError(getText("eproc.alert.deletefail"));
							return ERROR;
						}
					}else{
						return SUCCESS;
					}
				}
				
				@Action(
						results={
								@Result(name=SUCCESS, location="ajax/listTmpVndIjin", type="redirectAction")
						},value="ajax/submitTmpVndIjin"
				)
				public String submitTmpVndIjin() throws Exception{
					TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
					TmpVndIjin tmpVndIjin = model.getIjin();
					tmpVndIjin.setTmpVndHeader(vndHeader);
					
					boolean cek  = regSevice.saveOrUpdateTmpVndIjin(tmpVndIjin);
					if(cek){
						addActionMessage(getText("eproc.alert.success"));
					}else{
						addActionError(getText("eproc.alert.failed"));
					}
					return SUCCESS;
				}
}
