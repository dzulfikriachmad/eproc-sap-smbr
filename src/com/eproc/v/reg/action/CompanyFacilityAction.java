package com.eproc.v.reg.action;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.web.util.WebUtils;

import com.eproc.core.BaseController;
import com.eproc.utils.SessionGetter;
import com.eproc.v.reg.command.CompanyFacilityCommand;
import com.orm.model.TmpVndEquip;
import com.orm.model.TmpVndHeader;

import eproc.controller.VndAuthService;
import eproc.controller.VndAuthServiceImpl;
import eproc.controller.VndRegService;
import eproc.controller.VndRegServiceImpl;

@ParentPackage("eprocVnd")
@Namespace("/")
public class CompanyFacilityAction extends BaseController {
	private CompanyFacilityCommand model;
	private VndAuthService authService;
	private VndRegService regService;
	
	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return model;
	}
	
	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		model = new CompanyFacilityCommand();
		authService = new VndAuthServiceImpl();
		regService = new VndRegServiceImpl();
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/reg/companyFacility/comFac.jsp"),
					@Result(name=ERROR, location="vnd/reg/companyFacility/comFac.jsp"),
					@Result(name="restricted", location="vnd/reg/restrictedPage.jsp"),
					@Result(name="next", location="vnd/projectExperience", type="redirectAction")
			},value="vnd/companyFacility"
	)
	public String companyFacility() throws Exception{
		TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		TmpVndHeader tmp = authService.getTmpVndHeaderById(vndHeader.getVendorId());
		List<TmpVndEquip> equip = regService.listTmpVndEquip(tmp);
		
		if(tmp.getVendorNextPage() >= 7){
			model.setPageId(tmp.getVendorNextPage());
						
			if(WebUtils.hasSubmitParameter(request, "saveAndNext")){
				if(equip.size() == 0){
					clearErrors();
					clearMessages();
					addActionError(getText("com.fac.minimal"));
					
					model.setTmpVndHeader(tmp);
					model.setListTmpVndEquip(equip);
					
					return ERROR;
				}else{
					if(tmp.getVendorNextPage() == 7)regService.saveNextPage(tmp.getVendorNextPage()+1, tmp);
					return "next";
				}				
			}	
			
			model.setTmpVndHeader(tmp);
			model.setListTmpVndEquip(equip);
			return SUCCESS;
		}else{
			return "restricted";
		}
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/reg/companyFacility/listFac.jsp")
			},value="ajax/listTmpVndEquip"
	)
	public String listTmpVndEquip() throws Exception{
		TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		TmpVndHeader tmp = authService.getTmpVndHeaderById(vndHeader.getVendorId());
		model.setListTmpVndEquip(regService.listTmpVndEquip(tmp));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/reg/companyFacility/setFac.jsp"),
					@Result(name=ERROR, location="error/wrong", type="redirectAction"),
					@Result(name="delete", location="ajax/listTmpVndEquip", type="redirectAction"),
					@Result(name="edit", location="vnd/reg/companyFacility/setFac.jsp")
			},value="ajax/setTmpVndEquip"
	)
	public String setTmpVndEquip() throws Exception{
		TmpVndHeader tmpVndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		Map<Integer, String> type = new TreeMap<Integer, String>();
			type.put(1, getText("com.fac.type1"));
			type.put(2, getText("com.fac.type2"));
			type.put(3, getText("com.fac.type3"));
			type.put(4, getText("com.fac.type4"));
			type.put(5, getText("com.fac.type5"));
			type.put(6, getText("com.fac.type6"));
			type.put(7, getText("com.fac.type7"));
			type.put(8, getText("com.fac.type8"));
		model.setListEquipType(type);
				
		if(request.getParameter("id") != null && request.getParameter("id") != "" && request.getParameter("act") != null && request.getParameter("act").equals("edit")){
			int id = Integer.parseInt(request.getParameter("id"));
			model.setTmpVndEquip(regService.getTmpVndEquipById(id, tmpVndHeader));
			return "edit";
			
		}else if (request.getParameter("id") != null && request.getParameter("id") != "" && request.getParameter("act") != null && request.getParameter("act").equals("delete")) {
			int id = Integer.parseInt(request.getParameter("id"));
			boolean cek = regService.deleteTmpVndEquip(regService.getTmpVndEquipById(id, tmpVndHeader));
			if(cek){
				addActionMessage(getText("eproc.alert.delete"));
				return "delete";
			}else{
				addActionError(getText("eproc.alert.deletefail"));
				return ERROR;
			}
		}else{
			return SUCCESS;
		}
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="ajax/listTmpVndEquip", type="redirectAction")
			},value="ajax/submitTmpVndEquip"
	)
	public String submitTmpVndEquip() throws Exception{
		TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		TmpVndEquip equip = model.getTmpVndEquip();
		equip.setTmpVndHeader(vndHeader);
						
		boolean cek  = regService.saveOrUpdateTmpVndEquip(equip);
		if(cek){
			addActionMessage(getText("eproc.alert.success"));
		}else{
			addActionError(getText("eproc.alert.failed"));
		}
		return SUCCESS;
	}
}
