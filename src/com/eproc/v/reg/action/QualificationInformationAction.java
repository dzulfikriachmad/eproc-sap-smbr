package com.eproc.v.reg.action;

import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.web.util.WebUtils;

import com.eproc.core.BaseController;
import com.eproc.utils.SessionGetter;
import com.eproc.v.reg.command.QualificationInformationCommand;
import com.orm.model.TmpVndCert;
import com.orm.model.TmpVndHeader;

import eproc.controller.VndAuthService;
import eproc.controller.VndAuthServiceImpl;
import eproc.controller.VndRegService;
import eproc.controller.VndRegServiceImpl;

@ParentPackage("eprocVnd")
@Namespace("/")
public class QualificationInformationAction extends BaseController {
	private QualificationInformationCommand model;
	private VndAuthService authService;
	private VndRegService regService;
	
	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return model;
	}
	
	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		model = new QualificationInformationCommand();
		authService = new VndAuthServiceImpl();
		regService = new VndRegServiceImpl();
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/reg/qualificationInformation/quaInf.jsp"),
					@Result(name=ERROR, location="vnd/reg/qualificationInformation/quaInf.jsp"),
					@Result(name="restricted", location="vnd/reg/restrictedPage.jsp"),
					@Result(name="next", location="vnd/resourcesInformation", type="redirectAction"),
					@Result(name="skip", location="vnd/resourcesInformation", type="redirectAction")
			},value="vnd/qualificationInformation"
	)
	public String qualificationInformation() throws Exception{
		TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		TmpVndHeader tmp = authService.getTmpVndHeaderById(vndHeader.getVendorId());
		List<TmpVndCert> cert = regService.listTmpVndCert(tmp);
		
		if(tmp.getVendorNextPage() >= 5){
			model.setPageId(tmp.getVendorNextPage());
						
			if(WebUtils.hasSubmitParameter(request, "saveAndNext")){
				if(cert.size() == 0){
					clearErrors();
					clearMessages();
					addActionError(getText("qua.inf.minimal"));
					
					model.setTmpVndHeader(tmp);
					model.setListTmpVndCert(cert);
					
					return ERROR;
				}else{
					if(tmp.getVendorNextPage() == 5)regService.saveNextPage(tmp.getVendorNextPage()+1, tmp);
					return "next";
				}				
			}else if(WebUtils.hasSubmitParameter(request, "skip")){
				if(tmp.getVendorNextPage() == 5)regService.saveNextPage(tmp.getVendorNextPage()+1, tmp);
				return "skip";
			}		
			
			model.setTmpVndHeader(tmp);
			model.setListTmpVndCert(cert);
			return SUCCESS;
		}else{
			return "restricted";
		}
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/reg/qualificationInformation/listCert.jsp")
			},value="ajax/listTmpVndCert"
	)
	public String listTmpVndCert() throws Exception{
		TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		TmpVndHeader tmp = authService.getTmpVndHeaderById(vndHeader.getVendorId());
		model.setListTmpVndCert(regService.listTmpVndCert(tmp));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/reg/qualificationInformation/setCert.jsp"),
					@Result(name=ERROR, location="error/wrong", type="redirectAction"),
					@Result(name="delete", location="ajax/listTmpVndCert", type="redirectAction"),
					@Result(name="edit", location="vnd/reg/qualificationInformation/setCert.jsp")
			},value="ajax/setTmpVndCert"
	)
	public String setTmpVndCert() throws Exception{
		TmpVndHeader tmpVndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
				
		if(request.getParameter("id") != null && request.getParameter("id") != "" && request.getParameter("act") != null && request.getParameter("act").equals("edit")){
			int id = Integer.parseInt(request.getParameter("id"));
			model.setTmpVndCert(regService.getTmpVndCertById(id, tmpVndHeader));
			return "edit";
			
		}else if (request.getParameter("id") != null && request.getParameter("id") != "" && request.getParameter("act") != null && request.getParameter("act").equals("delete")) {
			int id = Integer.parseInt(request.getParameter("id"));
			boolean cek = regService.deleteTmpVndCert(regService.getTmpVndCertById(id, tmpVndHeader));
			if(cek){
				addActionMessage(getText("eproc.alert.delete"));
				return "delete";
			}else{
				addActionError(getText("eproc.alert.deletefail"));
				return ERROR;
			}
		}else{
			return SUCCESS;
		}
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="ajax/listTmpVndCert", type="redirectAction")
			},value="ajax/submitTmpVndCert"
	)
	public String submitTmpVndCert() throws Exception{
		TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		TmpVndCert cert = model.getTmpVndCert();
		cert.setTmpVndHeader(vndHeader);
						
		boolean cek  = regService.saveOrUpdateTmpVndCert(cert);
		if(cek){
			addActionMessage(getText("eproc.alert.success"));
		}else{
			addActionError(getText("eproc.alert.failed"));
		}
		return SUCCESS;
	}
}
