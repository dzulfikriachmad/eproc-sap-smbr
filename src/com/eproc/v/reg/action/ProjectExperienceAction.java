package com.eproc.v.reg.action;

import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.web.util.WebUtils;

import com.eproc.core.BaseController;
import com.eproc.utils.SessionGetter;
import com.eproc.v.reg.command.ProjectExperienceCommand;
import com.orm.model.TmpVndCv;
import com.orm.model.TmpVndHeader;

import eproc.controller.AdmService;
import eproc.controller.AdmServiceImpl;
import eproc.controller.VndAuthService;
import eproc.controller.VndAuthServiceImpl;
import eproc.controller.VndRegService;
import eproc.controller.VndRegServiceImpl;

@ParentPackage("eprocVnd")
@Namespace("/")
public class ProjectExperienceAction extends BaseController {
	private ProjectExperienceCommand model;
	private VndAuthService authService;
	private VndRegService regService;
	private AdmService admService;
	
	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return model;
	}
	
	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		model = new ProjectExperienceCommand();
		authService = new VndAuthServiceImpl();
		regService = new VndRegServiceImpl();
		admService = new AdmServiceImpl();
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/reg/projectExperience/proExp.jsp"),
					@Result(name=ERROR, location="vnd/reg/projectExperience/proExp.jsp"),
					@Result(name="restricted", location="vnd/reg/restrictedPage.jsp"),
					@Result(name="next", location="vnd/commodityWorkingAreaInformation", type="redirectAction"),
					@Result(name="skip", location="vnd/commodityWorkingAreaInformation", type="redirectAction")
			},value="vnd/projectExperience"
	)
	public String projectExperience() throws Exception{
		TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		TmpVndHeader tmp = authService.getTmpVndHeaderById(vndHeader.getVendorId());
		List<TmpVndCv> cv = regService.listTmpVndCv(tmp);
		
		if(tmp.getVendorNextPage() >= 8){
			model.setPageId(tmp.getVendorNextPage());
						
			if(WebUtils.hasSubmitParameter(request, "saveAndNext")){
				if(cv.size() == 0){
					clearErrors();
					clearMessages();
					addActionError(getText("pro.exp.minimal"));
					
					model.setTmpVndHeader(tmp);
					model.setListTmpVndCv(cv);
					
					return ERROR;
				}else{
					if(tmp.getVendorNextPage() == 8)regService.saveNextPage(tmp.getVendorNextPage()+1, tmp);
					return "next";
				}				
			}else if(WebUtils.hasSubmitParameter(request, "skip")){
				if(tmp.getVendorNextPage() == 8)regService.saveNextPage(tmp.getVendorNextPage()+1, tmp);
				return "skip";
			}		
			
			model.setTmpVndHeader(tmp);
			model.setListTmpVndCv(cv);
			return SUCCESS;
		}else{
			return "restricted";
		}
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/reg/projectExperience/listPro.jsp")
			},value="ajax/listTmpVndCv"
	)
	public String listTmpVndCv() throws Exception{
		TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		TmpVndHeader tmp = authService.getTmpVndHeaderById(vndHeader.getVendorId());
		model.setListTmpVndCv(regService.listTmpVndCv(tmp));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/reg/projectExperience/setPro.jsp"),
					@Result(name=ERROR, location="error/wrong", type="redirectAction"),
					@Result(name="delete", location="ajax/listTmpVndCv", type="redirectAction"),
					@Result(name="edit", location="vnd/reg/projectExperience/setPro.jsp")
			},value="ajax/setTmpVndCv"
	)
	public String setTmpVndCv() throws Exception{
		TmpVndHeader tmpVndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		model.setListAdmCurrency(admService.listAdmCurr());
		
		if(request.getParameter("id") != null && request.getParameter("id") != "" && request.getParameter("act") != null && request.getParameter("act").equals("edit")){
			int id = Integer.parseInt(request.getParameter("id"));
			model.setTmpVndCv(regService.getTmpVndCvById(id, tmpVndHeader));
			return "edit";
			
		}else if (request.getParameter("id") != null && request.getParameter("id") != "" && request.getParameter("act") != null && request.getParameter("act").equals("delete")) {
			int id = Integer.parseInt(request.getParameter("id"));
			boolean cek = regService.deleteTmpVndCv(regService.getTmpVndCvById(id, tmpVndHeader));
			if(cek){
				addActionMessage(getText("eproc.alert.delete"));
				return "delete";
			}else{
				addActionError(getText("eproc.alert.deletefail"));
				return ERROR;
			}
		}else{
			return SUCCESS;
		}
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="ajax/listTmpVndCv", type="redirectAction")
			},value="ajax/submitTmpVndCv"
	)
	public String submitTmpVndCv() throws Exception{
		TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		TmpVndCv cv = model.getTmpVndCv();
		cv.setTmpVndHeader(vndHeader);
						
		boolean cek  = regService.saveOrUpdateTmpVndCv(cv);
		if(cek){
			addActionMessage(getText("eproc.alert.success"));
		}else{
			addActionError(getText("eproc.alert.failed"));
		}
		return SUCCESS;
	}
}
