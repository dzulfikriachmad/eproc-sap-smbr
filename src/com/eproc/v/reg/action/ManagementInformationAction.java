package com.eproc.v.reg.action;

import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.web.util.WebUtils;

import com.eproc.core.BaseController;
import com.eproc.utils.SessionGetter;
import com.eproc.v.reg.command.ManagementInformationCommand;
import com.orm.model.TmpVndBoard;
import com.orm.model.TmpVndHeader;

import eproc.controller.VndAuthService;
import eproc.controller.VndAuthServiceImpl;
import eproc.controller.VndRegService;
import eproc.controller.VndRegServiceImpl;

@ParentPackage("eprocVnd")
@Namespace("/")
public class ManagementInformationAction extends BaseController {
	private ManagementInformationCommand model;
	private VndAuthService authService;
	private VndRegService regService;
	
	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return model;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		model = new ManagementInformationCommand();
		authService = new VndAuthServiceImpl();
		regService = new VndRegServiceImpl();
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/reg/managementInformation/manInf.jsp"),
					@Result(name=ERROR, location="vnd/reg/managementInformation/manInf.jsp"),
					@Result(name="restricted", location="vnd/reg/restrictedPage.jsp"),
					@Result(name="next", location="vnd/companyFinanceInformation", type="redirectAction"),
			},value="vnd/managementInformation"
	)
	public String vndManagementInformation() throws Exception{
		TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		TmpVndHeader tmp = authService.getTmpVndHeaderById(vndHeader.getVendorId());
		
		List<TmpVndBoard> komisaris = regService.listTmpVndBoardKomisaris(tmp);
		List<TmpVndBoard> direksi = regService.listTmpVndBoardDireksi(tmp);
		
		if(tmp.getVendorNextPage() >= 3){
			model.setPageId(tmp.getVendorNextPage());
			
			if(WebUtils.hasSubmitParameter(request, "save") || WebUtils.hasSubmitParameter(request, "saveAndNext")){
				addActionMessage(getText("eproc.alert.success"));
				
				if(WebUtils.hasSubmitParameter(request, "saveAndNext")){
//					if(komisaris.size() == 0){
//						clearMessages();
//						addActionError("Minimal harus ada 1 data Dewan Komisaris..");
//					}else
					if(direksi.size() == 0){
						clearMessages();
						addActionError(getText("man.inf.direksi.minimal"));
					}else{
						if(tmp.getVendorNextPage() == 3)
							regService.saveNextPage(tmp.getVendorNextPage()+1, tmp);
						return "next";
					}	
				}
			}
			
			model.setTmpVndHeader(tmp);
			model.setListTmpVndBoardDireksi(direksi);
			model.setListTmpVndBoardKomisaris(komisaris);
			return SUCCESS;
		}else{
			return "restricted";
		}
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/reg/managementInformation/listKomisaris.jsp")
			},value="ajax/listTmpVndBoardKomisaris"
	)
	public String listTmpVndBoardKomisaris() throws Exception{
		TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		TmpVndHeader tmp = authService.getTmpVndHeaderById(vndHeader.getVendorId());
		model.setListTmpVndBoardKomisaris(regService.listTmpVndBoardKomisaris(tmp));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/reg/managementInformation/setKomisaris.jsp"),
					@Result(name=ERROR, location="error/wrong", type="redirectAction"),
					@Result(name="delete", location="ajax/listTmpVndBoardKomisaris", type="redirectAction"),
					@Result(name="edit", location="vnd/reg/managementInformation/setKomisaris.jsp")
			},value="ajax/setTmpVndBoardKomisaris"
	)
	public String setTmpVndBoardKomisaris() throws Exception{
		TmpVndHeader tmpVndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		model.setListAdmCountry(regService.listAdmCountry());
		
		if(request.getParameter("id") != null && request.getParameter("id") != "" && request.getParameter("act") != null && request.getParameter("act").equals("edit")){
			int id = Integer.parseInt(request.getParameter("id"));
			model.setTmpVndBoard(regService.getTmpVndBoardById(id, tmpVndHeader));
			return "edit";
			
		}else if (request.getParameter("id") != null && request.getParameter("id") != "" && request.getParameter("act") != null && request.getParameter("act").equals("delete")) {
			int id = Integer.parseInt(request.getParameter("id"));
			boolean cek = regService.deleteTmpVndBoard(regService.getTmpVndBoardById(id, tmpVndHeader));
			if(cek){
				addActionMessage(getText("eproc.alert.delete"));
				return "delete";
			}else{
				addActionError(getText("eproc.alert.deletefail"));
				return ERROR;
			}
		}else{
			return SUCCESS;
		}
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="ajax/listTmpVndBoardKomisaris", type="redirectAction")
			},value="ajax/submitTmpVndBoardKomisaris"
	)
	public String submitTmpVndBoardKomisaris() throws Exception{
		TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		TmpVndBoard tmpVndBoard = model.getTmpVndBoard();
		tmpVndBoard.setTmpVndHeader(vndHeader);
						
		boolean cek  = regService.saveOrUpdateTmpVndBoard(tmpVndBoard);
		if(cek){
			addActionMessage(getText("eproc.alert.success"));
		}else{
			addActionError(getText("eproc.alert.failed"));
		}
		return SUCCESS;
	}
		
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/reg/managementInformation/listDireksi.jsp")
			},value="ajax/listTmpVndBoardDireksi"
	)
	public String listTmpVndBoardDireksi() throws Exception{
		TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		TmpVndHeader tmp = authService.getTmpVndHeaderById(vndHeader.getVendorId());
		model.setListTmpVndBoardDireksi(regService.listTmpVndBoardDireksi(tmp));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/reg/managementInformation/setDireksi.jsp"),
					@Result(name=ERROR, location="error/wrong", type="redirectAction"),
					@Result(name="delete", location="ajax/listTmpVndBoardDireksi", type="redirectAction"),
					@Result(name="edit", location="vnd/reg/managementInformation/setDireksi.jsp")
			},value="ajax/setTmpVndBoardDireksi"
	)
	public String setTmpVndBoardDireksi() throws Exception{
		TmpVndHeader tmpVndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		model.setListAdmCountry(regService.listAdmCountry());
		
		if(request.getParameter("id") != null && request.getParameter("id") != "" && request.getParameter("act") != null && request.getParameter("act").equals("edit")){
			int id = Integer.parseInt(request.getParameter("id"));
			model.setTmpVndBoard(regService.getTmpVndBoardById(id, tmpVndHeader));
			return "edit";
			
		}else if (request.getParameter("id") != null && request.getParameter("id") != "" && request.getParameter("act") != null && request.getParameter("act").equals("delete")) {
			int id = Integer.parseInt(request.getParameter("id"));
			boolean cek = regService.deleteTmpVndBoard(regService.getTmpVndBoardById(id, tmpVndHeader));
			if(cek){
				addActionMessage(getText("eproc.alert.delete"));
				return "delete";
			}else{
				addActionError(getText("eproc.alert.deletefail"));
				return ERROR;
			}
		}else{
			return SUCCESS;
		}
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="ajax/listTmpVndBoardDireksi", type="redirectAction")
			},value="ajax/submitTmpVndBoardDireksi"
	)
	public String submitTmpVndBoardDireksi() throws Exception{
		TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		TmpVndBoard tmpVndBoard = model.getTmpVndBoard();
		tmpVndBoard.setTmpVndHeader(vndHeader);
						
		boolean cek  = regService.saveOrUpdateTmpVndBoard(tmpVndBoard);
		if(cek){
			addActionMessage(getText("eproc.alert.success"));
		}else{
			addActionError(getText("eproc.alert.failed"));
		}
		return SUCCESS;
	}
}