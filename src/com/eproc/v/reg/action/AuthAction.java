package com.eproc.v.reg.action;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.web.util.WebUtils;

import com.eproc.core.BaseController;
import com.eproc.utils.SessionGetter;
import com.eproc.v.reg.command.AuthCommand;
import com.orm.model.TmpVndHeader;

import eproc.controller.VndAuthService;
import eproc.controller.VndAuthServiceImpl;

@ParentPackage("eprocVnd")
@Namespace("/")
public class AuthAction extends BaseController{
	private AuthCommand model;
	private VndAuthService authService;
	
	private String url;

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.model;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		model = new AuthCommand();
		authService = new VndAuthServiceImpl();
	}
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/login.jsp"),
					@Result(name="dashboard", location="vnd/dashboard", type="redirectAction"),
					@Result(name="redirectOk", location="${url}", type="redirect"),
					@Result(name="notActivated", location="vnd/confirmation", type="redirectAction"),
					@Result(name="page1", location="vnd/companyInformation", type="redirectAction"),
					@Result(name="page2", location="vnd/legalInformation", type="redirectAction"),
					@Result(name="page3", location="vnd/managementInformation", type="redirectAction"),
					@Result(name="page4", location="vnd/companyFinanceInformation", type="redirectAction"),
					@Result(name="page5", location="vnd/qualificationInformation", type="redirectAction"),
					@Result(name="page6", location="vnd/resourcesInformation", type="redirectAction"),
					@Result(name="page7", location="vnd/companyFacility", type="redirectAction"),
					@Result(name="page8", location="vnd/projectExperience", type="redirectAction"),
					@Result(name="page9", location="vnd/commodityWorkingAreaInformation", type="redirectAction"),
					@Result(name="page10", location="vnd/regFinish", type="redirectAction"),
					@Result(name="routeToVendor", location="vnd/routeToVendor", type="redirectAction")
			}, value = "vnd/login",
			interceptorRefs={
					@InterceptorRef("loginInterceptorStack")
			}
	)
	public String login() throws Exception {
		if(WebUtils.hasSubmitParameter(request, "loginVendor")){
			System.out.println("Login Vendor");
			TmpVndHeader cek = authService.doLogin(model.getVndHeader());
			if(cek != null){
				SessionGetter.setSessionValue(cek, SessionGetter.VENDOR_SESSION);
				String exe = model.getLastUrl();
				
				if(cek.getVendorIsActivate() == 1){
					System.out.println("is Activated");
					if(cek.getVendorStatus().equals("P")){
						switch (cek.getVendorNextPage()) {
						case 1:
							return "page1";
						case 2:
							return "page2";
						case 3:
							return "page3";
						case 4:
							return "page4";
						case 5:
							return "page5";
						case 6:
							return "page6";
						case 7:
							return "page7";
						case 8:
							return "page8";
						case 9:
							return "page9";
						case 10:
							return "page10";
						default:
							return "page1";
						}
					}else if(cek.getVendorStatus().equals("V")){
						return "page10";
					}else if(cek.getVendorStatus().equals("R")){
						return "page9";
					}
					
					if(exe != null && !exe.equals("")){
						setUrl(exe);
						return "redirectOk";
					}
					
					if(cek.getVendorStatus().equals("A") || cek.getVendorStatus().equals("E")){
						return "dashboard";
					}
				}else{
					if(exe != null && !exe.equals("")){
						setUrl(exe);
						return "redirectOk";
					}
					System.out.println("Not Activated");
					return "notActivated";
				}
				
			}else{
				addActionError(getText("alert.loginNotValid")); 
			}
		}
		model.setLastUrl(request.getParameter("url"));
		if(request.getParameter("url") != null){
			addActionError(getText("login.error"));
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/login.jwebs", type="redirectAction")
			},value="vnd/logout",
			interceptorRefs={
					@InterceptorRef("logoutInterceptorStack")
			}
	)	
	public String keluar() throws Exception{
		SessionGetter.destroySessionValue(SessionGetter.VENDOR_SESSION);
		addActionMessage(getText("login.logout.alert"));
		//request.getSession().setMaxInactiveInterval(20);
		return SUCCESS;
	}	
	
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/language.jsp")
			},value="ajax/bahasa",
			interceptorRefs={
					@InterceptorRef("loginInterceptorStack")
			}
	)	
	public String bahasa() throws Exception{
		return SUCCESS;
	}	
}
