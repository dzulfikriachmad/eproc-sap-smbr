package com.eproc.v.reg.action;

import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.web.util.WebUtils;

import com.eproc.core.BaseController;
import com.eproc.utils.SessionGetter;
import com.eproc.v.reg.command.ComWorkInformationCommand;
import com.orm.model.AdmDistrict;
import com.orm.model.TmpVndDistrict;
import com.orm.model.TmpVndDistrictId;
import com.orm.model.TmpVndHeader;
import com.orm.model.TmpVndProduct;

import eproc.controller.AdmService;
import eproc.controller.AdmServiceImpl;
import eproc.controller.ComService;
import eproc.controller.ComServiceImpl;
import eproc.controller.VndAuthService;
import eproc.controller.VndAuthServiceImpl;
import eproc.controller.VndRegService;
import eproc.controller.VndRegServiceImpl;

@Namespace("/")
@ParentPackage("eprocVnd")
public class ComWorkInformationAction extends BaseController {
	private ComWorkInformationCommand model;
	private VndAuthService authService;
	private VndRegService regService;
	private ComService comService;
	private AdmService admService;
	
	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return model;
	}
	
	@Override
	public void prepare() throws Exception {
		model = new ComWorkInformationCommand();
		authService = new VndAuthServiceImpl();
		regService = new VndRegServiceImpl();
		comService = new ComServiceImpl();
		admService = new AdmServiceImpl();
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/reg/commodityInformation/comWork.jsp"),
					@Result(name=ERROR, location="vnd/reg/commodityInformation/comWork.jsp"),
					@Result(name="restricted", location="vnd/reg/restrictedPage.jsp"),
					@Result(name="next", location="vnd/regFinish", type="redirectAction")
			},value="vnd/commodityWorkingAreaInformation"
	)
	public String commodityWorkingAreaInformation() throws Exception{
		TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		TmpVndHeader tmp = authService.getTmpVndHeaderById(vndHeader.getVendorId());
		List<TmpVndProduct> prod = regService.listTmpVndProduct(tmp);
		
		if(tmp.getVendorNextPage() >= 9){
			model.setPageId(tmp.getVendorNextPage());
						
			if(WebUtils.hasSubmitParameter(request, "saveAndNext") || WebUtils.hasSubmitParameter(request, "save")){
				System.out.println(">>> PHASE 1");
				regService.deleteTmpVndDistrictByTmpVndHeader(tmp);
				for(Integer i:model.getComDistrict()){
					AdmDistrict dist = new AdmDistrict(i);
					
					//Set Id
					TmpVndDistrictId id = new TmpVndDistrictId();
					id.setVendorId(tmp.getVendorId());
					id.setDistrictId(dist.getId());
					
					//Set Property
					TmpVndDistrict district = new TmpVndDistrict();
					district.setAdmDistrict(dist);
					district.setTmpVndHeader(tmp);
					district.setId(id);
					
					regService.saveOrUpdateTmpVndDistrict(district);
				}
				System.out.println(">>> PHASE 2");
				addActionMessage(getText("eproc.alert.success"));
				//Simpan dan lanjut
				if(WebUtils.hasSubmitParameter(request, "saveAndNext")){
					if(prod.size() == 0){
						clearErrors();
						clearMessages();
						addActionError(getText("com.work.minimal"));
						
						model.setTmpVndHeader(tmp);
						model.setListTmpVndProduct(prod);
						model.setListAdmDistrict(admService.listAdmDistrict());
						
						return ERROR;
					}else if(model.getComDistrict().length == 0){
						clearErrors();
						clearMessages();
						addActionError(getText("com.work.minimal.working"));
						
						model.setTmpVndHeader(tmp);
						model.setListTmpVndProduct(prod);
						model.setListAdmDistrict(admService.listAdmDistrict());
					}else{
						if(tmp.getVendorNextPage() == 9){
							regService.saveNextPage(tmp.getVendorNextPage()+1, tmp);
						}
						return "next";
					}	
				}
			}	
			System.out.println(">>> PHASE 3");
			List<TmpVndDistrict> comDist = regService.listTmpVndDistrict(vndHeader);
			Integer ct[] = new Integer[comDist.size()];
			for (int i = 0; i < comDist.size(); i++) {
				ct[i] = comDist.get(i).getAdmDistrict().getId();
			}
			model.setComDistrict(ct);
			model.setTmpVndHeader(tmp);
			model.setListTmpVndProduct(prod);
			model.setListAdmDistrict(admService.listAdmDistrict());
			return SUCCESS;
		}else{
			return "restricted";
		}
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/reg/commodityInformation/listProd.jsp")
			},value="ajax/listTmpVndProduct"
	)
	public String listTmpVndProduct() throws Exception{
		TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		TmpVndHeader tmp = authService.getTmpVndHeaderById(vndHeader.getVendorId());
		model.setListTmpVndProduct(regService.listTmpVndProduct(tmp));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/reg/commodityInformation/setProd.jsp"),
					@Result(name=ERROR, location="error/wrong", type="redirectAction"),
					@Result(name="delete", location="ajax/listTmpVndProduct", type="redirectAction"),
					@Result(name="edit", location="vnd/reg/commodityInformation/setProd.jsp")
			},value="ajax/setTmpVndProduct"
	)
	public String setTmpVndProduct() throws Exception{
		TmpVndHeader tmpVndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		model.setListComCatalog(comService.listComCatalog());
		model.setListComGroup(comService.listComGroup(null));
		
		if(request.getParameter("id") != null && request.getParameter("id") != "" && request.getParameter("act") != null && request.getParameter("act").equals("edit")){
			int id = Integer.parseInt(request.getParameter("id"));
			model.setTmpVndProduct(regService.getTmpVndProductById(id, tmpVndHeader));
			return "edit";
			
		}else if (request.getParameter("id") != null && request.getParameter("id") != "" && request.getParameter("act") != null && request.getParameter("act").equals("delete")) {
			int id = Integer.parseInt(request.getParameter("id"));
			boolean cek = regService.deleteTmpVndProduct(regService.getTmpVndProductById(id, tmpVndHeader));
			if(cek){
				addActionMessage(getText("eproc.alert.delete"));
				return "delete";
			}else{
				addActionError(getText("eproc.alert.deletefail"));
				return ERROR;
			}
		}else{
			return SUCCESS;
		}
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="ajax/listTmpVndProduct", type="redirectAction")
			},value="ajax/submitTmpVndProduct"
	)
	public String submitTmpVndProduct() throws Exception{
		TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		TmpVndProduct prod = model.getTmpVndProduct();
		prod.setTmpVndHeader(vndHeader);
						
		boolean cek  = regService.saveOrUpdateTmpVndProduct(prod);
		if(cek){
			addActionMessage(getText("eproc.alert.success"));
		}else{
			addActionError(getText("eproc.alert.failed"));
		}
		return SUCCESS;
	}
	
}
