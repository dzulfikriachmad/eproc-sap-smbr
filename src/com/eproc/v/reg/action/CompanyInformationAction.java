package com.eproc.v.reg.action;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.hibernate.Query;
import org.springframework.web.util.WebUtils;

import com.bogcamp.mail.BogcampMail;
import com.bogcamp.mail.BogcampMailConfig;
import com.eproc.core.BaseController;
import com.eproc.utils.Crypto;
import com.eproc.utils.EmailSessionBean;
import com.eproc.utils.QueryUtil;
import org.hibernate.Query;
import org.hibernate.Session;
import com.eproc.utils.RandomCode;
import com.eproc.utils.SessionGetter;
import com.eproc.utils.StringEncrypter;
import com.eproc.v.reg.command.CompanyInformationCommand;
import com.orm.model.AdmCompanyType;
import com.orm.model.AdmMailConfig;
import com.orm.model.TmpVndAddress;
import com.orm.model.TmpVndCompanyType;
import com.orm.model.TmpVndCompanyTypeId;
import com.orm.model.TmpVndHeader;
import com.orm.model.TmpVndTambahan;

import eproc.controller.AdmServiceImpl;
import eproc.controller.VndAuthService;
import eproc.controller.VndAuthServiceImpl;
import eproc.controller.VndRegService;
import eproc.controller.VndRegServiceImpl;

@ParentPackage("eprocVnd")
@Namespace("/")
public class CompanyInformationAction extends BaseController {
	private CompanyInformationCommand model;
	private VndAuthService authService;
	private VndRegService regSevice;

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.model;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		model = new CompanyInformationCommand();
		authService = new VndAuthServiceImpl();
		regSevice = new VndRegServiceImpl();
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/reg/tos.jsp")
			},value="vnd/tos"
	)
	public String vndTos() throws Exception{
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/reg/aktivasiManual.jsp"),
					@Result(name="ok", location="vnd/login",type="redirectAction")
			},value="vnd/aktivasiManual",
			interceptorRefs={
					@InterceptorRef("loginInterceptorStack")
			}
	)
	public String vndAktivasi() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "save")){
				authService.aktivasiManual(model.getVndHeader());
				addActionMessage("Akun Anda Sudah Diaktivasi, Silahkan Login ");
				return "ok";
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/reg/tos.jsp"),
					@Result(name=ERROR, location="vnd/reg/registration.jsp"),
					@Result(name="next", location="vnd/reg/registration.jsp"),
					@Result(name="registerOk", location="vnd/confirmation", type="redirectAction")
			},value="vnd/registration",
			interceptorRefs={
					@InterceptorRef("loginInterceptorStack")
			}
	)
	public String vndReg() throws Exception{
		model.setListKantor(new AdmServiceImpl().getListDistrict(null));
		if(WebUtils.hasSubmitParameter(request, "tos")){
			if(request.getParameter("tos") != null && request.getParameter("tos").equals("Ya") || request.getParameter("tos").equals("Yes")){
				return "next";
			}
		}else if(WebUtils.hasSubmitParameter(request, "registerVendor")){
			TmpVndHeader cekMail = authService.getVndHeaderByMail(model.getVndHeader().getVendorEmail());
			TmpVndHeader cekLogin = authService.getVndHeaderByLogin(model.getVndHeader().getVendorLogin());
			boolean npwpCheck = authService.isExistNpwp(model.getVndHeader().getVendorNpwpNo());
			
			if(npwpCheck){
				addActionError("Vendor sudah pernah terdaftar");
				return ERROR;
			}else if(cekMail != null){
				addActionError("Email sudah pernah terdaftar, silahkan coba dengan email lain..");
				return ERROR;
			}else if(cekLogin != null){
				addActionError("Login tidak tersedia, silahkan coba dengan pilihan lain..");
				return ERROR;
			}else{
				TmpVndHeader vndHeader = model.getVndHeader();
				String password = model.getVndHeader().getVendorPassword();
				vndHeader.setVendorPassword(RandomCode.stringToMd5(password));
				
				try {
					//StringEncrypter enc = new StringEncrypter(StringEncrypter.PROC_MODULE_SECRET_KEY);
					Crypto enc = new Crypto(Crypto.PROC_MODULE_SECRET_KEY);
					AdmMailConfig config = SessionGetter.getMailConfig();
					String urlActivation = config.getServerName();
					urlActivation=urlActivation.concat(request.getContextPath().length() > 0 ?"/"+request.getContextPath()+"/vnd/confirmation.jwebs?request_locale=en_US&x=" + enc.encrypt(vndHeader.getVendorEmail()+";"+vndHeader.getVendorLogin()):"/vnd/confirmation.jwebs?request_locale=en_US&x=" + enc.encrypt(vndHeader.getVendorEmail()+";"+vndHeader.getVendorLogin()) );
					String content ="";
					Object obj[] = new Object[3];
					obj[0] = urlActivation;
					obj[1] = vndHeader.getVendorLogin();
					obj[2] = password;
					content = SessionGetter.getPropertiesValue("vnd.reg.email", obj);
					
					try {
						EmailSessionBean.sendMail("eProc PT Semen Baturaja (Persero)", content, vndHeader.getVendorEmail());
						addActionMessage(getText("vnd.reg.email.success"));
					} catch (Exception e) {
						addActionError(getText("vnd.reg.email.failed"));
						e.printStackTrace();
						return ERROR;
					}
				} catch (Exception e) {
					addActionError(getText("vnd.reg.email.failed"));
					return ERROR;
				}
				
				if(SessionGetter.isDrtJadwal(model.getVndHeader().getDistrict())){
					vndHeader.setDrt(1);
				}
				vndHeader.setRegisteredDate(new Date());
				boolean cek = authService.saveOrUpdateTmpVndHeader(vndHeader);
				if(!cek){
					addActionError(getText("eproc.alert.failed"));
					return ERROR;
				}
				
				
				SessionGetter.setSessionValue(vndHeader, SessionGetter.VENDOR_SESSION);
				
				return "registerOk";
			}
		}		
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/reg/regConfirm.jsp"),
					@Result(name=ERROR, location="vnd/reg/regConfirm.jsp"),
					@Result(name="activated", location="vnd/login", type="redirectAction")
			},value="vnd/confirmation",
			interceptorRefs={
					@InterceptorRef("activationInterceptorStack")
			}
	)
	public String vndRegConfirmation() throws Exception{
		if(request.getParameter("x") != null && request.getParameter("x") != ""){
			//StringEncrypter e = new StringEncrypter(StringEncrypter.PROC_MODULE_SECRET_KEY);
			Crypto e = new Crypto(Crypto.PROC_MODULE_SECRET_KEY);
			
			String secret = e.decrypt(request.getParameter("x"));
			
			String[] secretArray = secret.split(";");
			String email = secretArray[0];
			String login = secretArray[1];
			
			if(authService.getVndHeaderByMailAndLogin(email, login) == null){
				addActionError(getText("vnd.reg.email.code.failed"));
				return ERROR;
			}
			
			boolean cek = authService.setVendorActivation(login, email);
			if(cek){
				addActionMessage(getText("vnd.reg.email.active"));
				SessionGetter.destroySessionValue(SessionGetter.VENDOR_SESSION);
				return "activated";
			}else{
				addActionError(getText("eproc.alert.failed"));
				return ERROR;
			}
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/reg/companyInformation/comInf.jsp"),
					@Result(name=ERROR, location="vnd/reg/companyInformation/comInf.jsp"),
					@Result(name="next", location="vnd/legalInformation", type="redirectAction"),
					@Result(name="restricted", location="vnd/reg/restrictedPage.jsp")
			},value="vnd/companyInformation"
	)
	public String vndCompanyInformation() throws Exception{
		TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		TmpVndHeader tmp = authService.getTmpVndHeaderById(vndHeader.getVendorId());
				
		if(tmp.getVendorNextPage() >= 1){
			model.setPageId(tmp.getVendorNextPage());
			
			if(WebUtils.hasSubmitParameter(request, "save") || WebUtils.hasSubmitParameter(request, "saveAndNext")){				
				tmp.setVendorPrefix(model.getVndHeader().getVendorPrefix());
				tmp.setVendorPrefixOther(model.getVndHeader().getVendorPrefixOther());
				tmp.setVendorName(model.getVndHeader().getVendorName());
				tmp.setVendorSuffix(model.getVndHeader().getVendorSuffix());
				tmp.setVendorSuffixOther(model.getVndHeader().getVendorSuffixOther());
				
				tmp.setVendorContactName(model.getVndHeader().getVendorContactName());
				tmp.setVendorContactPos(model.getVndHeader().getVendorContactPos());
				tmp.setVendorContactPhone(model.getVndHeader().getVendorContactPhone());
				tmp.setVendorContactFax(model.getVndHeader().getVendorContactFax());
				tmp.setVendorContactEmail(model.getVndHeader().getVendorContactEmail());
				
				boolean cek = authService.saveOrUpdateTmpVndHeader(tmp);
				
				if(cek){
					addActionMessage(getText("eproc.alert.success"));
				}else{
					addActionError(getText("eproc.alert.failed"));
					
					model.setVndHeader(authService.getTmpVndHeaderById(vndHeader.getVendorId()));
					model.setListCompanyType(regSevice.listCompanyType());
					return ERROR;
				}
				
				regSevice.deleteTmpVndCompanyTypeByVndHeader(tmp);
				for (Integer i:model.getCompanyType()) {
					AdmCompanyType type =  new AdmCompanyType(i);
					
					//set id
					TmpVndCompanyTypeId id = new TmpVndCompanyTypeId();
					id.setCompanyType(type.getId());
					id.setVendorId(tmp.getVendorId());
					
					//set property lain
					TmpVndCompanyType tType = new TmpVndCompanyType();
					tType.setId(id);
					tType.setTmpVndHeader(tmp);
					tType.setAdmCompanyType(type);
					
					regSevice.saveOrUpdateTmpVndCompanyType(tType);				
				}
				
				List<TmpVndAddress> tmpAdd = regSevice.listTmpVndAddByVndHeader(tmp);
				if(WebUtils.hasSubmitParameter(request, "saveAndNext")){					
					if(tmpAdd.size() == 0){
						clearMessages();
						addActionError(getText("com.inf.address.size"));
					}else{
						if(tmp.getVendorNextPage() == 1)regSevice.saveNextPage(tmp.getVendorNextPage()+1, tmp);
						return "next";
					}					
				}
			}
			
			List<TmpVndCompanyType> companyType = regSevice.getTmpCompanyTypeByVndHeader(vndHeader);
			Integer ct[] = new Integer[companyType.size()];
			for (int i = 0; i < companyType.size(); i++) {
				ct[i] = companyType.get(i).getAdmCompanyType().getId();
			}

			model.setCompanyType(ct);
			model.setVndHeader(tmp);
			model.setListTmpVndAddress(regSevice.listTmpVndAddByVndHeader(tmp));
			model.setListTmpVndTambahan(regSevice.listTmpVndTambahanHeader(tmp));
			model.setListCompanyType(regSevice.listCompanyType());
			return SUCCESS;
		}else{
			return "restricted";
		}
		
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/reg/companyInformation/ajaxListComInf.jsp")
			},value="ajax/listTmpVndAdd"
	)
	public String listTmpVndAdd() throws Exception{
		TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		TmpVndHeader tmp = authService.getTmpVndHeaderById(vndHeader.getVendorId());
		model.setListTmpVndAddress(regSevice.listTmpVndAddByVndHeader(tmp));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/reg/companyInformation/ajaxListAfiliasi.jsp")
			},value="ajax/listTmpVndTambahan"
	)
	public String listTmpVndTambahan() throws Exception{
		TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		TmpVndHeader tmp = authService.getTmpVndHeaderById(vndHeader.getVendorId());
		model.setListTmpVndTambahan(regSevice.listTmpVndTambahanHeader(tmp));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/reg/companyInformation/ajaxSetComInf.jsp"),
					@Result(name=ERROR, location="error/wrong", type="redirectAction"),
					@Result(name="delete", location="ajax/listTmpVndAdd", type="redirectAction"),
					@Result(name="edit", location="vnd/reg/companyInformation/ajaxSetComInf.jsp")
			},value="ajax/setTmpVndAdd"
	)
	public String ajaxSetVndAdd() throws Exception{
		model.setListAdmCountry(regSevice.listAdmCountry());
		model.setListAddType(regSevice.listAddType());
		TmpVndHeader tmpVndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		
		if(request.getParameter("id") != null && request.getParameter("id") != "" && request.getParameter("act") != null && request.getParameter("act").equals("edit")){
			int id = Integer.parseInt(request.getParameter("id"));
			model.setTmpVndAdd(regSevice.getTmpVndAddById(id, tmpVndHeader));
			return "edit";
			
		}else if (request.getParameter("id") != null && request.getParameter("id") != "" && request.getParameter("act") != null && request.getParameter("act").equals("delete")) {
			int id = Integer.parseInt(request.getParameter("id"));
			boolean cek = regSevice.deleteTmpVndAdd(new TmpVndAddress(id, tmpVndHeader));
			if(cek){
				addActionMessage(getText("eproc.alert.delete"));
				return "delete";
			}else{
				addActionError(getText("eproc.alert.failed"));
				return ERROR;
			}
		}else{
			return SUCCESS;
		}
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/reg/companyInformation/ajaxSetComAffiliasi.jsp"),
					@Result(name=ERROR, location="error/wrong", type="redirectAction"),
					@Result(name="delete", location="ajax/listTmpVndTambahan", type="redirectAction"),
					@Result(name="edit", location="vnd/reg/companyInformation/ajaxSetComAffiliasi.jsp")
			},value="ajax/setTmpVndTambahan"
	)
	public String ajaxSetVndTambahan() throws Exception{
		model.setListAdmCountry(regSevice.listAdmCountry());
		TmpVndHeader tmpVndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
			if(request.getParameter("id") != null && request.getParameter("id") != "" && request.getParameter("act") != null && request.getParameter("act").equals("edit")){
				int id = Integer.parseInt(request.getParameter("id"));
				model.setTmpVndTambahan(regSevice.getTmpVndTambahanById(id,tmpVndHeader));
				return "edit";
				
			}else if (request.getParameter("act") != null && request.getParameter("act").equals("delete")) {
				boolean cek = regSevice.deleteTmpVndTambahan(model.getTmpVndTambahan());
				if(cek){
					addActionMessage(getText("eproc.alert.delete"));
					return "delete";
				}else{
					addActionError(getText("eproc.alert.failed"));
					return ERROR;
				}
			}else{
				return SUCCESS;
			}
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="ajax/listTmpVndAdd", type="redirectAction")
			},value="ajax/submitTmpVndAdd"
	)
	public String ajaxSubmitTmpVndAdd() throws Exception{
		TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		TmpVndAddress tmpAdd = model.getTmpVndAdd();
		tmpAdd.setTmpVndHeader(vndHeader);
		System.out.println(">>> SAVE SINI >>>>");
		boolean cek  = regSevice.saveOrUpdateTmpVndAdd(tmpAdd);
		if(cek){
			addActionMessage(getText("eproc.alert.success"));
		}else{
			addActionError(getText("eproc.alert.failed"));
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="ajax/listTmpVndTambahan", type="redirectAction")
			},value="ajax/submitTmpVndTambahan"
	)
	public String ajaxSubmitTmpVndTambahan() throws Exception{
		TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		model.getTmpVndTambahan().setVendorId(vndHeader);
		try {
			regSevice.saveTmpVndTambahan(model.getTmpVndTambahan());
			addActionMessage(getText("eproc.alert.success"));
		} catch (Exception e) {
			addActionError(getText("eproc.alert.failed"));
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/reg/finish.jsp"),
					@Result(name="restricted", location="vnd/reg/restrictedPage.jsp"),
					@Result(name="aktivated", location="vnd/dashboard", type="redirectAction")
			},value="vnd/regFinish"
	)
	public String regFinish() throws Exception{
		TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		TmpVndHeader tmp = authService.getTmpVndHeaderById(vndHeader.getVendorId());
		model.setPageId(tmp.getVendorNextPage());
		
		if(tmp.getVendorNextPage() >= 10){
			if(tmp.getVendorStatus().equals("V")){
				model.setStatus("V");
				model.setStatusText(getText("vnd.aktivasi"));
			}else if(tmp.getVendorStatus().equals("E")){
				model.setStatus("E");
				model.setStatusText(getText("vnd.edit"));
			}else if(tmp.getVendorStatus().equals("R")){
				model.setStatus("R");
				model.setStatusText(getText("vnd.route"));
			}else if(tmp.getVendorStatus().equals("P")){
				model.setStatus("P");
				model.setStatusText(getText("vnd.verifikasi"));
			}else{
				return "aktivated";
			}
			
			SessionGetter.destroySessionValue(SessionGetter.VENDOR_SESSION);
			return SUCCESS;
		}else{
			return "restricted";
		}
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/reg/route.jsp"),
					@Result(name="restricted", location="vnd/reg/restrictedPage.jsp")
			},value="vnd/routeToVendor"
	)
	public String routeToVendor() throws Exception{
		TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		TmpVndHeader tmp = authService.getTmpVndHeaderById(vndHeader.getVendorId());
		model.setPageId(tmp.getVendorNextPage());
		
		if(tmp.getVendorNextPage() < 10){
			return SUCCESS;
		}else{
			return "restricted";
		}
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="vnd/resetPassword.jsp"),
					@Result(name="list",location="vnd/resetPassword",type="redirectAction")
			},value="vnd/resetPassword",
			interceptorRefs={
					@InterceptorRef("loginInterceptorStack")
			}
			)
	public String resetPassword() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "reset")){
				String randomString ="ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
				model.getVndHeader().setVendorPassword(RandomStringUtils.random(8, randomString));
				authService.resetPassword(model.getVndHeader());
				addActionMessage(getText("eproc.alert.success"));
				return "list";
			}
		} catch (Exception e) {
			addActionError(getText("eproc.alert.failed"));
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="vnd/changePassword.jsp")
			},value="ajax/vnd/changePassword"
	)
	public String changePassword() throws Exception{
		try {
			if (WebUtils.hasSubmitParameter(request, "ubah")) {
				TmpVndHeader vndHeaders = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
				System.out.println(">>>>>>>>>>>>>> session password: "+vndHeaders.getVendorPassword());
				System.out.println(">>>>>>>>>>>>>>> Password input: "+RandomCode.stringToMd5(model.getOldPassword()));
				System.out.println(">>>>>>>>>>>>>>> password tanpa enkrip:"+ model.getOldPassword());
				if(!RandomCode.stringToMd5(model.getOldPassword()).equals(vndHeaders.getVendorPassword())){
					addActionError("Password Lama Tidak Cocok");
				}else{
					vndHeaders.setVendorPassword(RandomCode.stringToMd5(model.getVndHeader().getVendorPassword()));
					boolean save = authService.saveOrUpdateTmpVndHeader(vndHeaders);
					if (save) {
						addActionMessage("Password berhasil diubah");
					}else{
						addActionError("Password gagal diubah");
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
}
