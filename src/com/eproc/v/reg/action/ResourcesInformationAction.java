package com.eproc.v.reg.action;

import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.web.util.WebUtils;

import com.eproc.core.BaseController;
import com.eproc.utils.SessionGetter;
import com.eproc.v.reg.command.ResourcesInformationCommand;
import com.orm.model.TmpVndCert;
import com.orm.model.TmpVndHeader;
import com.orm.model.TmpVndSdm;

import eproc.controller.VndAuthService;
import eproc.controller.VndAuthServiceImpl;
import eproc.controller.VndRegService;
import eproc.controller.VndRegServiceImpl;

@Namespace("/")
@ParentPackage("eprocVnd")
public class ResourcesInformationAction extends BaseController {
	private ResourcesInformationCommand model;
	private VndAuthService authService;
	private VndRegService regService;
	
	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return model;
	}
	
	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		model = new ResourcesInformationCommand();
		authService = new VndAuthServiceImpl();
		regService = new VndRegServiceImpl();
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/reg/resourcesInformation/sdmInf.jsp"),
					@Result(name=ERROR, location="vnd/reg/resourcesInformation/sdmInf.jsp"),
					@Result(name="restricted", location="vnd/reg/restrictedPage.jsp"),
					@Result(name="next", location="vnd/companyFacility", type="redirectAction")
			},value="vnd/resourcesInformation"
	)
	public String resourcesInformation() throws Exception{
		TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		TmpVndHeader tmp = authService.getTmpVndHeaderById(vndHeader.getVendorId());
		List<TmpVndSdm> sdm = regService.listTmpVndSdm(tmp);
		
		if(tmp.getVendorNextPage() >= 6){
			model.setPageId(tmp.getVendorNextPage());
						
			if(WebUtils.hasSubmitParameter(request, "saveAndNext")){
				if(sdm.size() == 0){
					clearErrors();
					clearMessages();
					addActionError(getText("sdm.inf.minimal"));
					
					model.setTmpVndHeader(tmp);
					model.setListTmpVndSdm(sdm);
					
					return ERROR;
				}else{
					if(tmp.getVendorNextPage() == 6)regService.saveNextPage(tmp.getVendorNextPage()+1, tmp);
					return "next";
				}				
			}	
			
			model.setTmpVndHeader(tmp);
			model.setListTmpVndSdm(sdm);
			return SUCCESS;
		}else{
			return "restricted";
		}
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/reg/resourcesInformation/listSdm.jsp")
			},value="ajax/listTmpVndSdm"
	)
	public String listTmpVndSdm() throws Exception{
		TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		TmpVndHeader tmp = authService.getTmpVndHeaderById(vndHeader.getVendorId());
		model.setListTmpVndSdm(regService.listTmpVndSdm(tmp));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vnd/reg/resourcesInformation/setSdm.jsp"),
					@Result(name=ERROR, location="error/wrong", type="redirectAction"),
					@Result(name="delete", location="ajax/listTmpVndSdm", type="redirectAction"),
					@Result(name="edit", location="vnd/reg/resourcesInformation/setSdm.jsp")
			},value="ajax/setTmpVndSdm"
	)
	public String setTmpVndSdm() throws Exception{
		TmpVndHeader tmpVndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
				
		if(request.getParameter("id") != null && request.getParameter("id") != "" && request.getParameter("act") != null && request.getParameter("act").equals("edit")){
			int id = Integer.parseInt(request.getParameter("id"));
			model.setTmpVndSdm(regService.getTmpVndSdmById(id, tmpVndHeader));
			return "edit";
			
		}else if (request.getParameter("id") != null && request.getParameter("id") != "" && request.getParameter("act") != null && request.getParameter("act").equals("delete")) {
			int id = Integer.parseInt(request.getParameter("id"));
			boolean cek = regService.deleteTmpVndSdm(regService.getTmpVndSdmById(id, tmpVndHeader));
			if(cek){
				addActionMessage(getText("eproc.alert.delete"));
				return "delete";
			}else{
				addActionError(getText("eproc.alert.deletefail"));
				return ERROR;
			}
		}else{
			return SUCCESS;
		}
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="ajax/listTmpVndSdm", type="redirectAction")
			},value="ajax/submitTmpVndSdm"
	)
	public String submitTmpVndSdm() throws Exception{
		TmpVndHeader vndHeader = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		TmpVndSdm sdm = model.getTmpVndSdm();
		sdm.setTmpVndHeader(vndHeader);
						
		boolean cek  = regService.saveOrUpdateTmpVndSdm(sdm);
		if(cek){
			addActionMessage(getText("eproc.alert.success"));
		}else{
			addActionError(getText("eproc.alert.failed"));
		}
		return SUCCESS;
	}	
}
