package com.eproc.interceptor;

import org.apache.struts2.ServletActionContext;

import com.eproc.utils.SessionGetter;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;
import com.orm.model.TmpVndHeader;

public class VndLoginInterceptor implements Interceptor {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init() {
		// TODO Auto-generated method stub

	}

	@Override
	public String intercept(ActionInvocation act) throws Exception {
		System.out.println(">> Lewat Vendor Login Interceptor >>");
		TmpVndHeader acc = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		
		if(acc!=null){		
			ServletActionContext.getResponse().sendRedirect(ServletActionContext.getRequest().getContextPath().concat("/vnd/").concat("dashboard.jwebs"));
			return null;
		}else{
			return act.invoke();
		}
		
	}

}
