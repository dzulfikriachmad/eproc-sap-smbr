package com.eproc.interceptor;

import org.apache.struts2.ServletActionContext;

import com.eproc.utils.SessionGetter;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;
import com.orm.model.AdmUser;
import com.orm.model.TmpVndHeader;

public class LoginInterceptor implements Interceptor {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init() {
		// TODO Auto-generated method stub

	}

	@Override
	public String intercept(ActionInvocation act) throws Exception {
		System.out.println(">> Lewat Login Interceptor >>");
		AdmUser user = (AdmUser) SessionGetter.getSessionValue(SessionGetter.USER_SESSION);
		
		if(user!=null){		
			ServletActionContext.getResponse().sendRedirect(ServletActionContext.getRequest().getContextPath().concat("/").concat("dashboard.jwebs"));
			return null;
		}else{
			return act.invoke();
		}
	}

}
