package com.eproc.interceptor;

import org.hibernate.EmptyInterceptor;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import org.hibernate.type.Type;

import com.eproc.utils.SessionGetter;
import com.orm.model.TmpVndHeader;

public class OrmVndInterceptor extends EmptyInterceptor {
	/**
	 * when updated
	 */
	@Override
	public boolean onFlushDirty(Object entity, Serializable id,
			Object[] currentState, Object[] previousState,
			String[] propertyNames, Type[] types) {

		if((TmpVndHeader)SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION) == null){
			setValue(currentState, propertyNames, "updatedBy", "System");
		}else{
			setValue(currentState, propertyNames, "updatedBy", ((TmpVndHeader)SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION)).getVendorLogin());
		}
		
		setValue(currentState, propertyNames, "updatedDate", new Date());
		return true;
	}

	/**
	 * when insert
	 */
	@Override
	public boolean onSave(Object entity, Serializable id, Object[] state,
			String[] propertyNames, Type[] types) {
		
		if((TmpVndHeader)SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION) == null){
			setValue(state, propertyNames, "createdBy", "System");
		}else{
			setValue(state, propertyNames, "createdBy", ((TmpVndHeader)SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION)).getVendorLogin());
		}
		 
		 setValue(state, propertyNames, "createdDate", new Date());
		 return true;
	}
	
	private void setValue(Object[] currentState, String[] propertyNames,
            String propertyToSet, Object value) {
		int index = Arrays.asList(propertyNames).indexOf(propertyToSet);
		if (index >= 0) {
			currentState[index] = value;
		}
	}

}
