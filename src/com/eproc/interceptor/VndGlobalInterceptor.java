package com.eproc.interceptor;

import org.apache.struts2.ServletActionContext;

import com.eproc.utils.SessionGetter;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;
import com.orm.model.TmpVndHeader;

public class VndGlobalInterceptor implements Interceptor {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init() {
		// TODO Auto-generated method stub

	}

	@Override
	public String intercept(ActionInvocation act) throws Exception {
		System.out.println(">> Lewat Vnd Global Interceptor >>");
		
		TmpVndHeader acc = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		if(acc != null){
			if(acc.getVendorIsActivate() == 1){
				System.out.println(">> ENTER");
				return act.invoke();
			}else{
				System.out.println(">>> Account not activated");
				ServletActionContext.getResponse().sendRedirect(ServletActionContext.getRequest().getContextPath().concat("/vnd/").concat("confirmation.jwebs"));
				return null;
			}
		}else{
			System.out.println(">>> NULL");
			
			String requestUri =  ServletActionContext.getRequest().getRequestURL().toString();
			String requestQueryParam = ServletActionContext.getRequest().getQueryString();
			
			if(requestQueryParam != null){
				requestUri = requestUri + "?" + requestQueryParam;
			}

			ServletActionContext.getResponse().sendRedirect(ServletActionContext.getRequest().getContextPath().concat("/vnd/").concat("login.jwebs?url=").concat(requestUri));
			return null;
		}
	}
}
