package com.eproc.interceptor;

import org.apache.struts2.ServletActionContext;

import com.eproc.utils.SessionGetter;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;
import com.orm.model.AdmUser;
import com.orm.model.TmpVndHeader;

public class VndActivationInterceptor implements Interceptor {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init() {
		// TODO Auto-generated method stub

	}

	@Override
	public String intercept(ActionInvocation act) throws Exception {
		System.out.println(">> Lewat Vendor Activation Interceptor >>");
		TmpVndHeader acc = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		
		if(acc != null){
			if(acc.getVendorIsActivate() != 1){
				System.out.println(">> not activated >> "+acc.getVendorIsActivate());
				return act.invoke();
			}else{
				ServletActionContext.getResponse().sendRedirect(ServletActionContext.getRequest().getContextPath().concat("/vnd/").concat("dashboard.jwebs"));
				return null;
			}
		}else{
			String requestUri =  ServletActionContext.getRequest().getRequestURL().toString();
			String requestQueryParam = ServletActionContext.getRequest().getQueryString();
			
			if(requestQueryParam != null){
				requestUri = requestUri + "?" + requestQueryParam;
			}

			ServletActionContext.getResponse().sendRedirect(ServletActionContext.getRequest().getContextPath().concat("/vnd/").concat("login.jwebs?url=").concat(requestUri));
			return null;
		}
	}

}
