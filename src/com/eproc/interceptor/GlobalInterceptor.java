package com.eproc.interceptor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.mapper.ActionMapping;

import com.eproc.utils.SessionGetter;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;
import com.orm.model.AdmMenu;
import com.orm.model.AdmRoleMenu;
import com.orm.model.AdmUser;

public class GlobalInterceptor implements Interceptor {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public String intercept(ActionInvocation act) throws Exception {
		System.out.println(">> Lewat Global Interceptor >>");
		
		AdmUser user = (AdmUser) SessionGetter.getSessionValue(SessionGetter.USER_SESSION);
		if(user != null){
			if(user.getIsActive() == 1){
				System.out.println(">> ENTER");
				boolean exist = false;
				//cek apakah punya role ?
				ActionMapping am = ServletActionContext.getActionMapping();
				if(!am.getName().equalsIgnoreCase("dashboard")){
					String aksi = am.getName();
					if(am.getName().contains("ajax")){
						aksi = aksi.substring(5);
					}
					AdmRoleMenu arm = SessionGetter.getAdmRoleMenuByAction(aksi,user.getAdmEmployee());
					if(arm!=null){
						for(AdmMenu m:(List<AdmMenu>)SessionGetter.getSessionValue(SessionGetter.MENU_SESSION)){
							if(m.getId()==arm.getAdmMenu().getId()){
								exist = true;
							}
						}
						ActionContext.getContext().getValueStack().set("create", arm.getCreate());
						ActionContext.getContext().getValueStack().set("retrieve", arm.getRetrieve());
						ActionContext.getContext().getValueStack().set("update", arm.getUpdate());
						ActionContext.getContext().getValueStack().set("delete", arm.getDelete());
					}
					if(am.getName().startsWith("ajax") || am.getName().startsWith("bypass")){
						exist = true;
					}
				}
				if(exist){
					return act.invoke();
				}else{
					if(!am.getName().equalsIgnoreCase("dashboard")){
//						ServletActionContext.getResponse().sendRedirect(ServletActionContext.getRequest().getContextPath().concat("/").concat("dashboard.jwebs"));
//						return null;
						return act.invoke();
					}else{
						return act.invoke();
					}
				}
			}else{
				System.out.println(">>> Account not activated");
				ServletActionContext.getResponse().sendRedirect(ServletActionContext.getRequest().getContextPath().concat("/").concat("logout.jwebs"));
				return null;
			}
		}else{
			System.out.println(">>> NULL");
			
			String requestUri =  ServletActionContext.getRequest().getRequestURL().toString();
			String requestQueryParam = ServletActionContext.getRequest().getQueryString();
			
			if(requestQueryParam != null){
				requestUri = requestUri + "?" + requestQueryParam;
			}

			ServletActionContext.getResponse().sendRedirect(ServletActionContext.getRequest().getContextPath().concat("/").concat("login.jwebs?url=").concat(requestUri));
			return null;
		}
	}

}
