package com.eproc.interceptor;

import org.apache.struts2.ServletActionContext;

import com.eproc.utils.SessionGetter;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;
import com.orm.model.TmpVndHeader;

public class VndLogoutInterceptor implements Interceptor {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init() {
		// TODO Auto-generated method stub

	}

	@Override
	public String intercept(ActionInvocation act) throws Exception {
		System.out.println(">> Lewat Vendor Logout Interceptor >>");
		TmpVndHeader acc = (TmpVndHeader) SessionGetter.getSessionValue(SessionGetter.VENDOR_SESSION);
		
		if(acc != null){
			return act.invoke();
		}else{
			ServletActionContext.getResponse().sendRedirect(ServletActionContext.getRequest().getContextPath().concat("/vnd/").concat("login.jwebs"));
			return null;
		}
	}
}
