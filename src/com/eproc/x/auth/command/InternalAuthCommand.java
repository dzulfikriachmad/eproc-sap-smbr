package com.eproc.x.auth.command;

import java.io.Serializable;

import com.orm.model.AdmUser;

public class InternalAuthCommand implements Serializable {
	private AdmUser admUser;
	
	private String lastUrl;

	public AdmUser getAdmUser() {
		return admUser;
	}

	public void setAdmUser(AdmUser admUser) {
		this.admUser = admUser;
	}

	public String getLastUrl() {
		return lastUrl;
	}

	public void setLastUrl(String lastUrl) {
		this.lastUrl = lastUrl;
	}	
}
