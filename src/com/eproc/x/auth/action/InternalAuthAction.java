package com.eproc.x.auth.action;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.web.util.WebUtils;

import com.eproc.core.BaseController;
import com.eproc.sap.controller.IntegrationService;
import com.eproc.sap.controller.IntegrationServiceImpl;
import com.eproc.utils.SessionGetter;
import com.eproc.x.auth.command.InternalAuthCommand;
import com.orm.model.AdmUser;

import eproc.controller.AdmService;
import eproc.controller.AdmServiceImpl;

@ParentPackage("eproc")
@Namespace("/")
public class InternalAuthAction extends BaseController {
	private InternalAuthCommand model;
	private AdmService admService;
	
	private String url;

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return model;
	}

	@Override
	public void prepare() throws Exception {
		model = new InternalAuthCommand();
		admService = new AdmServiceImpl();
	}	
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Action(
			results={
					@Result(name=SUCCESS, location="adm/login.jsp"),
					@Result(name="dashboard", location="dashboard", type="redirectAction"),
					@Result(name="redirectOk", location="${url}", type="redirect"),
					@Result(name="vnd",location="vnd/login",type="redirectAction")
			}, value="login",
			interceptorRefs={
					@InterceptorRef("loginInterceptorStack")
			}
	)
	public String internalLogin() throws Exception{
		try {
//			if(!SessionGetter.localIpAddress(request.getRemoteAddr())){
//				return "vnd";
//			}
			
			if(WebUtils.hasSubmitParameter(request, "loginInternal")){
				AdmUser cek = admService.doLogin(model.getAdmUser());
				/*IntegrationService s = new IntegrationServiceImpl();
				s.generateTokenAndCookies();*/
				if(cek != null){
					String exe = model.getLastUrl();
					SessionGetter.setSessionValue(cek, SessionGetter.USER_SESSION);
					SessionGetter.setSessionValue(admService.getListRoleMenuByEmployee(cek.getAdmEmployee()), SessionGetter.MENU_SESSION);
					if(cek.getIsActive()==0){
						addActionError(getText("alert.userNotAktive"));
						return SUCCESS;
					}
					if(exe != null && !exe.equals("")){
						setUrl(exe);
						return "redirectOk";
					}
					return "dashboard";
				}else{
					addActionError(getText("alert.loginNotValid")); 
				}
			}
			model.setLastUrl(request.getParameter("url"));
			if(request.getParameter("url") != null){
				addActionError(getText("login.error"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "vnd";
		}
		
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="login", type="redirectAction")
			},value="logout",
			interceptorRefs={
					@InterceptorRef("logoutInterceptorStack")
			}
	)	
	public String keluar() throws Exception{
		SessionGetter.destroySessionValue(SessionGetter.USER_SESSION);
		addActionMessage(getText("login.logout.alert"));
		//request.getSession().setMaxInactiveInterval(20);
		return SUCCESS;
	}	
}
