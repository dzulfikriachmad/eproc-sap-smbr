package com.eproc.x.oe.command;

import java.io.File;
import java.io.Serializable;
import java.util.List;

import com.orm.model.AdmEmployee;
import com.orm.model.AdmUom;
import com.orm.model.AdmUser;
import com.orm.model.HistMainComment;
import com.orm.model.PrcMainHeader;
import com.orm.model.PrcMainItem;

public class OwnerEstimateCommand implements Serializable {
	private List<PrcMainHeader> listPrcMainHeader;
	private List<AdmUser> listEstimator;
	private List<HistMainComment> listHistMainComment;
	private List<PrcMainItem> listPrcMainItem;
	private List<AdmUom> listUom;
	
	private PrcMainHeader prcMainHeader;
	private AdmEmployee admEmployee;
	private HistMainComment histMainComment;
	
	private File docsKomentar;
	private String docsKomentarFileName;
	private String docsKomentarContentType;

	public List<PrcMainHeader> getListPrcMainHeader() {
		return listPrcMainHeader;
	}

	public void setListPrcMainHeader(List<PrcMainHeader> listPrcMainHeader) {
		this.listPrcMainHeader = listPrcMainHeader;
	}

	public PrcMainHeader getPrcMainHeader() {
		return prcMainHeader;
	}

	public void setPrcMainHeader(PrcMainHeader prcMainHeader) {
		this.prcMainHeader = prcMainHeader;
	}

	public List<AdmUser> getListEstimator() {
		return listEstimator;
	}

	public void setListEstimator(List<AdmUser> listAdmUser) {
		this.listEstimator = listAdmUser;
	}

	public AdmEmployee getAdmEmployee() {
		return admEmployee;
	}

	public void setAdmEmployee(AdmEmployee admEmployee) {
		this.admEmployee = admEmployee;
	}

	public List<HistMainComment> getListHistMainComment() {
		return listHistMainComment;
	}

	public void setListHistMainComment(List<HistMainComment> listHistMainComment) {
		this.listHistMainComment = listHistMainComment;
	}

	public List<PrcMainItem> getListPrcMainItem() {
		return listPrcMainItem;
	}

	public void setListPrcMainItem(List<PrcMainItem> listPrcMainItem) {
		this.listPrcMainItem = listPrcMainItem;
	}

	public HistMainComment getHistMainComment() {
		return histMainComment;
	}

	public void setHistMainComment(HistMainComment histMainComment) {
		this.histMainComment = histMainComment;
	}

	public File getDocsKomentar() {
		return docsKomentar;
	}

	public void setDocsKomentar(File docsKomentar) {
		this.docsKomentar = docsKomentar;
	}

	public String getDocsKomentarFileName() {
		return docsKomentarFileName;
	}

	public void setDocsKomentarFileName(String docsKomentarFileName) {
		this.docsKomentarFileName = docsKomentarFileName;
	}

	public String getDocsKomentarContentType() {
		return docsKomentarContentType;
	}

	public void setDocsKomentarContentType(String docsKomentarContentType) {
		this.docsKomentarContentType = docsKomentarContentType;
	}

	public List<AdmUom> getListUom() {
		return listUom;
	}

	public void setListUom(List<AdmUom> listUom) {
		this.listUom = listUom;
	}
	
}
