package com.eproc.x.oe.action;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.web.util.WebUtils;

import com.eproc.core.BaseController;
import com.eproc.sap.controller.IntegrationService;
import com.eproc.sap.controller.IntegrationServiceImpl;
import com.eproc.sap.dto.ResultDto;
import com.eproc.utils.SessionGetter;
import com.eproc.utils.UploadUtil;
import com.eproc.x.oe.command.OwnerEstimateCommand;
import com.orm.model.AdmEmployee;
import com.orm.model.PrcMainHeader;
import com.orm.model.PrcMainItem;

import eproc.controller.AdmService;
import eproc.controller.AdmServiceImpl;
import eproc.controller.PrcService;
import eproc.controller.PrcServiceImpl;

@ParentPackage("eproc")
@Namespace("/")
public class OwnerEstimateAction extends BaseController {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private OwnerEstimateCommand model;
	private PrcService prcService;
	private AdmService admService;
	private IntegrationService integrationService;

	@Override
	public Object getModel() {
		return model;
	}

	@Override
	public void prepare() throws Exception {
		model = new OwnerEstimateCommand();
		prcService = new PrcServiceImpl();
		admService = new AdmServiceImpl();
		integrationService = new IntegrationServiceImpl();
	}
	
	@Action(
			results={
					@Result(name = SUCCESS, location = "proc/oe/listOeEstimator.jsp"),
					@Result(name = "view", location = "proc/oe/viewOeEstimator.jsp"),
//					@Result(name = "ok", location = "oe/estimator", type="redirectAction")
					@Result(name = "ok", location = "dashboard", type="redirectAction")
			}, value = "oe/estimator"
	)
	public String selectEstimator() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "proses")){
				PrcMainHeader prcMainHeader = prcService.getPrcMainIHeaderById(model.getPrcMainHeader().getPpmId());			
				AdmEmployee admEmployee = SessionGetter.getUser().getAdmEmployee();
				model.setPrcMainHeader(prcMainHeader);
				model.setAdmEmployee(admEmployee);
				model.setListHistMainComment(prcService.listHistMainCommentByPrcHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId())));
				List<PrcMainItem> listItem = prcService.listPrcMainItemByHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId()));
				model.setListPrcMainItem(listItem);
				model.setListEstimator(prcService.listEstimator(null));
				return "view";
			}else if(WebUtils.hasSubmitParameter(request, "save")){
				model.getHistMainComment().setAksi("Pemilihan Estimator");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("oe.1", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("oe.2", null));
				prcService.saveEstimator(model.getPrcMainHeader(),model.getHistMainComment(),model.getDocsKomentar(),model.getDocsKomentarFileName());
				addActionMessage("Estimator Berhasil dipilih");
				return "ok";
			}
			
			model.setListPrcMainHeader(prcService.listPrcMainHeaderOEPemilihanEstimator(null));
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
			return "ok";
		}
		
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name = SUCCESS, location = "proc/oe/listOeCreate.jsp"),
					@Result(name = "view", location = "proc/oe/setOeCreate.jsp"),
					@Result(name = "save", location = "oe/create", type="redirectAction"),
					@Result(name = "revisiee", location = "dashboard", type="redirectAction"),
					@Result(name = "ok", location = "dashboard", type="redirectAction")
			}, value = "oe/create"
	)
	public String createOe() throws Exception{		
		try {
			if(WebUtils.hasSubmitParameter(request, "proses")){
				PrcMainHeader prcMainHeader = prcService.getPrcMainIHeaderById(model.getPrcMainHeader().getPpmId());			
				AdmEmployee admEmployee = admService.getAdmEmployeeById(prcMainHeader.getAdmUserByPpmCurrentUser().getId());
				model.setPrcMainHeader(prcMainHeader);
				model.setAdmEmployee(admEmployee);
				model.setListHistMainComment(prcService.listHistMainCommentByPrcHeader(new PrcMainHeader(prcMainHeader.getPpmId())));
				List<PrcMainItem> listItem = prcService.listPrcMainItemByHeader(new PrcMainHeader(prcMainHeader.getPpmId()));
				model.setListPrcMainItem(listItem);
				model.setListUom(admService.getListUom(null));
				return "view";
			}else if(WebUtils.hasSubmitParameter(request, "save")){
				prcService.saveOeItems(model.getListPrcMainItem());
				addActionMessage("Harga OE Berhasil disimpan");
				return "save";
			}else if(WebUtils.hasSubmitParameter(request, "saveAndFinish")){
				model.getHistMainComment().setAksi("Pembuatan OE");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("oe.2", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("oe.3", null));
				prcService.saveOeFinis(model.getPrcMainHeader(),model.getListPrcMainItem(),model.getHistMainComment(),model.getDocsKomentar(),model.getDocsKomentarFileName());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			}else if(WebUtils.hasSubmitParameter(request, "sap")){
				integrationService.changePr(model.getListPrcMainItem(), model.getPrcMainHeader());
				return "ok";
			}
			else if(WebUtils.hasSubmitParameter(request, "revisiee")){
				model.getHistMainComment().setAksi("Revisi EE");
				model.getHistMainComment().setProsesAksi("Revisi EE");
				model.getHistMainComment().setCreatedBy("Revisi EE");
				prcService.saveRevisiEEInit(model.getPrcMainHeader(),model.getListPrcMainItem(),model.getHistMainComment(),model.getDocsKomentar(),model.getDocsKomentarFileName());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			}else if(WebUtils.hasSubmitParameter(request, "validate")){
				ResultDto result = integrationService.changePr(model.getListPrcMainItem(), model.getPrcMainHeader());
				addActionMessage("SUCCESS "+result.getMessage());
				return "view";
			}else if(WebUtils.hasSubmitParameter(request, "upload")) {
				try {
					UploadUtil.uploadDocsFile(model.getDocsKomentar(),model.getDocsKomentarFileName());
					addActionMessage("SUCCESS ");
				}catch(Exception e) {
					StringWriter errors = new StringWriter();
					e.printStackTrace(new PrintWriter(errors));
					addActionMessage(errors.toString());
					e.printStackTrace();
				}
				return "view";
			}
			
			model.setListPrcMainHeader(prcService.listPrcMainHeaderOE(null));
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	/** update oe ditengah **/
	@Action(
			results={
					@Result(name = SUCCESS, location = "proc/oe/listOeCreateManual.jsp"),
					@Result(name = "view", location = "proc/oe/setOeCreateManual.jsp"),
					@Result(name = "save", location = "oe/createManual", type="redirectAction"),
					@Result(name = "ok", location = "dashboard", type="redirectAction")
			}, value = "oe/createManual"
	)
	public String createOeManual() throws Exception{		
		try {
			if(WebUtils.hasSubmitParameter(request, "proses")){
				PrcMainHeader prcMainHeader = prcService.getPrcMainIHeaderById(model.getPrcMainHeader().getPpmId());			
				AdmEmployee admEmployee = admService.getAdmEmployeeById(prcMainHeader.getAdmUserByPpmCurrentUser().getId());
				model.setPrcMainHeader(prcMainHeader);
				model.setAdmEmployee(admEmployee);
				model.setListHistMainComment(prcService.listHistMainCommentByPrcHeader(new PrcMainHeader(prcMainHeader.getPpmId())));
				List<PrcMainItem> listItem = prcService.listPrcMainItemByHeader(new PrcMainHeader(prcMainHeader.getPpmId()));
				model.setListPrcMainItem(listItem);
				model.setListUom(admService.getListUom(null));
				return "view";
			}else if(WebUtils.hasSubmitParameter(request, "save")){
				prcService.saveOeItems(model.getListPrcMainItem());
				addActionMessage("Harga OE Berhasil disimpan");
				return "save";
			}else if(WebUtils.hasSubmitParameter(request, "saveAndFinish")){
				model.getHistMainComment().setAksi("Update OE");
				prcService.saveOeFinisManual(model.getPrcMainHeader(),model.getListPrcMainItem(),model.getHistMainComment(),model.getDocsKomentar(),model.getDocsKomentarFileName());
				addActionMessage("Data Sudah Disimpan");
				return "save";
			}
			
			model.setListPrcMainHeader(prcService.listPrcMainHeaderOEManual(null));
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="proc/oe/listOeApproval.jsp"),
					@Result(name="view",location="proc/oe/viewOeApproval.jsp"),
//					@Result(name="ok",location="oe/approval",type="redirectAction")
					@Result(name = "ok", location = "dashboard", type="redirectAction")
			},value="oe/approval"
			)
	public String approvalOe() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "proses")){
				PrcMainHeader prcMainHeader = prcService.getPrcMainIHeaderById(model.getPrcMainHeader().getPpmId());			
				AdmEmployee admEmployee = admService.getAdmEmployeeById(prcMainHeader.getAdmUserByPpmCurrentUser().getId());
				model.setPrcMainHeader(prcMainHeader);
				model.setAdmEmployee(admEmployee);
				model.setListHistMainComment(prcService.listHistMainCommentByPrcHeader(new PrcMainHeader(prcMainHeader.getPpmId())));
				List<PrcMainItem> listItem = prcService.listPrcMainItemByHeader(new PrcMainHeader(prcMainHeader.getPpmId()));
				model.setListPrcMainItem(listItem);
				return "view";
			}else if(WebUtils.hasSubmitParameter(request, "revisi")){
				model.getHistMainComment().setAksi("Revisi");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("oe.3", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("oe.-2", null));
				prcService.revisiOE(model.getPrcMainHeader(),model.getListPrcMainItem(),model.getHistMainComment(),model.getDocsKomentar(),model.getDocsKomentarFileName());
				addActionMessage("Data Sudah Disimpan disimpan");
				return "ok";
			}else if(WebUtils.hasSubmitParameter(request, "saveAndFinish")){
				model.getHistMainComment().setAksi("Persetujuan OE");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("oe.3", null));
				prcService.setujuOE(model.getPrcMainHeader(),model.getListPrcMainItem(),model.getHistMainComment(),model.getDocsKomentar(),model.getDocsKomentarFileName());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			}
			
			model.setListPrcMainHeader(prcService.listPrcMainHeaderApprovalOE(null));
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
			return "ok";
		}
		return SUCCESS;
	}
	
	
	@Action(
			results={
					@Result(name=SUCCESS,location="proc/oe/listEeRevisi.jsp"),
					@Result(name="view",location="proc/oe/viewEERevisi.jsp"),
					@Result(name = "ok", location = "dashboard", type="redirectAction")
			},value="oe/revisiEE"
			)
	public String revisiEe() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "saveAndFinish")){
				model.getHistMainComment().setAksi("Revisi EE");
				model.getHistMainComment().setProsesAksi("Revisi EE");
				model.getHistMainComment().setCreatedBy("Persetujuan Revisi EE");
				prcService.saveEEFinis(model.getPrcMainHeader(),model.getListPrcMainItem(),model.getHistMainComment(),model.getDocsKomentar(),model.getDocsKomentarFileName());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			}else if(WebUtils.hasSubmitParameter(request, "proses")){
				PrcMainHeader prcMainHeader = prcService.getPrcMainIHeaderById(model.getPrcMainHeader().getPpmId());			
				AdmEmployee admEmployee = admService.getAdmEmployeeById(prcMainHeader.getAdmUserByPpmCurrentUser().getId());
				model.setPrcMainHeader(prcMainHeader);
				model.setAdmEmployee(admEmployee);
				model.setListHistMainComment(prcService.listHistMainCommentByPrcHeader(new PrcMainHeader(prcMainHeader.getPpmId())));
				List<PrcMainItem> listItem = prcService.listPrcMainItemByHeader(new PrcMainHeader(prcMainHeader.getPpmId()));
				model.setListPrcMainItem(listItem);
				model.setListUom(admService.getListUom(null));
				return "view";
			}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
		}
		
		model.setListPrcMainHeader(prcService.listPrcMainHeaderRevisiEEInit(null));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="proc/oe/listEeApproval.jsp"),
					@Result(name="view",location="proc/oe/viewEeApproval.jsp"),
					@Result(name = "ok", location = "dashboard", type="redirectAction")
			},value="oe/revisiEEApproval"
			)
	public String approvalEe() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "proses")){
				PrcMainHeader prcMainHeader = prcService.getPrcMainIHeaderById(model.getPrcMainHeader().getPpmId());			
				AdmEmployee admEmployee = admService.getAdmEmployeeById(prcMainHeader.getAdmUserByPpmCurrentUser().getId());
				model.setPrcMainHeader(prcMainHeader);
				model.setAdmEmployee(admEmployee);
				model.setListHistMainComment(prcService.listHistMainCommentByPrcHeader(new PrcMainHeader(prcMainHeader.getPpmId())));
				List<PrcMainItem> listItem = prcService.listPrcMainItemByHeader(new PrcMainHeader(prcMainHeader.getPpmId()));
				model.setListPrcMainItem(listItem);
				return "view";
			}else if(WebUtils.hasSubmitParameter(request, "revisi")){
				model.getHistMainComment().setAksi("Revisi EE");
				model.getHistMainComment().setProsesAksi("Revisi EE");
				model.getHistMainComment().setCreatedBy("Revisi EE");
				prcService.revisiEEApproval(model.getPrcMainHeader(),model.getListPrcMainItem(),model.getHistMainComment(),model.getDocsKomentar(),model.getDocsKomentarFileName());
				addActionMessage("Data Sudah Disimpan disimpan");
				return "ok";
			}else if(WebUtils.hasSubmitParameter(request, "saveAndFinish")){
				model.getHistMainComment().setAksi("Persetujuan Revisi EE");
				model.getHistMainComment().setProsesAksi("Setuju Revisi EE");
				prcService.setujuEEApproval(model.getPrcMainHeader(),model.getListPrcMainItem(),model.getHistMainComment(),model.getDocsKomentar(),model.getDocsKomentarFileName());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			}
			
			model.setListPrcMainHeader(prcService.listPrcMainHeaderApprovalOE(null));
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
}
