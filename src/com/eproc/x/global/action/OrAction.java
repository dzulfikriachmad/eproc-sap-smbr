package com.eproc.x.global.action;

import org.apache.commons.net.util.Base64;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.web.util.WebUtils;

import com.eproc.core.BaseController;
import com.eproc.utils.UploadUtil;
import com.eproc.x.global.command.OrCommand;
import com.orm.model.AdmDept;
import com.orm.model.OrHeader;
import com.orm.model.OrHeaderId;

import eproc.controller.AdmServiceImpl;
import eproc.controller.OrService;
import eproc.controller.OrServiceImpl;
import eproc.controller.PrcService;
import eproc.controller.PrcServiceImpl;

@Namespace("/")
@ParentPackage("eproc")
public class OrAction extends BaseController {

	private OrCommand model;
	private OrService service;
	private PrcService prcService;
	@Override
	public Object getModel() {
		return this.model;
	}
	@Override
	public void prepare() throws Exception {
		model = new OrCommand();
		service = new OrServiceImpl();
		prcService = new PrcServiceImpl();
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="or/daftarOr.jsp"),
					@Result(name="view",location="or/viewOr.jsp"),
					@Result(name="ok",location="pr/daftarPr", type="redirectAction"),
					@Result(name="manual",location="procurement/dispatcherManual", type="redirectAction",params= {"9e13ce5cbca0a164a2b890816aaabec1", "%{encprnum}", "a53e2de861ba18f78f3eeaad85e2c1cf", "%{encorh}", "48b8379d7bb37243ed25c4fd8e7e4b64","%{encsite}"}),
					@Result(name ="json", type = "json")
			},value="pr/daftarPr"
			)
	public String orTransform() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "proses")){
				model.setOrHeader(service.getOrHeaderById(model.getOrHeader().getId()));
				model.setListOrDetail(service.getOrDetailByOrHeader(model.getOrHeader()));
				model.setListAdmJenis(service.getListAdmJenis(model.getListOrDetail().get(0).getItemCategory()));
				model.setListGroup(service.getListComGrup(null));
				String prnumber = String.valueOf(model.getOrHeader().getId().getNomorOr()); 
				String orh = model.getOrHeader().getId().getOrH();
				String headersite = model.getOrHeader().getId().getHeaderSite();
				byte[]   bytesEncodedPr = Base64.encodeBase64(prnumber.getBytes());//encoding part
				byte[]   bytesEncodedOrh = Base64.encodeBase64(orh.getBytes());//encoding part
				byte[]   bytesEncodedSite = Base64.encodeBase64(headersite.getBytes());//encoding part
				String encprnum = new String(bytesEncodedPr);
				String encorh = new String(bytesEncodedOrh);
				String encsite = new String(bytesEncodedSite);
				model.setEncprnum(encprnum);
				model.setEncorh(encorh);
				model.setEncsite(encsite);
				AdmDept satker = new AdmServiceImpl().getAdmDeptByCode(model.getOrHeader().getPurchasingGroup());
				model.setAdmDept(satker);
				if(model.getOrHeader().getAttachment()!="" || model.getOrHeader().getAttachment()!=null) {
					service.getDocument(model.getOrHeader(), model.getListOrDetail()); //Uncomment kalo deploy
				}
				return "view";
			}else if(WebUtils.hasSubmitParameter(request, "transformPrSAP")) {
				service.generatePrSap(model.getOrHeader(),model.getPrcMainHeader());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			}else if(WebUtils.hasSubmitParameter(request, "batal")){
				service.batalOR(model.getOrHeader(),model.getPrcMainHeader());
				addActionMessage("Data Sudah di Batalkan");
				return "ok";
			}else if(WebUtils.hasSubmitParameter(request, "manual")) {
				//service.generatePrManual(model.getOrHeader(),model.getPrcMainHeader());
				
				
				 
				return "manual";
			}
			/*else if(WebUtils.hasSubmitParameter(request, "transform")){
				service.generatePr(model.getOrHeader(),model.getPrcMainHeader());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			}*/
			/*}
			else if(WebUtils.hasSubmitParameter(request, "generate")){
				SchedulerOrBatch rs = new SchedulerOrBatch();
				rs.prosesBatchHeaderOr();
				rs.prosesBatchHeaderZr();
				addActionMessage("Data Sudah Di Proses");
				return "ok";*/
			
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
		}
		model.setLisOrHeader(service.listActiveOrHeader(null));
		return SUCCESS;
	}
	

	
	
	@Action(
			results={
					@Result(name=SUCCESS,location="or/ajax/daftarOr.jsp")
			},value="ajax/pr/daftarPr"
			)
	public String ajaxDaftarOr() throws Exception{
		try {
//			model.setResultSize(service.countActiveOr(model.getOrHeader()));
//			model.setLisOrHeader(service.listActiveOrHeader(model.getOrHeader(),model.getPage(request),10));
			model.setLisOrHeader(service.listActiveOrHeader(model.getOrHeader()));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	@Action(
			results={
					@Result(name="view",location="proc/viewPengadaanManualDispatcher.jsp"),
					@Result(name = "ok", location = "dashboard", type="redirectAction")
			},value="procurement/dispatcherManual"
			)
	public String prManualDispatcher() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "save")) {
				if(model.getDocsKomentar()!=null){
					String docName = UploadUtil.uploadDocsFile(model.getDocsKomentar(), model.getDocsKomentarFileName());
					model.getHistMainComment().setDocument(docName);
				}
				service.prManualDispatcher(model.getOrHeader(), model.getPrcMainHeader(), model.getPrcManDocument(),
						model.getPrcManDocumentCategory(),  model.getHistMainComment(), 
						model.getPrcManDocumentDescription(), model.getPrcManDocumentFileName(),
						model.getDocsKomentar(),model.getDocsKomentarFileName());
				addActionMessage("Data berhasil disimpan");
				return "ok";
			}
			if(request.getParameter("9e13ce5cbca0a164a2b890816aaabec1")!=null) {
				String prnumb=request.getParameter("9e13ce5cbca0a164a2b890816aaabec1");
				String orh=request.getParameter("a53e2de861ba18f78f3eeaad85e2c1cf");
				String site=request.getParameter("48b8379d7bb37243ed25c4fd8e7e4b64");
				byte[] prnumbDecoded= Base64.decodeBase64(prnumb);//decoding part
				String decodedPr=new String(prnumbDecoded);
				byte[] orhDecoded= Base64.decodeBase64(orh);//decoding part
				String decodedOrh=new String(orhDecoded);
				byte[] siteDecoded= Base64.decodeBase64(site);//decoding part
				String decodedSite=new String(siteDecoded);
				
				OrHeaderId id = new OrHeaderId();
				id.setNomorOr(Long.valueOf(decodedPr));
				id.setOrH(decodedOrh);
				id.setHeaderSite(decodedSite);
				OrHeader orHeader = new OrHeader();
				orHeader.setId(id);
				model.setOrHeader(service.getOrHeaderById(orHeader.getId()));
				model.setListOrDetail(service.getOrDetailByOrHeader(model.getOrHeader()));
				model.setListAdmJenis(service.getListAdmJenis(model.getListOrDetail().get(0).getItemCategory()));
				model.setListGroup(service.getListComGrup(null));
				AdmDept satker = new AdmServiceImpl().getAdmDeptByCode(model.getOrHeader().getPurchasingGroup());
				model.setAdmDept(satker);
				return "view";
			}
				
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
			return "ok";
		}
		return "ok";
		
	}
	
}
