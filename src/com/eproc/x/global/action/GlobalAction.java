package com.eproc.x.global.action;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Actions;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.web.util.WebUtils;

import com.eproc.core.BaseController;
import com.eproc.utils.EmailSessionBean;
import com.eproc.utils.QueryUtil;
import com.eproc.utils.SessionGetter;
import com.eproc.utils.UploadUtil;
import com.eproc.x.global.command.GlobalCommand;
import com.orm.model.AdmEmployee;
import com.orm.model.AdmProsesHirarki;
import com.orm.model.AdmRoleMenu;
import com.orm.model.AdmUser;
import com.orm.model.PrcMainHeader;
import com.orm.model.PrcMainItem;
import com.orm.model.PrcMsgChat;
import com.orm.model.PrcMsgSanggahan;
import com.orm.model.PrcTemplateDetail;
import com.orm.model.TmpVndHeader;
import com.orm.model.VndHeader;

import eproc.controller.AdmService;
import eproc.controller.AdmServiceImpl;
import eproc.controller.PrcService;
import eproc.controller.PrcServiceImpl;
import eproc.controller.ToolsService;
import eproc.controller.ToolsServiceImpl;
import eproc.controller.VendorServiceImpl;

@ParentPackage("eproc")
@Namespace("/")
public class GlobalAction extends BaseController {
	private GlobalCommand model;
	private AdmService admService;
	private PrcService prcService;
	private ToolsService toolsService;

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return this.model;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		model = new GlobalCommand();
		admService = new AdmServiceImpl();
		prcService = new PrcServiceImpl();
		toolsService = new ToolsServiceImpl();
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="index.jsp")
			},value = "dashboard"
	)
	public String index() throws Exception{
		model.setListTodo(prcService.getListTodoUser(QueryUtil.todoListUserSpphPr(SessionGetter.getUser())));
		model.setListTodoPanitiaPp(prcService.getListTodoUser(QueryUtil.todoListPanitiaPP(SessionGetter.getUser())));
		model.setListTodoKontrak(prcService.getListTodoUserKontrak(QueryUtil.todoListKontrak(SessionGetter.getUser())));
//		model.setListPrcMainHeader(prcService.listPrcMainHeaderPpPanitiaApproval(null));//panitia
//		model.setListPrcMainHeaderOeCreate(prcService.listPrcMainHeaderEstimator(user));
		return SUCCESS;
	}
	
	//SANGAHAN	
	@Action(
			results={
					@Result(name=SUCCESS,location="adm/sanggahan/sanggahan.jsp")
			},value="ajax/adm/listSanggahan"
			)
	public String sanggahan() throws Exception{
		try {
			model.setPrcMainHeader(toolsService.getPrcMainHeader(model.getPrcMainHeader()));
			model.setListPrcVendor(toolsService.getListPrcVendor(model.getPrcMainHeader()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="adm/sanggahan/listSanggahan.jsp")
			},value="ajax/adm/addSanggahan"
			)
	public String listSanggahan() throws Exception{
		try {
			if(model.getMsgSanggahan() != null){
				System.out.println("lewat sini");
				PrcMsgSanggahan ms = model.getMsgSanggahan();
				if(model.getPrcVendor().getVndHeader().getVendorId()==0){
					addActionError("Sanggahan gagal dikirim, mohon ulangi kembali");
				}else{
					ms.setVndHeader(new VendorServiceImpl().getVndHeaderById(model.getPrcVendor().getVndHeader().getVendorId()));
					ms.setMsgTo(ms.getVndHeader().getVendorName().toUpperCase());
					ms.setMsgFrom("PEMBELI");
					boolean save = toolsService.saveSanggahan(ms);
					if (!save) {
						addActionError("Sanggahan gagal dikirim, mohon ulangi kembali");
					}else{
						PrcMainHeader prch = prcService.getPrcMainHeaderSPPHById(model.getPrcMainHeader().getPpmId());
						EmailSessionBean.sendMail("eProc PT Semen Baturaja (Persero)", "Buyer membalas sanggahan anda pada SPPH NO."+prch.getPpmNomorPr()+
								" "+prch.getPpmSubject(), ms.getVndHeader().getVendorEmail());
					}
				}
			}
			if(model.getPrcVendor().getVndHeader().getVendorId()==0)
				model.setListSanggahan(toolsService.getListSanggahan(model.getPrcMainHeader(),null));
			else
				model.setListSanggahan(toolsService.getListSanggahan(model.getPrcMainHeader(), model.getPrcVendor().getVndHeader()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="adm/prc/listPengadaan.jsp")
			},value="adm/daftarPengadaan"
	)
	//ubah tanggal prc
	public String ubahPrc() throws Exception{
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="adm/prc/ajax/daftarPengadaan.jsp")
			},value="ajax/adm/searchPengadaan"
	)
	public String searchPrc() throws Exception{
//		model.setListPrcMainHeader(toolsService.searchPrcMainHeaderAll(model.getPrcMainHeader()));
		model.setListPrcMainHeader(toolsService.searchPrcMainHeaderAll(model.getPrcMainHeader(), null));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="adm/prc/ajax/tanggalForm.jsp")
			},value="ajax/adm/editTanggalPrc"
	)
	public String editTanggalPrc() throws Exception{
		System.out.println("PRC HEADER >>>>>>>>>>>>>>> "+model.getPrcMainHeader().getPpmId());
		model.setPrcMainPreparation(toolsService.getPrcMainPreparation(model.getPrcMainHeader()));
		model.setPrcMainHeader(prcService.getPrcMainHeaderSPPHById(model.getPrcMainHeader().getPpmId()));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="adm/prc/ajax/tanggalForm.jsp")
			},value="ajax/adm/saveTanggalPengadaan"
	)
	public String saveTanggalPrc() throws Exception{
		System.out.println(">>> "+model.getPrcMainPreparation()+" ,><><> "+model.getPrcMainPreparation().getPpmId()+"<><><>< "+model.getPrcMainPreparation().getPrcMainHeader().getPpmId());
		boolean save = toolsService.editTanggal(model.getPrcMainPreparation());
		if (save) {
			addActionMessage("Edit tanggal pengadaan berhasil");
		}else{
			model.setPrcMainPreparation(toolsService.getPrcMainPreparation(model.getPrcMainPreparation().getPrcMainHeader()));
			addActionError("Edit tanggal pengadaan GAGAL");
		}
		return SUCCESS;
	}
	/** Administrasi **/
	
	@Action(
			results={
					@Result(name="list",location="adm/dept/listDept.jsp")
			},value="adm/listSatker"
			)
	public String admDept() throws Exception{
		model.setListDept(admService.getListAdmDept(null));
		return "list";
	}
	
	@Action(
			results={
					@Result(name="list",location="adm/dept/ajax/listDept.jsp")
			},value="ajax/adm/listSatker"
			)
	public String ajaxAdmDept() throws Exception{
		model.setListDept(admService.getListAdmDept(null));
		return "list";
	}
	
	@Action(
			results={
					@Result(name=INPUT,location="adm/dept/formDept.jsp"),
					@Result(name=SUCCESS, location="ajax/adm/listSatker", type="redirectAction")
			},value="ajax/adm/crudDept"
			)
	public String formDept() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "save")){
				admService.saveDept(model.getDept());
				addActionMessage("Data Sudah Disimpan");
				return SUCCESS;
			}else{
				System.out.println(model.getxCommand() +"<><><><><>");
				if(SessionGetter.isNotNull(model.getDept())){
					if(SessionGetter.isEdit(model.getxCommand())){
						model.setDept(admService.getAdmDeptById(model.getDept()));
					}else if(SessionGetter.isDelete(model.getxCommand())){
						admService.deleteDept(model.getDept());
						return SUCCESS;
					}
				}
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
			return INPUT;
		}
		model.setListDistrict(admService.getListDistrict(null));
		model.setListRole(admService.getListRole(null));
		model.setListUser(admService.getListUser(null));
		return INPUT;
	}
	
	
	/** Kantor **/
	
	@Action(
			results={
					@Result(name=SUCCESS,location="adm/district/listDistrict.jsp")
			},value="adm/listKantor"
			)
	public String admDistrict() throws Exception{
		try {
			model.setListDistrict(admService.getListDistrict(null));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="adm/district/ajax/listDistrict.jsp")
			},value="ajax/adm/listKantor"
			)
	public String ajaxAdmDistrict() throws Exception{
		try {
			model.setListDistrict(admService.getListDistrict(null));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=INPUT,location="adm/district/formDistrict.jsp"),
					@Result(name=SUCCESS,location="ajax/adm/listKantor",type="redirectAction")
			},value="ajax/adm/crudDistrict"
			)
	public String formDistrict() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "save")){
				System.out.println("save");
				if(model.getDistrict().getDistrict().getId()==0)
					model.getDistrict().setDistrict(null);
				admService.saveDistrict(model.getDistrict(),model.getKeuangan());
				addActionMessage("data sudah disimpan");
				return SUCCESS;
			}else{
				if(model.getDistrict()!=null){
					if(SessionGetter.isEdit(model.getxCommand())){
						model.setDistrict(admService.getDistrictById(model.getDistrict()));
						model.setKeuangan(admService.getMappingKeuanganByKantor(model.getDistrict()));
					}else if(SessionGetter.isDelete(model.getxCommand())){
						admService.deleteDistrict(model.getDistrict());
						addActionMessage("Data Sudah Dihapus");
						return SUCCESS;
					}
				}
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
			return SUCCESS;
		}
		model.setListDistrict(admService.getListDistrict(null));
		model.setListUser(admService.getListUser(null));
		return INPUT;
	}
	
	/** Negara */
	@Action(
			results={
					@Result(name=SUCCESS,location="adm/country/listCountry.jsp")
			},value="adm/listNegara"
			)
	public String admCountry() throws Exception{
		try {
			model.setListCountry(admService.listAdmCountry());
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="adm/country/ajax/listCountry.jsp")
			},value="ajax/adm/listNegara"
			)
	public String ajaxAdmCountry() throws Exception{
		try {
			model.setListCountry(admService.listAdmCountry());
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=INPUT,location="adm/country/formCountry.jsp"),
					@Result(name=SUCCESS,location="ajax/adm/listNegara",type="redirectAction")
			},value="ajax/adm/crudCountry"
			)
	public String formCountry() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "save")){
				System.out.println("save");
				admService.saveCountry(model.getCountry());
				addActionMessage("data sudah disimpan");
				return SUCCESS;
			}else{
				if(model.getCountry()!=null){
					if(SessionGetter.isEdit(model.getxCommand())){
						model.setCountry(admService.getCountryById(model.getCountry()));
					}else if(SessionGetter.isDelete(model.getxCommand())){
						admService.deleteCountry(model.getCountry());
						addActionMessage("Data Sudah Dihapus");
						return SUCCESS;
					}
				}
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
			return SUCCESS;
		}
		return INPUT;
	}
	
	
	/** Dokumen */
	@Action(
			results={
					@Result(name=SUCCESS,location="adm/doc/listDoc.jsp")
			},value="adm/listDokumen"
			)
	public String admDocument() throws Exception{
		try {
			model.setListDoc(admService.getAdmDoc(null));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="adm/doc/ajax/listDoc.jsp")
			},value="ajax/adm/listDokumen"
			)
	public String ajaxAdmDoc() throws Exception{
		try {
			model.setListDoc(admService.getAdmDoc(null));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=INPUT,location="adm/doc/formDoc.jsp"),
					@Result(name=SUCCESS,location="ajax/adm/listDokumen",type="redirectAction")
			},value="ajax/adm/crudDoc"
			)
	public String formDoc() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "save")){
				System.out.println("save");
				admService.saveDoc(model.getDoc());
				addActionMessage("data sudah disimpan");
				return SUCCESS;
			}else{
				if(model.getDoc()!=null){
					if(SessionGetter.isEdit(model.getxCommand())){
						System.out.println(">>>> EDIT <<<<");
						model.setDoc(admService.getDocById(model.getDoc()));
					}else if(SessionGetter.isDelete(model.getxCommand())){
						admService.deleteDoc(model.getDoc());
						addActionMessage("Data Sudah Dihapus");
						return SUCCESS;
					}
				}
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
			return SUCCESS;
		}
		return INPUT;
	}
	
	/** Role */
	@Action(
			results={
					@Result(name=SUCCESS,location="adm/role/listRole.jsp")
			},value="adm/listRole"
			)
	public String admRole() throws Exception{
		try {
			model.setListRole(admService.getListRole(null));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="adm/role/ajax/listRole.jsp")
			},value="ajax/adm/listRole"
			)
	public String ajaxAdmRole() throws Exception{
		try {
			model.setListRole(admService.getListRole(null));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=INPUT,location="adm/role/formRole.jsp"),
					@Result(name=SUCCESS,location="ajax/adm/listRole",type="redirectAction"),
					@Result(name="menu",location="adm/role/formRoleMenu.jsp")
			},value="ajax/adm/crudRole"
			)
	public String formRole() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "save")){
				System.out.println("save");
				admService.saveRole(model.getRole());
				addActionMessage("data sudah disimpan");
				return SUCCESS;
			}else if(WebUtils.hasSubmitParameter(request, "rolemenu")){
				System.out.println(">> UPDATE MENU >>");
				admService.saveListRoleMenu(model.getListRoleMenu());
				addActionMessage("Data sudah disimpan");
				return SUCCESS;
			}else{
				if(model.getRole()!=null){
					if(SessionGetter.isEdit(model.getxCommand())){
						System.out.println(">>>> EDIT <<<<");
						model.setRole(admService.getRoleById(model.getRole()));
					}else if(SessionGetter.isDelete(model.getxCommand())){
						admService.deleteRole(model.getRole());
						addActionMessage("Data Sudah Dihapus");
						return SUCCESS;
					}else if(SessionGetter.isAddChild(model.getxCommand())){
						System.out.println("??? ADD CHILD ROLE ??");
						// load menu if exist
						model.setRole(admService.getRoleById(model.getRole()));
						List<AdmRoleMenu> old = admService.getListRoleMenuByRole(model.getRole());
						model.setListRoleMenu(old);
						return "menu";
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
			return SUCCESS;
		}
		return INPUT;
	}
	
	/** MENU */
	@Action(
			results={
					@Result(name=SUCCESS,location="adm/menu/listMenu.jsp")
			},value="adm/listMenu"
			)
	public String admMenu() throws Exception{
		try {
			model.setListMenu(admService.getListMenu(null));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="adm/menu/ajax/listMenu.jsp")
			},value="ajax/adm/listMenu"
			)
	public String ajaxAdmMenu() throws Exception{
		try {
			model.setListMenu(admService.getListMenu(null));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=INPUT,location="adm/menu/formMenu.jsp"),
					@Result(name=SUCCESS,location="ajax/adm/listMenu",type="redirectAction")
			},value="ajax/adm/crudMenu"
			)
	public String formMenu() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "save")){
				System.out.println("save");
				if(model.getMenu().getMenuParent().getId()==0)
					model.getMenu().setMenuParent(null);
				admService.saveMenu(model.getMenu());
				addActionMessage("data sudah disimpan");
				return SUCCESS;
			}else{
				if(model.getMenu()!=null){
					if(SessionGetter.isEdit(model.getxCommand())){
						model.setMenu(admService.getMenuById(model.getMenu()));
					}else if(SessionGetter.isDelete(model.getxCommand())){
						admService.deleteMenu(model.getMenu());
						addActionMessage("Data Sudah Dihapus");
						return SUCCESS;
					}
				}
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
			return SUCCESS;
		}
		model.setListMenu(admService.getListMenu(null));
		return INPUT;
	}
	
	
	/** EMPLOYEE */
	@Action(
			results={
					@Result(name=SUCCESS,location="adm/user/listUser.jsp")
			},value="adm/listUser"
			)
	public String admUser() throws Exception{
		try {
			model.setListUser(admService.getListUser(null));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="adm/user/ajax/listUser.jsp")
			},value="ajax/adm/listUser"
			)
	public String ajaxAdmUser() throws Exception{
		try {
			model.setListUser(admService.getListUser(null));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=INPUT,location="adm/user/formUser.jsp"),
					@Result(name=SUCCESS,location="ajax/adm/listUser",type="redirectAction")
			},value="ajax/adm/crudUser"
			)
	public String formUser() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "save")){
				System.out.println("save");
				if(model.getUser().getAdmEmployee().getAdmRoleByEmpRole2().getId()==0)
					model.getUser().getAdmEmployee().setAdmRoleByEmpRole2(null);
				if(model.getUser().getAdmEmployee().getAdmRoleByEmpRole3().getId()==0)
					model.getUser().getAdmEmployee().setAdmRoleByEmpRole3(null);
				if(model.getUser().getAdmEmployee().getAdmRoleByEmpRole4().getId()==0)
					model.getUser().getAdmEmployee().setAdmRoleByEmpRole4(null);
				admService.saveUser(model.getUser());
				addActionMessage("data sudah disimpan");
				return SUCCESS;
			}else if(WebUtils.hasSubmitParameter(request, "reset")) {
				admService.resetPasswordUser(model.getUser());
				addActionMessage("Reset password berhasil");
				return SUCCESS;
			}
			else{
				if(model.getUser()!=null){
					if(SessionGetter.isEdit(model.getxCommand())){
						model.setUser(admService.getUserById(model.getUser()));
					}else if(SessionGetter.isDelete(model.getxCommand())){
						admService.deleteUser(model.getUser());
						addActionMessage("User sudah di aktivasi/ deaktivasi");
						return SUCCESS;
					}
				}
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
			return SUCCESS;
		}
		model.setListRole(admService.getListRole(null));
		model.setListDept(admService.getListAdmDept(null));
		return INPUT;
	}
	
	/** UOM */
	@Action(
			results={
					@Result(name=SUCCESS,location="adm/uom/listUom.jsp")
			},value="adm/listUom"
			)
	public String admUom() throws Exception{
		try {
			model.setListUom(admService.getListUom(null));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="adm/uom/ajax/listUom.jsp")
			},value="ajax/adm/listUom"
			)
	public String ajaxAdmUom() throws Exception{
		try {
			model.setListUom(admService.getListUom(null));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=INPUT,location="adm/uom/formUom.jsp"),
					@Result(name=SUCCESS,location="ajax/adm/listUom",type="redirectAction")
			},value="ajax/adm/crudUom"
			)
	public String formUom() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "save")){
				System.out.println("save");
				admService.saveUom(model.getUom());
				addActionMessage("data sudah disimpan");
				return SUCCESS;
			}else{
				if(model.getUom()!=null){
					if(SessionGetter.isEdit(model.getxCommand())){
						model.setUom(admService.getUomById(model.getUom()));
					}else if(SessionGetter.isDelete(model.getxCommand())){
						admService.deleteUom(model.getUom());
						addActionMessage("Data Sudah Dihapus");
						return SUCCESS;
					}
				}
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
			return SUCCESS;
		}
		return INPUT;
	}
	
	
	/** PROSES */
	@Action(
			results={
					@Result(name=SUCCESS,location="adm/proses/listProses.jsp")
			},value="adm/listProses"
			)
	public String admProses() throws Exception{
		try {
			model.setListProses(admService.getListProses(null));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="adm/proses/ajax/listProses.jsp")
			},value="ajax/adm/listProses"
			)
	public String ajaxAdmProses() throws Exception{
		try {
			model.setListProses(admService.getListProses(null));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=INPUT,location="adm/proses/formProses.jsp"),
					@Result(name=SUCCESS,location="ajax/adm/listProses",type="redirectAction")
			},value="ajax/adm/crudProses"
			)
	public String formProses() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "save")){
				System.out.println("save");
				admService.saveProses(model.getProses());
				addActionMessage("data sudah disimpan");
				return SUCCESS;
			}else{
				if(model.getProses()!=null){
					if(SessionGetter.isEdit(model.getxCommand())){
						model.setProses(admService.getProsesById(model.getProses()));
					}else if(SessionGetter.isDelete(model.getxCommand())){
						admService.deleteProses(model.getProses());
						addActionMessage("Data Sudah Dihapus");
						return SUCCESS;
					}
				}
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
			return SUCCESS;
		}
		return INPUT;
	}
	
	/** HIRARKI PROSES */
	@Action(
			results={
					@Result(name=SUCCESS,location="adm/hirarkiproses/listHirarkiProses.jsp")
			},value="adm/listHirarkiProses"
			)
	public String admHirarkiProses() throws Exception{
		try {
			model.setListHirarki(admService.getListHirarki(null));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="adm/hirarkiproses/ajax/listHirarkiProses.jsp")
			},value="ajax/adm/listHirarkiProses"
			)
	public String ajaxAdmHirarkiProses() throws Exception{
		try {
			model.setListHirarki(admService.getListHirarki(null));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=INPUT,location="adm/hirarkiproses/formHirarkiProses.jsp"),
					@Result(name=SUCCESS,location="ajax/adm/listHirarkiProses",type="redirectAction")
			},value="ajax/adm/crudHirarkiProses"
			)
	public String formHirarkiProses() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "save")){
				model.getHirarki().setAdmDistrict(admService.getAdmDeptById(model.getHirarki().getAdmDept()).getAdmDistrict());
				if(model.getHirarki().getTingkat()==null)
					model.getHirarki().setTingkat(1);
				if(model.getHirarki().getAdmRoleByNextPos().getId()==0)
					model.getHirarki().setAdmRoleByNextPos(null);
				admService.saveHirarki(model.getHirarki());
				addActionMessage("data sudah disimpan");
				return SUCCESS;
			}else{
				if(model.getHirarki()!=null){
					if(SessionGetter.isEdit(model.getxCommand())){
						model.setHirarki(admService.getHirarkiById(model.getHirarki()));
					}else if(SessionGetter.isDelete(model.getxCommand())){
						admService.deleteHirarki(model.getHirarki());
						addActionMessage("Data Sudah Dihapus");
						return SUCCESS;
					}else if(SessionGetter.isAddChild(model.getxCommand())){
						AdmProsesHirarki ap = admService.getHirarkiById(model.getHirarki());
						ap.setAdmRoleByCurrentPos(ap.getAdmRoleByNextPos());
						ap.setAdmRoleByNextPos(null);
						ap.setMinimalValue(null);
						ap.setTingkat(ap.getTingkat()+1);
						ap.setId(null);
						model.setHirarki(ap);
					}
				}
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
			return SUCCESS;
		}
		model.setListDept(admService.getListAdmDept(null));
		model.setListRole(admService.getListRole(null));
		model.setListMenu(admService.getListMenu(null));
		model.setListProses(admService.getListProses(null));
		return INPUT;
	}
	
	
	/** COST CENTER */
	@Action(
			results={
					@Result(name=SUCCESS,location="adm/costcenter/listCostCenter.jsp")
			},value="adm/listCostCenter"
			)
	public String admCostCenter() throws Exception{
		try {
			model.setListCostCenter(admService.getListCostCenter(null));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="adm/costcenter/ajax/listCostCenter.jsp")
			},value="ajax/adm/listCostCenter"
			)
	public String ajaxAdmCostCenter() throws Exception{
		try {
			model.setListCostCenter(admService.getListCostCenter(null));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=INPUT,location="adm/costcenter/formCostCenter.jsp"),
					@Result(name=SUCCESS,location="ajax/adm/listCostCenter",type="redirectAction")
			},value="ajax/adm/crudCostCenter"
			)
	public String formCostCenter() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "save")){
				System.out.println("save");
				model.getCostCenter().getId().setDeptId(model.getCostCenter().getAdmDept().getId());
				admService.saveCostCenter(model.getCostCenter());
				addActionMessage("data sudah disimpan");
				return SUCCESS;
			}else{
				if(model.getCostCenter()!=null){
					if(SessionGetter.isEdit(model.getxCommand())){
						model.setCostCenter(admService.getCostCenterById(model.getCostCenter()));
					}else if(SessionGetter.isDelete(model.getxCommand())){
						admService.deleteCostCenter(model.getCostCenter());
						addActionMessage("Data Sudah Dihapus");
						return SUCCESS;
					}
				}
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
			return SUCCESS;
		}
		model.setListDept(admService.getListAdmDept(null));
		return INPUT;
	}
	
	/** JADWAL DRT */
	@Action(
			results={
					@Result(name=SUCCESS,location="adm/jadwal/listJadwal.jsp")
			},value="adm/listJadwal"
			)
	public String admJadwal() throws Exception{
		try {
			model.setListJadwal(admService.getListJadwal(SessionGetter.getUser()));
		} catch (Exception e) {
			addActionError(e.getMessage());
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="adm/jadwal/ajax/listJadwal.jsp")
			},value="ajax/adm/listJadwal"
			)
	public String ajaxAdmJadwal() throws Exception{
		try {
			model.setListJadwal(admService.getListJadwal(SessionGetter.getUser()));
		} catch (Exception e) {
			addActionError(e.getMessage());
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=INPUT,location="adm/jadwal/formJadwal.jsp"),
					@Result(name=SUCCESS,location="ajax/adm/listJadwal",type="redirectAction")
			},value="ajax/adm/crudJadwal"
			)
	public String formJadwal() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "save")){
				System.out.println("save");
				model.getJadwal().setDistrict(SessionGetter.getUser().getAdmEmployee().getAdmDept().getAdmDistrict());
				admService.saveJadwal(model.getJadwal());
				addActionMessage("data sudah disimpan");
				return SUCCESS;
			}else{
				if(model.getJadwal()!=null){
					if(SessionGetter.isEdit(model.getxCommand())){
						model.setJadwal(admService.getJadwalById(model.getJadwal()));
					}else if(SessionGetter.isDelete(model.getxCommand())){
						admService.deleteJadwal(model.getJadwal());
						addActionMessage("Data Sudah Dihapus");
						return SUCCESS;
					}
				}
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
			return SUCCESS;
		}
		return INPUT;
	}
	
	/** Pembuatan Panitia DRT **/
	@Action(
			results={
					@Result(name=SUCCESS,location="vendor/panitia/listPanitiaVnd.jsp")
			},value="vendor/listPanitia"
			)
	public String listPanitiaVnd() throws Exception{
		try {
			model.setListPanitiaVnd(admService.getListPanitiaVnd(null));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="vendor/panitia/ajax/listPanitiaVndDetail.jsp")
			},value="ajax/vendor/listPanitiaDetail"
			)
	public String listAjaxPanitiaVndDetail() throws Exception{
		try {
			model.setListPanitiaVndDetail(admService.getListPanitiaDetailByPanitia(model.getPanitiaVnd()));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=INPUT,location="vendor/panitia/ajax/formPanitia.jsp"),
					@Result(name="EDIT",location="vendor/panitia/ajax/formPanitiaEdit.jsp"),
					@Result(name=SUCCESS,location="vendor/listPanitia",type="redirectAction")
			},value="vendor/crudPanitia"
			)
	public String formPanitiaVnd() throws Exception{
		model.setListDept(admService.getListAdmDept(null));
		try {
			if(WebUtils.hasSubmitParameter(request, "save")){
				admService.savePanitiaVnd(model.getPanitiaVnd());
				addActionMessage("data sudah disimpan");
				return "EDIT";
			}else if(WebUtils.hasSubmitParameter(request, "savefinish")){
				admService.savePanitiaVnd(model.getPanitiaVnd());
				addActionMessage("data sudah disimpan");
				return SUCCESS;
			}else if(WebUtils.hasSubmitParameter(request, "edit")){
				model.setPanitiaVnd(admService.getPanitiaVndById(model.getPanitiaVnd()));
				model.setListPanitiaVndDetail(admService.getListPanitiaDetailByPanitia(model.getPanitiaVnd()));
				return "EDIT";
			}else if(WebUtils.hasSubmitParameter(request, "hapus")){
				admService.hapusPanitia(model.getPanitiaVnd());
				addActionMessage("Data Sudah Dihapus");
				return SUCCESS;
			}
			else{
				if(model.getPanitiaVnd()!=null){
					if(SessionGetter.isEdit(model.getxCommand())){
						model.setPanitiaVnd(admService.getPanitiaVndById(model.getPanitiaVnd()));
						model.setListPanitiaVndDetail(admService.getListPanitiaDetailByPanitia(model.getPanitiaVnd()));
						return "EDIT";
					}else if(SessionGetter.isDelete(model.getxCommand())){
						admService.hapusPanitia(model.getPanitiaVnd());
						addActionMessage("Data Sudah Dihapus");
						return SUCCESS;
					}
				}
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
			return SUCCESS;
		}
		
		return INPUT;
	}
	
	@Action(
			results={
					@Result(name=INPUT,location="vendor/panitia/ajax/formPanitiaDetail.jsp"),
					@Result(name=SUCCESS,location="ajax/vendor/listPanitiaDetail",type="redirectAction",params={"panitiaVnd.id","%{panitiaVnd.id}"})
			},value="ajax/vendor/crudPanitiaDetail"
			)
	public String formPanitiaDetail() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "save")){
				admService.savePanitiaDetail(model.getPanitiaVndDetail());
				addActionMessage("data sudah disimpan");
				return SUCCESS;
			}
			else{
				if(model.getPanitiaVndDetail()!=null){
					if(SessionGetter.isEdit(model.getxCommand())){
						model.setPanitiaVndDetail(admService.getPanitiaDetailById(model.getPanitiaVndDetail()));
					}else if(SessionGetter.isDelete(model.getxCommand())){
						admService.hapusPanitiaDetail(model.getPanitiaVndDetail());
						addActionMessage("Data Sudah Dihapus");
						return SUCCESS;
					}
				}
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
			return SUCCESS;
		}
		model.setListUser(admService.getListUser(null));
		return INPUT;
	}
	
	/** pembuatan template **/
	
	@Action(
			results={
					@Result(name=SUCCESS,location="proc/template/listTemplate.jsp")
			},value="proc/listTemplate"
			)
	public String listTemplate() throws Exception{
		try {
			model.setListTemplate(admService.getListTemplate(null));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="proc/template/ajax/listTemplate.jsp")
			},value="ajax/proc/listTemplate"
			)
	public String ajaxListTemplate() throws Exception{
		try {
			model.setListTemplate(admService.getListTemplate(null));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	
	@SuppressWarnings("unchecked")
	@Action(
			results={
					@Result(name=INPUT,location="proc/template/ajax/formTemplate.jsp"),
					@Result(name=SUCCESS,location="ajax/proc/listTemplate", type="redirectAction")
			},value="ajax/proc/crudTemplate"
			)
	public String crudTemplatePengadaan() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "save")){
				admService.saveTemplatePengadaan(model.getTemplate(),(List<PrcTemplateDetail>)SessionGetter.getSessionValue("listTemplateDetail"));
				addActionMessage("Data Sudah Disimpan");
				SessionGetter.destroySessionValue("listTemplateDetail");
				return SUCCESS;
			}else{
				if(SessionGetter.isNotNull(model.getTemplate())){
					if(SessionGetter.isEdit(model.getxCommand())){
						model.setTemplate(admService.getTemplateById(model.getTemplate()));
						model.setListTemplateDetail(admService.getListTemplateDetailByTemplate(model.getTemplate()));
						SessionGetter.setSessionValue(model.getListTemplateDetail(), "listTemplateDetail");
					}else if(SessionGetter.isDelete(model.getxCommand())){
						admService.deleteTemplate(model.getTemplate());
						return SUCCESS;
					}
				}else{
					SessionGetter.destroySessionValue("listTemplateDetail");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
			return SUCCESS;
		}
		
		return INPUT;
	}
	
	@SuppressWarnings("unchecked")
	@Action(
			results={
					@Result(name=SUCCESS,location="proc/template/ajax/listTemplateDetail.jsp")
			},value="ajax/submitPrcTemplateDetail"
			)
	public String submitPrcTemplateDetail() throws Exception{
		System.out.println(">>>>>>>>> MASUK SINI >>>>>>>>>>");
		List<PrcTemplateDetail> tmpTemplateDetail = (List<PrcTemplateDetail>) SessionGetter.getSessionValue("listTemplateDetail");
		List<PrcTemplateDetail> listTemplateDetail = new ArrayList<PrcTemplateDetail>();
		
		if(request.getParameter("index") != null && request.getParameter("index") != "" && request.getParameter("act") != null && request.getParameter("act").equals("delete")){
			if(request.getParameter("id") != null && request.getParameter("id") != ""){
				long id = Long.parseLong(request.getParameter("id"));
				if(id!=0)
					admService.hapusTemplateDetail(admService.getTemplateDetailById(new PrcTemplateDetail(id)));
			}
			
 			int n = 0;
			int target = Integer.parseInt(request.getParameter("index"));
			if(tmpTemplateDetail != null){
				for(PrcTemplateDetail i:tmpTemplateDetail){
					if(n != target){
						listTemplateDetail.add(i);
					}
					n = n+1;
				}
			}
			SessionGetter.setSessionValue(listTemplateDetail, "listTemplateDetail");
			model.setListTemplateDetail(listTemplateDetail);
			return SUCCESS;
		}
		
		if(tmpTemplateDetail != null){
			for(PrcTemplateDetail i:tmpTemplateDetail){
				listTemplateDetail.add(i);
			}
		}
		
		listTemplateDetail.add(model.getTemplateDetail());
		model.setListTemplateDetail(listTemplateDetail);
		SessionGetter.setSessionValue(listTemplateDetail, "listTemplateDetail");
		return SUCCESS;
	}
	
	
	/** chatting **/
	@Action(
			results={
					@Result(name=SUCCESS,location="proc/chatInternal.jsp"),
					@Result(name="ok",location="ajax/chatInternal",type="redirectAction"),
			},value="ajax/chatInternal"
			)
	public String chatInternal() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "save")){
				PrcMsgChat msg = model.getChat();
				if (model.getDocsChat() != null) {
					msg.setDokumen(UploadUtil.uploadDocsFile(model.getDocsChat(), model.getDocsChatFileName()));
				}
				Long userId = Long.valueOf(model.getChat().getKepada());
				AdmUser us = new AdmUser();
				us.setId(userId);
				
				AdmUser kpd = new AdmServiceImpl().getUserById(us);
				model.getChat().setKepada(kpd.getAdmEmployee().getEmpStructuralPos().toUpperCase());
				model.getChat().setDari(SessionGetter.getUser().getCompleteName());
				PrcMainHeader ppm = prcService.getPrcMainIHeaderById(model.getChat().getPrcMainHeader().getPpmId());
				//kirim email
				String message = "<table>";
					message = message.concat("<tr>");
						message = message.concat("<td>");
							message = message.concat("Dari :");
						message = message.concat("</td>");
						message = message.concat("<td>");
							message = message.concat(model.getChat().getDari());
						message = message.concat("</td>");
					message = message.concat("</tr>");
					
					message = message.concat("<tr>");
					message = message.concat("<td>");
						message = message.concat("Nomor OR / OQ:");
					message = message.concat("</td>");
					message = message.concat("<td>");
						message = message.concat(ppm.getPpmNomorRfq()!=null ? ppm.getPpmNomorRfq():ppm.getPpmNomorPr());
					message = message.concat("</td>");
					message = message.concat("</tr>");
				
					message = message.concat("<tr>");
					message = message.concat("<td>");
						message = message.concat("Pesan :");
					message = message.concat("</td>");
					message = message.concat("<td>");
						message = message.concat(model.getChat().getPesan());
					message = message.concat("</td>");
				message = message.concat("</tr>");
				message = message+"</table>";
				EmailSessionBean.sendMail("Pesan Eproc PTSB", message, kpd.getAdmEmployee().getEmpEmail());
				prcService.saveChat(model.getChat());
				return "ok";
			}
			model.setListUser(admService.getListUser(null));
			model.setListChat(prcService.listChatByHeader(model.getPrcMainHeader()));
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	/** Delegasi **/
	@Action(
			results={
					@Result(name=SUCCESS,location="adm/delegasi/listDelegasi.jsp"),
					@Result(name="form",location="adm/delegasi/formDelegasi.jsp"),
					@Result(name="OK",location="adm/delegasi", type="redirectAction")
			},value="adm/delegasi"
			)
	public String delegasi() throws Exception{
		try {
			model.setListAdmDelegasi(admService.listDelegasi(null));
		} catch (Exception e) {
			addActionError(e.getMessage());
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="adm/delegasi/ajax/listDelegasi.jsp"),
			},value="ajax/adm/delegasi"
			)
	public String ajaxDelegasi() throws Exception{
		model.setListAdmDelegasi(admService.listDelegasi(null));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="adm/delegasi/formDelegasi.jsp"),
			},value="ajax/adm/crudDelegasi"
			)
	public String ajaxCrudDelegasi() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "save")){
				admService.saveDelegasi(model.getAdmDelegasi());
				addActionMessage("Data Sudah Disimpan");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(model.getxCommand()!=null &&  model.getxCommand().equalsIgnoreCase("edit")){
			model.setAdmDelegasi(admService.getDelegasiById(model.getAdmDelegasi()));
		}
		model.setListUser(admService.getListUser(null));
		return SUCCESS;
	}
	
	/**
	 * ganti password
	 */
	@Action(
			results={
					@Result(name=SUCCESS,location="adm/user/changePassword.jsp")
			},value="bypass/adm/gantiPassword"
			)
	public String gantiPassword() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "save")){
				admService.savePassword(model.getUser());
				addActionMessage("Password Sudah Di ubah");
				model.setUser(null);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	/**
	 * Web Issues
	 * @return
	 * @throws Exception
	 */
	@Action(
			results={
					@Result(name=SUCCESS,location="adm/issues/listIssues.jsp")
			},value="bypass/adm/webIssues"
			)
	public String webIssues() throws Exception{
		try {
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="adm/issues/ajax/listIssues.jsp")
			},value="ajax/adm/webIssues"
			)
	public String ajaxWebIssues() throws Exception{
		try {
			model.setListIssues(admService.getListIssues(null));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="adm/issues/formIssues.jsp"),
			},value="ajax/adm/crudIssues"
			)
	public String ajaxCrudwebIssues() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "save")){
				admService.saveIssues(model.getIssues());
				addActionMessage("Data Sudah Disimpan");
			}else{
				if(model.getxCommand()!=null && model.getxCommand().equalsIgnoreCase("edit")){
					model.setIssues(admService.getIssuesById(model.getIssues()));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	
	@Action(
			results={
					@Result(name=SUCCESS,location="adm/issues/listIssuesUpdate.jsp")
			},value="bypass/adm/webIssuesUpdate"
			)
	public String webIssuesUpdate() throws Exception{
		try {
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="adm/issues/ajax/listIssuesUpdate.jsp")
			},value="ajax/adm/webIssuesUpdate"
			)
	public String ajaxWebIssuesUpdate() throws Exception{
		try {
			model.setListIssues(admService.getListIssues(null));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="adm/issues/formIssuesUpdate.jsp"),
			},value="ajax/adm/crudIssuesUpdate"
			)
	public String ajaxCrudwebIssuesUpdate() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "save")){
				admService.saveIssues(model.getIssues());
				addActionMessage("Data Sudah Disimpan");
			}else{
				if(model.getxCommand()!=null && model.getxCommand().equalsIgnoreCase("edit")){
					model.setIssues(admService.getIssuesById(model.getIssues()));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	/** update tools **/
	
	/**
	 * Broadcast message ke semua vendor
	 */
	@Action(
			results={
					@Result(name=SUCCESS,location="adm/util/broadcastMessage.jsp")
			},value="adm/broadcastMessage"
			)
	public String bordcastMessage() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "save")){
				EmailSessionBean.sendMail("eProcurement. (Pemberitahuan)", model.getBroadCastMessage(), model.getKepada());
				addActionMessage("Data Sudah Dikirim");
			}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	
	
	
	
}
