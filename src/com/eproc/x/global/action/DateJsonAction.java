package com.eproc.x.global.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.eproc.core.BaseController;
import com.eproc.x.global.command.DateJsonCommand;

@ParentPackage("eproc")
@Namespace("/")
public class DateJsonAction extends BaseController {
	private DateJsonCommand model;

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return model;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		model = new DateJsonCommand();
	}
	
	@Action(
			results={
					@Result(name = SUCCESS, type="json")
			}, value = "dateJson",
			interceptorRefs={
					@InterceptorRef("defaultInterceptorStack")
			}
	)
	public String execute() throws Exception{
		DateFormat dfDate = new SimpleDateFormat("dd MMM yyyy");
		DateFormat dfTime = new SimpleDateFormat("HH:mm:ss");
		
		Date date = new Date();
		
		model.setTanggal(dfDate.format(date));
		model.setWaktu(dfTime.format(date));
		return SUCCESS;
	}
}
