package com.eproc.x.global.action;

import java.util.Date;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.joda.time.DateTime;
import org.springframework.web.util.WebUtils;

import com.eproc.core.BaseController;
import com.eproc.utils.SessionGetter;
import com.eproc.utils.TimerEauction;
import com.eproc.x.global.command.EauctionCommand;
import com.orm.model.AdmMailConfig;
import com.orm.model.EauctionHeader;

import com.orm.model.EauctionVendor; //plus production

import com.orm.model.PrcMainHeader;

import eproc.controller.EauctionService;
import eproc.controller.EauctionServiceImpl;
import eproc.controller.PrcServiceImpl;

@Namespace("/")
@ParentPackage("eproc")
public class EauctionAction extends BaseController {

	private EauctionCommand model;
	private EauctionService service;

	@Override
	public void prepare() throws Exception {
		model = new EauctionCommand();
		service = new EauctionServiceImpl();
	}

	@Override
	public Object getModel() {
		return this.model;
	}

	@Action(results = { @Result(name = SUCCESS, location = "proc/eauction/listEauction.jsp"),
			@Result(name = "form", location = "proc/eauction/viewEauction.jsp"),
			@Result(name = "ok", location = "rfq/eauction", type = "redirectAction") }, value = "rfq/eauction")
	public String formEauction() throws Exception {
		try {
			if (WebUtils.hasSubmitParameter(request, "save")) {
				service.saveEauction(model.getEauctionHeader(), model.getListEauctionVendor());
				addActionMessage("Data Sudah Disimpan");
				EauctionHeader eauctionHeader = service.eauctionHeaderByPpm(model.getPrcMainHeader());
				model.setPrcMainHeader(new PrcServiceImpl().getPrcMainIHeaderById(model.getPrcMainHeader().getPpmId()));
				model.setEauctionHeader(eauctionHeader);
				model.setListEauctionVendor(
						service.listVendorEauctionByHeader(model.getEauctionHeader(), model.getPrcMainHeader()));
				model.setListItem(new PrcServiceImpl().listPrcMainItemByHeader(model.getPrcMainHeader()));
				AdmMailConfig config = SessionGetter.getMailConfig();
				model.setVendorUrl(config.getServerName().concat("/"));
				return "form";
			} else if (WebUtils.hasSubmitParameter(request, "proses")) {
				EauctionHeader eauctionHeader = service.eauctionHeaderByPpm(model.getPrcMainHeader());
				model.setPrcMainHeader(new PrcServiceImpl().getPrcMainIHeaderById(model.getPrcMainHeader().getPpmId()));
				model.setEauctionHeader(eauctionHeader);
				model.setListEauctionVendor(
						service.listVendorEauctionByHeader(model.getEauctionHeader(), model.getPrcMainHeader()));
				model.setListItem(new PrcServiceImpl().listPrcMainItemByHeader(model.getPrcMainHeader()));
				AdmMailConfig config = SessionGetter.getMailConfig();
				model.setVendorUrl(config.getServerName().concat("/"));
				return "form";
			} else if (WebUtils.hasSubmitParameter(request, "reset")) {
				addActionMessage("Data berhasil di reset");
				EauctionHeader eauctionHeader = service.eauctionHeaderByPpm(model.getPrcMainHeader());
				service.resetEauction(eauctionHeader, model.getPrcMainHeader());
				model.setListEauctionVendor(
						service.listVendorEauctionByHeader(model.getEauctionHeader(), model.getPrcMainHeader()));
				model.setEauctionHeader(eauctionHeader);
				return "ok";
			}else if(WebUtils.hasSubmitParameter(request, "pause")){
				EauctionHeader eauctionHeader = service.eauctionHeaderByPpm(model.getPrcMainHeader());
				service.pauseEauction(eauctionHeader, model.getPrcMainHeader());

				addActionMessage("Latihan Dihentikan!");
				model.setPrcMainHeader(new PrcServiceImpl().getPrcMainIHeaderById(model.getPrcMainHeader().getPpmId()));
				model.setEauctionHeader(eauctionHeader);
				model.setListEauctionVendor(
				service.listVendorEauctionByHeader(model.getEauctionHeader(), model.getPrcMainHeader()));
				model.setListItem(new PrcServiceImpl().listPrcMainItemByHeader(model.getPrcMainHeader()));
				AdmMailConfig config = SessionGetter.getMailConfig();
				model.setVendorUrl(config.getServerName().concat("/"));
				return "form";
			}else if(WebUtils.hasSubmitParameter(request, "resume")){
				EauctionHeader eauctionHeader = service.eauctionHeaderByPpm(model.getPrcMainHeader());
				service.resumeEauction(eauctionHeader, model.getPrcMainHeader());
				model.setPrcMainHeader(new PrcServiceImpl().getPrcMainIHeaderById(model.getPrcMainHeader().getPpmId()));
				model.setEauctionHeader(eauctionHeader);
				model.setListEauctionVendor(
						service.listVendorEauctionByHeader(model.getEauctionHeader(), model.getPrcMainHeader()));
				model.setListItem(new PrcServiceImpl().listPrcMainItemByHeader(model.getPrcMainHeader()));
				AdmMailConfig config = SessionGetter.getMailConfig();
				model.setVendorUrl(config.getServerName().concat("/"));
				return "form";
			}else if (WebUtils.hasSubmitParameter(request, "latihan")) {
				service.latihanEauction(model.getEauctionHeader(), model.getListEauctionVendor());
				addActionMessage("Data Sudah Disimpan");
				EauctionHeader eauctionHeader = service.eauctionHeaderByPpm(model.getPrcMainHeader());
				model.setPrcMainHeader(new PrcServiceImpl().getPrcMainIHeaderById(model.getPrcMainHeader().getPpmId()));
				model.setEauctionHeader(eauctionHeader);
				model.setListEauctionVendor(
						service.listVendorEauctionByHeader(model.getEauctionHeader(), model.getPrcMainHeader()));
				model.setListItem(new PrcServiceImpl().listPrcMainItemByHeader(model.getPrcMainHeader()));
				AdmMailConfig config = SessionGetter.getMailConfig();
				model.setVendorUrl(config.getServerName().concat("/"));
				return "form";
			}
			else if (WebUtils.hasSubmitParameter(request, "saveLatihan")){
				addActionMessage("Data Sudah Disimpan");
				model.setPrcMainHeader(new PrcServiceImpl().getPrcMainIHeaderById(model.getPrcMainHeader().getPpmId()));
				EauctionHeader eauctionHeader = service.eauctionHeaderByPpm(model.getPrcMainHeader());
				model.setPrcMainHeader(new PrcServiceImpl().getPrcMainIHeaderById(model.getPrcMainHeader().getPpmId()));
				model.setEauctionHeader(eauctionHeader);
				model.setListEauctionVendor(
						service.listVendorEauctionByHeader(model.getEauctionHeader(), model.getPrcMainHeader()));
				service.saveLatihan(model.getEauctionHeader(), model.getListEauctionVendor());
				model.setListItem(new PrcServiceImpl().listPrcMainItemByHeader(model.getPrcMainHeader()));
				AdmMailConfig config = SessionGetter.getMailConfig();
				model.setVendorUrl(config.getServerName().concat("/"));
				return "form";
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
			e.printStackTrace();
		}
		model.setListPrcMainHeader(service.listPrcMainHeaderEauction(null));
		return SUCCESS;
	}

	@Action(results = { @Result(name = SUCCESS, type = "json") }, value = "initTimerEauctionJson", interceptorRefs = {
			@InterceptorRef("defaultInterceptorStack") })
	public String executeInitTimer() throws Exception {
		try {
			EauctionHeader eauctionHeader = service.eauctionHeaderByPpm(model.getPrcMainHeader());
			// instance timer
			if (SessionGetter.TIMER_EAUCTION != null) {
				SessionGetter.TIMER_EAUCTION.cancelTimer();
			}
			TimerEauction te = new TimerEauction(eauctionHeader.getTanggalMulai(), eauctionHeader.getWaktu().intValue(),
					eauctionHeader.getPpmId());
			te.startTimer();
			SessionGetter.TIMER_EAUCTION = te;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	@Action(results = {
			@Result(name = SUCCESS, location = "proc/eauction/detailVendorEauction.jsp") }, value = "ajax/detailVendorEauction", interceptorRefs = {
					@InterceptorRef("defaultInterceptorStack") })
	public String detailvendor() throws Exception {
		try {
			if (model.getEauctionHeader() != null && model.getEauctionHeader().getPpmId() != null) {
				model.setListEauctionVendor(
						service.listVendorEauctionByHeader(model.getEauctionHeader(), model.getPrcMainHeader()));
			} else {
				model.setListEauctionVendor(service.listVendorEauctionByHeader(null, model.getPrcMainHeader()));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
}
