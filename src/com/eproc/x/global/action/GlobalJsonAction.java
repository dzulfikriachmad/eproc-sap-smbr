package com.eproc.x.global.action;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.eproc.core.BaseController;
import com.eproc.utils.QueryUtil;
import com.eproc.utils.SessionGetter;
import com.eproc.utils.UploadUtil;
import com.eproc.x.global.command.GlobalJsonCommand;
import com.eproc.x.report.command.ProcurementProsesCommand;
import com.eproc.x.report.command.ProcurementValueCommand;

//import eproc.controller.JdeService;
import eproc.controller.RfqService;
import eproc.controller.RfqServiceImpl;
import eproc.controller.RptServiceImpl;

@ParentPackage("eproc")
@Namespace("/")
public class GlobalJsonAction extends BaseController {

	private GlobalJsonCommand model;
	private RfqService service;
	@Override
	public Object getModel() {
		return this.model;
	}
	
	@Override
	public void prepare() throws Exception {
		model = new GlobalJsonCommand();
		service = new RfqServiceImpl();
	}
	
	@Action(
			results={
					@Result(name = SUCCESS, type="json")
			}, value = "anggaranDeskripsiJson",
			interceptorRefs={
					@InterceptorRef("defaultInterceptorStack")
			}
	)
	public String execute() throws Exception{
		String desk = service.getDeskripsiAnggaran(model.getKode());
		if(desk!=null){
			model.setStatus(1);
			model.setDeskripsi(desk);
		}else{
			model.setStatus(-1);
			model.setDeskripsi("Data Tidak Ditemukan");
		}
		return SUCCESS;
	}
	

	@Action(
			results={
					@Result(name=SUCCESS,type="json")
			},value="jsonCaptcha",
			interceptorRefs={
					@InterceptorRef("defaultInterceptorStack")
			}
			)
	public String randomCaptcha() throws Exception{
		model.setGlobalReturn(SessionGetter.randomString(8));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,type="json")
			},value="jsonMessageCount",
			interceptorRefs={
					@InterceptorRef("defaultInterceptorStack")
			}
			)
	public String messageCount() throws Exception{
		if(model.getStatus()!=null){
			model.setStatus(service.countMessage(model.getStatus()));
		}
		return SUCCESS;
	}
	
	
	@Action(
			results={
					@Result(name=SUCCESS,type="json")
			},value="ajax/json/dashboardProcurementValue"
			)
	public String dashboardProcurementValue() throws Exception{
		try {
			Calendar c = Calendar.getInstance();
			c.get(Calendar.YEAR);
			c.set(Calendar.YEAR, Calendar.JANUARY, 1);
			Date startDate = c.getTime();
			List<ProcurementValueCommand> data = new RptServiceImpl().getListProcurementValue(QueryUtil.procurementValue(), startDate, new Date());
			
			if(data!=null){
				String chart = "<chart palette='2' rotateNames='0' animation='1' numdivlines='4' caption='Procurement Value YTD "+new SimpleDateFormat("yyyy").format(new Date())+"' baseFont='Arial' baseFontSize='12' useRoundEdges='1' legendBorderAlpha='0'>";
				
				/** add category **/
				chart = chart.concat("<categories font='Arial' fontSize='12'>");
				for(ProcurementValueCommand p:data){
					chart = chart.concat("<category label='"+p.getMetode()+"' toolText='"+p.getMetode()+"'/>");
				}
				chart = chart.concat("</categories>");
				
				/** add data set ee**/
				chart = chart.concat("<dataset seriesname='Nilai EE'  alpha='90' showValues='0'>");
				for(ProcurementValueCommand p:data){
					chart = chart.concat("<set value='"+p.getEE()+"'/>");
				}
				chart = chart.concat("</dataset>");
				
				/** add data set oe**/
				chart = chart.concat("<dataset seriesname='Nilai OE'  alpha='90' showValues='0'>");
				for(ProcurementValueCommand p:data){
					chart = chart.concat("<set value='"+p.getOE()+"'/>");
				}
				chart = chart.concat("</dataset>");
				
				/** add data set oe**/
				chart = chart.concat("<dataset seriesname='Nilai Kontrak' alpha='90' showValues='0'>");
				for(ProcurementValueCommand p:data){
					chart = chart.concat("<set value='"+p.getKontrak()+"'/>");
				}
				chart = chart.concat("</dataset>");
				
				chart = chart.concat("</chart>");
				
				model.setGlobalReturn(chart);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,type="json")
			},value="ajax/json/dashboardProcurementProses"
			)
	public String dashboardProcurementProses() throws Exception{
		try {
			List<ProcurementProsesCommand> data = new RptServiceImpl().getListProcurementProses(QueryUtil.queryProsesPengadaan(null));
			
			if(data!=null){
				String chart = "<chart caption='Proses Pengadaan' xAxisName='Proses' yAxisName='Metode' showValues='0' decimals='0' formatNumberScale='0'>";
				
				/** add value **/
				for(ProcurementProsesCommand p:data){
					chart = chart.concat("<set value='"+p.getJumlah()+"' label='"+p.getProsesName()+"'/>");
				}

				chart = chart.concat("</chart>");
				model.setGlobalReturn(chart);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return SUCCESS;
	}
}
