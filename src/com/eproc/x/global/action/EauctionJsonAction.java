package com.eproc.x.global.action;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.eproc.core.BaseController;
import com.eproc.utils.QueryUtil;
import com.eproc.x.global.command.EauctionJsonCommand;
import com.orm.model.EauctionHeader;

import eproc.controller.EauctionService;
import eproc.controller.EauctionServiceImpl;

@Namespace("/")
@ParentPackage("eproc")
public class EauctionJsonAction extends BaseController {

	private EauctionJsonCommand model;
	private EauctionService service;

	@Override
	public Object getModel() {
		return this.model;
	}

	@Override
	public void prepare() throws Exception {
		model = new EauctionJsonCommand();
		service = new EauctionServiceImpl();
	}

	@Action(results = { @Result(name = SUCCESS, type = "json") }, value = "timerEauctionJson", interceptorRefs = {
			@InterceptorRef("defaultInterceptorStack") })
	public String execute() throws Exception {
		try {
			// setTimeEauction(model.getPpmId());
			/*
			 * model.setLowestBid(DecimalFormat.getInstance().format(
			 * service.getLowestBid(model.getPpmId())));
			 */
			
			EauctionHeader eauction = service.getEauctionHeaderByPpmId(model.getPpmId());
			Date time = new Date();
			//if (time.equals(eauction.getTanggalMulai())) {
				Integer remaining = service.getTimeEauction(eauction.getPpmId(), eauction.getTanggalBerakhir());
				if (remaining < 0) {
					if (eauction.getTanggalBerakhir() == null) {
						model.setRemaningTime(0);
					} else {
						if (time.after(eauction.getTanggalMulai())) {
						model.setRemaningTime(remaining);
						}
					}
				} else {
					if (time.after(eauction.getTanggalMulai())) {
						model.setRemaningTime(remaining);
					}
				}
			//};
			

			model.setGlobalMessage("Eauction Akan Mulai Pada :: "
					+ new SimpleDateFormat("dd/MM/yyyy HH:mm").format(eauction.getTanggalMulai()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	@Action(results = { @Result(name = SUCCESS, type = "json") }, value = "countdownTimer", interceptorRefs = {
			@InterceptorRef("defaultInterceptorStack") })
	public String countdown() throws Exception {
		model.setRemaningTime(service.countdownTime(model.getRemaningTime()));
		/*
		 * model.setGlobalMessage("Eauction Akan Mulai Pada :: " + new
		 * SimpleDateFormat("dd/MM/yyyy HH:mm") .format(eauction.getTanggalMulai()));
		 */
		return SUCCESS;
	}

	@Action(results = { @Result(name = SUCCESS, type = "json") }, value = "bidderEauctionJson", interceptorRefs = {
			@InterceptorRef("defaultInterceptorStack") })
	public String bidderEauction() throws Exception {
		try {
			// trigger timer
			Date time = service.getDatabaseTime();
			EauctionHeader eauction = service.getEauctionHeaderByPpmId(model.getPpmId());
			if (time.equals(eauction.getTanggalMulai())) {
				execute();
			}
			model.setLowestBid(DecimalFormat.getInstance().format(service.getLowestBid(model.getPpmId())));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	/*
	 * private void setTimeEauction(Long ppmId) throws Exception { EauctionHeader
	 * eauction = service.getEauctionHeaderByPpmId(ppmId);
	 * if(eauction.getTanggalBerakhir()==null) { model.setRemaningTime(0); }else {
	 * model.setRemaningTime(service.getTimeEauction(eauction.getPpmId(),
	 * eauction.getTanggalBerakhir())); }
	 * model.setGlobalMessage("Eauction Akan Mulai Pada :: " + new
	 * SimpleDateFormat("dd/MM/yyyy HH:mm") .format(eauction.getTanggalMulai())); }
	 */

	@Action(results = {
			@Result(name = SUCCESS, location = "proc/eauction/detailEauction.jsp") }, value = "ajax/detailEauction", interceptorRefs = {
					@InterceptorRef("defaultInterceptorStack") })
	public String formDetailEauction() throws Exception {
		try {
			model.setListModelVendor(service.listEauctionVendor(QueryUtil.listVendorEauction(model.getPpmId())));
			model.setListHistory(service.listEauctionHistory(QueryUtil.listHistoryEauction(model.getPpmId())));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	@Action(results = {
			@Result(name = SUCCESS, type = "json") }, value = "ajax/diskualifikasiEauctionJson", interceptorRefs = {
					@InterceptorRef("defaultInterceptorStack") })
	public String diskualifikasiEauction() throws Exception {
		try {
			service.diskualifikasiVendor(model.getEauctionVendorId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

}
