package com.eproc.x.global.command;

import java.util.Date;

public class TodoUserCommand {
	
	private Integer ppmId;
	private Integer contractId;
	private String judulKontrak;
	private String pemenang;
	private String nomorKontrak;
	private String nomorPr;
	private String nomorSpph;
	private String judulSpph;
	private Integer currentUser;
	private String completeName;
	private Date updatedDate;
	private String keterangan;
	private String url;
	
	public TodoUserCommand() {
		// TODO Auto-generated constructor stub
	}

	public Integer getPpmId() {
		return ppmId;
	}

	public void setPpmId(Integer ppmId) {
		this.ppmId = ppmId;
	}

	public String getNomorPr() {
		return nomorPr;
	}

	public void setNomorPr(String nomorPr) {
		this.nomorPr = nomorPr;
	}

	public String getNomorSpph() {
		return nomorSpph;
	}

	public void setNomorSpph(String nomorSpph) {
		this.nomorSpph = nomorSpph;
	}

	public Integer getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(Integer currentUser) {
		this.currentUser = currentUser;
	}

	public String getCompleteName() {
		return completeName;
	}

	public void setCompleteName(String completeName) {
		this.completeName = completeName;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getKeterangan() {
		return keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getJudulSpph() {
		return judulSpph;
	}

	public void setJudulSpph(String judulSpph) {
		this.judulSpph = judulSpph;
	}

	public Integer getContractId() {
		return contractId;
	}

	public void setContractId(Integer contractId) {
		this.contractId = contractId;
	}

	public String getJudulKontrak() {
		return judulKontrak;
	}

	public void setJudulKontrak(String judulKontrak) {
		this.judulKontrak = judulKontrak;
	}

	public String getPemenang() {
		return pemenang;
	}

	public void setPemenang(String pemenang) {
		this.pemenang = pemenang;
	}

	public String getNomorKontrak() {
		return nomorKontrak;
	}

	public void setNomorKontrak(String nomorKontrak) {
		this.nomorKontrak = nomorKontrak;
	}
	

}
