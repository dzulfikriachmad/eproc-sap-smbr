package com.eproc.x.global.command;

import java.util.List;

public class EauctionJsonCommand {

	private Long ppmId;
	private Integer remaningTime;
	private String lowestBid;
	private String globalMessage;
	private Integer eauctionVendorId;
	private Integer isVendor;

	private List<EauctionTableModel> listModelVendor;
	private List<EauctionTableHistoryModel> listHistory;

	public Long getPpmId() {
		return ppmId;
	}

	public void setPpmId(Long ppmId) {
		this.ppmId = ppmId;
	}

	public Integer getRemaningTime() {
		return remaningTime;
	}

	public void setRemaningTime(Integer remaningTime) {
		this.remaningTime = remaningTime;
	}

	public List<EauctionTableModel> getListModelVendor() {
		return listModelVendor;
	}

	public void setListModelVendor(List<EauctionTableModel> listModelVendor) {
		this.listModelVendor = listModelVendor;
	}

	public List<EauctionTableHistoryModel> getListHistory() {
		return listHistory;
	}

	public void setListHistory(List<EauctionTableHistoryModel> listHistory) {
		this.listHistory = listHistory;
	}

	public String getLowestBid() {
		return lowestBid;
	}

	public void setLowestBid(String lowestBid) {
		this.lowestBid = lowestBid;
	}

	public String getGlobalMessage() {
		return globalMessage;
	}

	public void setGlobalMessage(String globalMessage) {
		this.globalMessage = globalMessage;
	}

	public Integer getEauctionVendorId() {
		return eauctionVendorId;
	}

	public void setEauctionVendorId(Integer eauctionVendorId) {
		this.eauctionVendorId = eauctionVendorId;
	}

	public Integer getIsVendor() {
		return isVendor;
	}

	public void setIsVendor(Integer isVendor) {
		this.isVendor = isVendor;
	}

}
