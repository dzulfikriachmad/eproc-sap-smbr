package com.eproc.x.global.command;

public class GlobalJsonCommand {

	private String kode;
	private String deskripsi;
	private Integer status;
	private String globalReturn;
	private String attachment;
	public String getDeskripsi() {
		return deskripsi;
	}

	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}

	public String getKode() {
		return kode;
	}

	public void setKode(String kode) {
		this.kode = kode;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getGlobalReturn() {
		return globalReturn;
	}

	public void setGlobalReturn(String globalReturn) {
		this.globalReturn = globalReturn;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}
}
