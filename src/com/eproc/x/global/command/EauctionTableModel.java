package com.eproc.x.global.command;

import java.io.Serializable;
import java.util.Date;

public class EauctionTableModel implements Serializable {

	private Long id;
	private Long eauctionId;
	private String namaVendor;
	private Double lastBid;
	private Date tglBid;
	private String tglBidString;
	
	public EauctionTableModel() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getEauctionId() {
		return eauctionId;
	}

	public void setEauctionId(Long eauctionId) {
		this.eauctionId = eauctionId;
	}

	public String getNamaVendor() {
		return namaVendor;
	}

	public void setNamaVendor(String namaVendor) {
		this.namaVendor = namaVendor;
	}

	public Double getLastBid() {
		return lastBid;
	}

	public void setLastBid(Double lastBid) {
		this.lastBid = lastBid;
	}

	public Date getTglBid() {
		return tglBid;
	}

	public void setTglBid(Date tglBid) {
		this.tglBid = tglBid;
	}

	public String getTglBidString() {
		return tglBidString;
	}

	public void setTglBidString(String tglBidString) {
		this.tglBidString = tglBidString;
	}
	
	
}
