package com.eproc.x.global.command;

import java.io.Serializable;

public class DateJsonCommand implements Serializable {
	private String tanggal;
	private String waktu;

	public String getTanggal() {
		return tanggal;
	}
	
	public void setTanggal(String tanggal) {
		this.tanggal = tanggal;
	}
	
	public String getWaktu() {
		return waktu;
	}
	
	public void setWaktu(String waktu) {
		this.waktu = waktu;
	}	
}
