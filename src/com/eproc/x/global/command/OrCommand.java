package com.eproc.x.global.command;

import java.io.File;
import java.io.Serializable;
import java.util.List;

import com.eproc.x.rfq.command.TabulasiPenilaian;
import com.orm.model.AdmDept;
import com.orm.model.AdmEmployee;
import com.orm.model.AdmJenis;
import com.orm.model.ComGroup;
import com.orm.model.HistMainComment;
import com.orm.model.HistoryHargaNegosiasi;
import com.orm.model.OrDetail;
import com.orm.model.OrHeader;
import com.orm.model.PrcMainDoc;
import com.orm.model.PrcMainHeader;
import com.orm.model.PrcMainItem;
import com.orm.model.PrcMainPreparation;
import com.orm.model.PrcMainVendor;
import com.orm.model.PrcManualDoc;
import com.orm.model.PrcMsgNegotiation;
import com.orm.tools.AbstractCommand;

public class OrCommand extends AbstractCommand implements Serializable {
	
	private OrHeader orHeader;
	private List<OrHeader> lisOrHeader;
	
	private OrDetail orDetail;
	private List<OrDetail> listOrDetail;
	
	private List<ComGroup> listGroup;
	
	private PrcMainHeader prcMainHeader;
	
	private List<AdmJenis> listAdmJenis;
	
	private AdmDept admDept;
	
	private AdmEmployee admEmployee;
	private List<PrcMainItem> listPrcMainItem;
	private List<HistMainComment> listHistMainComment;

	private List<File> prcManDocument;
	private List<String> prcManDocumentFileName;
	private List<String> prcManDocumentCategory;
	private List<String> prcManDocumentDescription;
	private List<PrcManualDoc> listPrcManualDoc;
	
	private List<PrcMainVendor> listVendor;
	private PrcMainPreparation prcMainPreparation;
	private List<TabulasiPenilaian> listTabulasi;
	private List<PrcMsgNegotiation> listNegosiasi;
	private List<PrcMainDoc> listPrcMainDoc;
	private List<HistoryHargaNegosiasi> listHargaNegosiasi;
	
	private File docsKomentar;
	private String docsKomentarFileName;
	private String docsKomentarContentType;
	
	private String encprnum;
	private String encorh;
	private String encsite;
	
	private HistMainComment histMainComment;
	
	public OrHeader getOrHeader() {
		return orHeader;
	}
	public void setOrHeader(OrHeader orHeader) {
		this.orHeader = orHeader;
	}
	public List<OrHeader> getLisOrHeader() {
		return lisOrHeader;
	}
	public void setLisOrHeader(List<OrHeader> lisOrHeader) {
		this.lisOrHeader = lisOrHeader;
	}
	public OrDetail getOrDetail() {
		return orDetail;
	}
	public void setOrDetail(OrDetail orDetail) {
		this.orDetail = orDetail;
	}
	public List<OrDetail> getListOrDetail() {
		return listOrDetail;
	}
	public void setListOrDetail(List<OrDetail> listOrDetail) {
		this.listOrDetail = listOrDetail;
	}
	public List<ComGroup> getListGroup() {
		return listGroup;
	}
	public void setListGroup(List<ComGroup> listGroup) {
		this.listGroup = listGroup;
	}
	public PrcMainHeader getPrcMainHeader() {
		return prcMainHeader;
	}
	public void setPrcMainHeader(PrcMainHeader prcMainHeader) {
		this.prcMainHeader = prcMainHeader;
	}
	public List<AdmJenis> getListAdmJenis() {
		return listAdmJenis;
	}
	public void setListAdmJenis(List<AdmJenis> listAdmJenis) {
		this.listAdmJenis = listAdmJenis;
	}
	public AdmDept getAdmDept() {
		return admDept;
	}
	public void setAdmDept(AdmDept admDept) {
		this.admDept = admDept;
	}
	public AdmEmployee getAdmEmployee() {
		return admEmployee;
	}
	public void setAdmEmployee(AdmEmployee admEmployee) {
		this.admEmployee = admEmployee;
	}
	public List<PrcMainItem> getListPrcMainItem() {
		return listPrcMainItem;
	}
	public void setListPrcMainItem(List<PrcMainItem> listPrcMainItem) {
		this.listPrcMainItem = listPrcMainItem;
	}
	public List<HistMainComment> getListHistMainComment() {
		return listHistMainComment;
	}
	public void setListHistMainComment(List<HistMainComment> listHistMainComment) {
		this.listHistMainComment = listHistMainComment;
	}
	public List<File> getPrcManDocument() {
		return prcManDocument;
	}
	public void setPrcManDocument(List<File> prcManDocument) {
		this.prcManDocument = prcManDocument;
	}
	public List<String> getPrcManDocumentFileName() {
		return prcManDocumentFileName;
	}
	public void setPrcManDocumentFileName(List<String> prcManDocumentFileName) {
		this.prcManDocumentFileName = prcManDocumentFileName;
	}
	public List<String> getPrcManDocumentCategory() {
		return prcManDocumentCategory;
	}
	public void setPrcManDocumentCategory(List<String> prcManDocumentCategory) {
		this.prcManDocumentCategory = prcManDocumentCategory;
	}
	public List<String> getPrcManDocumentDescription() {
		return prcManDocumentDescription;
	}
	public void setPrcManDocumentDescription(List<String> prcManDocumentDescription) {
		this.prcManDocumentDescription = prcManDocumentDescription;
	}
	public List<PrcManualDoc> getListPrcManualDoc() {
		return listPrcManualDoc;
	}
	public void setListPrcManualDoc(List<PrcManualDoc> listPrcManualDoc) {
		this.listPrcManualDoc = listPrcManualDoc;
	}
	public List<PrcMainVendor> getListVendor() {
		return listVendor;
	}
	public void setListVendor(List<PrcMainVendor> listVendor) {
		this.listVendor = listVendor;
	}
	public PrcMainPreparation getPrcMainPreparation() {
		return prcMainPreparation;
	}
	public void setPrcMainPreparation(PrcMainPreparation prcMainPreparation) {
		this.prcMainPreparation = prcMainPreparation;
	}
	public List<TabulasiPenilaian> getListTabulasi() {
		return listTabulasi;
	}
	public void setListTabulasi(List<TabulasiPenilaian> listTabulasi) {
		this.listTabulasi = listTabulasi;
	}
	public List<PrcMsgNegotiation> getListNegosiasi() {
		return listNegosiasi;
	}
	public void setListNegosiasi(List<PrcMsgNegotiation> listNegosiasi) {
		this.listNegosiasi = listNegosiasi;
	}
	public List<PrcMainDoc> getListPrcMainDoc() {
		return listPrcMainDoc;
	}
	public void setListPrcMainDoc(List<PrcMainDoc> listPrcMainDoc) {
		this.listPrcMainDoc = listPrcMainDoc;
	}
	public List<HistoryHargaNegosiasi> getListHargaNegosiasi() {
		return listHargaNegosiasi;
	}
	public void setListHargaNegosiasi(List<HistoryHargaNegosiasi> listHargaNegosiasi) {
		this.listHargaNegosiasi = listHargaNegosiasi;
	}
	public HistMainComment getHistMainComment() {
		return histMainComment;
	}
	public void setHistMainComment(HistMainComment histMainComment) {
		this.histMainComment = histMainComment;
	}
	public String getEncprnum() {
		return encprnum;
	}
	public void setEncprnum(String encprnum) {
		this.encprnum = encprnum;
	}
	public String getEncorh() {
		return encorh;
	}
	public void setEncorh(String encorh) {
		this.encorh = encorh;
	}
	public String getEncsite() {
		return encsite;
	}
	public void setEncsite(String encsite) {
		this.encsite = encsite;
	}
	public File getDocsKomentar() {
		return docsKomentar;
	}
	public void setDocsKomentar(File docsKomentar) {
		this.docsKomentar = docsKomentar;
	}
	public String getDocsKomentarFileName() {
		return docsKomentarFileName;
	}
	public void setDocsKomentarFileName(String docsKomentarFileName) {
		this.docsKomentarFileName = docsKomentarFileName;
	}
	public String getDocsKomentarContentType() {
		return docsKomentarContentType;
	}
	public void setDocsKomentarContentType(String docsKomentarContentType) {
		this.docsKomentarContentType = docsKomentarContentType;
	}
	

}
