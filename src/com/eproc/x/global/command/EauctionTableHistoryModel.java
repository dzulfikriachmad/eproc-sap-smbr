package com.eproc.x.global.command;

import java.io.Serializable;
import java.util.Date;

public class EauctionTableHistoryModel implements Serializable {

	private Long id;
	private Long ppmId;
	private String vendorName;
	private Double jumlahBid;
	private Date tglBid;
	private String tglBidString;
	public EauctionTableHistoryModel() {
		// TODO Auto-generated constructor stub
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getPpmId() {
		return ppmId;
	}
	public void setPpmId(Long ppmId) {
		this.ppmId = ppmId;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public Double getJumlahBid() {
		return jumlahBid;
	}
	public void setJumlahBid(Double jumlahBid) {
		this.jumlahBid = jumlahBid;
	}
	public Date getTglBid() {
		return tglBid;
	}
	public void setTglBid(Date tglBid) {
		this.tglBid = tglBid;
	}
	public String getTglBidString() {
		return tglBidString;
	}
	public void setTglBidString(String tglBidString) {
		this.tglBidString = tglBidString;
	}
}
