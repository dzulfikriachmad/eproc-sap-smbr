package com.eproc.x.global.command;

import java.util.List;

import com.orm.model.EauctionHeader;
import com.orm.model.EauctionHistory;
import com.orm.model.EauctionVendor;
import com.orm.model.PrcMainHeader;
import com.orm.model.PrcMainItem;

public class EauctionCommand {

	private PrcMainHeader prcMainHeader;
	private String vendorUrl;
	private EauctionHeader eauctionHeader;
	private List<PrcMainHeader> listPrcMainHeader;
	private List<EauctionHeader> listEauctionHeader;
	private List<EauctionVendor> listEauctionVendor;
	private List<EauctionHistory> listEauctionHistory;
	private List<PrcMainItem> listItem;
	public PrcMainHeader getPrcMainHeader() {
		return prcMainHeader;
	}
	public void setPrcMainHeader(PrcMainHeader prcMainHeader) {
		this.prcMainHeader = prcMainHeader;
	}
	public EauctionHeader getEauctionHeader() {
		return eauctionHeader;
	}
	public void setEauctionHeader(EauctionHeader eauctionHeader) {
		this.eauctionHeader = eauctionHeader;
	}
	public List<PrcMainHeader> getListPrcMainHeader() {
		return listPrcMainHeader;
	}
	public void setListPrcMainHeader(List<PrcMainHeader> listPrcMainHeader) {
		this.listPrcMainHeader = listPrcMainHeader;
	}
	public List<EauctionHeader> getListEauctionHeader() {
		return listEauctionHeader;
	}
	public void setListEauctionHeader(List<EauctionHeader> listEauctionHeader) {
		this.listEauctionHeader = listEauctionHeader;
	}
	public List<EauctionVendor> getListEauctionVendor() {
		return listEauctionVendor;
	}
	public void setListEauctionVendor(List<EauctionVendor> listEauctionVendor) {
		this.listEauctionVendor = listEauctionVendor;
	}
	public List<EauctionHistory> getListEauctionHistory() {
		return listEauctionHistory;
	}
	public void setListEauctionHistory(List<EauctionHistory> listEauctionHistory) {
		this.listEauctionHistory = listEauctionHistory;
	}
	public List<PrcMainItem> getListItem() {
		return listItem;
	}
	public void setListItem(List<PrcMainItem> listItem) {
		this.listItem = listItem;
	}
	public String getVendorUrl() {
		return vendorUrl;
	}
	public void setVendorUrl(String vendorUrl) {
		this.vendorUrl = vendorUrl;
	}
}
