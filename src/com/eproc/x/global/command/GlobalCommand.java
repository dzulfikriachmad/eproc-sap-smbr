
package com.eproc.x.global.command;

import java.io.File;
import java.io.Serializable;
import java.util.List;

import com.orm.model.AdmCostCenter;
import com.orm.model.AdmCountry;
import com.orm.model.AdmDelegasi;
import com.orm.model.AdmDept;
import com.orm.model.AdmDistrict;
import com.orm.model.AdmDoc;
import com.orm.model.AdmIssues;
import com.orm.model.AdmJadwalDrt;
import com.orm.model.AdmMenu;
import com.orm.model.AdmProses;
import com.orm.model.AdmProsesHirarki;
import com.orm.model.AdmRole;
import com.orm.model.AdmRoleMenu;
import com.orm.model.AdmUom;
import com.orm.model.AdmUser;
import com.orm.model.PanitiaVnd;
import com.orm.model.PanitiaVndDetail;
import com.orm.model.PrcMainHeader;
import com.orm.model.PrcMainPreparation;
import com.orm.model.PrcMainVendor;
import com.orm.model.PrcMappingKeuangan;
import com.orm.model.PrcMethod;
import com.orm.model.PrcMsgChat;
import com.orm.model.PrcMsgSanggahan;
import com.orm.model.PrcTemplate;
import com.orm.model.PrcTemplateDetail;
import com.orm.tools.AbstractCommand;

public class GlobalCommand extends AbstractCommand implements Serializable {
	private List<TodoUserCommand> listTodo;
	private List<TodoUserCommand> listTodoPanitiaPp;
	private List<TodoUserCommand> listTodoKontrak;
	private PrcMsgChat chat;
	private List<PrcMsgChat> listChat;
	private String broadCastMessage;
	private String kepada;
	
	public String getBroadCastMessage() {
		return broadCastMessage;
	}

	public void setBroadCastMessage(String broadCastMessage) {
		this.broadCastMessage = broadCastMessage;
	}

	public String getKepada() {
		return kepada;
	}

	public void setKepada(String kepada) {
		this.kepada = kepada;
	}

	private AdmIssues issues;
	private List<AdmIssues> listIssues;
	
	private AdmDelegasi admDelegasi;
	private List<AdmDelegasi> listAdmDelegasi;
	
	private AdmDept dept;
	private List<AdmDept> listDept;
	
	private AdmDistrict district;
	private List<AdmDistrict> listDistrict;
	private PrcMappingKeuangan keuangan;
	
	private AdmCountry country;
	private List<AdmCountry> listCountry;
	
	private AdmDoc doc;
	private List<AdmDoc> listDoc;
	
	private AdmRole role;
	private List<AdmRole> listRole;
	
	private AdmMenu menu;
	private List<AdmMenu> listMenu;
	
	private AdmRoleMenu roleMenu;
	private List<AdmRoleMenu> listRoleMenu;
	
	private AdmUser user;
	private List<AdmUser> listUser;
	
	private AdmUom uom;
	private List<AdmUom> listUom;
	
	private AdmProses proses;
	private List<AdmProses> listProses;
	
	private AdmProsesHirarki hirarki;
	private List<AdmProsesHirarki> listHirarki;
	
	private AdmCostCenter costCenter;
	private List<AdmCostCenter> listCostCenter;
	
	private AdmJadwalDrt jadwal;
	private List<AdmJadwalDrt> listJadwal;
	
	private PanitiaVnd panitiaVnd;
	private List<PanitiaVnd> listPanitiaVnd;
	
	private PanitiaVndDetail panitiaVndDetail;
	private List<PanitiaVndDetail> listPanitiaVndDetail;
	
	private List<PrcMainHeader> listPrcMainHeader;
	private List<PrcMainHeader> listPrcMainHeaderOeCreate;
	
	private PrcTemplate template;
	private List<PrcTemplate> listTemplate;
	private PrcTemplateDetail templateDetail;
	private List<PrcTemplateDetail> listTemplateDetail;
	
	private PrcMainHeader prcMainHeader;
	private PrcMainPreparation prcMainPreparation;
	
	private List<PrcMsgSanggahan> listSanggahan;
	private PrcMsgSanggahan msgSanggahan;
	private List<PrcMainVendor> listPrcVendor;
	private PrcMainVendor prcVendor;
	
	private File docsChat;
	private String docsChatFileName;

	
	public File getDocsChat() {
		return docsChat;
	}

	public void setDocsChat(File docsChat) {
		this.docsChat = docsChat;
	}

	public String getDocsChatFileName() {
		return docsChatFileName;
	}

	public void setDocsChatFileName(String docsChatFileName) {
		this.docsChatFileName = docsChatFileName;
	}

	public PrcMainVendor getPrcVendor() {
		return prcVendor;
	}

	public void setPrcVendor(PrcMainVendor prcVendor) {
		this.prcVendor = prcVendor;
	}

	public List<PrcMainVendor> getListPrcVendor() {
		return listPrcVendor;
	}

	public void setListPrcVendor(List<PrcMainVendor> listPrcVendor) {
		this.listPrcVendor = listPrcVendor;
	}

	public List<PrcMsgSanggahan> getListSanggahan() {
		return listSanggahan;
	}

	public void setListSanggahan(List<PrcMsgSanggahan> listSanggahan) {
		this.listSanggahan = listSanggahan;
	}

	public PrcMsgSanggahan getMsgSanggahan() {
		return msgSanggahan;
	}

	public void setMsgSanggahan(PrcMsgSanggahan msgSanggahan) {
		this.msgSanggahan = msgSanggahan;
	}

	public PrcMainHeader getPrcMainHeader() {
		return prcMainHeader;
	}

	public void setPrcMainHeader(PrcMainHeader prcMainHeader) {
		this.prcMainHeader = prcMainHeader;
	}

	public PrcMainPreparation getPrcMainPreparation() {
		return prcMainPreparation;
	}

	public void setPrcMainPreparation(PrcMainPreparation prcMainPreparation) {
		this.prcMainPreparation = prcMainPreparation;
	}

	public List<PrcMainHeader> getListPrcMainHeader() {
		return listPrcMainHeader;
	}

	public void setListPrcMainHeader(List<PrcMainHeader> listPrcMainHeader) {
		this.listPrcMainHeader = listPrcMainHeader;
	}

	public List<PrcMainHeader> getListPrcMainHeaderOeCreate() {
		return listPrcMainHeaderOeCreate;
	}

	public void setListPrcMainHeaderOeCreate(
			List<PrcMainHeader> listPrcMainHeaderOeCreate) {
		this.listPrcMainHeaderOeCreate = listPrcMainHeaderOeCreate;
	}

	public AdmDept getDept() {
		return dept;
	}

	public void setDept(AdmDept dept) {
		this.dept = dept;
	}

	public List<AdmDept> getListDept() {
		return listDept;
	}

	public void setListDept(List<AdmDept> listDept) {
		this.listDept = listDept;
	}

	public AdmDistrict getDistrict() {
		return district;
	}

	public void setDistrict(AdmDistrict district) {
		this.district = district;
	}

	public List<AdmDistrict> getListDistrict() {
		return listDistrict;
	}

	public void setListDistrict(List<AdmDistrict> listDistrict) {
		this.listDistrict = listDistrict;
	}

	public AdmCountry getCountry() {
		return country;
	}

	public void setCountry(AdmCountry country) {
		this.country = country;
	}

	public List<AdmCountry> getListCountry() {
		return listCountry;
	}

	public void setListCountry(List<AdmCountry> listCountry) {
		this.listCountry = listCountry;
	}

	public AdmDoc getDoc() {
		return doc;
	}

	public void setDoc(AdmDoc doc) {
		this.doc = doc;
	}

	public List<AdmDoc> getListDoc() {
		return listDoc;
	}

	public void setListDoc(List<AdmDoc> listDoc) {
		this.listDoc = listDoc;
	}

	public AdmRole getRole() {
		return role;
	}

	public void setRole(AdmRole role) {
		this.role = role;
	}

	public List<AdmRole> getListRole() {
		return listRole;
	}

	public void setListRole(List<AdmRole> listRole) {
		this.listRole = listRole;
	}

	public AdmMenu getMenu() {
		return menu;
	}

	public void setMenu(AdmMenu menu) {
		this.menu = menu;
	}

	public List<AdmMenu> getListMenu() {
		return listMenu;
	}

	public void setListMenu(List<AdmMenu> listMenu) {
		this.listMenu = listMenu;
	}

	public AdmUser getUser() {
		return user;
	}

	public void setUser(AdmUser user) {
		this.user = user;
	}

	public List<AdmUser> getListUser() {
		return listUser;
	}

	public void setListUser(List<AdmUser> listUser) {
		this.listUser = listUser;
	}

	public AdmUom getUom() {
		return uom;
	}

	public void setUom(AdmUom uom) {
		this.uom = uom;
	}

	public List<AdmUom> getListUom() {
		return listUom;
	}

	public void setListUom(List<AdmUom> listUom) {
		this.listUom = listUom;
	}

	public AdmProses getProses() {
		return proses;
	}

	public void setProses(AdmProses proses) {
		this.proses = proses;
	}

	public List<AdmProses> getListProses() {
		return listProses;
	}

	public void setListProses(List<AdmProses> listProses) {
		this.listProses = listProses;
	}

	public AdmProsesHirarki getHirarki() {
		return hirarki;
	}

	public void setHirarki(AdmProsesHirarki hirarki) {
		this.hirarki = hirarki;
	}

	public List<AdmProsesHirarki> getListHirarki() {
		return listHirarki;
	}

	public void setListHirarki(List<AdmProsesHirarki> listHirarki) {
		this.listHirarki = listHirarki;
	}

	public AdmRoleMenu getRoleMenu() {
		return roleMenu;
	}

	public void setRoleMenu(AdmRoleMenu roleMenu) {
		this.roleMenu = roleMenu;
	}

	public List<AdmRoleMenu> getListRoleMenu() {
		return listRoleMenu;
	}

	public void setListRoleMenu(List<AdmRoleMenu> listRoleMenu) {
		this.listRoleMenu = listRoleMenu;
	}

	public AdmCostCenter getCostCenter() {
		return costCenter;
	}

	public void setCostCenter(AdmCostCenter costCenter) {
		this.costCenter = costCenter;
	}

	public List<AdmCostCenter> getListCostCenter() {
		return listCostCenter;
	}

	public void setListCostCenter(List<AdmCostCenter> listCostCenter) {
		this.listCostCenter = listCostCenter;
	}

	public AdmJadwalDrt getJadwal() {
		return jadwal;
	}

	public void setJadwal(AdmJadwalDrt jadwal) {
		this.jadwal = jadwal;
	}

	public List<AdmJadwalDrt> getListJadwal() {
		return listJadwal;
	}

	public void setListJadwal(List<AdmJadwalDrt> listJadwal) {
		this.listJadwal = listJadwal;
	}

	public PanitiaVnd getPanitiaVnd() {
		return panitiaVnd;
	}

	public void setPanitiaVnd(PanitiaVnd panitiaVnd) {
		this.panitiaVnd = panitiaVnd;
	}

	public List<PanitiaVnd> getListPanitiaVnd() {
		return listPanitiaVnd;
	}

	public void setListPanitiaVnd(List<PanitiaVnd> listPanitiaVnd) {
		this.listPanitiaVnd = listPanitiaVnd;
	}

	public PanitiaVndDetail getPanitiaVndDetail() {
		return panitiaVndDetail;
	}

	public void setPanitiaVndDetail(PanitiaVndDetail panitiaVndDetail) {
		this.panitiaVndDetail = panitiaVndDetail;
	}

	public List<PanitiaVndDetail> getListPanitiaVndDetail() {
		return listPanitiaVndDetail;
	}

	public void setListPanitiaVndDetail(List<PanitiaVndDetail> listPanitiaVndDetail) {
		this.listPanitiaVndDetail = listPanitiaVndDetail;
	}

	public PrcTemplate getTemplate() {
		return template;
	}

	public void setTemplate(PrcTemplate template) {
		this.template = template;
	}

	public List<PrcTemplate> getListTemplate() {
		return listTemplate;
	}

	public void setListTemplate(List<PrcTemplate> listTemplate) {
		this.listTemplate = listTemplate;
	}

	public PrcTemplateDetail getTemplateDetail() {
		return templateDetail;
	}

	public void setTemplateDetail(PrcTemplateDetail templateDetail) {
		this.templateDetail = templateDetail;
	}

	public List<PrcTemplateDetail> getListTemplateDetail() {
		return listTemplateDetail;
	}

	public void setListTemplateDetail(List<PrcTemplateDetail> listTemplateDetail) {
		this.listTemplateDetail = listTemplateDetail;
	}

	public List<TodoUserCommand> getListTodo() {
		return listTodo;
	}

	public void setListTodo(List<TodoUserCommand> listTodo) {
		this.listTodo = listTodo;
	}

	public List<TodoUserCommand> getListTodoPanitiaPp() {
		return listTodoPanitiaPp;
	}

	public void setListTodoPanitiaPp(List<TodoUserCommand> listTodoPanitiaPp) {
		this.listTodoPanitiaPp = listTodoPanitiaPp;
	}

	public List<PrcMsgChat> getListChat() {
		return listChat;
	}

	public void setListChat(List<PrcMsgChat> listChat) {
		this.listChat = listChat;
	}

	public PrcMsgChat getChat() {
		return chat;
	}

	public void setChat(PrcMsgChat chat) {
		this.chat = chat;
	}

	public List<TodoUserCommand> getListTodoKontrak() {
		return listTodoKontrak;
	}

	public void setListTodoKontrak(List<TodoUserCommand> listTodoKontrak) {
		this.listTodoKontrak = listTodoKontrak;
	}

	public AdmDelegasi getAdmDelegasi() {
		return admDelegasi;
	}

	public void setAdmDelegasi(AdmDelegasi admDelegasi) {
		this.admDelegasi = admDelegasi;
	}

	public List<AdmDelegasi> getListAdmDelegasi() {
		return listAdmDelegasi;
	}

	public void setListAdmDelegasi(List<AdmDelegasi> listAdmDelegasi) {
		this.listAdmDelegasi = listAdmDelegasi;
	}

	public PrcMappingKeuangan getKeuangan() {
		return keuangan;
	}

	public void setKeuangan(PrcMappingKeuangan keuangan) {
		this.keuangan = keuangan;
	}

	public AdmIssues getIssues() {
		return issues;
	}

	public void setIssues(AdmIssues issues) {
		this.issues = issues;
	}

	public List<AdmIssues> getListIssues() {
		return listIssues;
	}

	public void setListIssues(List<AdmIssues> listIssues) {
		this.listIssues = listIssues;
	}
	
}
