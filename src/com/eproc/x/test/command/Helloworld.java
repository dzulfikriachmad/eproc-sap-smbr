package com.eproc.x.test.command;

import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.Seconds;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;

public class Helloworld implements Job {

	@Override
	public void execute(JobExecutionContext ctx)
			throws JobExecutionException {
		int remaining = Seconds.secondsBetween(new DateTime(ctx.getFireTime()),
				new DateTime(ctx.getTrigger().getEndTime())).getSeconds();
		System.out.println(">>> HAIAI=="+remaining);
	}
}
