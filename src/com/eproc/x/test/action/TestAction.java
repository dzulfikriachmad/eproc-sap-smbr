package com.eproc.x.test.action;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.eproc.core.BaseController;
import com.eproc.sap.controller.IntegrationService;
import com.eproc.sap.controller.IntegrationServiceImpl;
import com.eproc.x.test.command.TestCommand;

@ParentPackage("eproc")
@Namespace("/")
public class TestAction extends BaseController {
	private TestCommand model;
	
	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return model;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		model = new TestCommand();
	}

	@Action(
			results={
					@Result(name=SUCCESS, location="test.jsp")
			},value = "test"
	)	
	public String test() throws Exception{
		
        
      
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="typografi.jsp")
			},value = "typografi"
	)	
	public String typografi() throws Exception{		
		return SUCCESS;
	}
}
