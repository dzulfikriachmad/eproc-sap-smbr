package com.eproc.x.report.command;

public class ProcurementValueCommand {

	private Integer id;
	private String metode;
	private String EE;
	private String OE;
	private String kontrak;
	private String frequensi;
	
	//detail
	private Integer ppmId;
	private String nomorSpph;
	private String hoe;
	private String hang;
	private String hpp;
	private String hpq;
	private String hspph;
	private String namaBuyer;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMetode() {
		return metode;
	}
	public void setMetode(String metode) {
		this.metode = metode;
	}
	public String getEE() {
		return EE;
	}
	public void setEE(String eE) {
		EE = eE;
	}
	public String getOE() {
		return OE;
	}
	public void setOE(String oE) {
		OE = oE;
	}
	public String getKontrak() {
		return kontrak;
	}
	public void setKontrak(String kontrak) {
		this.kontrak = kontrak;
	}
	public String getFrequensi() {
		return frequensi;
	}
	public void setFrequensi(String frequensi) {
		this.frequensi = frequensi;
	}
	public Integer getPpmId() {
		return ppmId;
	}
	public void setPpmId(Integer ppmId) {
		this.ppmId = ppmId;
	}
	public String getNomorSpph() {
		return nomorSpph;
	}
	public void setNomorSpph(String nomorSpph) {
		this.nomorSpph = nomorSpph;
	}
	public String getHoe() {
		return hoe;
	}
	public void setHoe(String hoe) {
		this.hoe = hoe;
	}
	public String getHang() {
		return hang;
	}
	public void setHang(String hang) {
		this.hang = hang;
	}
	public String getHpp() {
		return hpp;
	}
	public void setHpp(String hpp) {
		this.hpp = hpp;
	}
	public String getHpq() {
		return hpq;
	}
	public void setHpq(String hpq) {
		this.hpq = hpq;
	}
	public String getHspph() {
		return hspph;
	}
	public void setHspph(String hspph) {
		this.hspph = hspph;
	}
	public String getNamaBuyer() {
		return namaBuyer;
	}
	public void setNamaBuyer(String namaBuyer) {
		this.namaBuyer = namaBuyer;
	}
	
}
