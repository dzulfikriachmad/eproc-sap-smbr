package com.eproc.x.report.command;

import com.orm.tools.AbstractCommand;

public class DataSheetPengadaanCommand extends AbstractCommand {

	private Integer ppmId;
	private String nomorPr;
	private String judul;
	private String deskripsi;
	private String tanggalOe;
	private String tanggalRks;
	private String tanggalMitra;
	private String tanggalBukaSpph;
	private String tanggalKonfirmasiAnggaran;
	private String tanggalNegosiasi;
	private String tanggalEvaluasi;
	public Integer getPpmId() {
		return ppmId;
	}
	public void setPpmId(Integer ppmId) {
		this.ppmId = ppmId;
	}
	public String getJudul() {
		return judul;
	}
	public void setJudul(String judul) {
		this.judul = judul;
	}
	public String getDeskripsi() {
		return deskripsi;
	}
	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}
	public String getTanggalOe() {
		return tanggalOe;
	}
	public void setTanggalOe(String tanggalOe) {
		this.tanggalOe = tanggalOe;
	}
	public String getTanggalRks() {
		return tanggalRks;
	}
	public void setTanggalRks(String tanggalRks) {
		this.tanggalRks = tanggalRks;
	}
	public String getTanggalMitra() {
		return tanggalMitra;
	}
	public void setTanggalMitra(String tanggalMitra) {
		this.tanggalMitra = tanggalMitra;
	}
	public String getTanggalBukaSpph() {
		return tanggalBukaSpph;
	}
	public void setTanggalBukaSpph(String tanggalBukaSpph) {
		this.tanggalBukaSpph = tanggalBukaSpph;
	}
	public String getTanggalKonfirmasiAnggaran() {
		return tanggalKonfirmasiAnggaran;
	}
	public void setTanggalKonfirmasiAnggaran(String tanggalKonfirmasiAnggaran) {
		this.tanggalKonfirmasiAnggaran = tanggalKonfirmasiAnggaran;
	}
	public String getTanggalNegosiasi() {
		return tanggalNegosiasi;
	}
	public void setTanggalNegosiasi(String tanggalNegosiasi) {
		this.tanggalNegosiasi = tanggalNegosiasi;
	}
	public String getTanggalEvaluasi() {
		return tanggalEvaluasi;
	}
	public void setTanggalEvaluasi(String tanggalEvaluasi) {
		this.tanggalEvaluasi = tanggalEvaluasi;
	}
	public String getNomorPr() {
		return nomorPr;
	}
	public void setNomorPr(String nomorPr) {
		this.nomorPr = nomorPr;
	}
	
	
}
