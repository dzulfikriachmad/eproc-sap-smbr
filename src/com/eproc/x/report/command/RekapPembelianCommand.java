package com.eproc.x.report.command;

import com.orm.tools.AbstractCommand;

public class RekapPembelianCommand extends AbstractCommand {

	private Integer ppmId;
	private String nomorPr;
	private String tanggalOe;
	private String jumlah;
	private String satuan;
	private String namaBarang;
	private String harga;
	private String hargaTotal;
	private String nomorPo;
	private String tanggalPo;
	private String tanggalAkhirPo;
	private String namaVendor;
	private String tanggalPenyerahan;
	public Integer getPpmId() {
		return ppmId;
	}
	public void setPpmId(Integer ppmId) {
		this.ppmId = ppmId;
	}
	public String getNomorPr() {
		return nomorPr;
	}
	public void setNomorPr(String nomorPr) {
		this.nomorPr = nomorPr;
	}
	public String getTanggalOe() {
		return tanggalOe;
	}
	public void setTanggalOe(String tanggalOe) {
		this.tanggalOe = tanggalOe;
	}
	public String getJumlah() {
		return jumlah;
	}
	public void setJumlah(String jumlah) {
		this.jumlah = jumlah;
	}
	public String getSatuan() {
		return satuan;
	}
	public void setSatuan(String satuan) {
		this.satuan = satuan;
	}
	public String getNamaBarang() {
		return namaBarang;
	}
	public void setNamaBarang(String namaBarang) {
		this.namaBarang = namaBarang;
	}
	public String getHarga() {
		return harga;
	}
	public void setHarga(String harga) {
		this.harga = harga;
	}
	public String getHargaTotal() {
		return hargaTotal;
	}
	public void setHargaTotal(String hargaTotal) {
		this.hargaTotal = hargaTotal;
	}
	public String getNomorPo() {
		return nomorPo;
	}
	public void setNomorPo(String nomorPo) {
		this.nomorPo = nomorPo;
	}
	public String getTanggalPo() {
		return tanggalPo;
	}
	public void setTanggalPo(String tanggalPo) {
		this.tanggalPo = tanggalPo;
	}
	public String getTanggalAkhirPo() {
		return tanggalAkhirPo;
	}
	public void setTanggalAkhirPo(String tanggalAkhirPo) {
		this.tanggalAkhirPo = tanggalAkhirPo;
	}
	public String getNamaVendor() {
		return namaVendor;
	}
	public void setNamaVendor(String namaVendor) {
		this.namaVendor = namaVendor;
	}
	public String getTanggalPenyerahan() {
		return tanggalPenyerahan;
	}
	public void setTanggalPenyerahan(String tanggalPenyerahan) {
		this.tanggalPenyerahan = tanggalPenyerahan;
	}
	
}
