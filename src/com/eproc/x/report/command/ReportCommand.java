package com.eproc.x.report.command;

import java.io.File;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.eproc.x.rfq.command.MainMasterJdeCommand;
import com.eproc.x.rfq.command.TabulasiPenilaian;
import com.orm.model.AdmCostCenter;
import com.orm.model.AdmDept;
import com.orm.model.AdmDistrict;
import com.orm.model.AdmEmployee;
import com.orm.model.AdmUom;
import com.orm.model.AdmUser;
import com.orm.model.ComCatalog;
import com.orm.model.ComGroup;
import com.orm.model.CtrContractComment;
import com.orm.model.CtrContractHeader;
import com.orm.model.CtrContractItem;
import com.orm.model.CtrContractPb;
import com.orm.model.CtrContractTop;
import com.orm.model.HistMainComment;
import com.orm.model.HistoryHargaNegosiasi;
import com.orm.model.PanitiaPengadaan;
import com.orm.model.PanitiaPengadaanDetail;
import com.orm.model.PrcAnggaranJdeDetail;
import com.orm.model.PrcMainDoc;
import com.orm.model.PrcMainHeader;
import com.orm.model.PrcMainItem;
import com.orm.model.PrcMainPreparation;
import com.orm.model.PrcMainVendor;
import com.orm.model.PrcManualDoc;
import com.orm.model.PrcMsgNegotiation;
import com.orm.tools.AbstractCommand;

public class ReportCommand extends AbstractCommand implements Serializable {
	private List<AdmDistrict> listAdmDistrict;
	private List<AdmDept> listAdmDept;
	private List<AdmUser> listUser;
	private AdmDistrict dist;
	private AdmDept dept;
	private List<PrcMainVendor> listVendor;
	private PrcMainPreparation prcMainPreparation;
	private List<TabulasiPenilaian> listTabulasi;
	private List<PrcMsgNegotiation> listNegosiasi;
	private List<PrcMainDoc> listPrcMainDoc;
	private Integer tahunAnggaran;
	private String kodeAnggaran;
	private List<MainMasterJdeCommand> listJdeMasterMain;
	
	private CtrContractHeader contractHeader;
	private List<CtrContractHeader> listContractHeader;
	private List<CtrContractItem> listContractItem;
	
	private CtrContractTop contractTop;
	private CtrContractPb contractPb;
	private List<CtrContractTop> listContractTop;
	private List<CtrContractPb> listContractPb;
	private CtrContractComment ctrContractComment;
	private List<CtrContractComment> listContractComment;
	
	private Date startDate;
	private Date endDate;
	private List<ProcurementValueCommand> listProcurementValue;
	private List<ProcurementProsesCommand> listProcurementProses;
	private List<DataSheetPengadaanCommand> listDataSheetPengadaan;
	private List<RekapPembelianCommand> listRekapPembelian;
	
	private PrcAnggaranJdeDetail anggaranJdeDetail;
	private List<PrcAnggaranJdeDetail> listAnggaranDetail;
	
	private Integer prosesId;
	private Integer prosesLevel;
	private Integer metode;
	
	private PanitiaPengadaan panitia;
	private List<PanitiaPengadaanDetail> listPanitiaDetail;
	private PanitiaPengadaanDetail panitiaDetail;
	
	private List<File> ppdDocument;
	private List<String> ppdDocumentFileName;
	private List<String> ppdCategory;
	private List<String> ppdDescription;
	private List<HistoryHargaNegosiasi> listHargaNegosiasi;
	
	private List<ComGroup> listGroup;
	
	private List<AdmUser> listEstimator;

	private List<Integer> listOfYears;
	
	private Integer year;
	
	private List<PrcManualDoc> listPrcManualDoc;

	public List<AdmDistrict> getListAdmDistrict() {
		return listAdmDistrict;
	}

	public void setListAdmDistrict(List<AdmDistrict> listAdmDistrict) {
		this.listAdmDistrict = listAdmDistrict;
	}
	

	public List<AdmDept> getListAdmDept() {
		return listAdmDept;
	}

	public void setListAdmDept(List<AdmDept> listAdmDept) {
		this.listAdmDept = listAdmDept;
	}

	public AdmDistrict getDist() {
		return dist;
	}

	public void setDist(AdmDistrict dist) {
		this.dist = dist;
	}

	public AdmDept getDept() {
		return dept;
	}

	public void setDept(AdmDept dept) {
		this.dept = dept;
	}
	
	private PrcMainHeader prcMainHeader;
	private PrcMainItem prcMainItem;
	private AdmEmployee admEmployee;
	private HistMainComment histMainComment;
	
	private List<PrcMainHeader> listPrcMainHeader;
	private List<PrcMainItem> listPrcMainItem;
	private List<AdmUom> listAdmUom;
	private List<AdmCostCenter> listAdmCostCenter;
	private List<ComCatalog> listComCatalog;
	private List<HistMainComment> listHistMainComment;
	
	private File docsKomentar;
	private String docsKomentarFileName;
	private String docsKomentarContentType;
	
	public PrcMainHeader getPrcMainHeader() {
		return prcMainHeader;
	}
	
	public void setPrcMainHeader(PrcMainHeader prcMainHeader) {
		this.prcMainHeader = prcMainHeader;
	}
	
	public List<PrcMainHeader> getListPrcMainHeader() {
		return listPrcMainHeader;
	}
	
	public void setListPrcMainHeader(List<PrcMainHeader> listPrcMainHeader) {
		this.listPrcMainHeader = listPrcMainHeader;
	}

	public AdmEmployee getAdmEmployee() {
		return admEmployee;
	}

	public void setAdmEmployee(AdmEmployee admEmployee) {
		this.admEmployee = admEmployee;
	}

	public PrcMainItem getPrcMainItem() {
		return prcMainItem;
	}

	public void setPrcMainItem(PrcMainItem prcMainItem) {
		this.prcMainItem = prcMainItem;
	}

	public List<AdmUom> getListAdmUom() {
		return listAdmUom;
	}

	public void setListAdmUom(List<AdmUom> listAdmUom) {
		this.listAdmUom = listAdmUom;
	}

	public List<AdmCostCenter> getListAdmCostCenter() {
		return listAdmCostCenter;
	}

	public void setListAdmCostCenter(List<AdmCostCenter> listAdmCostCenter) {
		this.listAdmCostCenter = listAdmCostCenter;
	}

	public List<ComCatalog> getListComCatalog() {
		return listComCatalog;
	}

	public void setListComCatalog(List<ComCatalog> listComCatalog) {
		this.listComCatalog = listComCatalog;
	}

	public List<PrcMainItem> getListPrcMainItem() {
		return listPrcMainItem;
	}

	public void setListPrcMainItem(List<PrcMainItem> listPrcMainItem) {
		this.listPrcMainItem = listPrcMainItem;
	}

	public List<HistMainComment> getListHistMainComment() {
		return listHistMainComment;
	}

	public void setListHistMainComment(List<HistMainComment> listHistMainComment) {
		this.listHistMainComment = listHistMainComment;
	}

	public HistMainComment getHistMainComment() {
		return histMainComment;
	}

	public void setHistMainComment(HistMainComment histMainComment) {
		this.histMainComment = histMainComment;
	}

	public File getDocsKomentar() {
		return docsKomentar;
	}

	public void setDocsKomentar(File docsKomentar) {
		this.docsKomentar = docsKomentar;
	}

	public String getDocsKomentarFileName() {
		return docsKomentarFileName;
	}

	public void setDocsKomentarFileName(String docsKomentarFileName) {
		this.docsKomentarFileName = docsKomentarFileName;
	}

	public String getDocsKomentarContentType() {
		return docsKomentarContentType;
	}

	public void setDocsKomentarContentType(String docsKomentarContentType) {
		this.docsKomentarContentType = docsKomentarContentType;
	}

	public List<PrcMainVendor> getListVendor() {
		return listVendor;
	}

	public void setListVendor(List<PrcMainVendor> listVendor) {
		this.listVendor = listVendor;
	}

	public PrcMainPreparation getPrcMainPreparation() {
		return prcMainPreparation;
	}

	public void setPrcMainPreparation(PrcMainPreparation prcMainPreparation) {
		this.prcMainPreparation = prcMainPreparation;
	}

	public CtrContractHeader getContractHeader() {
		return contractHeader;
	}

	public void setContractHeader(CtrContractHeader contractHeader) {
		this.contractHeader = contractHeader;
	}

	public List<CtrContractHeader> getListContractHeader() {
		return listContractHeader;
	}

	public void setListContractHeader(List<CtrContractHeader> listContractHeader) {
		this.listContractHeader = listContractHeader;
	}

	public List<CtrContractItem> getListContractItem() {
		return listContractItem;
	}

	public void setListContractItem(List<CtrContractItem> listContractItem) {
		this.listContractItem = listContractItem;
	}

	public CtrContractTop getContractTop() {
		return contractTop;
	}

	public void setContractTop(CtrContractTop contractTop) {
		this.contractTop = contractTop;
	}

	public CtrContractPb getContractPb() {
		return contractPb;
	}

	public void setContractPb(CtrContractPb contractPb) {
		this.contractPb = contractPb;
	}

	public List<CtrContractTop> getListContractTop() {
		return listContractTop;
	}

	public void setListContractTop(List<CtrContractTop> listContractTop) {
		this.listContractTop = listContractTop;
	}

	public List<CtrContractPb> getListContractPb() {
		return listContractPb;
	}

	public void setListContractPb(List<CtrContractPb> listContractPb) {
		this.listContractPb = listContractPb;
	}

	public CtrContractComment getCtrContractComment() {
		return ctrContractComment;
	}

	public void setCtrContractComment(CtrContractComment ctrContractComment) {
		this.ctrContractComment = ctrContractComment;
	}

	public List<CtrContractComment> getListContractComment() {
		return listContractComment;
	}

	public void setListContractComment(List<CtrContractComment> listContractComment) {
		this.listContractComment = listContractComment;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public List<ProcurementValueCommand> getListProcurementValue() {
		return listProcurementValue;
	}

	public void setListProcurementValue(
			List<ProcurementValueCommand> listProcurementValue) {
		this.listProcurementValue = listProcurementValue;
	}

	public Integer getMetode() {
		return metode;
	}

	public void setMetode(Integer metode) {
		this.metode = metode;
	}

	public List<TabulasiPenilaian> getListTabulasi() {
		return listTabulasi;
	}

	public void setListTabulasi(List<TabulasiPenilaian> listTabulasi) {
		this.listTabulasi = listTabulasi;
	}

	public List<PrcMsgNegotiation> getListNegosiasi() {
		return listNegosiasi;
	}

	public void setListNegosiasi(List<PrcMsgNegotiation> listNegosiasi) {
		this.listNegosiasi = listNegosiasi;
	}

	public List<PrcMainDoc> getListPrcMainDoc() {
		return listPrcMainDoc;
	}

	public void setListPrcMainDoc(List<PrcMainDoc> listPrcMainDoc) {
		this.listPrcMainDoc = listPrcMainDoc;
	}

	public List<ProcurementProsesCommand> getListProcurementProses() {
		return listProcurementProses;
	}

	public void setListProcurementProses(
			List<ProcurementProsesCommand> listProcurementProses) {
		this.listProcurementProses = listProcurementProses;
	}

	public Integer getProsesId() {
		return prosesId;
	}

	public void setProsesId(Integer prosesId) {
		this.prosesId = prosesId;
	}

	public Integer getProsesLevel() {
		return prosesLevel;
	}

	public void setProsesLevel(Integer prosesLevel) {
		this.prosesLevel = prosesLevel;
	}

	public List<DataSheetPengadaanCommand> getListDataSheetPengadaan() {
		return listDataSheetPengadaan;
	}

	public void setListDataSheetPengadaan(
			List<DataSheetPengadaanCommand> listDataSheetPengadaan) {
		this.listDataSheetPengadaan = listDataSheetPengadaan;
	}

	public List<RekapPembelianCommand> getListRekapPembelian() {
		return listRekapPembelian;
	}

	public void setListRekapPembelian(List<RekapPembelianCommand> listRekapPembelian) {
		this.listRekapPembelian = listRekapPembelian;
	}

	public List<PrcAnggaranJdeDetail> getListAnggaranDetail() {
		return listAnggaranDetail;
	}

	public void setListAnggaranDetail(List<PrcAnggaranJdeDetail> listAnggaranDetail) {
		this.listAnggaranDetail = listAnggaranDetail;
	}

	public PrcAnggaranJdeDetail getAnggaranJdeDetail() {
		return anggaranJdeDetail;
	}

	public void setAnggaranJdeDetail(PrcAnggaranJdeDetail anggaranJdeDetail) {
		this.anggaranJdeDetail = anggaranJdeDetail;
	}

	public Integer getTahunAnggaran() {
		return tahunAnggaran;
	}

	public void setTahunAnggaran(Integer tahunAnggaran) {
		this.tahunAnggaran = tahunAnggaran;
	}

	public String getKodeAnggaran() {
		return kodeAnggaran;
	}

	public void setKodeAnggaran(String kodeAnggaran) {
		this.kodeAnggaran = kodeAnggaran;
	}

	public List<MainMasterJdeCommand> getListJdeMasterMain() {
		return listJdeMasterMain;
	}

	public void setListJdeMasterMain(List<MainMasterJdeCommand> listJdeMasterMain) {
		this.listJdeMasterMain = listJdeMasterMain;
	}

	public PanitiaPengadaan getPanitia() {
		return panitia;
	}

	public void setPanitia(PanitiaPengadaan panitia) {
		this.panitia = panitia;
	}

	public List<PanitiaPengadaanDetail> getListPanitiaDetail() {
		return listPanitiaDetail;
	}

	public void setListPanitiaDetail(List<PanitiaPengadaanDetail> listPanitiaDetail) {
		this.listPanitiaDetail = listPanitiaDetail;
	}

	public List<AdmUser> getListUser() {
		return listUser;
	}

	public void setListUser(List<AdmUser> listUser) {
		this.listUser = listUser;
	}

	public List<File> getPpdDocument() {
		return ppdDocument;
	}

	public void setPpdDocument(List<File> ppdDocument) {
		this.ppdDocument = ppdDocument;
	}

	public List<String> getPpdDocumentFileName() {
		return ppdDocumentFileName;
	}

	public void setPpdDocumentFileName(List<String> ppdDocumentFileName) {
		this.ppdDocumentFileName = ppdDocumentFileName;
	}

	public List<String> getPpdCategory() {
		return ppdCategory;
	}

	public void setPpdCategory(List<String> ppdCategory) {
		this.ppdCategory = ppdCategory;
	}

	public List<String> getPpdDescription() {
		return ppdDescription;
	}

	public void setPpdDescription(List<String> ppdDescription) {
		this.ppdDescription = ppdDescription;
	}

	public PanitiaPengadaanDetail getPanitiaDetail() {
		return panitiaDetail;
	}

	public void setPanitiaDetail(PanitiaPengadaanDetail panitiaDetail) {
		this.panitiaDetail = panitiaDetail;
	}

	public List<ComGroup> getListGroup() {
		return listGroup;
	}

	public void setListGroup(List<ComGroup> listGroup) {
		this.listGroup = listGroup;
	}

	public List<HistoryHargaNegosiasi> getListHargaNegosiasi() {
		return listHargaNegosiasi;
	}

	public void setListHargaNegosiasi(List<HistoryHargaNegosiasi> listHargaNegosiasi) {
		this.listHargaNegosiasi = listHargaNegosiasi;
	}

	public List<AdmUser> getListEstimator() {
		return listEstimator;
	}

	public void setListEstimator(List<AdmUser> listEstimator) {
		this.listEstimator = listEstimator;
	}

	public List<Integer> getListOfYears() {
		return listOfYears;
	}

	public void setListOfYears(List<Integer> listOfYears) {
		this.listOfYears = listOfYears;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public List<PrcManualDoc> getListPrcManualDoc() {
		return listPrcManualDoc;
	}

	public void setListPrcManualDoc(List<PrcManualDoc> listPrcManualDoc) {
		this.listPrcManualDoc = listPrcManualDoc;
	}

	
}
