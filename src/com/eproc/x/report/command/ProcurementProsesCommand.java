package com.eproc.x.report.command;

import com.orm.tools.AbstractCommand;

public class ProcurementProsesCommand extends AbstractCommand {

	private Integer prosesId;
	private String prosesName;
	private Integer jumlah;
	private Integer tingkat;
	public Integer getProsesId() {
		return prosesId;
	}
	public void setProsesId(Integer prosesId) {
		this.prosesId = prosesId;
	}
	public String getProsesName() {
		return prosesName;
	}
	public void setProsesName(String prosesName) {
		this.prosesName = prosesName;
	}
	public Integer getJumlah() {
		return jumlah;
	}
	public void setJumlah(Integer jumlah) {
		this.jumlah = jumlah;
	}
	public Integer getTingkat() {
		return tingkat;
	}
	public void setTingkat(Integer tingkat) {
		this.tingkat = tingkat;
	}
	
	
}
