package com.eproc.x.report.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.WebUtils;

import com.eproc.core.BaseController;
import com.eproc.core.ReportGenerator;
import com.eproc.sap.controller.IntegrationServiceImpl;
import com.eproc.utils.QueryUtil;
import com.eproc.utils.SessionGetter;
import com.eproc.utils.UploadUtil;
import com.eproc.x.report.command.ReportCommand;
import com.orm.model.AdmDept;
import com.orm.model.AdmEmployee;
import com.orm.model.PrcMainHeader;
import com.orm.model.PrcMainItem;

import eproc.controller.AdmService;
import eproc.controller.AdmServiceImpl;
import eproc.controller.PrcService;
import eproc.controller.PrcServiceImpl;
import eproc.controller.RfqService;
import eproc.controller.RfqServiceImpl;
import eproc.controller.RptService;
import eproc.controller.RptServiceImpl;
import eproc.controller.ToolsService;
import eproc.controller.ToolsServiceImpl;

@Namespace("/")
@ParentPackage("eproc")
public class ReportAction extends BaseController {
	private ReportCommand model;
	private ReportGenerator generator;
	private RptService rptService;
	private PrcService prcService;
	private AdmService admService;
	private ToolsService toolsService;
	private RfqService rfqService;

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return model;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		model = new ReportCommand();
		generator = new ReportGenerator();
		rptService = new RptServiceImpl();
		admService = new AdmServiceImpl();
		prcService = new PrcServiceImpl();
		toolsService = new ToolsServiceImpl();
		rfqService = new RfqServiceImpl();
	}
	
	@Action(
			results={
					@Result(name = SUCCESS, location="report/reportVendor.jsp" )
			}, value = "report/vendor"
	)
	public String reportVendor() throws Exception{
		model.setListAdmDistrict(admService.listAdmDistrict());
		
		if(WebUtils.hasSubmitParameter(request, "view")){
			request.setAttribute("reportVendor", generator.statistikVendor(rptService.getStatisticVendor(model.getDist())));
			return SUCCESS;
		}
		//Fussion Chart 
		request.setAttribute("reportVendor", generator.statistikVendor(rptService.getStatisticVendor()));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name = SUCCESS, location="report/reportPrc.jsp" )
			}, value = "report/prc"
	)
	public String reportPrc() throws Exception{
		model.setListAdmDept(admService.listAdmDept());
		if(WebUtils.hasSubmitParameter(request, "view")){
			request.setAttribute("reportPrc", generator.statistikPrc(rptService.getStatisticProc(model.getDept())));
			return SUCCESS;
		}
		AdmDept dept = new AdmDept();
		dept.setId(0);
		
		request.setAttribute("reportPrc", generator.statistikPrc(rptService.getStatisticProc(dept)));
		return SUCCESS;
	}
	
	
	/***
	 * 
	 * Report Permintaan dan SPPH
	 * 
	 */
	
	@Action(
			results={
					@Result(name=SUCCESS,location="report/reportPermintaan.jsp"),
					@Result(name="user", location="report/viewReportPermintaanUser.jsp"),
					@Result(name="view",location="report/viewReportPermintaan.jsp"),
					@Result(name="ok",location="report/permintaan", type="redirectAction"),
			},value="report/permintaan"
			)
	public String reportPr() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "proses")){
				PrcMainHeader prcMainHeader = prcService.getPrcMainHeaderSPPHById(model.getPrcMainHeader().getPpmId());
				AdmEmployee admEmployee = SessionGetter.getUser().getAdmEmployee();
				model.setPrcMainHeader(prcMainHeader);
				model.setAdmEmployee(admEmployee);
				model.setListHistMainComment(prcService.listHistMainCommentByPrcHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId())));
				List<PrcMainItem> listItem = prcService.listPrcMainItemByHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId()));
				model.setListPrcMainItem(listItem);
				model.setListVendor(prcService.listMitraKerjaByHeader(prcMainHeader));
				model.setPrcMainPreparation(prcService.getPrcMainPreparationByHeader(prcMainHeader));
				model.setListTabulasi(rfqService.getListTabulasi(QueryUtil.tabulasiEvaluasi(prcMainHeader.getPpmId())));
				model.setListNegosiasi(rfqService.listNegosiasiByMainHeader(model.getPrcMainHeader()));
				model.setListPrcMainDoc(prcService.listPrcMainDocByHeader(prcMainHeader));
				if(prcMainHeader.getPanitia()!=null){
					model.setPanitia(prcService.getPanitiaPengadaan(prcMainHeader.getPanitia()));
					model.setListPanitiaDetail(prcService.getListPanitiaPengadaanDetail(prcMainHeader.getPanitia()));
				}
				model.setListGroup(prcService.getListComGrup(null));
				model.setListHargaNegosiasi(rfqService.listHargaNegosiasi(prcMainHeader));
				model.setListPrcManualDoc(prcService.listPrcManualDocByHeader(prcMainHeader));
				if((admEmployee.getAdmRoleByEmpRole1().getId() == 113) || 
						(admEmployee.getAdmRoleByEmpRole1().getId()==113  
						&& admEmployee.getAdmRoleByEmpRole2().getId()==113 
						&& admEmployee.getAdmRoleByEmpRole3().getId()==113
						&& admEmployee.getAdmRoleByEmpRole4().getId()==113)
						){
					return "user";
				}else {
					return "view";
				}
				
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
			e.printStackTrace();
			return "ok";
		}
		model.setListOfYears(toolsService.getListOfYears());
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="report/ajax/reportPermintaan.jsp"),
					@Result(name="search",location="report/ajax/reportPermintaanSearch.jsp")
			},value="ajax/report/searchPengadaan"
			)
	public String searchPr() throws Exception{
		try {
			model.setListPrcMainHeader(toolsService.searchPrcMainHeaderAllByYear(model.getPrcMainHeader(), model.getYear()));
			return "search";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="report/ajax/reportPermintaan.jsp")
			},value="ajax/report/permintaan"
			)
	public String ajaxReportPr() throws Exception{
		try {
			model.setResultSize(prcService.countPrcMainHeaderPRSPPHReport(null));
			model.setListPrcMainHeader(prcService.listPrcMainHeaderPRSPPHReport(null,model.getPage(request),10));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	
	/***
	 * 
	 * Report Permintaan dan SPPH
	 * 
	 */
	
	@Action(
			results={
					@Result(name=SUCCESS,location="report/reportPermintaanRfq.jsp"),
					@Result(name="user", location="report/viewReportPermintaanUser.jsp"),
					@Result(name="view",location="report/viewReportPermintaan.jsp"),
					@Result(name="ok",location="report/permintaanRfq", type="redirectAction"),
			},value="report/permintaanRfq"
			)
	public String reportRfq() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "proses")){
				PrcMainHeader prcMainHeader = prcService.getPrcMainHeaderSPPHById(model.getPrcMainHeader().getPpmId());
				AdmEmployee admEmployee = SessionGetter.getUser().getAdmEmployee();
				model.setPrcMainHeader(prcMainHeader);
				model.setAdmEmployee(admEmployee);
				model.setListHistMainComment(prcService.listHistMainCommentByPrcHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId())));
				List<PrcMainItem> listItem = prcService.listPrcMainItemByHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId()));
				model.setListPrcMainItem(listItem);
				model.setListVendor(prcService.listMitraKerjaByHeader(prcMainHeader));
				model.setPrcMainPreparation(prcService.getPrcMainPreparationByHeader(prcMainHeader));
				model.setListTabulasi(rfqService.getListTabulasi(QueryUtil.tabulasiEvaluasi(prcMainHeader.getPpmId())));
				model.setListNegosiasi(rfqService.listNegosiasiByMainHeader(model.getPrcMainHeader()));
				model.setListPrcMainDoc(prcService.listPrcMainDocByHeader(prcMainHeader));
				model.setListPrcManualDoc(prcService.listPrcManualDocByHeader(prcMainHeader));
				if(prcMainHeader.getPanitia()!=null){
					model.setPanitia(prcService.getPanitiaPengadaan(prcMainHeader.getPanitia()));
					model.setListPanitiaDetail(prcService.getListPanitiaPengadaanDetail(prcMainHeader.getPanitia()));
				}
				model.setListGroup(prcService.getListComGrup(null));
				model.setListHargaNegosiasi(rfqService.listHargaNegosiasi(prcMainHeader));
				if((admEmployee.getAdmRoleByEmpRole1().getId() == 113) || 
						(admEmployee.getAdmRoleByEmpRole1().getId()==113  
						&& admEmployee.getAdmRoleByEmpRole2().getId()==113 
						&& admEmployee.getAdmRoleByEmpRole3().getId()==113
						&& admEmployee.getAdmRoleByEmpRole4().getId()==113)
						){
					return "user";
				}else {
					return "view";
				}
				
			}
			model.setListOfYears(toolsService.getListOfYears());
		} catch (Exception e) {
			addActionError(e.getMessage());
			e.printStackTrace();
			return "ok";
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="report/ajax/reportPermintaanRfq.jsp"),
					@Result(name="search",location="report/ajax/reportPermintaanSearchRfq.jsp")
			},value="ajax/report/searchPengadaanRfq"
			)
	public String searchRfq() throws Exception{
		try {
			model.setListPrcMainHeader(toolsService.searchPrcMainHeaderRfq(model.getPrcMainHeader(), model.getYear()));
			return "search";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="report/ajax/reportPermintaanRfq.jsp")
			},value="ajax/report/permintaanRfq"
			)
	public String ajaxReportRfq() throws Exception{
		try {
			model.setResultSize(prcService.countPrcMainHeaderPRSPPHReportRfq(null));
			model.setListPrcMainHeader(prcService.listPrcMainHeaderPRSPPHReportRfq(null,model.getPage(request),10));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	
	/***
	 * 
	 * Report Permintaan dan SPPH
	 * 
	 */
	
	@Action(
			results={
					@Result(name=SUCCESS,location="report/reportPermintaanBatal.jsp"),
					@Result(name="view",location="report/viewReportPermintaanBatal.jsp"),
					@Result(name="ok",location="report/permintaanBatal", type="redirectAction"),
			},value="report/permintaanBatal"
			)
	public String reportPrBatal() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "proses")){
				PrcMainHeader prcMainHeader = prcService.getPrcMainHeaderSPPHById(model.getPrcMainHeader().getPpmId());
				AdmEmployee admEmployee = SessionGetter.getUser().getAdmEmployee();
				model.setPrcMainHeader(prcMainHeader);
				model.setAdmEmployee(admEmployee);
				model.setListHistMainComment(prcService.listHistMainCommentByPrcHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId())));
				List<PrcMainItem> listItem = prcService.listPrcMainItemByHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId()));
				model.setListPrcMainItem(listItem);
				model.setListVendor(prcService.listMitraKerjaByHeader(prcMainHeader));
				model.setPrcMainPreparation(prcService.getPrcMainPreparationByHeader(prcMainHeader));
				model.setListTabulasi(rfqService.getListTabulasi(QueryUtil.tabulasiEvaluasi(prcMainHeader.getPpmId())));
				model.setListNegosiasi(rfqService.listNegosiasiByMainHeader(model.getPrcMainHeader()));
				model.setListPrcMainDoc(prcService.listPrcMainDocByHeader(prcMainHeader));
				model.setListHargaNegosiasi(rfqService.listHargaNegosiasi(prcMainHeader));
				return "view";
			}else if(WebUtils.hasSubmitParameter(request, "batal")){
				if(model.getDocsKomentar()!=null){
					String docName = UploadUtil.uploadDocsFile(model.getDocsKomentar(), model.getDocsKomentarFileName());
					model.getHistMainComment().setDocument(docName);
				}
				prcService.batalPrSpph(model.getPrcMainHeader(),model.getHistMainComment());
				addActionError("Data Sudah Dihapus");
				return "ok";
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
			e.printStackTrace();
			return "ok";
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="report/ajax/reportPermintaanBatal.jsp")
			},value="ajax/report/searchPengadaanBatal"
			)
	public String searchPrBatal() throws Exception{
		try {
			model.setListPrcMainHeader(toolsService.searchPrcMainHeaderAll(model.getPrcMainHeader(), model.getYear()));
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="report/ajax/reportPermintaanBatal.jsp")
			},value="ajax/report/permintaanBatal"
			)
	public String ajaxReportPrBatal() throws Exception{
		try {
			model.setResultSize(prcService.countPrcMainHeaderPRSPPHReport(null));
			model.setListPrcMainHeader(prcService.listPrcMainHeaderPRSPPHReport(null,model.getPage(request),10));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	/**
	 * report by kantor
	 */
	
	@Action(
			results={
					@Result(name=SUCCESS,location="report/reportPermintaanKantor.jsp"),
					@Result(name="view",location="report/viewReportPermintaanKantor.jsp"),
					@Result(name="ok",location="report/permintaanKantor", type="redirectAction"),
			},value="report/permintaanKantor"
			)
	public String reportPrKantor() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "proses")){
				PrcMainHeader prcMainHeader = prcService.getPrcMainHeaderSPPHById(model.getPrcMainHeader().getPpmId());
				AdmEmployee admEmployee = SessionGetter.getUser().getAdmEmployee();
				model.setPrcMainHeader(prcMainHeader);
				model.setAdmEmployee(admEmployee);
				model.setListHistMainComment(prcService.listHistMainCommentByPrcHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId())));
				List<PrcMainItem> listItem = prcService.listPrcMainItemByHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId()));
				model.setListPrcMainItem(listItem);
				model.setListVendor(prcService.listMitraKerjaByHeader(prcMainHeader));
				model.setPrcMainPreparation(prcService.getPrcMainPreparationByHeader(prcMainHeader));
				model.setListTabulasi(rfqService.getListTabulasi(QueryUtil.tabulasiEvaluasi(prcMainHeader.getPpmId())));
				model.setListNegosiasi(rfqService.listNegosiasiByMainHeader(model.getPrcMainHeader()));
				model.setListPrcMainDoc(prcService.listPrcMainDocByHeader(prcMainHeader));
				model.setListHargaNegosiasi(rfqService.listHargaNegosiasi(prcMainHeader));
				return "view";
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
			e.printStackTrace();
			return "ok";
		}
		model.setListOfYears(toolsService.getListOfYears());
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="report/ajax/reportPermintaanKantor.jsp")
			},value="ajax/report/searchPengadaanKantor"
			)
	public String searchPrKantor() throws Exception{
		try {
			model.getPrcMainHeader().setDistrictUser(SessionGetter.getUser().getAdmEmployee().getAdmDept().getAdmDistrict());
			model.setListPrcMainHeader(toolsService.searchPrcMainHeaderAllByYear(model.getPrcMainHeader(), model.getYear()));
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="report/ajax/reportPermintaanKantor.jsp")
			},value="ajax/report/permintaanKantor"
			)
	public String ajaxReportPrKantor() throws Exception{
		try {
			model.setResultSize(prcService.countPrcMainHeaderPRSPPHReportByYear(SessionGetter.getUser().getAdmEmployee().getAdmDept().getAdmDistrict(), model.getYear()));
			model.setListPrcMainHeader(prcService.listPrcMainHeaderPRSPPHReportByYear(SessionGetter.getUser().getAdmEmployee().getAdmDept().getAdmDistrict(),model.getPage(request),10, model.getYear()));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	/**
	 * 
	 * Report Kontrak
	 * 
	 */
	
	@Action(
			results={
					@Result(name=SUCCESS,location="report/listReportKontrak.jsp"),
					@Result(name="view",location="report/viewReportKontrak.jsp"),
					@Result(name="ok",location="report/kontrak", type="redirectAction"),
			},value="report/kontrak"
			)
	public String reportKontrak() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "proses")){
				model.setContractHeader(rfqService.getContractHeaderById(model.getContractHeader()));
				model.setListContractItem(rfqService.listContractItemByHeader(model.getContractHeader()));
				model.setListContractComment(rfqService.listCommentByContractHeader(model.getContractHeader()));
				model.getContractHeader().getPrcMainHeader().setAdmProses(model.getContractHeader().getAdmProses());
				model.getContractHeader().getPrcMainHeader().setPpmProsesLevel(model.getContractHeader().getContractProsesLevel());
				model.setListContractTop(rfqService.listContractTopByHeader(model.getContractHeader()));
				model.setListContractPb(rfqService.listContractPbByHeader(model.getContractHeader()));
				return "view";
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
			e.printStackTrace();
			return "ok";
		}
		model.setListOfYears(toolsService.getListOfYears());
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="report/ajax/listReportKontrakSearch.jsp")
			},value="ajax/report/searchKontrak"
			)
	public String searchKontrak() throws Exception{
		try {
			model.setListContractHeader(rptService.searchKontrakHeader(model.getContractHeader(), model.getYear()));
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="report/ajax/listReportKontrak.jsp")
			},value="ajax/report/kontrak"
			)
	public String ajaxReportKontrak() throws Exception{
		try {
			model.setResultSize(rptService.countContractHeader(model.getContractHeader()));
			model.setListContractHeader(rptService.listCountContractHeaderReport(model.getContractHeader(),model.getPage(request),10));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	
	/** procurement value **/
	@Action(
			results={
					@Result(name=SUCCESS,location="report/reportProcurementValue.jsp")
			},value="report/procurementValue"
			)
	public String procurementValue() throws Exception{
		model.setListProcurementValue(rptService.getListProcurementValue(QueryUtil.procurementValue(), model.getStartDate(), model.getEndDate()));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="report/ajax/reportProcurementValueDetail.jsp")
			},value="ajax/report/procurementValueDetail"
			)
	public String procurementValueDetail() throws Exception{
		try {
			model.setListProcurementValue(rptService.listProcurementValueDetail(QueryUtil.procurementValueDetail(model.getMetode())));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	
	/** proses pengadaan **/
	@Action(
			results={
					@Result(name=SUCCESS,location="report/reportProsesPengadaan.jsp")
			},value="report/prosesPengadaan",
			interceptorRefs={
					@InterceptorRef("defaultInterceptorStack")
			}
			)
	public String prosesPengadaan() throws Exception{
		model.setListProcurementProses(rptService.getListProcurementProses(QueryUtil.queryProsesPengadaan(null)));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="report/ajax/reportProsesPengadaanDetail.jsp")
			},value="ajax/report/prosesPengadaanDetail",
			interceptorRefs={
					@InterceptorRef("defaultInterceptorStack")
			}
			)
	public String prosesPengadaanDetail() throws Exception{
		try {
			model.setListPrcMainHeader(rptService.getListPengadaanByProses(model.getProsesId(),model.getProsesLevel()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	/** Data sheet Pengadaan **/
	
	@Action(
			results={
					@Result(name=SUCCESS,location="report/reportDataSheet.jsp")
			},value="report/dataSheetPengadaan"
			)
	public String dataSheetPengadaan() throws Exception{
		try {
			
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="report/ajax//reportDataSheet.jsp")
			},value="ajax/report/dataSheetPengadaan",
			interceptorRefs={
					@InterceptorRef("defaultInterceptorStack")
			}
			)
	public String ajaxdataSheetPengadaan() throws Exception{
		try {
			model.setListDataSheetPengadaan(rptService.getDataSheetPengadaan(QueryUtil.queryDataSheetPengadaan(),model.getStartDate(),model.getEndDate()));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	
	@Action(
			results={
					@Result(name=SUCCESS,location="report/reportRekapPembelian.jsp")
			},value="report/rekapPembelian"
			)
	public String rekapPembelian() throws Exception{
		try {
			
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="report/ajax/reportRekapPembelian.jsp")
			},value="ajax/report/rekapPembelian",
			interceptorRefs={
					@InterceptorRef("defaultInterceptorStack")
			}
			)
	public String ajaxRekapPembelian() throws Exception{
		try {
			model.setListRekapPembelian(rptService.getRekapPembelian(QueryUtil.rekapPembelian(),model.getStartDate(),model.getEndDate()));
		} catch (Exception e) {
			addActionError(e.getMessage());
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	
	/** tools action **/
	@Action(
			results={
					@Result(name=SUCCESS,location="report/listMataAnggaran.jsp")
			},value="rpt/updateMataAnggaran"
			)
	public String editMataAnggaran() throws Exception{
		try {
//			model.setListAnggaranDetail(rptService.getListAnggaranDetail(null));
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	
	@Action(
			results={
					@Result(name=SUCCESS,location="report/ajax/listMataAnggaran.jsp")
			},value="ajax/rpt/updateMataAnggaran"
			)
	public String ajaxeditMataAnggaran() throws Exception{
		try {
			model.setListAnggaranDetail(rptService.getListAnggaranDetail(model.getAnggaranJdeDetail()));
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	
	@Action(
			results={
					@Result(name=SUCCESS,location="report/formMataAnggaran.jsp"),
					@Result(name="ok",location="ajax/rpt/updateMataAnggaran",type="redirectAction")
			},value="ajax/rpt/crudUpdateMataAnggaran"
			)
	public String crudMataAnggaran() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "save")){
				rptService.updateMataAnggaran(model.getAnggaranJdeDetail());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			}else{
				if(model.getAnggaranJdeDetail()!=null && model.getAnggaranJdeDetail().getId()!=null){
					if(model.getxCommand()!=null && model.getxCommand().equalsIgnoreCase("edit")){
						model.setAnggaranJdeDetail(new RptServiceImpl().getAnggaranJdeDetailById(model.getAnggaranJdeDetail()));
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	/**
	 * Report Anggaran
	 */
	@Action(
			results={
					@Result(name=SUCCESS,location="report/reportAnggaran.jsp")
			},value="report/reportAnggaran"
			)
	public String reportAction() throws Exception{
		try {
			System.out.println(">>> PENCARIAN");
			if(WebUtils.hasSubmitParameter(request, "find")){
				model.setListJdeMasterMain(rfqService.listJdeMasterAnggaran(model.getTahunAnggaran(),model.getKodeAnggaran()));
			}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	/***
	 * 
	 * Update Tools
	 * 
	 */
	
	@Action(
			results={
					@Result(name=SUCCESS,location="report/reportPermintaanUpdate.jsp"),
					@Result(name="view",location="report/viewReportPermintaanUpdate.jsp"),
					@Result(name="ok",location="report/permintaanUpdate", type="redirectAction"),
					@Result(name=INPUT,location="report/reportPermintaanUpdate.jsp"),
			},value="report/permintaanUpdate"
			)
	public String updateTools() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "proses")){
				PrcMainHeader prcMainHeader = prcService.getPrcMainHeaderSPPHById(model.getPrcMainHeader().getPpmId());
				AdmEmployee admEmployee = SessionGetter.getUser().getAdmEmployee();
				model.setPrcMainHeader(prcMainHeader);
				model.setAdmEmployee(admEmployee);
				model.setListHistMainComment(prcService.listHistMainCommentByPrcHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId())));
				List<PrcMainItem> listItem = prcService.listPrcMainItemByHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId()));
				model.setListPrcMainItem(listItem);
				model.setListVendor(prcService.listMitraKerjaByHeader(prcMainHeader));
				model.setPrcMainPreparation(prcService.getPrcMainPreparationByHeader(prcMainHeader));
				model.setListTabulasi(rfqService.getListTabulasi(QueryUtil.tabulasiEvaluasi(prcMainHeader.getPpmId())));
				model.setListNegosiasi(rfqService.listNegosiasiByMainHeader(model.getPrcMainHeader()));
				model.setListPrcMainDoc(prcService.listPrcMainDocByHeader(prcMainHeader));
				model.setListUser(admService.getListUser(null));
				if(model.getPrcMainHeader().getPanitia()!=null){
					model.setPanitia(prcService.getPanitiaPengadaan(model.getPrcMainHeader().getPanitia()));
					model.setListPanitiaDetail(prcService.getListPanitiaPengadaanDetail(model.getPanitia()));
				}
				model.setListEstimator(prcService.listEstimator(null));
				model.setListHargaNegosiasi(rfqService.listHargaNegosiasi(prcMainHeader));
				return "view";
			}else if(WebUtils.hasSubmitParameter(request, "saveAndFinish")){
				System.out.println(">>>> UPDATEEE >>>");
				if(model.getDocsKomentar()!=null){
					String docName = UploadUtil.uploadDocsFile(model.getDocsKomentar(), model.getDocsKomentarFileName());
					model.getHistMainComment().setDocument(docName);
				}
				prcService.updateSpph(model.getPrcMainHeader(),model.getHistMainComment(),model.getDocsKomentar(),model.getDocsKomentarFileName(),model.getListPanitiaDetail(),model.getPpdDocument(),model.getPpdDocumentFileName(),model.getPpdCategory(),model.getPpdDescription(),model.getListPrcMainDoc());
				addActionMessage("Data Sudah Di Simpan");
				return "ok";
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
			e.printStackTrace();
			return "ok";
		}
		return SUCCESS;
	}
	
	
	
	@Action(
			results={
					@Result(name=SUCCESS,location="report/ajax/reportPermintaanUpdate.jsp")
			},value="ajax/report/searchPengadaanUpdate"
			)
	public String searchPrUpdate() throws Exception{
		try {
			model.setListPrcMainHeader(toolsService.searchPrcMainHeaderAll(model.getPrcMainHeader(), model.getYear()));
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="proc/tmp/panitiaUpdate.jsp")
			},value="ajax/report/panitiaHasilUpdate"
			)
	public String panitiaHasilUpdate() throws Exception{
		try {
			model.setPanitia(prcService.getPanitiaPengadaan(model.getPanitia()));
			model.setListPanitiaDetail(prcService.getListPanitiaPengadaanDetail(model.getPanitia()));
			model.setListUser(admService.getListUser(null));
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="proc/tmp/panitiaFormHeader.jsp"),
					@Result(name="ok",location="ajax/report/panitiaHasilUpdate",type="redirectAction",params={"panitia.id","%{panitia.id}"})
			},value="ajax/report/panitiaHeaderUpdate"
			)
	public String panitiaHeaderUpdate() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "save")){
				model.setPanitia(prcService.savePanitiaHeader(model.getPanitia(),model.getPrcMainHeader()));
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			}else{
				if(model.getPanitia()!=null && model.getPanitia().getId()!=null && model.getPanitia().getId()!=0){
					model.setPanitia(prcService.getPanitiaPengadaan(model.getPanitia()));
				}
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="proc/tmp/panitiaFormDetail.jsp"),
					@Result(name="ok",location="ajax/report/panitiaHasilUpdate",type="redirectAction",params={"panitia.id","%{panitia.id}"})
			},value="ajax/report/panitiaDetailUpdate"
			)
	public String panitiaUpdate() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "save")){
				prcService.savePanitiaDetail(model.getPanitiaDetail());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			}
			model.setListUser(admService.getListUser(null));
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name="ok",location="ajax/report/panitiaHasilUpdate",type="redirectAction",params={"panitia.id","%{panitia.id}"})
			},value="ajax/report/panitiaDetailHapus"
			)
	public String panitiaHapusUpdate() throws Exception{
		try {
			prcService.deletePanitiaDetail(model.getPanitiaDetail());
			addActionMessage("Data Sudah Disimpan");
			return "ok";
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="report/ajax/reportPermintaanUpdate.jsp")
			},value="ajax/report/permintaanUpdate"
			)
	public String ajaxReportPrUpdate() throws Exception{
		try {
			model.setResultSize(prcService.countPrcMainHeaderPRSPPHReport(null));
			model.setListPrcMainHeader(prcService.listPrcMainHeaderPRSPPHReport(null,model.getPage(request),10));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="report/sapApiTest.jsp"),
					@Result(name="user", location="report/sapApiTest.jsp"),
					@Result(name="view",location="report/sapApiTest.jsp"),
					@Result(name="ok",location="report/sapApiTest", type="redirectAction"),
			},value="report/sapApiTest"
			)
	public String sapAPI() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "view")){
				System.out.println("saya di report");
//				IntegrationService integrationService = new IntegrationServiceImpl();
//				System.out.println("saya di dalem...");
				ResponseEntity<String> res = new IntegrationServiceImpl().generateToken();
				Map<String, String> sap = new HashMap<String, String>();
				sap.put("x-csrf-token", res.getHeaders().get("x-csrf-token").get(0));
				String[] val = res.getHeaders().get("set-cookie").get(1).split(";")[0].split("=");
				sap.put(val[0], val[1]);
				System.out.println("saya keluar...");

				for (Map.Entry<String, String> entry : sap.entrySet())
				{
					addActionMessage(entry.getKey() + "/" + entry.getValue());
				    System.out.println(entry.getKey() + "/" + entry.getValue());
				}
			}else if(WebUtils.hasSubmitParameter(request, "view2")){
				System.out.println("saya di report");
//				IntegrationService integrationService = new IntegrationServiceImpl();
//				System.out.println("saya di dalem...");
				Map<String, String> sap = new IntegrationServiceImpl().generateTokenAndCookies();
				
				System.out.println("saya keluar...");

				for (Map.Entry<String, String> entry : sap.entrySet())
				{
					addActionMessage(entry.getKey() + "/" + entry.getValue());
				    System.out.println(entry.getKey() + "/" + entry.getValue());
				}
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
			e.printStackTrace();
			return "ok";
		}
		return SUCCESS;
	}
	
	
	
}
