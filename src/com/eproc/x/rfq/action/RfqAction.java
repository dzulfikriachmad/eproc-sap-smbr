package com.eproc.x.rfq.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.WebUtils;

import com.eproc.core.BaseController;
import com.eproc.sap.controller.IntegrationService;
import com.eproc.sap.controller.IntegrationServiceImpl;
import com.eproc.sap.dto.PoHeaderDto;
import com.eproc.sap.dto.ResultDto;
import com.eproc.utils.QueryUtil;
import com.eproc.utils.SessionGetter;
import com.eproc.x.rfq.command.RfqCommand;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.jde.model.POHeader;
import com.orm.model.AdmCurrency;
import com.orm.model.AdmEmployee;
import com.orm.model.CtrContractHeader;
import com.orm.model.CtrContractPb;
import com.orm.model.CtrContractTop;
import com.orm.model.PrcMainHeader;
import com.orm.model.PrcMainItem;
import com.orm.model.PrcMainVendorQuote;

import eproc.controller.AdmServiceImpl;
import eproc.controller.PrcService;
import eproc.controller.PrcServiceImpl;
import eproc.controller.RfqService;
import eproc.controller.RfqServiceImpl;
import eproc.controller.RptService;
import eproc.controller.RptServiceImpl;


@SuppressWarnings("serial")
@Namespace("/")
@ParentPackage("eproc")
public class RfqAction extends BaseController {

	private RfqCommand model;
	private PrcService prcService;
	private RfqService rfqService;
	private RptService rptService;
	private IntegrationService integrationService;
	
	@Override
	public Object getModel() {
		return this.model;
	}
	
	@Override
	public void prepare() throws Exception {
		model = new RfqCommand();
		prcService = new PrcServiceImpl();
		rfqService = new RfqServiceImpl();
		rptService = new RptServiceImpl();
		integrationService = new IntegrationServiceImpl();
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="proc/rfq/listUndangVendor.jsp"),
					@Result(name="view",location="proc/rfq/viewUndangVendor.jsp"),
//					@Result(name="ok",location="rfq/undangVendor",type="redirectAction")
					@Result(name = "ok", location = "dashboard", type="redirectAction")
			},value="rfq/undangVendor"
			)
	public String undangVendor() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "proses")){
				PrcMainHeader prcMainHeader = prcService.getPrcMainHeaderSPPHById(model.getPrcMainHeader().getPpmId());
				AdmEmployee admEmployee = SessionGetter.getUser().getAdmEmployee();
				model.setPrcMainHeader(prcMainHeader);
				model.setAdmEmployee(admEmployee);
				model.setListHistMainComment(prcService.listHistMainCommentByPrcHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId())));
				List<PrcMainItem> listItem = prcService.listPrcMainItemByHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId()));
				model.setListPrcMainItem(listItem);
				if(prcMainHeader.getPrcMethod().getId()>3){
					model.setListVendor(prcService.listMitraKerjaByStatusPq(prcMainHeader));
				} else {
					model.setListVendor(prcService.listMitraKerjaByHeader(prcMainHeader));
				}
				
				model.setListPrcMainDoc(prcService.listPrcMainDocByHeader(prcMainHeader));
				model.setPrcMainPreparation(prcService.getPrcMainPreparationByHeader(prcMainHeader));
				if(prcMainHeader.getPanitia()!=null){
					model.setPanitia(prcService.getPanitiaPengadaan(prcMainHeader.getPanitia()));
					model.setListPanitiaDetail(prcService.getListPanitiaPengadaanDetail(prcMainHeader.getPanitia()));
				}
				return "view";
			}else if(WebUtils.hasSubmitParameter(request, "saveAndFinish")){
				model.getHistMainComment().setAksi("Undang Vendor");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("rfq.1", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("rfq.2", null));
				rfqService.saveUndangVendor(model.getPrcMainHeader(),model.getHistMainComment(),model.getDocsKomentar(),model.getDocsKomentarFileName(),model.getListVendor());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
			return "ok";
		}
		model.setListPrcMainHeader(rfqService.listPrcMainHeaderUndangVendor(null));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="proc/rfq/listAanwijzing.jsp"),
					@Result(name="view",location="proc/rfq/viewAanwijzing.jsp"),
//					@Result(name="ok",location="rfq/aanwijzing",type="redirectAction")
					@Result(name = "ok", location = "dashboard", type="redirectAction")
			},value="rfq/aanwijzing"
			)
	public String aanwijzing() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "proses")){
				PrcMainHeader prcMainHeader = prcService.getPrcMainHeaderSPPHById(model.getPrcMainHeader().getPpmId());
				AdmEmployee admEmployee = SessionGetter.getUser().getAdmEmployee();
				model.setPrcMainHeader(prcMainHeader);
				model.setAdmEmployee(admEmployee);
				model.setListHistMainComment(prcService.listHistMainCommentByPrcHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId())));
				List<PrcMainItem> listItem = prcService.listPrcMainItemByHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId()));
				model.setListPrcMainItem(listItem);
				model.setListVendor(prcService.listMitraKerjaTerdaftar(prcMainHeader));
				model.setListPrcMainDoc(prcService.listPrcMainDocByHeader(prcMainHeader));
				model.setPrcMainPreparation(prcService.getPrcMainPreparationByHeader(prcMainHeader));
				if(model.getPrcMainPreparation().getAanwijzingDate().after(new Date())){
					addActionError("Belum Waktunya Untuk Aanwijzing");
					return "ok";
				}else
					return "view";
			}else if(WebUtils.hasSubmitParameter(request, "saveAndFinish")){
				model.getHistMainComment().setAksi("Aanwijzing");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("rfq.2", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("rfq.3", null));
				rfqService.saveAanwijzing(model.getPrcMainHeader(),model.getHistMainComment(),model.getDocsKomentar(),model.getDocsKomentarFileName(),model.getListVendor());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			}else if(WebUtils.hasSubmitParameter(request, "rebid")){
				model.getHistMainComment().setAksi("Rebid");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("rfq.-98", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("rfq.-98", null));
				rfqService.rebidPengadaan(model.getPrcMainHeader(),model.getHistMainComment(),model.getDocsKomentar(),model.getDocsKomentarFileName(),model.getListVendor());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
			return "ok";
		}
		model.setListPrcMainHeader(rfqService.listPrcMainHeaderAanwijzing(null));
		return SUCCESS;
	}
	
	
	@Action(
			results={
					@Result(name=SUCCESS,location="proc/rfq/listOpening.jsp"),
					@Result(name="view",location="proc/rfq/viewOpening.jsp"),
//					@Result(name="ok",location="rfq/opening",type="redirectAction")
					@Result(name = "ok", location = "dashboard", type="redirectAction")
			},value="rfq/opening"
			)
	public String opening() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "proses")){
				PrcMainHeader prcMainHeader = prcService.getPrcMainHeaderSPPHById(model.getPrcMainHeader().getPpmId());
				AdmEmployee admEmployee = SessionGetter.getUser().getAdmEmployee();
				model.setPrcMainHeader(prcMainHeader);
				model.setAdmEmployee(admEmployee);
				model.setListHistMainComment(prcService.listHistMainCommentByPrcHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId())));
				List<PrcMainItem> listItem = prcService.listPrcMainItemByHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId()));
				model.setListPrcMainItem(listItem);
				if(prcMainHeader.getPrcMethod().getId()>3){
					if(prcMainHeader.getPrcMethod().getId()==6){
						if(SessionGetter.isNotNull(prcMainHeader.getStatusEvaluasi())){
							model.setListVendor(prcService.listMitraKerjaByStatusTeknis(prcMainHeader));
						}else{
							model.setListVendor(prcService.listMitraKerjaByStatusPq(prcMainHeader));
						}
					} else{
						model.setListVendor(prcService.listMitraKerjaByStatusPq(prcMainHeader));
					}
				} else {
					model.setListVendor(prcService.listMitraKerjaByHeader(prcMainHeader));
				}
				model.setListPrcMainDoc(prcService.listPrcMainDocByHeader(prcMainHeader));
				model.setPrcMainPreparation(prcService.getPrcMainPreparationByHeader(prcMainHeader));
				if(prcMainHeader.getPanitia()!=null){
					model.setPanitia(prcService.getPanitiaPengadaan(prcMainHeader.getPanitia()));
					model.setListPanitiaDetail(prcService.getListPanitiaPengadaanDetail(prcMainHeader.getPanitia()));
				}
				if(model.getPrcMainPreparation().getQuotationOpening().after(new Date())){
					addActionError("Belum Waktunya Bid Opening");
					return "ok";
				}else if(SessionGetter.isNotNull(model.getPrcMainPreparation().getQuotationOpening2())){
					if(model.getPrcMainPreparation().getQuotationOpening2().after(new Date())){
						addActionError("Belum Waktunya Bid Opening");
						return "ok";
					}else{
						return "view";
					}
				}else{
					return "view";
				}
			}else if(WebUtils.hasSubmitParameter(request, "saveAndFinish")){
				model.getHistMainComment().setAksi("Bid Opening");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("rfq.3", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("rfq.4", null));
				rfqService.saveOpening(model.getPrcMainHeader(),model.getHistMainComment(),model.getDocsKomentar(),model.getDocsKomentarFileName());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			}else if(WebUtils.hasSubmitParameter(request, "rebid")){
				model.getHistMainComment().setAksi("Rebid");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("rfq.-98", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("rfq.-98", null));
				rfqService.rebidPengadaan(model.getPrcMainHeader(),model.getHistMainComment(),model.getDocsKomentar(),model.getDocsKomentarFileName(),model.getListVendor());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
			return "ok";
		}
		model.setListPrcMainHeader(rfqService.listPrcMainHeaderOpening(null));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="proc/rfq/ajax/listMitraKerjaOpening.jsp")
			},value="ajax/rfq/openingVendor"
			)
	public String ajaxOpening() throws Exception{
		try {
			PrcMainHeader prcMainHeader = prcService.getPrcMainHeaderSPPHById(model.getPrcMainHeader().getPpmId());
			if(prcMainHeader.getPrcMethod().getId()>3){
				if(prcMainHeader.getPrcMethod().getId()==6){
					if(SessionGetter.isNotNull(prcMainHeader.getStatusEvaluasi())){
						model.setListVendor(prcService.listMitraKerjaByStatusTeknis(prcMainHeader));
					}else{
						model.setListVendor(prcService.listMitraKerjaByStatusPq(prcMainHeader));
					}
				} else{
					model.setListVendor(prcService.listMitraKerjaByStatusPq(prcMainHeader));
				}
			} else {
				model.setListVendor(prcService.listMitraKerjaByHeader(prcMainHeader));
			}
		} catch (Exception e) {

		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=INPUT,location="proc/rfq/ajax/formOpening.jsp"),
					@Result(name=SUCCESS,location="ajax/rfq/openingVendor",type="redirectAction")
			},value="ajax/rfq/crudOpening"
			)
	public String crudOpening() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "lulus")){
				model.getPrcMainVendor().setPmpStatus(5);
				model.getPrcMainVendor().setPmpAdmStatus(1);
				rfqService.saveVerifikasiAdm(model.getQuote(),model.getListAdministrasi(),model.getPrcMainVendor());
				addActionMessage(getText("notif.save"));
				return SUCCESS;
			}else if(WebUtils.hasSubmitParameter(request, "tidakLulus")){
				model.getPrcMainVendor().setPmpStatus(-5);
				model.getPrcMainVendor().setPmpAdmStatus(-1);
				rfqService.saveVerifikasiAdm(model.getQuote(),model.getListAdministrasi(),model.getPrcMainVendor());
				addActionMessage(getText("notif.save"));
				return SUCCESS;
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		model.setPrcMainVendor(rfqService.getPrcMainVendorById(model.getPrcMainVendor().getId()));
		model.setListAdministrasi(prcService.listItemAdministrasi(model.getQuote(), model.getPrcMainHeader()));
		return INPUT;
	}
	
	
	@Action(
			results={
					@Result(name=SUCCESS,location="proc/rfq/listEvaluasi.jsp"),
					@Result(name="view",location="proc/rfq/viewEvaluasi.jsp"),
					@Result(name="eval1",location="proc/rfq/viewEvaluasiSampul1.jsp"),
					@Result(name="eval2",location="proc/rfq/viewEvaluasiSampul2.jsp"),
//					@Result(name="ok",location="rfq/evaluasi",type="redirectAction")
					@Result(name = "ok", location = "dashboard", type="redirectAction")
			},value="rfq/evaluasi"
			)
	public String evaluation() throws Exception{
		try {
//			List <PrcMainVendor> lulusTmp = new ArrayList<PrcMainVendor>();
//			List <PrcMainVendor> tidakLulusTmp = new ArrayList<PrcMainVendor>();
			if(WebUtils.hasSubmitParameter(request, "proses")){
				PrcMainHeader prcMainHeader = prcService.getPrcMainHeaderSPPHById(model.getPrcMainHeader().getPpmId());
				AdmEmployee admEmployee = SessionGetter.getUser().getAdmEmployee();
				model.setPrcMainHeader(prcMainHeader);
				model.setAdmEmployee(admEmployee);
				model.setListHistMainComment(prcService.listHistMainCommentByPrcHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId())));
				List<PrcMainItem> listItem = prcService.listPrcMainItemByHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId()));
				model.setListPrcMainItem(listItem);
				model.setListTabulasi(rfqService.getListTabulasi(QueryUtil.tabulasiEvaluasi(prcMainHeader.getPpmId())));
				model.setPrcMainPreparation(prcService.getPrcMainPreparationByHeader(prcMainHeader));
				if(prcMainHeader.getPrcMethod().getId()== 3 || //PL 2 sampul
						prcMainHeader.getPrcMethod().getId()==5){ //LL 2 sampul
					if(!SessionGetter.isNotNull(prcMainHeader.getStatusEvaluasi())){	
						return "eval1";
					}else if(prcMainHeader.getStatusEvaluasi()==1){//selesai evaluasi sampul 1
						return "eval2";
					}
				}else if(prcMainHeader.getPrcMethod().getId() == 6){
					//LL 2 tahap
					if(!SessionGetter.isNotNull(prcMainHeader.getStatusEvaluasi())){
						
						return "eval1";
					}else if(prcMainHeader.getStatusEvaluasi()==1){//selesai evaluasi sampul 1
						return "eval2";
					}
				}
				if(prcMainHeader.getPanitia()!=null){
					model.setPanitia(prcService.getPanitiaPengadaan(prcMainHeader.getPanitia()));
					model.setListPanitiaDetail(prcService.getListPanitiaPengadaanDetail(prcMainHeader.getPanitia()));
				}
				return "view";
			}else if(WebUtils.hasSubmitParameter(request, "saveAndFinish")){
				model.getHistMainComment().setAksi("Evaluasi Penawaran");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("rfq.4", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("rfq.5", null));
				rfqService.saveEvaluasi(model.getPrcMainHeader(),model.getHistMainComment(),model.getDocsKomentar(),model.getDocsKomentarFileName());
				addActionMessage(getText("notif.save"));
				return "ok";
			}else if(WebUtils.hasSubmitParameter(request, "sampul1")){
				PrcMainHeader prcMainHeader = prcService.getPrcMainHeaderSPPHById(model.getPrcMainHeader().getPpmId());
				model.getHistMainComment().setAksi("Evaluasi Penawaran Sampul 1");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("rfq.4", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("rfq.4", null));
				model.setListVendorLulus(prcService.listMitraKerjaEvaluasiLulusTeknis(prcMainHeader));
				model.setListVendorTidakLulus(prcService.listMitraKerjaEvaluasiTidakLulusTeknis(prcMainHeader));
				if(prcMainHeader.getPrcMethod().getId()==6){
					model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("rfq.3", null));
				}
				rfqService.saveEvaluasiSampul1(model.getPrcMainHeader(),model.getHistMainComment(),model.getDocsKomentar(),model.getDocsKomentarFileName(), model.getPrcMainPreparation(), model.getListVendorLulus(), model.getListVendorTidakLulus());
				
				addActionMessage(getText("notif.save"));
				return "ok";
			}else if(WebUtils.hasSubmitParameter(request, "sampul2")){
				PrcMainHeader prcMainHeader = prcService.getPrcMainHeaderSPPHById(model.getPrcMainHeader().getPpmId());
				model.getHistMainComment().setAksi("Evaluasi Penawaran Sampul 2");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("rfq.4", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("rfq.5", null));
				model.setListVendorLulus(prcService.listMitraKerjaEvaluasiLulusHarga(prcMainHeader));
				model.setListVendorTidakLulus(prcService.listMitraKerjaEvaluasiTidakLulusHarga(prcMainHeader));
				rfqService.saveEvaluasiSampul2(model.getPrcMainHeader(),model.getHistMainComment(),model.getDocsKomentar(),model.getDocsKomentarFileName(), model.getListVendorLulus(), model.getListVendorTidakLulus());
				addActionMessage(getText("notif.save"));
				return "ok";
			}
			else if(WebUtils.hasSubmitParameter(request, "rebid")){
				model.getHistMainComment().setAksi("Rebid");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("rfq.-98", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("rfq.-98", null));
				rfqService.rebidPengadaan(model.getPrcMainHeader(),model.getHistMainComment(),model.getDocsKomentar(),model.getDocsKomentarFileName(),model.getListVendor());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			}else if(WebUtils.hasSubmitParameter(request, "revisisatu")){
				model.getHistMainComment().setAksi("Revisi");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("rfq.4", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("rfq.3", null));
				rfqService.revisiPengadaan(model.getPrcMainHeader(),model.getHistMainComment(),model.getDocsKomentar(),model.getDocsKomentarFileName(),model.getListVendor());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
			return "ok";
		}
		model.setListPrcMainHeader(rfqService.listPrcMainHeaderEvaluasi(null));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="proc/rfq/ajax/listTabulasi.jsp"),
					@Result(name="tahap1",location="proc/rfq/ajax/listTabulasiSampul1.jsp")
			},value="ajax/rfq/tabulasi"
			)
	public String tabulasi() throws Exception{
		try {
			PrcMainHeader hh = rfqService.getPrcMainHeaderSPPHById(model.getPrcMainHeader().getPpmId());	
			model.setListTabulasi(rfqService.getListTabulasi(QueryUtil.tabulasiEvaluasi(model.getPrcMainHeader().getPpmId())));
			if((hh.getPrcMethod().getId()==3 || hh.getPrcMethod().getId()==5 || hh.getPrcMethod().getId()==6) && (hh.getStatusEvaluasi() == null || hh.getStatusEvaluasi()==0)){//3 = PL 2 sampul, 5=LL 2 tahap
				return "tahap1";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	@Action(
			results={
					@Result(name = SUCCESS, type="json")
			}, value = "validateTabulasi",
			interceptorRefs={
					@InterceptorRef("defaultInterceptorStack")
			}
	)
	public String validationTabulasi() throws Exception{
		try {
			PrcMainHeader hh = rfqService.getPrcMainHeaderSPPHById(model.getPrcMainHeader().getPpmId());	
			model.setListTabulasi(rfqService.getListTabulasi(QueryUtil.tabulasiEvaluasi(model.getPrcMainHeader().getPpmId())));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	@Action(
			results={
					@Result(name=INPUT,location="proc/rfq/ajax/formEvaluasiTeknis.jsp"),
					@Result(name=SUCCESS,location="ajax/rfq/tabulasi", type="redirectAction", params={"prcMainHeader.ppmId","%{prcMainVendor.id.ppmId}"})
			},value="ajax/rfq/crudEvaluasi"
			)
	public String evaluasiTeknis() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "save")){
				rfqService.saveVerifikasiTeknis(model.getQuote(),model.getListTeknis(),model.getPrcMainVendor());
				addActionMessage(getText("notif.save"));
				return SUCCESS;
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
			return SUCCESS;
		}
		model.setPrcTemplate(new AdmServiceImpl().getTemplateById(model.getPrcTemplate()));
		model.setPrcMainVendor(rfqService.getPrcMainVendorById(model.getPrcMainVendor().getId()));
		model.setListTeknis(prcService.listItemTeknis(model.getQuote(), model.getPrcMainHeader()));
		return INPUT;
	}
	
	@Action(
			results={
					@Result(name=INPUT,location="proc/rfq/ajax/viewEvaluasiTeknis.jsp")
			},value="ajax/rfq/viewEvaluasi"
			)
	public String ViewevaluasiTeknis() throws Exception{
		model.setPrcTemplate(new AdmServiceImpl().getTemplateById(model.getPrcTemplate()));
		model.setPrcMainVendor(rfqService.getPrcMainVendorById(model.getPrcMainVendor().getId()));
		model.setListTeknis(prcService.listItemTeknis(model.getQuote(), model.getPrcMainHeader()));
		return INPUT;
	}
	
	@Action(
			results={
					@Result(name=INPUT,location="proc/rfq/ajax/formEvaluasiHarga.jsp"),
					@Result(name=SUCCESS,location="ajax/rfq/tabulasi", type="redirectAction")
			},value="ajax/rfq/crudEvaluasiHarga"
			)
	public String evaluasiHarga() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "save")){
				rfqService.saveVerifikasiHarga(model.getListPrcMainVendor());
				addActionMessage(getText("notif.save"));
				return SUCCESS;
			}
			model.setPrcTemplate(new AdmServiceImpl().getTemplateById(model.getPrcTemplate()));
			model.setListPrcMainVendor(prcService.listMitraKerjaEvaluasiLulusTeknisHarga(model.getPrcMainHeader()));
			model.setListQuoteItem(prcService.listQuoteItemEvaluasi(model.getListPrcMainVendor()));
			model.setListPrcMainItem(prcService.listPrcMainItemByHeader(model.getPrcMainHeader()));
			model.setListQuote(prcService.getListMainVendorQuoteByPpmId(model.getPrcMainHeader()));
			model.setHargaTerendah(kalkulasiHargaTerendah(model.getPrcMainHeader(),model.getListQuote()));
		} catch (Exception e) {
			addActionError(e.getMessage());
			e.printStackTrace();
			return SUCCESS;
		}
		return INPUT;
	}
	
	private Double kalkulasiHargaTerendah(PrcMainHeader prcMainHeader,
			List<PrcMainVendorQuote> listQuote) {
		Double hargaTerendah = 0.0;
		if(listQuote!=null){
			int i= 0 ;
			for(PrcMainVendorQuote q:listQuote){
				if(i>0){
					if(q.getPqmTotalPenawaran()!=null){
						if(q.getPqmTotalPenawaran().doubleValue()< hargaTerendah){
							hargaTerendah = q.getPqmTotalPenawaran().doubleValue();
						}
					}
				}else{
					if(q.getPqmTotalPenawaran()!=null){
						hargaTerendah = q.getPqmTotalPenawaran().doubleValue();
					}
				}
				i++;
			}
		}
		return hargaTerendah;
	}
	
	@Action(
			results={
					@Result(name=INPUT,location="proc/rfq/ajax/viewEvaluasiHarga.jsp")
			},value="ajax/rfq/viewEvaluasiHarga"
			)
	public String ViewevaluasiHarga() throws Exception{
		model.setPrcTemplate(new AdmServiceImpl().getTemplateById(model.getPrcTemplate()));
		model.setListPrcMainVendor(prcService.listMitraKerjaEvaluasi(model.getPrcMainHeader()));
		model.setListQuoteItem(prcService.listQuoteItemEvaluasi(model.getListPrcMainVendor()));
		model.setListPrcMainItem(prcService.listPrcMainItemByHeader(model.getPrcMainHeader()));
		model.setListQuote(prcService.getListMainVendorQuoteByPpmId(model.getPrcMainHeader()));
		return INPUT;
	}
	
	
	/** Negosiasi **/
	@Action(
			results={
					@Result(name=SUCCESS,location="proc/rfq/listNegosiasi.jsp"),
					@Result(name="view",location="proc/rfq/viewNegosiasi.jsp"),
//					@Result(name="ok",location="rfq/negosiasi", type="redirectAction")
					@Result(name = "ok", location = "dashboard", type="redirectAction")
			},value="rfq/negosiasi"
			)
	public String negosiasi() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "proses")){
				PrcMainHeader prcMainHeader = prcService.getPrcMainHeaderSPPHById(model.getPrcMainHeader().getPpmId());
				AdmEmployee admEmployee = SessionGetter.getUser().getAdmEmployee();
				model.setPrcMainHeader(prcMainHeader);
				model.setAdmEmployee(admEmployee);
				model.setListHistMainComment(prcService.listHistMainCommentByPrcHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId())));
				List<PrcMainItem> listItem = prcService.listPrcMainItemByHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId()));
				model.setListPrcMainItem(listItem);
				model.setPrcMainPreparation(prcService.getPrcMainPreparationByHeader(prcMainHeader));
				model.setListTabulasi(rfqService.getListTabulasi(QueryUtil.tabulasiEvaluasi(prcMainHeader.getPpmId())));
				model.setListPrcMainVendor(prcService.listMitraKerjaNegosiasi(model.getPrcMainHeader()));
				model.setListNegosiasi(rfqService.listNegosiasiByMainHeader(model.getPrcMainHeader()));

				if(prcMainHeader.getPanitia()!=null){
					model.setPanitia(prcService.getPanitiaPengadaan(prcMainHeader.getPanitia()));
					model.setListPanitiaDetail(prcService.getListPanitiaPengadaanDetail(prcMainHeader.getPanitia()));
				}
				model.setListHargaNegosiasi(rfqService.listHargaNegosiasi(model.getPrcMainHeader()));
				return "view";
			}else if(WebUtils.hasSubmitParameter(request, "saveAndFinish")){
				model.getHistMainComment().setAksi("Negosiasi");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("rfq.5", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("rfq.6", null));
				rfqService.saveNegosiasi(model.getPrcMainHeader(),model.getHistMainComment(),model.getDocsKomentar(),model.getDocsKomentarFileName());
				addActionMessage(getText("notif.save"));
				return "ok";
			}else if(WebUtils.hasSubmitParameter(request, "rebid")){
				model.getHistMainComment().setAksi("Rebid");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("rfq.-98", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("rfq.-98", null));
				rfqService.rebidPengadaan(model.getPrcMainHeader(),model.getHistMainComment(),model.getDocsKomentar(),model.getDocsKomentarFileName(),model.getListVendor());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			}else if(WebUtils.hasSubmitParameter(request, "revisisatu")){
				model.getHistMainComment().setAksi("Revisi");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("rfq.5", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("rfq.4", null));
				rfqService.revisiPengadaan(model.getPrcMainHeader(),model.getHistMainComment(),model.getDocsKomentar(),model.getDocsKomentarFileName(),model.getListVendor());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
			return "ok";
		}
		model.setListPrcMainHeader(rfqService.listPrcMainHeaderNegosiasi(null));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=INPUT,location="proc/rfq/ajax/formNegosiasi.jsp"),
					@Result(name=SUCCESS,location="ajax/rfq/listNegosiasi",type="redirectAction")
			},value="ajax/rfq/crudNegosiasi"
			)
	public String formNegosiasi() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "save")){
				rfqService.saveNegosiasi(model.getPrcMainVendor(),model.getNegosiasi());
				addActionMessage(getText("notif.save"));
				return SUCCESS;
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
			return SUCCESS;
		}
		model.setPrcMainVendor(rfqService.getPrcMainVendorById(model.getPrcMainVendor().getId()));
		return INPUT;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="proc/rfq/ajax/listNegosiasi.jsp")
			},value="ajax/rfq/listNegosiasi"
			)
	public String listNegosiasi() throws Exception{
		try {
			model.setListNegosiasi(rfqService.listNegosiasiByMainHeader(model.getPrcMainHeader()));
			model.setListHargaNegosiasi(rfqService.listHargaNegosiasi(model.getPrcMainHeader()));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	
	/** Pemilihan Pemenang **/
	@Action(
			results={
					@Result(name=SUCCESS,location="proc/rfq/listUsulanPemenang.jsp"),
					@Result(name="view",location="proc/rfq/viewUsulanPemenang.jsp"),
//					@Result(name="ok",location="rfq/usulanPemenang", type="redirectAction")
					@Result(name = "ok", location = "dashboard", type="redirectAction")
			},value="rfq/usulanPemenang"
			)
	public String UsulanPemenang() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "proses")){
				PrcMainHeader prcMainHeader = prcService.getPrcMainHeaderSPPHById(model.getPrcMainHeader().getPpmId());
				AdmEmployee admEmployee = SessionGetter.getUser().getAdmEmployee();
				model.setPrcMainHeader(prcMainHeader);
				model.setAdmEmployee(admEmployee);
				model.setListHistMainComment(prcService.listHistMainCommentByPrcHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId())));
				List<PrcMainItem> listItem = prcService.listPrcMainItemByHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId()));
				model.setListPrcMainItem(listItem);
				model.setPrcMainPreparation(prcService.getPrcMainPreparationByHeader(prcMainHeader));
				model.setListTabulasi(rfqService.getListTabulasi(QueryUtil.tabulasiEvaluasi(prcMainHeader.getPpmId())));
				model.setListPrcMainVendor(rfqService.listVndUsulanPemenang(model.getPrcMainHeader(), model.getPrcMainHeader().getPpmJumlahOe().doubleValue()));
				model.setListNegosiasi(rfqService.listNegosiasiByMainHeader(model.getPrcMainHeader()));
				if(prcMainHeader.getPanitia()!=null){
					model.setPanitia(prcService.getPanitiaPengadaan(prcMainHeader.getPanitia()));
					model.setListPanitiaDetail(prcService.getListPanitiaPengadaanDetail(prcMainHeader.getPanitia()));
				}
				model.setListHargaNegosiasi(rfqService.listHargaNegosiasi(model.getPrcMainHeader()));
				return "view";
			}else if(WebUtils.hasSubmitParameter(request, "saveAndFinish")){
				model.getHistMainComment().setAksi("Usulan Pemenang");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("rfq.6", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("rfq.7", null));
				rfqService.saveUsulanPemenang(model.getPrcMainHeader(),model.getHistMainComment(),model.getDocsKomentar(),model.getDocsKomentarFileName(),model.getListPrcMainItem());
				addActionMessage(getText("notif.save"));
				return "ok";
			}else if(WebUtils.hasSubmitParameter(request, "rebid")){
				model.getHistMainComment().setAksi("Rebid");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("rfq.-98", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("rfq.-98", null));
				rfqService.rebidPengadaan(model.getPrcMainHeader(),model.getHistMainComment(),model.getDocsKomentar(),model.getDocsKomentarFileName(),model.getListVendor());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			}else if(WebUtils.hasSubmitParameter(request, "revisisatu")){
				model.getHistMainComment().setAksi("Revisi");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("rfq.6", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("rfq.5", null));
				rfqService.revisiPengadaan(model.getPrcMainHeader(),model.getHistMainComment(),model.getDocsKomentar(),model.getDocsKomentarFileName(),model.getListVendor());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
			return "ok";
		}
		model.setListPrcMainHeader(rfqService.listPrcMainHeaderUsulanPemenang(null));
		return SUCCESS;
	}
	
	/** Persetujuan Pemilihan Pemenang **/
	@Action(
			results={
					@Result(name=SUCCESS,location="proc/rfq/listPersetujuanPemenang.jsp"),
					@Result(name=INPUT,location="proc/rfq/viewPersetujuanPemenang.jsp"),
//					@Result(name="ok",location="rfq/persetujuanPemenang", type="redirectAction")
					@Result(name = "ok", location = "dashboard", type="redirectAction")
			},value="rfq/persetujuanPemenang"
			)
	public String PersetujuanPemenang() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "proses")){
				PrcMainHeader prcMainHeader = prcService.getPrcMainHeaderSPPHById(model.getPrcMainHeader().getPpmId());
				AdmEmployee admEmployee = SessionGetter.getUser().getAdmEmployee();
				model.setPrcMainHeader(prcMainHeader);
				model.setAdmEmployee(admEmployee);
				model.setListHistMainComment(prcService.listHistMainCommentByPrcHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId())));
				List<PrcMainItem> listItem = prcService.listPrcMainItemByHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId()));
				model.setListPrcMainItem(listItem);
				model.setPrcMainPreparation(prcService.getPrcMainPreparationByHeader(prcMainHeader));
				model.setListTabulasi(rfqService.getListTabulasi(QueryUtil.tabulasiEvaluasi(prcMainHeader.getPpmId())));
				model.setListPrcMainVendor(prcService.listMitraKerjaNegosiasi(model.getPrcMainHeader()));
				model.setListNegosiasi(rfqService.listNegosiasiByMainHeader(model.getPrcMainHeader()));
				if(prcMainHeader.getPanitia()!=null){
					model.setPanitia(prcService.getPanitiaPengadaan(prcMainHeader.getPanitia()));
					model.setListPanitiaDetail(prcService.getListPanitiaPengadaanDetail(prcMainHeader.getPanitia()));
				}
				model.setListHargaNegosiasi(rfqService.listHargaNegosiasi(model.getPrcMainHeader()));
				return INPUT;
			}else if(WebUtils.hasSubmitParameter(request, "saveAndFinish")){
				model.getHistMainComment().setAksi("Persetujuan Pemenang");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("rfq.7", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("rfq.7", null));
				rfqService.savePersetujuanPemenang(model.getPrcMainHeader(),model.getHistMainComment(),model.getDocsKomentar(),model.getDocsKomentarFileName());
				addActionMessage(getText("notif.save"));
				return "ok";
			}else if(WebUtils.hasSubmitParameter(request, "setujuPanitia")){
				model.getHistMainComment().setAksi("Persetujuan Pemenang");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("rfq.7", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("rfq.7", null));
				rfqService.savePersetujuanPemenangPanitia(model.getPrcMainHeader(),model.getHistMainComment(),model.getDocsKomentar(),model.getDocsKomentarFileName());
				addActionMessage(getText("notif.save"));
				return "ok";
			}
			else if(WebUtils.hasSubmitParameter(request, "revisi")){
				model.getHistMainComment().setAksi("Revisi Pemenang");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("rfq.7", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("rfq.6", null));
				rfqService.revisiPemenang(model.getPrcMainHeader(),model.getHistMainComment(),model.getDocsKomentar(),model.getDocsKomentarFileName(),model.getListPrcMainItem());
				addActionMessage(getText("notif.save"));
				return "ok";
			}else if(WebUtils.hasSubmitParameter(request, "finalisasi")){
				model.getHistMainComment().setAksi("Finalisai Pemenang");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("rfq.7", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("rfq.8", null));
				rfqService.finalisasiPemenang(model.getPrcMainHeader(),model.getHistMainComment(),model.getDocsKomentar(),model.getDocsKomentarFileName(),model.getListPrcMainItem());
				addActionMessage(getText("notif.save"));
				return "ok";
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
			e.printStackTrace();
			return "ok";
		}
		model.setListPrcMainHeader(rfqService.listPrcMainHeaderPersetujuanUsulanPemenang(null));
		return SUCCESS;
	}
	
	
	/* KONTRAK MANAGEMENT */
	
	/**
	 * Pemilihan Pembuat Kontrak
	 */
	@Action(
			results={
					@Result(name=SUCCESS,location="proc/rfq/listPemilihanPembuatKontrak.jsp"),
					@Result(name="view",location="proc/rfq/viewPemilihanPembuatKontrak.jsp"),
//					@Result(name="ok",location="kontrak/pembuatKontrak", type="redirectAction")
					@Result(name = "ok", location = "dashboard", type="redirectAction")
			},value="kontrak/pembuatKontrak"
			)
	public String pembuatKontrak() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "proses")){
				model.setContractHeader(rfqService.getContractHeaderById(model.getContractHeader()));
				model.setListContractItem(rfqService.listContractItemByHeader(model.getContractHeader()));
				model.setListContractComment(rfqService.listCommentByContractHeader(model.getContractHeader()));
				model.getContractHeader().getPrcMainHeader().setAdmProses(model.getContractHeader().getAdmProses());
				model.getContractHeader().getPrcMainHeader().setPpmProsesLevel(model.getContractHeader().getContractProsesLevel());
				model.setListUser(prcService.listUserByProsesLevel(model.getContractHeader().getPrcMainHeader()));
				return "view";
			}else if(WebUtils.hasSubmitParameter(request, "saveAndFinish")){
				model.getCtrContractComment().setAksi("Pemilihan Pembuat Kontrak");
				model.getCtrContractComment().setProsesAksi(SessionGetter.getPropertiesValue("ctr.1", null));
				model.getCtrContractComment().setCreatedBy(SessionGetter.getPropertiesValue("ctr.2", null));
				rfqService.savePemilihanPembuatKontrak(model.getContractHeader(),model.getCtrContractComment(),model.getDocsKomentar(),model.getDocsKomentarFileName());
				addActionMessage(getText("notif.save"));
				return "ok";
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
			e.printStackTrace();
			return "ok";
		}
		model.setListContractHeader(rfqService.listContractPemilihanPembuatKontrak(null));
		return SUCCESS;
	}
	
	/**
	 * Draft Pembuat Kontrak
	 */
	@SuppressWarnings("unchecked")
	@Action(
			results={
					@Result(name=SUCCESS,location="proc/rfq/listDraftKontrak.jsp"),
					@Result(name="view",location="proc/rfq/viewDraftKontrak.jsp"),
//					@Result(name="ok",location="kontrak/draftKontrak", type="redirectAction")
					@Result(name = "ok", location = "dashboard", type="redirectAction")
			},value="kontrak/draftKontrak"
			)
	public String draftKontrak() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "proses")){
				model.setContractHeader(rfqService.getContractHeaderById(model.getContractHeader()));
				model.setListContractItem(rfqService.listContractItemByHeader(model.getContractHeader()));
				model.setListContractComment(rfqService.listCommentByContractHeader(model.getContractHeader()));
				model.getContractHeader().getPrcMainHeader().setAdmProses(model.getContractHeader().getAdmProses());
				model.getContractHeader().getPrcMainHeader().setPpmProsesLevel(model.getContractHeader().getContractProsesLevel());
				String kodePlant = model.getContractHeader().getContractNumber().split("PO")[1];
				model.setListPurchasingOrganization(rfqService.listAdmPurchOrgByPlant(kodePlant.substring(0,5)));
				
				model.setListUser(new AdmServiceImpl().getListUser(null));
				model.setListContractTop(rfqService.listContractTopByHeader(model.getContractHeader()));
				model.setListContractPb(rfqService.listContractPbByHeader(model.getContractHeader()));
				SessionGetter.setSessionValue(model.getListContractTop(), "listCtrContractTop");
				SessionGetter.setSessionValue(model.getListContractPb(), "listCtrContractPb");
				return "view";
			}else if(WebUtils.hasSubmitParameter(request, "saveAndFinish")){
				model.getCtrContractComment().setAksi(" Pembuatan Kontrak");
				model.getCtrContractComment().setProsesAksi(SessionGetter.getPropertiesValue("ctr.2", null));
				model.getCtrContractComment().setCreatedBy(SessionGetter.getPropertiesValue("ctr.3", null));
				List<CtrContractTop> listTop = (List<CtrContractTop>) SessionGetter.getSessionValue("listCtrContractTop");
				List<CtrContractPb> listPb = (List<CtrContractPb>) SessionGetter.getSessionValue("listCtrContractPb");
				model.getContractHeader().setPurchasingOrganization(model.getKodePurcOrg());
				rfqService.saveDraftKontrak(model.getContractHeader(),model.getCtrContractComment(),model.getDocsKomentar(),model.getDocsKomentarFileName(),listTop,listPb);
				SessionGetter.destroySessionValue("listCtrContractTop");
				SessionGetter.destroySessionValue("listCtrContractPb");
				addActionMessage(getText("notif.save"));
				return "ok";
			}else if(WebUtils.hasSubmitParameter(request, "legal")){
				model.getCtrContractComment().setAksi(" Pembuatan Kontrak");
				model.getCtrContractComment().setProsesAksi(SessionGetter.getPropertiesValue("ctr.2", null));
				model.getCtrContractComment().setCreatedBy(SessionGetter.getPropertiesValue("ctr.3", null));
				List<CtrContractTop> listTop = (List<CtrContractTop>) SessionGetter.getSessionValue("listCtrContractTop");
				List<CtrContractPb> listPb = (List<CtrContractPb>) SessionGetter.getSessionValue("listCtrContractPb");
				rfqService.saveDraftKontrakLegal(model.getContractHeader(),model.getCtrContractComment(),model.getDocsKomentar(),model.getDocsKomentarFileName(),listTop,listPb);
				SessionGetter.destroySessionValue("listCtrContractTop");
				SessionGetter.destroySessionValue("listCtrContractPb");
				return "ok";
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
			e.printStackTrace();
			return "ok";
		}
		model.setListContractHeader(rfqService.listContractPembuatanDraftKontrak(null));
		return SUCCESS;
	}
	
	/**
	 * submit TOP
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Action(
			results={
					@Result(name=SUCCESS, location="proc/rfq/ajax/listTop.jsp")
			},value="ajax/submitTop"
	)
	public String ajaxsubmitTop() throws Exception{
		try {
			System.out.println(">>>>>> SUBMITTT TOPPPP ");
			List<CtrContractTop> tmpListItem = (List<CtrContractTop>) SessionGetter.getSessionValue("listCtrContractTop");
			List<CtrContractTop> listItem = new ArrayList<CtrContractTop>();
			
			if(request.getParameter("index") != null && request.getParameter("index") != "" && request.getParameter("act") != null && request.getParameter("act").equals("delete")){
				if(request.getParameter("id") != null && request.getParameter("id") != ""){
					int id = Integer.parseInt(request.getParameter("id"));
					rfqService.deleteTopItem(rfqService.getTopItemById(id));
				}
				
	 			int n = 0;
				int target = Integer.parseInt(request.getParameter("index"));
				if(tmpListItem != null){
					for(CtrContractTop i:tmpListItem){
						if(n != target){
							listItem.add(i);
						}
						n = n+1;
					}
				}
				SessionGetter.setSessionValue(listItem, "listCtrContractTop");
				
				model.setListContractTop(listItem);
				return SUCCESS;
			}
			
			if(tmpListItem != null){
				for(CtrContractTop i:tmpListItem){
					listItem.add(i);
				}
			}
			
			listItem.add(model.getContractTop());
			SessionGetter.setSessionValue(listItem, "listCtrContractTop");
			
			model.setListContractTop(listItem);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}	
	
	/**
	 * submit TOP
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Action(
			results={
					@Result(name=SUCCESS, location="proc/rfq/ajax/listPb.jsp")
			},value="ajax/submitPb"
	)
	public String ajaxsubmitPrcMainItem() throws Exception{
		System.out.println(">>> MASUK SUBMIT PB");
		List<CtrContractPb> tmpListItem = (List<CtrContractPb>) SessionGetter.getSessionValue("listCtrContractPb");
		List<CtrContractPb> listItem = new ArrayList<CtrContractPb>();
		
		if(request.getParameter("index") != null && request.getParameter("index") != "" && request.getParameter("act") != null && request.getParameter("act").equals("delete")){
			if(request.getParameter("id") != null && request.getParameter("id") != ""){
				int id = Integer.parseInt(request.getParameter("id"));
				rfqService.deletePbItem(rfqService.getPbItemById(id));
			}
			
 			int n = 0;
			int target = Integer.parseInt(request.getParameter("index"));
			if(tmpListItem != null){
				for(CtrContractPb i:tmpListItem){
					if(n != target){
						listItem.add(i);
					}
					n = n+1;
				}
			}
			SessionGetter.setSessionValue(listItem, "listCtrContractPb");
			
			model.setListContractPb(listItem);
			return SUCCESS;
		}
		
		if(tmpListItem != null){
			for(CtrContractPb i:tmpListItem){
				listItem.add(i);
			}
		}
		
		listItem.add(model.getContractPb());
		SessionGetter.setSessionValue(listItem, "listCtrContractPb");
		
		model.setListContractPb(listItem);
		System.out.println(">>> MASUK SUBMIT PB"+model.getListContractPb().size() );
		return SUCCESS;
	}
	
	
	/**
	 * Draft Pembuat Kontrak
	 */
	@Action(
			results={
					@Result(name=SUCCESS,location="proc/rfq/listApprovalDraftKontrak.jsp"),
					@Result(name="view",location="proc/rfq/viewApprovalDraftKontrak.jsp"),
//					@Result(name="ok",location="kontrak/approvalDraftKontrak", type="redirectAction")
					@Result(name = "ok", location = "dashboard", type="redirectAction")
			},value="kontrak/approvalDraftKontrak"
			)
	public String ApprovaldraftKontrak() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "proses")){
				model.setContractHeader(rfqService.getContractHeaderById(model.getContractHeader()));
				model.setListContractItem(rfqService.listContractItemByHeader(model.getContractHeader()));
				model.setListContractComment(rfqService.listCommentByContractHeader(model.getContractHeader()));
				model.getContractHeader().getPrcMainHeader().setAdmProses(model.getContractHeader().getAdmProses());
				model.getContractHeader().getPrcMainHeader().setPpmProsesLevel(model.getContractHeader().getContractProsesLevel());
				String kodePlant = model.getContractHeader().getContractNumber().split("PO")[1];
				System.out.println(model.getContractHeader().getPurchasingOrganization());
				System.out.println(kodePlant.substring(0,5));
				model.setAdmPurchOrg(rfqService.getAdmPurchOrgByKode(model.getContractHeader().getPurchasingOrganization(), kodePlant.substring(0,5)));
				if(model.getAdmPurchOrg()==null) {
					model.setAdmPurchOrg(rfqService.getAdmPurchOrgByKode(model.getContractHeader().getPurchasingOrganization(), "1501"));
				}
				model.setListUser(new AdmServiceImpl().getListUser(null));
				model.setListContractTop(rfqService.listContractTopByHeader(model.getContractHeader()));
				model.setListContractPb(rfqService.listContractPbByHeader(model.getContractHeader()));
				return "view";
			}else if(WebUtils.hasSubmitParameter(request, "saveAndFinish")){
				model.getCtrContractComment().setAksi("Persetujuan Draft Kontrak");
				model.getCtrContractComment().setProsesAksi(SessionGetter.getPropertiesValue("ctr.3", null));
				model.getCtrContractComment().setCreatedBy(SessionGetter.getPropertiesValue("ctr.3", null));
				rfqService.saveApprovalKontrak(model.getContractHeader(),model.getCtrContractComment(),model.getDocsKomentar(),model.getDocsKomentarFileName());
				addActionMessage(getText("notif.save"));
				return "ok";
			}else if(WebUtils.hasSubmitParameter(request, "revisi")){
				model.getCtrContractComment().setAksi("Revisi Draft Kontrak");
				model.getCtrContractComment().setProsesAksi(SessionGetter.getPropertiesValue("ctr.3", null));
				model.getCtrContractComment().setCreatedBy(SessionGetter.getPropertiesValue("ctr.2", null));
				rfqService.saveRevisiKontrak(model.getContractHeader(),model.getCtrContractComment(),model.getDocsKomentar(),model.getDocsKomentarFileName());
				return "ok";
			}else if(WebUtils.hasSubmitParameter(request, "legal")){
				model.getCtrContractComment().setAksi("Persetujuan Legal Kontrak");
				model.getCtrContractComment().setProsesAksi(SessionGetter.getPropertiesValue("ctr.3", null));
				model.getCtrContractComment().setCreatedBy(SessionGetter.getPropertiesValue("ctr.2", null));
				rfqService.saveSetujuKontrakLegal(model.getContractHeader(),model.getCtrContractComment(),model.getDocsKomentar(),model.getDocsKomentarFileName());
				return "ok";
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
			e.printStackTrace();
			return "ok";
		}
		model.setListContractHeader(rfqService.listContractApprovalDraftKontrak(null));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="proc/rfq/listApprovalDraftKontrak.jsp")
			},value="kontrak/daftarKontrak"
			)
	public String listKontrak() throws Exception{
		return SUCCESS;
	}
	
	
	/**
	 * 
	 * Report Kontrak
	 * 
	 */
	public String generateToken() {
		try {
			 RestTemplate template = new RestTemplate();
				String url = SessionGetter.MIDDLEWARE_URL+"CSRF-Token?sap-client=120";
				HttpHeaders headers = new HttpHeaders();
				headers.set("Authorization", "Basic dGxrbS1rZW1hbDpqYWthcnRhMTIz");
				headers.set("X-CSRF-Token", "Fetch");
				
				/*template.getInterceptors().add(
						  new BasicAuthorizationInterceptor("tlkm-kemal", "jakarta123"));				*/
				HttpEntity<Object> request = new HttpEntity<Object>(headers);
				ResponseEntity<String> response = template.exchange(url, HttpMethod.GET, request, String.class);
		String		xcsrftoken = response.getHeaders().getFirst("x-csrf-token");
		
				System.out.println(xcsrftoken);
				return xcsrftoken;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	@Action(
			results={
					@Result(name=SUCCESS,location="proc/rfq/listKontrakAktif.jsp"),
					@Result(name="view",location="proc/rfq/viewKontrak.jsp"),
					@Result(name="viewsap",location="proc/rfq/viewKontrakSap.jsp"),
					@Result(name="adendum",location="proc/rfq/viewKontrakAdendum.jsp"),
					@Result(name="viewjde",location="proc/rfq/viewKontrakJde.jsp"),
					@Result(name="ok",location="kontrak/daftarKontrakAktif", type="redirectAction"),
					@Result(name="close",location="proc/rfq/viewCloseKontrak.jsp"),
			},value="kontrak/daftarKontrakAktif"
			)
	public String reportKontrak() throws Exception, HttpClientErrorException{
		try {
			if(WebUtils.hasSubmitParameter(request, "proses")){
				model.setContractHeader(rfqService.getContractHeaderById(model.getContractHeader()));
				model.setListContractItem(rfqService.listContractItemByHeader(model.getContractHeader()));
				model.setListContractComment(rfqService.listCommentByContractHeader(model.getContractHeader()));
				model.getContractHeader().getPrcMainHeader().setAdmProses(model.getContractHeader().getAdmProses());
				model.getContractHeader().getPrcMainHeader().setPpmProsesLevel(model.getContractHeader().getContractProsesLevel());
				String kodePlant = model.getContractHeader().getContractNumber().split("PO")[1];
				model.setAdmPurchOrg(rfqService.getAdmPurchOrgByKode(model.getContractHeader().getPurchasingOrganization(), kodePlant.substring(0,5)));
				if(model.getAdmPurchOrg()==null) {
					model.setAdmPurchOrg(rfqService.getAdmPurchOrgByKode(model.getContractHeader().getPurchasingOrganization(), "1501"));
				}
				model.setListContractTop(rfqService.listContractTopByHeader(model.getContractHeader()));
				model.setListContractPb(rfqService.listContractPbByHeader(model.getContractHeader()));
				return "view";
			}else if(WebUtils.hasSubmitParameter(request, "adendum")){
				model.setContractHeader(rfqService.getContractHeaderById(model.getContractHeader()));
				CtrContractHeader amend =  new CtrContractHeader();
				String nomor = model.getContractHeader().getContractNumber();
//				String tmp[] = nomor.split("-");
//				if(tmp.length==1){
//					nomor = nomor.concat("-").concat("1");
//				}else{
//					Integer no = Integer.valueOf(tmp[1]);
//					no = no+1;
//					nomor = nomor.trim().concat("-").concat(String.valueOf(no));;
//				}
				amend.setContractNumber(nomor);
				model.setContractHeaderAdendum(amend);
				
				model.setListContractItem(rfqService.listContractItemByHeader(model.getContractHeader()));
				model.setListContractComment(rfqService.listCommentByContractHeader(model.getContractHeader()));
				model.getContractHeader().getPrcMainHeader().setAdmProses(model.getContractHeader().getAdmProses());
				model.getContractHeader().getPrcMainHeader().setPpmProsesLevel(model.getContractHeader().getContractProsesLevel());
				model.setListContractTop(rfqService.listContractTopByHeader(model.getContractHeader()));
				model.setListContractPb(rfqService.listContractPbByHeader(model.getContractHeader()));
				return "adendum";
			}else if(WebUtils.hasSubmitParameter(request, "saveadendum")){
				rfqService.createAdendum(model.getContractHeaderAdendum(),model.getContractHeader(),model.getListContractItem(),model.getCtrContractComment());
				addActionMessage("Data Sudah Disimpan");
				return SUCCESS;
			}else if(WebUtils.hasSubmitParameter(request, "viewsap")){
				model.setContractHeader(rfqService.getContractHeaderById(model.getContractHeader()));
				model.setListContractItem(rfqService.listContractItemByHeader(model.getContractHeader()));
				model.setListContractComment(rfqService.listCommentByContractHeader(model.getContractHeader()));
				model.getContractHeader().getPrcMainHeader().setAdmProses(model.getContractHeader().getAdmProses());
				model.getContractHeader().getPrcMainHeader().setPpmProsesLevel(model.getContractHeader().getContractProsesLevel());
				String kodePlant = model.getContractHeader().getContractNumber().split("PO")[1];
				model.setAdmPurchOrg(rfqService.getAdmPurchOrgByKode(model.getContractHeader().getPurchasingOrganization(), kodePlant.substring(0,5)));
				if(model.getAdmPurchOrg()==null) {
					model.setAdmPurchOrg(rfqService.getAdmPurchOrgByKode(model.getContractHeader().getPurchasingOrganization(), "1501"));
				}
				model.setListCurrency(new AdmServiceImpl().listAdmCurr());
				model.setListContractTop(rfqService.listContractTopByHeader(model.getContractHeader()));
				model.setListContractPb(rfqService.listContractPbByHeader(model.getContractHeader()));
				return "viewsap";
			}else if(WebUtils.hasSubmitParameter(request, "savesap")){
				CtrContractHeader tmpCtr = new CtrContractHeader();
				tmpCtr = model.getContractHeader();
				model.setContractHeader(rfqService.getContractHeaderForSAP(model.getContractHeader()));
				model.setListContractItem(rfqService.listContractItemByHeader(model.getContractHeader()));
				AdmCurrency curr = new AdmServiceImpl().getCurrName(tmpCtr.getAdmCurrency());
				model.getContractHeader().setAdmCurrency(curr);
				ResultDto result = rfqService.saveContractSap(model.getContractHeader(), model.getListContractItem());
				PoHeaderDto poSap = (PoHeaderDto) result.getData();
				if(result.getStatus()==200) {
					addActionMessage("Push PO Berhasil : " + poSap.getEBELN());
				}else {
					ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
					String json = ow.writeValueAsString(poSap.getMESSAGES());
					throw new Exception(json);
				}
				return SUCCESS;
				
			}else if(WebUtils.hasSubmitParameter(request, "close")){
				rfqService.saveCloseKontrak(model.getContractHeader(),model.getCtrContractComment(),model.getDocsKomentar(),model.getDocsKomentarFileName());
				return SUCCESS;
			}else if(WebUtils.hasSubmitParameter(request, "viewclose")){
				model.setContractHeader(rfqService.getContractHeaderById(model.getContractHeader()));
				model.setListContractItem(rfqService.listContractItemByHeader(model.getContractHeader()));
				model.setListContractComment(rfqService.listCommentByContractHeader(model.getContractHeader()));
				model.getContractHeader().getPrcMainHeader().setAdmProses(model.getContractHeader().getAdmProses());
				model.getContractHeader().getPrcMainHeader().setPpmProsesLevel(model.getContractHeader().getContractProsesLevel());
				model.setListContractTop(rfqService.listContractTopByHeader(model.getContractHeader()));
				model.setListContractPb(rfqService.listContractPbByHeader(model.getContractHeader()));
				return "close";
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
			e.printStackTrace();
			return "ok";
		}
		return SUCCESS;
	}
	
	@SuppressWarnings("unused")
	private POHeader setHeaderCOntractToPoHeader(
			CtrContractHeader contractHeader) {
		POHeader poHeader = new POHeader();
		String tmp [] =contractHeader.getContractNumber().trim().split("OP");
		poHeader.setMnOrderNumber(Integer.valueOf(tmp[0].trim()));
		poHeader.setSzOrderType("O1");
		String com = tmp[1];
		if(com.split("-").length>1){
			com = com.split("-")[0].trim();
		}
		poHeader.setSzOrderCompany(com);
		poHeader.setSzOrderSuffix("000");
		poHeader.setSzBranchPlant(com);
		poHeader.setMnSupplierNumber(Integer.valueOf(contractHeader.getVndHeader().getVendorSmkNo()));
		poHeader.setMnShipToNumber(Integer.valueOf(com.trim()));
		poHeader.setJdRequestedDate(new Date());
		poHeader.setJdOrderDate(new Date());
		poHeader.setSzProgramID("EPROC");
		return poHeader;
	}

	@Action(
			results={
					@Result(name=SUCCESS,location="proc/rfq/ajax/searchKontrakAktif.jsp")
			},value="ajax/report/searchKontrakAktif"
			)
	public String searchKontrak() throws Exception{
		try {
			model.getContractHeader().setContractProsesLevel(99);
//			model.setListContractHeader(rptService.searchKontrakHeader(model.getContractHeader()));
			model.setListContractHeader(rptService.searchKontrakHeaderAktif(model.getContractHeader(),model.getPeriode()));
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="proc/rfq/ajax/listKontrakAktif.jsp")
			},value="ajax/kontrak/daftarKontrakAktif"
			)
	public String ajaxReportKontrak() throws Exception{
		try {
			CtrContractHeader ch = new CtrContractHeader();
			ch.setContractProsesLevel(99);
			model.setResultSize(rptService.countContractHeader(ch));
			model.setListContractHeader(rptService.listCountContractHeaderReport(ch,model.getPage(request),10));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	/** kontrak non aktif **/
	@Action(
			results={
					@Result(name=SUCCESS,location="proc/rfq/listKontrakNonAktif.jsp"),
					@Result(name="view",location="proc/rfq/viewKontrak.jsp"),
					@Result(name="ok",location="kontrak/daftarKontrakNonAktif", type="redirectAction"),
			},value="kontrak/daftarKontrakNonAktif"
			)
	public String reportKontrakNonAktif() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "proses")){
				model.setContractHeader(rfqService.getContractHeaderById(model.getContractHeader()));
				model.setListContractItem(rfqService.listContractItemByHeader(model.getContractHeader()));
				model.setListContractComment(rfqService.listCommentByContractHeader(model.getContractHeader()));
				model.getContractHeader().getPrcMainHeader().setAdmProses(model.getContractHeader().getAdmProses());
				model.getContractHeader().getPrcMainHeader().setPpmProsesLevel(model.getContractHeader().getContractProsesLevel());
				model.setListContractTop(rfqService.listContractTopByHeader(model.getContractHeader()));
				model.setListContractPb(rfqService.listContractPbByHeader(model.getContractHeader()));
				return "view";
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
			e.printStackTrace();
			return "ok";
		}
		return SUCCESS;
	}
	@Action(
			results={
					@Result(name=SUCCESS,location="proc/rfq/ajax/listKontrakNonAktif.jsp")
			},value="ajax/report/searchKontrakNonAktif"
			)
	public String searchKontrakNonAktif() throws Exception{
		try {
			model.getContractHeader().setContractProsesLevel(100);
			model.setListContractHeader(rptService.searchKontrakHeader(model.getContractHeader(), 0));
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="proc/rfq/ajax/listKontrakNonAktif.jsp")
			},value="ajax/kontrak/daftarKontrakNonAktif"
			)
	public String ajaxReportKontrakNonAktif() throws Exception{
		try {
			CtrContractHeader ch = new CtrContractHeader();
			ch.setContractProsesLevel(100);
			model.setResultSize(rptService.countContractHeader(ch));
			model.setListContractHeader(rptService.listCountContractHeaderReport(ch,model.getPage(request),10));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="proc/rfq/ajax/formSearchMainJde.jsp")
			},value="ajax/formSearchMainJde"
			)
	public String ajaxFormSearchJdeMain() throws Exception{
		try {
			model.setListJdeMasterMain(rfqService.listJdeMasterMain(model.getJdeSearch()));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="proc/tmp/updateTanggalKontrak.jsp"),
					@Result(name="ok",location="kontrak/daftarKontrakAktif",type="redirectAction")
			},value="ajax/formUpdateTanggalKontrak"
			)
	public String ajaxFormUpdateTanggal() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "save")){
				rfqService.updateTanggalKontrak(model.getContractHeader());
				return "ok";
			}else{
				model.setContractHeader(rfqService.getContractHeaderById(model.getContractHeader()));
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
			return "ok";
		}
		return SUCCESS;
	}
}
//ResultDto resultDto = new ResultDto();
//RestTemplate template = new RestTemplate();
//String url = SessionGetter.SAP_URL + "CREATEPO?sap-client=120";
//DateFormat df = new SimpleDateFormat("yyyyMMdd");
//Date today = new Date();
//
//HttpHeaders headers = new HttpHeaders();
//headers.setContentType(MediaType.APPLICATION_JSON);
//headers.set("Authorization", "Basic dGxrbS1rZW1hbDpqYWthcnRhMTIz");
//headers.set("X-CSRF-Token", "noCDfhVUXYNVc_a4Px7LPQ==");
//headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
//CtrContractHeader ctr = model.getContractHeader();
//

////poHeader.setITEM(tmp);
//String json;
//
//String header;
//String detail = "";
//String empty = " \"\" ";
////DateFormat df = new SimpleDateFormat("yyyyMMdd");
////Date today = new Date();
//String date = df.format(today);
////System.out.println(date);
//
//String curr =  ctr.getAdmCurrency()==null?empty:ctr.getAdmCurrency().getCurrencyCode();
//header ="  \"ZEBELN\": \"" + ctr.getContractNumber().split("OP")[0].trim()+ "\"," + 
//		"  \"EBELN\": " + empty + "," + 
//		"  \"BEDAT\": \"" + df.format(ctr.getCreatedDate()).trim()+"\"," + 
//		"  \"ELIFN\": \"" +String.valueOf(ctr.getVndHeader().getVendorId()).trim() + "\"," + 
//		"  \"WAERS\": \"" +curr.trim()+"\"," + 
//		"  \"EKORG\": \"0\"," +
//		"  \"BKGRP\": \"0\"," +
//		"  \"ZTERM\":" + empty +"," +
//		"  \"INCO1\":" + empty +"," +
//		"  \"AEDAT\":\"" + date.trim() + "\"," +
//		"  \"ERNAM\":\"" + ((AdmUser)SessionGetter.getSessionValue(SessionGetter.USER_SESSION)).getUsername() + "\"," +
//		"  \"KDATB\":\"" + df.format(ctr.getContractStart()) +"\"," +
//		"  \"KDATE\":\"" + df.format(ctr.getContractEnd()) + "\"," ;
//
//int i = 0;
//for(CtrContractItem itm : model.getListContractItem() ) {
//	String bcommitmnt;
//	if (itm.getServicePriceOe().doubleValue()!=0)
//		bcommitmnt = String.valueOf(itm.getServicePriceOe().doubleValue());
//	else
//		bcommitmnt = empty;
//	String d = "    { "
//			+ "    \"EBELP\":\""+ itm.getLineId() + "\"," +
//			"      \"MATNR\":\""+ itm.getItemCode() + "\"," + 
//			"      \"EWERK\":\""+ ctr.getContractNumber().split("OP")[1].trim() + "\"," + 
//			"      \"LGORT\":"+ empty+"," + 
//			"      \"TXZ01\":\"" + itm.getItemDescription().substring(0, 39).trim() +"\"," + 
//			"      \"BSTMG\":\"" + String.valueOf(itm.getItemQuantityOe()).trim() + "\"," + 
//			"      \"BSTME\":\"" + itm.getAdmUom().getUomName().trim() + "\"," + 
//			"      \"EPEIN\":\"" +String.valueOf(itm.getItemPriceOe().doubleValue()).trim() + "\"," + 
//			"      \"AFNAM\":\"" + ctr.getAdmUserByContractCreator().getCompleteName().trim() + "\"," + 
//			"      \"BANFN\":\"" + ctr.getPrcMainHeader().getPpmNomorPr().split("ZMN")[0].trim() + "\"," + 
//			"      \"BNFPO\":\"" + String.valueOf(itm.getLineId()).trim() + "\"," + 
//			"      \"BSUMLIMIT\":" + empty +"," + 
//			"      \"BCOMMITMNT\":\" " +bcommitmnt + "\""
//			+ " }";
//	detail += d;
//	
//	i++;
//	if(model.getListContractItem().size()>1) {
//		if(i <= model.getListContractItem().size()) {
//			detail = detail.concat(",");
//		}else if (i == model.getListContractItem().size()-1) {
//			detail = detail.concat("");
//		}
//	}
//	
//}
//
//
//json = "{"
//		+ header
//		+ "\"ITEMS\":["+detail + "]"
//		+ "}";
//System.out.println(header);
//System.out.println(detail);
//System.out.println(json);
//String js = "{\n" + 
//		"  \"ZEBELN\": \"E000000003\",\n" + 
//		"  \"EBELN\": \"\",\n" + 
//		"  \"BEDAT\": \"20180702\",\n" + 
//		"  \"ELIFN\": \"3000010\",\n" + 
//		"  \"WAERS\": \"IDR\",\n" + 
//		"  \"EKORG\": \"1001\",\n" + 
//		"  \"BKGRP\": \"103\",\n" + 
//		"  \"ZTERM\": \"Z030\",\n" + 
//		"  \"INCO1\": \"FRC\",\n" + 
//		"  \"AEDAT\": \"20180702\",\n" + 
//		"  \"ERNAM\": \"TLKM-KEMAL\",\n" + 
//		"  \"KDATB\": \"20180702\",\n" + 
//		"  \"KDATE\": \"20180731\",\n" + 
//		"  \"ITEMS\": [\n" + 
//		"    {\n" + 
//		"      \"EBELP\": \"10\",\n" + 
//		"      \"MATNR\": \"10000000\",\n" + 
//		"      \"EWERK\": \"1203\",\n" + 
//		"      \"LGORT\": \"E305\",\n" + 
//		"      \"TXZ01\": \"MINING CLAY\",\n" + 
//		"      \"BSTMG\": \"1\",\n" + 
//		"      \"BSTME\": \"TON\",\n" + 
//		"      \"EPEIN\": \"30000\",\n" + 
//		"      \"AFNAM\": \"Pudjadhi\",\n" + 
//		"      \"BANFN\": \"2200000600\",\n" + 
//		"      \"BNFPO\": \"10\",\n" + 
//		"      \"BSUMLIMIT\": \"\",\n" + 
//		"      \"BCOMMITMNT\": \"\"\n" + 
//		"    }\n" + 
//		"  ]\n" + 
//		"}\n" + 
//		"";
//HttpEntity<Object> request = new HttpEntity<Object>(js, headers);
//System.out.println(js);
//Object result = template.postForObject(url, request, Object.class);
//System.out.println(result.getClass().getField("BANFN"));