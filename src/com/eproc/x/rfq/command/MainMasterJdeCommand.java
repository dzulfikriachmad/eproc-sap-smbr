package com.eproc.x.rfq.command;

import java.io.Serializable;

public class MainMasterJdeCommand implements Serializable {

	private String key;
	private String value;
	private String tipe;
	
	//ANggaran
	private String saldoAwal;
	private String transaksi;
	private String saldoSisa;
	private String costCenter;
	private String rekening;
	private String subRekening;
	public MainMasterJdeCommand() {
		// TODO Auto-generated constructor stub
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getTipe() {
		return tipe;
	}
	public void setTipe(String tipe) {
		this.tipe = tipe;
	}
	public String getSaldoAwal() {
		return saldoAwal;
	}
	public void setSaldoAwal(String saldoAwal) {
		this.saldoAwal = saldoAwal;
	}
	public String getTransaksi() {
		return transaksi;
	}
	public void setTransaksi(String transaksi) {
		this.transaksi = transaksi;
	}
	public String getSaldoSisa() {
		return saldoSisa;
	}
	public void setSaldoSisa(String saldoSisa) {
		this.saldoSisa = saldoSisa;
	}
	public String getCostCenter() {
		return costCenter;
	}
	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}
	public String getRekening() {
		return rekening;
	}
	public void setRekening(String rekening) {
		this.rekening = rekening;
	}
	public String getSubRekening() {
		return subRekening;
	}
	public void setSubRekening(String subRekening) {
		this.subRekening = subRekening;
	}
}
