package com.eproc.x.rfq.command;

import java.io.Serializable;
import java.math.BigDecimal;

public class TabulasiPenilaian implements Serializable {

	private Integer vendorId;
	private String vendorName;
	private Integer pmpAdmStatus;
	private BigDecimal passingGrade;
	private BigDecimal bobotTeknis;
	private BigDecimal nilaiTeknis;
	private BigDecimal bobotHarga;
	private BigDecimal nilaiHarga;
	private BigDecimal totalPenawaran;
	private Integer statusTeknis;
	private String catatanTeknis;
	private Integer statusHarga;
	private String catatanHarga;
	private BigDecimal totalNilai;
	private Integer ppmId;
	private BigDecimal totalPenawaranNego;
	private BigDecimal totalPenawaranEauction;
	private Integer priceEvaluated;
	private Integer techEvaluated;
	
	public TabulasiPenilaian() {
		// TODO Auto-generated constructor stub
	}

	public Integer getVendorId() {
		return vendorId;
	}

	public void setVendorId(Integer vendorId) {
		this.vendorId = vendorId;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public Integer getPmpAdmStatus() {
		return pmpAdmStatus;
	}

	public void setPmpAdmStatus(Integer pmpAdmStatus) {
		this.pmpAdmStatus = pmpAdmStatus;
	}

	public BigDecimal getPassingGrade() {
		return passingGrade;
	}

	public void setPassingGrade(BigDecimal passingGrade) {
		this.passingGrade = passingGrade;
	}

	public BigDecimal getBobotTeknis() {
		return bobotTeknis;
	}

	public void setBobotTeknis(BigDecimal bobotTeknis) {
		this.bobotTeknis = bobotTeknis;
	}

	public BigDecimal getNilaiTeknis() {
		return nilaiTeknis;
	}

	public void setNilaiTeknis(BigDecimal nilaiTeknis) {
		this.nilaiTeknis = nilaiTeknis;
	}

	public BigDecimal getBobotHarga() {
		return bobotHarga;
	}

	public void setBobotHarga(BigDecimal bobotHarga) {
		this.bobotHarga = bobotHarga;
	}

	public BigDecimal getNilaiHarga() {
		return nilaiHarga;
	}

	public void setNilaiHarga(BigDecimal nilaiHarga) {
		this.nilaiHarga = nilaiHarga;
	}

	public BigDecimal getTotalPenawaran() {
		return totalPenawaran;
	}

	public void setTotalPenawaran(BigDecimal totalPenawaran) {
		this.totalPenawaran = totalPenawaran;
	}

	public Integer getStatusTeknis() {
		return statusTeknis;
	}

	public void setStatusTeknis(Integer statusTeknis) {
		this.statusTeknis = statusTeknis;
	}

	public String getCatatanTeknis() {
		return catatanTeknis;
	}

	public void setCatatanTeknis(String catatanTeknis) {
		this.catatanTeknis = catatanTeknis;
	}

	public Integer getStatusHarga() {
		return statusHarga;
	}

	public void setStatusHarga(Integer statusHarga) {
		this.statusHarga = statusHarga;
	}

	public String getCatatanHarga() {
		return catatanHarga;
	}

	public void setCatatanHarga(String catatanHarga) {
		this.catatanHarga = catatanHarga;
	}

	public BigDecimal getTotalNilai() {
		return totalNilai;
	}

	public void setTotalNilai(BigDecimal totalNilai) {
		this.totalNilai = totalNilai;
	}

	public Integer getPpmId() {
		return ppmId;
	}

	public void setPpmId(Integer ppmId) {
		this.ppmId = ppmId;
	}

	public BigDecimal getTotalPenawaranNego() {
		return totalPenawaranNego;
	}

	public void setTotalPenawaranNego(BigDecimal totalPenawaranNego) {
		this.totalPenawaranNego = totalPenawaranNego;
	}

	public BigDecimal getTotalPenawaranEauction() {
		return totalPenawaranEauction;
	}

	public void setTotalPenawaranEauction(BigDecimal totalPenawaranEauction) {
		this.totalPenawaranEauction = totalPenawaranEauction;
	}

	public Integer getPriceEvaluated() {
		return priceEvaluated;
	}

	public void setPriceEvaluated(Integer priceEvaluated) {
		this.priceEvaluated = priceEvaluated;
	}

	public Integer getTechEvaluated() {
		return techEvaluated;
	}

	public void setTechEvaluated(Integer techEvaluated) {
		this.techEvaluated = techEvaluated;
	}
	
}
