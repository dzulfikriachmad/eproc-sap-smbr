package com.eproc.x.rfq.command;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.jde.model.PODetail;
import com.jde.model.POHeader;
import com.orm.model.AdmCurrency;
import com.orm.model.AdmEmployee;
import com.orm.model.AdmPurchasingOrganization;
import com.orm.model.AdmUser;
import com.orm.model.CtrContractComment;
import com.orm.model.CtrContractHeader;
import com.orm.model.CtrContractItem;
import com.orm.model.CtrContractPb;
import com.orm.model.CtrContractTop;
import com.orm.model.HistMainComment;
import com.orm.model.HistoryHargaNegosiasi;
import com.orm.model.PanitiaPengadaan;
import com.orm.model.PanitiaPengadaanDetail;
import com.orm.model.PrcMainDoc;
import com.orm.model.PrcMainHeader;
import com.orm.model.PrcMainItem;
import com.orm.model.PrcMainPreparation;
import com.orm.model.PrcMainVendor;
import com.orm.model.PrcMainVendorQuote;
import com.orm.model.PrcMainVendorQuoteAdmtech;
import com.orm.model.PrcMainVendorQuoteItem;
import com.orm.model.PrcMethod;
import com.orm.model.PrcMsgNegotiation;
import com.orm.model.PrcTemplate;
import com.orm.model.VndHeader;
import com.orm.tools.AbstractCommand;

public class RfqCommand extends AbstractCommand {

	private List<PrcMainHeader> listPrcMainHeader;
	private List<AdmUser> listPembeli;
	private List<AdmUser> listStaffPembeli;
	private List<HistMainComment> listHistMainComment;
	private List<PrcMainItem> listPrcMainItem;
	private String idKomponen;
	private String jdeSearch;
	private Double hargaTerendah;
	private String kodePurcOrg;
	
	private List<HistoryHargaNegosiasi> listHargaNegosiasi;
	
	private PanitiaPengadaan panitia;
	private PanitiaPengadaanDetail panitiaDetail;
	private List<PanitiaPengadaanDetail> listPanitiaDetail;
	private List<AdmUser> listUser;
	private List<PrcMethod> listMetode;
	private List<PrcTemplate> listTemplate;
	private List<VndHeader> listMitraKerja;
	private List<PrcMainVendor> listPrcMainVendor = new ArrayList<PrcMainVendor>();
	private PrcTemplate prcTemplate;
	private List<File> ppdDocument;
	private List<String> ppdDocumentFileName;
	private List<String> ppdCategory;
	private List<String> ppdDescription;
	private PrcMainPreparation prcMainPreparation;
	private List<PrcMainDoc> listPrcMainDoc;
	private List<PrcMainVendor> listVendor;
	private List<PrcMainVendor> listVendorLulus;
	private List<PrcMainVendor> listVendorTidakLulus;
	private POHeader poHeader;
	private List<PODetail> listPoDetail;
	private List<MainMasterJdeCommand> listJdeMasterMain;

	private List<AdmCurrency> listCurrency;
	private AdmCurrency curr;
	private PrcMainHeader prcMainHeader;
	private AdmEmployee admEmployee;
	private HistMainComment histMainComment;

	private File docsKomentar;
	private String docsKomentarFileName;
	private String docsKomentarContentType;
	
	private String jdeUser;
	private String jdePassword; 
	
	private Integer periode;
	
	
	private List<PrcMainVendorQuoteAdmtech> listAdministrasi;
	private List<PrcMainVendorQuoteAdmtech> listTeknis;
	private PrcMainVendorQuote quote;
	private List<PrcMainVendorQuote> listQuote;
	private List<PrcMainVendorQuoteItem> listQuoteItem;
	private List<List<PrcMainVendor>> listVendorUsulanPemenang;
	private PrcMainVendor prcMainVendor;
	private List<TabulasiPenilaian> listTabulasi;
	private PrcMsgNegotiation negosiasi;
	private List<PrcMsgNegotiation> listNegosiasi;
	
	private CtrContractHeader contractHeader;
	private CtrContractHeader contractHeaderAdendum;
	private List<CtrContractHeader> listContractHeader;
	private List<CtrContractItem> listContractItem;
	
	private CtrContractTop contractTop;
	private CtrContractPb contractPb;
	private List<CtrContractTop> listContractTop;
	private List<CtrContractPb> listContractPb;
	private CtrContractComment ctrContractComment;
	private List<CtrContractComment> listContractComment;
	
	private AdmPurchasingOrganization admPurchOrg;

	private List<AdmPurchasingOrganization> listPurchasingOrganization;
	
	public List<PrcMainHeader> getListPrcMainHeader() {
		return listPrcMainHeader;
	}

	public void setListPrcMainHeader(List<PrcMainHeader> listPrcMainHeader) {
		this.listPrcMainHeader = listPrcMainHeader;
	}

	public PrcMainHeader getPrcMainHeader() {
		return prcMainHeader;
	}

	public void setPrcMainHeader(PrcMainHeader prcMainHeader) {
		this.prcMainHeader = prcMainHeader;
	}

	public AdmEmployee getAdmEmployee() {
		return admEmployee;
	}

	public void setAdmEmployee(AdmEmployee admEmployee) {
		this.admEmployee = admEmployee;
	}

	public List<HistMainComment> getListHistMainComment() {
		return listHistMainComment;
	}

	public void setListHistMainComment(List<HistMainComment> listHistMainComment) {
		this.listHistMainComment = listHistMainComment;
	}

	public List<PrcMainItem> getListPrcMainItem() {
		return listPrcMainItem;
	}

	public void setListPrcMainItem(List<PrcMainItem> listPrcMainItem) {
		this.listPrcMainItem = listPrcMainItem;
	}

	public HistMainComment getHistMainComment() {
		return histMainComment;
	}

	public void setHistMainComment(HistMainComment histMainComment) {
		this.histMainComment = histMainComment;
	}

	public File getDocsKomentar() {
		return docsKomentar;
	}

	public void setDocsKomentar(File docsKomentar) {
		this.docsKomentar = docsKomentar;
	}

	public String getDocsKomentarFileName() {
		return docsKomentarFileName;
	}

	public void setDocsKomentarFileName(String docsKomentarFileName) {
		this.docsKomentarFileName = docsKomentarFileName;
	}

	public String getDocsKomentarContentType() {
		return docsKomentarContentType;
	}

	public void setDocsKomentarContentType(String docsKomentarContentType) {
		this.docsKomentarContentType = docsKomentarContentType;
	}

	public List<AdmUser> getListPembeli() {
		return listPembeli;
	}

	public void setListPembeli(List<AdmUser> listPembeli) {
		this.listPembeli = listPembeli;
	}

	public List<AdmUser> getListStaffPembeli() {
		return listStaffPembeli;
	}

	public void setListStaffPembeli(List<AdmUser> listStaffPembeli) {
		this.listStaffPembeli = listStaffPembeli;
	}

	public PanitiaPengadaan getPanitia() {
		return panitia;
	}

	public void setPanitia(PanitiaPengadaan panitia) {
		this.panitia = panitia;
	}

	public PanitiaPengadaanDetail getPanitiaDetail() {
		return panitiaDetail;
	}

	public void setPanitiaDetail(PanitiaPengadaanDetail panitiaDetail) {
		this.panitiaDetail = panitiaDetail;
	}

	public List<PanitiaPengadaanDetail> getListPanitiaDetail() {
		return listPanitiaDetail;
	}

	public void setListPanitiaDetail(
			List<PanitiaPengadaanDetail> listPanitiaDetail) {
		this.listPanitiaDetail = listPanitiaDetail;
	}

	public List<AdmUser> getListUser() {
		return listUser;
	}

	public void setListUser(List<AdmUser> listUser) {
		this.listUser = listUser;
	}

	public List<PrcMethod> getListMetode() {
		return listMetode;
	}

	public void setListMetode(List<PrcMethod> listMetode) {
		this.listMetode = listMetode;
	}

	public List<PrcTemplate> getListTemplate() {
		return listTemplate;
	}

	public void setListTemplate(List<PrcTemplate> listTemplate) {
		this.listTemplate = listTemplate;
	}

	public List<VndHeader> getListMitraKerja() {
		return listMitraKerja;
	}

	public void setListMitraKerja(List<VndHeader> listMitraKerja) {
		this.listMitraKerja = listMitraKerja;
	}

	public List<PrcMainVendor> getListPrcMainVendor() {
		return listPrcMainVendor;
	}

	public void setListPrcMainVendor(List<PrcMainVendor> listPrcMainVendor) {
		this.listPrcMainVendor = listPrcMainVendor;
	}

	public PrcTemplate getPrcTemplate() {
		return prcTemplate;
	}

	public void setPrcTemplate(PrcTemplate prcTemplate) {
		this.prcTemplate = prcTemplate;
	}

	public List<File> getPpdDocument() {
		return ppdDocument;
	}

	public void setPpdDocument(List<File> ppdDocument) {
		this.ppdDocument = ppdDocument;
	}

	public List<String> getPpdCategory() {
		return ppdCategory;
	}

	public void setPpdCategory(List<String> ppdCategory) {
		this.ppdCategory = ppdCategory;
	}

	public List<String> getPpdDescription() {
		return ppdDescription;
	}

	public void setPpdDescription(List<String> ppdDescription) {
		this.ppdDescription = ppdDescription;
	}

	public List<String> getPpdDocumentFileName() {
		return ppdDocumentFileName;
	}

	public void setPpdDocumentFileName(List<String> ppdDocumentFileName) {
		this.ppdDocumentFileName = ppdDocumentFileName;
	}

	public PrcMainPreparation getPrcMainPreparation() {
		return prcMainPreparation;
	}

	public void setPrcMainPreparation(PrcMainPreparation prcMainPreparation) {
		this.prcMainPreparation = prcMainPreparation;
	}

	public List<PrcMainVendor> getListVendor() {
		return listVendor;
	}

	public void setListVendor(List<PrcMainVendor> listVendor) {
		this.listVendor = listVendor;
	}

	public List<PrcMainDoc> getListPrcMainDoc() {
		return listPrcMainDoc;
	}

	public void setListPrcMainDoc(List<PrcMainDoc> listPrcMainDoc) {
		this.listPrcMainDoc = listPrcMainDoc;
	}

	public List<PrcMainVendorQuoteAdmtech> getListAdministrasi() {
		return listAdministrasi;
	}

	public void setListAdministrasi(List<PrcMainVendorQuoteAdmtech> listAdministrasi) {
		this.listAdministrasi = listAdministrasi;
	}

	public List<PrcMainVendorQuoteAdmtech> getListTeknis() {
		return listTeknis;
	}

	public void setListTeknis(List<PrcMainVendorQuoteAdmtech> listTeknis) {
		this.listTeknis = listTeknis;
	}

	public PrcMainVendorQuote getQuote() {
		return quote;
	}

	public void setQuote(PrcMainVendorQuote quote) {
		this.quote = quote;
	}

	public List<PrcMainVendorQuoteItem> getListQuoteItem() {
		return listQuoteItem;
	}

	public void setListQuoteItem(List<PrcMainVendorQuoteItem> listQuoteItem) {
		this.listQuoteItem = listQuoteItem;
	}

	public PrcMainVendor getPrcMainVendor() {
		return prcMainVendor;
	}

	public void setPrcMainVendor(PrcMainVendor prcMainVendor) {
		this.prcMainVendor = prcMainVendor;
	}

	public List<TabulasiPenilaian> getListTabulasi() {
		return listTabulasi;
	}

	public void setListTabulasi(List<TabulasiPenilaian> listTabulasi) {
		this.listTabulasi = listTabulasi;
	}

	public List<PrcMsgNegotiation> getListNegosiasi() {
		return listNegosiasi;
	}

	public void setListNegosiasi(List<PrcMsgNegotiation> listNegosiasi) {
		this.listNegosiasi = listNegosiasi;
	}

	public PrcMsgNegotiation getNegosiasi() {
		return negosiasi;
	}

	public void setNegosiasi(PrcMsgNegotiation negosiasi) {
		this.negosiasi = negosiasi;
	}

	public CtrContractHeader getContractHeader() {
		return contractHeader;
	}

	public void setContractHeader(CtrContractHeader contractHeader) {
		this.contractHeader = contractHeader;
	}

	public List<CtrContractHeader> getListContractHeader() {
		return listContractHeader;
	}

	public void setListContractHeader(List<CtrContractHeader> listContractHeader) {
		this.listContractHeader = listContractHeader;
	}

	public List<CtrContractTop> getListContractTop() {
		return listContractTop;
	}

	public void setListContractTop(List<CtrContractTop> listContractTop) {
		this.listContractTop = listContractTop;
	}

	public List<CtrContractPb> getListContractPb() {
		return listContractPb;
	}

	public void setListContractPb(List<CtrContractPb> listContractPb) {
		this.listContractPb = listContractPb;
	}

	public CtrContractComment getCtrContractComment() {
		return ctrContractComment;
	}

	public void setCtrContractComment(CtrContractComment ctrContractComment) {
		this.ctrContractComment = ctrContractComment;
	}

	public List<CtrContractComment> getListContractComment() {
		return listContractComment;
	}

	public void setListContractComment(List<CtrContractComment> listContractComment) {
		this.listContractComment = listContractComment;
	}

	public List<CtrContractItem> getListContractItem() {
		return listContractItem;
	}

	public void setListContractItem(List<CtrContractItem> listContractItem) {
		this.listContractItem = listContractItem;
	}

	public CtrContractTop getContractTop() {
		return contractTop;
	}

	public void setContractTop(CtrContractTop contractTop) {
		this.contractTop = contractTop;
	}

	public CtrContractPb getContractPb() {
		return contractPb;
	}

	public void setContractPb(CtrContractPb contractPb) {
		this.contractPb = contractPb;
	}

	public List<PrcMainVendorQuote> getListQuote() {
		return listQuote;
	}

	public void setListQuote(List<PrcMainVendorQuote> listQuote) {
		this.listQuote = listQuote;
	}

	public CtrContractHeader getContractHeaderAdendum() {
		return contractHeaderAdendum;
	}

	public void setContractHeaderAdendum(CtrContractHeader contractHeaderAdendum) {
		this.contractHeaderAdendum = contractHeaderAdendum;
	}

	public POHeader getPoHeader() {
		return poHeader;
	}

	public void setPoHeader(POHeader poHeader) {
		this.poHeader = poHeader;
	}

	public List<PODetail> getListPoDetail() {
		return listPoDetail;
	}

	public void setListPoDetail(List<PODetail> listPoDetail) {
		this.listPoDetail = listPoDetail;
	}


	public String getIdKomponen() {
		return idKomponen;
	}

	public void setIdKomponen(String idKomponen) {
		this.idKomponen = idKomponen;
	}

	public List<MainMasterJdeCommand> getListJdeMasterMain() {
		return listJdeMasterMain;
	}

	public void setListJdeMasterMain(List<MainMasterJdeCommand> listJdeMasterMain) {
		this.listJdeMasterMain = listJdeMasterMain;
	}

	public String getJdeSearch() {
		return jdeSearch;
	}

	public void setJdeSearch(String jdeSearch) {
		this.jdeSearch = jdeSearch;
	}

	public String getJdeUser() {
		return jdeUser;
	}

	public void setJdeUser(String jdeUser) {
		this.jdeUser = jdeUser;
	}

	public String getJdePassword() {
		return jdePassword;
	}

	public void setJdePassword(String jdePassword) {
		this.jdePassword = jdePassword;
	}

	public Double getHargaTerendah() {
		return hargaTerendah;
	}

	public void setHargaTerendah(Double hargaTerendah) {
		this.hargaTerendah = hargaTerendah;
	}

	public Integer getPeriode() {
		return periode;
	}

	public void setPeriode(Integer periode) {
		this.periode = periode;
	}

	public List<HistoryHargaNegosiasi> getListHargaNegosiasi() {
		return listHargaNegosiasi;
	}

	public void setListHargaNegosiasi(List<HistoryHargaNegosiasi> listHargaNegosiasi) {
		this.listHargaNegosiasi = listHargaNegosiasi;
	}

	public List<PrcMainVendor> getListVendorLulus() {
		return listVendorLulus;
	}

	public void setListVendorLulus(List<PrcMainVendor> listVendorLulus) {
		this.listVendorLulus = listVendorLulus;
	}

	public List<PrcMainVendor> getListVendorTidakLulus() {
		return listVendorTidakLulus;
	}

	public void setListVendorTidakLulus(List<PrcMainVendor> listVendorTidakLulus) {
		this.listVendorTidakLulus = listVendorTidakLulus;
	}

	public List<List<PrcMainVendor>> getListVendorUsulanPemenang() {
		return listVendorUsulanPemenang;
	}

	public void setListVendorUsulanPemenang(List<List<PrcMainVendor>> listVendorUsulanPemenang) {
		this.listVendorUsulanPemenang = listVendorUsulanPemenang;
	}

	public List<AdmPurchasingOrganization> getListPurchasingOrganization() {
		return listPurchasingOrganization;
	}

	public void setListPurchasingOrganization(List<AdmPurchasingOrganization> listPurchasingOrganization) {
		this.listPurchasingOrganization = listPurchasingOrganization;
	}

	public String getKodePurcOrg() {
		return kodePurcOrg;
	}

	public void setKodePurcOrg(String kodePurcOrg) {
		this.kodePurcOrg = kodePurcOrg;
	}

	public AdmPurchasingOrganization getAdmPurchOrg() {
		return admPurchOrg;
	}

	public void setAdmPurchOrg(AdmPurchasingOrganization admPurchOrg) {
		this.admPurchOrg = admPurchOrg;
	}

	public List<AdmCurrency> getListCurrency() {
		return listCurrency;
	}

	public void setListCurrency(List<AdmCurrency> listCurrency) {
		this.listCurrency = listCurrency;
	}

	public AdmCurrency getCurr() {
		return curr;
	}

	public void setCurr(AdmCurrency curr) {
		this.curr = curr;
	}

	
}
