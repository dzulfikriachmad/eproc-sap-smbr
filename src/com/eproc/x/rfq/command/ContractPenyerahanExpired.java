package com.eproc.x.rfq.command;

import java.io.Serializable;

public class ContractPenyerahanExpired implements Serializable {

	private String nomorKontrak;
	private String judulKontrak;
	private String deskripsiKontrak;
	private String tanggalPenyerahan;
	private String mitraKerja;
	private String email;
	public ContractPenyerahanExpired() {
		// TODO Auto-generated constructor stub
	}
	public String getNomorKontrak() {
		return nomorKontrak;
	}
	public void setNomorKontrak(String nomorKontrak) {
		this.nomorKontrak = nomorKontrak;
	}
	public String getJudulKontrak() {
		return judulKontrak;
	}
	public void setJudulKontrak(String judulKontrak) {
		this.judulKontrak = judulKontrak;
	}
	public String getDeskripsiKontrak() {
		return deskripsiKontrak;
	}
	public void setDeskripsiKontrak(String deskripsiKontrak) {
		this.deskripsiKontrak = deskripsiKontrak;
	}
	public String getTanggalPenyerahan() {
		return tanggalPenyerahan;
	}
	public void setTanggalPenyerahan(String tanggalPenyerahan) {
		this.tanggalPenyerahan = tanggalPenyerahan;
	}
	public String getMitraKerja() {
		return mitraKerja;
	}
	public void setMitraKerja(String mitraKerja) {
		this.mitraKerja = mitraKerja;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
}
