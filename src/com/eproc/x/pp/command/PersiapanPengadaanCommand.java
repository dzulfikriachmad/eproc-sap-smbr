package com.eproc.x.pp.command;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.eproc.x.rfq.command.MainMasterJdeCommand;
import com.orm.model.AdmCostCenter;
import com.orm.model.AdmEmployee;
import com.orm.model.AdmJenis;
import com.orm.model.AdmUser;
import com.orm.model.ComGroup;
import com.orm.model.HistMainComment;
import com.orm.model.PanitiaPengadaan;
import com.orm.model.PanitiaPengadaanDetail;
import com.orm.model.PrcAnggaranJdeDetail;
import com.orm.model.PrcAnggaranJdeHeader;
import com.orm.model.PrcMainDoc;
import com.orm.model.PrcMainHeader;
import com.orm.model.PrcMainItem;
import com.orm.model.PrcMainPreparation;
import com.orm.model.PrcMainVendor;
import com.orm.model.PrcMethod;
import com.orm.model.PrcTemplate;
import com.orm.model.VndHeader;
import com.orm.tools.AbstractCommand;

public class PersiapanPengadaanCommand extends AbstractCommand {

	private List<PrcMainHeader> listPrcMainHeader;
	private List<AdmUser> listPembeli;
	private List<AdmUser> listStaffPembeli;
	private List<HistMainComment> listHistMainComment;
	private List<PrcMainItem> listPrcMainItem;
	private List<AdmCostCenter> listCostCenter;
	private List<MainMasterJdeCommand> listJdeMasterMain;
	private Integer tahunAnggaran;
	private String kodeAnggaran;
	
	private PrcAnggaranJdeHeader anggaranJdeHeader;
	private PrcAnggaranJdeDetail anggaranJdeDetail;
	private List<PrcAnggaranJdeDetail> listAnggaranJdeDetail;
	
	private PanitiaPengadaan panitia;
	private PanitiaPengadaanDetail panitiaDetail;
	private List<PanitiaPengadaanDetail> listPanitiaDetail;
	private List<AdmUser> listUser;
	private List<PrcMethod> listMetode;
	private List<PrcTemplate> listTemplate;
	private List<VndHeader> listMitraKerja;
	private List<PrcMainVendor> listPrcMainVendor = new ArrayList<PrcMainVendor>();
	private PrcTemplate prcTemplate;
	private List<File> ppdDocument;
	private List<String> ppdDocumentFileName;
	private List<String> ppdCategory;
	private List<String> ppdDescription;
	private PrcMainPreparation prcMainPreparation;
	private List<PrcMainDoc> listPrcMainDoc;
	private List<PrcMainVendor> listVendor;
	private List<PrcMainVendor> listVendorLulusPq;
	private List<PrcMainVendor> listVendorTidakLulusPq;

	
	private PrcMainHeader prcMainHeader;
	private AdmEmployee admEmployee;
	private HistMainComment histMainComment;
	private List<ComGroup> listGroup;
	
	private File docsKomentar;
	private String docsKomentarFileName;
	private String docsKomentarContentType;
	
	private List<AdmJenis> listAdmJenis;

	public List<ComGroup> getListGroup() {
		return listGroup;
	}

	public void setListGroup(List<ComGroup> listGroup) {
		this.listGroup = listGroup;
	}

	public List<PrcMainHeader> getListPrcMainHeader() {
		return listPrcMainHeader;
	}

	public void setListPrcMainHeader(List<PrcMainHeader> listPrcMainHeader) {
		this.listPrcMainHeader = listPrcMainHeader;
	}

	public PrcMainHeader getPrcMainHeader() {
		return prcMainHeader;
	}

	public void setPrcMainHeader(PrcMainHeader prcMainHeader) {
		this.prcMainHeader = prcMainHeader;
	}

	public AdmEmployee getAdmEmployee() {
		return admEmployee;
	}

	public void setAdmEmployee(AdmEmployee admEmployee) {
		this.admEmployee = admEmployee;
	}

	public List<HistMainComment> getListHistMainComment() {
		return listHistMainComment;
	}

	public void setListHistMainComment(List<HistMainComment> listHistMainComment) {
		this.listHistMainComment = listHistMainComment;
	}

	public List<PrcMainItem> getListPrcMainItem() {
		return listPrcMainItem;
	}

	public void setListPrcMainItem(List<PrcMainItem> listPrcMainItem) {
		this.listPrcMainItem = listPrcMainItem;
	}

	public HistMainComment getHistMainComment() {
		return histMainComment;
	}

	public void setHistMainComment(HistMainComment histMainComment) {
		this.histMainComment = histMainComment;
	}

	public File getDocsKomentar() {
		return docsKomentar;
	}

	public void setDocsKomentar(File docsKomentar) {
		this.docsKomentar = docsKomentar;
	}

	public String getDocsKomentarFileName() {
		return docsKomentarFileName;
	}

	public void setDocsKomentarFileName(String docsKomentarFileName) {
		this.docsKomentarFileName = docsKomentarFileName;
	}

	public String getDocsKomentarContentType() {
		return docsKomentarContentType;
	}

	public void setDocsKomentarContentType(String docsKomentarContentType) {
		this.docsKomentarContentType = docsKomentarContentType;
	}

	public List<AdmUser> getListPembeli() {
		return listPembeli;
	}

	public void setListPembeli(List<AdmUser> listPembeli) {
		this.listPembeli = listPembeli;
	}

	public List<AdmUser> getListStaffPembeli() {
		return listStaffPembeli;
	}

	public void setListStaffPembeli(List<AdmUser> listStaffPembeli) {
		this.listStaffPembeli = listStaffPembeli;
	}

	public PanitiaPengadaan getPanitia() {
		return panitia;
	}

	public void setPanitia(PanitiaPengadaan panitia) {
		this.panitia = panitia;
	}

	public PanitiaPengadaanDetail getPanitiaDetail() {
		return panitiaDetail;
	}

	public void setPanitiaDetail(PanitiaPengadaanDetail panitiaDetail) {
		this.panitiaDetail = panitiaDetail;
	}

	public List<PanitiaPengadaanDetail> getListPanitiaDetail() {
		return listPanitiaDetail;
	}

	public void setListPanitiaDetail(List<PanitiaPengadaanDetail> listPanitiaDetail) {
		this.listPanitiaDetail = listPanitiaDetail;
	}

	public List<AdmUser> getListUser() {
		return listUser;
	}

	public void setListUser(List<AdmUser> listUser) {
		this.listUser = listUser;
	}

	public List<PrcMethod> getListMetode() {
		return listMetode;
	}

	public void setListMetode(List<PrcMethod> listMetode) {
		this.listMetode = listMetode;
	}

	public List<PrcTemplate> getListTemplate() {
		return listTemplate;
	}

	public void setListTemplate(List<PrcTemplate> listTemplate) {
		this.listTemplate = listTemplate;
	}

	public List<VndHeader> getListMitraKerja() {
		return listMitraKerja;
	}

	public void setListMitraKerja(List<VndHeader> listMitraKerja) {
		this.listMitraKerja = listMitraKerja;
	}

	public List<PrcMainVendor> getListPrcMainVendor() {
		return listPrcMainVendor;
	}

	public void setListPrcMainVendor(List<PrcMainVendor> listPrcMainVendor) {
		this.listPrcMainVendor = listPrcMainVendor;
	}

	public PrcTemplate getPrcTemplate() {
		return prcTemplate;
	}

	public void setPrcTemplate(PrcTemplate prcTemplate) {
		this.prcTemplate = prcTemplate;
	}

	public List<File> getPpdDocument() {
		return ppdDocument;
	}

	public void setPpdDocument(List<File> ppdDocument) {
		this.ppdDocument = ppdDocument;
	}

	public List<String> getPpdCategory() {
		return ppdCategory;
	}

	public void setPpdCategory(List<String> ppdCategory) {
		this.ppdCategory = ppdCategory;
	}

	public List<String> getPpdDescription() {
		return ppdDescription;
	}

	public void setPpdDescription(List<String> ppdDescription) {
		this.ppdDescription = ppdDescription;
	}

	public List<String> getPpdDocumentFileName() {
		return ppdDocumentFileName;
	}

	public void setPpdDocumentFileName(List<String> ppdDocumentFileName) {
		this.ppdDocumentFileName = ppdDocumentFileName;
	}
	
	public PrcMainPreparation getPrcMainPreparation() {
		return prcMainPreparation;
	}

	public void setPrcMainPreparation(PrcMainPreparation prcMainPreparation) {
		this.prcMainPreparation = prcMainPreparation;
	}

	public List<PrcMainVendor> getListVendor() {
		return listVendor;
	}

	public void setListVendor(List<PrcMainVendor> listVendor) {
		this.listVendor = listVendor;
	}

	public List<PrcMainDoc> getListPrcMainDoc() {
		return listPrcMainDoc;
	}

	public void setListPrcMainDoc(List<PrcMainDoc> listPrcMainDoc) {
		this.listPrcMainDoc = listPrcMainDoc;
	}

	public PrcAnggaranJdeHeader getAnggaranJdeHeader() {
		return anggaranJdeHeader;
	}

	public void setAnggaranJdeHeader(PrcAnggaranJdeHeader anggaranJdeHeader) {
		this.anggaranJdeHeader = anggaranJdeHeader;
	}

	public List<PrcAnggaranJdeDetail> getListAnggaranJdeDetail() {
		return listAnggaranJdeDetail;
	}

	public void setListAnggaranJdeDetail(
			List<PrcAnggaranJdeDetail> listAnggaranJdeDetail) {
		this.listAnggaranJdeDetail = listAnggaranJdeDetail;
	}

	public PrcAnggaranJdeDetail getAnggaranJdeDetail() {
		return anggaranJdeDetail;
	}

	public void setAnggaranJdeDetail(PrcAnggaranJdeDetail anggaranJdeDetail) {
		this.anggaranJdeDetail = anggaranJdeDetail;
	}

	public List<AdmCostCenter> getListCostCenter() {
		return listCostCenter;
	}

	public void setListCostCenter(List<AdmCostCenter> listCostCenter) {
		this.listCostCenter = listCostCenter;
	}

	public List<MainMasterJdeCommand> getListJdeMasterMain() {
		return listJdeMasterMain;
	}

	public void setListJdeMasterMain(List<MainMasterJdeCommand> listJdeMasterMain) {
		this.listJdeMasterMain = listJdeMasterMain;
	}

	public Integer getTahunAnggaran() {
		return tahunAnggaran;
	}

	public void setTahunAnggaran(Integer tahunAnggaran) {
		this.tahunAnggaran = tahunAnggaran;
	}

	public String getKodeAnggaran() {
		return kodeAnggaran;
	}

	public void setKodeAnggaran(String kodeAnggaran) {
		this.kodeAnggaran = kodeAnggaran;
	}

	public List<PrcMainVendor> getListVendorLulusPq() {
		return listVendorLulusPq;
	}

	public void setListVendorLulusPq(List<PrcMainVendor> listVendorLulusPq) {
		this.listVendorLulusPq = listVendorLulusPq;
	}

	public List<PrcMainVendor> getListVendorTidakLulusPq() {
		return listVendorTidakLulusPq;
	}

	public void setListVendorTidakLulusPq(List<PrcMainVendor> listVendorTidakLulusPq) {
		this.listVendorTidakLulusPq = listVendorTidakLulusPq;
	}

	public List<AdmJenis> getListAdmJenis() {
		return listAdmJenis;
	}

	public void setListAdmJenis(List<AdmJenis> listAdmJenis) {
		this.listAdmJenis = listAdmJenis;
	}
}
