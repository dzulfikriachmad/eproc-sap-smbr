package com.eproc.x.pp.action;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.web.util.WebUtils;

import com.eproc.core.BaseController;
import com.eproc.utils.SessionGetter;
import com.eproc.x.pp.command.PersiapanPengadaanCommand;
import com.orm.model.AdmEmployee;
import com.orm.model.PanitiaPengadaan;
import com.orm.model.PanitiaPengadaanDetail;
import com.orm.model.PrcMainHeader;
import com.orm.model.PrcMainItem;
import com.orm.model.PrcMainVendor;

import eproc.controller.AdmServiceImpl;
import eproc.controller.PrcService;
import eproc.controller.PrcServiceImpl;
import eproc.controller.RfqService;
import eproc.controller.RfqServiceImpl;

@SuppressWarnings("serial")
@Namespace("/")
@ParentPackage("eproc")
public class PersiapanPengadaanAction extends BaseController {

	private PersiapanPengadaanCommand model;
	private PrcService prcService;
	private RfqService rfqService;

	@Override
	public Object getModel() {
		return model;
	}

	@Override
	public void prepare() throws Exception {
		model = new PersiapanPengadaanCommand();
		prcService = new PrcServiceImpl();
		rfqService = new RfqServiceImpl();
	}

	@Action(results = { @Result(name = SUCCESS, location = "proc/pp/listPraPpDisposisi.jsp"),
			@Result(name = "view", location = "proc/pp/viewPraPpDisposisi.jsp"),
			// @Result(name="ok",location="pp/disposisiPraPP",type="redirectAction")
			@Result(name = "ok", location = "dashboard", type = "redirectAction") }, value = "pp/disposisiPraPP")
	public String disposisiPengawas() throws Exception {
		try {
			if (WebUtils.hasSubmitParameter(request, "proses")) {
				PrcMainHeader prcMainHeader = prcService.getPrcMainIHeaderById(model.getPrcMainHeader().getPpmId());
				AdmEmployee admEmployee = SessionGetter.getUser().getAdmEmployee();
				model.setPrcMainHeader(prcMainHeader);
				model.setAdmEmployee(admEmployee);
				model.setListHistMainComment(prcService
						.listHistMainCommentByPrcHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId())));
				List<PrcMainItem> listItem = prcService
						.listPrcMainItemByHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId()));
				model.setListPrcMainItem(listItem);
				model.setListPembeli(prcService.listUserByProsesLevel(prcMainHeader));
				return "view";
			} else if (WebUtils.hasSubmitParameter(request, "saveAndFinish")) {
				model.getHistMainComment().setAksi("Disposisi");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("pp.1", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("pp.2", null));
				prcService.saveDisposisiPraPP(model.getPrcMainHeader(), model.getHistMainComment(),
						model.getDocsKomentar(), model.getDocsKomentarFileName());
				addActionMessage("Data Sudah Disimpan");
			}
			model.setListPrcMainHeader(prcService.listPrcMainHeaderDisposisiPp(null));
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
			return "ok";
		}
		return SUCCESS;
	}

	@Action(results = { @Result(name = SUCCESS, location = "proc/pp/listPpDisposisi.jsp"),
			@Result(name = "view", location = "proc/pp/viewPpDisposisi.jsp"),
			// @Result(name="ok",location="pp/disposisi",type="redirectAction")
			@Result(name = "ok", location = "dashboard", type = "redirectAction") }, value = "pp/disposisi")
	public String disposisiPP() throws Exception {
		try {
			if (WebUtils.hasSubmitParameter(request, "proses")) {
				PrcMainHeader prcMainHeader = prcService.getPrcMainIHeaderById(model.getPrcMainHeader().getPpmId());
				AdmEmployee admEmployee = SessionGetter.getUser().getAdmEmployee();
				model.setPrcMainHeader(prcMainHeader);
				model.setAdmEmployee(admEmployee);
				model.setListHistMainComment(prcService
						.listHistMainCommentByPrcHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId())));
				List<PrcMainItem> listItem = prcService
						.listPrcMainItemByHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId()));
				model.setListPrcMainItem(listItem);
				model.setListAdmJenis(prcService.getListAdmJenisByParent(model.getPrcMainHeader().getPpmJenis()));
				model.setListPembeli(prcService.listBuyer(prcMainHeader));
				return "view";
			} else if (WebUtils.hasSubmitParameter(request, "saveAndFinish")) {
				model.getHistMainComment().setAksi("Disposisi");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("pp.2", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("pp.3", null));
				prcService.saveDisposisiPP(model.getPrcMainHeader(), model.getHistMainComment(),
						model.getDocsKomentar(), model.getDocsKomentarFileName());
				addActionMessage("Data Sudah Disimpan");
			}
			model.setListPrcMainHeader(prcService.listPrcMainHeaderDisposisiPp(null));
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
			return "ok";
		}
		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	@Action(results = { @Result(name = SUCCESS, location = "proc/pp/listPpSetup.jsp"),
			@Result(name = "view", location = "proc/pp/setPpSetup.jsp"),
			// @Result(name="ok",location="pp/setup",type="redirectAction")
			@Result(name = "ok", location = "dashboard", type = "redirectAction") }, value = "pp/setup")
	public String setupPP() throws Exception {
		try {
			if (WebUtils.hasSubmitParameter(request, "proses")) {
				PrcMainHeader prcMainHeader = prcService.getPrcMainIHeaderById(model.getPrcMainHeader().getPpmId());
				AdmEmployee admEmployee = SessionGetter.getUser().getAdmEmployee();
				model.setPrcMainHeader(prcMainHeader);
				model.setAdmEmployee(admEmployee);
				model.setListHistMainComment(prcService
						.listHistMainCommentByPrcHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId())));
				List<PrcMainItem> listItem = prcService
						.listPrcMainItemByHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId()));
				model.setListPrcMainItem(listItem);
				model.setListStaffPembeli(prcService.listUserByProsesLevel(prcMainHeader));
				model.setListUser(new AdmServiceImpl().getListUser(null));
				model.setListMetode(prcService.listMetode(null));
				return "view";
			} else if (WebUtils.hasSubmitParameter(request, "saveAndFinish")) {
				model.getHistMainComment().setAksi("Persiapan Pengadaan");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("pp.3", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("pp.4", null));
				if (model.getPanitia() != null && SessionGetter.getSessionValue("listPanitiaDetail") != null) {
					List<PanitiaPengadaanDetail> listPanitiaDetail = (List<PanitiaPengadaanDetail>) SessionGetter
							.getSessionValue("listPanitiaDetail");
					prcService.savePpSetup(model.getPrcMainHeader(), model.getHistMainComment(),
							model.getDocsKomentar(), model.getDocsKomentarFileName(), model.getPanitia(),
							listPanitiaDetail);
				} else {
					prcService.savePpSetup(model.getPrcMainHeader(), model.getHistMainComment(),
							model.getDocsKomentar(), model.getDocsKomentarFileName(), null, null);
				}
				SessionGetter.destroySessionValue("listPanitiaDetail");
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
			return "ok";
		}
		model.setListPrcMainHeader(prcService.listPrcMainHeaderPpSetup(SessionGetter.getUser()));
		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	@Action(results = {
			@Result(name = SUCCESS, location = "proc/pp/ajax/listPanitiaDetail.jsp") }, value = "ajax/submitPanitiaDetail")
	public String ajaxsubmitPanitiadetail() throws Exception {
		List<PanitiaPengadaanDetail> listPanitiaDetail = (List<PanitiaPengadaanDetail>) SessionGetter
				.getSessionValue("listPanitiaDetail");
		List<PanitiaPengadaanDetail> retListPanitiaDetail = new ArrayList<PanitiaPengadaanDetail>();

		if (request.getParameter("index") != null && request.getParameter("index") != ""
				&& request.getParameter("act") != null && request.getParameter("act").equals("delete")) {

			int n = 0;
			int target = Integer.parseInt(request.getParameter("index"));
			if (listPanitiaDetail != null) {
				for (PanitiaPengadaanDetail i : listPanitiaDetail) {
					if (n != target) {
						retListPanitiaDetail.add(i);
					}
					n = n + 1;
				}
			}
			SessionGetter.setSessionValue(retListPanitiaDetail, "listPanitiaDetail");

			model.setListPanitiaDetail(retListPanitiaDetail);
			return SUCCESS;
		}

		if (listPanitiaDetail != null) {
			for (PanitiaPengadaanDetail i : listPanitiaDetail) {
				retListPanitiaDetail.add(i);
			}
		}
		model.getPanitiaDetail().setAnggota(new AdmServiceImpl().getUserById(model.getPanitiaDetail().getAnggota()));
		retListPanitiaDetail.add(model.getPanitiaDetail());
		SessionGetter.setSessionValue(retListPanitiaDetail, "listPanitiaDetail");
		model.setListPanitiaDetail(retListPanitiaDetail);
		return SUCCESS;
	}

	@Action(results = { @Result(name = SUCCESS, location = "proc/pp/listPpVendor.jsp"),
			@Result(name = "view", location = "proc/pp/setPpVendor.jsp"),
			// @Result(name="ok",location="pp/vendor",type="redirectAction")
			@Result(name = "ok", location = "dashboard", type = "redirectAction") }, value = "pp/vendor")
	public String setupVendor() throws Exception {
		try {
			if (WebUtils.hasSubmitParameter(request, "proses")) {
				PrcMainHeader prcMainHeader = prcService.getPrcMainHeaderSPPHById(model.getPrcMainHeader().getPpmId());
				prcMainHeader.setKecil(1);
				prcMainHeader.setMenengah(1);
				AdmEmployee admEmployee = SessionGetter.getUser().getAdmEmployee();
				model.setPrcMainHeader(prcMainHeader);
				model.setAdmEmployee(admEmployee);
				model.setListHistMainComment(prcService
						.listHistMainCommentByPrcHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId())));
				List<PrcMainItem> listItem = prcService
						.listPrcMainItemByHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId()));
				model.setListPrcMainItem(listItem);
				model.setListTemplate(prcService.listTemplate(null));
				// model.setListMitraKerja(prcService.listMitraKerjaDiUndang(prcMainHeader));
				if (prcMainHeader.getPanitia() != null) {
					model.setPanitia(prcService.getPanitiaPengadaan(prcMainHeader.getPanitia()));
					model.setListPanitiaDetail(prcService.getListPanitiaPengadaanDetail(prcMainHeader.getPanitia()));
				}
				model.setListMetode(prcService.listMetode(null));
				model.setListGroup(prcService.getListComGrup(null));
				return "view";
			} else if (WebUtils.hasSubmitParameter(request, "saveAndFinish")) {
				model.getHistMainComment().setAksi("Persiapan Pengadaan");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("pp.4", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("pp.5", null));
				prcService.savePpMitraKerja(model.getPrcMainHeader(), model.getHistMainComment(),
						model.getListPrcMainVendor(), model.getDocsKomentar(), model.getDocsKomentarFileName(),
						model.getPpdDocument(), model.getPpdCategory(), model.getPpdDescription(),
						model.getPrcMainPreparation(), model.getPpdDocumentFileName());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
		}
		model.setListPrcMainHeader(prcService.listPrcMainHeaderPpVendor(null));
		return SUCCESS;
	}

	@Action(results = {
			@Result(name = SUCCESS, location = "proc/pp/ajax/listMitraKerja.jsp") }, value = "ajax/submitMitraKerja")
	public String ajaxSubmitMitraKerja() throws Exception {
		try {
			model.setListMitraKerja(prcService.listMitraKerjaDiUndang(model.getPrcMainHeader()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}

	@Action(results = { @Result(name = SUCCESS, location = "proc/pp/listPpApproval.jsp"),
			@Result(name = "view", location = "proc/pp/viewPpApproval.jsp"),
			// @Result(name="ok",location="pp/approval",type="redirectAction")
			@Result(name = "ok", location = "dashboard", type = "redirectAction") }, value = "pp/approval")
	public String approvalPp() throws Exception {
		try {
			if (WebUtils.hasSubmitParameter(request, "proses")) {
				PrcMainHeader prcMainHeader = prcService.getPrcMainHeaderSPPHById(model.getPrcMainHeader().getPpmId());
				AdmEmployee admEmployee = SessionGetter.getUser().getAdmEmployee();
				model.setPrcMainHeader(prcMainHeader);
				model.setAdmEmployee(admEmployee);
				model.setListHistMainComment(prcService
						.listHistMainCommentByPrcHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId())));
				List<PrcMainItem> listItem = prcService
						.listPrcMainItemByHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId()));
				model.setListPrcMainItem(listItem);
				//model.setListVendor(prcService.listMitraKerjaByHeader(prcMainHeader));
				if (this.model.getPrcMainHeader().getPrcMethod().getId() > 3) {
			          this.model.setListVendor(this.prcService.listMitraKerjaByStatusPq(prcMainHeader));
				} else {
			          this.model.setListVendor(this.prcService.listMitraKerjaByHeader(prcMainHeader));
			    }
				model.setListPrcMainDoc(prcService.listPrcMainDocByHeader(prcMainHeader));
				model.setPrcMainPreparation(prcService.getPrcMainPreparationByHeader(prcMainHeader));
				if (prcMainHeader.getPanitia() != null) {
					model.setPanitia(prcService.getPanitiaPengadaan(prcMainHeader.getPanitia()));
					model.setListPanitiaDetail(prcService.getListPanitiaPengadaanDetail(prcMainHeader.getPanitia()));
				}
				return "view";
			} else if (WebUtils.hasSubmitParameter(request, "revisi")) {
				model.getHistMainComment().setAksi("Revisi");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("pp.5", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("pp.4", null));
				prcService.revisiApprovalPP(model.getPrcMainHeader(), model.getHistMainComment(),
						model.getDocsKomentar(), model.getDocsKomentarFileName());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			} else if (WebUtils.hasSubmitParameter(request, "revisi")) {
				model.getHistMainComment().setAksi("Revisi");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("pp.5", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("pp.4", null));
				prcService.revisiApprovalPP(model.getPrcMainHeader(), model.getHistMainComment(),
						model.getDocsKomentar(), model.getDocsKomentarFileName());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			}else if (WebUtils.hasSubmitParameter(request, "saveAndFinish")) {
				model.getHistMainComment().setAksi("Setuju");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("pp.5", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("pp.5", null));
				prcService.setujuApprovalPP(model.getPrcMainHeader(), model.getHistMainComment(),
						model.getDocsKomentar(), model.getDocsKomentarFileName());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
			return "ok";
		}
		model.setListPrcMainHeader(prcService.listPrcMainHeaderPpApproval(null));
		return SUCCESS;
	}

	@Action(results = { @Result(name = SUCCESS, location = "proc/pp/listPpPanitia.jsp"),
			@Result(name = "view", location = "proc/pp/viewPpPanitia.jsp"),
			// @Result(name="ok",location="pp/panitia",type="redirectAction")
			@Result(name = "ok", location = "dashboard", type = "redirectAction") }, value = "pp/panitia")
	public String approvalPanitiaPp() throws Exception {
		try {
			if (WebUtils.hasSubmitParameter(request, "proses")) {
				PrcMainHeader prcMainHeader = prcService.getPrcMainHeaderSPPHById(model.getPrcMainHeader().getPpmId());
				AdmEmployee admEmployee = SessionGetter.getUser().getAdmEmployee();
				model.setPrcMainHeader(prcMainHeader);
				model.setAdmEmployee(admEmployee);
				model.setListHistMainComment(prcService
						.listHistMainCommentByPrcHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId())));
				List<PrcMainItem> listItem = prcService
						.listPrcMainItemByHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId()));
				model.setListPrcMainItem(listItem);
				//model.setListVendor(prcService.listMitraKerjaByHeader(prcMainHeader));
				if (this.model.getPrcMainHeader().getPrcMethod().getId() > 3) {
			          this.model.setListVendor(this.prcService.listMitraKerjaByStatusPq(prcMainHeader));
				} else {
			          this.model.setListVendor(this.prcService.listMitraKerjaByHeader(prcMainHeader));
			    }
				model.setListPrcMainDoc(prcService.listPrcMainDocByHeader(prcMainHeader));
				model.setPrcMainPreparation(prcService.getPrcMainPreparationByHeader(prcMainHeader));
				if (prcMainHeader.getPanitia() != null) {
					model.setPanitia(prcService.getPanitiaPengadaan(prcMainHeader.getPanitia()));
					model.setListPanitiaDetail(prcService.getListPanitiaPengadaanDetail(prcMainHeader.getPanitia()));
				}
				return "view";
			} else if (WebUtils.hasSubmitParameter(request, "revisi")) {
				model.getHistMainComment().setAksi("Revisi");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("pp.7", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("pp.4", null));
				prcService.revisiApprovalPanitiaPp(model.getPrcMainHeader(), model.getHistMainComment(),
						model.getDocsKomentar(), model.getDocsKomentarFileName());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			} else if (WebUtils.hasSubmitParameter(request, "saveAndFinish")) {
				model.getHistMainComment().setAksi("Setuju");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("pp.7", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("pp.7", null));
				prcService.setujuApprovalPanitiaPP(model.getPrcMainHeader(), model.getHistMainComment(),
						model.getDocsKomentar(), model.getDocsKomentarFileName());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
			return "ok";
		}
		model.setListPrcMainHeader(prcService.listPrcMainHeaderPpPanitiaApproval(null));
		return SUCCESS;
	}

	/** PENGUMUMAN PQ **/
	@Action(results = { @Result(name = SUCCESS, location = "proc/pp/listPpLelang.jsp"),
			@Result(name = "view", location = "proc/pp/viewPpLelang.jsp"),
			// @Result(name="ok",location="pp/pengumumanLelang",type="redirectAction")
			@Result(name = "ok", location = "dashboard", type = "redirectAction") }, value = "pp/pengumumanLelang")
	public String pengumumanLelang() throws Exception {
		try {
			if (WebUtils.hasSubmitParameter(request, "proses")) {
				PrcMainHeader prcMainHeader = prcService.getPrcMainHeaderSPPHById(model.getPrcMainHeader().getPpmId());
				AdmEmployee admEmployee = SessionGetter.getUser().getAdmEmployee();
				model.setPrcMainHeader(prcMainHeader);
				model.setAdmEmployee(admEmployee);
				model.setListHistMainComment(prcService
						.listHistMainCommentByPrcHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId())));
				List<PrcMainItem> listItem = prcService
						.listPrcMainItemByHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId()));
				model.setListPrcMainItem(listItem);
				model.setListVendor(prcService.listMitraKerjaByHeader(prcMainHeader));
				model.setListPrcMainDoc(prcService.listPrcMainDocByHeader(prcMainHeader));
				model.setPrcMainPreparation(prcService.getPrcMainPreparationByHeader(prcMainHeader));
				model.setListUser(prcService.listUserByProsesLevel(prcMainHeader));
				if (prcMainHeader.getPanitia() != null) {
					model.setPanitia(prcService.getPanitiaPengadaan(prcMainHeader.getPanitia()));
					model.setListPanitiaDetail(prcService.getListPanitiaPengadaanDetail(prcMainHeader.getPanitia()));
				}
				return "view";
			} else if (WebUtils.hasSubmitParameter(request, "saveAndFinish")) {
				model.getHistMainComment().setAksi("Pengumuman Lelang");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("pq.1", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("pq.2", null));
				prcService.savePengumumanLelang(model.getPrcMainHeader(), model.getHistMainComment(),
						model.getDocsKomentar(), model.getDocsKomentarFileName());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
			return "ok";
		}
		model.setListPrcMainHeader(prcService.listPrcMainHeaderPpPengumumanLelang(null));
		return SUCCESS;
	}

	/** PQ **/
	@Action(results = { @Result(name = SUCCESS, location = "proc/pp/listPpPq.jsp"),
			@Result(name = "view", location = "proc/pp/viewPpPq.jsp"),
			// @Result(name="ok",location="pp/prakualifikasi",type="redirectAction")
			@Result(name = "ok", location = "dashboard", type = "redirectAction") }, value = "pp/prakualifikasi")
	public String prakualifikasi() throws Exception {
		try {
			if (WebUtils.hasSubmitParameter(request, "proses")) {
				PrcMainHeader prcMainHeader = prcService.getPrcMainHeaderSPPHById(model.getPrcMainHeader().getPpmId());
				AdmEmployee admEmployee = SessionGetter.getUser().getAdmEmployee();
				model.setPrcMainHeader(prcMainHeader);
				model.setAdmEmployee(admEmployee);
				model.setListHistMainComment(prcService
						.listHistMainCommentByPrcHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId())));
				List<PrcMainItem> listItem = prcService
						.listPrcMainItemByHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId()));
				model.setListPrcMainItem(listItem);
				model.setListPrcMainVendor(prcService.listMitraKerjaByHeader(prcMainHeader));
				model.setListPrcMainDoc(prcService.listPrcMainDocByHeader(prcMainHeader));
				model.setPrcMainPreparation(prcService.getPrcMainPreparationByHeader(prcMainHeader));
				if (prcMainHeader.getPanitia() != null) {
					model.setPanitia(prcService.getPanitiaPengadaan(prcMainHeader.getPanitia()));
					model.setListPanitiaDetail(prcService.getListPanitiaPengadaanDetail(prcMainHeader.getPanitia()));
				}
				return "view";
			} else if (WebUtils.hasSubmitParameter(request, "saveAndFinish")) {
				model.getHistMainComment().setAksi("Prakualifikasi");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("pq.2", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("pq.3", null));
				prcService.savePrakualifikasi(model.getPrcMainHeader(), model.getHistMainComment(),
						model.getDocsKomentar(), model.getDocsKomentarFileName(), model.getListPrcMainVendor());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
			return "ok";
		}
		model.setListPrcMainHeader(prcService.listPrcMainHeaderPpPq(null));
		return SUCCESS;
	}

	/** PQ **/
	@Action(results = { @Result(name = SUCCESS, location = "proc/pp/listPpFinalisasiPq.jsp"),
			@Result(name = "view", location = "proc/pp/viewPpFinalisasiPq.jsp"),
			// @Result(name="ok",location="pp/finalisasiPq",type="redirectAction")
			@Result(name = "ok", location = "dashboard", type = "redirectAction") }, value = "pp/finalisasiPq")
	public String finalisasiPq() throws Exception {
		try {
			if (WebUtils.hasSubmitParameter(request, "proses")) {
				PrcMainHeader prcMainHeader = prcService.getPrcMainHeaderSPPHById(model.getPrcMainHeader().getPpmId());
				AdmEmployee admEmployee = SessionGetter.getUser().getAdmEmployee();
				model.setPrcMainHeader(prcMainHeader);
				model.setAdmEmployee(admEmployee);
				model.setListHistMainComment(prcService
						.listHistMainCommentByPrcHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId())));
				List<PrcMainItem> listItem = prcService
						.listPrcMainItemByHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId()));
				model.setListPrcMainItem(listItem);
				model.setListPrcMainVendor(prcService.listMitraKerjaByHeader(prcMainHeader));
				
				model.setListPrcMainDoc(prcService.listPrcMainDocByHeader(prcMainHeader));
				model.setPrcMainPreparation(prcService.getPrcMainPreparationByHeader(prcMainHeader));
				if (prcMainHeader.getPanitia() != null) {
					model.setPanitia(prcService.getPanitiaPengadaan(prcMainHeader.getPanitia()));
					model.setListPanitiaDetail(prcService.getListPanitiaPengadaanDetail(prcMainHeader.getPanitia()));
				}
				return "view";
			} else if (WebUtils.hasSubmitParameter(request, "saveAndFinish")) {
				model.getHistMainComment().setAksi("Finalisasi Prakualifikasi");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("pq.3", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("pq.3", null));
				prcService.finalisasiPq(model.getPrcMainHeader(), model.getHistMainComment(), model.getDocsKomentar(),
						model.getDocsKomentarFileName(), model.getListPrcMainVendor());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			} else if (WebUtils.hasSubmitParameter(request, "revisi")) {
				model.getHistMainComment().setAksi("Revisi");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("pq.3", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("pq.2", null));
				prcService.revisiPq(model.getPrcMainHeader(), model.getHistMainComment(), model.getDocsKomentar(),
						model.getDocsKomentarFileName());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
			return "ok";
		}
		model.setListPrcMainHeader(prcService.listPrcMainHeaderPpPq(null));
		return SUCCESS;
	}

	/** anggaran **/
	@Action(results = { @Result(name = SUCCESS, location = "proc/pp/listPpAnggaran.jsp"),
			@Result(name = "view", location = "proc/pp/viewPpAnggaran.jsp"),
			// @Result(name="ok",location="pp/anggaran",type="redirectAction")
			@Result(name = "ok", location = "dashboard", type = "redirectAction") }, value = "pp/anggaran")
	public String createAnggaran() throws Exception {
		try {
			if (WebUtils.hasSubmitParameter(request, "proses")) {
				PrcMainHeader prcMainHeader = prcService.getPrcMainIHeaderById(model.getPrcMainHeader().getPpmId());
				AdmEmployee admEmployee = new AdmServiceImpl()
						.getAdmEmployeeById(prcMainHeader.getAdmUserByPpmCurrentUser().getId());
				model.setPrcMainHeader(prcMainHeader);
				model.setAdmEmployee(admEmployee);
				model.setListHistMainComment(
						prcService.listHistMainCommentByPrcHeader(new PrcMainHeader(prcMainHeader.getPpmId())));
				List<PrcMainItem> listItem = prcService
						.listPrcMainItemByHeader(new PrcMainHeader(prcMainHeader.getPpmId()));
				model.setListPrcMainItem(listItem);
				model.setListAnggaranJdeDetail(prcService.listAnggaranDetailByPPmId(model.getPrcMainHeader()));
				return "view";
			} else if (WebUtils.hasSubmitParameter(request, "saveAndFinish")) {
				model.getHistMainComment().setAksi("Pembuatan Anggaran");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("aa.1", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("aa.2", null));
				prcService.createAnggaran(model.getPrcMainHeader(), model.getHistMainComment(), model.getDocsKomentar(),
						model.getDocsKomentarFileName());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			} else if (WebUtils.hasSubmitParameter(request, "realokasi")) {
				model.getHistMainComment().setAksi("Pengecekan Anggaran");
				model.getHistMainComment().setCreatedBy("Dokumen Realokasi Anggaran");
				prcService.relokasiAnggaran(model.getPrcMainHeader(), model.getHistMainComment(),
						model.getDocsKomentar(), model.getDocsKomentarFileName());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
			return "ok";
		}
		model.setListPrcMainHeader(prcService.listPrcMainHeaderPpAnggaran(null));
		return SUCCESS;
	}

	@Action(results = { @Result(name = SUCCESS, location = "dashboard", type = "redirectAction"),
			@Result(name = "view", location = "proc/pp/viewRealokasiAnggaran.jsp"),
			// @Result(name="ok",location="pp/anggaran",type="redirectAction")
			@Result(name = "ok", location = "dashboard", type = "redirectAction") }, value = "pp/realokasiAnggaran")
	public String realokasiAnggaran() throws Exception {
		try {
			if (WebUtils.hasSubmitParameter(request, "proses")) {
				PrcMainHeader prcMainHeader = prcService.getPrcMainIHeaderById(model.getPrcMainHeader().getPpmId());
				AdmEmployee admEmployee = new AdmServiceImpl()
						.getAdmEmployeeById(prcMainHeader.getAdmUserByPpmCurrentUser().getId());
				model.setPrcMainHeader(prcMainHeader);
				model.setAdmEmployee(admEmployee);
				model.setListHistMainComment(
						prcService.listHistMainCommentByPrcHeader(new PrcMainHeader(prcMainHeader.getPpmId())));
				List<PrcMainItem> listItem = prcService
						.listPrcMainItemByHeader(new PrcMainHeader(prcMainHeader.getPpmId()));
				model.setListPrcMainItem(listItem);
				model.setListAnggaranJdeDetail(prcService.listAnggaranDetailByPPmId(model.getPrcMainHeader()));
				return "view";
			} else if (WebUtils.hasSubmitParameter(request, "saverealokasi")) {
				model.getHistMainComment().setAksi("Attach Dok Anggaran");
				model.getHistMainComment().setCreatedBy("Pembuatan Anggaran");
				prcService.saveRelokasiAnggaran(model.getPrcMainHeader(), model.getHistMainComment(),
						model.getDocsKomentar(), model.getDocsKomentarFileName());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
			return "ok";
		}
		model.setListPrcMainHeader(prcService.listPrcMainHeaderPpAnggaran(null));
		return SUCCESS;
	}

	/**
	 * generate anggaran dari JDE ON
	 */

	@Action(results = { @Result(name = SUCCESS, location = "ajax/pp/listAnggaran", type = "redirectAction", params = {
			"prcMainHeader.ppmId", "%{prcMainHeader.ppmId}" }) }, value = "ajax/generateAnggaranJde")
	public String generateAnggaran() throws Exception {
		try {
			prcService.generateAnggaran(model.getPrcMainHeader());
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}

	/** anggaran manual **/
	@Action(results = { @Result(name = SUCCESS, location = "proc/pp/listPpAnggaranManual.jsp"),
			@Result(name = "view", location = "proc/pp/viewPpAnggaranManual.jsp"),
			@Result(name = "ok", location = "pp/anggaranManual", type = "redirectAction") }, value = "pp/anggaranManual")
	public String createAnggaranManual() throws Exception {
		try {
			if (WebUtils.hasSubmitParameter(request, "proses")) {
				PrcMainHeader prcMainHeader = prcService.getPrcMainIHeaderById(model.getPrcMainHeader().getPpmId());
				AdmEmployee admEmployee = new AdmServiceImpl()
						.getAdmEmployeeById(prcMainHeader.getAdmUserByPpmCurrentUser().getId());
				model.setPrcMainHeader(prcMainHeader);
				model.setAdmEmployee(admEmployee);
				model.setListHistMainComment(
						prcService.listHistMainCommentByPrcHeader(new PrcMainHeader(prcMainHeader.getPpmId())));
				List<PrcMainItem> listItem = prcService
						.listPrcMainItemByHeader(new PrcMainHeader(prcMainHeader.getPpmId()));
				model.setListPrcMainItem(listItem);
				model.setListAnggaranJdeDetail(prcService.listAnggaranDetailByPPmId(model.getPrcMainHeader()));
				return "view";
			} else if (WebUtils.hasSubmitParameter(request, "saveAndFinish")) {
				model.getHistMainComment().setAksi("Revisi Anggaran");
				prcService.createAnggaranManual(model.getPrcMainHeader(), model.getHistMainComment(),
						model.getDocsKomentar(), model.getDocsKomentarFileName());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
			return "ok";
		}
		model.setListPrcMainHeader(prcService.listPrcMainHeaderPpAnggaranManual(null));
		return SUCCESS;
	}

	@Action(results = { @Result(name = INPUT, location = "proc/pp/ajax/formAnggaran.jsp"),
			@Result(name = SUCCESS, location = "ajax/pp/listAnggaran", type = "redirectAction", params = {
					"prcMainHeader.ppmId", "%{prcMainHeader.ppmId}" }) }, value = "ajax/pp/crudAnggaran")
	public String anggaranSubmit() throws Exception {
		try {
			if (WebUtils.hasSubmitParameter(request, "save")) {
				System.out.println("SAVEE");
				prcService.createAnggaranDetail(model.getAnggaranJdeDetail());
				addActionMessage(getText("notif.save"));
				return SUCCESS;
			} else {
				if (model.getxCommand() != null) {
					if (model.getxCommand().equalsIgnoreCase("delete")) {
						prcService.deleteAnggaran(model.getAnggaranJdeDetail());
						addActionMessage("Data Sudah Di Hapus");
						return SUCCESS;
					} else if (model.getxCommand().equalsIgnoreCase("edit")) {
						model.setAnggaranJdeDetail(prcService.getAnggaranDetailById(model.getAnggaranJdeDetail()));
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
			return SUCCESS;
		}
		model.setListCostCenter(new AdmServiceImpl().getListCostCenterAnggaran(null));
		return INPUT;
	}

	@Action(results = {
			@Result(name = SUCCESS, location = "proc/pp/ajax/listAnggaranJde.jsp") }, value = "ajax/pp/listAnggaranJde")
	public String listAnggaranJde() throws Exception {
		try {
			model.setListJdeMasterMain(
					rfqService.listJdeMasterAnggaran(model.getTahunAnggaran(), model.getKodeAnggaran()));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}

	@Action(results = {
			@Result(name = SUCCESS, location = "proc/pp/ajax/listAnggaran.jsp") }, value = "ajax/pp/listAnggaran")
	public String listAnggaran() throws Exception {
		try {
			model.setListAnggaranJdeDetail(prcService.listAnggaranDetailByPPmId(model.getPrcMainHeader()));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}

	@Action(results = {
			@Result(name = SUCCESS, location = "proc/pp/ajax/listAnggaranRead.jsp") }, value = "ajax/pp/listAnggaranRead")
	public String listAnggaranRead() throws Exception {
		try {
			model.setListAnggaranJdeDetail(prcService.listAnggaranDetailByPPmId(model.getPrcMainHeader()));
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		return SUCCESS;
	}

	/** anggaran **/
	@Action(results = { @Result(name = SUCCESS, location = "proc/pp/listPpApprovalAnggaran.jsp"),
			@Result(name = "view", location = "proc/pp/viewPpApprovalAnggaran.jsp"),
			// @Result(name="ok",location="pp/approvalAnggaran",type="redirectAction")
			@Result(name = "ok", location = "dashboard", type = "redirectAction") }, value = "pp/approvalAnggaran")
	public String approvalAnggaran() throws Exception {
		try {
			if (WebUtils.hasSubmitParameter(request, "proses")) {
				PrcMainHeader prcMainHeader = prcService.getPrcMainIHeaderById(model.getPrcMainHeader().getPpmId());
				AdmEmployee admEmployee = new AdmServiceImpl()
						.getAdmEmployeeById(prcMainHeader.getAdmUserByPpmCurrentUser().getId());
				model.setPrcMainHeader(prcMainHeader);
				model.setAdmEmployee(admEmployee);
				model.setListHistMainComment(
						prcService.listHistMainCommentByPrcHeader(new PrcMainHeader(prcMainHeader.getPpmId())));
				List<PrcMainItem> listItem = prcService
						.listPrcMainItemByHeader(new PrcMainHeader(prcMainHeader.getPpmId()));
				model.setListPrcMainItem(listItem);
				model.setListAnggaranJdeDetail(prcService.listAnggaranDetailByPPmId(model.getPrcMainHeader()));
				return "view";
			} else if (WebUtils.hasSubmitParameter(request, "saveAndFinish")) {
				model.getHistMainComment().setAksi("Persetujuan Anggaran");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("aa.2", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("aa.2", null));
				prcService.setujuApprovalAnggaran(model.getPrcMainHeader(), model.getHistMainComment(),
						model.getDocsKomentar(), model.getDocsKomentarFileName());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			} else if (WebUtils.hasSubmitParameter(request, "revisi")) {
				model.getHistMainComment().setAksi("Revisi Anggaran");
				model.getHistMainComment().setProsesAksi(SessionGetter.getPropertiesValue("aa.2", null));
				model.getHistMainComment().setCreatedBy(SessionGetter.getPropertiesValue("aa.-1", null));
				prcService.revisiApprovalAnggaran(model.getPrcMainHeader(), model.getHistMainComment(),
						model.getDocsKomentar(), model.getDocsKomentarFileName());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
			return "ok";
		}
		model.setListPrcMainHeader(prcService.listPrcMainHeaderPpAnggaran(null));
		return SUCCESS;
	}
}
