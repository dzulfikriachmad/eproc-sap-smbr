package com.eproc.x.verification.command;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.orm.model.AdmDistrict;
import com.orm.model.AdmUser;
import com.orm.model.ComGroup;
import com.orm.model.TmpVndDoc;
import com.orm.model.TmpVndHeader;
import com.orm.model.UsulanVnd;
import com.orm.model.UsulanVndDetail;
import com.orm.model.UsulanVndHistory;

public class VendorVerificationCommand implements Serializable {
	private List<TmpVndHeader> listTmpVndHeader;
	private List<TmpVndDoc> listTmpVndDoc;
	private List<AdmUser> listUser;
	
	private List<BidderListModel> listBidder;
	private List<BidderDetailModel> listBidderDetail;
	private List<VendorExpiredModel> listVendorExpired;
	private List<AdmDistrict> listKantor;
	private List<ComGroup> listGrup;
	private Integer kantor;
	private Integer klasifikasi;
	private String komoditi;
	private String namaVendor;
	private Integer tipe;
	private Integer vendorId;
	private String statusVendor;
	
	private TmpVndHeader tmpVndHeader;
	private UsulanVnd usulan;
	private UsulanVndHistory usulanHistory;
	private List<UsulanVnd> listUsulan;
	private List<UsulanVndDetail> listUsulanDetail;
	private List<UsulanVndHistory> listUsulanHistory;
	
	private String jdeUser;
	private String jdePassword;    
	private Integer status;
	private Map<Integer, String> listJde = new HashMap<Integer, String>();
	private List<Integer> listOfYears;

	private Integer year;

	public List<TmpVndHeader> getListTmpVndHeader() {
		return listTmpVndHeader;
	}

	public void setListTmpVndHeader(List<TmpVndHeader> listTmpVndHeader) {
		this.listTmpVndHeader = listTmpVndHeader;
	}

	public TmpVndHeader getTmpVndHeader() {
		return tmpVndHeader;
	}

	public void setTmpVndHeader(TmpVndHeader tmpVndHeader) {
		this.tmpVndHeader = tmpVndHeader;
	}

	public List<TmpVndDoc> getListTmpVndDoc() {
		return listTmpVndDoc;
	}

	public void setListTmpVndDoc(List<TmpVndDoc> listTmpVndDoc) {
		this.listTmpVndDoc = listTmpVndDoc;
	}

	public UsulanVnd getUsulan() {
		return usulan;
	}

	public void setUsulan(UsulanVnd usulan) {
		this.usulan = usulan;
	}

	public List<UsulanVnd> getListUsulan() {
		return listUsulan;
	}

	public void setListUsulan(List<UsulanVnd> listUsulan) {
		this.listUsulan = listUsulan;
	}

	public List<UsulanVndDetail> getListUsulanDetail() {
		return listUsulanDetail;
	}

	public void setListUsulanDetail(List<UsulanVndDetail> listUsulanDetail) {
		this.listUsulanDetail = listUsulanDetail;
	}

	public List<UsulanVndHistory> getListUsulanHistory() {
		return listUsulanHistory;
	}

	public void setListUsulanHistory(List<UsulanVndHistory> listUsulanHistory) {
		this.listUsulanHistory = listUsulanHistory;
	}

	public UsulanVndHistory getUsulanHistory() {
		return usulanHistory;
	}

	public void setUsulanHistory(UsulanVndHistory usulanHistory) {
		this.usulanHistory = usulanHistory;
	}

	public List<AdmUser> getListUser() {
		return listUser;
	}

	public void setListUser(List<AdmUser> listUser) {
		this.listUser = listUser;
	}

	public String getJdeUser() {
		return jdeUser;
	}

	public void setJdeUser(String jdeUser) {
		this.jdeUser = jdeUser;
	}

	public String getJdePassword() {
		return jdePassword;
	}

	public void setJdePassword(String jdePassword) {
		this.jdePassword = jdePassword;
	}
	
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Map<Integer, String> getListJde() {
		listJde.put(1, "Sudah Terdaftar");
		listJde.put(2, "Belum Terdaftar");
		return listJde;
	}

	public List<BidderListModel> getListBidder() {
		return listBidder;
	}

	public void setListBidder(List<BidderListModel> listBidder) {
		this.listBidder = listBidder;
	}

	public List<AdmDistrict> getListKantor() {
		return listKantor;
	}

	public void setListKantor(List<AdmDistrict> listKantor) {
		this.listKantor = listKantor;
	}

	public List<ComGroup> getListGrup() {
		return listGrup;
	}

	public void setListGrup(List<ComGroup> listGrup) {
		this.listGrup = listGrup;
	}

	public void setListJde(Map<Integer, String> listJde) {
		this.listJde = listJde;
	}

	public Integer getKantor() {
		return kantor;
	}

	public void setKantor(Integer kantor) {
		this.kantor = kantor;
	}

	public Integer getKlasifikasi() {
		return klasifikasi;
	}

	public void setKlasifikasi(Integer klasifikasi) {
		this.klasifikasi = klasifikasi;
	}

	public String getKomoditi() {
		return komoditi;
	}

	public void setKomoditi(String komoditi) {
		this.komoditi = komoditi;
	}

	public String getNamaVendor() {
		return namaVendor;
	}

	public void setNamaVendor(String namaVendor) {
		this.namaVendor = namaVendor;
	}

	public List<BidderDetailModel> getListBidderDetail() {
		return listBidderDetail;
	}

	public void setListBidderDetail(List<BidderDetailModel> listBidderDetail) {
		this.listBidderDetail = listBidderDetail;
	}

	public Integer getTipe() {
		return tipe;
	}

	public void setTipe(Integer tipe) {
		this.tipe = tipe;
	}

	public Integer getVendorId() {
		return vendorId;
	}

	public void setVendorId(Integer vendorId) {
		this.vendorId = vendorId;
	}

	public List<VendorExpiredModel> getListVendorExpired() {
		return listVendorExpired;
	}

	public void setListVendorExpired(List<VendorExpiredModel> listVendorExpired) {
		this.listVendorExpired = listVendorExpired;
	}

	public String getStatusVendor() {
		return statusVendor;
	}

	public void setStatusVendor(String statusVendor) {
		this.statusVendor = statusVendor;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public List<Integer> getListOfYears() {
		return listOfYears;
	}

	public void setListOfYears(List<Integer> listOfYears) {
		this.listOfYears = listOfYears;
	}	
}
