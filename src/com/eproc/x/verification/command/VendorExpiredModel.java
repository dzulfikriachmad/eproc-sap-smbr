package com.eproc.x.verification.command;

import java.io.Serializable;

public class VendorExpiredModel implements Serializable {

	private Integer vendorId;
	private String vendorName;
	private Integer siup;
	private Integer tdp;
	private Integer domisili;
	private Integer ijin;
	private Integer sertifikat;
	private String email;
	private String status;
	
	public VendorExpiredModel() {
		// TODO Auto-generated constructor stub
	}

	public Integer getVendorId() {
		return vendorId;
	}

	public void setVendorId(Integer vendorId) {
		this.vendorId = vendorId;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public Integer getSiup() {
		return siup;
	}

	public void setSiup(Integer siup) {
		this.siup = siup;
	}

	public Integer getTdp() {
		return tdp;
	}

	public void setTdp(Integer tdp) {
		this.tdp = tdp;
	}

	public Integer getDomisili() {
		return domisili;
	}

	public void setDomisili(Integer domisili) {
		this.domisili = domisili;
	}

	public Integer getIjin() {
		return ijin;
	}

	public void setIjin(Integer ijin) {
		this.ijin = ijin;
	}

	public Integer getSertifikat() {
		return sertifikat;
	}

	public void setSertifikat(Integer sertifikat) {
		this.sertifikat = sertifikat;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
