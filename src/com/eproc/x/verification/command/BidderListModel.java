package com.eproc.x.verification.command;

import java.io.Serializable;

public class BidderListModel implements Serializable {

	private Integer vendorId;
	private String namaVendor;
	private Integer diundang;
	private Integer mendaftar;
	private Integer lulusAdm;
	private Integer lulusTeknis;
	private Integer lulusHarga;
	private Integer menang;
	
	public BidderListModel() {
		// TODO Auto-generated constructor stub
	}

	public Integer getVendorId() {
		return vendorId;
	}

	public void setVendorId(Integer vendorId) {
		this.vendorId = vendorId;
	}

	public String getNamaVendor() {
		return namaVendor;
	}

	public void setNamaVendor(String namaVendor) {
		this.namaVendor = namaVendor;
	}

	public Integer getDiundang() {
		return diundang;
	}

	public void setDiundang(Integer diundang) {
		this.diundang = diundang;
	}

	public Integer getMendaftar() {
		return mendaftar;
	}

	public void setMendaftar(Integer mendaftar) {
		this.mendaftar = mendaftar;
	}

	public Integer getLulusAdm() {
		return lulusAdm;
	}

	public void setLulusAdm(Integer lulusAdm) {
		this.lulusAdm = lulusAdm;
	}

	public Integer getLulusTeknis() {
		return lulusTeknis;
	}

	public void setLulusTeknis(Integer lulusTeknis) {
		this.lulusTeknis = lulusTeknis;
	}

	public Integer getLulusHarga() {
		return lulusHarga;
	}

	public void setLulusHarga(Integer lulusHarga) {
		this.lulusHarga = lulusHarga;
	}

	public Integer getMenang() {
		return menang;
	}

	public void setMenang(Integer menang) {
		this.menang = menang;
	}
	
}
