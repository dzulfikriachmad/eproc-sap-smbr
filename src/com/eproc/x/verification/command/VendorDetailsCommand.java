package com.eproc.x.verification.command;

import java.io.Serializable;
import java.util.List;

import com.orm.model.TmpVndAddress;
import com.orm.model.TmpVndAgent;
import com.orm.model.TmpVndAkta;
import com.orm.model.TmpVndBank;
import com.orm.model.TmpVndBoard;
import com.orm.model.TmpVndCert;
import com.orm.model.TmpVndCompanyType;
import com.orm.model.TmpVndCv;
import com.orm.model.TmpVndDistrict;
import com.orm.model.TmpVndDomisili;
import com.orm.model.TmpVndEquip;
import com.orm.model.TmpVndFinRpt;
import com.orm.model.TmpVndHeader;
import com.orm.model.TmpVndIjin;
import com.orm.model.TmpVndProduct;
import com.orm.model.TmpVndSdm;
import com.orm.model.TmpVndTambahan;
import com.orm.model.VndAddress;
import com.orm.model.VndAgent;
import com.orm.model.VndAkta;
import com.orm.model.VndBank;
import com.orm.model.VndBoard;
import com.orm.model.VndCert;
import com.orm.model.VndCompanyType;
import com.orm.model.VndCv;
import com.orm.model.VndDistrict;
import com.orm.model.VndDomisili;
import com.orm.model.VndEquip;
import com.orm.model.VndFinRpt;
import com.orm.model.VndHeader;
import com.orm.model.VndIjin;
import com.orm.model.VndProduct;
import com.orm.model.VndSdm;
import com.orm.model.VndTambahan;

public class VendorDetailsCommand implements Serializable {
	private TmpVndHeader tmpVndHeader;
	private VndHeader vndHeader;
	private List<TmpVndAddress> listTmpVndAddress;
	private List<VndAddress> listVndAddress;
	
	private List<TmpVndAkta> listTmpVndAkta;
	private List<VndAkta> listVndAkta;
	
	private List<TmpVndBoard> listTmpVndBoardKomisaris;
	private List<VndBoard> listVndBoardKomisaris;
	
	private List<TmpVndBoard> listTmpVndBoardDireksi;
	private List<VndBoard> listVndBoardDireksi;
	
	private List<TmpVndBank> listTmpVndBank;
	private List<VndBank> listVndBank;
	
	private List<TmpVndFinRpt> listTmpVndFinRpt;
	private List<VndFinRpt> listVndFinRpt;
	
	private List<TmpVndCert> listTmpVndCert;
	private List<VndCert> listVndCert;
	
	private List<TmpVndSdm> listTmpVndSdm;
	private List<VndSdm> listVndSdm;
	
	private List<TmpVndEquip> listTmpVndEquip;
	private List<VndEquip> listVndEquip;
	
	private List<TmpVndCv> listTmpVndCv;
	private List<VndCv> listVndCv;
	
	private List<TmpVndProduct> listTmpVndProduk;
	private List<VndProduct> listVndProduk;
	
	private List<TmpVndAgent> listTmpVndAgent;
	private List<VndAgent> listVndAgent;
	
	private List<TmpVndTambahan> listTmpVndTambahan;
	private List<VndTambahan> listVndTambahan;
	
	private List<TmpVndDomisili> listTmpVndDomisili;
	private List<VndDomisili> listVndDomisili;
	
	private List<TmpVndDistrict> listTmpVndDistrict;
	private List<VndDistrict> listVndDistrict;
	
	private List<TmpVndCompanyType> listTmpVndCompanyType;
	private List<VndCompanyType> listVndCompanyType;
	
	private List<TmpVndIjin> listIjin;
	private List<VndIjin> listVndIjin;
	
	public TmpVndHeader getTmpVndHeader() {
		return tmpVndHeader;
	}
	
	public void setTmpVndHeader(TmpVndHeader tmpVndHeader) {
		this.tmpVndHeader = tmpVndHeader;
	}
	
	public List<TmpVndAddress> getListTmpVndAddress() {
		return listTmpVndAddress;
	}
	
	public void setListTmpVndAddress(List<TmpVndAddress> listTmpVndAddress) {
		this.listTmpVndAddress = listTmpVndAddress;
	}

	public List<TmpVndBoard> getListTmpVndBoardKomisaris() {
		return listTmpVndBoardKomisaris;
	}

	public void setListTmpVndBoardKomisaris(
			List<TmpVndBoard> listTmpVndBoardKomisaris) {
		this.listTmpVndBoardKomisaris = listTmpVndBoardKomisaris;
	}

	public List<TmpVndBoard> getListTmpVndBoardDireksi() {
		return listTmpVndBoardDireksi;
	}

	public void setListTmpVndBoardDireksi(List<TmpVndBoard> listTmpVndBoardDireksi) {
		this.listTmpVndBoardDireksi = listTmpVndBoardDireksi;
	}

	public List<TmpVndBank> getListTmpVndBank() {
		return listTmpVndBank;
	}

	public void setListTmpVndBank(List<TmpVndBank> listTmpVndBank) {
		this.listTmpVndBank = listTmpVndBank;
	}

	public List<TmpVndFinRpt> getListTmpVndFinRpt() {
		return listTmpVndFinRpt;
	}

	public void setListTmpVndFinRpt(List<TmpVndFinRpt> listTmpVndFinRpt) {
		this.listTmpVndFinRpt = listTmpVndFinRpt;
	}

	public List<TmpVndCert> getListTmpVndCert() {
		return listTmpVndCert;
	}

	public void setListTmpVndCert(List<TmpVndCert> listTmpVndCert) {
		this.listTmpVndCert = listTmpVndCert;
	}

	public List<TmpVndSdm> getListTmpVndSdm() {
		return listTmpVndSdm;
	}

	public void setListTmpVndSdm(List<TmpVndSdm> listTmpVndSdm) {
		this.listTmpVndSdm = listTmpVndSdm;
	}

	public List<TmpVndEquip> getListTmpVndEquip() {
		return listTmpVndEquip;
	}

	public void setListTmpVndEquip(List<TmpVndEquip> listTmpVndEquip) {
		this.listTmpVndEquip = listTmpVndEquip;
	}

	public List<TmpVndCv> getListTmpVndCv() {
		return listTmpVndCv;
	}

	public void setListTmpVndCv(List<TmpVndCv> listTmpVndCv) {
		this.listTmpVndCv = listTmpVndCv;
	}

	public List<TmpVndAkta> getListTmpVndAkta() {
		return listTmpVndAkta;
	}

	public void setListTmpVndAkta(List<TmpVndAkta> listTmpVndAkta) {
		this.listTmpVndAkta = listTmpVndAkta;
	}

	public List<TmpVndProduct> getListTmpVndProduk() {
		return listTmpVndProduk;
	}

	public void setListTmpVndProduk(List<TmpVndProduct> listTmpVndProduk) {
		this.listTmpVndProduk = listTmpVndProduk;
	}

	public List<TmpVndAgent> getListTmpVndAgent() {
		return listTmpVndAgent;
	}

	public void setListTmpVndAgent(List<TmpVndAgent> listTmpVndAgent) {
		this.listTmpVndAgent = listTmpVndAgent;
	}

	public List<TmpVndTambahan> getListTmpVndTambahan() {
		return listTmpVndTambahan;
	}

	public void setListTmpVndTambahan(List<TmpVndTambahan> listTmpVndTambahan) {
		this.listTmpVndTambahan = listTmpVndTambahan;
	}

	public List<TmpVndDistrict> getListTmpVndDistrict() {
		return listTmpVndDistrict;
	}

	public void setListTmpVndDistrict(List<TmpVndDistrict> listTmpVndDistrict) {
		this.listTmpVndDistrict = listTmpVndDistrict;
	}

	public List<TmpVndDomisili> getListTmpVndDomisili() {
		return listTmpVndDomisili;
	}

	public void setListTmpVndDomisili(List<TmpVndDomisili> listTmpVndDomisili) {
		this.listTmpVndDomisili = listTmpVndDomisili;
	}

	public List<TmpVndCompanyType> getListTmpVndCompanyType() {
		return listTmpVndCompanyType;
	}

	public void setListTmpVndCompanyType(
			List<TmpVndCompanyType> listTmpVndCompanyType) {
		this.listTmpVndCompanyType = listTmpVndCompanyType;
	}

	public List<TmpVndIjin> getListIjin() {
		return listIjin;
	}

	public void setListIjin(List<TmpVndIjin> listIjin) {
		this.listIjin = listIjin;
	}

	public VndHeader getVndHeader() {
		return vndHeader;
	}

	public void setVndHeader(VndHeader vndHeader) {
		this.vndHeader = vndHeader;
	}

	public List<VndAddress> getListVndAddress() {
		return listVndAddress;
	}

	public void setListVndAddress(List<VndAddress> listVndAddress) {
		this.listVndAddress = listVndAddress;
	}

	public List<VndAkta> getListVndAkta() {
		return listVndAkta;
	}

	public void setListVndAkta(List<VndAkta> listVndAkta) {
		this.listVndAkta = listVndAkta;
	}

	public List<VndBoard> getListVndBoardKomisaris() {
		return listVndBoardKomisaris;
	}

	public void setListVndBoardKomisaris(List<VndBoard> listVndBoardKomisaris) {
		this.listVndBoardKomisaris = listVndBoardKomisaris;
	}

	public List<VndBoard> getListVndBoardDireksi() {
		return listVndBoardDireksi;
	}

	public void setListVndBoardDireksi(List<VndBoard> listVndBoardDireksi) {
		this.listVndBoardDireksi = listVndBoardDireksi;
	}

	public List<VndBank> getListVndBank() {
		return listVndBank;
	}

	public void setListVndBank(List<VndBank> listVndBank) {
		this.listVndBank = listVndBank;
	}

	public List<VndFinRpt> getListVndFinRpt() {
		return listVndFinRpt;
	}

	public void setListVndFinRpt(List<VndFinRpt> listVndFinRpt) {
		this.listVndFinRpt = listVndFinRpt;
	}

	public List<VndCert> getListVndCert() {
		return listVndCert;
	}

	public void setListVndCert(List<VndCert> listVndCert) {
		this.listVndCert = listVndCert;
	}

	public List<VndSdm> getListVndSdm() {
		return listVndSdm;
	}

	public void setListVndSdm(List<VndSdm> listVndSdm) {
		this.listVndSdm = listVndSdm;
	}

	public List<VndEquip> getListVndEquip() {
		return listVndEquip;
	}

	public void setListVndEquip(List<VndEquip> listVndEquip) {
		this.listVndEquip = listVndEquip;
	}

	public List<VndCv> getListVndCv() {
		return listVndCv;
	}

	public void setListVndCv(List<VndCv> listVndCv) {
		this.listVndCv = listVndCv;
	}

	public List<VndProduct> getListVndProduk() {
		return listVndProduk;
	}

	public void setListVndProduk(List<VndProduct> listVndProduk) {
		this.listVndProduk = listVndProduk;
	}

	public List<VndAgent> getListVndAgent() {
		return listVndAgent;
	}

	public void setListVndAgent(List<VndAgent> listVndAgent) {
		this.listVndAgent = listVndAgent;
	}

	public List<VndTambahan> getListVndTambahan() {
		return listVndTambahan;
	}

	public void setListVndTambahan(List<VndTambahan> listVndTambahan) {
		this.listVndTambahan = listVndTambahan;
	}

	public List<VndDomisili> getListVndDomisili() {
		return listVndDomisili;
	}

	public void setListVndDomisili(List<VndDomisili> listVndDomisili) {
		this.listVndDomisili = listVndDomisili;
	}

	public List<VndDistrict> getListVndDistrict() {
		return listVndDistrict;
	}

	public void setListVndDistrict(List<VndDistrict> listVndDistrict) {
		this.listVndDistrict = listVndDistrict;
	}

	public List<VndCompanyType> getListVndCompanyType() {
		return listVndCompanyType;
	}

	public void setListVndCompanyType(List<VndCompanyType> listVndCompanyType) {
		this.listVndCompanyType = listVndCompanyType;
	}

	public List<VndIjin> getListVndIjin() {
		return listVndIjin;
	}

	public void setListVndIjin(List<VndIjin> listVndIjin) {
		this.listVndIjin = listVndIjin;
	}
	
}
