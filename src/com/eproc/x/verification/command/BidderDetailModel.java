package com.eproc.x.verification.command;

import java.io.Serializable;

public class BidderDetailModel implements Serializable {

	private Integer ppmId;
	private String nomorRfq;
	private String judul;
	private String oe;
	private String realisasi;
	
	public BidderDetailModel() {
		// TODO Auto-generated constructor stub
	}

	public Integer getPpmId() {
		return ppmId;
	}

	public void setPpmId(Integer ppmId) {
		this.ppmId = ppmId;
	}

	public String getNomorRfq() {
		return nomorRfq;
	}

	public void setNomorRfq(String nomorRfq) {
		this.nomorRfq = nomorRfq;
	}

	public String getJudul() {
		return judul;
	}

	public void setJudul(String judul) {
		this.judul = judul;
	}

	public String getOe() {
		return oe;
	}

	public void setOe(String oe) {
		this.oe = oe;
	}

	public String getRealisasi() {
		return realisasi;
	}

	public void setRealisasi(String realisasi) {
		this.realisasi = realisasi;
	}
	
}
