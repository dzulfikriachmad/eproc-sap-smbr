package com.eproc.x.verification.action;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.web.util.WebUtils;

import com.eproc.core.BaseController;
import com.eproc.sap.controller.IntegrationService;
import com.eproc.sap.controller.IntegrationServiceImpl;
import com.eproc.sap.dto.ResultDto;
import com.eproc.utils.EmailSessionBean;
import com.eproc.utils.JdeSessionUtil;
import com.eproc.utils.QueryUtil;
import com.eproc.utils.SessionGetter;
import com.eproc.x.verification.command.VendorVerificationCommand;
import com.jdedwards.system.connector.dynamic.Connector;
import com.jdedwards.system.connector.dynamic.UserSession;
import com.orm.model.AdmDoc;
import com.orm.model.PrcMainHeader;
import com.orm.model.TmpVndAddress;
import com.orm.model.TmpVndDoc;
import com.orm.model.TmpVndHeader;
import com.orm.model.UsulanVndDetail;

import eproc.controller.AdmService;
import eproc.controller.AdmServiceImpl;
import eproc.controller.ComServiceImpl;
//import eproc.controller.JdeService;
import eproc.controller.PrcServiceImpl;
import eproc.controller.ToolsService;
import eproc.controller.ToolsServiceImpl;
import eproc.controller.VendorService;
import eproc.controller.VendorServiceImpl;
import eproc.controller.VndRegService;
import eproc.controller.VndRegServiceImpl;

@Namespace("/")
@ParentPackage("eproc")
public class VendorVerificationAction extends BaseController {
	private VendorVerificationCommand model;
	private VendorService vndService;
	private AdmService admService;
	private VndRegService vndRegService;
	private ToolsService toolsService;
	private IntegrationService integrationService;
	
	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return model;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		model = new VendorVerificationCommand();
		vndService = new VendorServiceImpl();
		admService = new AdmServiceImpl();
		vndRegService = new VndRegServiceImpl();
		toolsService = new ToolsServiceImpl();
		integrationService = new IntegrationServiceImpl();
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vendor/verificationList.jsp"),
					@Result(name=ERROR, location="vendor/verificationList.jsp"),
					@Result(name="verPage", location="vendor/verificationPage.jsp")
			}, value = "vendor/verification"
	)
	public String vendorNewVerification() throws Exception{
		if(WebUtils.hasSubmitParameter(request, "verification")){
			System.out.println(">>>> VERIFY >>>>>");
			if(model.getTmpVndHeader().getVendorId() != 0){
				List<TmpVndDoc> listDoc = vndService.listTmpVndDocByTmpVndHeader(model.getTmpVndHeader().getVendorId());
				List<AdmDoc> listAdmDoc = admService.listAdmDoc();
				
				if(listDoc.size() == 0){
					for (int i = 0; i < listAdmDoc.size(); i++) {
						TmpVndDoc tmpDoc = new TmpVndDoc();
						tmpDoc.setTmpVndHeader(model.getTmpVndHeader());
						tmpDoc.setAdmDoc(listAdmDoc.get(i));
						tmpDoc.setDocName(listAdmDoc.get(i).getDocName());
						vndService.saveOrUpdateTmpVndDoc(tmpDoc);
					}
					model.setListTmpVndDoc(vndService.listTmpVndDocByTmpVndHeader(model.getTmpVndHeader().getVendorId()));
				}else{
					model.setListTmpVndDoc(listDoc);
				}
				
				return "verPage";
			}else{
				addActionError("Terjadi kesalahan, harap ulangi beberapa saat lagi");
				return ERROR;
			}
		}else if(WebUtils.hasSubmitParameter(request, "save")){
			if(model.getListTmpVndDoc() != null && model.getListTmpVndDoc().size() > 0){
				for(TmpVndDoc d:model.getListTmpVndDoc()){
					vndService.saveOrUpdateTmpVndDoc(d);
				}
			}
			addActionMessage("Data verifikasi vendor berhasil disimpan..");
			return "verPage";
		}else if(WebUtils.hasSubmitParameter(request, "approve")){
			System.out.println(">>>> approve >>>>>");
			if(model.getListTmpVndDoc() != null && model.getListTmpVndDoc().size() > 0){
				for(TmpVndDoc d:model.getListTmpVndDoc()){
					vndService.saveOrUpdateTmpVndDoc(d);
				}
				vndRegService.updateStatusTmpVndHeader("V", model.getTmpVndHeader());
			}
			addActionMessage( " Berhasil diapprove..");
			
		}else if(WebUtils.hasSubmitParameter(request, "routeToVendor")){
			System.out.println(">>>> Route to Vendor >>>>>");
			vndRegService.updateStatusTmpVndHeader("R", model.getTmpVndHeader());
			vndRegService.saveNextPage(9, model.getTmpVndHeader());
			
			String message = "Data Yang Perlu Perbaikan (Data Need to be revised) : <br/>";
			if(model.getListTmpVndDoc() != null && model.getListTmpVndDoc().size() > 0){
				for(TmpVndDoc d:model.getListTmpVndDoc()){
					vndService.saveOrUpdateTmpVndDoc(d);
					message = message.concat(d.getDocName()).concat(" : ").concat(d.getDocRemark()).concat("<br/>");
				}
			}
			model.setTmpVndHeader(vndService.getTmpVndHeaderById(model.getTmpVndHeader().getVendorId()));
			EmailSessionBean.sendMail("eProc PT Semen Baturaja (Persero)", message, model.getTmpVndHeader().getVendorEmail());
			addActionMessage("Route to vendor berhasil..");
			
		}
		model.setListTmpVndHeader(vndService.listTmpVndHeaderByStatusVerifikasi("P"));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name="verPage", location="vendor/vendorDetail.jsp")
			}, value = "ajax/vendor/detail"
	)
	public String ajaxVendorVetail() throws Exception{
			if(model.getTmpVndHeader().getVendorId() != 0){
				List<TmpVndDoc> listDoc = vndService.listTmpVndDocByTmpVndHeader(model.getTmpVndHeader().getVendorId());
				List<AdmDoc> listAdmDoc = admService.listAdmDoc();
				
				if(listDoc.size() == 0){
					for (int i = 0; i < listAdmDoc.size(); i++) {
						TmpVndDoc tmpDoc = new TmpVndDoc();
						tmpDoc.setTmpVndHeader(model.getTmpVndHeader());
						tmpDoc.setAdmDoc(listAdmDoc.get(i));
						tmpDoc.setDocName(listAdmDoc.get(i).getDocName());
						vndService.saveOrUpdateTmpVndDoc(tmpDoc);
					}
					model.setListTmpVndDoc(vndService.listTmpVndDocByTmpVndHeader(model.getTmpVndHeader().getVendorId()));
				}else{
					model.setListTmpVndDoc(listDoc);
				}
			}
			return "verPage";
	}
	
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vendor/activationList.jsp"),
					@Result(name=ERROR, location="vendor/activation", type="redirectAction"),
					@Result(name="actPage", location="vendor/activationPage.jsp")
			}, value = "vendor/activation"
	)
	public String vendorActivation() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "activation")){
				List<TmpVndDoc> listDoc = vndService.listTmpVndDocByTmpVndHeader(model.getTmpVndHeader().getVendorId());
				model.setListTmpVndDoc(listDoc);
				return "actPage";
			}else if(WebUtils.hasSubmitParameter(request, "ok")){
				
					TmpVndHeader tmp = vndService.getTmpVndHeaderById(model.getTmpVndHeader().getVendorId());
					if(model.getTmpVndHeader().getVendorSmkNo()!=null && model.getTmpVndHeader().getVendorSmkNo().length()>0){
						tmp.setVendorSmkNo(model.getTmpVndHeader().getVendorSmkNo());
					}
					tmp.setVendorStatus("A");
					//vndService.aktivasiDanPenomoran(tmp);
					vndService.aktivasiDanPenomoran(tmp);
					addActionMessage("Data Sudah Disimpan "+tmp.getVendorName().toUpperCase());
				
			}
			model.setListTmpVndHeader(vndService.listTmpVndHeaderByStatus("F"));
			
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
			return ERROR;
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vendor/verificationListOld.jsp"),
					@Result(name=ERROR, location="vendor/verificationOld", type="redirectAction"),
					@Result(name="actPage", location="vendor/verificationPageOld.jsp")
			}, value = "vendor/verificationOld"
	)
	public String vendorVerificationOld() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "activation")){
				return "actPage";
			}else if(WebUtils.hasSubmitParameter(request, "ok")){
				TmpVndHeader tmp = vndService.getTmpVndHeaderById(model.getTmpVndHeader().getVendorId());
				if(model.getTmpVndHeader().getVendorSmkNo()!=null && model.getTmpVndHeader().getVendorSmkNo().length()>0){
					tmp.setVendorSmkNo(model.getTmpVndHeader().getVendorSmkNo());
				}
				tmp.setVendorStatus("A");
				vndService.aktivasiDanPenomoran(tmp);
				addActionMessage("Data Sudah Disimpan "+tmp.getVendorName().toUpperCase()+ ", Nomor Vendor : "+tmp.getVendorSmkNo());
					
			}
			
			model.setListTmpVndHeader(vndService.listTmpVndHeaderByStatus("EE"));
			
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
			return ERROR;
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vendor/vendorList.jsp")
			}, value = "vendor/list"
	)
	public String vendorList() throws Exception{
		if(WebUtils.hasSubmitParameter(request, "cari")){
			model.setListTmpVndHeader(vndService.listTmpVndHeaderByStatusCari(model.getTmpVndHeader().getVendorStatus()));
			return SUCCESS;
		}
		model.setListTmpVndHeader(vndService.listTmpVndHeaderByStatusCari(null));
		return SUCCESS;
	}
	
	
	
	/** USULAN VENDOR 
	 * 
	 * **/
	@Action(
			results={
					@Result(name=SUCCESS,location="vendor/usulan/listUsulan.jsp"),
					@Result(name="edit",location="vendor/usulan/formUsulan.jsp")
			},value="vendor/listUsulan"
			)
	public String listUsulan() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "edit")){
				model.setUsulan(vndService.getUsulanById(model.getUsulan()));
				model.setListUsulanDetail(vndService.getListUsulanDetailByUsulan(model.getUsulan()));
				model.setListUsulanHistory(vndService.getListUsulanHistoryByUsulan(model.getUsulan()));
				PrcMainHeader prcMainHeader = new PrcMainHeader();
				prcMainHeader.setAdmProses(SessionGetter.VENDOR_PROSES_ID);
				prcMainHeader.setPpmProsesLevel(1);
				prcMainHeader.setPpmJumlahEe(new BigDecimal(0));
				prcMainHeader.setPpmJumlahOe(new BigDecimal(0));
				prcMainHeader.setAdmDistrict(SessionGetter.getUser().getAdmEmployee().getAdmDept().getAdmDistrict());
				model.setListUser(new PrcServiceImpl().listUserByProsesLevel(prcMainHeader));
				return "edit";	 	
			}else if(WebUtils.hasSubmitParameter(request, "delete")){
				vndService.deleteUsulan(model.getUsulan());
				addActionMessage("Data Sudah Di Hapus ");
				return SUCCESS;
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		model.setListUsulan(vndService.getListUsulan(SessionGetter.getUser(),1));
		return SUCCESS;
	}
	

	@Action(
			results={
					@Result(name=INPUT,location="vendor/usulan/formUsulan.jsp"),
					@Result(name=SUCCESS,location="vendor/listUsulan",type="redirectAction")
			},value="vendor/formUsulan"
			)
	public String formUsulan() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "save")){
				int check = 0;
				for(UsulanVndDetail d:model.getListUsulanDetail()){
					TmpVndHeader vnd = vndService.getTmpVndHeaderById(d.getVendor().getVendorId());
					if(vnd.getVendorStatus().equalsIgnoreCase("R")) {
						check++;
					}
				}
				if(check>0) {
					addActionError("Ada vendor yang sedang melakukan revisi data");
				}else {
					vndService.saveUsulan(model.getUsulan(),model.getListUsulanDetail(),model.getUsulanHistory());
					addActionMessage(getText("notif.save"));
				}
				
				return SUCCESS;
			}else if(WebUtils.hasSubmitParameter(request, "revisi")){
				vndService.backToVerifikator(model.getUsulan(),model.getListUsulanDetail(),model.getUsulanHistory());
				addActionMessage(getText("notif.save"));
				return SUCCESS;
			}
			//prepare first time
			List<TmpVndHeader> listVendor = vndService.listTmpVndHeaderByStatusVerifikasi("V");
			if(listVendor!=null){
				model.setListUsulanDetail(SessionGetter.convertUsulanDetail(listVendor));
			}
			
			PrcMainHeader prcMainHeader = new PrcMainHeader();
			prcMainHeader.setAdmProses(SessionGetter.VENDOR_PROSES_ID);
			prcMainHeader.setPpmProsesLevel(1);
			prcMainHeader.setPpmJumlahEe(new BigDecimal(0));
			prcMainHeader.setPpmJumlahOe(new BigDecimal(0));
			prcMainHeader.setAdmDistrict(SessionGetter.getUser().getAdmEmployee().getAdmDept().getAdmDistrict());
			model.setListUser(new PrcServiceImpl().listUserByProsesLevel(prcMainHeader));
			
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
		}
		
		return INPUT;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="vendor/verifiedList.jsp")
			},value="vendor/listVerifikasi"
			)
	public String listVerifiVend() throws Exception{
		model.setListTmpVndHeader(vndService.listTmpVndHeaderByStatusVerifikasi("V"));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="vendor/bidderList.jsp")
			},value="vendor/listBidder"
			)
	public String listBidder() throws Exception{
		model.setListKantor(new AdmServiceImpl().getListDistrict(null));
		model.setListGrup(new ComServiceImpl().listComGroup(null));
		return SUCCESS;
	}
	
	
	@Action(
			results={
					@Result(name=SUCCESS,location="vendor/bidderList.jsp"),
					@Result(name="search", location="vendor/searchBidderList.jsp")
			},value="ajax/vendor/searchBidderList"
			)
	public String searchBidderList(){
		try {
				if(model.getKantor()==0){
					model.setKantor(null);
				}
				if(model.getKlasifikasi()==0){
					model.setKlasifikasi(null);
				}
				if(model.getKomoditi().trim().equalsIgnoreCase("0")){
					model.setKomoditi(null);
				}
				
				if(model.getNamaVendor()==null && model.getNamaVendor().trim().length()==0){
					model.setNamaVendor(null);
				}
				model.setListBidder(vndService.listBidder(QueryUtil.bidderList(model.getKantor(), model.getKomoditi(), model.getKlasifikasi(),model.getNamaVendor())));
				model.setNamaVendor(null);
				model.setKantor(null);
				model.setKlasifikasi(null);
				model.setKomoditi(null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "search";
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="vendor/bidderDetailPengadaan.jsp")
			},value="ajax/vendor/bidderDetailPengadaan"
			)
	public String bidderDetail() throws Exception{
		try {
			model.setListBidderDetail(vndService.listDetailBidder(QueryUtil.bidderDetail(model.getVendorId(), model.getTipe())));
			System.out.println(model.getVendorId());
			System.out.println( model.getTipe());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	
	@Action(
			results={
					@Result(name=SUCCESS,location="vendor/vendorExpired.jsp")
			},value="ajax/vendor/vendorExpired"
			)
	public String vendorExpired() throws Exception{
		try {
			model.setListVendorExpired(vndService.getExpiredVendor(QueryUtil.expiredDokumen(model.getVendorId())));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	
	@Action(
			results={
					@Result(name=INPUT,location="vendor/formSuspend.jsp"),
					@Result(name=SUCCESS,location="ajax/vendor/vendorExpired",type="redirectAction")
			},value="ajax/vendor/crudSuspend"
			)
	public String formDept() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "save")){
				vndService.setStatusVendor(QueryUtil.statusVendor(model.getVendorId(),model.getStatusVendor(),"vnd_header"));
				vndService.setStatusVendor(QueryUtil.statusVendor(model.getVendorId(),model.getStatusVendor(),"tmp_vnd_header"));
				addActionMessage("Data Sudah Disimpan");
				return SUCCESS;
			}else{
				if(model.getVendorId()!=null){
					TmpVndHeader h = vndService.getTmpVndHeaderById(Long.valueOf(model.getVendorId()));
					model.setStatusVendor(h.getVendorStatus());
				}
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
			return INPUT;
		}
		return INPUT;
	}
	
	/** approval usulan **/
	@Action(
			results={
					@Result(name=SUCCESS,location="vendor/usulan/listUsulanApproval.jsp"),
					@Result(name="edit",location="vendor/usulan/formUsulanApproval.jsp")
			},value="vendor/listUsulanApproval"
			)
	public String listUsulanApproval() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "edit")){
				model.setUsulan(vndService.getUsulanById(model.getUsulan()));
				model.setListUsulanDetail(vndService.getListUsulanDetailByUsulan(model.getUsulan()));
				model.setListUsulanHistory(vndService.getListUsulanHistoryByUsulan(model.getUsulan()));
				return "edit";
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		model.setListUsulan(vndService.getListUsulan(SessionGetter.getUser(),3));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=INPUT,location="vendor/usulan/formUsulanApproval.jsp"),
					@Result(name=SUCCESS,location="vendor/listUsulanApproval",type="redirectAction")
			},value="vendor/formUsulanApproval"
			)
	public String formUsulanApproval() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "save")){
				vndService.saveApprovalUsulan(model.getUsulan(),model.getListUsulanDetail(),model.getUsulanHistory(),SessionGetter.SETUJU);
				addActionMessage(getText("notif.save"));
				return SUCCESS;
			}else if(WebUtils.hasSubmitParameter(request, "revisi")){
				vndService.saveApprovalUsulan(model.getUsulan(),model.getListUsulanDetail(),model.getUsulanHistory(),SessionGetter.REVISI);
				addActionMessage(getText("notif.save"));
				return SUCCESS;
			}
			if(model.getUsulan()==null || model.getUsulan().getId()==null){
				return SUCCESS;
			}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
			return SUCCESS;
		}
		return INPUT;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="vendor/usulan/listUsulanMonitor.jsp"),
					@Result(name="edit",location="vendor/usulan/formUsulanMonitor.jsp")
			},value="vendor/monitorUsulan"
			)
	public String monitorUsulan() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "edit")){
				model.setUsulan(vndService.getUsulanById(model.getUsulan()));
				model.setListUsulanDetail(vndService.getListUsulanDetailByUsulan(model.getUsulan()));
				model.setListUsulanHistory(vndService.getListUsulanHistoryByUsulan(model.getUsulan()));
				return "edit";
			}else if(WebUtils.hasSubmitParameter(request, "cari")) {
				 model.setListUsulan(vndService.getListUsulanByYear(null,null, model.getYear()));
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		 //model.setListUsulan(vndService.getListUsulan(null,null));
		model.setListOfYears(toolsService.getListOfYears());
		return SUCCESS;
	}
	
	/** Finalisasi **/
	@Action(
			results={
					@Result(name=SUCCESS,location="vendor/usulan/listUsulanFinalisasi.jsp"),
					@Result(name="edit",location="vendor/usulan/formUsulanFinalisasi.jsp")
			},value="vendor/listUsulanFinalisasi"
			)
	public String listUsulanFinalisasi() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "edit")){
				model.setUsulan(vndService.getUsulanById(model.getUsulan()));
				model.setListUsulanDetail(vndService.getListUsulanDetailByUsulan(model.getUsulan()));
				model.setListUsulanHistory(vndService.getListUsulanHistoryByUsulan(model.getUsulan()));
				return "edit";
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
		}
		model.setListUsulan(vndService.getListUsulan(SessionGetter.getUser(),4));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=INPUT,location="vendor/usulan/formUsulanFinalisasi.jsp"),
					@Result(name=SUCCESS,location="vendor/listUsulanFinalisasi",type="redirectAction")
			},value="vendor/formUsulanFinalisasi"
			)
	public String formUsulanFinalisasi() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "save")){
				vndService.saveFinalisasiUsulan(model.getUsulan(),model.getListUsulanDetail(),model.getUsulanHistory(),SessionGetter.SETUJU);
				addActionMessage(getText("notif.save"));
				return SUCCESS;
			}
			
			if(model.getUsulan()==null || model.getUsulan().getId()==null){
				return SUCCESS;
			}
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
			return SUCCESS;
		}
		return INPUT;
	}
}
