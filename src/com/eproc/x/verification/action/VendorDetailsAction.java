package com.eproc.x.verification.action;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.eproc.core.BaseController;
import com.eproc.x.verification.command.VendorDetailsCommand;

import eproc.controller.VendorService;
import eproc.controller.VendorServiceImpl;
import eproc.controller.VndRegService;
import eproc.controller.VndRegServiceImpl;

@Namespace("/")
@ParentPackage("eproc")
public class VendorDetailsAction extends BaseController {
	private VendorDetailsCommand model;
	private VndRegService vndRegService;
	private VendorService vndService;

	@Override
	public Object getModel() {
		return model;
	}

	@Override
	public void prepare() throws Exception {
		model = new VendorDetailsCommand();
		vndRegService = new VndRegServiceImpl();
		vndService = new VendorServiceImpl();
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vendor/tabs/companyInformation.jsp")
			}, value="ajax/companyInformation",
			interceptorRefs={
					@InterceptorRef("defaultInterceptorStack")
			}
	)
	public String companyInformation() throws Exception{
		model.setTmpVndHeader(vndService.getTmpVndHeaderById(model.getTmpVndHeader().getVendorId()));
		model.setListTmpVndAddress(vndRegService.listTmpVndAddByVndHeader(model.getTmpVndHeader()));
		model.setListTmpVndTambahan(vndRegService.listTmpVndTambahanHeader(model.getTmpVndHeader()));
		model.setListTmpVndCompanyType(vndRegService.getTmpCompanyTypeByVndHeader(model.getTmpVndHeader()));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vendor/tabs/companyInformation.jsp")
			}, value="ajax/legalInformation",
			interceptorRefs={
					@InterceptorRef("defaultInterceptorStack")
			}
	)
	public String legalInformation() throws Exception{
		model.setTmpVndHeader(vndService.getTmpVndHeaderById(model.getTmpVndHeader().getVendorId()));
		model.setListTmpVndAddress(vndRegService.listTmpVndAddByVndHeader(model.getTmpVndHeader()));
		model.setListIjin(vndRegService.listTmpVndIjin(model.getTmpVndHeader()));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vendor/tabs/manInf.jsp")
			}, value="ajax/manInf",
			interceptorRefs={
					@InterceptorRef("defaultInterceptorStack")
			}
	)
	public String manInf() throws Exception{
		model.setListTmpVndBoardDireksi(vndRegService.listTmpVndBoardDireksi(model.getTmpVndHeader()));
		model.setListTmpVndBoardKomisaris(vndRegService.listTmpVndBoardKomisaris(model.getTmpVndHeader()));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vendor/tabs/finInf.jsp")
			}, value="ajax/finInf",
			interceptorRefs={
					@InterceptorRef("defaultInterceptorStack")
			}
	)
	public String finInf() throws Exception{
		model.setTmpVndHeader(vndService.getTmpVndHeaderById(model.getTmpVndHeader().getVendorId()));
		model.setListTmpVndBank(vndRegService.listTmpVndBank(model.getTmpVndHeader()));
		model.setListTmpVndFinRpt(vndRegService.listTmpVndFinRpt(model.getTmpVndHeader()));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vendor/tabs/quaInf.jsp")
			}, value="ajax/quaInf",
			interceptorRefs={
					@InterceptorRef("defaultInterceptorStack")
			}
	)
	public String quaInf() throws Exception{
		model.setListTmpVndCert(vndRegService.listTmpVndCert(model.getTmpVndHeader()));
		model.setListTmpVndProduk(vndRegService.listTmpVndProduct(model.getTmpVndHeader()));
		model.setListTmpVndDistrict(vndRegService.listTmpVndDistrict(model.getTmpVndHeader()));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vendor/tabs/resInf.jsp")
			}, value="ajax/resInf",
			interceptorRefs={
					@InterceptorRef("defaultInterceptorStack")
			}
	)
	public String resInf() throws Exception{
		model.setListTmpVndSdm(vndRegService.listTmpVndSdm(model.getTmpVndHeader()));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vendor/tabs/comFac.jsp")
			}, value="ajax/comFac",
			interceptorRefs={
					@InterceptorRef("defaultInterceptorStack")
			}
	)
	public String comFac() throws Exception{
		model.setListTmpVndEquip(vndRegService.listTmpVndEquip(model.getTmpVndHeader()));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vendor/tabs/proExp.jsp")
			}, value="ajax/proExp",
			interceptorRefs={
					@InterceptorRef("defaultInterceptorStack")
			}
	)
	public String proExp() throws Exception{
		model.setListTmpVndCv(vndRegService.listTmpVndCv(model.getTmpVndHeader()));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vendor/tabs/legInf.jsp")
			}, value="ajax/legInf",
			interceptorRefs={
					@InterceptorRef("defaultInterceptorStack")
			}
	)
	public String legInf() throws Exception{
		model.setTmpVndHeader(vndService.getTmpVndHeaderById(model.getTmpVndHeader().getVendorId()));
		model.setListTmpVndAkta(vndRegService.listTmpVndAkta(model.getTmpVndHeader()));
		model.setListTmpVndDomisili(vndRegService.listTmpVndDom(model.getTmpVndHeader()));
		return SUCCESS;
	}
	
	
	/*** Untuk Yang sudah AKtif **/
	@Action(
			results={
					@Result(name=SUCCESS, location="vendor/tabs/companyInformationAct.jsp")
			}, value="ajax/companyInformationAct",
			interceptorRefs={
					@InterceptorRef("defaultInterceptorStack")
			}
	)
	public String companyInformationAct() throws Exception{
		try {
			model.setVndHeader(vndService.getVndHeaderById(model.getVndHeader().getVendorId()));
			model.setListVndAddress(vndService.listVndAddress(model.getVndHeader() ));
			model.setListVndTambahan(vndService.listVndTambahan(model.getVndHeader()));
			model.setListVndCompanyType(vndService.listVndCompanyType(model.getVndHeader()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vendor/tabs/companyInformationAct.jsp")
			}, value="ajax/legalInformationAct",
			interceptorRefs={
					@InterceptorRef("defaultInterceptorStack")
			}
	)
	public String legalInformationAct() throws Exception{
		model.setVndHeader(vndService.getVndHeaderById(model.getVndHeader().getVendorId()));
		model.setListVndAddress(vndService.listVndAddress(model.getVndHeader()));
		model.setListVndIjin(vndService.listVndIjin(model.getVndHeader()));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vendor/tabs/manInfAct.jsp")
			}, value="ajax/manInfAct",
			interceptorRefs={
					@InterceptorRef("defaultInterceptorStack")
			}
	)
	public String manInfAct() throws Exception{
		try {
			model.setListVndBoardDireksi(vndService.listVndBard(model.getVndHeader()));
			model.setListVndBoardKomisaris(vndService.listVndBardKomisaris(model.getVndHeader()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vendor/tabs/finInfAct.jsp")
			}, value="ajax/finInfAct",
			interceptorRefs={
					@InterceptorRef("defaultInterceptorStack")
			}
	)
	public String finInfAct() throws Exception{
		model.setVndHeader(vndService.getVndHeaderById(model.getVndHeader().getVendorId()));
		model.setListVndBank(vndService.listVndBank(model.getVndHeader()));
		model.setListVndFinRpt(vndService.listVndFinRpt(model.getVndHeader()));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vendor/tabs/quaInfAct.jsp")
			}, value="ajax/quaInfAct",
			interceptorRefs={
					@InterceptorRef("defaultInterceptorStack")
			}
	)
	public String quaInfAct() throws Exception{
		model.setListVndCert(vndService.listVndCerts(model.getVndHeader()));
		model.setListVndProduk(vndService.listVndProduct(model.getVndHeader()));
		model.setListVndDistrict(vndService.listVndDistric(model.getVndHeader()));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vendor/tabs/resInfAct.jsp")
			}, value="ajax/resInfAct",
			interceptorRefs={
					@InterceptorRef("defaultInterceptorStack")
			}
	)
	public String resInfAct() throws Exception{
		model.setListVndSdm(vndService.listVndSdm(model.getVndHeader()));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vendor/tabs/comFacAct.jsp")
			}, value="ajax/comFacAct",
			interceptorRefs={
					@InterceptorRef("defaultInterceptorStack")
			}
	)
	public String comFacAct() throws Exception{
		model.setListVndEquip(vndService.listVndEquip(model.getVndHeader()));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vendor/tabs/proExpAct.jsp")
			}, value="ajax/proExpAct",
			interceptorRefs={
					@InterceptorRef("defaultInterceptorStack")
			}
	)
	public String proExpAct() throws Exception{
		model.setListVndCv(vndService.listVndCv(model.getVndHeader()));
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS, location="vendor/tabs/legInfAct.jsp")
			}, value="ajax/legInfAct",
			interceptorRefs={
					@InterceptorRef("defaultInterceptorStack")
			}
	)
	public String legInfAct() throws Exception{
		model.setVndHeader(vndService.getVndHeaderById(model.getVndHeader().getVendorId()));
		model.setListVndAkta(vndService.listVndAkta(model.getVndHeader()));
		model.setListVndDomisili(vndService.listVndDomisili(model.getVndHeader()));
		return SUCCESS;
	}

}
