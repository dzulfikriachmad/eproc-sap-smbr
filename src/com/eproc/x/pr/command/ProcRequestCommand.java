package com.eproc.x.pr.command;

import java.io.File;
import java.io.Serializable;
import java.util.List;

import com.eproc.x.rfq.command.TabulasiPenilaian;
import com.orm.model.AdmCostCenter;
import com.orm.model.AdmEmployee;
import com.orm.model.AdmUom;
import com.orm.model.ComCatalog;
import com.orm.model.HistMainComment;
import com.orm.model.HistoryHargaNegosiasi;
import com.orm.model.PrcMainDoc;
import com.orm.model.PrcMainHeader;
import com.orm.model.PrcMainItem;
import com.orm.model.PrcMainPreparation;
import com.orm.model.PrcMainVendor;
import com.orm.model.PrcManualDoc;
import com.orm.model.PrcMsgNegotiation;
import com.orm.tools.AbstractCommand;

public class ProcRequestCommand extends AbstractCommand implements Serializable {
	private PrcMainHeader prcMainHeader;
	private PrcMainItem prcMainItem;
	private AdmEmployee admEmployee;
	private HistMainComment histMainComment;
	
	private List<PrcMainHeader> listPrcMainHeader;
	private List<PrcMainItem> listPrcMainItem;
	private List<AdmUom> listAdmUom;
	private List<AdmCostCenter> listAdmCostCenter;
	private List<ComCatalog> listComCatalog;
	private List<HistMainComment> listHistMainComment;
	
	private File docsKomentar;
	private String docsKomentarFileName;
	private String docsKomentarContentType;
	
	private List<PrcMainVendor> listVendor;
	private PrcMainPreparation prcMainPreparation;
	private List<TabulasiPenilaian> listTabulasi;
	private List<PrcMsgNegotiation> listNegosiasi;
	private List<PrcMainDoc> listPrcMainDoc;
	private List<HistoryHargaNegosiasi> listHargaNegosiasi;
	
	private List<File> prcManDocument;
	private List<String> prcManDocumentFileName;
	private List<String> prcManDocumentCategory;
	private List<String> prcManDocumentDescription;
	private List<PrcManualDoc> listPrcManualDoc;
	
	private Integer year;
	
	public PrcMainHeader getPrcMainHeader() {
		return prcMainHeader;
	}
	
	public void setPrcMainHeader(PrcMainHeader prcMainHeader) {
		this.prcMainHeader = prcMainHeader;
	}
	
	public List<PrcMainHeader> getListPrcMainHeader() {
		return listPrcMainHeader;
	}
	
	public void setListPrcMainHeader(List<PrcMainHeader> listPrcMainHeader) {
		this.listPrcMainHeader = listPrcMainHeader;
	}

	public AdmEmployee getAdmEmployee() {
		return admEmployee;
	}

	public void setAdmEmployee(AdmEmployee admEmployee) {
		this.admEmployee = admEmployee;
	}

	public PrcMainItem getPrcMainItem() {
		return prcMainItem;
	}

	public void setPrcMainItem(PrcMainItem prcMainItem) {
		this.prcMainItem = prcMainItem;
	}

	public List<AdmUom> getListAdmUom() {
		return listAdmUom;
	}

	public void setListAdmUom(List<AdmUom> listAdmUom) {
		this.listAdmUom = listAdmUom;
	}

	public List<AdmCostCenter> getListAdmCostCenter() {
		return listAdmCostCenter;
	}

	public void setListAdmCostCenter(List<AdmCostCenter> listAdmCostCenter) {
		this.listAdmCostCenter = listAdmCostCenter;
	}

	public List<ComCatalog> getListComCatalog() {
		return listComCatalog;
	}

	public void setListComCatalog(List<ComCatalog> listComCatalog) {
		this.listComCatalog = listComCatalog;
	}

	public List<PrcMainItem> getListPrcMainItem() {
		return listPrcMainItem;
	}

	public void setListPrcMainItem(List<PrcMainItem> listPrcMainItem) {
		this.listPrcMainItem = listPrcMainItem;
	}

	public List<HistMainComment> getListHistMainComment() {
		return listHistMainComment;
	}

	public void setListHistMainComment(List<HistMainComment> listHistMainComment) {
		this.listHistMainComment = listHistMainComment;
	}

	public HistMainComment getHistMainComment() {
		return histMainComment;
	}

	public void setHistMainComment(HistMainComment histMainComment) {
		this.histMainComment = histMainComment;
	}

	public File getDocsKomentar() {
		return docsKomentar;
	}

	public void setDocsKomentar(File docsKomentar) {
		this.docsKomentar = docsKomentar;
	}

	public String getDocsKomentarFileName() {
		return docsKomentarFileName;
	}

	public void setDocsKomentarFileName(String docsKomentarFileName) {
		this.docsKomentarFileName = docsKomentarFileName;
	}

	public String getDocsKomentarContentType() {
		return docsKomentarContentType;
	}

	public void setDocsKomentarContentType(String docsKomentarContentType) {
		this.docsKomentarContentType = docsKomentarContentType;
	}

	public List<PrcMainVendor> getListVendor() {
		return listVendor;
	}

	public void setListVendor(List<PrcMainVendor> listVendor) {
		this.listVendor = listVendor;
	}

	public PrcMainPreparation getPrcMainPreparation() {
		return prcMainPreparation;
	}

	public void setPrcMainPreparation(PrcMainPreparation prcMainPreparation) {
		this.prcMainPreparation = prcMainPreparation;
	}

	public List<TabulasiPenilaian> getListTabulasi() {
		return listTabulasi;
	}

	public void setListTabulasi(List<TabulasiPenilaian> listTabulasi) {
		this.listTabulasi = listTabulasi;
	}

	public List<PrcMsgNegotiation> getListNegosiasi() {
		return listNegosiasi;
	}

	public void setListNegosiasi(List<PrcMsgNegotiation> listNegosiasi) {
		this.listNegosiasi = listNegosiasi;
	}

	public List<PrcMainDoc> getListPrcMainDoc() {
		return listPrcMainDoc;
	}

	public void setListPrcMainDoc(List<PrcMainDoc> listPrcMainDoc) {
		this.listPrcMainDoc = listPrcMainDoc;
	}

	public List<HistoryHargaNegosiasi> getListHargaNegosiasi() {
		return listHargaNegosiasi;
	}

	public void setListHargaNegosiasi(List<HistoryHargaNegosiasi> listHargaNegosiasi) {
		this.listHargaNegosiasi = listHargaNegosiasi;
	}

	public List<File> getPrcManDocument() {
		return prcManDocument;
	}

	public void setPrcManDocument(List<File> prcManDocument) {
		this.prcManDocument = prcManDocument;
	}

	public List<String> getPrcManDocumentFileName() {
		return prcManDocumentFileName;
	}

	public void setPrcManDocumentFileName(List<String> prcManDocumentFileName) {
		this.prcManDocumentFileName = prcManDocumentFileName;
	}

	public List<String> getPrcManDocumentCategory() {
		return prcManDocumentCategory;
	}

	public void setPrcManDocumentCategory(List<String> prcManDocumentCategory) {
		this.prcManDocumentCategory = prcManDocumentCategory;
	}

	public List<String> getPrcManDocumentDescription() {
		return prcManDocumentDescription;
	}

	public void setPrcManDocumentDescription(List<String> prcManDocumentDescription) {
		this.prcManDocumentDescription = prcManDocumentDescription;
	}

	public List<PrcManualDoc> getListPrcManualDoc() {
		return listPrcManualDoc;
	}

	public void setListPrcManualDoc(List<PrcManualDoc> listPrcManualDoc) {
		this.listPrcManualDoc = listPrcManualDoc;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}
	
	
}
