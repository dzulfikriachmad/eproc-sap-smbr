package com.eproc.x.pr.action;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.web.util.WebUtils;

import com.eproc.core.BaseController;
import com.eproc.utils.QueryUtil;
import com.eproc.utils.SessionGetter;
import com.eproc.utils.UploadUtil;
import com.eproc.x.pr.command.ProcRequestCommand;
import com.orm.model.AdmDept;
import com.orm.model.AdmEmployee;
import com.orm.model.AdmUser;
import com.orm.model.HistMainComment;
import com.orm.model.PrcMainHeader;
import com.orm.model.PrcMainItem;

import eproc.controller.AdmService;
import eproc.controller.AdmServiceImpl;
import eproc.controller.ComService;
import eproc.controller.ComServiceImpl;
import eproc.controller.PrcService;
import eproc.controller.PrcServiceImpl;
import eproc.controller.RfqServiceImpl;
import eproc.controller.ToolsServiceImpl;

@ParentPackage("eproc")
@Namespace("/")
public class ProcRequestAction extends BaseController {
	private ProcRequestCommand model;
	private AdmService admService;
	private ComService comService;
	private PrcService prcService;

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return model;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		model = new ProcRequestCommand();
		admService = new AdmServiceImpl();
		comService = new ComServiceImpl();
		prcService = new PrcServiceImpl();
	}	
	
	@SuppressWarnings("unchecked")
	@Action(
			results={
					@Result(name=SUCCESS, location="proc/listProcRequest.jsp"),
					@Result(name="createPR", location="proc/setProcRequest.jsp"),
					@Result(name="edit", location="proc/setProcRequest.jsp"),
					@Result(name="view", location="proc/viewProcRequest.jsp"),
					@Result(name="save", location="proc/setProcRequest.jsp"),
					@Result(name="saveAndFinish", location="procurement/request", type="redirectAction"),
			}, value="procurement/request"
	)
	public String procReq() throws Exception{
		try {
			AdmUser admUser = (AdmUser) SessionGetter.getSessionValue(SessionGetter.USER_SESSION);
			if(WebUtils.hasSubmitParameter(request, "createPR")){
				AdmEmployee admEmployee = admService.getAdmEmployeeById(admUser.getId());
				PrcMainHeader prcMainHeader = new PrcMainHeader();
				prcMainHeader.setPpmProsesLevel(1);
				
				model.setPrcMainHeader(prcMainHeader);
				model.setAdmEmployee(admEmployee);
				model.setListAdmUom(admService.listAdmUom());
				model.setListAdmCostCenter(admService.listAdmCostCenterByDept(admEmployee.getAdmDept()));
				return "createPR";
			}else if(WebUtils.hasSubmitParameter(request, "save")){
				
				/** histori **/
				HistMainComment hist = model.getHistMainComment();
				if(model.getDocsKomentar() != null){
					hist.setDocument(UploadUtil.uploadDocsFile(model.getDocsKomentar(), model.getDocsKomentarFileName()));
				}			
				hist.setProses(model.getPrcMainHeader().getAdmProses().getId());
				hist.setAksi("Draft PR");
				hist.setProsesAksi(getText("pr.-1"));
				/** item **/
				List<PrcMainItem> tmpListItem = (List<PrcMainItem>) SessionGetter.getSessionValue("listItem");
				model.setListPrcMainItem(tmpListItem);
				
				PrcMainHeader header =  prcService.savePR(model.getPrcMainHeader(),model.getHistMainComment(),model.getListPrcMainItem());
				addActionMessage("Data PR "+header.getPpmNomorPr() +" Sudah Disimpan");
				return "saveAndFinish";
			}else if(WebUtils.hasSubmitParameter(request, "saveAndFinish")){
				/** histori **/
				HistMainComment hist = model.getHistMainComment();
				if(model.getDocsKomentar() != null){
					hist.setDocument(UploadUtil.uploadDocsFile(model.getDocsKomentar(), model.getDocsKomentarFileName()));
				}
				hist.setAksi("Pembuatan PR");
				hist.setProsesAksi(getText("pr.1"));
				
				/** item **/
				List<PrcMainItem> tmpListItem = (List<PrcMainItem>) SessionGetter.getSessionValue("listItem");
				model.setListPrcMainItem(tmpListItem);
				
				PrcMainHeader header =  prcService.savePRFinish(model.getPrcMainHeader(),model.getHistMainComment(),model.getListPrcMainItem());
				addActionMessage("Data PR "+header.getPpmNomorPr() +" Sudah Disimpan");
				return "saveAndFinish";
			}else{

				if(request.getParameter("id") != null && request.getParameter("id") != ""){
					int id = Integer.parseInt(request.getParameter("id"));
					AdmEmployee admEmployee = admService.getAdmEmployeeById(admUser.getId());
					PrcMainHeader prcMainHeader = prcService.getPrcMainIHeaderById(id);
					model.setPrcMainHeader(prcMainHeader);
					model.setAdmEmployee(admEmployee);
					model.setListAdmUom(admService.listAdmUom());
					model.setListAdmCostCenter(admService.listAdmCostCenterByDept(admEmployee.getAdmDept()));
					model.setListHistMainComment(prcService.listHistMainCommentByPrcHeader(new PrcMainHeader(id)));
					List<PrcMainItem> listItem = prcService.listPrcMainItemByHeader(new PrcMainHeader(id));
					SessionGetter.setSessionValue(listItem, "listItem");
					model.setListPrcMainItem(listItem);
					
					if(prcMainHeader.getPpmProsesLevel() > 1 && prcMainHeader.getAdmProses().getId() == 100){
						return "view";
					}else if(prcMainHeader.getAdmProses().getId() != 100){
						return "view";
					}else{
						return "edit";
					}
				}
			}
			SessionGetter.destroySessionValue("listItem");
			model.setListPrcMainHeader(prcService.listPrcMainHeaderByCurrentUser(SessionGetter.getUser()));
		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
		}
		
		return SUCCESS;
	}
		
	@Action(
			results={
					@Result(name=SUCCESS, location="proc/ajax/listCatalog.jsp")
			}, value="ajax/searchCommodity"
	)
	public String ajaxSearchCommodity() throws Exception{
		model.setListComCatalog(comService.listComCatalog());
		return SUCCESS;
	}
	
	@SuppressWarnings("unchecked")
	@Action(
			results={
					@Result(name=SUCCESS, location="proc/ajax/listItem.jsp")
			},value="ajax/submitPrcMainItem"
	)
	public String ajaxsubmitPrcMainItem() throws Exception{
		List<PrcMainItem> tmpListItem = (List<PrcMainItem>) SessionGetter.getSessionValue("listItem");
		List<PrcMainItem> listItem = new ArrayList<PrcMainItem>();
		
		if(request.getParameter("index") != null && request.getParameter("index") != "" && request.getParameter("act") != null && request.getParameter("act").equals("delete")){
			if(request.getParameter("id") != null && request.getParameter("id") != ""){
				int id = Integer.parseInt(request.getParameter("id"));
				prcService.deletePrcMainItem(prcService.getPrcMainItemById(id));
			}
			
 			int n = 0;
			int target = Integer.parseInt(request.getParameter("index"));
			if(tmpListItem != null){
				for(PrcMainItem i:tmpListItem){
					if(n != target){
						listItem.add(i);
					}
					n = n+1;
				}
			}
			SessionGetter.setSessionValue(listItem, "listItem");
			
			model.setListPrcMainItem(listItem);
			return SUCCESS;
		}
		
		if(tmpListItem != null){
			for(PrcMainItem i:tmpListItem){
				listItem.add(i);
			}
		}
		
		listItem.add(model.getPrcMainItem());
		SessionGetter.setSessionValue(listItem, "listItem");
		
		model.setListPrcMainItem(listItem);
		return SUCCESS;
	}	
	
	@Action(
			results={
					@Result(name="view", location="proc/viewProcRequestApproval.jsp"),
					@Result(name="ok", location="procurement/approval", type="redirectAction"),
					@Result(name=SUCCESS, location="proc/listProcRequestApproval.jsp")
			}, value="procurement/approval"
	)
	public String appPR() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "revisi")){
				model.getHistMainComment().setProsesAksi(getText("pr.-1"));
				model.getHistMainComment().setAksi("Revisi");
				prcService.revisiPR(model.getPrcMainHeader(),model.getHistMainComment(),model.getDocsKomentar(),model.getDocsKomentarFileName());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			}else if( WebUtils.hasSubmitParameter(request, "approve")){
				model.getHistMainComment().setProsesAksi(getText("pr.2"));
				model.getHistMainComment().setAksi("Setuju");
				prcService.setujuPR(model.getPrcMainHeader(),model.getHistMainComment(),model.getDocsKomentar(),model.getDocsKomentarFileName());
				addActionMessage("Data Sudah Disimpan");
				return "ok";
			}else if(request.getParameter("id") != null && request.getParameter("id") != ""){
				int id = Integer.parseInt(request.getParameter("id"));
				PrcMainHeader prcMainHeader = prcService.getPrcMainIHeaderById(id);			
				AdmEmployee admEmployee = admService.getAdmEmployeeById(prcMainHeader.getAdmUserByPpmCreator().getId());
				
				model.setPrcMainHeader(prcMainHeader);
				model.setAdmEmployee(admEmployee);
				
				model.setListHistMainComment(prcService.listHistMainCommentByPrcHeader(new PrcMainHeader(id)));
				 
				List<PrcMainItem> listItem = prcService.listPrcMainItemByHeader(new PrcMainHeader(id));
				model.setListPrcMainItem(listItem);
				return "view";
			}else{
				model.setListPrcMainHeader(prcService.listPrcMainHeaderByCurrentUser(SessionGetter.getUser()));
				return SUCCESS;
			}

		} catch (Exception e) {
			e.printStackTrace();
			addActionError(e.getMessage());
		}
		return SUCCESS;
				
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="proc/listPengadaanManual.jsp"),
					@Result(name="view",location="proc/viewPengadaanManual.jsp"),
					@Result(name="ok",location="procurement/manual", type="redirectAction"),
			},value="procurement/manual"
			)
	public String pengadaanManual() throws Exception{
		try {
			if(WebUtils.hasSubmitParameter(request, "proses")){
				PrcMainHeader prcMainHeader = prcService.getPrcMainHeaderSPPHById(model.getPrcMainHeader().getPpmId());
				AdmEmployee admEmployee = SessionGetter.getUser().getAdmEmployee();
				model.setPrcMainHeader(prcMainHeader);
				model.setAdmEmployee(admEmployee);
				model.setListHistMainComment(prcService.listHistMainCommentByPrcHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId())));
				List<PrcMainItem> listItem = prcService.listPrcMainItemByHeader(new PrcMainHeader(model.getPrcMainHeader().getPpmId()));
				model.setListPrcMainItem(listItem);
				model.setListVendor(prcService.listMitraKerjaByHeader(prcMainHeader));
				model.setPrcMainPreparation(prcService.getPrcMainPreparationByHeader(prcMainHeader));
				model.setListTabulasi(new RfqServiceImpl().getListTabulasi(QueryUtil.tabulasiEvaluasi(prcMainHeader.getPpmId())));
				model.setListNegosiasi(new RfqServiceImpl().listNegosiasiByMainHeader(model.getPrcMainHeader()));
				model.setListPrcManualDoc(prcService.listPrcManualDocByHeader(prcMainHeader));
				model.setListHargaNegosiasi(new RfqServiceImpl().listHargaNegosiasi(prcMainHeader));				
				return "view";
			}else if(WebUtils.hasSubmitParameter(request, "manual")){
				if(model.getDocsKomentar()!=null){
					String docName = UploadUtil.uploadDocsFile(model.getDocsKomentar(), model.getDocsKomentarFileName());
					model.getHistMainComment().setDocument(docName);
				}
				prcService.pengadaanManual(model.getPrcMainHeader(),model.getHistMainComment(), model.getPrcManDocument(), 
						model.getPrcManDocumentCategory(), model.getPrcManDocumentDescription(), model.getPrcManDocumentFileName(), 
						model.getDocsKomentar(),model.getDocsKomentarFileName());
				addActionMessage("Data berhasil disimpan");
				return "ok";
			}
		} catch (Exception e) {
			addActionError(e.getMessage());
			e.printStackTrace();
			return "ok";
		}
		return SUCCESS;
	}
	
	@Action(
			results={
					@Result(name=SUCCESS,location="proc/ajax/pengadaanManual.jsp")
			},value="ajax/searchPengadaanManual"
			)
	public String searchPrManual() throws Exception{
		try {
			model.setListPrcMainHeader(new ToolsServiceImpl().searchPrcMainHeaderManual(model.getPrcMainHeader()));
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	
	
}
