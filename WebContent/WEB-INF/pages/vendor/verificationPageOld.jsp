<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Daftar Verifikasi Mitra Lama</title>
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	
	<script type="text/javascript">
		$(document).ready(function(){
			$(".chzn").chosen();
			// FORM VALIDATION
			$("#procForm").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler:function(form){
					loadingScreen();
					form.submit();
				}
			});
			
			
		});
		
		//regCancel Action
		function regCancelVerification(){
			jQuery.facebox.close();
			window.location = "<s:url action="vendor/activation" />"; 
		}
		
		function selectMitraKerja(obj){
			if(obj.value==1){
				$('#smkNoDiv').show();
				$('#jdeUserDiv').hide();
				$('#jdePassDiv').hide();
			}else if(obj.value==2){
				$('#smkNoDiv').hide();
				$('#jdeUserDiv').show();
				$('#jdePassDiv').show();
			}
		}
	</script>
	
	<!-- Tabs Panel -->
	<script type="text/javascript">
		$(function() {
			$(".tabs").tabs();
		});
		//First Tab Load
		loadPage("<s:url action="ajax/companyInformation" />?tmpVndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");
	</script>
	<link rel="stylesheet" href="${contextPath}/assets/css/tabs-toggle.css" type="text/css" media="screen" />
	
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="dashboard">Home</s:a></li> 
	        <li>Vendor Management</li> 
	        <li><s:a action="vendor/activation">Aktivasi Mitra Kerja</s:a></li>
	        
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
		
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
					
			<s:form action="vendor/verificationOld" method="post" id="procForm" name="procForm">
				<s:hidden name="tmpVndHeader.vendorId" />
				<!-- Nama Perusahaan -->
				<div class="box">
				<div class="title">
					Detail Vendor
					<span class="hide"></span>
				</div>
				<div class="content nopadding" style="padding: 0px;">
					<div class="tabs">
						<div class="tabmenu">
							<ul> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/companyInformation" />?tmpVndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");'>Data Utama</a></li> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/companyInformationAct" />?vndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");' style="color:red">Data Utama Lama</a></li> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/legInf" />?tmpVndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");' >Data Legal</a></li>
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/legInfAct" />?vndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");' style="color:red">Data Legal Lama</a></li> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/manInf" />?tmpVndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");'>Pengurus</a></li> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/manInfAct" />?tmpVndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");' style="color:red">Pengurus Lama</a></li> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/finInf" />?tmpVndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");'>Keuangan</a></li>
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/finInfAct" />?vndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");' style="color:red">Keuangan Lama</a></li> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/quaInf" />?tmpVndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");'>Barang / Jasa & Sertifikasi</a></li>
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/quaInfAct" />?vndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");' style="color:red">Barang / Jasa & Sertifikasi Lama</a></li> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/resInf" />?tmpVndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");'>SDM</a></li>
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/resInfAct" />?vndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");' style="color:red">SDM Lama</a></li> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/comFac" />?tmpVndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");'>Fasilitas / Peralatan</a></li>
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/comFacAct" />?vndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");' style="color:red">Fasilitas / Peralatan Lama</a></li> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/proExp" />?tmpVndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");'>Pengalaman Proyek</a></li>
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/proExpAct" />?vndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");' style="color:red">Pengalaman Proyek Lama</a></li> 
							</ul>
						</div>
						
						<div id="tabsContent"> 
						</div>
								
					</div>
				</div>
			</div>
	
				<div class="uibutton-toolbar extend">
					<s:submit name="ok" value="Verifikasi dan Aktifkan" cssClass="uibutton"></s:submit>
					<a href="javascript:void(0)" class="uibutton" onclick="confirmPopUp('regCancelVerification();', 'Batal', 'Apakah anda yakin untuk membatalkan transaksi ?', 'Ya', 'Tidak');"><s:text name="cancel"></s:text> </a>
				</div>
						
			</s:form>
				
		</div>
	</div>
</body>
</html>