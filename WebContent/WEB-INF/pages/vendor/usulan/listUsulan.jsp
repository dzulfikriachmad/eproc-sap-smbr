<%@ include file="/include/definitions.jsp" %>
<!DOCTYPE html> 
<html lang="id">
<head>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="dashboard">Home</s:a></li> 
	        <li>Daftar Usulan Mitra Kerja</li> 
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
	
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
					
			<s:form>
				<s:hidden name="usulan.id" id="usulan.id"></s:hidden>
				<div class="box">
					<div class="title">
						Daftar Usulan Mitra Kerja
						<span class="hide"></span>
					</div>
					<div class="content">
						<div style="text-align: center; margin-bottom: 10px;">
							<div class="uibutton-toolbar extend">
								<s:url action="vendor/formUsulan" id="url"></s:url>
								<a <c:if test="${create !=1 }"> style="visibility:hidden"</c:if> href='${url }' class='uibutton icon add' >Tambah Data Usulan</a>
							</div>
						</div>
						
						<display:table name="${listUsulan }" id="s" excludedParams="*" style="width: 100%;" class="all style1">
							<display:setProperty name="paging.banner.full" value=""></display:setProperty>
							<display:setProperty name="paging.banner.first" value=""></display:setProperty>
							<display:setProperty name="paging.banner.last" value=""></display:setProperty>
							
							<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
							<display:column title="Nomor Usulan" total="true" style="width: 200px;" class="center">
								${s.nomorUsulan }
							</display:column>
							<display:column title="Nama Usulan" style="width: 150px; " class="center">${s.namaUsulan }</display:column>
							<display:column title="Keterangan" style="width: 150px;" class="center">${s.keterangan }</display:column>
							<display:column title="Aksi" class="center">
									<input type="submit" <c:if test="${update !=1 }"> style="visibility:hidden"</c:if> class="uibutton icon edit" name="edit" onclick="document.getElementById('usulan.id').value=${s.id}" value="Edit" title="Edit Data"/>
									<input type="submit" <c:if test="${delete !=1 }"> style="visibility:hidden"</c:if> class="uibutton icon edit" name="delete" onclick="document.getElementById('usulan.id').value=${s.id}" value="X" title="Hapus Data"/>
							</display:column>
						</display:table>
					</div>
				</div>
				
			</s:form>
		</div>
	</div>
</body>
</html>