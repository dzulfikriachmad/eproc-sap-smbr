<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	
	<script type="text/javascript">	
		$(document).ready(function(){
			// FORM VALIDATION
			$("#procForm").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler:function(form){
					loadingScreen();
					form.submit();
				}
			});
		});
		
		//regCancel Action
		function regCancel(){
			jQuery.facebox.close();
			window.location = "<s:url action="vendor/monitorUsulan" />"; 
		}
		
		$(document).ready(function() {
			// DATATABLE
		    $('table.history').dataTable({
		        "bInfo": false,
		        "iDisplayLength": 200,
		        "aLengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
		        "sPaginationType": "full_numbers",
		        "bPaginate": true,
		        "sDom": '<f>t<pl>'
		    });
		});
		
	</script>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="dashboard">Home</s:a></li> 
	        <li>Daftar Usulan Mitra Kerja</li> 
	        
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
    
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
					
			<s:form action="vendor/monitorUsulan" id="procForm" name="procForm" enctype="multipart/form-data" method="post">
				<s:hidden name="usulan.id" />
				<s:hidden name="usulan.createdDate" />
				<s:hidden name="usulan.createdBy" />
				<s:hidden name="usulan.tingkat" />
				<s:hidden name="usulan.nomorUsulan"></s:hidden>
				<s:hidden name="usulan.keterangan"></s:hidden>
				<s:hidden name="usulan.namaUsulan"></s:hidden>
				<s:hidden name="usulan.tipe"></s:hidden>
				<s:hidden name="usulan.kantor.id"></s:hidden>
				<s:hidden name="usulan.creator.id"></s:hidden>
				<div class="box">
					<div class="title">
						Header
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle" >
							<tr>
								<td width="150px">Nomor Usulan</td>
								<td>
									${usulan.nomorUsulan}
								</td>
							</tr>
							<tr>
								<td width="150px">Nama Usulan</td>
								<td>
									${usulan.namaUsulan }
								</td>
							</tr>
							<tr>
								<td width="150px">Deskripsi Usulan</td>
								<td>
									${usulan.keterangan }
								</td>
							</tr>
						</table>
						
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Mitra Kerja
						<span class="hide"></span>
					</div>
					<div class="content">
						<display:table name="${listUsulanDetail }" id="s" excludedParams="*" style="width: 100%;" class="all style1">
							<display:setProperty name="paging.banner.full" value=""></display:setProperty>
							<display:setProperty name="paging.banner.first" value=""></display:setProperty>
							<display:setProperty name="paging.banner.last" value=""></display:setProperty>
							
							<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
							<display:column title="Nama Mitra Kerja" total="true" style="width: 200px;">
								<a href="javascript:void(0)" onclick="load_into_box('<s:url action="ajax/vendor/detail"/>','tmpVndHeader.vendorId=${s.vendor.vendorId }')">
									${fn:toUpperCase(s.vendor.vendorPrefix) }.
									${fn:toUpperCase(s.vendor.vendorName) }
									${fn:toUpperCase(s.vendor.vendorSuffix) }
								</a>
								<input type="hidden" name="listUsulanDetail[${s_rowNum-1 }].id" value="${s.id }"/>
								<input type="hidden" name="listUsulanDetail[${s_rowNum-1 }].vendor.vendorId" value="${s.vendor.vendorId }"/>
								<input type="hidden" name="listUsulanDetail[${s_rowNum-1 }].usulan.id" value="${s.usulan.id }"/>
								<input type="hidden" name="listUsulanDetail[${s_rowNum-1 }].status" value="${s.status}"/>
							</display:column>
							<display:column title="Alamat">
								${fn:toUpperCase(s.vendor.vendorNpwpAddress) }
							</display:column>
						</display:table>
					</div>
				</div>

				<div class="box">
					<div class="title">
						Daftar Komentar
						<span class="hide"></span>
					</div>
					<div class="content">
						<display:table name="${listUsulanHistory }" id="k" excludedParams="*" style="width: 100%;" class="history style1">
							<display:column title="No." style="width: 20px; text-align: center;">${k_rowNum}</display:column>
							<display:column title="Nama" style="width: 100px;" class="center">${k.nama }</display:column>
							<display:column title="Posisi" style="width: 100px;" class="center">${k.posisi }</display:column>
							<display:column title="Proses" style="width: 100px;" class="center">${k.proses }</display:column>
							<display:column title="Tgl Dibuat" style="width: 100px;" class="center">${k.createdDate }</display:column>
							<display:column title="Tgl Eksekusi" style="width: 100px;" class="center">${k.updatedDate }</display:column>
							<display:column title="Aksi" style="width: 100px;" class="center">${k.aksi }</display:column>
							<display:column title="Komentar" class="center" style="width: 250px;">${k.komentar }</display:column>
							<display:column title="Dokumen" class="center" style="width: 200px;">
								
							</display:column>
						</display:table>
					</div>
				</div>
				<div class="uibutton-toolbar extend">
					<a href="javascript:void(0)" class="uibutton" onclick="confirmPopUp('regCancel();', 'Batal', 'Apakah anda yakin untuk membatalkan transaksi ?', 'Ya', 'Tidak');"><s:text name="cancel"></s:text> </a>
				</div>
						
			</s:form>
			
		</div>
	</div>
</body>
</html>