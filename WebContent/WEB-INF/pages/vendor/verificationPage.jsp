<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Vendor Verification List </title>
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	
	<script type="text/javascript">
		//]Cancel Action
		function regCancelVerification(){
			
			jQuery.facebox.close();
			window.location = "<s:url action="vendor/verification" />"; 
		}
	</script>
	
	<!-- Tabs Panel -->
	<script type="text/javascript">
		$(function() {
			$(".tabs").tabs();
		});
		//First Tab Load
		loadPage("<s:url action="ajax/companyInformation" />?tmpVndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");
	</script>
	<link rel="stylesheet" href="${contextPath}/assets/css/tabs-toggle.css" type="text/css" media="screen" />
	
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="dashboard">Home</s:a></li> 
	        <li>Vendor Management</li> 
	        <li>Verifikasi Mitra Kerja</li> 
	        <li><s:a action="vnd/tos">Mitra Kerja Baru</s:a></li>
	        
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
		
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
			
			<div class="box">
				<div class="title">
					Detail Vendor
					<span class="hide"></span>
				</div>
				<div class="content nopadding" style="padding: 0px;">
					<div class="tabs">
						<div class="tabmenu">
							<ul> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/companyInformation" />?tmpVndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");'>Data Utama</a></li> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/legInf" />?tmpVndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");' >Data Legal</a></li> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/manInf" />?tmpVndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");'>Pengurus</a></li> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/finInf" />?tmpVndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");'>Keuangan</a></li> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/quaInf" />?tmpVndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");'>Barang / Jasa & Sertifikasi</a></li> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/resInf" />?tmpVndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");'>SDM</a></li> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/comFac" />?tmpVndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");'>Fasilitas / Peralatan</a></li> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/proExp" />?tmpVndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");'>Pengalaman Proyek</a></li> 
							</ul>
						</div>
						
						<div id="tabsContent">
							 
						</div>
								
					</div>
				</div>
			</div>
				
					
			<s:form action="vendor/verification">
				<s:hidden name="tmpVndHeader.vendorId" />
				<!-- Nama Perusahaan -->
				<div class="box">
					<div class="title">
						Checklist Kelengkapan Persyaratan Administrasi 
						<span class="hide"></span>
					</div>
					<div class="content">
						
							<s:set id="k" value="0"></s:set>
							<display:table name="${listTmpVndDoc }" id="s" excludedParams="*" style="width: 100%;" class="style1">	
									<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}
									
										<input type="hidden" name="listTmpVndDoc[${k }].id" value="${s.id }"/>
										<input type="hidden" name="listTmpVndDoc[${k }].docName" value="${s.docName }"/>
										<input type="hidden" name="listTmpVndDoc[${k }].admDoc.id" value="${s.admDoc.id }"/>
										<input type="hidden" name="listTmpVndDoc[${k }].tmpVndHeader.vendorId" value="${s.tmpVndHeader.vendorId }"/>
									
									</display:column>
									<display:column title="Persyaratan Administrasi" total="true" style="width: 250px;">${s.docName }	</display:column>
									<display:column title="Status" style="width: 250px; " class="center">
										<s:radio name="listTmpVndDoc[%{#k}].docStatus" list="#{1:'Ada', 2:'Tidak Ada', 3:'Perlu Konfirmasi'}" />
									</display:column>
									<display:column title="Catatan" style="width: 300px;" class="center">
										<s:textarea cssClass="grow" cssStyle="width: 295px;" name="listTmpVndDoc[%{#k}].docRemark"></s:textarea>
									</display:column>
									<s:set var="k" value="#k+1"></s:set>
							</display:table>
						
						</div>
				</div>
			
					
				<div class="uibutton-toolbar extend">
					<s:submit name="save" key="save" cssClass="uibutton" onclick="loadingScreen();"></s:submit>
					<s:submit name="approve" value="Setuju" cssClass="uibutton" onclick="loadingScreen();"></s:submit>	
					<s:submit name="routeToVendor" value="Kembalikan ke Vendor" cssClass="uibutton" onclick="loadingScreen();"></s:submit>
					<a href="javascript:void(0)" class="uibutton" onclick="confirmPopUp('regCancelVerification();', 'Batal', 'Apakahanda yakin untuk membatalkan transaksi ?', 'Ya', 'Tidak');"><s:text name="cancel"></s:text> </a>
				</div>
						
			</s:form>
				
		</div>
	</div>
</body>
</html>