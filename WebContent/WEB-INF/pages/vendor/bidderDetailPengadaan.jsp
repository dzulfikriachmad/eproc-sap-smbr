			<%@ include file="/include/definitions.jsp" %>
			<script type="text/javascript">
				   $(document).ready(function() {
					// DATATABLE
					 $('table.detail').dataTable({
				        "bInfo": false,
				        "iDisplayLength": 10,
				        "aLengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
				        "sPaginationType": "full_numbers",
				        "bPaginate": true,
				        "sDom": '<f>t<pl>'
				        });
					});
			</script>
			<center>
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>		
			<s:form action="report/permintaan" method="post">
			<s:hidden name="prcMainHeader.ppmId" id="ppmId"></s:hidden>	
			<display:table name="${listBidderDetail }" id="s"  excludedParams="*" style="width: 100%;" class="detail style1">
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column title="Nomor Spph" style=" text-align:center" class="center">${s.nomorRfq}</display:column>
				<display:column title="Nama Pengadaan" style=" text-align:center " >${s.judul}</display:column>
				<display:column title="OE" style=" text-align:right " ><fmt:formatNumber> ${s.oe}</fmt:formatNumber></display:column>
				<display:column title="Realisasi" style=" text-align:right " >${s.realisasi}</display:column>
				<display:column title="" media="html" style="width: 80px;" class="center">
					<input type="submit" name="proses" value="Proses"onclick="$('#ppmId').val(${s.ppmId});loadingScreen();" class="uibutton" />
				</display:column>
			</display:table>
			</s:form>
			</center>
			
			<script>
				$(document).ready(function(){	
					// SYSTEM MESSAGES
					$(".message").click(function () {
				      $(this).fadeOut();
				    });
				});
			</script>