<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Daftar Aktivasi Mitra Kerja</title>
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	
	<script type="text/javascript">
		//regCancel Action
		function regCancelVerification(){
			
			jQuery.facebox.close();
			window.location = "<s:url action="vendor/verification" />"; 
		}
		
		function setVendorId(id){
			//alert(id);
			$('#vendorId').val(id);
		}
	</script>
	
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="dashboard">Home</s:a></li> 
	        <li>Vendor Management</li> 
	        <li><s:a action="vendor/activation">Persetujuan Mitra Kerja</s:a></li>
	        
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
		
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
					
			<s:hidden name="vndHeader.vendorId" />
			<!-- Nama Perusahaan -->
			<div class="box">
				<div class="title center">
					Daftar Vendor
					<span class="hide"></span>
				</div>
				<div class="content">
					<s:form>
						<s:hidden name="tmpVndHeader.vendorId" id="vendorId"></s:hidden>
						<display:table name="${listTmpVndHeader }" id="s" excludedParams="*" style="width: 100%;" class="all style1">
								<display:setProperty name="paging.banner.full" value=""></display:setProperty>
								<display:setProperty name="paging.banner.first" value=""></display:setProperty>
								<display:setProperty name="paging.banner.last" value=""></display:setProperty>
								<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
								<display:column title="Nama Vendor" total="true" style="width: 400px;">${fn:toUpperCase(s.vendorName)}</display:column>
								<display:column title="CP" style="width: 250px; " class="center">${s.vendorContactName }</display:column>
								<display:column title="Telp." style="width: 150px;" class="center">${s.vendorContactPhone }</display:column>
								
								<display:column title="Aksi" media="html" style="width: 60px;" class="center">
									<div class="uibutton-group">
										<input type="submit" name="activation" value="Aktivasi" onclick="setVendorId('${s.vendorId }');" class="uibutton" />
										
									</div>
								</display:column>
							
						</display:table>
					</s:form>
				</div>
			</div>
				
		</div>
	</div>
</body>
</html>