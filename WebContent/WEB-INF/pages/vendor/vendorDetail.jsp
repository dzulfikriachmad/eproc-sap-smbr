<%@ include file="/include/definitions.jsp" %>
	<script type="text/javascript">
		
		$(function() {
			$(".tabs").tabs();
		});
		//First Tab Load
		loadPage("<s:url action="ajax/companyInformation" />?tmpVndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");
	</script>
	<link rel="stylesheet" href="${contextPath}/assets/css/tabs-toggle.css" type="text/css" media="screen" />
<div class='fbox_header'>Vendor Detail</div>
    <div class='fbox_container' style="background-color: white;">
        <div class='fbox_content'>
            <div class="box" >
				<div class="title">
					Detail Vendor
					<span class="hide"></span>
				</div>
				<div class="content nopadding" style="padding: 0px;">
					<div class="tabs">
						<div class="tabmenu">
							<ul> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/companyInformation" />?tmpVndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");'>Data Utama</a></li> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/legInf" />?tmpVndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");' >Data Legal</a></li> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/manInf" />?tmpVndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");'>Pengurus</a></li> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/finInf" />?tmpVndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");'>Keuangan</a></li> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/quaInf" />?tmpVndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");'>Barang / Jasa & Sertifikasi</a></li> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/resInf" />?tmpVndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");'>SDM</a></li> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/comFac" />?tmpVndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");'>Fasilitas / Peralatan</a></li> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/proExp" />?tmpVndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");'>Pengalaman Proyek</a></li>  
							</ul>
						</div>
						
						<div id="tabsContent" style="width: 580px">
							 
						</div>
								
					</div>
				</div>
			</div>
        </div>
        <div class='fbox_footer'>
            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()'>Tutup</a>
        </div>
    </div>
    