<%@ include file="/include/definitions.jsp" %>

<script type="text/javascript">
    Cufon.replace('caption', {fontFamily: 'Caviar Dreams'});
</script>
<style>caption{font-size: 19px; color: #FF9E0D; text-align: left;}</style>
	
<div class="tab center">
	<display:table name="${listKomentar }" id="k" pagesize="1000"
		excludedParams="*" style="width: 100%;" class="style1">
		<display:column title="No." style="width: 20px; text-align: center;">${k_rowNum}</display:column>
		<display:column title="Nama" style="width: 100px;" class="center">${k.nama }</display:column>
		<display:column title="Posisi" style="width: 100px;" class="center">${k.posisi }</display:column>
		<display:column title="Proses" style="width: 100px;" class="center">${k.proses }</display:column>
		<display:column title="Tgl Pembuatan" style="width: 100px;"
			class="center">
			<fmt:formatDate value="${k.createdDate }" pattern="dd.MM.yyyy HH:mm" />
		</display:column>
		<display:column title="Tgl Update" style="width: 100px;"
			class="center">
			<fmt:formatDate value="${k.updatedDate }" pattern="dd.MM.yyyy HH:mm" />
		</display:column>
		<display:column title="Aksi" class="center" style="width: 250px;">${k.aksi }</display:column>
		<display:column title="Komentar" class="center" style="width: 250px;">${k.komentar }</display:column>
	</display:table>
</div>