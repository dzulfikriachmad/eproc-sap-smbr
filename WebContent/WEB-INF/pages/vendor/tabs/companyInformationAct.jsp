<%@ include file="/include/definitions.jsp" %>

<script type="text/javascript">
    Cufon.replace('caption', {fontFamily: 'Caviar Dreams'});
</script>
<style>caption{font-size: 19px; color: #FF9E0D; text-align: left;}</style>
	
<div class="tab center">
	<table class="style1" style="margin-top: 10px;background-color:white:widt;100%">
		<caption>Nama Perusahaan</caption>
		<thead>
			<tr>
				<th>Prefiks</th>
				<th>Prefiks Lainnya</th>
				<th>Nama Perusahaan</th>
				<th>Suffiks</th>
				<th>Suffiks Lainnya</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="center">${vndHeader.vendorPrefix }</td>
				<td class="center">${vndHeader.vendorPrefixOther }</td>
				<td class="center">${vndHeader.vendorName }</td>
				<td class="center">${vndHeader.vendorSuffix }</td>
				<td class="center">${vndHeader.vendorSuffixOther }</td>
		</tbody>
	</table>
	
	<table class="style1" style="margin-top: 10px;background-color:white">
		<caption>Kontak Person</caption>
		<thead>
			<tr>
				<th>Nama</th>
				<th>Jabatan</th>
				<th>No. Telp</th>
				<th>Fax</th>
				<th>Alamat Email</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="center">${vndHeader.vendorContactName }</td>
				<td class="center">${vndHeader.vendorContactPos }</td>
				<td class="center">${vndHeader.vendorContactPhone }</td>
				<td class="center">${vndHeader.vendorContactFax }</td>
				<td class="center">${vndHeader.vendorContactEmail }</td>
		</tbody>
	</table>
	<br/>
	<table class="style1" style="width:100%">
		<caption>Tipe Perusahaan</caption>
		<thead>
			<tr>
				<th>-</th>
				<th>-</th>
			</tr>
		</thead>
		
	</table>
		
	<display:table name="${listVndAddress }" id="s" pagesize="10" excludedParams="*" style="width: 100%; margin-top: 10px;background-color:white" class="style1">
		<display:caption>Kontak Perusahaan</display:caption>
		<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
		<display:column title="Provinsi" style="width: 100px;">${s.addProvince }</display:column>
		<display:column title="Kota">${s.addCity }</display:column>
		<display:column title="Alamat" style="width: 200px;">${s.addAddress  }, ${s.addPostCode }</display:column>
		<display:column title="Phone" style="width: 130px;">${s.addPhone1 }</display:column>
	</display:table>
	
	<display:table name="${listVndTambahan }" id="s" pagesize="100" requestURI="companyInformation.jwebs" excludedParams="*" style="width: 100%;background-color:white" class="style1">
				<display:caption>Afiliasi Perusahaan</display:caption>
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column title="Jenis" total="true" style="width: 100px;">${s.tipe }</display:column>
				<display:column title="Nama" style="width: 200px;">${s.nama  }</display:column>
				<display:column title="Alamat" style="width: 200px;">${s.alamat  }, ${s.kodePos }</display:column>
			</display:table>
	<div class="clear"></div>
</div>