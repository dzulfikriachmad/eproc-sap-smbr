<%@ include file="/include/definitions.jsp" %>

<script type="text/javascript">
    Cufon.replace('caption', {fontFamily: 'Caviar Dreams'});
</script>
<style>caption{font-size: 19px; color: #FF9E0D; text-align: left;}</style>
	
<div class="tab center">
	<display:table name="${listTmpVndAkta }" id="s" pagesize="1000" requestURI="legalInformation.jwebs" excludedParams="*" style="width: 100%;background-color:white" class="style1">
				<display:caption>Data Akta Perusahaan</display:caption>
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column title="Tipe" total="true" style="width: 40px;"  class="center">
					<c:if test="${s.aktaType == 1 }">PENDIRIAN</c:if>
					<c:if test="${s.aktaType == 2 }">PERUBAHAN</c:if>
				</display:column>
				<display:column title="Nomor Akta" style="width: 140px; ">${s.aktaNomor }</display:column>
				<display:column title="Pembuatan Akta" style="width: 110px;" class="center"><fmt:formatDate value="${s.aktaCreatedDate }" pattern="dd-MMM-yyyy"/> </display:column>
				<display:column title="Berita Negara" class="center" style="width: 100px;"><fmt:formatDate value="${s.aktaBeritaNegara }" pattern="dd-MMM-yyyy"/> </display:column>
				<display:column title="Pengesahan" style="width: 110px;" class="center"><fmt:formatDate value="${s.aktaPengesahanHakim }" pattern="dd-MMM-yyyy"/> </display:column>
				<display:column title="Nama Notaris" style="width: 130px;" class="center">${s.aktaNotarisName }</display:column>
				<display:column title="Aksi" media="html" style="width: 60px;" class="center">
					
				</display:column>
			</display:table>
			
			<display:table name="${listTmpVndDomisili }" id="s" pagesize="1000" requestURI="legalInformation.jwebs" excludedParams="*" style="width: 100%;background-color:white" class="style1">
				<display:caption>Domisili Perusahaan</display:caption>
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column title="No. Dom" total="true" style="width: 100px;"  class="center">${s.domNomorDomisili }</display:column>
				<display:column title="Tanggal Dom" style="width: 100px; "><fmt:formatDate value="${s.domTglDomisili }" pattern="dd-MMM-yyyy"/></display:column>
				<display:column title="Kadaluarsa" style="width: 100px;" class="center"><fmt:formatDate value="${s.domTglKadaluarsa }" pattern="dd-MMM-yyyy"/> </display:column>
				<display:column title="Alamat" class="center" style="width: 200px;">${s.domAddress }, ${s.domPostCode }</display:column>
				<display:column title="Kota" style="width: 100px;" class="center">${s.comCity }, ${s.domProvince }</display:column>
				<display:column title="Negara" style="width: 90px;" class="center">${s.admCountry.countryName }</display:column>
				<display:column title="Aksi" media="html" style="width: 60px;" class="center">
					
				</display:column>
			</display:table>

						<table class="form formStyle" style="width:100%">
						<caption>NPWP</caption>
							<tr>
								<td width="180px">No. (*)</td>
								<td colspan="3">
									${tmpVndHeader.vendorNpwpNo }
								</td>
							</tr>
							<tr>
								<td>Provinsi (*)</td>
								<td colspan="3">${tmpVndHeader.vendorNpwpProvince }</td>
							</tr>
							<tr>
								<td>Kota (*)</td>
								<td colspan="3">${tmpVndHeader.vendorNpwpCity }</td>
							</tr>
							<tr>
								<td >Alamat (Sesuai NPWP) (*)</td>
								<td colspan="3">${tmpVndHeader.vendorNpwpAddress}</td>
							</tr>
							<tr>
								<td>Kode Pos</td>
								<td colspan="3">${tmpVndHeader.vendorNpwpPostCode }</td>
							</tr>
							<tr id="noPkp">
								<td >No. PKP</td>
								<td colspan="3">${tmpVndHeader.vendorNpwpPkpNo }</td>
							</tr>
						</table>

					
				

						<table class="form formStyle" style="width:100%">
							<caption>SIUP</caption>
							<tr>
								<td width="180px">Dikeluarkan Oleh (*)</td>
								<td colspan="3">${tmpVndHeader.vendorSiupIssued }</td>
							</tr>
							<tr>
								<td>Nomor (*)</td>
								<td width="250px">${tmpVndHeader.vendorSiupNo }</td>
								<td style="width: 180px; text-align: right;">Jenis Usaha (*)&nbsp;</td>
								<td>
									${tmpVndHeader.vendorSiupTipe }
								</td>
							</tr>
							<tr>
								<td>Berlaku Mulai (*)</td>
								<td width="250px"><fmt:formatDate value="${tmpVndHeader.vendorSiupFrom }" pattern="dd.MM.yyyy"/> </td>
								<td style="width: 180px; text-align: right;">Sampai (*)&nbsp;</td>
								<%--<td><fmt:formatDate value="${tmpVndHeader.vendorSiupTo }" pattern="dd.MM.yyyy"/> </td>--%>
								<td>-</td>
							</tr>						
						</table>
					
					
						<table class="form formStyle" style="width:100%">
							<caption>TDP</caption>
							<tr>
								<td width="180px">Dikeluarkan Oleh (*)</td>
								<td colspan="3">${tmpVndHeader.vendorTdpIssued }</td>
							</tr>
							<tr>
								<td>Nomor (*)</td>
								<td colspan="3">${tmpVndHeader.vendorTdpNo }</td>
								
							</tr>
							<tr>
								<td>Berlaku Mulai (*)</td>
								<td width="250px"><fmt:formatDate value="${tmpVndHeader.vendorTdpFrom}" pattern="dd.MM.yyyy"/> </td>
								<td style="width: 180px; text-align: right;">Sampai (*)&nbsp;</td>
								<td><fmt:formatDate value="${tmpVndHeader.vendorTdpTo }" pattern="dd.MM.yyyy"/></td>
							</tr>						
						</table>
						
						<display:table name="${listIjin }" id="s" pagesize="10" requestURI="legalInformation.jwebs" excludedParams="*" style="width: 100%;" class="style1">
							<display:caption>Ijin Lain-Lain (Opsional)</display:caption>
							<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
							<display:column title="Tipe" total="true" style="width: 40px;"  class="center">
								${s.vndIjinTipeName }
							</display:column>
							<display:column title="Nomor" style="width: 140px; ">${s.vndIjinNo }</display:column>
							<display:column title="Dikeluarkan Oleh" style="width: 110px;" class="center">
								${s.vndIjinIssued }
							</display:column>
							<display:column title="Berlaku Mulai" class="center" style="width: 100px;"><fmt:formatDate value="${s.vndIjinStart }" pattern="dd-MMM-yyyy"/> </display:column>
							<display:column title="Sampai" style="width: 110px;" class="center"><fmt:formatDate value="${s.vndIjinEnd }" pattern="dd-MMM-yyyy"/> </display:column>
							<display:column title="" media="html" style="width: 60px;" class="center">
								
							</display:column>
						</display:table>
	
	<div class="clear"></div>
</div>