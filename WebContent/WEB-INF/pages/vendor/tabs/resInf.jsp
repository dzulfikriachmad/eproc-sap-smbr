<%@ include file="/include/definitions.jsp" %>

<script type="text/javascript">
    Cufon.replace('caption', {fontFamily: 'Caviar Dreams'});
</script>
<style>caption{font-size: 19px; color: #FF9E0D; text-align: left;}</style>
	
<div class="tab center">
	<display:table name="${listTmpVndSdm }" id="s" pagesize="1000" requestURI="resourcesInformation.jwebs" excludedParams="*" style="width: 100%;background-color:white" class="style1">
				<display:caption>SDM</display:caption>
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column title="Tipe" style="width: 200px; " class="center">
					<c:if test="${s.sdmTipe == 1}">
						TETAP
					</c:if>
					<c:if test="${s.sdmTipe == 2}">
						OUTSOURCING
					</c:if>
					<c:if test="${s.sdmTipe == 3}">
						LAIN - LAIN
					</c:if>
					
				</display:column>
				<display:column title="Nama" style="width: 150px;" class="center">${s.sdmName }</display:column>
				<display:column title="Pendidikan Terakhir" class="center" style="width: 200px;">${s.sdmLastEducation }</display:column>
				<display:column title="Keahlian Utama" class="center" style="width: 250px;">${s.sdmSkillDesc }</display:column>
				<display:column title="Pengalaman" class="center" style="width: 70px;">${s.sdmExperienceYear } Tahun</display:column>
				<display:column title="Aksi" media="html" style="width: 80px;" class="center">
					<!--  
					<div class="uibutton-group">
						<s:url var="set" action="ajax/setTmpVndSdm" />
						<a title="Ubah data Tenaga Ahli perusahaan" class="uibutton icon edit" href='javascript:void(0)' onclick='load_into_box("${set}", "act=edit&id=${s.id}");'>&nbsp;</a>
						<a title="Hapus data Tenaga Ahli perusahaan" class="uibutton" href='javascript:void(0)' onclick='confirmPopUp("deleteObj(\"${set}\", \"act=delete&id=${s.id}\", \"#sdm\")", "Hapus Data Tenaga Ahli Perusahaan", "Yakin ingin menghapus?", "Hapus", "Batal");'>X</a>
					</div>
					-->
				</display:column>
			</display:table>
	<div class="clear"></div>
</div>