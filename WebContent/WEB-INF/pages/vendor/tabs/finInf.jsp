<%@ include file="/include/definitions.jsp"%>

<script type="text/javascript">
	Cufon.replace('caption', {
		fontFamily : 'Caviar Dreams'
	});
</script>
<style>
caption {
	font-size: 19px;
	color: #FF9E0D;
	text-align: left;
}
</style>

<div class="tab center">
	<display:table name="${listTmpVndBank }" id="s" pagesize="1000"
		requestURI="financeInformation.jwebs" excludedParams="*"
		style="width: 100%;background-color:white" class="style1">
		<display:caption>Data Akun Bank</display:caption>
		<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
		<display:column title="Rekening" total="true" style="width: 200px;">${s.bankAccountNo }</display:column>
		<display:column title="Atas Nama" style="width: 150px; "
			class="center">${s.bankAccountName }</display:column>
		<display:column title="Nama Bank" style="width: 150px;" class="center">${s.bankName }</display:column>
		<display:column title="Cabang Bank" style="width: 100px;"
			class="center">${s.bankAddress }</display:column>
		<display:column title="Alamat" class="center" style="width: 200px;">${s.bankBranch }</display:column>
		<display:column title="Aksi" media="html" style="width: 60px;"
			class="center">
			<!--  
					<div class="uibutton-group">
						<s:url var="set" action="ajax/setTmpVndBank" />
						<a title="Ubah data rekening bank perusahaan" class="uibutton icon edit" href='javascript:void(0)' onclick='load_into_box("${set}", "act=edit&id=${s.id}");'>&nbsp;</a>
						<a title="Hapus data rekening bank perusahaan" class="uibutton" href='javascript:void(0)' onclick='confirmPopUp("deleteObj(\"${set}\", \"act=delete&id=${s.id}\", \"#tmpVndBank\")", "Hapus Data Rekening Bank Perusahaan", "Yakin ingin menghapus?", "Hapus", "Batal");'>X</a>
					</div>
					-->
		</display:column>
	</display:table>
	<br />

	<display:table name="${listTmpVndFinRpt }" id="s" pagesize="1000"
		requestURI="financeInformation.jwebs" excludedParams="*"
		style="width: 100%;background-color:white" class="style1">
		<display:caption>Laporan Keuangan</display:caption>
		<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
		<display:column title="Tahun" total="true" style="width: 80px;"
			class="center">${s.finYear }</display:column>
		<display:column title="Jenis" style="width: 100px; " class="center">
			<c:if test="${s.finType == 1}">
						AUDIT
					</c:if>
			<c:if test="${s.finType == 2}">
						NON AUDIT
					</c:if>
		</display:column>
		<display:column title="Valuta" style="width: 100px;" class="center">${s.admCurrency.currencyName }</display:column>
		<display:column title="Asset" style="width: 100px;" class="center">${s.finAssetValue }</display:column>
		<display:column title="Hutang" class="center" style="width: 100px;">${s.finHutang }</display:column>
		<display:column title="Pendapatan Kotor" class="center"
			style="width: 150px;">${s.finRevenue }</display:column>
		<display:column title="Laba Bersih" class="center"
			style="width: 100px;">${s.finNetIncome }</display:column>
		<display:column title="Aksi" media="html" style="width: 60px;"
			class="center">
			<!--  
					<div class="uibutton-group">
						<s:url var="set" action="ajax/setTmpVndFinRpt" />
						<a title="Ubah data laporan keuangan perusahaan" class="uibutton icon edit" href='javascript:void(0)' onclick='load_into_box("${set}", "act=edit&id=${s.id}");'>&nbsp;</a>
						<a title="Hapus data laporan keuangan perusahaan" class="uibutton" href='javascript:void(0)' onclick='confirmPopUp("deleteObj(\"${set}\", \"act=delete&id=${s.id}\", \"#tmpVndFinRpt\")", "Hapus Data Laporan Keuangan Perusahaan", "Yakin ingin menghapus?", "Hapus", "Batal");'>X</a>
					</div>
					-->
		</display:column>
	</display:table>
	<br />

	<table class="style1" style="width:100%">
		<caption>Modal</caption>
		<tr>
			<td width="170px">Modal Dasar (*)</td>
			<td width="60px">${tmpVndHeader.admCurrencyByVendorCurrBaseCap.currencyCode }</td>
			<td><fmt:formatNumber> ${tmpVndHeader.vendorBaseCap}</fmt:formatNumber></td>
		</tr>
		<tr>
			<td>Modal Disetor (*)</td>
			<td>${tmpVndHeader.admCurrencyByVendorCurrInCap.currencyCode }</td>
			<td><fmt:formatNumber>${tmpVndHeader.vendorInCap }</fmt:formatNumber> </td>
		</tr>
	</table><br/>
	<table style="width: 100%" class="style1">
		<caption>Kualifikasi Perusahaan</caption>
		<tr>
			<td width="250px;">Kualifikasi Perusahaan (*)</td>
			<td>
				<c:if test="${tmpVndHeader.vendorFinClass==1 }">
					Kecil
				</c:if>
				<c:if test="${tmpVndHeader.vendorFinClass==2 }">
					Menengah
				</c:if>
				<c:if test="${tmpVndHeader.vendorFinClass==3 }">
					Besar
				</c:if>
				<c:if test="${tmpVndHeader.vendorFinClass==4 }">
					Grade 2
				</c:if>
				<c:if test="${tmpVndHeader.vendorFinClass==5 }">
					Grade 3
				</c:if>
				<c:if test="${tmpVndHeader.vendorFinClass==6 }">
					Grade 4
				</c:if>
				<c:if test="${tmpVndHeader.vendorFinClass==7 }">
					Grade 5
				</c:if>
				<c:if test="${tmpVndHeader.vendorFinClass==8 }">
					Grade 6
				</c:if>
				<c:if test="${tmpVndHeader.vendorFinClass==9 }">
					Grade 7
				</c:if>
			</td>
		</tr>
		<tr>
			<td width="250px;">Kualifikasi Perusahaan 2 (*)</td>
			<td>
				<c:if test="${tmpVndHeader.vendorFinClass1==1 }">
					Kecil
				</c:if>
				<c:if test="${tmpVndHeader.vendorFinClass1==2 }">
					Menengah
				</c:if>
				<c:if test="${tmpVndHeader.vendorFinClass1==3 }">
					Besar
				</c:if>
				<c:if test="${tmpVndHeader.vendorFinClass1==4 }">
					Grade 2
				</c:if>
				<c:if test="${tmpVndHeader.vendorFinClass1==5 }">
					Grade 3
				</c:if>
				<c:if test="${tmpVndHeader.vendorFinClass1==6 }">
					Grade 4
				</c:if>
				<c:if test="${tmpVndHeader.vendorFinClass1==7 }">
					Grade 5
				</c:if>
				<c:if test="${tmpVndHeader.vendorFinClass1==8 }">
					Grade 6
				</c:if>
				<c:if test="${tmpVndHeader.vendorFinClass1==9 }">
					Grade 7
				</c:if>
			</td>
		</tr>
		<tr>
			<td width="250px;">Kualifikasi Perusahaan 3 (*)</td>
			<td>
				<c:if test="${tmpVndHeader.vendorFinClass2==1 }">
					Kecil
				</c:if>
				<c:if test="${tmpVndHeader.vendorFinClass2==2 }">
					Menengah
				</c:if>
				<c:if test="${tmpVndHeader.vendorFinClass2==3 }">
					Besar
				</c:if>
				<c:if test="${tmpVndHeader.vendorFinClass2==4 }">
					Grade 2
				</c:if>
				<c:if test="${tmpVndHeader.vendorFinClass2==5 }">
					Grade 3
				</c:if>
				<c:if test="${tmpVndHeader.vendorFinClass2==6 }">
					Grade 4
				</c:if>
				<c:if test="${tmpVndHeader.vendorFinClass2==7 }">
					Grade 5
				</c:if>
				<c:if test="${tmpVndHeader.vendorFinClass2==8 }">
					Grade 6
				</c:if>
				<c:if test="${tmpVndHeader.vendorFinClass2==9 }">
					Grade 7
				</c:if>
			</td>
		</tr>
	</table>
	<div class="clear"></div>
</div>