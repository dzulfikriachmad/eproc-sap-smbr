<%@ include file="/include/definitions.jsp" %>

<script type="text/javascript">
    Cufon.replace('caption', {fontFamily: 'Caviar Dreams'});
</script>
<style>caption{font-size: 19px; color: #FF9E0D; text-align: left;}</style>
	
<div class="tab center">
	<display:table name="${listVndCv }" id="s" pagesize="1000" requestURI="qualificationInformation.jwebs" excludedParams="*" style="width: 100%;background-color:white" class="style1">
			<display:caption>Pengalaman Kerja</display:caption>
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column title="Nama Pelanggan" style="width: 150px;" class="center">${s.cvClientName }</display:column>
				<display:column title="Nama Project" style="width: 150px;" class="center">${s.cvProjectName }</display:column>
				<display:column title="No. Kontrak" class="center" style="width: 150px;">${s.cvContractNo }</display:column>
				<display:column title="Nilai Proyek" class="center" style="width: 120px;">${s.admCurrency.currencyCode} <fmt:formatNumber> ${s.cvProjectAmount }</fmt:formatNumber></display:column>
				<display:column title="Berlaku" class="center" style="width: 90px;"><fmt:formatDate value="${s.cvProjectStart }" pattern="dd-MMM-yyyy"/><br />s.d<br /><fmt:formatDate value="${s.cvProjectEnd }" pattern="dd-MMM-yyyy"/></display:column>
				<display:column title="Kontak" class="center" style="width: 100px;">${s.cvProjectContactPhone }</display:column>
				<display:column title="Aksi" media="html" style="width: 80px;" class="center">
					<!--  
					<div class="uibutton-group">
						<s:url var="set" action="ajax/setTmpVndCv" />
						<a title="Ubah data pengalaman proyek perusahaan" class="uibutton icon edit" href='javascript:void(0)' onclick='load_into_box("${set}", "act=edit&id=${s.id}");'>&nbsp;</a>
						<a title="Hapus data pengalaman proyek perusahaan" class="uibutton" href='javascript:void(0)' onclick='confirmPopUp("deleteObj(\"${set}\", \"act=delete&id=${s.id}\", \"#tmpVndCv\")", "Hapus Data Pengalaman Poryek Perusahaan", "Yakin ingin menghapus?", "Hapus", "Batal");'>X</a>
					</div>
					-->
				</display:column>
			</display:table>
	<div class="clear"></div>
</div>