<%@ include file="/include/definitions.jsp" %>

<script type="text/javascript">
    Cufon.replace('caption', {fontFamily: 'Caviar Dreams'});
</script>
<style>caption{font-size: 19px; color: #FF9E0D; text-align: left;}</style>
	
<div class="tab center">
	<display:table name="${listTmpVndBoardKomisaris }" id="s" pagesize="1000" requestURI="managementInformation.jwebs" excludedParams="*" style="width: 100%;background-color:white" class="style1">
			<display:caption>Daftar Komisaris</display:caption>
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column title="Nama" total="true" style="width: 100px;">${s.boardName }</display:column>
				<display:column title="Jabatan" style="width: 100px; " class="center">${s.boardPosition }</display:column>
				<display:column title="No. Telepon" style="width: 100px;" class="center">${s.boardPhone }</display:column>
				<display:column title="No. KTP" style="width: 100px;" class="center">${s.boardKtp }</display:column>
				<display:column title="NPWP Pribadi" class="center" style="width: 200px;">${s.boardNpwpNo }</display:column>
				<display:column title="Negara" style="width: 100px;" class="center">${s.admCountry.countryName }</display:column>
				<display:column title="Aksi" media="html" style="width: 60px;" class="center">
					<!--  
					<div class="uibutton-group">
						<s:url var="set" action="ajax/setTmpVndBoardKomisaris" /> 
						<a title="Ubah data dewan komisaris perusahaan" class="uibutton icon edit" href='javascript:void(0)' onclick='load_into_box("${set}", "act=edit&id=${s.id}");'>&nbsp;</a>
						<a title="Hapus data dewan komisaris perusahaan" class="uibutton" href='javascript:void(0)' onclick='confirmPopUp("deleteObj(\"${set}\", \"act=delete&id=${s.id}\", \"#komisarisPerusahaan\")", "Hapus Data Dewan Komisaris Perusahaan", "Yakin ingin menghapus?", "Hapus", "Batal");'>X</a>
					</div>
					-->
				</display:column>
			</display:table><br /><br />
			
			<display:table name="${listTmpVndBoardDireksi }" id="s" pagesize="1000" requestURI="managementInformation.jwebs" excludedParams="*" style="width: 100%;background-color:white" class="style1">
				<display:caption>Daftar Direksi</display:caption>
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column title="Nama" total="true" style="width: 100px;">${s.boardName }</display:column>
				<display:column title="Jabatan" style="width: 100px; " class="center">${s.boardPosition }</display:column>
				<display:column title="No. Telepon" style="width: 100px;" class="center">${s.boardPhone }</display:column>
				<display:column title="No. KTP" style="width: 100px;" class="center">${s.boardKtp }</display:column>
				<display:column title="NPWP Pribadi" class="center" style="width: 200px;">${s.boardNpwpNo }</display:column>
				<display:column title="Negara" style="width: 100px;" class="center">${s.admCountry.countryName }</display:column>
				<display:column title="Aksi" media="html" style="width: 60px;" class="center">
					<!--  
					<div class="uibutton-group">
						<s:url var="set" action="ajax/setTmpVndBoardDireksi" /> 
						<a title="Ubah data dewan direksi perusahaan" class="uibutton icon edit" href='javascript:void(0)' onclick='load_into_box("${set}", "act=edit&id=${s.id}");'>&nbsp;</a>
						<a title="Hapus data dewan direksi perusahaan" class="uibutton" href='javascript:void(0)' onclick='confirmPopUp("deleteObj(\"${set}\", \"act=delete&id=${s.id}\", \"#direksiPerusahaan\")", "Hapus Data Dewan Direksi Perusahaan", "Yakin ingin menghapus?", "Hapus", "Batal");'>X</a>
					</div>
					-->
				</display:column>
			</display:table>
	<div class="clear"></div>
</div>