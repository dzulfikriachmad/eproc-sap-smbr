<%@ include file="/include/definitions.jsp" %>

<script type="text/javascript">
    Cufon.replace('caption', {fontFamily: 'Caviar Dreams'});
</script>
<style>caption{font-size: 19px; color: #FF9E0D; text-align: left;}</style>
	
<div class="tab center">
	<display:table name="${listTmpVndCert }" id="s" pagesize="1000" requestURI="qualificationInformation.jwebs" excludedParams="*" style="width: 100%;background-color:white" class="style1">
				<display:caption>Klasifikasi Perusahaan</display:caption>
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column title="Tipe" style="width: 200px; " class="center">
					<c:if test="${s.certTipe == 1}">
						MUTU
					</c:if>
					<c:if test="${s.certTipe == 2}">
						PENGAHARGAAN
					</c:if>
					<c:if test="${s.certTipe == 3}">
						ASOSIASI DAN PROFESI
					</c:if>
					<c:if test="${s.certTipe == 4}">
						PATENT DAN LISENSI
					</c:if>
					<c:if test="${s.certTipe == 5}">
						LINGKUNGAN HIDUP
					</c:if>
					<c:if test="${s.certTipe == 6}">
						Lainnya
					</c:if>
				</display:column>
				<display:column title="Nomor" style="width: 150px;" class="center">${s.certNo }</display:column>
				<display:column title="Nama" style="width: 150px;" class="center">${s.certName }</display:column>
				<display:column title="Keterangan" class="center" style="width: 150px;">${s.certDescription }</display:column>
				<display:column title="Dikeluarkan Oleh" class="center" style="width: 159px;">${s.certIssued }</display:column>
				<display:column title="Berlaku" class="center" style="width: 90px;"><fmt:formatDate value="${s.certFrom }" pattern="dd-MMM-yyyy"/><br />s.d<br /><fmt:formatDate value="${s.certTo }" pattern="dd-MMM-yyyy"/></display:column>
			</display:table>
			
			<display:table name="${listTmpVndProduk }" id="s" pagesize="1000" requestURI="qualificationInformation.jwebs" excludedParams="*" style="width: 100%;background-color:white" class="style1">
				<display:caption>Barang Dan Jasa</display:caption>
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column title="Jenis" style="width: 250px;" class="center">${s.comGroup.groupName }</display:column>
				<display:column title="Nama Barang / Jasa" style="width: 150px;" class="center">${s.productName }</display:column>
				<display:column title="Merk (*Barang)" class="center" style="width: 150px;">${s.productBrand }</display:column>
				<display:column title="Sumber (*Barang)" class="center" style="width: 100px;">${s.productSource }</display:column>
				<display:column title="Tipe (*Barang)" class="center" style="width: 100px;">
					<c:if test="${s.productTipe == 'A' }">AGENT</c:if>
					<c:if test="${s.productTipe == 'B' }">SOLE AGENT</c:if>
					<c:if test="${s.productTipe == 'C' }">NON AGENT</c:if>
					<c:if test="${s.productTipe == 'D' }">DISTRIBUTOR</c:if>
					<c:if test="${s.productTipe == 'E' }">MANUFAKTUR</c:if>
					<c:if test="${s.productTipe == 'F' }">LAIN - LAIN</c:if>
				</display:column>
				<%-- <display:column title="Keterangan" class="center" style="width: 300px;">${s.productDescription }</display:column> --%>
			</display:table>
			
			<display:table name="${listTmpVndDistrict }" id="s" pagesize="1000" requestURI="qualificationInformation.jwebs" excludedParams="*" style="width: 100%;background-color:white" class="style1">
				<display:caption>Area Kerja</display:caption>
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column title="Kantor" style="width: 250px;" class="center">${s.admDistrict.districtName }</display:column>
			</display:table>
	<div class="clear"></div>
</div>