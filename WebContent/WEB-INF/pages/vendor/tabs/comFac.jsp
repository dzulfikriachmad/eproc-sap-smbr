<%@ include file="/include/definitions.jsp" %>

<script type="text/javascript">
    Cufon.replace('caption', {fontFamily: 'Caviar Dreams'});
</script>
<style>caption{font-size: 19px; color: #FF9E0D; text-align: left;}</style>
	
<div class="tab center">
	<display:table name="${listTmpVndEquip }" id="s" pagesize="1000" requestURI="companyFacility.jwebs" excludedParams="*" style="width: 100%;background-color:white" class="all style1">
			<display:caption>Fasilitas Perusahaan</display:caption>
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column title="Tipe" style="width: 200px; " class="center">
					<c:if test="${s.equipTipe == 1}">
						<s:text name="com.fac.type1"></s:text>
					</c:if>
					<c:if test="${s.equipTipe == 2}">
						<s:text name="com.fac.type2"></s:text>
					</c:if>
					<c:if test="${s.equipTipe == 3}">
						<s:text name="com.fac.type3"></s:text>
					</c:if>
					<c:if test="${s.equipTipe == 4}">
						<s:text name="com.fac.type4"></s:text>
					</c:if>
					<c:if test="${s.equipTipe == 5}">
						<s:text name="com.fac.type5"></s:text>
					</c:if>
					<c:if test="${s.equipTipe == 6}">
						<s:text name="com.fac.type6"></s:text>
					</c:if>
					<c:if test="${s.equipTipe == 7}">
						<s:text name="com.fac.type7"></s:text>
					</c:if>
					<c:if test="${s.equipTipe == 8}">
						<s:text name="com.fac.type8"></s:text>
					</c:if>					
				</display:column>
				<display:column title="Nama" style="width: 150px;" class="center">${s.equipName }</display:column>
				<display:column title="Spesifikasi" class="center" style="width: 200px;">${s.equipSpecification }</display:column>
				<display:column title="Qty." class="center" style="width: 40px;">${s.equipQuantity }</display:column>
				<display:column title="Status" class="center" style="width: 60px;">
					<c:if test="${s.equipStatus == 1}">
						MILIK
					</c:if>
					<c:if test="${s.equipStatus == 2}">
						SEWA
					</c:if>		
				</display:column>
				<display:column title="Tahun" class="center" >${s.equipYearMade }</display:column>
				<display:column title="Tgl. Beli" class="center" style="width: 70px;"><fmt:formatDate value="${s.equipBuyDate }" pattern="dd-MMM-yyyy"/> </display:column>
				<display:column title="Aksi" media="html" style="width: 80px;" class="center">
					<!--  
					<div class="uibutton-group">
						<s:url var="set" action="ajax/setTmpVndEquip" />
						<a title="Ubah data fasilitas perusahaan" class="uibutton icon edit" href='javascript:void(0)' onclick='load_into_box("${set}", "act=edit&id=${s.id}");'>&nbsp;</a>
						<a title="Hapus data fasilitas perusahaan" class="uibutton" href='javascript:void(0)' onclick='confirmPopUp("deleteObj(\"${set}\", \"act=delete&id=${s.id}\", \"#fac\")", "Hapus Data Fasilitas / Peralatan Perusahaan", "Yakin ingin menghapus?", "Hapus", "Batal");'>X</a>
					</div>
					-->
				</display:column>
			</display:table>
	<div class="clear"></div>
</div>