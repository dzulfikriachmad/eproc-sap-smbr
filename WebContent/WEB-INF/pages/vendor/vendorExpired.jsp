			<%@ include file="/include/definitions.jsp" %>
			<script type="text/javascript">
				   $(document).ready(function() {
					// DATATABLE
					 $('table.doc').dataTable({
				        "bInfo": false,
				        "iDisplayLength": 10,
				        "aLengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
				        "sPaginationType": "full_numbers",
				        "bPaginate": true,
				        "sDom": '<f>t<pl>'
				        });
					});
			</script>
			<center>
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>			
			<display:table name="${listVendorExpired }" id="s"  excludedParams="*" style="width: 100%;" class="doc style1">
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column title="Nama Vendor" style=" width: 400px; text-align:center" class="center">
					<a href="javascript:void(0)" onclick="load_into_box('<s:url action="ajax/vendor/detail"/>','tmpVndHeader.vendorId=${s.vendorId }')">
					${fn:toUpperCase(s.vendorName)}
					</a>
				</display:column>
				<display:column title="Status Vendor" style=" width:130px; text-align:center " >
					<c:if test="${s.status=='S' }">
						<span style="color:orange">Suspended</span>
					</c:if>
					<c:if test="${s.status=='B' }">
						<span style="color:red">Blacklisted</span>
					</c:if>
					<c:if test="${s.status!='S' and s.status!='B' }">
						<span style="color:green">Aktif</span>
					</c:if>
				</display:column>
				<display:column title="SIUP" style=" text-align:center " >
				<span style="color:green">Aktif</span>
					<%-- <c:if test="${s.siup>=1 }">
						<span style="color:red">Expired</span>
					</c:if>
					<c:if test="${s.siup==0 }">
						<span style="color:green">Aktif</span>
					</c:if> --%>
				</display:column>
				<display:column title="TDP" style=" text-align:center " >
					<c:if test="${s.tdp>=1 }">
						<span style="color:red">Expired</span>
					</c:if>
					<c:if test="${s.tdp==0 }">
						<span style="color:green">Aktif</span>
					</c:if>
				</display:column>
				<display:column title="Domisili" style=" text-align:center " >
						<span style="color:green">Aktif</span>
					<%-- <c:if test="${s.domisili>=1 }">
						<span style="color:red">Expired</span>
					</c:if>
					<c:if test="${s.domisili==0 }">
						<span style="color:green">Aktif</span>
					</c:if> --%>
				</display:column>
				<display:column title="Ijin" style=" text-align:center " >
					<c:if test="${s.ijin>=1 }">
						<span style="color:red">Expired</span>
					</c:if>
					<c:if test="${s.ijin==0 }">
						<span style="color:green">Aktif</span>
					</c:if>
				</display:column>
				<display:column title="Sertifikat" style=" text-align:center">
					<c:if test="${s.sertifikat>=1}">
						<span style="color:red">Expired</span>
					</c:if>
					<c:if test="${s.sertifikat==0}">
						<span style="color:green">Aktif</span>
					</c:if>
				</display:column>
				<display:column title="" media="html" style="width: 85px;text-align:center" class="center">
					<a style="text-align: center" href='javascript:void(0)' class='uibutton icon add' onclick='load_into_box("<s:url action="ajax/vendor/crudSuspend" />?vendorId=${s.vendorId }")'>
						Aksi
					</a>
				</display:column>
			</display:table>
			</center>
			
			<script>
				$(document).ready(function(){	
					// SYSTEM MESSAGES
					$(".message").click(function () {
				      $(this).fadeOut();
				    });
				});
			</script>