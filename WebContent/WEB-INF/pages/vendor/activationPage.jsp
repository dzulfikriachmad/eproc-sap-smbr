<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Daftar Aktivasi Mitra</title>
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	
	<script type="text/javascript">
		$(document).ready(function(){
			$(".chzn").chosen();
			// FORM VALIDATION
			$("#procForm").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler:function(form){
					loadingScreen();
					form.submit();
				}
			});
			
			
		});
		
		//regCancel Action
		function regCancelVerification(){
			jQuery.facebox.close();
			window.location = "<s:url action="vendor/activation" />"; 
		}
		
		function selectMitraKerja(obj){
			if(obj.value==1){
				$('#smkNoDiv').show();
				$('#jdeUserDiv').hide();
				$('#jdePassDiv').hide();
			}else if(obj.value==2){
				$('#smkNoDiv').hide();
				$('#jdeUserDiv').show();
				$('#jdePassDiv').show();
			}
		}
	</script>
	
	<!-- Tabs Panel -->
	<script type="text/javascript">
		$(function() {
			$(".tabs").tabs();
		});
		//First Tab Load
		loadPage("<s:url action="ajax/companyInformation" />?tmpVndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");
	</script>
	<link rel="stylesheet" href="${contextPath}/assets/css/tabs-toggle.css" type="text/css" media="screen" />
	
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="dashboard">Home</s:a></li> 
	        <li>Vendor Management</li> 
	        <li><s:a action="vendor/activation">Aktivasi Mitra Kerja</s:a></li>
	        
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
		
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
					
			<s:form action="vendor/activation" method="post" id="procForm" name="procForm">
				<s:hidden name="tmpVndHeader.vendorId" />
				<!-- Nama Perusahaan -->
				<div class="box">
				<div class="title">
					Detail Vendor
					<span class="hide"></span>
				</div>
				<div class="content nopadding" style="padding: 0px;">
					<div class="tabs">
						<div class="tabmenu">
							<ul> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/companyInformation" />?tmpVndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");'>Data Utama</a></li> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/legInf" />?tmpVndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");' >Data Legal</a></li> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/manInf" />?tmpVndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");'>Pengurus</a></li> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/finInf" />?tmpVndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");'>Keuangan</a></li> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/quaInf" />?tmpVndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");'>Barang / Jasa & Sertifikasi</a></li> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/resInf" />?tmpVndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");'>SDM</a></li> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/comFac" />?tmpVndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");'>Fasilitas / Peralatan</a></li> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/proExp" />?tmpVndHeader.vendorId=${tmpVndHeader.vendorId }", "#tabsContent");'>Pengalaman Proyek</a></li> 
							</ul>
						</div>
						
						<div id="tabsContent"> 
						</div>
								
					</div>
				</div>
			</div>
				<div class="box">
					<div class="title center">
						Checklist Kelengkapan Persyaratan Administrasi 
						<span class="hide"></span>
					</div>
					<div class="content">
							
							<display:table name="${listTmpVndDoc }" id="s" excludedParams="*" style="width: 100%;" class="style1">	
									<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}
						
									</display:column>
									<display:column title="Persyaratan Administrasi" total="true" style="width: 350px;">${s.docName }	</display:column>
									<display:column title="Status" style="width: 150px; " class="center">
										<c:if test="${s.docStatus == 1 }">Ada</c:if>
										<c:if test="${s.docStatus == 2 }">Tidak Ada</c:if>
										<c:if test="${s.docStatus == 3 }">Perlu Konfirmasi</c:if>
									</display:column>
									<display:column title="Catatan" style="width: 300px;" class="center">${s.docRemark }</display:column>
							</display:table>
						
						</div>
				</div>
				
				<div class="box">
					<div class="title center">
						Nomor Mitra Kerja
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle1">
<!-- 							<tr> -->
<!-- 								<td>Mitra Kerja Sudah Ada di JDE</td> -->
<!-- 								<td> -->
<%-- 									<s:select list="listJde" listKey="key" listValue="value" name="status" onchange="selectMitraKerja(this);"></s:select> --%>
<!-- 								</td> -->
<!-- 							</tr> -->
<!-- 							<tr id="smkNoDiv"> -->
<!-- 								<td>Nomor Mitra Kerja (* Diisi Jika Vendor Sudah ada di JDE)</td> -->
<!-- 								<td> -->
<%-- 									<s:textfield name="tmpVndHeader.vendorSmkNo"></s:textfield> --%>
<!-- 								</td> -->
<!-- 							</tr> -->
<!-- 							<tr id="jdeUserDiv" style="display: none"> -->
<!-- 								<td>Jde USER</td> -->
<!-- 								<td> -->
<%-- 									<s:textfield name="jdeUser"></s:textfield> --%>
<!-- 								</td> -->
<!-- 							</tr> -->
<!-- 							<tr id="jdePassDiv" style="display: none"> -->
<!-- 								<td>Jde PASSWORD</td> -->
<!-- 								<td> -->
<%-- 									<s:password name="jdePassword" maxlength="10"></s:password> --%>
<!-- 								</td> -->
<!-- 							</tr> -->
						</table>		
					</div>
				</div>
				
				
					
				<div class="uibutton-toolbar extend">
					<s:submit name="ok" value="Aktifkan" cssClass="uibutton"></s:submit>
					<%-- <s:submit name="pushVendor" value="Push Vendor" cssClass="uibutton"></s:submit> --%>
					<a href="javascript:void(0)" class="uibutton" onclick="confirmPopUp('regCancelVerification();', 'Batal', 'Apakah anda yakin untuk membatalkan transaksi ?', 'Ya', 'Tidak');"><s:text name="cancel"></s:text> </a>
				</div>
						
			</s:form>
				
		</div>
	</div>
</body>
</html>