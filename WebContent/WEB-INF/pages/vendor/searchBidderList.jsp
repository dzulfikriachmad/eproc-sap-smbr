<%@ include file="/include/definitions.jsp"%>
<script type="text/javascript">

$(document).ready(function() {
	// DATATABLE
	$('table.all').dataTable({
        "bInfo": false,
        "iDisplayLength": 10,
        "aLengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
        "sPaginationType": "full_numbers",
        "bPaginate": true,
        "sDom": '<f>t<pl>'
        });
	});
			</script>
	<display:table name="${listBidder }" id="s" excludedParams="*"
		style="width: 100%;" class="all style1">
		<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
		<display:column title="Nama Vendor" total="true" style="width: 400px;">
			<a href="javascript:void(0)"
				onclick="load_into_box('<s:url action="ajax/vendor/detail"/>','tmpVndHeader.vendorId=${s.vendorId }')">
				${fn:toUpperCase(s.namaVendor) } </a>
		</display:column>
		<display:column title="Di Undang" style="width: 250px; "
			class="center">
			<a href="javascript:void(0)"
				onclick="detailProcurement(${s.vendorId},1)"> ${s.diundang } </a>
		</display:column>
		<display:column title="Mendaftar" style="width: 250px;" class="center">
			<a href="javascript:void(0)"
				onclick="detailProcurement(${s.vendorId},2)"> ${s.mendaftar } </a>
		</display:column>
		<display:column title="Lulus Adm" style="width: 250px;" class="center">
			<a href="javascript:void(0)"
				onclick="detailProcurement(${s.vendorId},3)"> ${s.lulusAdm } </a>
		</display:column>
		<display:column title="Lulus Teknis" style="width: 250px;"
			class="center">
			<a href="javascript:void(0)"
				onclick="detailProcurement(${s.vendorId},4)"> ${s.lulusTeknis }
			</a>
		</display:column>
		<display:column title="Lulus Harga" style="width: 250px;"
			class="center">
			<a href="javascript:void(0)"
				onclick="detailProcurement(${s.vendorId},5)"> ${s.lulusHarga } </a>
		</display:column>
		<display:column title="Menang" style="width: 250px;" class="center">
			<a href="javascript:void(0)"
				onclick="detailProcurement(${s.vendorId},6)"> ${s.menang } </a>
		</display:column>
		<display:column title="Dokumen">
			<a href="javascript:void(0)" onclick="detailDokumen(${s.vendorId})">
				Lihat </a>
		</display:column>
	</display:table>