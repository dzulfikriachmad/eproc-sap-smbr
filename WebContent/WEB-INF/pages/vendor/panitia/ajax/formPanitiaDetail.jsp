<%@ include file="/include/definitions.jsp" %>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.chzn').chosen();
			// FORM VALIDATION
			$("#formeproc").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
			});
			
			$("#formeproc").ajaxForm({
		        beforeSubmit: function() {
		            //$('#notif').html('<img src="${contextPath}/assets/images/loading/loading-circle.gif"/>');
		        },
		        success: function(data) {
		            loadPage("<s:url action="ajax/vendor/listPanitiaDetail"/>?panitiaVnd.id=${panitiaVnd.id}","#result");
		            jQuery.facebox.close();
		        }
		    });
		});
		
		
	</script>
	
<div class='fbox_header'>Tambah Data Anggota Panitia</div>
    <div class='fbox_container'>
    <s:form name="formeproc" id="formeproc">
        <div class='fbox_content'>
        	<div id="cek"></div>
        		<s:hidden name="panitiaVnd.id"></s:hidden>
            	<s:hidden name="panitiaVndDetail.panitia.id" value="%{panitiaVnd.id}"></s:hidden>
            	<s:hidden name="panitiaVndDetail.id"></s:hidden>
            	<s:hidden name="panitiaVndDetail.createdDate"></s:hidden>
            	<s:hidden name="panitiaVndDetail.createdBy"></s:hidden>
            	<s:hidden name="panitiaVndDetail.updatedDate"></s:hidden>
            	<s:hidden name="panitiaVndDetail.updatedBy"></s:hidden>
            	<table style="min-width: 600px; margin: 0px 10px;" class="form formStyle">        
            		<tr>
            			<td><s:label value="User"></s:label> </td>
            			<td colspan="3">
            				<s:select list="listUser" listKey="id" listValue="completeName" name="panitiaVndDetail.anggota.id" cssClass="chzn"></s:select>
            				
            			</td>
            		</tr>		
            		<tr>
            			<td><s:label value="Posisi Panitia (*)"></s:label> </td>
            			<td colspan="3"><s:textfield name="panitiaVndDetail.posisi" cssClass="required" /> </td>
            		</tr>  
            	</table>
            
        </div>
        <div class='fbox_footer'>
            <s:submit key="save" cssClass="uibutton confirm" ></s:submit>
            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()'>Batalkan</a>
        </div>
        </s:form>
    </div>
    