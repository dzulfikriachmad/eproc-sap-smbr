<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	
	<script type="text/javascript">	
		$(document).ready(function(){
			$(".chzn").chosen();
			$.datepicker.setDefaults($.datepicker.regional['in_ID']);
			$("#tanggal1").datepicker({
				changeYear: true,
				changeMonth: true
			});
			$("#tanggal2").datepicker({
				changeYear: true,
				changeMonth: true
			});
			
			// FORM VALIDATION
			$("#frm").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				}
			});
		});
		
		//regCancel Action
		function regCancel(){
			window.location = "<s:url action="vendor/listPanitia" />"; 
		}
		
		$(document).ready(function() {
			// DATATABLE
		    $('table.history').dataTable({
		        "bInfo": false,
		        "iDisplayLength": 200,
		        "aLengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
		        "sPaginationType": "full_numbers",
		        "bPaginate": true,
		        "sDom": '<f>t<pl>'
		    });
		});
		
		function setCek(obj){
			if(obj.checked){
				obj.value=1;
			}else{
				obj.value=0;
			}
		}
		
	</script>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="dashboard">Home</s:a></li> 
	        <li>Usulan Mitra Kerja</li> 
	        
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
    
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
					
			<s:form action="vendor/crudPanitia" id="frm" name="frm" enctype="multipart/form-data" method="post">
				<s:hidden name="panitiaVnd.id" />
				<s:hidden name="panitiaVnd.createdDate" />
				<s:hidden name="panitiaVnd.createdBy" />
				
				<div class="box">
					<div class="title">
						Header
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle" >
							<tr>
								<td width="150px">Nomor Surat</td>
								<td>
									<s:textfield name="panitiaVnd.nomorSurat" cssClass="required"></s:textfield>
								</td>
							</tr>
							<tr>
								<td width="150px">Nama Panitia</td>
								<td>
									<s:textfield name="panitiaVnd.namaPanitia" cssClass="required"></s:textfield>
								</td>
							</tr>
							<tr>
								<td width="150px">Tanggal Mulai</td>
								<td>
									<s:textfield name="panitiaVnd.tanggalMulai" cssClass="required" id="tanggal1"></s:textfield>
								</td>
							</tr>
							<tr>
								<td width="150px">Tanggal Berakhir</td>
								<td>
									<s:textfield name="panitiaVnd.tanggalBerakhir" cssClass="required" id="tanggal2"></s:textfield>
								</td>
							</tr>
							<tr>
								<td width="150px">Satuan Kerja</td>
								<td>
									<s:select list="listDept" listKey="id" listValue="deptName" name="panitiaVnd.satker.id" cssClass="chzn"></s:select>
								</td>
							</tr>
							<tr>
								<td width="150px">Status</td>
								<td>
									
									<input type="checkbox" name="panitiaVnd.status" onclick="setCek(this)" <c:if test="${panitiaVnd.status==1 }">checked="checked"</c:if>/> (* Cek Jika Aktif)
								</td>
							</tr>
						</table>
						
					</div>
				</div>
				
				<div class="uibutton-toolbar extend">
					<a href='javascript:void(0)' class='uibutton icon add' onclick='load_into_box("<s:url action="ajax/vendor/crudPanitiaDetail" />?panitiaVnd.id=${panitiaVnd.id }")'>Tambah Data Panitia</a>
				</div>
				<div class="box">
					<div class="title">
						Detail
						<span class="hide"></span>
					</div>
					<div class="content" id="result">
						<jsp:include page="listPanitiaVndDetail.jsp"></jsp:include>
					</div>
				</div>

				<div class="uibutton-toolbar extend">
					<s:submit name="savefinish" key="save.finish" cssClass="uibutton" ></s:submit>
					<a href="javascript:void(0)" class="uibutton" onclick="confirmPopUp('regCancel();', 'Batal', 'Apakah anda yakin untuk membatalkan transaksi ?', 'Ya', 'Tidak');"><s:text name="cancel"></s:text> </a>
				</div>
						
			</s:form>
			
		</div>
	</div>
</body>
</html>