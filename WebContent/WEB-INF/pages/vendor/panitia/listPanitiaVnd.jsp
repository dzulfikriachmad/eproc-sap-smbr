<%@ include file="/include/definitions.jsp" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Daftar Panitia</title>
<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
<script type="text/javascript">
	function setId(id){
		document.getElementById('panitiaVnd.id').value=id;
	}
</script>
</head>
<body>
<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="#">Home</s:a></li> 
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
		
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
			
			<div class="uibutton-toolbar extend">
				<s:url action="vendor/crudPanitia" id="url"></s:url>
				<a href="${url }" target="_parent" class="uibutton icon add" <c:if test="${create !=1 }"> style="visibility:hidden"</c:if>> Tambah Panitia</a>
			</div>
			
			
			<div class="box">
				<div class="title center">
					DAFTAR PANITIA
					<span class="hide"></span>
				</div>
				<div class="content">
					<s:form action="vendor/crudPanitia" method="post">
						<s:hidden name="panitiaVnd.id" id="panitiaVnd.id"></s:hidden>
			            <display:table name="listPanitiaVnd" id="s" requestURI=".jwebs" excludedParams="*" style="width: 100%;" class="all style1">
							<display:setProperty name="paging.banner.full" value=""></display:setProperty>
							<display:setProperty name="paging.banner.first" value=""></display:setProperty>
							<display:setProperty name="paging.banner.last" value=""></display:setProperty>
							
							<display:column title="No." style="width: 27px; text-align: center; background: #fafafa;">${s_rowNum }</display:column>
							<display:column title="Nomor Surat">${s.nomorSurat }</display:column>
							<display:column title="Nama Panitia">${s.namaPanitia}</display:column>
							<display:column title="Tanggal Berlaku" style="text-align:center"><fmt:formatDate value="${s.tanggalMulai}" pattern="dd.MM.yyyy"/>  s/d  <fmt:formatDate value="${s.tanggalBerakhir }" pattern="dd.MM.yyyy"/></display:column>
							<display:column title="Satuan Kerja">${s.satker.deptName }</display:column>
							<display:column>
								<div class="uibutton-group">
									<input type="submit" class="uibutton icon edit" value="Edit" name="edit" onclick="setId(${s.id})" <c:if test="${update !=1 }"> style="visibility:hidden"</c:if>/>
									<input type="submit" class="uibutton icon edit" value="Hapus" name="hapus" onclick="setId(${s.id})" <c:if test="${delete !=1 }"> style="visibility:hidden"</c:if>/>
								</div>
							</display:column>
						</display:table>
			       </s:form>
				</div>
			</div>
				
		</div>
	</div>

</body>
</html>