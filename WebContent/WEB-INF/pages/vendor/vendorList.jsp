<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Daftar Mitra Kerja</title>
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	
	<script type="text/javascript">
		//regCancel Action
		function regCancelVerification(){
			
			jQuery.facebox.close();
			window.location = "<s:url action="vendor/verification" />"; 
		}
		
		function setVendorId(id){
			//alert(id);
			$('#vendorId').val(id);
		}
	</script>
	
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="dashboard">Home</s:a></li> 
	        <li>Vendor Management</li> 
	        <li><s:a action="vendor/list">Daftar Vendor</s:a></li>
	        
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
		
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
			
			<div class="box">
				<div class="title">
					Pencarian 
					<span class="hide"></span>
				</div>
				<div class="content">
					<s:form method="post">
						<table class="form formStyle">
							<tr>
								<td width="150px">Status Mitra Kerja</td>
								<td>
									<s:select list="#{'0':'--Semua--','R':'Route', 'P':'Sedang Di proses','A':'Aktif','E':'Sedang Di Ubah','EE':'Menunggu Verifikasi','V':'Diverifikasi','U':'Sedang Diusulkan','F':'Sedang Finalisasi'}" name="tmpVndHeader.vendorStatus"  cssStyle="width:200px" ></s:select>
								</td>
							</tr>
						</table>
						<div class="uibutton-toolbar extend">
							<s:submit name="cari" value="Pencarian" cssClass="uibutton" onclick="loadingScreen();"></s:submit>	
						</div>
					</s:form>
				</div>
			</div>
					
			<s:hidden name="vndHeader.vendorId" />
			<!-- Nama Perusahaan -->
			<div class="box">
				<div class="title center">
					Daftar Vendor
					<span class="hide"></span>
				</div>
				<div class="content">
					<s:form action="vendor/list">
						<s:hidden name="tmpVndHeader.vendorId" id="vendorId"></s:hidden>
						<display:table name="${listTmpVndHeader }" id="s" excludedParams="*" style="width: 100%;" class="all style1">
								<display:setProperty name="paging.banner.full" value=""></display:setProperty>
								<display:setProperty name="paging.banner.first" value=""></display:setProperty>
								<display:setProperty name="paging.banner.last" value=""></display:setProperty>
								<display:column title="No. Mitra Kerja" style="width: 20px; text-align: center;">${s.vendorSapId}</display:column>
								<display:column title="Nama Vendor" total="true" style="width: 400px;">
									<a href="javascript:void(0)" onclick="load_into_box('<s:url action="ajax/vendor/detail"/>','tmpVndHeader.vendorId=${s.vendorId }')">
										${fn:toUpperCase(s.vendorName) }
									</a>
								</display:column>
								<display:column title="Status" style="width: 100px; " class="center">
									<c:if test="${s.vendorStatus == 'R' }">Route to Vendor</c:if>
									<c:if test="${s.vendorStatus == 'P' and s.vendorNextPage<10}">Sedang diproses</c:if>
									<c:if test="${s.vendorStatus == 'A' }">Aktif</c:if>
									<c:if test="${s.vendorStatus == 'E' }">Sedang diubah</c:if>
									<c:if test="${s.vendorStatus == 'EE' or (s.vendorStatus == 'P' and s.vendorNextPage==10) }">Menunggu Verifikasi</c:if>
									<c:if test="${s.vendorStatus == 'V' }">Diverifikasi</c:if>
									<c:if test="${s.vendorStatus == 'U' }">Diusulkan</c:if>
									<c:if test="${s.vendorStatus == 'F' }">Sedang Di Finalisasi</c:if>
								</display:column>
								<display:column title="CP" style="width: 170px; " class="center">${s.vendorContactName }</display:column>
								<display:column title="Telp." style="width: 150px;" class="center">${s.vendorContactPhone }</display:column>
								
								<display:column title="Aksi" media="html" style="width: 60px;" class="center">
									
								</display:column>
							
						</display:table>
					</s:form>
				</div>
			</div>
				
		</div>
	</div>
</body>
</html>