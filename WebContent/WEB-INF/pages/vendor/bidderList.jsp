<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Daftar Bidder</title>
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	
	<script type="text/javascript">
		$(document).ready(function(){
			detailDokumenLoad();
		});

		function setVendorId(id){
			//alert(id);
			$('#vendorId').val(id);
		}
		
		function detailProcurement(vendorId,tipe){
			$.ajax({
		        url: "<s:url action="ajax/vendor/bidderDetailPengadaan" />?vendorId="+vendorId+"&tipe="+tipe, 
		        beforeSend: function(){
		        	$("#detail").html('<img src="${contextPath}/assets/images/loading/loading-circle.gif"/>');
		        },
		        success: function(response){
		        		jQuery.facebox.close();
		                $('#detail').html(response);
		            },
		        type: "post", 
		        dataType: "html"
		    }); 
		}
		
		function detailDokumen(vendorId){
			$.ajax({
		        url: "<s:url action="ajax/vendor/vendorExpired" />?vendorId="+vendorId, 
		        beforeSend: function(){
		        	jQuery.facebox('<div style="padding: 17px; font-size: 36px;">Loading....</div>');
		        },
		        success: function(response){
		        		jQuery.facebox.close();
		                $('#dokumen').html(response);
		            },
		        type: "post", 
		        dataType: "html"
		    }); 
		}
		
		function detailDokumenLoad(){
			$.ajax({
		        url: "<s:url action="ajax/vendor/vendorExpired" />", 
		        beforeSend: function(){
		        	jQuery.facebox('<div style="padding: 17px; font-size: 36px;">Loading....</div>');
		        },
		        success: function(response){
		        		jQuery.facebox.close();
		                $('#dokumen').html(response);
		            },
		        type: "post", 
		        dataType: "html"
		    }); 
		}
		
		$("#searchButton").live("click",function(){
			$.ajax({
		        url: "<s:url action="ajax/vendor/searchBidderList" />", 
		        data: $(document.bidderSearch.elements).serialize(),
		        beforeSend: function(){
		        	$("#hasil").html('<img src="${contextPath}/assets/images/loading/loading-circle.gif"/>');
		        },
		        success: function(response){
		                $("#hasil").html(response);
		            },
		        type: "post", 
		        dataType: "html"
		    }); 
		});
		
	</script>
	
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="dashboard">Home</s:a></li> 
	        <li>Vendor Management</li> 
	        <li><s:a action="vendor/listBidder">Bidder List</s:a></li>
	        
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
		
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
					
			<s:hidden name="vndHeader.vendorId" />
			<div class="box">
				<div class="title center">
					Pencarian
					<span class="hide"></span>
				</div>
				<div class="content">
					<s:form method="post" id="bidderSearch" action="vendor/listBidder">
					<table class="form formStyle1">
						<tr>
							<td>Kantor</td>
							<td>
								<s:select list="listKantor" listKey="id" listValue="districtName" name="kantor" headerKey="0" headerValue="--Semua--"></s:select>
							</td>
						</tr>
						<tr>
							<td>Komoditi</td>
							<td>
								<s:select list="listGrup" listKey="groupCode" listValue="groupName" headerKey="0" name="komoditi" headerValue="--Semua--"></s:select>
							</td>
						</tr>
						<tr>
							<td>Klasifikasi</td>
							<td>
								<s:select list="#{'0':'--Semua--','1':'KECIL', '2':'NON - KECIL'}" name="klasifikasi"  cssStyle="width:200px" ></s:select>
							</td>
						</tr>
						<tr>
							<td>Nama Mitra Kerja</td>
							<td>
								<s:textfield name="namaVendor" onkeyup="upper(this);"></s:textfield>
							</td>
						</tr>
					</table>
					
					<div class="uibutton-toolbar extend">
								<input type="button" value="Pencarian" class="uibutton" id="searchButton"/> 
					</div>
					</s:form>
				</div>
			</div>
			<!-- Nama Perusahaan -->
			<div class="box">
				<div class="title center">
					Daftar Bidder List
					<span class="hide"></span>
				</div>
				<div class="content center">
						<div id="hasil">
						</div>
				</div>
			</div>
			
			<div class="box">
					<div class="title">
						Detail Pengadaan
						<span class="hide"></span>
					</div>
					
					<div class="content">
						<div id="detail"></div>
					</div>
				</div>
			
			<div class="box">
					<div class="title">
						Detail Dokumen
						<span class="hide"></span>
					</div>
					
					<div class="content">
						<div id="dokumen"></div>
					</div>
				</div>
				
		</div>
	</div>
</body>
</html>