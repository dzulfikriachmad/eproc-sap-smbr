<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Dashboard - To Do List</title>
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	<script type="text/javascript">
		$(document).ready(function(){	
			//statistikProcurementValue();
			//statistikProcurementProses();
		});
		
		function statistikProcurementValue(){
			$.ajax({
				url: "<s:url action="ajax/json/dashboardProcurementValue" />", 
		        beforeSend: function(){
		        	$('#chartContainer').html('<img alt="" src="${contextPath}/assets/images/loading/loading-circle.gif">');
		        },
		        success: function(response){
		        		var dataXml = response.globalReturn;
		        		var chart = new FusionCharts('MSColumn3D', 'ChartId2', '40%', '30%');
	                 	chart.setXMLData(dataXml);
	                 	chart.render("chartContainer");
		            },
		        type: "get", 
		        dataType: "json"
			});
		}
		
		function statistikProcurementProses(){
			$.ajax({
				url: "<s:url action="ajax/json/dashboardProcurementProses" />", 
		        beforeSend: function(){
		        	$('#chartContainerProses').html('<img alt="" src="${contextPath}/assets/images/loading/loading-circle.gif">');
		        },
		        success: function(response){
		        		var dataXml = response.globalReturn;
		        		var chart = new FusionCharts('SSGrid', 'ChartId3', '40%', '40%');
	                 	chart.setXMLData(dataXml);
	                 	chart.render("chartContainerProses");
		            },
		        type: "get", 
		        dataType: "json"
			});
		}
		
		Cufon.replace('h1#welcome', {fontFamily: 'Caviar Dreams'});
		
		//Disable Enter key on form
		function stopRKey(evt) {
		  var evt = (evt) ? evt : ((event) ? event : null);
		  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
		  if ((evt.keyCode == 13) && (node.type=="text"))  {return false;}
		}
		
		document.onkeypress = stopRKey;
	</script>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="dashboard">Home</s:a></li> 
	        <li>Beranda (To Do List)</li> 
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
    
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
			

			<div style="margin: 20px auto; width: 100%; text-align: center;">
				<h1 id="welcome">..:: Selamat Datang di  Procurement Module ::..</h1>
			</div>	
				
				<!-- <div class="box">
						<div class="title">
							Dashboard Summary
							<span class="hide"></span>
						</div>
						<div class="content center">
							<div id="chartContainer" style="float: left"></div>
							<div id="chartContainerProses"></div>
						</div>
				</div>	 -->
			
			
				<div class="box">
					<div class="title">
						Proses Permintaan, persiapan pengadaan dan proses pengadaan
						<span class="hide"></span>
					</div>
					<div class="content center">
						<display:table name="${listTodo }" id="s" excludedParams="*" style="width: 100%;" class="all style1">
							<display:setProperty name="paging.banner.full" value=""></display:setProperty>
							<display:setProperty name="paging.banner.first" value=""></display:setProperty>
							<display:setProperty name="paging.banner.last" value=""></display:setProperty>
							
							<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
							<display:column title="Permintaan" total="true" style="width: 200px;">
								<c:choose>
									<c:when test="${s.nomorSpph==null or s.nomorSpph=='' }">
										${s.nomorPr }
									</c:when>
									<c:otherwise>
										${s.nomorSpph }
									</c:otherwise>
								</c:choose>
							</display:column>
							<display:column title="Nama Permintaan" style="width: 150px; " class="center">${s.judulSpph}</display:column>
							<display:column title="User" style="width: 150px;" class="center">${s.completeName }</display:column>
							<display:column title="Tanggal" class="center" style="width: 200px;"><fmt:formatDate value="${s.updatedDate }" pattern="HH:mm:ss, dd MMM yyyy"/> </display:column>
							<display:column title="Status" class="center" style="width: 200px;">
								${s.keterangan }
							</display:column>
							<display:column title="Proses" class="center" style="width: 200px;">
								<%-- <a href="${contextPath }/${s.url}.jwebs" target="_parent" class="uibutton" onclick="loadingScreen();">Lihat</a> --%>
								<form id="${s_rowNum }" action="${s.url }.jwebs" method="post">
									<input type="hidden" name="prcMainHeader.ppmId" value="${s.ppmId }"/>
									<button name="proses" value="Proses" class="uibutton" onclick="loadingScreen();">Proses</button>
								</form>
							</display:column>
						</display:table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Panitia
						<span class="hide"></span>
					</div>
					<div class="content center">
						<display:table name="${listTodoPanitiaPp }" id="k" excludedParams="*" style="width: 100%;" class="all style1">
							<display:setProperty name="paging.banner.full" value=""></display:setProperty>
							<display:setProperty name="paging.banner.first" value=""></display:setProperty>
							<display:setProperty name="paging.banner.last" value=""></display:setProperty>
							
							<display:column title="No." style="width: 20px; text-align: center;">${k_rowNum}</display:column>
							<display:column title="Permintaan" total="true" style="width: 200px;">
								<c:choose>
									<c:when test="${k.nomorSpph==null or k.nomorSpph=='' }">
										${k.nomorPr }
									</c:when>
									<c:otherwise>
										${k.nomorSpph }
									</c:otherwise>
								</c:choose>
							</display:column>
							<display:column title="Nama Permintaan" style="width: 150px; " class="center">${k.judulSpph}</display:column>
							<display:column title="User" style="width: 150px;" class="center">${k.completeName }</display:column>
							<display:column title="Tanggal" class="center" style="width: 200px;"><fmt:formatDate value="${k.updatedDate }" pattern="HH:mm:ss, dd MMM yyyy"/> </display:column>
							<display:column title="Status" class="center" style="width: 200px;">
								${k.keterangan }
							</display:column>
							<display:column title="Proses" class="center" style="width: 200px;">
								<%-- <a href="${contextPath }/${k.url}.jwebs" target="_parent" class="uibutton" onclick="loadingScreen();">Lihat</a> --%>
								<form id="${k_rowNum }" action="${k.url }.jwebs" method="post">
									<input type="hidden" name="prcMainHeader.ppmId" value="${k.ppmId }"/>
									<button name="proses" value="Proses" class="uibutton" onclick="loadingScreen();">Proses</button>
								</form>
							</display:column>
						</display:table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Kontrak / PO
						<span class="hide"></span>
					</div>
					<div class="content center">
						<display:table name="${listTodoKontrak }" id="j" excludedParams="*" style="width: 100%;" class="all style1">
							<display:setProperty name="paging.banner.full" value=""></display:setProperty>
							<display:setProperty name="paging.banner.first" value=""></display:setProperty>
							<display:setProperty name="paging.banner.last" value=""></display:setProperty>
							
							<display:column title="No." style="width: 20px; text-align: center;">${j_rowNum}</display:column>
							<display:column title="Permintaan" total="true" style="width: 200px;">
								${j.nomorSpph }
							</display:column>
							<display:column title="Kontrak /PO" total="true" style="width: 200px;">
								${j.nomorKontrak }
							</display:column>
							<display:column title="Nama Permintaan" style="width: 150px; " class="center">${j.judulSpph}</display:column>
							<display:column title="User" style="width: 150px;" class="center">${j.completeName }</display:column>
							<display:column title="Tanggal" class="center" style="width: 200px;"><fmt:formatDate value="${j.updatedDate }" pattern="HH:mm:ss, dd MMM yyyy"/> </display:column>
							<display:column title="Pemenang" class="center" style="width: 200px;">
								${j.pemenang }
							</display:column>
							<display:column title="Status" class="center" style="width: 200px;">
								${j.keterangan }
							</display:column>
							<display:column title="Proses" class="center" style="width: 200px;">
								<%-- <a href="${contextPath }/${j.url}.jwebs" target="_parent" class="uibutton" onclick="loadingScreen();">Lihat</a> --%>
								<form id="${j_rowNum }" action="${j.url }.jwebs" method="post">
									<input type="hidden" name="contractHeader.contractId" value="${j.contractId }"/>
									<button name="proses" value="Proses" class="uibutton" onclick="loadingScreen();">Proses</button>
								</form>
							</display:column>
						</display:table>
					</div>
				</div>				
		</div>
	</div>
</body>
</html>