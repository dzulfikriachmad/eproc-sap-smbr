<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Laporan Statistik Vendor</title>
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	<script type="text/javascript">
		$(document).ready(function(){	
			
		});
	</script>
	<script type="text/javascript" src="${contextPath}/assets/js/FusionCharts.js"></script>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="dashboard">Home</s:a></li> 
	        <li>Laporan</li> 
	        <li>Statistik Vendor</li> 
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
    
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
			
				<div class="box">
					<div class="title">
						Laporan Statistik Vendor
						<span class="hide"></span>
					</div>
					<div class="content center">
						<s:form>
							<table style="margin-bottom: 30px;">
								<tr>
									<td width="70px">District</td>
									<td>:&nbsp;</td>
									<td><s:select name="dist.id" list="listAdmDistrict" listKey="id" listValue="districtName" headerKey="0" headerValue="Semua District"></s:select> </td>
								</tr>
								<tr>
									<td></td>
									<td>&nbsp;</td>
									<td><s:submit name="view" value="Lihat" cssClass="uibutton"></s:submit> </td>
								</tr>
							</table>
						</s:form>
						<!-- Fussion Chart -->
						<%=request.getAttribute("reportVendor") %>
					</div>
				</div>
				
		</div>
	</div>
</body>
</html>