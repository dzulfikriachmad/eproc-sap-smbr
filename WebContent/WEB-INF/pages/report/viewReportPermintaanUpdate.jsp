<%@ include file="/include/definitions.jsp" %>
<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Update Permintaan</title>
	
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	
	<script type="text/javascript">	
		$(document).ready(function(){
			$(".chzn").chosen();
			// FORM VALIDATION
			$("#procForm").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler:function(form){
					FreezeScreen('Data Sedang di Proses');
					form.submit();
				}
			});
			
			
		});
		
		
		//dokumen vendor
		function addRow(tableId){
			var table = document.getElementById(tableId);
			var rowCount = table.rows.length;
			var row = table.insertRow(rowCount);
			
			var cell1 = row.insertCell(0);
			var element1 = document.createElement('input');
			element1.type = "checkbox";
			cell1.appendChild(element1);
			
			var cell3 = row.insertCell(1);
			cell3.innerHTML='<input type="text" name="ppdCategory" style="width:90%" maxlength="30"/>';
			
			var cell4 = row.insertCell(2);
			cell4.innerHTML='<input type="text" name="ppdDescription" style="width:96%" maxlength="40"/>';

			var cell5 = row.insertCell(3);
			cell5.innerHTML='<input type="file" name="ppdDocument" size="40" value="[]" id="ppdDocument"/>';
		}

		function delRow(tableId){
			try{
				var table = document.getElementById(tableId);
				var rowCount = table.rows.length;
				for(var i=0;i<rowCount;i++){
					var row = table.rows[i];
					var chkbox = row.cells[0].childNodes[0];
					if(null!=chkbox && true == chkbox.checked){
						table.deleteRow(i);
						rowCount--;
						i--;
					}
				}
			}catch(e){
				alert(e);
			}
		}

		function cekValue(obj){
			if(obj.checked){
				obj.value='1';
			}else{
				obj.value='0';
			}
		}
	</script>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="dashboard">Home</s:a></li>  
	        <li>Update Permintaan</li>
	        
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
    
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
					
			<s:form action="report/permintaanUpdate" id="procForm" name="procForm" enctype="multipart/form-data" method="post">
				<s:hidden name="prcMainHeader.ppmId" />
				<s:hidden name="prcMainHeader.admProses.id"></s:hidden>
				<s:hidden name="prcMainHeader.ppmProsesLevel"></s:hidden>
				<s:hidden name="prcMainHeader.groupCode"></s:hidden>
				
				<div class="box">
					<div class="title">
						Header
						<span class="hide"></span>
					</div>
					<div class="content">
						<jsp:include page="../proc/tmp/headerSpph.jsp"></jsp:include>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Item
						<span class="hide"></span>
					</div>
					<div class="content center">
						<jsp:include page="../proc/tmp/itemSpph.jsp"></jsp:include>
					</div>
				</div>

				<div class="box">
					<div class="title">
						Estimator
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle">
							<tr>
								<td width="150px;">Estimator</td>
								<td>
									<%-- <c:choose>
										<c:when test="${not empty prcMainHeader.admUserByPpmEstimator.id }">
											${prcMainHeader.admUserByPpmEstimator.completeName }
										</c:when>
										<c:otherwise>
											<s:select name="prcMainHeader.admUserByPpmEstimator.id" list="listEstimator" listKey="id" listValue="completeName" cssStyle="width:300px" cssClass="chzn required"></s:select>	
										</c:otherwise>
									</c:choose> --%>
									<s:select name="prcMainHeader.admUserByPpmEstimator.id" list="listEstimator" listKey="id" listValue="completeName" cssStyle="width:300px" cssClass="chzn required"></s:select>	
								</td>
							</tr>						
						</table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Ka.Bag / Ka.Sie Pengadaan
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle">
							<tr>
								<td width="250px;">Ka.Bag / Ka.Sie Pengadaan</td>
								<td><s:select list="listUser" listKey="id" listValue="completeName" name="prcMainHeader.pengawasPp.id" cssClass="chzn" cssStyle="width:400px"></s:select></td>
							</tr>						
						</table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Pembeli
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle">
							<tr>
								<td width="250px;">Pembeli</td>
								<td>
									<s:select list="listUser" listKey="id" listValue="completeName" name="prcMainHeader.admUserByPpmBuyer.id" cssClass="chzn" cssStyle="width:400px"></s:select>
								</td>
							</tr>						
						</table>
					</div>
				</div>
				
				<div id="result">
				<c:if test="${prcMainHeader.panitia!=null }">
					<jsp:include page="../proc/tmp/panitiaUpdate.jsp"></jsp:include>
				</c:if>
				
				<c:if test="${prcMainHeader.panitia==null }">
					<div class="box">
						<div class="title">
							Panitia Pengadaan <span class="hide"></span>
						</div>
						<div class="content center">
							<s:url action="ajax/report/panitiaHeaderUpdate" var="urlHeader"/>
							<a href="javascript:void(0)" onclick="load_into_box('${urlHeader}','panitia.id=${panitia.id }&prcMainHeader.ppmId=${prcMainHeader.ppmId }');" class="uibutton">Tambah Panitia</a>
						</div>
					</div>
				</c:if>
				</div>
				
				<div class="box">
					<div class="title">
						Lampiran Dokumen Untuk Vendor
						<span class="hide"></span>
					</div>
					<div class="content center">
						<input type="button" onclick="addRow('tblDoc')" value="Tambah" class="uibutton icon add">
						<input type="button" onclick="delRow('tblDoc')" value="Hapus" class="uibutton">
						
						<table class="style1" id="tblDoc" style="margin-top: 10px;">
							<thead>
								<tr>
									<th></th>
									<th width="30%">
										Nama Dokumen
									</th>
									<th width="50%" >
										Deskripsi
									</th>
		
									<th width="20%" >
										Dokumen
									</th>
								</tr>
							</thead>
						</table>
						
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Lampiran Dokumen Untuk Vendor
						<span class="hide"></span>
					</div>
					<div class="content center">
						<s:set id="i" value="0"></s:set>
						<display:table list="${listPrcMainDoc }" id="k" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
							<display:column title="No." style="width: 20px; text-align: center;">${k_rowNum}</display:column>
							<display:column title="Kategori " style="width: 100px;" class="center">${k.docCategory }</display:column>
							<display:column title="Nama" style="width: 100px;" class="center">${k.docName }</display:column>
							<display:column title="Dokumen" class="center" style="width: 200px;">
								<a href="${contextPath }/upload/file/${k.docPath }">${k.docPath }</a>
							</display:column>
							<display:column title="Hapus" class="center" style="width: 200px;">
								<input type="hidden" name="listPrcMainDoc[${k_rowNum-1 }].id" value="${k.id }"/>
								<input type="hidden" name="listPrcMainDoc[${k_rowNum-1 }].prcMainHeader.ppmId" value="${k.prcMainHeader.ppmId }"/>
								<s:checkbox name="listPrcMainDoc[%{#i}].updatedBy" onclick="cekValue(this);"></s:checkbox>
							</display:column>
							<s:set var="i" value="#i+1"></s:set>
						</display:table>
					</div>
				</div>
				
				
				
				
				<div class="box">
					<div class="title">
						Daftar Komentar
						<span class="hide"></span>
					</div>
					<div class="content center">
						<display:table name="${listHistMainComment }" id="k" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
							<display:column title="No." style="width: 20px; text-align: center;">${k_rowNum}</display:column>
							<display:column title="Nama" style="width: 100px;" class="center">${k.username }</display:column>
							<display:column title="Posisi" style="width: 100px;" class="center">${k.posisi }</display:column>
							<display:column title="Proses" style="width: 100px;" class="center">${k.prosesAksi }</display:column>
							<display:column title="Tgl Pembuatan" style="width: 100px;" class="center">
								<fmt:formatDate value="${k.createdDate }" pattern="dd.MM.yyyy HH:mm"/> 
							</display:column>
							<display:column title="Tgl Update" style="width: 100px;" class="center">
								<fmt:formatDate value="${k.updatedDate }" pattern="dd.MM.yyyy HH:mm"/> 
							</display:column>
							<display:column title="Aksi" class="center" style="width: 250px;">${k.aksi }</display:column>
							<display:column title="Komentar" class="center" style="width: 250px;">${k.comment }</display:column>
							<display:column title="Dokumen" class="center" style="width: 200px;">
								<a href="${contextPath }/upload/file/${k.document }" target="_blank">${k.document }</a>
							</display:column>
						</display:table>
					</div>
				</div>	
				
				<div class="box">
					<div class="title">
						Komentar
						<span class="hide"></span>
					</div>
					<div class="content">
						<s:hidden name="histMainComment.username" value="%{admEmployee.empFullName}" />
						<table class="form formStyle">
							<tr>
								<td width="250px;">Komentar (*)</td>
								<td><s:textarea name="histMainComment.comment" cssClass="required" id="komentarVal"></s:textarea> </td>
							</tr>	
							<tr>
								<td width="250px;">Lampiran</td>
								<td><s:file name="docsKomentar" cssClass="uibutton" /></td>
							</tr>							
						</table>
					</div>
				</div>
							
				<div class="uibutton-toolbar extend">
					<s:submit name="saveAndFinish" value="Simpan dan Selesai" id="saveAndFinish" onclick="actionAlert('saveAndFinish','simpan dan selesai');return false;" cssClass="uibutton special"></s:submit>
					<a href="javascript:void()" class="uibutton" onclick="history.go(-1);"><s:text name="cancel"></s:text> </a>					
				</div>
						
			</s:form>
			
		</div>
	</div>
</body>
</html>