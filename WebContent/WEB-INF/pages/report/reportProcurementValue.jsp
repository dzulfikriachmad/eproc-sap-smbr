<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<title>Laporan Nilai Pengadaan</title>
	
	<script type="text/javascript">
		$(document).ready(function(){
			$.datepicker.setDefaults($.datepicker.regional['in_ID']);
			$("#tgl1").datepicker({
				changeYear: true,
				changeMonth: true
			});
			$("#tgl2").datepicker({
				changeYear: true,
				changeMonth: true
			});
		});
		
		function detailProcurement(metode){
			$.ajax({
		        url: "<s:url action="ajax/report/procurementValueDetail" />?metode="+metode, 
		        beforeSend: function(){
		        	jQuery.facebox('<div style="padding: 17px; font-size: 36px;">Loading....</div>');
		        },
		        success: function(response){
		        		jQuery.facebox.close();
		                $('#detail').html(response);
		            },
		        type: "post", 
		        dataType: "html"
		    }); 
		}
	</script>
	<script type="text/javascript">
		function setValue(id){
			$('#ctrId').val(id);
		}
	</script>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="dashboard">Home</s:a></li> 
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
	
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
			
				<div class="box">
					<div class="title">
						Laporan Pengadaan
						<span class="hide"></span>
					</div>
					
					<div class="content">	
					<s:form method="post">
					<table style="margin-bottom: 30px;" class="form formStyle1">
						<tr>
							<td width="70px">Periode</td>
							<td>:&nbsp;</td>
							<td>
								<s:textfield name="startDate" id="tgl1"></s:textfield>
								&nbsp; S/D &nbsp;
								<s:textfield name="endDate" id="tgl2"></s:textfield>
							</td>
						</tr>
						
					</table>
					<div class="uibutton-toolbar extend">
						<s:submit name="cari" value="Cari" cssClass="uibutton" onclick="loadingScreen();"></s:submit>
										
					</div>
					</s:form>					
					<c:set value="0" var="ee"></c:set>
					<c:set value="0" var="oe"></c:set>
					<c:set value="0" var="ko"></c:set>
					<c:set value="0" var="jml"></c:set>
					<display:table name="${listProcurementValue }" id="s" excludedParams="*" style="width: 100%;" class="all style1">
								<display:setProperty name="paging.banner.full" value=""></display:setProperty>
								<display:setProperty name="paging.banner.first" value=""></display:setProperty>
								<display:setProperty name="paging.banner.last" value=""></display:setProperty>
								
								<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
								<display:column title="Metode" total="true" style="width: 200px;">
									<a href="javascript:void(0)" onclick="detailProcurement(${s.id})">
										${s.metode }
									</a>
								</display:column>
								<display:column title="Jumlah"  style="width: 30px;text-align:center">
									<fmt:formatNumber> ${s.frequensi } </fmt:formatNumber>
									<c:set value="${jml+s.frequensi }" var="jml"></c:set>
								</display:column>
								<display:column title="Nilai EE" style="text-align:right">
									<fmt:formatNumber> ${s.EE }</fmt:formatNumber>
									<c:set value="${ee+s.EE }" var="ee"></c:set>
								</display:column>
								<display:column title="Nilai OE"  style="text-align:right">
									<fmt:formatNumber> ${s.OE }</fmt:formatNumber>
									<c:set value="${oe+s.OE }" var="oe"></c:set>
								</display:column>
								<display:column title="Kontrak"  style="text-align:right">
									<fmt:formatNumber> ${s.kontrak }</fmt:formatNumber>
									<c:set value="${ko+s.kontrak }" var="ko"></c:set>
								</display:column>
								<display:footer>
									<tr style="font-weight:bold;color:red">
										<td colspan="2" style="text-align:right">Total</td>
										<td style="text-align:center"><fmt:formatNumber>${jml }</fmt:formatNumber> </td>
										<td style="text-align:right"><fmt:formatNumber>${ee }</fmt:formatNumber> </td>
										<td style="text-align:right"><fmt:formatNumber>${oe }</fmt:formatNumber> </td>
										<td style="text-align:right"><fmt:formatNumber>${ko }</fmt:formatNumber> </td>
									</tr>
								</display:footer>
							</display:table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Detail Proses Pengadaan (hari)
						<span class="hide"></span>
					</div>
					
					<div class="content">
						<div id="detail"></div>
					</div>
				</div>
		</div>
	</div>
</body>
</html>