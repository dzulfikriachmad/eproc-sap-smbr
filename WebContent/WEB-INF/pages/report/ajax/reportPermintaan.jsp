<%@ include file="/include/definitions.jsp"%>
<s:form action="report/permintaan" method="post">
<s:hidden name="prcMainHeader.ppmId" id="ppmId"></s:hidden>

<display:table name="${listPrcMainHeader }" id="s" pagesize="10"
	style="width: 100%;" class="style1"
	requestURIcontext="true"
	requestURI="${contextPath }/ajax/report/permintaan.jwebs"
	size="resultSize" partialList="true">
	<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
	<display:column title="Nomor PR" total="true" style="width: 200px;">
			${s.ppmNomorPr }
	</display:column>
	<display:column title="Nomor SPPH" total="true" style="width: 200px;">
		${s.ppmNomorRfq }
	</display:column>
	<display:column title="Nama Permintaan" style="width: 150px; "
		class="center">${s.ppmSubject }</display:column>
	<display:column title="Proses Saat Ini" style="width: 150px;" class="center">
		<c:if test="${s.admProses.id==100 && s.ppmProsesLevel==1 }">
		      PEMBUATAN PR
		</c:if>
		<c:if test="${s.admProses.id==100 && s.ppmProsesLevel==2 }">
		      PERSETUJUAN PR
		</c:if>
		<c:if test="${s.admProses.id==100 && s.ppmProsesLevel==-1 }">
		      REVISI PR
		</c:if>
		<c:if test="${s.admProses.id==102 && s.ppmProsesLevel==2}">
		      PEMBUATAN OE
		</c:if>
		<c:if test="${s.admProses.id==102 && s.ppmProsesLevel==1}">
		      PEMILIHAN ESTIMATOR
		</c:if>
		<c:if test="${s.admProses.id==102 && (s.ppmProsesLevel==3||s.ppmProsesLevel==4||s.ppmProsesLevel==5||s.ppmProsesLevel==6)}">
		      PERSETUJUAN OE
		</c:if>
		<c:if test="${s.admProses.id==102 && s.ppmProsesLevel==-2}">
		      REVISI OE
		</c:if>
		<c:if test="${s.admProses.id==103 && s.ppmProsesLevel==1}">
		      DISPOSISI
		</c:if>
		<c:if test="${s.admProses.id==103 && s.ppmProsesLevel==2}">
		      PEMILIHAN BUYER
		</c:if>
		<c:if test="${s.admProses.id==103 && s.ppmProsesLevel==3}">
		      PERSIAPAN PENGADAAN
		</c:if>
		<c:if test="${s.admProses.id==103 && s.ppmProsesLevel==4}">
		      PEMILIHAN MITRA KERJA
		</c:if>
		<c:if test="${s.admProses.id==103 && (s.ppmProsesLevel==5||s.ppmProsesLevel==6||s.ppmProsesLevel==7||s.ppmProsesLevel==8||s.ppmProsesLevel==9)}">
		      PERSETUJUAN PENGADAAN
		</c:if>
		<c:if test="${s.admProses.id==103 && s.ppmProsesLevel==-3}">
		      REVISI PERSIAPAN PENGADAAN
		</c:if>
		<c:if test="${s.admProses.id==104 && s.ppmProsesLevel==1}">
		      PEMBUATAN ANGGARAN
		</c:if>
		<c:if test="${s.admProses.id==104 && (s.ppmProsesLevel==2||s.ppmProsesLevel==3||s.ppmProsesLevel==4||s.ppmProsesLevel==5)}">
		      PERSETUJUAN ANGGARAN
		</c:if>
		<c:if test="${s.admProses.id==104 && s.ppmProsesLevel==-1}">
		      REVISI ANGGARAN
		</c:if>
		<c:if test="${s.admProses.id==105 && s.ppmProsesLevel==1}">
		      UNDANG MITRA KERJA
		</c:if>
		<c:if test="${s.admProses.id==105 && s.ppmProsesLevel==2}">
		      AANWIJZING
		</c:if>
		<c:if test="${s.admProses.id==105 && s.ppmProsesLevel==3}">
		      PEMBUKAAN PENAWARAN
		</c:if>
		<c:if test="${s.admProses.id==105 && s.ppmProsesLevel==4}">
		      EVALUASI PENAWARAN
		</c:if>
		<c:if test="${s.admProses.id==105 && s.ppmProsesLevel==5}">
		      NEGOSIASI
		</c:if>
		<c:if test="${s.admProses.id==105 && s.ppmProsesLevel==6}">
		      USULAN PEMENANG
		</c:if>
		<c:if test="${s.admProses.id==105 && (s.ppmProsesLevel==7||s.ppmProsesLevel==8||s.ppmProsesLevel==9||s.ppmProsesLevel==10||s.ppmProsesLevel==11||s.ppmProsesLevel==12||s.ppmProsesLevel==13||s.ppmProsesLevel==14||s.ppmProsesLevel==15||s.ppmProsesLevel==16)}">
		      PERSETUJUAN PEMENANG
		</c:if>
		<c:if test="${s.admProses.id==105 && s.ppmProsesLevel==17}">
		      FINALISASI PEMENANG
		</c:if>
		<c:if test="${s.admProses.id==106 && s.ppmProsesLevel==1}">
		      USULAN MITRA KERJA
		</c:if>
		<c:if test="${s.admProses.id==106 && (s.ppmProsesLevel==2||s.ppmProsesLevel==3||s.ppmProsesLevel==4)}">
		      PERSETUJUAN USULAN MITRA KERJA
		</c:if>
		<c:if test="${s.admProses.id==107 && s.ppmProsesLevel==1}">
		     PENGUMUMAN PRAKUALIFIKASI
		</c:if>
		<c:if test="${s.admProses.id==107 && s.ppmProsesLevel==2}">
		      PRA KUALIFIKASI
		</c:if>
		<c:if test="${s.admProses.id==107 && s.ppmProsesLevel==3}">
		      FINALISASI PRAKUALIFIKASI
		</c:if>
		<c:if test="${s.admProses.id==108 && s.ppmProsesLevel==1}">
		      PEMILIHAN PEMBUAT KONTRAK
		</c:if>
		<c:if test="${s.admProses.id==108 && (s.ppmProsesLevel==2||s.ppmProsesLevel==3||s.ppmProsesLevel==4||s.ppmProsesLevel==5||s.ppmProsesLevel==6||s.ppmProsesLevel==7)}">
		      PEMBUATAN DRAFT KONTRAK
		</c:if>
		<c:if test="${s.admProses.id==108 && s.ppmProsesLevel==8}">
		      FINALISASI DRAFT KONTRAK
		</c:if>
		<c:if test="${s.admProses.id==109 && s.ppmProsesLevel==1}">
		      PERSETUJUAN PANITIA PENGADAAN
		</c:if>
		<c:if test="${s.admProses.id==113 && s.ppmProsesLevel==1}">
		      REVISI EE
		</c:if>
		<c:if test="${s.admProses.id==113 && s.ppmProsesLevel==2}">
		      PERSETUJUAN REVISI EE
		</c:if>
		<c:if test="${s.admProses.id==114 && s.ppmProsesLevel==1}">
		     DOKUMEN REALOKASI ANGGARAN
		</c:if>
	</display:column>
	<display:column title="Pelaku Saat Ini" style="width: 150px;" class="center">
		<c:if test="${s.admUserByPpmCurrentUser!=null}">
			${s.admUserByPpmCurrentUser.completeName }
		</c:if>
		<c:if test="${s.admUserByPpmCurrentUser==null && s.ppmProsesLevel==99}">
			<span style="color: BLUE">SELESAI</span>
		</c:if>
		<c:if test="${s.admUserByPpmCurrentUser==null && s.ppmProsesLevel==-99}">
			<span style="color: RED">BATAL</span>
		</c:if>
		<c:if test="${s.admUserByPpmCurrentUser==null && s.ppmProsesLevel==-98}">
			<span style="color: RED">REBID</span>
		</c:if>
		<c:if test="${s.admUserByPpmCurrentUser==null && s.ppmProsesLevel==-100}">
			<span style="color: RED">MANUAL</span>
		</c:if>
	</display:column>
	<display:column title="Tanggal" class="center" style="width: 200px;">
		<fmt:formatDate value="${s.createdDate }"
			pattern="dd MMM yyyy" />
	</display:column>
	<display:column title="Aksi" class="center" style="width: 200px;">
		<input type="submit" name="proses" value="Lihat"onclick="setValue('${s.ppmId }');loadingScreen();" class="uibutton" />
	</display:column>
</display:table>
</s:form>
