<%@ include file="/include/definitions.jsp"%>
<script type="text/javascript">
				   $(document).ready(function() {
					// DATATABLE
					 $('table.doc').dataTable({
				        "bInfo": false,
				        "iDisplayLength": 10,
				        "aLengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
				        "sPaginationType": "full_numbers",
				        "bPaginate": true,
				        "sDom": '<f>t<pl>'
				        });
					});
			</script>
<s:form action="report/permintaan" method="post">
<s:hidden name="prcMainHeader.ppmId" id="ppmId"></s:hidden>
<display:table name="${listPrcMainHeader }" id="s"
	excludedParams="*" style="width: 100%;" class="doc style1">
	<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
	<display:column title="Nomor PR" total="true" style="width: 200px;">
			${s.ppmNomorPr }
	</display:column>
	<display:column title="Nomor SPPH" total="true" style="width: 200px;">
		${s.ppmNomorRfq }
	</display:column>
	<display:column title="Nama Permintaan" style="width: 150px; "
		class="center">${s.ppmSubject }</display:column>
	<display:column title="Tanggal" class="center" style="width: 200px;">
		<fmt:formatDate value="${s.createdDate }"
			pattern="dd MMM yyyy" />
	</display:column>
	<display:column title="Aksi" class="center" style="width: 200px;">
		<input type="submit" name="proses" value="Lihat"onclick="setValue('${s.ppmId }');loadingScreen();" class="uibutton" />
	</display:column>
</display:table>
</s:form>
