<%@ include file="/include/definitions.jsp"%>
<script type="text/javascript">
				   $(document).ready(function() {
					// DATATABLE
					 $('table.doc').dataTable({
				        "bInfo": false,
				        "iDisplayLength": 10,
				        "aLengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
				        "sPaginationType": "full_numbers",
				        "bPaginate": true,
				        "sDom": '<f>t<pl>'
				        });
					});
			</script>
<display:table name="${listDataSheetPengadaan }" id="s"
	excludedParams="*" style="width: 100%;" class="doc style1">
	<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
	<display:column title="Nomor PR" total="true" style="width: 200px;">
			${s.nomorPr }
	</display:column>
	<display:column title="Judul" total="true" style="width: 200px;">
		${s.judul }
	</display:column>
	<display:column title="Deskripsi" style="width: 150px; "
		class="center">${s.deskripsi }</display:column>
	
	<display:column title="Tanggal OE" class="center" >
		${s.tanggalOe }
	</display:column>
	<display:column title="Tanggal RKS" class="center" >
		${s.tanggalRks }
	</display:column>
	<display:column title="Tanggal MItra Kerja" class="center" >
		${s.tanggalMitra }
	</display:column>
	<display:column title="Tanggal Buka SPPH" class="center" >
		${s.tanggalBukaSpph }
	</display:column>
	<display:column title="Tanggal KA" class="center" >
		${s.tanggalKonfirmasiAnggaran }
	</display:column>
	<display:column title="Tanggal Negosiasi" class="center" >
		${s.tanggalNegosiasi }
	</display:column>
	
	<display:column title="Tanggal Evaluasi" class="center" >
		${s.tanggalEvaluasi }
	</display:column>
</display:table>
