			<%@ include file="/include/definitions.jsp" %>
			<center>
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>		
			
			<div style="margin-top: 20px;"></div>
			
			<s:set id="i" value="0"></s:set>
			<display:table name="${listNegosiasi }" id="s" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
				<display:caption>Histori Negosiasi</display:caption>
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column title="Dari" style="text-align:left">
					${fn:toUpperCase(s.msgFrom) }
				</display:column>
				<display:column title="Ke" style="text-align:left">
					${fn:toUpperCase(s.msgTo) }
				</display:column>
				<display:column title="Pesan"  style="text-align:left">
					${fn:toUpperCase(s.msgComment) }
				</display:column>
				<display:column title="Tanggal Pesan"  class="center">
					<fmt:formatDate value="${s.msgDate }" pattern="dd.MM.yyyy HH:mm:ss"/>	
				</display:column>
			<s:set var="i" value="#i+1"></s:set>
			</display:table>
			
			<br/>
			<display:table name="${listHargaNegosiasi }" id="s" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
				<display:caption>Histori Harga Negosiasi</display:caption>
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column title="Mitra Kerja" style="text-align:left">
					${fn:toUpperCase(s.vendorName) }
				</display:column>
				<display:column title="Harga" style="text-align:left">
					<fmt:formatNumber>${s.nilaiNego }</fmt:formatNumber>
				</display:column>
				<display:column title="Tanggal Update"  class="center">
					<fmt:formatDate value="${s.createdDate }" pattern="dd.MM.yyyy HH:mm:ss"/>	
				</display:column>
			</display:table>
			</center>
			