<%@ include file="/include/definitions.jsp"%>
<s:form action="report/permintaanUpdate" method="post">
<s:hidden name="prcMainHeader.ppmId" id="ppmId"></s:hidden>
<display:table name="${listPrcMainHeader }" id="s" pagesize="10"
	excludedParams="*" style="width: 100%;" class="style1"
	requestURIcontext="true"
	requestURI="${contextPath }/ajax/report/permintaanUpdate.jwebs"
	size="resultSize" partialList="true">
	<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
	<display:column title="Nomor PR" total="true" style="width: 200px;">
			${s.ppmNomorPr }
	</display:column>
	<display:column title="Nomor SPPH" total="true" style="width: 200px;">
		${s.ppmNomorRfq }
	</display:column>
	<display:column title="Nama Permintaan" style="width: 150px; "
		class="center">${s.ppmSubject }</display:column>
	<display:column title="Pelaku Saat Ini" style="width: 150px;" class="center">
		<c:if test="${s.admUserByPpmCurrentUser!=null}">
			${s.admUserByPpmCurrentUser.completeName }
		</c:if>
		<c:if test="${s.admUserByPpmCurrentUser==null && s.ppmProsesLevel==99}">
			<span style="color: BLUE">SELESAI</span>
			
		</c:if>
		<c:if test="${s.admUserByPpmCurrentUser==null && s.ppmProsesLevel==-99}">
			<span style="color: RED">BATAL</span>
		</c:if>
		<c:if test="${s.admUserByPpmCurrentUser==null && s.ppmProsesLevel==-98}">
			<span style="color: RED">REBID</span>
		</c:if>
		<c:if test="${s.admUserByPpmCurrentUser==null && s.ppmProsesLevel==-100}">
			<span style="color: RED">MANUAL</span>
		</c:if>
	</display:column>
	<display:column title="Tanggal" class="center" style="width: 200px;">
		<fmt:formatDate value="${s.createdDate }"
			pattern="dd MMM yyyy" />
	</display:column>
	<display:column title="Aksi" class="center" style="width: 200px;">
		<input type="submit" name="proses" value="Lihat"onclick="setValue('${s.ppmId }');loadingScreen();" class="uibutton" />
	</display:column>
</display:table>
</s:form>
