<%@ include file="/include/definitions.jsp"%>
<s:form action="report/kontrak">
	<s:hidden name="contractHeader.contractId" id="ctrId" />
	<display:table name="${listContractHeader }" id="s" excludedParams="*"
		style="width: 100%;" class="all style1" pagesize="10" partialList="true" size="resultSize" requestURI="${contextPath}/ajax/report/kontrak.jwebs" requestURIcontext="true">

		<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
		<display:column title="Nomor Kontrak" total="true"
			style="width: 200px;">${s.contractNumber }</display:column>
		<display:column title="Nama Kontrak" style="width: 150px; "
			class="center">${s.contractSubject }</display:column>
		<display:column title="Jumlah" style="width: 150px;" class="center"><fmt:formatNumber>${s.contractAmount }</fmt:formatNumber> </display:column>
		<display:column title="Tanggal" class="center" style="width: 200px;">
			<fmt:formatDate value="${s.createdDate }"
				pattern="HH:mm:ss, dd MMM yyyy" />
		</display:column>
		<display:column title="Status">
			<c:if test="${s.contractProsesLevel==99 }">Aktif</c:if>
			<c:if test="${s.contractProsesLevel==-98 }">Adendum</c:if>
		</display:column>
		<display:column title="Tanggal Mulai" class="center" style="width: 200px;">
			<fmt:formatDate value="${s.contractStart }"
				pattern="dd MMM yyyy" />
		</display:column>
		<display:column title="Tanggal Berakhir" class="center" style="width: 200px;">
			<fmt:formatDate value="${s.contractEnd }"
				pattern="dd MMM yyyy" />
		</display:column>
		<display:column title="Aksi" class="center" style="width: 100px;">
			<input type="submit" name="proses" value="Proses"
				onclick="setValue('${s.contractId }');loadingScreen()"
				class="uibutton"/>
		</display:column>
	</display:table>
</s:form>
