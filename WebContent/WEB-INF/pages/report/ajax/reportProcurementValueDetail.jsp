			<%@ include file="/include/definitions.jsp" %>
			<script type="text/javascript">
				   $(document).ready(function() {
					// DATATABLE
					 $('table.doc').dataTable({
				        "bInfo": false,
				        "iDisplayLength": 10,
				        "aLengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
				        "sPaginationType": "full_numbers",
				        "bPaginate": true,
				        "sDom": '<f>t<pl>'
				        });
					});
			</script>
			<center>
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>		
			<s:form action="report/permintaan" method="post">
			<s:hidden name="prcMainHeader.ppmId" id="ppmId"></s:hidden>			
			<display:table name="${listProcurementValue }" id="s"  excludedParams="*" style="width: 100%;" class="doc style1">
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column title="Nomor Spph" style="width: 200px; text-align:center" class="center">${s.nomorSpph}</display:column>
				<display:column title="OE" style="width: 200px; text-align:center " class="center">${s.hoe}</display:column>
				<display:column title="Anggaran" style="width: 200px; text-align:center " class="center">${s.hang}</display:column>
				<display:column title="Persiapan" style="width: 200px; text-align:center " class="center">${s.hpp}</display:column>
				<display:column title="Praqualifikasi" style="width: 200px; text-align:center " class="center">${s.hpq}</display:column>
				<display:column title="SPPH" style="width: 200px; text-align:center " class="center">${s.hspph}</display:column>
				<display:column title="Pembeli" style="width: 200px; text-align:center " class="center">${s.namaBuyer}</display:column>
				<display:column title="" media="html" style="width: 80px;" class="center">
					<input type="submit" name="proses" value="Proses"onclick="$('#ppmId').val(${s.ppmId});loadingScreen();" class="uibutton" />
				</display:column>
			</display:table>
			</s:form>
			</center>
			
			<script>
				$(document).ready(function(){	
					// SYSTEM MESSAGES
					$(".message").click(function () {
				      $(this).fadeOut();
				    });
				});
			</script>