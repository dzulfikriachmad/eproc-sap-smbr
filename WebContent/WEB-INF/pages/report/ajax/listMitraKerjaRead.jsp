			<%@ include file="/include/definitions.jsp" %>
			<center>
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>		
			
			<div style="margin-top: 20px;"></div>
			
			<s:set id="i" value="0"></s:set>

			<display:table name="${listVendor }" id="s" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
				<display:caption>Mitra Kerja</display:caption>
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column title="Nama Mitra Kerja" >
					<a href="javascript:void(0)" onclick="load_into_box('<s:url action="ajax/vendor/detail"/>','tmpVndHeader.vendorId=${s.vndHeader.vendorId }')">${fn:toUpperCase(s.vndHeader.vendorName) }</a>
				</display:column>
				<display:column title="Alamat">
					${fn:toUpperCase(s.vndHeader.vendorNpwpCity) }	
				</display:column>
				<display:column title="Status" style="width: 150px;" class="center">
					<c:if test="${s.pmpStatus==1 }">Diusulan</c:if>
					<c:if test="${s.pmpStatus==2 }">Di Undang</c:if>
					<c:if test="${s.pmpStatus==3 }">Mendaftar</c:if>
					<c:if test="${s.pmpStatus==-3 }">Tidak Mendaftar</c:if>
					<c:if test="${s.pmpStatus==4 }">Sudah Menawarkan</c:if>
					<c:if test="${s.pmpStatus==5 }">Lulus Administrasi</c:if>
					<c:if test="${s.pmpStatus==-5 }">Tidak Lulus Administrasi</c:if>
					<c:if test="${s.pmpStatus==6 }">Lulus Teknis</c:if>
					<c:if test="${s.pmpStatus==-6 }">Tidak Lulus Teknis</c:if>
					<c:if test="${s.pmpStatus==7 }">Lulus Harga</c:if>
					<c:if test="${s.pmpStatus==-7 }">Tidak Lulus Harga</c:if>	
				</display:column>
				<display:column title="" media="html" style="width: 30px;" class="center">
					<input type="hidden" name="listVendor[${s_rowNum-1 }].id.ppmId" value="${s.id.ppmId }"/>
					<input type="hidden" name="listVendor[${s_rowNum-1 }].id.vendorId" value="${s.id.vendorId }"/>
					<input type="hidden" name="listVendor[${s_rowNum-1 }].vndHeader.vendorId" value="${s.vndHeader.vendorId }"/>
				</display:column>
			<s:set var="i" value="#i+1"></s:set>
			</display:table>
			</center>
			