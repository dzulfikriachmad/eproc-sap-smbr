<%@ include file="/include/definitions.jsp"%>
<script type="text/javascript">
				   $(document).ready(function() {
					// DATATABLE
					 $('table.doc').dataTable({
				        "bInfo": false,
				        "iDisplayLength": 10,
				        "aLengthMenu": [[5, 10, 25, 50, 100,1000,10000], [5, 10, 25, 50, 100,1000,10000]],
				        "sPaginationType": "full_numbers",
				        "bPaginate": true,
				        "sDom": '<f>t<pl>'
				        });
					});
			</script>
<display:table name="${listRekapPembelian }" id="s"
	excludedParams="*" style="width: 100%;" class="doc style1">
	<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
	<display:column title="Nomor PR" total="true">
			${s.nomorPr }
	</display:column>
	
	<display:column title="Tanggal OE" class="center" >
		${s.tanggalOe }
	</display:column>
	<display:column title="Jumlah" class="center" >
		${s.jumlah }
	</display:column>
	<display:column title="" class="center" >
		${s.satuan }
	</display:column>
	<display:column title="Nama Barang" class="center" >
		${s.namaBarang }
	</display:column>
	<display:column title="Harga Satuan" class="center" style="text-align:right">
		<fmt:formatNumber>${s.harga }</fmt:formatNumber>
	</display:column>
	<display:column title="Harga Total" style="text-align:right" >
		<fmt:formatNumber>${s.hargaTotal }</fmt:formatNumber>
	</display:column>
	<display:column title="Nomor PO" class="center" >
		${s.nomorPo }
	</display:column>
	
	<display:column title="Tanggal PO" class="center" >
		${s.tanggalPo }
	</display:column>
	<display:column title="Tanggal Akhir PO" class="center" >
		${s.tanggalAkhirPo }
	</display:column>
	<display:column title="Tanggal Penyerahan" class="center" >
		${s.tanggalPenyerahan }
	</display:column>
	<display:column title="Mitra Kerja" class="center" >
		${s.namaVendor }
	</display:column>
</display:table>
