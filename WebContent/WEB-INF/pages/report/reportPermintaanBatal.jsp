<%@ include file="/include/definitions.jsp"%>

<!DOCTYPE html>
<html lang="id">
<head>
<title>Daftar Permintaan</title>
<script type="text/javascript">
	function setValue(id) {
		$('#ppmId').val(id);
	}
</script>
<script type="text/javascript">
	if (!display)
		var display = {};
	display.jwebs = {
		onTableLoad : function() {
			// call when data loaded 
			$("table#s th.sortable").each(function() {
				$(this).click(function() {
					var link = $(this).find("a").attr("href");
					jQuery.facebox('<div style="padding: 17px; font-size: 36px;">Loading....</div>');
					$("div#hasil").load(link, {}, display.jwebs.onTableLoad);
					jQuery.facebox.close();
					return false;
				});
			});

			$("div#hasil .pagelinks a").each(function() {
				// Iterate over the pagination-generated links to override also
				$(this).click(function() {
					var link = $(this).attr("href");
					jQuery.facebox('<div style="padding: 17px; font-size: 36px;">Loading....</div>');
					$("div#hasil").load(link, {}, display.jwebs.onTableLoad);
					jQuery.facebox.close();
					return false;
				});
			});
		}
	};

	$(document).ready(
			function() {
				/* jQuery.facebox('<div style="padding: 17px; font-size: 36px;">Loading....</div>');
				$("div#hasil").load(
						"${contextPath}/ajax/report/permintaanBatal.jwebs", {},display.jwebs.onTableLoad);
				jQuery.facebox.close(); */
			});

	$("#searchButton").live("click",function(){
		if($("#searchNomoPr").val().length!=0){
		$.ajax({
	        url: "<s:url action="ajax/report/searchPengadaanBatal" />", 
	        data: $(document.formSearchPr.elements).serialize(),
	        beforeSend: function(){
	        	$("#hasil").html('<img src="${contextPath}/assets/images/loading/loading-circle.gif"/>');
	        },
	        success: function(response){
	                $("#hasil").html(response);
	            },
	        type: "post", 
	        dataType: "html"
	    }); 
		}
		
	});
	$("#searchNomoPr").live("keypress",function(e){
		   if(e.keyCode == 13){
			   $("#searchButton").click();
			   return false;
		   }
	   });
	/* $("#searchNomoPr").live("keyup",function(e){
		if($(this).val() == ""){
			$("div#hasil").load(
					"${contextPath}/ajax/report/permintaan.jwebs", {},display.jwebs.onTableLoad);
		}else{
			$("#searchButton").click();
		}
	}); */
</script>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
			<li><s:a action="dashboard">Home</s:a></li>
		</ul>
	</div>
	<!-- End of BreadCrumbs -->

	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red">
					<s:actionerror />
				</div>
			</s:if>
			<s:if test="hasActionMessages()">
				<div class="message green">
					<s:actionmessage />
				</div>
			</s:if>

			<div class="box">
				<div class="title">
					Daftar Permintaan Pengadaan <span class="hide"></span>
				</div>
				<div class="content">
					<s:form id="formSearchPr">
					<table style="margin-bottom: 30px;" class="form formStyle1">
						<tr>
							<td width="70px">Nomor PR</td>
							<td>:&nbsp;</td>
							<td>
								<s:textfield name="prcMainHeader.ppmNomorPr" id="searchNomoPr"></s:textfield>
								<s:hidden name="prcMainHeader.ppmProsesLevel" value="0"></s:hidden>
							</td>
						</tr>
						<tr>
							<td></td>
							<td>&nbsp;</td>
							<td>
								<input type="button" value="Cari" class="uibutton" id="searchButton"/> 
							</td>
						</tr>
					</table>
					</s:form>
					<div id="hasil"></div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>