<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<title>Daftar Kontrak</title>
	
	<script type="text/javascript">
		function setValue(id){
			$('#ctrId').val(id);
		}
	</script>
	<script type="text/javascript">
		if (!display)
			var display = {};
		display.jwebs = {
			onTableLoad : function() {
				// call when data loaded 
				$("table#s th.sortable").each(function() {
					$(this).click(function() {
						var link = $(this).find("a").attr("href");
						jQuery.facebox('<div style="padding: 17px; font-size: 36px;">Loading....</div>');
						$("div#hasil").load(link, {}, display.jwebs.onTableLoad);
						jQuery.facebox.close();
						return false;
					});
				});
	
				$("div#hasil .pagelinks a").each(function() {
					// Iterate over the pagination-generated links to override also
					$(this).click(function() {
						var link = $(this).attr("href");
						jQuery.facebox('<div style="padding: 17px; font-size: 36px;">Loading....</div>');
						$("div#hasil").load(link, {}, display.jwebs.onTableLoad);
						jQuery.facebox.close();
						return false;
					});
				});
			}
		};
	
		$(document).ready(
				function() {
// 					jQuery.facebox('<div style="padding: 17px; font-size: 36px;">Loading....</div>');
// 					$("div#hasil").load(
// 							"${contextPath}/ajax/report/kontrak.jwebs", {},display.jwebs.onTableLoad);
// 					jQuery.facebox.close();
					
					//init autoNumeric
					$('#contractStart').autoNumeric('init');
					$('#contractEnd').autoNumeric('init');
				});
	
		$("#searchButton").live("click",function(){
			//init autoNumeric
			$('#contractStart').val($('#contractStart').autoNumeric('get'));
			$('#contractEnd').val($('#contractEnd').autoNumeric('get'));
			if($("#searchNomoKontrak").val().length!=0){
			$.ajax({
		        url: "<s:url action="ajax/report/searchKontrak" />", 
		        data: $(document.formSearchKontrak.elements).serialize(),
		        beforeSend: function(){
		        	$("#hasil").html('<img src="${contextPath}/assets/images/loading/loading-circle.gif"/>');
		        },
		        success: function(response){
		                $("#hasil").html(response);
		            },
		        type: "post", 
		        dataType: "html"
		    }); 
			}
			
		});

	</script>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="dashboard">Home</s:a></li> 
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
	
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
			
				<div class="box">
					<div class="title">
						Daftar Kontrak
						<span class="hide"></span>
					</div>
					
					<div class="content">	
					<s:form id="formSearchKontrak">
					<table style="margin-bottom: 30px;" class="form formStyle1">
						<tr>
							<td width="70px">Nomor Kontrak</td>
							<td>:&nbsp;</td>
							<td>
								<s:textfield name="contractHeader.contractNumber" id="searchNomoKontrak"></s:textfield>
							</td>
						</tr>
						<tr>
							<td width="70px">Nilai Kontrak</td>
							<td>:&nbsp;</td>
							<td>
								 
								<s:textfield name="contractHeader.contractAmmendReason" id="contractStart" cssStyle="width:200px"></s:textfield>S/D
								<s:textfield name="contractHeader.contractAmount" id="contractEnd" cssStyle="width:200px"></s:textfield> 
							</td>
						</tr>
						<tr>
							<td width="70px">Tahun Dibuat</td>
							<td>:&nbsp;</td>
							<td>
								<s:select list="listOfYears" name="year" id="year"></s:select>
							</td>
						</tr>
						<tr>
							<td></td>
							<td>&nbsp;</td>
							<td>
								<input type="button" value="Cari" class="uibutton" id="searchButton"/> 
							</td>
						</tr>
						
					</table>
					</s:form>					
					<div id="hasil"></div>
					</div>
				</div>
		</div>
	</div>
</body>
</html>