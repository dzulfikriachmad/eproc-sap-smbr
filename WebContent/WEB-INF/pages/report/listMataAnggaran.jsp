<%@ include file="/include/definitions.jsp"%>

<!DOCTYPE html>
<html lang="id">
<head>
<title>Daftar Permintaan</title>
<script type="text/javascript">
	function setValue(id) {
		$('#ppmId').val(id);
	}
</script>
<script type="text/javascript">
	
	$("#searchButton").live("click",function(){
		$.ajax({
	        url: "<s:url action="ajax/rpt/updateMataAnggaran" />", 
	        data: $(document.formSearchPr.elements).serialize(),
	        beforeSend: function(){
	        	$("#result").html('<img src="${contextPath}/assets/images/loading/loading-circle.gif"/>');
	        },
	        success: function(response){
	                $("#result").html(response);
	            },
	        type: "post", 
	        dataType: "html"
	    }); 
	});
	
	$("#rkaId").live("keypress",function(e){
	   if(e.keyCode == 13){
		   $("#searchButton").click();
		   return false;
	   }
   });

</script>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
			<li><s:a action="dashboard">Home</s:a></li>
		</ul>
	</div>
	<!-- End of BreadCrumbs -->

	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red">
					<s:actionerror />
				</div>
			</s:if>
			<s:if test="hasActionMessages()">
				<div class="message green">
					<s:actionmessage />
				</div>
			</s:if>

			<div class="box">
				<div class="title">
					Daftar Anggaran Detail <span class="hide"></span>
				</div>
				<div class="content">
					<s:form id="formSearchPr">
					<table style="margin-bottom: 30px;" class="form formStyle1">
						<tr>
							<td width="70px">RKAID</td>
							<td>:&nbsp;</td>
							<td>
								<s:textfield name="anggaranJdeDetail.rkaid" id="rkaId"></s:textfield>
							</td>
						</tr>
						<tr>
							<td></td>
							<td>&nbsp;</td>
							<td>
								<input type="button" value="Cari" class="uibutton" id="searchButton"/> 
							</td>
						</tr>
					</table>
					</s:form>
					<div id="result">
						<jsp:include page="ajax/listMataAnggaran.jsp"></jsp:include>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>