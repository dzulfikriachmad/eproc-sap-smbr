<%@ include file="/include/definitions.jsp"%>

<!DOCTYPE html>
<html lang="id">
<head>
<title>Daftar Permintaan</title>
<script type="text/javascript">
		$(document).ready(function(){
			$.datepicker.setDefaults($.datepicker.regional['in_ID']);
			$("#tgl1").datepicker({
				changeYear: true,
				changeMonth: true
			});
			$("#tgl2").datepicker({
				changeYear: true,
				changeMonth: true
			});
		});
		
		function submitData(){
			sendForm(document.procForm, "<s:url action="ajax/report/dataSheetPengadaan" />", "#hasil");
		}
	</script>
	
<script type="text/javascript">
	function setValue(id) {
		$('#ppmId').val(id);
	}
</script>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
			<li><s:a action="dashboard">Home</s:a></li>
		</ul>
	</div>
	<!-- End of BreadCrumbs -->

	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red">
					<s:actionerror />
				</div>
			</s:if>
			<s:if test="hasActionMessages()">
				<div class="message green">
					<s:actionmessage />
				</div>
			</s:if>

			<div class="box">
				<div class="title">
					Daftar Data Sheet <span class="hide"></span>
				</div>
				<div class="content">
					<s:form method="post" id="procForm" name="procForm">
					<table style="margin-bottom: 30px;" class="form formStyle1">
						<tr>
							<td width="70px">Periode</td>
							<td>:&nbsp;</td>
							<td>
								<s:textfield name="startDate" id="tgl1"></s:textfield>
								&nbsp; S/D &nbsp;
								<s:textfield name="endDate" id="tgl2"></s:textfield>
							</td>
						</tr>
						
					</table>
					<div class="uibutton-toolbar extend">
						<input type="button" value="Cari" onclick="submitData();" class="uibutton"/>
										
					</div>
					</s:form>
					<div id="hasil">
					<jsp:include page="ajax/reportDataSheet.jsp"></jsp:include>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>