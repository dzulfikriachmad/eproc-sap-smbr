<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<title>Proses Pengadaan</title>
	
	<script type="text/javascript">
		$(document).ready(function(){
			$.datepicker.setDefaults($.datepicker.regional['in_ID']);
			$("#tgl1").datepicker({
				changeYear: true,
				changeMonth: true
			});
			$("#tgl2").datepicker({
				changeYear: true,
				changeMonth: true
			});
		});
		
		function detailProcurement(prosesId, prosesLevel){
			$.ajax({
		        url: "<s:url action="ajax/report/prosesPengadaanDetail" />?prosesId="+prosesId+"&prosesLevel="+prosesLevel, 
		        beforeSend: function(){
		        	jQuery.facebox('<div style="padding: 17px; font-size: 36px;">Loading....</div>');
		        },
		        success: function(response){
		        		jQuery.facebox.close();
		                $('#detail').html(response);
		            },
		        type: "post", 
		        dataType: "html"
		    }); 
		}
		
		function setValue(id) {
			$('#ppmId').val(id);
		}
	</script>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="dashboard">Home</s:a></li> 
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
	
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
			
				<div class="box">
					<div class="title">
						Proses Pengadaan
						<span class="hide"></span>
					</div>
					
					<div class="content">	
					
					<display:table name="${listProcurementProses }" id="s" excludedParams="*" style="width: 100%;" class="all style1">
								
								<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
								<display:column title="Nama Proses" total="true" style="width: 200px;">
									<a href="javascript:void(0)" onclick="detailProcurement(${s.prosesId},${s.tingkat })">
										${s.prosesName }
									</a>
								</display:column>
								<display:column title="Jumlah"  style="width: 30px;text-align:center">
									<fmt:formatNumber> ${s.jumlah } </fmt:formatNumber>
								</display:column>
							</display:table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Detail Pengadaan
						<span class="hide"></span>
					</div>
					
					<div class="content">
						<div id="detail"></div>
					</div>
				</div>
		</div>
	</div>
</body>
</html>