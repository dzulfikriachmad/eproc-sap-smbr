<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Report Anggaran</title>
	
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	
	<script type="text/javascript">	
		$(document).ready(function(){
			$(".chzn").chosen();
			// FORM VALIDATION
			$("#procForm").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler:function(form){
					FreezeScreen('Data Sedang di Proses');
					form.submit();
				}
			});
		});
		
		
		
	</script>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="dashboard">Home</s:a></li> 
	        <li>Laporan</li> 
	        <li>Report Anggaran</li> 
	        
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
    
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
					
			<s:form action="report/reportAnggaran" id="procForm" name="procForm" enctype="multipart/form-data" method="post">
				
				<div class="box">
					<div class="title">
						Pencarian
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle" >
							<tr>
								<td width="150px">Tahun Anggaran (2 digit terakhir)</td>
								<td>
									<s:textfield name="tahunAnggaran" cssClass="required number"></s:textfield>
								</td>
							</tr>
							<tr>
								<td width="150px">Kode Anggaran</td>
								<td>
									<s:textfield name="kodeAnggaran" cssClass="required"></s:textfield>
								</td>
							</tr>

						</table>
						
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Detail Anggaran
						<span class="hide"></span>
					</div>
					<div class="content center">
						<display:table name="${ListJdeMasterMain }" id="c" excludedParams="*" class="all style1">
						<display:setProperty name="paging.banner.full" value=""></display:setProperty>
						<display:setProperty name="paging.banner.first" value=""></display:setProperty>
						<display:setProperty name="paging.banner.last" value=""></display:setProperty>
						<display:column title="No." style="width: 20px; text-align: center;">
							${c_rowNum}
						</display:column>
						<display:column title="Kode" total="true" style="width: 90px;"  class="center">
							${c.key }
						</display:column>
						<display:column title="Nama" style="width: 340px; ">
							${c.value }
						</display:column>
						<display:column title="Rekening" style="width: 340px; ">
							${c.rekening }
						</display:column>
						<display:column title="Saldo" style="  text-align: right;">
								<fmt:formatNumber> ${c.saldoAwal }</fmt:formatNumber>
						</display:column>
						<display:column title="Transaksi" style="text-align: right;">
								<fmt:formatNumber> ${c.transaksi }</fmt:formatNumber>
						</display:column>
						<display:column title="Sisa" style="text-align: right;">
							<fmt:formatNumber> ${c.saldoAwal - c.transaksi } </fmt:formatNumber>
						</display:column>				
					</display:table>
					</div>
				</div>
				<div class="uibutton-toolbar extend">
					<s:submit name="find" value="Pencarian" cssClass="uibutton"></s:submit>					
				</div>
						
			</s:form>
			
		</div>
	</div>
</body>
</html>