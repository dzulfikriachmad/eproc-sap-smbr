<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Broadcast Message </title>
	
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	<script src="${contextPath }/assets/js/tiny_mce/tiny_mce.js" type="text/javascript"></script>
	<script type="text/javascript">	
		$(document).ready(function(){
			$(".chzn").chosen();
			// FORM VALIDATION
			$("#procForm").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler:function(form){
					FreezeScreen('Data Sedang di Proses');
					form.submit();
				}
			});
			
			tinyMCE.init({
				// General options
				mode : "exact",
				elements : "message",
				theme : "advanced",
				skin : "o2k7",
				skin_variant : "silver",
				plugins : "lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups,autosave",

				// Theme options
				theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
				theme_advanced_buttons2 : "bullist,numlist,separator,outdent,indent,separator,undo,redo,separator,link,unlink,anchor",
				theme_advanced_toolbar_location : "top",
				theme_advanced_toolbar_align : "left",
				theme_advanced_statusbar_location : "bottom",
				theme_advanced_resizing : true,
			});
			
			
		});
		
		//regCancel Action
		function regCancel(){
			jQuery.facebox.close();
			window.location = "<s:url action="adm/broadcastMessage" />"; 
		}
		
	</script>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="dashboard">Home</s:a></li> 
	        
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
    
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
					
			<s:form id="procForm" name="procForm" enctype="multipart/form-data" method="post">
				
				<div class="box">
					<div class="title">
						Email Message
						<span class="hide"></span>
					</div>
					<div class="content">
						<table  class="form formStyle" >

							<tr id="email">
								<td width="250px;">Email</td>
								<td>
									<s:textfield name="kepada" placeholder="Email Disini"></s:textfield>
								</td>
							</tr>
							<tr>
								<td width="250px;">Message</td>
								<td>
									<s:textarea name="broadCastMessage" id="message" cssStyle="width:100%"></s:textarea>
								</td>
							</tr>
							
						</table>
						
					</div>
				</div>

				<div class="uibutton-toolbar extend">
					<s:submit name="save" value="Kirim" cssClass="uibutton special"></s:submit>	
					<a href="javascript:void(0)" class="uibutton" onclick="confirmPopUp('regCancel();', 'Batal', 'Apakah anda yakin untuk membatalkan transaksi ?', 'Ya', 'Tidak');"><s:text name="cancel"></s:text> </a>					
				</div>
						
			</s:form>
			
		</div>
	</div>
</body>
</html>