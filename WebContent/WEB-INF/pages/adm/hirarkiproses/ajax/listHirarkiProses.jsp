			<%@ include file="/include/definitions.jsp" %>
			<script type="text/javascript">
				   $(document).ready(function() {
					// DATATABLE
					 $('table.doc').dataTable({
				        "bInfo": false,
				        "iDisplayLength": 10,
				        "aLengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
				        "sPaginationType": "full_numbers",
				        "bPaginate": true,
				        "sDom": '<f>t<pl>'
				        });
					});
			</script>
			<center>
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>		
						
			<display:table name="${listHirarki }" id="s" requestURI="ajax/adm/listHirarkiProses.jwebs" excludedParams="*" style="width: 100%;" class="doc style1">
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column title="Kantor" style="width: 100px; text-align:left " group="1" class="center">${s.admDistrict.districtName}</display:column>
				<display:column title="Satker" style="width: 100px; text-align:left " group="2" class="center">${s.admDept.deptName}</display:column>
				<display:column title="Nama Proses" style="width: 100px; text-align:left " group="3" class="center">${s.admProses.prosesName}</display:column>
				<display:column title="Posisi" style="width: 200px; text-align:left " class="center">${s.admRoleByCurrentPos.roleName}</display:column>
				<display:column title="Posisi Selanjutnya" style="width: 200px; text-align:left " class="center">${s.admRoleByNextPos.roleName}</display:column>
				<display:column title="Batas Kewenangan" style="width: 200px; text-align:left " class="center">
					<fmt:formatNumber> ${s.minimalValue} </fmt:formatNumber> 
				</display:column>
				<display:column title="Tingkat">${s.tingkat }</display:column>
				<display:column title="Keterangan">${s.keterangan }</display:column>
				<display:column title="Aksi" media="html" style="width: 80px;" class="center">
					<div class="uibutton-group">
						<s:url var="set" action="ajax/adm/crudHirarkiProses" />
						<a <c:if test="${update !=1 }"> style="visibility:hidden"</c:if> title="Ubah data Hirarki Proses" class="uibutton icon edit" href='javascript:void(0)' onclick='load_into_box("${set}", "xCommand=edit&hirarki.id=${s.id}");'>&nbsp;</a>
						<a <c:if test="${create !=1 }"> style="visibility:hidden"</c:if> title="Tambah data Hirarki Proses" class="uibutton icon add" href='javascript:void(0)' onclick='load_into_box("${set}", "xCommand=child&hirarki.id=${s.id}");'>&nbsp;</a>
						<a <c:if test="${delete !=1 }"> style="visibility:hidden"</c:if> title="Hapus data Hirarki Proses" class="uibutton" href='javascript:void(0)' onclick='confirmPopUp("deleteObj(\"${set}\", \"xCommand=delete&hirarki.id=${s.id}\", \"#result\")", "Hapus Data", "Yakin ingin menghapus?", "Hapus", "Batal");'>X</a>
					</div>
				</display:column>
			</display:table>
			</center>
			
			<script>
				$(document).ready(function(){	
					// SYSTEM MESSAGES
					$(".message").click(function () {
				      $(this).fadeOut();
				    });
				});
			</script>