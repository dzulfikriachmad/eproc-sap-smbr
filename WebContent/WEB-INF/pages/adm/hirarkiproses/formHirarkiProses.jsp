<%@ include file="/include/definitions.jsp" %>
	<script type="text/javascript">
		$(document).ready(function(){
			// FORM VALIDATION
			$('.chzn').chosen();
			$("#formeproc").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				}
			});
			
			$("#formeproc").ajaxForm({
		        beforeSubmit: function() {
		           loadingScreen();
		        },
		        success: function(data) {
		            loadPage("<s:url action="ajax/adm/listHirarkiProses"/>","#result");
		            jQuery.facebox.close();
		        }
		    });
		});
		
		
	</script>
	
<div class='fbox_header'>Tambah Data Hirarki Proses</div>
    <div class='fbox_container'>
    <s:form name="formeproc" id="formeproc">
        <div class='fbox_content'>
        	<div id="cek"></div>
            
            	<s:hidden name="hirarki.id"></s:hidden>
            	<s:hidden name="hirarki.createdDate"></s:hidden>
            	<s:hidden name="hirarki.createdBy"></s:hidden>
            	<s:hidden name="hirarki.updatedDate"></s:hidden>
            	<s:hidden name="hirarki.updatedBy"></s:hidden>
            	<table style="min-width: 600px; margin: 0px 10px;" class="form formStyle">
            		<tr>
            			<td><s:label value="Satuan Kerja (*)"></s:label> </td>
            			<td colspan="3">
            				<s:select list="listDept" listKey="id" listValue="deptName" name="hirarki.admDept.id"></s:select>
            			</td>
            		</tr>
            		<tr>
            			<td><s:label value="Proses (*)"></s:label> </td>
            			<td colspan="3">
            				<s:select list="listProses" listKey="id" listValue="prosesName" name="hirarki.admProses.id"></s:select>
            			</td>
            		</tr>        
            		<tr>
            			<td><s:label value="Posisi (*)"></s:label> </td>
            			<td colspan="3">
            				<s:select list="listRole" listKey="id" listValue="roleName" name="hirarki.admRoleByCurrentPos.id"></s:select>
            			</td>
            		</tr>
            		<tr>
            			<td><s:label value="Posisi Selanjutnya (*)"></s:label> </td>
            			<td colspan="3">
            				<s:select list="listRole" listKey="id" listValue="roleName" name="hirarki.admRoleByNextPos.id" headerKey="0" headerValue=""></s:select>
            			</td>
            		</tr>
            		<tr>
            			<td><s:label value="Batas Minimal (*)"></s:label> </td>
            			<td colspan="3"><s:textfield name="hirarki.minimalValue" cssClass="number required" /> </td>
            		</tr>
            		<tr>
            			<td><s:label value="Tingkat (*)"></s:label> </td>
            			<td colspan="3"><s:textfield name="hirarki.tingkat" readonly="true" /> </td>
            		</tr>
            		<tr>
            			<td><s:label value="URL (*)"></s:label> </td>
            			<td colspan="3">
            				<s:select list="listMenu" listKey="menuUrl" listValue="menuLabel" cssClass="chzn" name="hirarki.url"></s:select>
            		</tr>
            		<tr>
            			<td><s:label value="Keterangan (*)"></s:label> </td>
            			<td colspan="3"><s:textfield name="hirarki.keterangan"  /> </td>
            		</tr>
            		
            	</table>
            
        </div>
        <div class='fbox_footer'>
            <s:submit key="save" cssClass="uibutton confirm"></s:submit>
            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()'>Batalkan</a>
        </div>
        </s:form>
    </div>
    