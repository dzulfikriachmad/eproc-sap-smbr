<%@ include file="/include/definitions.jsp"%>

<!DOCTYPE html>
<html lang="id">
<head>
<title>Daftar Pengadaan</title>
</head>
<body>
	<!-- BreadCrumbs -->
	<!-- End of BreadCrumbs -->
	<script type="text/javascript">
	$("#searchButton").live("click",function(){
		$.ajax({
	        url: "<s:url action="ajax/adm/searchPengadaan" />", 
	        data: $(document.formSearchPr.elements).serialize(),
	        beforeSend: function(){
	        	$("#hasil").html('<img src="${contextPath}/assets/images/loading/loading-circle.gif"/>');
	        },
	        success: function(response){
	                $("#hasil").html(response);
	            },
	        type: "post", 
	        dataType: "html"
	    }); 
	});

   $(".ubahTanggal").live("click",function(){
	   var idPrcHeader=$(this).attr("id").replace("edit_","");
	   $.ajax({
	        url: "<s:url action="ajax/adm/editTanggalPrc" />", 
	        data: {"prcMainHeader.ppmId":idPrcHeader},
	        beforeSend: function(){
	        	jQuery.facebox('<div style="padding: 17px; font-size: 36px;">Loading....</div>');
	        },
	        success: function(response){
	                jQuery.facebox(response);
	            },
	        type: "post", 
	        dataType: "html"
	    }); 
   });
   $("#searchNomoPr").live("keypress",function(e){
	   if(e.keyCode == 13){
		   $("#searchButton").click();
		   return false;
	   }
   });
	</script>

	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red">
					<s:actionerror />
				</div>
			</s:if>
			<s:if test="hasActionMessages()">
				<div class="message green">
					<s:actionmessage />
				</div>
			</s:if>

			<div class="box">
				<div class="title">
					Daftar Pengadaan
					<span class="hide"></span>
				</div>
				<div class="content">
					<s:form id="formSearchPr">
					<table style="margin-bottom: 30px;" class="form formStyle1">
						<tr>
							<td width="70px">Nomor PR</td>
							<td>:&nbsp;</td>
							<td>
								<s:textfield name="prcMainHeader.ppmNomorPr" id="searchNomoPr"></s:textfield>
								<s:hidden name="prcMainHeader.ppmProsesLevel" value="-1"></s:hidden>
							</td>
						</tr>
						<tr>
							<td></td>
							<td>&nbsp;</td>
							<td>
								<input type="button" value="Cari" class="uibutton" id="searchButton"/> 
							</td>
						</tr>
					</table>
					</s:form>
					<div id="hasil"></div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>