<%@ include file="/include/definitions.jsp" %>
	<script type="text/javascript">
		$(document).ready(function(){
			$.datepicker.setDefaults($.datepicker.regional['in_ID']);
			$("#opening").datetimepicker({
				changeYear: true,
				changeMonth: true
			});
			$("#closing").datetimepicker({
				changeYear: true,
				changeMonth: true
			});
			$("#quotation").datetimepicker({
				changeYear: true,
				changeMonth: true
			});
			
			// FORM VALIDATION
			$("#formTmpVndEquip").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler: function() {
					$.ajax({
				        url: "<s:url action="ajax/adm/saveTanggalPengadaan" />", 
				        data: $(document.formTmpVndEquip.elements).serialize(),
				        beforeSend: function(){
				        	jQuery.facebox('<div style="padding: 17px; font-size: 36px;">Loading....</div>');
				        },
				        success: function(response){
				                jQuery.facebox(response);
				            },
				        type: "post", 
				        dataType: "html"
				    }); 
				}
			});
		});
	</script>
	
<div class='fbox_header'>Ubah tanggal pengadaan</div>
    <div class='fbox_container'>
    	<s:if test="hasActionErrors()">
			<div class="message red"><s:actionerror/></div>
		</s:if>
		<s:if test="hasActionMessages()">
		    <div class="message green"><s:actionmessage/></div>
		    <script>
		    $(function(){
		    	setTimeout(function(){
				    jQuery.facebox.close();
	    		},1000);
	    	});
		    </script>
		</s:if>
    	<s:form name="formTmpVndEquip" id="formTmpVndEquip">
    	<s:hidden name="prcMainPreparation.ppmId" ></s:hidden>
        <s:hidden name="prcMainPreparation.prcMainHeader.ppmId"></s:hidden>
        <div class='fbox_content'>
        	<div id="cek"></div>
            
            	<table style="min-width: 600px; margin: 0px 10px;" class="form formStyle">
            		<tr>
            			<td >Pembukaan Pendaftaran (*)</td>
            			<td colspan="3">
            			<s:textfield name="prcMainPreparation.registrastionOpening" readonly="true" id="opening" cssClass="required" />
            			<label class="error" id="openingAlert" style="display: none;"></label>
            			</td>
            		</tr>            		
            		<tr>
            			<td>Penutupan Pendaftaran (*)</td>
            			<td colspan="3"><s:textfield id="closing" readonly="true" name="prcMainPreparation.registrastionClosing" cssClass="required" />
            			<label class="error" id="closingAlert" style="display: none;"></label>
            			</td>
            		</tr>  
            		<tr>
            			<td>Pembukaan Penawaran (*) </td>
            			<td colspan="3">
            			<s:textfield id="quotation" readonly="true" name="prcMainPreparation.quotationOpening" cssClass="required" />
            			<label class="error" id="quotationAlert" style="display: none;"></label> 
            			</td>
            		</tr>
            		<tr>
            			<td>Aanwijzing (*)</td>
            			<td colspan="3">
            			<s:textfield id="quotation" readonly="true" name="prcMainPreparation.aanwijzingDate"/>
            			<label class="error" id="quotationAlert" style="display: none;"></label> 
            			</td>
            		</tr>
            		<c:if test="prcMainHeader.prcMethod.id==6">
            		<tr>
            			<td>Pembukaan Penawaran Harga (Lelang 2 Tahap)</td>
            			<td colspan="3">
            			<s:textfield id="quotation" readonly="true" name="prcMainPreparation.quotationOpening2" cssClass="required" />
            			<label class="error" id="quotationAlert" style="display: none;"></label> 
            			</td>
            		</tr>
            		</c:if>
            	</table>
            
        </div>
        <div class='fbox_footer'>
            <s:submit key="save" name="ubah" id="submitPasword" cssClass="uibutton confirm" ></s:submit>
            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()'><s:label key="cancel"></s:label></a>
        </div>
        </s:form>
    </div>