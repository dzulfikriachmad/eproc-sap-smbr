			<%@ include file="/include/definitions.jsp" %>
			<script type="text/javascript">
				   $(document).ready(function() {
					// DATATABLE
					 $('table.doc').dataTable({
				        "bInfo": false,
				        "iDisplayLength": 10,
				        "aLengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
				        "sPaginationType": "full_numbers",
				        "bPaginate": true,
				        "sDom": '<f>t<pl>'
				        });
					});
			</script>
			<center>
<display:table name="${listPrcMainHeader }" id="s" style="width: 100%;" class="doc style1" requestURI="ajax/adm/searchPengadaan.jwebs" >
	<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
	<display:column title="Nomor Pr" total="true" style="width: 200px;">
			${s.ppmNomorPr }
	</display:column>
	<display:column title="Nomor SPPH" total="true" style="width: 200px;">
		${s.ppmNomorRfq }
	</display:column>
	<display:column title="Nama Permintaan" style="width: 150px; "
		class="center">${s.ppmSubject }</display:column>
	<display:column title="User Skr" style="width: 150px;" class="center">${s.admUserByPpmCurrentUser.completeName }</display:column>
	<display:column title="Satker" style="width: 100px;" class="center">${s.admDept.deptName }</display:column>
	<display:column title="Tanggal" class="center" style="width: 200px;">
		<fmt:formatDate value="${s.createdDate }"
			pattern="HH:mm:ss, dd MMM yyyy" />
	</display:column>
	<display:column title="Aksi" class="center" style="width: 200px;">
		<a id="edit_${s.ppmId }" class="uibutton ubahTanggal">Ubah</a>
	</display:column>
</display:table>
			</center>