			<%@ include file="/include/definitions.jsp" %>
			<script type="text/javascript">
				   $(document).ready(function() {
					// DATATABLE
					 $('table.doc').dataTable({
				        "bInfo": false,
				        "iDisplayLength": 10,
				        "aLengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
				        "sPaginationType": "full_numbers",
				        "bPaginate": true,
				        "sDom": '<f>t<pl>'
				        });
					});
			</script>
			<center>
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>		
						
			<display:table name="${listIssues }" id="s" requestURI="ajax/adm/listIssues.jwebs" excludedParams="*" style="width: 100%;" class="doc style1">
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column title="Pembuat" style="width: 250px;" class="center">${s.pembuat}</display:column>
				<display:column title="Masalah">${s.issues}</display:column>
				<display:column title="Status">${s.status}</display:column>
				<display:column title="Tanggal Di Buat">
					<fmt:formatDate value="${s.createdDate }" pattern="dd.MM.yyyy HH:mm"/>
				</display:column>
				<display:column title="Resolver">${s.resolver}</display:column>
				<display:column title="Penanganan">${s.keterangan}</display:column>
				<display:column title="Tanggal Di Selesaikan">
					<fmt:formatDate value="${s.updatedDate }" pattern="dd.MM.yyyy HH:mm"/>
				</display:column>
				<display:column title="Aksi" media="html" style="width: 80px;" class="center">
					<div class="uibutton-group">
						<s:url var="set" action="ajax/adm/crudIssuesUpdate" />
						<a <c:if test="${update !=1 }"> style="visibility:hidden"</c:if> title="Ubah Issues" class="uibutton icon edit" href='javascript:void(0)' onclick='load_into_box("${set}", "xCommand=edit&issues.id=${s.id}");'>&nbsp;</a>
					</div>
				</display:column>
			</display:table>
			</center>
			
			<script>
				$(document).ready(function(){	
					// SYSTEM MESSAGES
					$(".message").click(function () {
				      $(this).fadeOut();
				    });
				});
			</script>