<%@ include file="/include/definitions.jsp" %>
	<script type="text/javascript">
		$(document).ready(function(){
			// FORM VALIDATION
			$("#formeproc").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
			});
			
			$("#formeproc").ajaxForm({
		        beforeSubmit: function() {
		            //$('#notif').html('<img src="${contextPath}/assets/images/loading/loading-circle.gif"/>');
		        },
		        success: function(data) {
		            loadPage("<s:url action="ajax/adm/listIssues"/>","#result");
		            jQuery.facebox.close();
		        }
		    });
		});
		
		
	</script>
	
<div class='fbox_header'>Tambah Issues</div>
    <div class='fbox_container'>
    <s:form name="formeproc" id="formeproc">
        <div class='fbox_content'>
        	<div id="cek"></div>
            	<s:hidden name="issues.id"></s:hidden>
            	<s:hidden name="issues.createdDate"></s:hidden>
            	<s:hidden name="issues.createdBy"></s:hidden>
            	<s:hidden name="issues.updatedDate"></s:hidden>
            	<s:hidden name="issues.updatedBy"></s:hidden>
            	<s:hidden name="issues.pembuat"></s:hidden>
            	<s:hidden name="issues.status" value="Close"></s:hidden>
            	<table style="min-width: 600px; margin: 0px 10px;" class="form formStyle">       
            		<tr>
            			<td><s:label value="Resolver (*)"></s:label> </td>
            			<td colspan="3"><s:textfield name="issues.resolver" cssClass="required" readonly="true"/> </td>
            		</tr>
            		<tr>
            			<td><s:label value="Masalah Yang di Hadapi (*)"></s:label> </td>
            			<td colspan="3">
            				<s:textarea name="issues.issues" rows="8" readonly="true"></s:textarea>
            			</td>
            		</tr>
            		<tr>
            			<td><s:label value="Penyelesaian (*)"></s:label> </td>
            			<td colspan="3">
            				<s:textarea name="issues.keterangan" rows="8" cssClass="required"></s:textarea>
            			</td>
            		</tr>
            	</table>
            
        </div>
        <div class='fbox_footer'>
            <s:submit key="save" cssClass="uibutton confirm"></s:submit>
            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()'>Tutup</a>
        </div>
        </s:form>
    </div>
    