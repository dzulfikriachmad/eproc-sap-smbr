<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	<title>e-Proc System | Login</title>

	<!-- Slider -->
	<link rel="stylesheet" href="${contextPath}/assets/images/slider-themes/default/default.css" type="text/css" media="screen" />
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.nivo.slider.pack.js"></script>
	<link rel="stylesheet" href="${contextPath}/assets/css/slider.css" type="text/css" media="screen" />
	<script type="text/javascript">
	    $(window).load(function() {
	        $('#slider').nivoSlider();
	    });
	</script>
</head>
<body>
<!-- Main Menu -->
<%-- <nav>
	<ul>
		<li><a href="${contextPath }/">Pendaftaran Mitra Kerja</a></li>
		<li><a href="#">Pengumuman</a></li>
		<li><a href="#">Lupa Password?</a></li>
	</ul>
</nav> --%>
<!-- End of Main Menu -->

<div id="wrapper">
	<div id="container">			
		<div id="right">
			<div class="section">
				<div class="slider-wrapper theme-default">
		            <div id="slider" class="nivoSlider">
		            	<!--  
		            	<img src="${contextPath}/assets/images/slider/ep4.jpg" />
		            	-->
		                <%-- <img src="${contextPath}/assets/images/slider/4.png" alt="" /> --%>
		                <!--  
		                <img src="${contextPath}/assets/images/slider/ep3.jpg" alt="" />
		                -->
		            </div>
		        </div>
			</div>
			
			<div class="section">
 
				<div class="box" style="width: 900px; margin: 0px auto 30px auto;">
					<div class="title center" style="text-transform: none; font-size: 21px; padding-top: 2px; padding-bottom: 7px">
						Login - Procurement
						<span class="hide"></span>
					</div>
					<div class="content">
						<!-- Alert -->
						<s:if test="hasActionErrors()">
							<div class="message red"><s:actionerror/></div>
						</s:if>
						<s:if test="hasActionMessages()">
						    <div class="message orange"><s:actionmessage/></div>
						</s:if>
						
						<s:form action="login" method="post">
							<s:hidden name="lastUrl"></s:hidden>
							<table style="width: 437px; margin: 0 auto;"> 
								<tr>
									<td width="150px">Nama Pengguna</td>
									<td><s:textfield name="admUser.username" placeholder="Masukan Username Anda di sini"></s:textfield></td>
								</tr>
								<tr>
									<td width="90px">Kata kunci</td>
									<td><s:password name="admUser.password" placeholder="***********"></s:password></td>
								</tr>
								<tr style="line-height: 3"> 
									<td width="90px"></td>
									<td>
										<s:submit name="loginInternal" value="Login" cssClass="uibutton search" onclick="loadingScreen();"></s:submit>
										<%-- <s:a href="#" cssClass="uibutton icon secure"><s:text name="login.lostPassword"></s:text> </s:a> --%> 
									</td>
								</tr>
																
							</table>
						</s:form>
			
					</div> 
				</div>
			</div>
			
		</div>		
		
		
		<div class="clear"></div>
	</div>
</div> 

</body>
</html>