<%@ include file="/include/definitions.jsp" %>
	<script type="text/javascript">
		$(document).ready(function(){
			$.datepicker.setDefaults($.datepicker.regional['in_ID']);
			$("#tanggal1").datetimepicker({
				changeYear: true,
				changeMonth: true
			});
			$("#tanggal2").datetimepicker({
				changeYear: true,
				changeMonth: true
			});
			// FORM VALIDATION
			$("#formeproc").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
			});
			
			$("#formeproc").ajaxForm({
		        beforeSubmit: function() {
		            //$('#notif').html('<img src="${contextPath}/assets/images/loading/loading-circle.gif"/>');
		        },
		        success: function(data) {
		            loadPage("<s:url action="ajax/adm/listJadwal"/>","#result");
		            jQuery.facebox.close();
		        }
		    });
		});
		
		
	</script>
	
<div class='fbox_header'>Tambah Data Jadwal</div>
    <div class='fbox_container'>
    <s:form name="formeproc" id="formeproc">
        <div class='fbox_content'>
        	<div id="cek"></div>
            
            	<s:hidden name="jadwal.id"></s:hidden>
            	<s:hidden name="jadwal.createdDate"></s:hidden>
            	<s:hidden name="jadwal.createdBy"></s:hidden>
            	<s:hidden name="jadwal.updatedDate"></s:hidden>
            	<s:hidden name="jadwal.updatedBy"></s:hidden>
            	<table style="min-width: 600px; margin: 0px 10px;" class="form formStyle">       
            		<tr>
            			<td><s:label value="Nomor Jadwal (*)"></s:label> </td>
            			<td colspan="3"><s:textfield name="jadwal.nomorJadwal" cssClass="required" /> </td>
            		</tr>
            		<tr>
            			<td><s:label value="Nama Jadwal (*)"></s:label> </td>
            			<td colspan="3"><s:textfield name="jadwal.namaJadwal" cssClass="required" /> </td>
            		</tr>
            		<tr>
            			<td><s:label value="Deskripsi Jadwal (*)"></s:label> </td>
            			<td colspan="3"><s:textfield name="jadwal.deskripsiJadwal" cssClass="required" /> </td>
            		</tr>
            		<tr>
            			<td><s:label value="Tanggal Pembukaan (*)"></s:label> </td>
            			<td colspan="3"><s:textfield name="jadwal.tanggalPembukaan" cssClass="required" id="tanggal1"/> </td>
            		</tr>
            		<tr>
            			<td><s:label value="Tanggal Penutupan (*)"></s:label> </td>
            			<td colspan="3"><s:textfield name="jadwal.tanggalPenutupan" cssClass="required" id="tanggal2"/> </td>
            		</tr>
            	</table>
            
        </div>
        <div class='fbox_footer'>
            <s:submit key="save" cssClass="uibutton confirm"></s:submit>
            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()'>Batalkan</a>
        </div>
        </s:form>
    </div>
    