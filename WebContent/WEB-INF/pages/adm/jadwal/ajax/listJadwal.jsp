			<%@ include file="/include/definitions.jsp" %>
			<script type="text/javascript">
				   $(document).ready(function() {
					// DATATABLE
					 $('table.doc').dataTable({
				        "bInfo": false,
				        "iDisplayLength": 10,
				        "aLengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
				        "sPaginationType": "full_numbers",
				        "bPaginate": true,
				        "sDom": '<f>t<pl>'
				        });
					});
			</script>
			<center>
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>		
						
			<display:table name="${listJadwal }" id="s" requestURI="ajax/adm/listProses.jwebs" excludedParams="*" style="width: 100%;" class="doc style1">
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column title="Kantor" style="width: 100px; text-align:left " class="center">${s.district.districtName}</display:column>
				<display:column title="Nama Jadwal" style="width: 100px; text-align:left " class="center">${s.namaJadwal}</display:column>
				<display:column title="Deskripsi" style="width: 200px; text-align:left " class="center">${s.deskripsiJadwal}</display:column>
				<display:column title="Tanggal Pembukaan" style="width: 200px; text-align:center " class="center"><fmt:formatDate value="${s.tanggalPembukaan }" pattern="dd.MM.yyyy HH:mm"/> </display:column>
				<display:column title="Tanggal Penutupan" style="width: 200px; text-align:center " class="center"><fmt:formatDate value="${s.tanggalPenutupan }" pattern="dd.MM.yyyy HH:mm"/></display:column>
				<display:column title="Aksi" media="html" style="width: 80px;" class="center">
					<div class="uibutton-group">
						<s:url var="set" action="ajax/adm/crudJadwal" />
						<a <c:if test="${update !=1 }"> style="visibility:hidden"</c:if> title="Ubah data Jadwal" class="uibutton icon edit" href='javascript:void(0)' onclick='load_into_box("${set}", "xCommand=edit&jadwal.id=${s.id}");'>&nbsp;</a>
						<a <c:if test="${delete !=1 }"> style="visibility:hidden"</c:if> title="Hapus data Jadwal" class="uibutton" href='javascript:void(0)' onclick='confirmPopUp("deleteObj(\"${set}\", \"xCommand=delete&jadwal.id=${s.id}\", \"#result\")", "Hapus Data", "Yakin ingin menghapus?", "Hapus", "Batal");'>X</a>
					</div>
				</display:column>
			</display:table>
			</center>
			
			<script>
				$(document).ready(function(){	
					// SYSTEM MESSAGES
					$(".message").click(function () {
				      $(this).fadeOut();
				    });
				});
			</script>