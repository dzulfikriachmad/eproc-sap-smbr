<%@ include file="/include/definitions.jsp" %>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.chzn').chosen();
			// FORM VALIDATION
			$("#formeproc").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
			});
			
			$("#formeproc").ajaxForm({
		        beforeSubmit: function() {
		            //$('#notif').html('<img src="${contextPath}/assets/images/loading/loading-circle.gif"/>');
		        },
		        success: function(data) {
		            loadPage("<s:url action="ajax/adm/listSatker"/>","#result");
		            jQuery.facebox.close();
		        }
		    });
		});
		
		
	</script>
	
<div class='fbox_header'>Tambah Data Satker</div>
    <div class='fbox_container'>
    <s:form name="formeproc" id="formeproc">
        <div class='fbox_content'>
        	<div id="cek"></div>
            
            	<s:hidden name="dept.id"></s:hidden>
            	<s:hidden name="dept.createdDate"></s:hidden>
            	<s:hidden name="dept.createdBy"></s:hidden>
            	<s:hidden name="dept.updatedDate"></s:hidden>
            	<s:hidden name="dept.updatedBy"></s:hidden>
            	<table style="min-width: 600px; margin: 0px 10px;" class="form formStyle">        
            		<tr>
            			<td><s:label value="Kantor"></s:label> </td>
            			<td colspan="3">
            				<s:select list="listDistrict" listKey="id" listValue="districtName" name="dept.admDistrict.id"></s:select>
            				
            			</td>
            		</tr>
            		<tr>
            			<td><s:label value="Direktur Terkait"></s:label> </td>
            			<td colspan="3">
            				<s:select list="listRole" listKey="id" listValue="roleName" name="dept.direktur.id" cssClass="chzn" cssStyle="width:400px"></s:select>
            			</td>
            		</tr>
            		<tr>
            			<td><s:label value="Kode Satuan Kerja (*)"></s:label> </td>
            			<td colspan="3"><s:textfield name="dept.deptCode" cssClass="required" /> </td>
            		</tr>		
            		<tr>
            			<td><s:label value="Nama Satuan Kerja (*)"></s:label> </td>
            			<td colspan="3"><s:textfield name="dept.deptName" cssClass="required" /> </td>
            		</tr> 
            		
            		<tr>
            			<td><s:label value="Koordinator Anggaran"></s:label> </td>
            			<td colspan="3">
            				<s:select list="listUser" listKey="id" listValue="completeName" name="dept.kordang.id" cssClass="chzn" cssStyle="width:400px"></s:select>
            			</td>
            		</tr> 
            		
            	</table>
            
        </div>
        <div class='fbox_footer'>
            <s:submit key="save" cssClass="uibutton confirm" ></s:submit>
            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()'>Batalkan</a>
        </div>
        </s:form>
    </div>
    