			<%@ include file="/include/definitions.jsp" %>
			<script type="text/javascript">
				   $(document).ready(function() {
					// DATATABLE
					 $('table.doc').dataTable({
				        "bInfo": false,
				        "iDisplayLength": 10,
				        "aLengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
				        "sPaginationType": "full_numbers",
				        "bPaginate": true,
				        "sDom": '<f>t<pl>'
				        });
					});
			</script>
			<center>
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>		
						
			<display:table name="${listCostCenter }" id="s" requestURI="ajax/adm/listCostCenter.jwebs" excludedParams="*" style="width: 100%;" class="doc style1">
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column title="Satker" style="width: 100px; text-align:left " class="center">${s.admDept.deptName}</display:column>
				<display:column title="Cost Center" style="width: 200px; text-align:left " class="center">${s.id.costCenterCode}</display:column>
				<display:column title="Deskripsi" style="width: 200px; text-align:left " class="center">${s.costCenterName}</display:column>
				<display:column title="Aksi" media="html" style="width: 80px;" class="center">
					<div class="uibutton-group">
						<s:url var="set" action="ajax/adm/crudCostCenter" />
						<a <c:if test="${update !=1 }"> style="visibility:hidden"</c:if> title="Ubah data CostCenter" class="uibutton icon edit" href='javascript:void(0)' onclick='load_into_box("${set}", "xCommand=edit&costCenter.id.costCenterCode=${s.id.costCenterCode}&costCenter.id.deptId=${s.id.deptId }");'>&nbsp;</a>
						<a <c:if test="${delete !=1 }"> style="visibility:hidden"</c:if> title="Hapus data CostCenter" class="uibutton" href='javascript:void(0)' onclick='confirmPopUp("deleteObj(\"${set}\", \"xCommand=delete&costCenter.id.costCenterCode=${s.id.costCenterCode}&costCenter.id.deptId=${s.id.deptId }\", \"#result\")", "Hapus Data", "Yakin ingin menghapus?", "Hapus", "Batal");'>X</a>
					</div>
				</display:column>
			</display:table>
			</center>
			
			<script>
				$(document).ready(function(){	
					// SYSTEM MESSAGES
					$(".message").click(function () {
				      $(this).fadeOut();
				    });
				});
			</script>