<%@ include file="/include/definitions.jsp" %>
<!-- tree view --> 
<script type="text/javascript" src="${contextPath}/assets/js/tree/jquery.cookie.js"></script>
<script type="text/javascript" src="${contextPath}/assets/js/tree/jquery.treeview.js"></script>
<script type="text/javascript" src="${contextPath}/assets/js/tree/inittree.js"></script>
<link href="${contextPath}/assets/js/tree/jquery.treeview.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript">
		$(document).ready(function(){
			// FORM VALIDATION
			$("#formeproc").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
			});
			
			$("#formeproc").ajaxForm({
		        beforeSubmit: function() {
		        	loadingScreen();
		        },
		        success: function(data) {
		            loadPage("<s:url action="ajax/adm/listRole"/>","#result");
		            jQuery.facebox.close();
		        }
		    });
		});
		
		
		function cekValue(obj) {
			if(obj.checked){
				obj.value=1;
			}else{
				obj.value=0;	
			}
			
		}
		
	</script>
	
<div class='fbox_header'>Tambah Data Role Menu</div>
    <div class='fbox_container'>
    <s:form name="formeproc" id="formeproc">
        <div class='fbox_content'>
        	<div id="cek"></div>
            
            	<s:hidden name="role.id"></s:hidden>
            	<table style="min-width: 800px; margin: 0px 10px;" class="form formStyle">       
            		<tr>
            			<td><s:label value="Nama Role (*)"></s:label> </td>
            			<td colspan="3"><s:textfield name="role.RoleName" cssClass="required" readonly="true"/> </td>
            		</tr>
            	</table>
            	
	<div style="text-align: left">
			<c:set value="0" var="n"></c:set>
			<ul id="menu" class="filetree">
				<s:iterator value="%{listRoleMenu}" var="i">
					<c:if test="${i.admMenu.menuParent==null }">
						<li><span class="folder">
							<c:set var="n" value="${n+1 }"></c:set>
							${i.admMenu.menuLabel }
							<input type="hidden" name="listRoleMenu[${n }].id.roleId" value="${i.admRole.id }"/>
							<input type="hidden" name="listRoleMenu[${n }].id.menuId" value="${i.admMenu.id }"/>
							<input type="hidden" name="listRoleMenu[${n }].admMenu.id" value="${i.admMenu.id }"/>
							<input type="hidden" name="listRoleMenu[${n }].admRole.id" value="${i.admRole.id }"/>
							&nbsp;
							C
							<input type="checkbox" name="listRoleMenu[${n }].create" value="${i.create }" onclick="cekValue(this);" <c:if test="${i.create==1 }"> checked="checked" </c:if>/>
							&nbsp;
							R
							<input type="checkbox" name="listRoleMenu[${n }].retrieve" value="${i.retrieve }" onclick="cekValue(this);" <c:if test="${i.retrieve==1 }"> checked="checked" </c:if>/>
							&nbsp;
							D
							<input type="checkbox" name="listRoleMenu[${n }].delete" value="${i.delete }" onclick="cekValue(this);" <c:if test="${i.delete==1 }"> checked="checked" </c:if>/>
							&nbsp;
							U
							<input type="checkbox" name="listRoleMenu[${n }].update" value="${i.update }" onclick="cekValue(this);" <c:if test="${i.update==1 }"> checked="checked" </c:if>/>
							</span>
							
							<!-- second level -->
							<ul>
							<s:iterator value="%{listRoleMenu}" var="j">
								<c:if test="${j.admMenu.menuParent.id == i.admMenu.id}">
									<li>	
										<c:set var="n" value="${n+1 }"></c:set>						
										<c:choose>
											<c:when test="${j.admMenu.admMenus.size()>0 }">
												<span class="folder"> 
												${j.admMenu.menuLabel } &nbsp; 
												<input type="hidden" name="listRoleMenu[${n }].id.roleId" value="${j.admRole.id }"/>
												<input type="hidden" name="listRoleMenu[${n }].id.menuId" value="${j.admMenu.id }"/>
												<input type="hidden" name="listRoleMenu[${n }].admMenu.id" value="${j.admMenu.id }"/>
												<input type="hidden" name="listRoleMenu[${n }].admRole.id" value="${j.admRole.id }"/>
												&nbsp;
												C
												<input type="checkbox" name="listRoleMenu[${n }].create" value="${j.create }" onclick="cekValue(this);" <c:if test="${j.create==1 }"> checked="checked" </c:if>/>
												&nbsp;
												R
												<input type="checkbox" name="listRoleMenu[${n }].retrieve" value="${j.retrieve }" onclick="cekValue(this);" <c:if test="${j.retrieve==1 }"> checked="checked" </c:if>/>
												&nbsp;
												D
												<input type="checkbox" name="listRoleMenu[${n }].delete" value="${j.delete }" onclick="cekValue(this);" <c:if test="${j.delete==1 }"> checked="checked" </c:if>/>
												&nbsp;
												U
												<input type="checkbox" name="listRoleMenu[${n }].update" value="${j.update }" onclick="cekValue(this);" <c:if test="${j.update==1 }"> checked="checked" </c:if>/>
												</span>
											</c:when>
											<c:otherwise>
												${j.admMenu.menuLabel } &nbsp; 
												<input type="hidden" name="listRoleMenu[${n }].id.roleId" value="${j.admRole.id }"/>
												<input type="hidden" name="listRoleMenu[${n }].id.menuId" value="${j.admMenu.id }"/>
												<input type="hidden" name="listRoleMenu[${n }].admMenu.id" value="${j.admMenu.id }"/>
												<input type="hidden" name="listRoleMenu[${n }].admRole.id" value="${j.admRole.id }"/>
												&nbsp;
												C
												<input type="checkbox" name="listRoleMenu[${n }].create" value="${j.create }" onclick="cekValue(this);" <c:if test="${j.create==1 }"> checked="checked" </c:if>/>
												&nbsp;
												R
												<input type="checkbox" name="listRoleMenu[${n }].retrieve" value="${j.retrieve }" onclick="cekValue(this);" <c:if test="${j.retrieve==1 }"> checked="checked" </c:if>/>
												&nbsp;
												D
												<input type="checkbox" name="listRoleMenu[${n }].delete" value="${j.delete }" onclick="cekValue(this);" <c:if test="${j.delete==1 }"> checked="checked" </c:if>/>
												&nbsp;
												U
												<input type="checkbox" name="listRoleMenu[${n }].update" value="${j.update }" onclick="cekValue(this);" <c:if test="${j.update==1 }"> checked="checked" </c:if>/>
											</c:otherwise>
										</c:choose>			
										
										<!-- third level -->
										<ul>
											<s:iterator value="%{listRoleMenu}" var="k" >
												<c:if test="${k.admMenu.menuParent.id == j.admMenu.id and j.admMenu.id ne i.admMenu.id}">
													<li>${k.admMenu.menuLabel } &nbsp;
														<c:set var="n" value="${n+1 }"></c:set>
														<input type="hidden" name="listRoleMenu[${n }].id.roleId" value="${k.admRole.id }"/>
														<input type="hidden" name="listRoleMenu[${n }].id.menuId" value="${k.admMenu.id }"/>
														<input type="hidden" name="listRoleMenu[${n }].admMenu.id" value="${k.admMenu.id }"/>
														<input type="hidden" name="listRoleMenu[${n }].admRole.id" value="${k.admRole.id }"/>
														&nbsp;
														C
														<input type="checkbox" name="listRoleMenu[${n }].create" value="${k.create }" onclick="cekValue(this);" <c:if test="${k.create==1 }"> checked="checked" </c:if>/>
														&nbsp;
														R
														<input type="checkbox" name="listRoleMenu[${n }].retrieve" value="${k.retrieve }" onclick="cekValue(this);" <c:if test="${k.retrieve==1 }"> checked="checked" </c:if>/>
														&nbsp;
														D
														<input type="checkbox" name="listRoleMenu[${n }].delete" value="${k.delete }" onclick="cekValue(this);" <c:if test="${k.delete==1 }"> checked="checked" </c:if>/>
														&nbsp;
														U
														<input type="checkbox" name="listRoleMenu[${n }].update" value="${k.update }" onclick="cekValue(this);" <c:if test="${k.update==1 }"> checked="checked" </c:if>/>
													</li>	
												</c:if>
											</s:iterator>
										</ul>
									</li>	
								</c:if>
							</s:iterator>
							</ul>
						</li>
					</c:if>
				</s:iterator>
			</ul>
	</div>
            
        </div>
        <div class='fbox_footer'>
            <s:submit name="rolemenu" value="Save" cssClass="uibutton confirm"></s:submit>
            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()'>Batalkan</a>
        </div>
        </s:form>
    </div>
    