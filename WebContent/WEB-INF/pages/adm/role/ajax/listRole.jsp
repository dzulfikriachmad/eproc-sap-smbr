			<%@ include file="/include/definitions.jsp" %>
			<script type="text/javascript">
				   $(document).ready(function() {
					// DATATABLE
					 $('table.doc').dataTable({
				        "bInfo": false,
				        "iDisplayLength": 10,
				        "aLengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
				        "sPaginationType": "full_numbers",
				        "bPaginate": true,
				        "sDom": '<f>t<pl>'
				        });
					});
			</script>
			<center>
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>		
						
			<display:table name="${listRole }" id="s" requestURI="ajax/adm/listRole.jwebs" excludedParams="*" style="width: 100%;" class="doc style1">
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column title="Nama Role" style="width: 250px; text-align:left " class="center">${s.roleName}</display:column>
				<display:column title="Aksi" media="html" style="width: 80px;" class="center">
					<div class="uibutton-group">
						<s:url var="set" action="ajax/adm/crudRole" />
						<a <c:if test="${update !=1 }"> style="visibility:hidden"</c:if> title="Ubah data Role" class="uibutton icon edit" href='javascript:void(0)' onclick='load_into_box("${set}", "xCommand=edit&role.id=${s.id}");'>&nbsp;</a>
						<c:if test="${s.roleName ne 'PANITIA DRT' }">
							<a <c:if test="${delete !=1 }"> style="visibility:hidden"</c:if> title="Hapus data Role" class="uibutton" href='javascript:void(0)' onclick='confirmPopUp("deleteObj(\"${set}\", \"xCommand=delete&role.id=${s.id}\", \"#result\")", "Hapus Data", "Yakin ingin menghapus?", "Hapus", "Batal");'>X</a>
						</c:if>
						<a <c:if test="${update !=1 }"> style="visibility:hidden"</c:if> title="Ubah Menu Role" class="uibutton icon add" href='javascript:void(0)' onclick='load_into_box("${set}", "xCommand=child&role.id=${s.id}");'>&nbsp;</a>
					</div>
				</display:column>
			</display:table>
			</center>
			
			<script>
				$(document).ready(function(){	
					// SYSTEM MESSAGES
					$(".message").click(function () {
				      $(this).fadeOut();
				    });
				});
			</script>