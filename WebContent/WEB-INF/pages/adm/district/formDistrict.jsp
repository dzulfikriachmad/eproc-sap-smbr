<%@ include file="/include/definitions.jsp" %>
	<script type="text/javascript">
		$(document).ready(function(){
			// FORM VALIDATION
			$('.chzn').chosen();
			$("#formeproc").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
			});
			
			$("#formeproc").ajaxForm({
		        beforeSubmit: function() {
		            //$('#notif').html('<img src="${contextPath}/assets/images/loading/loading-circle.gif"/>');
		        },
		        success: function(data) {
		            loadPage("<s:url action="ajax/adm/listKantor"/>","#result");
		            jQuery.facebox.close();
		        }
		    });
		});
		
		
	</script>
	
<div class='fbox_header'>Tambah Data District</div>
    <div class='fbox_container'>
    <s:form name="formeproc" id="formeproc">
        <div class='fbox_content'>
        	<div id="cek"></div>
            
            	<s:hidden name="district.id"></s:hidden>
            	<s:hidden name="district.createdDate"></s:hidden>
            	<s:hidden name="district.createdBy"></s:hidden>
            	<s:hidden name="district.updatedDate"></s:hidden>
            	<s:hidden name="district.updatedBy"></s:hidden>
            	<table style="min-width: 600px; margin: 0px 10px;" class="form formStyle">
            		<tr>
            			<td><s:label value="Parent"></s:label> </td>
            			<td colspan="3">
            				<s:select list="listDistrict" listKey="id" listValue="districtName" headerKey="0" headerValue="" name="district.district.id" cssClass="chzn" cssStyle="width:300px"></s:select>
            			</td>
            		</tr>        
            		<tr>
            			<td><s:label value="Nama Kantor (*)"></s:label> </td>
            			<td colspan="3"><s:textfield name="district.districtName" cssClass="required" /> </td>
            		</tr>
            		<tr>
            			<td><s:label value="Kewenangan"></s:label> </td>
            			<td colspan="3"><s:textfield name="district.limit" cssClass="number" /> </td>
            		</tr>  
            		<tr>
            			<td><s:label value="User Keuangan"></s:label> </td>
            			<td colspan="3">
            				<s:select list="listUser" name="keuangan.admUser.id" listKey="id" listValue="completeName" cssClass="chzn" cssStyle="width:300px" headerKey="" headerValue="--Pilih--"></s:select>
            			</td>
            		</tr>  
            	</table>
            
        </div>
        <div class='fbox_footer'>
            <s:submit key="save" cssClass="uibutton confirm"></s:submit>
            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()'>Batalkan</a>
        </div>
        </s:form>
    </div>
    