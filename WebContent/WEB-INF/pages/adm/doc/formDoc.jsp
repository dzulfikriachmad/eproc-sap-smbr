<%@ include file="/include/definitions.jsp" %>
	<script type="text/javascript">
		$(document).ready(function(){
			// FORM VALIDATION
			$("#formeproc").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
			});
			
			$("#formeproc").ajaxForm({
		        beforeSubmit: function() {
		            //$('#notif').html('<img src="${contextPath}/assets/images/loading/loading-circle.gif"/>');
		        },
		        success: function(data) {
		            loadPage("<s:url action="ajax/adm/listDokumen"/>","#result");
		            jQuery.facebox.close();
		        }
		    });
		});
		
		
	</script>
	
<div class='fbox_header'>Tambah Data Dokumen</div>
    <div class='fbox_container'>
    <s:form name="formeproc" id="formeproc">
        <div class='fbox_content'>
        	<div id="cek"></div>
           	<s:hidden name="doc.id"></s:hidden>
           	<s:hidden name="doc.createdDate"></s:hidden>
           	<s:hidden name="doc.createdBy"></s:hidden>
           	<s:hidden name="doc.updatedDate"></s:hidden>
           	<s:hidden name="doc.updatedBy"></s:hidden>
           	<table style="min-width: 600px; margin: 0px 10px;" class="form formStyle">       
           		<tr>
           			<td><s:label value="Nama Dokumen (*)"></s:label> </td>
           			<td colspan="3"><s:textfield name="doc.docName" cssClass="required" /> </td>
           		</tr>
           	</table>
        </div>
        <div class='fbox_footer'>
            <s:submit key="save" cssClass="uibutton confirm"></s:submit>
            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()'>Batalkan</a>
        </div>
    </s:form>
    </div>
    