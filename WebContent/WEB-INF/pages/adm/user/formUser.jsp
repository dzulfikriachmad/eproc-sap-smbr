<%@ include file="/include/definitions.jsp" %>
	<script type="text/javascript">
		$(document).ready(function(){
			// FORM VALIDATION
			$("#formeproc").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
			});
			
			$("#formeproc").ajaxForm({
		        beforeSubmit: function() {
		            //$('#notif').html('<img src="${contextPath}/assets/images/loading/loading-circle.gif"/>');
		        },
		        success: function(data) {
		            loadPage("<s:url action="ajax/adm/listUser"/>","#result");
		            jQuery.facebox.close();
		        }
		    });
		});
		
		
	</script>
	
<div class='fbox_header'>Tambah Data User</div>
    <div class='fbox_container'>
    <s:form name="formeproc" id="formeproc">
        <div class='fbox_content'>
        	<div id="cek"></div>
            
            	<s:hidden name="user.id"></s:hidden>
            	<s:hidden name="user.createdDate"></s:hidden>
            	<s:hidden name="user.createdBy"></s:hidden>
            	<s:hidden name="user.updatedDate"></s:hidden>
            	<s:hidden name="user.updatedBy"></s:hidden>
            	<s:hidden name="user.password"></s:hidden>
            	
            	<s:hidden name="user.admEmployee.id"></s:hidden>
            	<s:hidden name="user.admEmployee.createdDate"></s:hidden>
            	<s:hidden name="user.admEmployee.createdBy"></s:hidden>
            	<s:hidden name="user.admEmployee.updatedDate"></s:hidden>
            	<s:hidden name="user.admEmployee.updatedBy"></s:hidden>
            	
            	<table style="min-width: 600px; margin: 0px 10px;" class="form formStyle">   
            		
            		<tr>
            			<td><s:label value="NPP(*)"></s:label> </td>
            			<td colspan="3"><s:textfield name="user.admEmployee.empNpp" cssClass="required" /> </td>
            		</tr>
            		
            		<tr>
            			<td><s:label value="Nama Lengkap (*)"></s:label> </td>
            			<td colspan="3"><s:textfield name="user.admEmployee.empFullName" cssClass="required" /> </td>
            		</tr>
            		
            		<tr>
            			<td><s:label value="Email (*)"></s:label> </td>
            			<td colspan="3"><s:textfield name="user.admEmployee.empEmail" cssClass="required" /> </td>
            		</tr>
            		
            		<tr>
            			<td><s:label value="Telp.(*)"></s:label> </td>
            			<td colspan="3"><s:textfield name="user.admEmployee.empPhone" cssClass="required" /> </td>
            		</tr>
            		
            		<tr>
            			<td><s:label value="Posisi Struktural (*)"></s:label> </td>
            			<td colspan="3"><s:textfield name="user.admEmployee.empStructuralPos" cssClass="required" /> </td>
            		</tr>
            		
            		<tr>
            			<td><s:label value="Satuan Kerja"></s:label> </td>
            			<td colspan="3">
            				<s:select list="listDept" listKey="id" listValue="deptName" name="user.admEmployee.admDept.id"></s:select>
            			</td>
            		</tr>
            		
            		<tr>
            			<td><s:label value="Role 1"></s:label> </td>
            			<td colspan="3">
            				<s:select list="listRole" listKey="id" listValue="roleName" name="user.admEmployee.admRoleByEmpRole1.id"></s:select>
            			</td>
            		</tr>
            		<tr>
            			<td><s:label value="Role 2"></s:label> </td>
            			<td colspan="3">
            				<s:select list="listRole" listKey="id" listValue="roleName" name="user.admEmployee.admRoleByEmpRole2.id" headerKey="0" headerValue=""></s:select>
            			</td>
            		</tr>
            		<tr>
            			<td><s:label value="Role 3"></s:label> </td>
            			<td colspan="3">
            				<s:select list="listRole" listKey="id" listValue="roleName" name="user.admEmployee.admRoleByEmpRole3.id" headerKey="0" headerValue=""></s:select>
            			</td>
            		</tr>
            		<tr>
            			<td><s:label value="Role 4"></s:label> </td>
            			<td colspan="3">
            				<s:select list="listRole" listKey="id" listValue="roleName" name="user.admEmployee.admRoleByEmpRole4.id" headerKey="0" headerValue=""></s:select>
            			</td>
            		</tr>
            		
            	</table>
            
        </div>
        <div class='fbox_footer'>
            <s:submit key="save" cssClass="uibutton confirm"></s:submit>
            <s:submit name="reset" value="Reset Password" cssClass="uibutton" cssStyle="color:red"></s:submit>
            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()'>Batalkan</a>
        </div>
        </s:form>
    </div>
    