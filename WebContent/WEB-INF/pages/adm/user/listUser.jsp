<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	<script type="text/javascript">
		$(document).ready(function(){	
			
		});
		
		//Disable Enter key on form
		function stopRKey(evt) {
		  var evt = (evt) ? evt : ((event) ? event : null);
		  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
		  if ((evt.keyCode == 13) && (node.type=="text"))  {return false;}
		}
		
		document.onkeypress = stopRKey;

	</script>
</head>
<body>
 
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
			
			<div class="uibutton-toolbar extend">
				<a <c:if test="${create !=1 }"> style="visibility:hidden"</c:if> href='javascript:void(0)' class='uibutton icon add' onclick='load_into_box("<s:url action="ajax/adm/crudUser" />")'>Tambah Data User</a>
			</div>
				<div class="box">
					<div class="title center">
						DATA USER
						<span class="hide"></span>
					</div>
					<div class="content center">						
						<div id="result">
							<s:include value="ajax/listUser.jsp" />
						</div>
					</div>
				</div>		
		</div>
	</div>
</body>
</html>