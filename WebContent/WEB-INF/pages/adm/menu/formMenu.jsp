<%@ include file="/include/definitions.jsp" %>
	<script type="text/javascript">
		$(document).ready(function(){
			$(".chzn").chosen();
			// FORM VALIDATION
			$("#formeproc").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
			});
			
			$("#formeproc").ajaxForm({
		        beforeSubmit: function() {
		            loadingScreen();
		        },
		        success: function(data) {
		            loadPage("<s:url action="ajax/adm/listMenu"/>","#result");
		            jQuery.facebox.close();
		        }
		    });
		});
		
		
	</script>
	
<div class='fbox_header'>Tambah Data Role</div>
    <div class='fbox_container'>
    <s:form name="formeproc" id="formeproc">
        <div class='fbox_content'>
        	<div id="cek"></div>
            
            	<s:hidden name="menu.id"></s:hidden>
            	<s:hidden name="menu.createdDate"></s:hidden>
            	<s:hidden name="menu.createdBy"></s:hidden>
            	<s:hidden name="menu.updatedDate"></s:hidden>
            	<s:hidden name="menu.updatedBy"></s:hidden>
            	<table style="min-width: 600px; margin: 0px 10px;" class="form formStyle">   
            		<tr>
            			<td><s:label value="Parent Menu (*)"></s:label> </td>
            			<td colspan="3">
            				<s:select list="listMenu" listKey="id" listValue="menuLabel" name="menu.menuParent.id" headerKey="0" headerValue="" cssClass="chzn"></s:select>
            			</td>
            		</tr>
            		    
            		<tr>
            			<td><s:label value="Kode Menu (*)"></s:label> </td>
            			<td colspan="3"><s:textfield name="menu.menuCode" cssClass="required" /> </td>
            		</tr>
            		
            		<tr>
            			<td><s:label value="Label Menu (*)"></s:label> </td>
            			<td colspan="3"><s:textfield name="menu.menuLabel" cssClass="required" /> </td>
            		</tr>
            		
            		<tr>
            			<td><s:label value="List URL (*)"></s:label> </td>
            			<td colspan="3"><s:textfield name="menu.menuUrl" cssClass="required" /> </td>
            		</tr>
            		
            		<tr>
            			<td><s:label value="Create URL (*)"></s:label> </td>
            			<td colspan="3"><s:textfield name="menu.createUrl" cssClass="required" /> </td>
            		</tr>
            		
            		<tr>
            			<td><s:label value="Delete URL (*)"></s:label> </td>
            			<td colspan="3"><s:textfield name="menu.deleteUrl" cssClass="required" /> </td>
            		</tr>
            	</table>
            
        </div>
        <div class='fbox_footer'>
            <s:submit key="save" cssClass="uibutton confirm"></s:submit>
            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()'>Batalkan</a>
        </div>
        </s:form>
    </div>
    