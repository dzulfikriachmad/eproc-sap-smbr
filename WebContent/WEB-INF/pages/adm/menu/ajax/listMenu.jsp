<%@ include file="/include/definitions.jsp"%>
<!-- tree view --> 
<script type="text/javascript" src="${contextPath}/assets/js/tree/jquery.cookie.js"></script>
<script type="text/javascript" src="${contextPath}/assets/js/tree/jquery.treeview.js"></script>
<script type="text/javascript" src="${contextPath}/assets/js/tree/inittree.js"></script>
<link href="${contextPath}/assets/js/tree/jquery.treeview.css" rel="stylesheet" type="text/css" />
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
	<div style="text-align: left">
	
	<s:url var="set" action="ajax/adm/crudMenu" />
			<ul id="menu" class="filetree">
				<s:iterator value="%{listMenu}" var="i">
					<c:if test="${i.menuParent==null }">
						<li><span class="folder">
							${i.menuLabel }
							<a <c:if test="${update !=1 }"> style="visibility:hidden"</c:if> title="Ubah data Menu" class="uibutton icon edit" href='javascript:void(0)' onclick='load_into_box("${set}", "xCommand=edit&menu.id=${i.id}");'>&nbsp;</a>
							<a <c:if test="${delete !=1 }"> style="visibility:hidden"</c:if> title="Hapus data Menu" class="uibutton" href='javascript:void(0)' onclick='confirmPopUp("deleteObj(\"${set}\", \"xCommand=delete&menu.id=${i.id}\", \"#result\")", "Hapus Data", "Yakin ingin menghapus?", "Hapus", "Batal");'>X</a> 
							</span>
							
							<!-- second level -->
							<ul>
							<s:iterator value="%{listMenu}" var="j">
								<c:if test="${j.menuParent.id == i.id}">
									<li>							
										<c:choose>
											<c:when test="${j.admMenus.size()>0 }">
												<span class="folder"> 
												${j.menuLabel } &nbsp; 
												<a title="Ubah data Menu" class="uibutton icon edit" href='javascript:void(0)' onclick='load_into_box("${set}", "xCommand=edit&menu.id=${j.id}");' <c:if test="${update !=1 }"> style="visibility:hidden"</c:if>>&nbsp;</a>
												<a title="Hapus data Menu" class="uibutton" href='javascript:void(0)' onclick='confirmPopUp("deleteObj(\"${set}\", \"xCommand=delete&menu.id=${j.id}\", \"#result\")", "Hapus Data", "Yakin ingin menghapus?", "Hapus", "Batal");' <c:if test="${delete !=1 }"> style="visibility:hidden"</c:if>>X</a>
												</span>
											</c:when>
											<c:otherwise>
												${j.menuLabel } &nbsp; 
												<a <c:if test="${update !=1 }"> style="visibility:hidden"</c:if> title="Ubah data Menu" class="uibutton icon edit" href='javascript:void(0)' onclick='load_into_box("${set}", "xCommand=edit&menu.id=${j.id}");'>&nbsp;</a>
												<a <c:if test="${delete !=1 }"> style="visibility:hidden"</c:if> title="Hapus data Menu" class="uibutton" href='javascript:void(0)' onclick='confirmPopUp("deleteObj(\"${set}\", \"xCommand=delete&menu.id=${j.id}\", \"#result\")", "Hapus Data", "Yakin ingin menghapus?", "Hapus", "Batal");'>X</a>
											</c:otherwise>
										</c:choose>			
										
										<!-- third level -->
										<ul>
											<s:iterator value="%{listMenu}" var="k" >
												<c:if test="${k.menuParent.id == j.id and j.id ne i.id}">
													<li>${k.menuLabel } &nbsp;
														<a <c:if test="${update !=1 }"> style="visibility:hidden"</c:if>  title="Ubah data Menu" class="uibutton icon edit" href='javascript:void(0)' onclick='load_into_box("${set}", "xCommand=edit&menu.id=${k.id}");'>&nbsp;</a>
														<a <c:if test="${delete !=1 }"> style="visibility:hidden"</c:if> title="Hapus data Menu" class="uibutton" href='javascript:void(0)' onclick='confirmPopUp("deleteObj(\"${set}\", \"xCommand=delete&menu.id=${k.id}\", \"#result\")", "Hapus Data", "Yakin ingin menghapus?", "Hapus", "Batal");'>X</a>
													</li>	
												</c:if>
											</s:iterator>
										</ul>
									</li>	
								</c:if>
							</s:iterator>
							</ul>
						</li>
					</c:if>
				</s:iterator>
			</ul>
	</div>
			
<script>
	$(document).ready(function() {
		// SYSTEM MESSAGES
		$(".message").click(function() {
			$(this).fadeOut();
		});
	});
</script>