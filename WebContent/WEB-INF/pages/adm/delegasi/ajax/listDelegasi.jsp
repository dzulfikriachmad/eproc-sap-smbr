			<%@ include file="/include/definitions.jsp" %>
			<script type="text/javascript">
				   $(document).ready(function() {
					// DATATABLE
					 $('table.doc').dataTable({
				        "bInfo": false,
				        "iDisplayLength": 10,
				        "aLengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
				        "sPaginationType": "full_numbers",
				        "bPaginate": true,
				        "sDom": '<f>t<pl>'
				        });
					});
			</script>
			<center>
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>		
						
			<display:table name="${listAdmDelegasi }" id="s" requestURI="ajax/adm/listDelegasi.jwebs" excludedParams="*" style="width: 100%;" class="doc style1">
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column title="Dari"  class="center">${s.dari.completeName}</display:column>
				<display:column title="Kepada"  class="center">${s.kepada.completeName}</display:column>
				<display:column title="Tanggal"  class="center">
					<fmt:formatDate value="${s.tglMulai }" pattern="dd MMM yyyy"/> s/d <fmt:formatDate value="${s.tglBerakhir }" pattern="dd MMM yyyy"/> 
				</display:column>
				<display:column title="Alasan"  class="center">${s.keterangan}</display:column>
				<display:column title="Aksi" media="html" style="width: 80px;" class="center">
					<div class="uibutton-group">
						<s:url var="set" action="ajax/adm/crudDelegasi" />
						<a <c:if test="${update !=1 }"> style="visibility:hidden"</c:if> title="Ubah data Delegasi" class="uibutton icon edit" href='javascript:void(0)' onclick='load_into_box("${set}", "xCommand=edit&admDelegasi.id=${s.id}");'>&nbsp;</a>
					</div>
				</display:column>
			</display:table>
			</center>
			
			<script>
				$(document).ready(function(){	
					// SYSTEM MESSAGES
					$(".message").click(function () {
				      $(this).fadeOut();
				    });
				});
			</script>