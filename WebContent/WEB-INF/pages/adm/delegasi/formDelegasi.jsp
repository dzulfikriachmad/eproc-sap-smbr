<%@ include file="/include/definitions.jsp" %>
	<script type="text/javascript">
		$(document).ready(function(){
			// FORM VALIDATION
			$(".chzn").chosen();
			$.datepicker.setDefaults($.datepicker.regional['in_ID']);
			$("#tanggal1").datetimepicker({
				changeYear: true,
				changeMonth: true
			});
			$("#tanggal2").datetimepicker({
				changeYear: true,
				changeMonth: true
			});
			
			$("#formeproc").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
			});
			
			$("#formeproc").ajaxForm({
		        beforeSubmit: function() {
		            //$('#notif').html('<img src="${contextPath}/assets/images/loading/loading-circle.gif"/>');
		        },
		        success: function(data) {
		            loadPage("<s:url action="ajax/adm/delegasi"/>","#result");
		            jQuery.facebox.close();
		        }
		    });
		});
		
		
	</script>
	
<div class='fbox_header'>Tambah Data Delegasi</div>
    <div class='fbox_container'>
    <s:form name="formeproc" id="formeproc">
        <div class='fbox_content'>
        	<div id="cek"></div>
            
            	<s:hidden name="admDelegasi.id"></s:hidden>
            	<s:hidden name="admDelegasi.createdDate"></s:hidden>
            	<s:hidden name="admDelegasi.createdBy"></s:hidden>
            	<s:hidden name="admDelegasi.updatedDate"></s:hidden>
            	<s:hidden name="admDelegasi.updatedBy"></s:hidden>
            	<table style="min-width: 600px; margin: 0px 10px;" class="form formStyle">        
            		<tr>
            			<td><s:label value="Dari"></s:label> </td>
            			<td colspan="3">
            				<s:select list="listUser" listKey="id" listValue="completeName" name="admDelegasi.dari.id" cssClass="chzn"></s:select>
            				
            			</td>
            		</tr>
            		<tr>
            			<td><s:label value="Kepada"></s:label> </td>
            			<td colspan="3">
            				<s:select list="listUser" listKey="id" listValue="completeName" name="admDelegasi.kepada.id" cssClass="chzn"></s:select>
            				
            			</td>
            		</tr>
            		<tr>
            			<td><s:label value="Tanggal Mulai (*)"></s:label> </td>
            			<td colspan="3"><s:textfield name="admDelegasi.tglMulai" cssClass="required" id="tanggal1"/> </td>
            		</tr>		
            		<tr>
            			<td><s:label value="Tanggal Berakhir (*)"></s:label> </td>
            			<td colspan="3"><s:textfield name="admDelegasi.tglBerakhir" cssClass="required" id="tanggal2"/> </td>
            		</tr>
            		<tr>
            			<td><s:label value="Status"></s:label> </td>
            			<td colspan="3">
            				<s:select list="#{'1':'Aktif','2':'Tidak Aktif' }" name="admDelegasi.status"></s:select>
            			</td>
            		</tr> 
            		<tr>
            			<td><s:label value="Keterangan"></s:label> </td>
            			<td colspan="3"><s:textarea name="admDelegasi.keterangan" cssClass="required"/> </td>
            		</tr>  
            	</table>
            
        </div>
        <div class='fbox_footer'>
            <s:submit key="save" cssClass="uibutton confirm" ></s:submit>
            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()'>Batalkan</a>
        </div>
        </s:form>
    </div>
    