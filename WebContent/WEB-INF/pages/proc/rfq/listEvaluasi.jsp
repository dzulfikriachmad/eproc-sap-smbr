<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<title>Evaluasi Penawaran</title>
	
	<script type="text/javascript">
		function setValue(id){
			$('#ppmId').val(id);
		}
	</script>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="dashboard">Home</s:a></li> 
	        <li>Manajemen Pengadaan</li> 
	        <li>Perencanaan Pengadaan</li>
	        <li><s:a action="rfq/evaluasi">Evaluasi Penawaran</s:a></li>
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
	
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
			
				<div class="box">
					<div class="title">
						Daftar Pengadaan
						<span class="hide"></span>
					</div>
					
					<div class="content">						
						<s:form action="rfq/evaluasi">
							<s:hidden name="prcMainHeader.ppmId" id="ppmId" />
							<display:table name="${listPrcMainHeader }" id="s" excludedParams="*" style="width: 100%;" class="all style1">
								<display:setProperty name="paging.banner.full" value=""></display:setProperty>
								<display:setProperty name="paging.banner.first" value=""></display:setProperty>
								<display:setProperty name="paging.banner.last" value=""></display:setProperty>
								
								<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
								<display:column title="Pengadaan" total="true" style="width: 200px;">${s.ppmNomorRfq }</display:column>
								<display:column title="Nama Permintaan" style="width: 150px; " class="center">${s.ppmSubject }</display:column>
								<display:column title="User" style="width: 150px;" class="center">${s.admUserByPpmCurrentUser.completeName }</display:column>
								<display:column title="Satker" style="width: 100px;" class="center">${s.admDept.deptName }</display:column>
								<display:column title="Tanggal" class="center" style="width: 200px;"><fmt:formatDate value="${s.createdDate }" pattern="HH:mm:ss, dd MMM yyyy"/> </display:column>
								<display:column title="Aksi" class="center" style="width: 100px;">
									<input type="submit" name="proses" value="Proses" onclick="setValue('${s.ppmId }');FreezeScreen('Data Sedang Di proses')" class="uibutton" <c:if test="${create !=1 }"> style="visibility:hidden"</c:if>/>
								</display:column>
							</display:table>
						</s:form>
					</div>
				</div>
		</div>
	</div>
</body>
</html>