<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Kontrak</title>
	
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	
	<script type="text/javascript">	
		
		//regCancel Action
		function regCancel(){
			jQuery.facebox.close();
			window.location = "<s:url action="kontrak/daftarKontrakAktif" />"; 
		}
	</script>
	<style>caption{font-size: 15px; color: #FF9E0D; text-align: left;}</style>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="dashboard">Home</s:a></li> 
	        
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
    
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
					
			<s:form action="kontrak/daftarKontrakAktif" id="procForm" name="procForm" enctype="multipart/form-data" method="post">
				<s:hidden name="contractHeader.contractId" />
				<div class="box">
					<div class="title">
						Header
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle" >
							<tr>
								<td width="150px">No. Kontrak</td>
								<td>
									${contractHeader.contractNumber }
								</td>
							</tr>
							<tr>
								<td width="150px">No. SPPH</td>
								<td>
									${contractHeader.prcMainHeader.ppmNomorRfq }
								</td>
							</tr>
							<tr>
								<td width="150px">No. PR</td>
								<td>
									${contractHeader.prcMainHeader.ppmNomorPr }
								</td>
							</tr>
							
							<tr>
								<td width="150px">Judul Kontrak  (*)</td>
								<td>
									${contractHeader.contractSubject}
								</td>
							</tr>
							<tr>
								<td>Deskripsi Kontrak (*)</td>
								<td>
									${contractHeader.contractDescription }
								</td>
							</tr>
							<tr>
								<td>Jenis Kontrak (*)</td>
								<td>
									<c:if test="${contractHeader.contractType==1}">PO</c:if>
									<c:if test="${contractHeader.contractType==2}">Perjanjian</c:if> 
								</td>
							</tr>
							<tr>
								<td>Tanggal Kontrak Mulai</td>
								<td>
									<fmt:formatDate value="${contractHeader.contractStart }" pattern="dd MMM yyyy"/>
								</td>
							</tr>
							
							<tr>
								<td>Tanggal Kontrak Berakhir</td>
								<td>
									<fmt:formatDate value="${contractHeader.contractEnd }" pattern="dd MMM yyyy"/>
								</td>
							</tr>
							
							<tr>
								<td>Tanggal Penyerahan</td>
								<td>
									<fmt:formatDate value="${contractHeader.tglPenyerahan }" pattern="dd MMM yyyy"/>
								</td>
							</tr>
							
							<tr>
								<td width="150px">Attachment</td>
								<td>
									${contractHeader.prcMainHeader.attachment }
								</td>
							</tr>
							<tr>
								<td width="150px">Jumlah Kontrak</td>
								<td>
									<fmt:formatNumber> ${contractHeader.contractAmount }</fmt:formatNumber>
								</td>
							</tr>
							
							<tr>
								<td width="150px" style="color:red; font-weight: bolder" >Pemenang</td>
								<td>
									${fn:toUpperCase(contractHeader.vndHeader.vendorName) }
								</td>
							</tr>
							
						</table>
						
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Item
						<span class="hide"></span>
					</div>
					<div class="content center">
												
						<s:set id="i" value="0"></s:set>
						<display:table name="${listContractItem }" id="s" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
							<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
							<display:column title="Kode" style="width: 70px;" class="center">${s.itemCode }</display:column>
							<display:column title="Cost Center" style="width: 150px;" class="center">
								${s.itemCostCenter }
							</display:column>
							<display:column title="Deskripsi" class="left">${s.itemDescription }</display:column>
							<display:column title="Jumlah" style="text-align:right">
								<fmt:formatNumber> ${s.itemQuantityOe }</fmt:formatNumber>
							</display:column>
							<display:column title="Harga Satuan" style="text-align:right">
								<fmt:formatNumber> ${s.itemPriceOe }</fmt:formatNumber>
							</display:column>
							<display:column title="Pajak (%)" style="text-align:right">
								${s.itemTaxOe }
								<input type="hidden" value="${s.itemTaxOe }" id="itemTaxOe-${i }">
							</display:column>
							<display:column title="Sub Total" style="text-align:right">
								<fmt:formatNumber> ${s.itemTotalPriceOe }</fmt:formatNumber>
								<input type="hidden" value="${s.itemTotalPriceOe }" id="itemTotalPriceOe-${i }">
							</display:column>
							<display:column title="Total + Pajak" style="text-align:right">
								<script type="text/javascript">
									$(document).ready(function(){
										var totalAllOe = 'totaloe-'+${i};
										$('#'+totalAllOe).autoNumeric('init');
										var itax2 = parseFloat($('#itemTaxOe-${i }').val());
										var itotal2 = parseFloat($('#itemTotalPriceOe-${i }').val());

										if(!isNaN(itax2) && !isNaN(itotal2)){
											$('#'+totalAllOe).autoNumeric('set',(itax2 / 100 * itotal2 + itotal2));
										}
										
										
									});
									
								</script>
								<input type="text" readonly="readonly" id="totaloe-${i }" style="width:inherit;text-align:right"/>
							</display:column>
							<display:footer>
								<tr>
									<td colspan="9" style="color:red"></td>
								</tr>
							</display:footer>
							
						<s:set var="i" value="#i+1"></s:set>
						</display:table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Pembuat Kontrak
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle">
							<tr>
								<td width="150px;">Pembuat Kontrak</td>
								<td>
									${fn:toUpperCase(contractHeader.admUserByContractCreator.completeName)}
								</td>
							</tr>						
						</table>
					</div>
				</div>

				
				<div class="box">
					<div class="title">
						Termin
						<span class="hide"></span>
					</div>
					<div class="content">
						<div id="listTop">
							<s:include value="ajax/listTopRead.jsp" />
						</div>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Jaminan Pelaksanaan
						<span class="hide"></span>
					</div>
					<div class="content">
						<div id="listPb">
							<s:include value="ajax/listPbRead.jsp" />
						</div>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Daftar Komentar
						<span class="hide"></span>
					</div>
					<div class="content center">
						<display:table name="${listContractComment }" id="k" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
							<display:column title="No." style="width: 20px; text-align: center;">${k_rowNum}</display:column>
							<display:column title="Nama" style="width: 100px;" class="center">${k.username }</display:column>
							<display:column title="Posisi" style="width: 100px;" class="center">${k.posisi }</display:column>
							<display:column title="Proses" style="width: 100px;" class="center">${k.prosesAksi }</display:column>
							<display:column title="Tgl Pembuatan" style="width: 100px;" class="center">
								<fmt:formatDate value="${k.createdDate }" pattern="dd.MM.yyyy HH:mm"/> 
							</display:column>
							<display:column title="Tgl Update" style="width: 100px;" class="center">
								<fmt:formatDate value="${k.updatedDate }" pattern="dd.MM.yyyy HH:mm"/> 
							</display:column>
							<display:column title="Aksi" class="center" style="width: 250px;">${k.aksi }</display:column>
							<display:column title="Komentar" class="center" style="width: 250px;">${k.comment }</display:column>
							<display:column title="Dokumen" class="center" style="width: 200px;">
								<a href="${contextPath }/upload/file/${k.document }" target="_blank">${k.document }</a>
							</display:column>
						</display:table>
					</div>
				</div>
				
				
				
				<div class="uibutton-toolbar extend" >
					<a href="javascript:void(0)" class="uibutton" onclick="confirmPopUp('regCancel();', 'Batal', 'Apakah anda yakin untuk membatalkan transaksi ?', 'Ya', 'Tidak');"><s:text name="cancel"></s:text> </a>					
				</div>
				
						
			</s:form>
			
		</div>
	</div>
</body>
</html>