<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Persetujuan Pemenang</title>
	
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	
	<script type="text/javascript">	
		$(document).ready(function(){
			$(".chzn").chosen();
			
			// FORM VALIDATION
			$("#procForm").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler:function(form){
					FreezeScreen('Data Sedang di Proses');
					form.submit();
				}
			});
			
			
		});
		
		//cek value
		function cekValue(obj){
			if(obj.checked){
				obj.value=1;
			}else{
				obj.value=0;
			}
		}
		
		//regCancel Action
		function regCancel(){
			jQuery.facebox.close();
			window.location = "<s:url action="rfq/persetujuanPemenang" />"; 
		}
		
	</script>
	<style>caption{font-size: 15px; color: #FF9E0D; text-align: left;}</style>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="dashboard">Home</s:a></li> 
	        <li>Managemen Pengadaan</li> 
	        <li>Perencanaan Pengadaan</li> 
	        <li><s:a action="rfq/persetujuanPemenang">Persetujuan Pemenang</s:a></li>
	        
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
    
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
					
			<s:form action="rfq/persetujuanPemenang" id="procForm" name="procForm" enctype="multipart/form-data" method="post">
				<s:hidden name="prcMainHeader.ppmId" />
				<s:hidden name="prcMainHeader.admProses.id"></s:hidden>
				<s:hidden name="prcMainHeader.ppmProsesLevel"></s:hidden>
				<s:hidden name="prcMainHeader.groupCode"></s:hidden>
				
				<div class="box">
					<div class="title">
						Header
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle" >
							<tr>
								<td width="150px">No. SPPH</td>
								<td>
									${prcMainHeader.ppmNomorRfq }
								</td>
							</tr>
							<tr>
								<td width="150px">No. PR</td>
								<td>
									${prcMainHeader.ppmNomorPr }
								</td>
							</tr>
							<tr>
								<td width="150px">Pembuat</td>
								<td>${prcMainHeader.namaPembuatPr }</td>
							</tr>
							<tr>
								<td>Departemen</td>
								<td>
									${prcMainHeader.admDept.deptName }
								</td>
							</tr>
							<tr>
								<td>Kantor</td>
								<td>
									${prcMainHeader.admDistrict.districtName }
								</td>
							</tr>
							<tr>
								<td width="150px">Nama Permintaan</td>
								<td>${prcMainHeader.ppmSubject }</td>
							</tr>
							<tr>
								<td>Deskripsi Permintaan</td>
								<td>${prcMainHeader.ppmDescription } </td>
							</tr>
							<tr>
								<td>Jenis Permintaan</td>
								<td>
									<c:if test="${prcMainHeader.ppmJenisDetail == null}">
										<c:if test="${prcMainHeader.ppmJenis.id == 1 }">Barang</c:if>
										<c:if test="${prcMainHeader.ppmJenis.id == 2 }">Jasa</c:if>
									</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 3 }">Barang Raw Material</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 4 }">Barang Sparepart</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 5 }">Barang Umum</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 6 }">Jasa Teknik</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 7 }">Jasa Umum</c:if>
								</td>
							</tr>
							
							<tr>
								<td>Prioritas</td>
								<td>
									<c:if test="${prcMainHeader.ppmPrioritas == 1 }">High</c:if>
									<c:if test="${prcMainHeader.ppmPrioritas == 2 }">Normal</c:if>
									<c:if test="${prcMainHeader.ppmPrioritas == 3 }">Low</c:if>
								</td>
							</tr>
							
							<tr>
								<td>Chatting</td>
								<td>
								
									<a href="javascript:void(0)" onclick="load_into_box('<s:url action="ajax/chatInternal"/>','prcMainHeader.ppmId=${prcMainHeader.ppmId }')"><span id="chat">Chatting Disini</span></a>
								</td>
							</tr>
							<tr>
								<td>Sanggahan</td>
								<td>
									<a href="javascript:void(0)" onclick="load_into_box('<s:url action="ajax/adm/listSanggahan"/>','prcMainHeader.ppmId=${prcMainHeader.ppmId }')">Sanggahan</a>
								</td>
							</tr>
							<tr>
								<td>Attachment</td>
								<td>
									${prcMainHeader.attachment }									
								</td>
							</tr>
							<tr>
								<td>File</td>
								<td>
									<c:if test="${prcMainHeader.attachment!=null and prcMainHeader.attachment!='' }">
										<a href="${contextPath }/upload/file/${prcMainHeader.attachment}" target="_blank">Donload</a>
									</c:if>
								</td>
							</tr>
							
							<%-- <tr>
								<td>Tipe Permintaan (*)</td>
								<td><s:select list="" name="prcMainHeader.ppmTipe"></s:select></td>
							</tr>
							<tr>
								<td>Tanggal Dibutuhkan</td>
								<td><s:textfield name="prcMainHeader.vendorContactEmail"></s:textfield></td>
							</tr>
							<tr>
								<td>Lokasi Penyerahan</td>
								<td><s:textfield name="prcMainHeader.vendorContactEmail"></s:textfield></td>
							</tr> --%>
						</table>
						
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Item
						<span class="hide"></span>
					</div>
					<div class="content center">
						<script type="text/javascript">var totalEE=0,totalOE=0;</script>						
						<s:set id="i" value="0"></s:set>
						<display:table name="${listPrcMainItem }" id="s" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
							<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
							<display:column title="Kode" style="width: 70px;" class="center">${s.itemCode }</display:column>
							<display:column title="Keterangan">
								<%-- <c:choose>
									<c:when test="${s.fileName!=null and s.fileName!='' }">
										<a href="${contextPath }/upload/file/${s.fileName}" target="_blank">${s.attachment }</a>
									</c:when>
									<c:otherwise>
										${s.attachment }
									</c:otherwise>
								</c:choose> --%>
								${s.keterangan}
							</display:column>
							<display:column title="Deskripsi" class="left">${s.itemDescription }</display:column>
							<display:column title="Jumlah" style="text-align:right">
								<fmt:formatNumber> ${s.itemQuantity }</fmt:formatNumber>
								<hr />
								<span style="font-weight:bold;"><fmt:formatNumber> ${s.itemQuantityOe }</fmt:formatNumber></span>
							</display:column>
								<display:column title="Satuan" style="width: 40px;text-align:right">
								${s.admUom.uomName }
								<s:hidden name="listPrcMainItem[%{#i}].admUom.uomId"></s:hidden>
							</display:column>
							<display:column title="Harga Satuan" style="text-align:right">
								<fmt:formatNumber> ${s.itemPrice }</fmt:formatNumber>
								<hr />
								<span style="font-weight:bold;"><fmt:formatNumber> ${s.itemPriceOe }</fmt:formatNumber></span>
							</display:column>
							<display:column title="Pajak (%)" style="text-align:right">
								${s.itemTax }
								<hr />
								<span style="font-weight:bold;">${s.itemTaxOe }</span>
								<input type="hidden" value="${s.itemTax }" id="itemTax-${i }">
								<input type="hidden" value="${s.itemTaxOe }" id="itemTaxOe-${i }">
							</display:column>
							<display:column title="Sub Total" style="text-align:right">
								<fmt:formatNumber> ${s.itemTotalPrice }</fmt:formatNumber>
								<hr />
								<span style="font-weight:bold;"><fmt:formatNumber> ${s.itemTotalPriceOe }</fmt:formatNumber></span>
								<input type="hidden" value="${s.itemTotalPrice }" id="itemTotalPrice-${i }">
								<input type="hidden" value="${s.itemTotalPriceOe }" id="itemTotalPriceOe-${i }">
							</display:column>
							<display:column title="Total + Pajak" style="text-align:right">
								<script type="text/javascript">
									$(document).ready(function(){
										var itax = parseFloat($('#itemTax-${i }').val());
										var itotal = parseFloat($('#itemTotalPrice-${i }').val());
										var totalAll = 'total-'+${i};
										$('#'+totalAll).autoNumeric('init');
										if(!isNaN(itax) && !isNaN(itotal)){
											$('#'+totalAll).autoNumeric('set',(itax / 100 * itotal + itotal));
										}else{
											$('#'+totalAll).autoNumeric('set',0);
										}
										$("#totalAllEE").autoNumeric('init');
										totalEE = totalEE + (itax / 100 * itotal + itotal);
										$("#totalAllEE").autoNumeric('set',totalEE);
										
										var totalAllOe = 'totaloe-'+${i};
										$('#'+totalAllOe).autoNumeric('init');
										var itax2 = parseFloat($('#itemTaxOe-${i }').val());
										var itotal2 = parseFloat($('#itemTotalPriceOe-${i }').val());

										if(!isNaN(itax2) && !isNaN(itotal2)){
											$('#'+totalAllOe).autoNumeric('set',(itax2 / 100 * itotal2 + itotal2));
										}
										$("#totalAllOE").autoNumeric('init');
										totalOE = totalOE + (itax2 / 100 * itotal2 + itotal2);
										$("#totalAllOE").autoNumeric('set',totalOE);
									});
									
								</script>
								<input type="text" readonly="readonly" id="total-${i }" style="width:inherit;text-align:right"/>
								<hr/>
								<input type="text" readonly="readonly" id="totaloe-${i }" style="width:inherit;text-align:right"/>
							</display:column>
							<display:column title="Pemenang" style="color:red; font-weight:bolder">
								${fn:toUpperCase(s.vndHeader.vendorName) }
							</display:column>
							<display:footer>
								<tr style="font-weight: bold;">
									<td colspan="8" style="color:red">&nbsp;</td>
									<td style="text-align: right;color:red">Total EE</td>
									<td style="text-align: right;">
									<input type="text" readonly="readonly" id="totalAllEE" style="width:inherit;text-align:right;color:red;font-weight:bold"/>
									</td>
								</tr>
								<tr style="font-weight: bold;">
									<td colspan="8" style="color:red">&nbsp;</td>
									<td style="text-align: right;color:blue">Total OE</td>
									<td style="text-align: right;">
									<input type="text" readonly="readonly" id="totalAllOE" style="width:inherit;text-align:right;color:blue;font-weight:bold"/>
									</td>
								</tr>
								<tr>
									<td colspan="9" style="font-weight: bold;">(* Nilai OE berhuruf tebal Biru</td>
								</tr>
							</display:footer>
							
						<s:set var="i" value="#i+1"></s:set>
						</display:table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Anggaran
						<span class="hide"></span>
					</div>
					<div class="content">
						<script type="text/javascript">
							$(document).ready(function(){
								$('#anggaran').load("${contextPath}/ajax/pp/listAnggaranRead.jwebs?prcMainHeader.ppmId=${prcMainHeader.ppmId}")
							});
							</script>
						<div id="anggaran">
						</div>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Estimator
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle">
							<tr>
								<td width="150px;">Estimator</td>
								<td>${prcMainHeader.admUserByPpmEstimator.completeName }</td>
							</tr>						
						</table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Pembeli
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle">
							<tr>
								<td width="150px;">Pembeli</td>
								<td>${prcMainHeader.admUserByPpmBuyer.completeName }</td>
							</tr>						
						</table>
					</div>
				</div>
				
				<c:if test="${prcMainHeader.ppmJumlahOe > 250000000 }">
				<div class="box">
					<div class="title">
						Panitia Pengadaan
						<span class="hide"></span>
					</div>
					<div class="content center">
						<table class="form formStyle">
							<tr>
								<td width="150px;">Nama Panitia</td>
								<td>
									${panitia.namaPanitia }
								</td>
							</tr>
							<tr>
								<td width="150px;">Keterangan</td>
								<td>
									${panitia.keterangan }
								</td>
							</tr>	
						</table>
						<div style="padding-top: 50px;">
						<s:set id="i" value="0"></s:set>
						<display:table name="${listPanitiaDetail }" id="s" excludedParams="*" style="width: 100%;" class="all style1">
							<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
							<display:column title="Nama Anggota" style="width: 100px;" class="center">${s.anggota.completeName }</display:column>
							<display:column title="Posisi" style="width: 150px;" class="center">
								<c:if test="${s.posisi==1 }">Ketua</c:if>
								<c:if test="${s.posisi==2 }">Sekretaris</c:if>
								<c:if test="${s.posisi==3 }">Anggota</c:if>
							</display:column>
							
						<s:set var="i" value="#i+1"></s:set>
						</display:table>
						</div>
					</div>
				</div>
				</c:if>
				
				
				<div class="box">
					<div class="title">
						Jadwal Pengadaan
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle">
							<tr>
								<td width="150px;">Tanggal Pembukaan Pendaftaran</td>
								<td>
									<fmt:formatDate value="${prcMainPreparation.registrastionOpening}" pattern="dd.MM.yyyy HH:mm"/>
								</td>
							</tr>
							<tr>
								<td width="150px;">Tanggal Penutupan Pendaftaran</td>
								<td>
									<fmt:formatDate value="${prcMainPreparation.registrastionClosing}" pattern="dd.MM.yyyy HH:mm"/>
								</td>
							</tr>	
							<tr>
								<td width="150px;">Tanggal Aanwijzing</td>
								<td>
									<fmt:formatDate value="${prcMainPreparation.aanwijzingDate}" pattern="dd.MM.yyyy HH:mm"/>
								</td>
							</tr>
							<tr>
								<td width="150px;">Tempat Aanwijzing</td>
								<td>
									${prcMainPreparation.aanwijzingPlace}
								</td>
							</tr>		
							<tr>
								<td width="150px;">Tanggal Pembukaan Penawaran</td>
								<td>
									<fmt:formatDate value="${prcMainPreparation.quotationOpening}" pattern="dd.MM.yyyy HH:mm"/>
								</td>
							</tr>	
							<c:if test="${prcMainHeader.prcMethod.id == 6 }">
							<c:if test="${prcMainHeader.tahap == 1 }">
							<tr>
								<td width="150px">Tanggal Pembukaan Penawaran 2 </td>
								<td>
									<fmt:formatDate value="${prcMainPreparation.quotationOpening2}" pattern="dd.MM.yyyy HH:mm"/>
								</td>
							</tr>
							</c:if>
							</c:if>																																												
						</table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Metode Pengadaan
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle">
							<tr>
								<td width="150px;">Metode Pengadaan</td>
								<td>${prcMainHeader.prcMethod.methodName }</td>
							</tr>
							<tr>
								<td width="150px;">Metode Penilaian</td>
								<td>
									${prcMainHeader.prcTemplate.templateName }
								</td>
							</tr>
							<tr>
								<td width="150px;">Tipe Penawaran</td>
								<td>
									  
									Tipe A <input type="checkbox" <c:if test="${prcMainHeader.ppmTipeA==1 }">checked="checked"</c:if> disabled="disabled"/> &nbsp;
									Tipe B <input type="checkbox" <c:if test="${prcMainHeader.ppmTipeB==1 }">checked="checked"</c:if> disabled="disabled"/> &nbsp;
									Tipe C <input type="checkbox" <c:if test="${prcMainHeader.ppmTipeC==1 }">checked="checked"</c:if> disabled="disabled"/> &nbsp;
									<br/>
									<pre>(* A: sesuai spek, B: Spek Beda jumlah Jumlah sama, C:Spek Beda, Jumlah Beda)</pre>
								</td>
							</tr>
							<tr>
								<td width="150px;">Harga </td>
								<td>
									<c:if test="${prcMainHeader.oeOpen==1 }">Terbuka</c:if>
									<c:if test="${prcMainHeader.oeOpen!=1 }">Tertutup</c:if>
								</td>
							</tr>
							<tr>
								<td width="150px;">Sistem Penawaran </td>
								<td>
									<c:if test="${prcMainHeader.ppmEauction==1 }">Paket</c:if>
									<c:if test="${prcMainHeader.ppmEauction!=1 }">Itemize</c:if>
								</td>
							</tr>						
						</table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Mitra Kerja
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle">
							<tr>
								<td width="150px;">Klasifikasi Peserta</td>
								<td>
									Kecil <input type="checkbox" <c:if test="${prcMainHeader.kecil==1}">checked="checked"</c:if> disabled="disabled"/> &nbsp;
									Non Kecil <input type="checkbox" <c:if test="${prcMainHeader.menengah==1}">checked="checked"</c:if> disabled="disabled"/> &nbsp;
									<s:hidden id="sizemitra"></s:hidden>
								</td>
							</tr>
						</table>
						
						<div>
							<s:include value="ajax/listTabulasiRead.jsp" />
						</div>

						<div id="result">
							<s:include value="ajax/listNegosiasi.jsp" />
						</div>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Daftar Komentar
						<span class="hide"></span>
					</div>
					<div class="content">
						<display:table name="${listHistMainComment }" id="k" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
							<display:column title="No." style="width: 20px; text-align: center;">${k_rowNum}</display:column>
							<display:column title="Nama" style="width: 100px;" class="center">${k.username }</display:column>
							<display:column title="Posisi" style="width: 100px;" class="center">${k.posisi }</display:column>
							<display:column title="Proses" style="width: 100px;" class="center">${k.prosesAksi }</display:column>
							<display:column title="Tgl Pembuatan" style="width: 100px;" class="center">
								<fmt:formatDate value="${k.createdDate }" pattern="dd.MM.yyyy HH:mm"/> 
							</display:column>
							<display:column title="Tgl Update" style="width: 100px;" class="center">
								<fmt:formatDate value="${k.updatedDate }" pattern="dd.MM.yyyy HH:mm"/> 
							</display:column>
							<display:column title="Aksi" class="center" style="width: 250px;">${k.aksi }</display:column>
							<display:column title="Komentar" class="center" style="width: 250px;">${k.comment }</display:column>
							<display:column title="Dokumen" class="center" style="width: 200px;">
								<a href="${contextPath }/upload/file/${k.document }" target="_blank">${k.document }</a>
							</display:column>
						</display:table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Komentar
						<span class="hide"></span>
					</div>
					<div class="content">
						<s:hidden name="histMainComment.username" value="%{admEmployee.empFullName}" />
						<table class="form formStyle">
							<tr>
								<td width="150px;">Komentar (*)</td>
								<td><s:textarea name="histMainComment.comment" cssClass="required" id="komentarVal"></s:textarea> </td>
							</tr>	
							<tr>
								<td width="150px;">Lampiran</td>
								<td><s:file name="docsKomentar" cssClass="uibutton" /></td>
							</tr>							
						</table>
					</div>
				</div>
				
				<div class="uibutton-toolbar extend" >
					<s:submit name="revisi" id="revisi" onclick="actionAlert('revisi','revisi');return false;" value="Revisi" cssClass="uibutton" cssStyle="color:red"></s:submit>
					<c:if test="${prcMainHeader.admUserByPpmCurrentUser.id!=sessionScope.user.id and  prcMainHeader.finalisasiSpph!=2}">
						<s:submit name="setujuPanitia" id="setujuPanitia" onclick="actionAlert('setujuPanitia','setujuPanitia');return false;" value="Approve Panitia" cssClass="uibutton" ></s:submit>
					</c:if>
					<c:if test="${prcMainHeader.admUserByPpmCurrentUser!=null and  prcMainHeader.finalisasiSpph!=2 and prcMainHeader.admUserByPpmCurrentUser.id==sessionScope.user.id}">
						<s:submit name="saveAndFinish" id="saveAndFinish" onclick="actionAlert('saveAndFinish','saveAndFinish');return false;" value="Approve" cssClass="uibutton"></s:submit>
					</c:if>
					<c:if test="${prcMainHeader.finalisasiSpph==2 }">
						<s:submit name="finalisasi" id="finalisasi" onclick="actionAlert('finalisasi','finalisasi');return false;" value="Finalisasi" cssClass="uibutton"></s:submit>
					</c:if>
					
					<a href="javascript:void(0)" class="uibutton" onclick="confirmPopUp('regCancel();', 'Batal', 'Apakah anda yakin untuk membatalkan transaksi ?', 'Ya', 'Tidak');"><s:text name="cancel"></s:text> </a>					
				</div>
						
			</s:form>
			
		</div>
	</div>
</body>
</html>