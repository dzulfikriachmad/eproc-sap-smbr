<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Draft Kontrak Adendum</title>
	
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	
	<script type="text/javascript">	
		$(document).ready(function(){
			$(".chzn").chosen();
			$.datepicker.setDefaults($.datepicker.regional['in_ID']);
			$("#tgl1").datepicker({
				changeYear: true,
				changeMonth: true
			});
			$("#tgl2").datepicker({
				changeYear: true,
				changeMonth: true
			});
			
			// FORM VALIDATION
			$("#procForm").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler:function(form){
					reFormat();
					FreezeScreen('Data Sedang di Proses');
					form.submit();
				}
			});
			
			$('#topPercentage').autoNumeric('init');
			$('#topValue').autoNumeric('init');
			$('#pbValue').autoNumeric('init');
			onloadData();
			
		});
		
		function calculateTermin(){
			var totalKontrak = parseFloat($('#contractAmount').val());
			var persen = parseFloat($('#topPercentage').autoNumeric('get'));
			if(persen>100){
				alert("Persentase tidak boleh lebih dari 100")
				$('#topPercentage').autoNumeric('set',0);
				$('#topValue').autoNumeric('set',0);
				return;
			}
			var hasil = (persen/100)*totalKontrak;
			$('#topValue').autoNumeric('set',hasil);
		}
		function tambahTop(){
			var cek = $('#topDescription').val();
			if(cek==''){
				alert("Termin Harus Diisi");
				return false;
			}
			
			cek = $('#topPercentage').val();
			if(cek==''){
				alert("Persentase Termin Harus Diisi");
				return false;
			}
			
			cek = $('#topValue').val();
			if(cek==''){
				alert("Nilai Termin Harus Diisi");
				return false;
			}
			
			$('#topPercentage').val($('#topPercentage').autoNumeric('get'));
			$('#topValue').val($('#topValue').autoNumeric('get'));
			sendForm(document.procForm, "<s:url action="ajax/submitTop" />", "#listTop");
			jQuery.facebox.close();
			resetTop();
		}
		
		function resetTop(){
			$('#topDescription').val('');
			$('#topPercentage').val('');
			$('#topValue').val('');
		}
		
		function tambahPb(){
			var cek = $('#pbNumber').val();
			if(cek==''){
				alert("Nomor Jaminan Harus Diisi");
				return false;
			}
			
			cek = $('#pbIssuedBy').val();
			if(cek==''){
				alert("Yang Mengeluarkan Jaminan Harus Diisi");
				return false;
			}
			
			cek = $('#pbIssuedDate').val();
			if(cek==''){
				alert("Tanggal Jaminan Harus Diisi");
				return false;
			}
			
			cek = $('#pbExpiredDate').val();
			if(cek==''){
				alert("Tanggal Expired Jaminan Harus Diisi");
				return false;
			}
			
			cek = $('#pbValue').val();
			if(cek==''){
				alert("Jumlah Jaminan Harus Diisi");
				return false;
			}
			$('#pbValue').val($('#pbValue').autoNumeric('get'));
			sendForm(document.procForm, "<s:url action="ajax/submitPb" />", "#listPb");
			jQuery.facebox.close();
			resetPb();
		}
		
		function resetPb(){
			$('#pbNumber').val('');
			$('#pbIssuedBy').val('');
			$('#pbIssuedDate').val('');
			$('#pbExpiredDate').val('');
			$('#pbValue').val('');
		}
		
		
		//regCancel Action
		function regCancel(){
			jQuery.facebox.close();
			window.location = "<s:url action="kontrak/draftKontrak" />"; 
		}
		
		function calculateData(obj,index){
			try{
				var price = 'itemPriceOe'+index;
				var quantity = 'itemQuantityOe'+index;
				var totalPrice = 'itemTotalPriceOe'+index;
				
				var totalPriceOe=0.0;
				var itemQtyOe = parseFloat($('#'+quantity).autoNumeric('get'));
				var itemPriceOe = parseFloat($('#'+price).autoNumeric('get'));
	
				totalPriceOe = itemQtyOe * itemPriceOe;
				if(isNaN(totalPriceOe))
					totalPriceOe=0;
				
				$('#'+totalPrice).autoNumeric('set',totalPriceOe);
				
				//calculate total
				var size = document.getElementById('size').value;
				
				var totalPenawaran = 0;
				if(size>0){
					for(var i=0;i<size;i++){
						var tp = 'itemTotalPriceOe'+i;
						totalPenawaran = totalPenawaran + parseFloat($('#'+tp).autoNumeric('get'));
					}	
				}
				//document.getElementById("contractHeaderAdendum.contractAmount").value=totalPenawaran.toFixed(2);
				document.getElementById("contractHeaderAdendum.contractAmount").value=totalPenawaran;
			}catch (e) {
				alert(e);
			}
			
		}
		
		function reFormat(){
			var size = document.getElementById('size').value;
			if(size>0){
				for(var i=0;i<size;i++){
					var price = 'itemPriceOe'+i;
					var quantity = 'itemQuantityOe'+i;
					var totalPrice = 'itemTotalPriceOe'+i;
					$('#'+price).val($('#'+price).autoNumeric('get'));
					$('#'+quantity).val($('#'+quantity).autoNumeric('get'));
					$('#'+totalPrice).val($('#'+totalPrice).autoNumeric('get'));
				}	
			}
		}
		
		function onloadData(){
			var size = document.getElementById('size').value;
			if(size>0){
				for(var i=0;i<size;i++){
					var price = 'itemPriceOe'+i;
					$('#'+price).autoNumeric('init');
					var totalPrice = 'itemTotalPriceOe'+i;
					$('#'+totalPrice).autoNumeric('init');
					var quantity = 'itemQuantityOe'+i;
					$('#'+quantity).autoNumeric('init');
				}
			}		
		}

	</script>
	<style>caption{font-size: 15px; color: #FF9E0D; text-align: left;}</style>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="dashboard">Home</s:a></li> 
	        <li>Managemen Kontrak</li>  
	        <li><s:a action="kontrak/daftarKontrakAktif"> Daftar Kontrak Aktif</s:a></li>
	        
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
    
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
					
			<s:form action="kontrak/daftarKontrakAktif" id="procForm" name="procForm" enctype="multipart/form-data" method="post">
				<s:hidden name="contractHeader.contractId" />
				<s:hidden name="contractHeader.contractAmount" id="contractAmount"></s:hidden>
				<s:hidden name="contractHeaderAdendum.ctrContractHeader.contractId" value="%{contractHeader.contractId}"></s:hidden>
				<s:hidden name="contractHeaderAdendum.contractProsesLevel" value="2"></s:hidden>
				<s:hidden name="contractHeaderAdendum.contractAmount" id="contractHeaderAdendum.contractAmount" value="%{contractHeader.contractAmount}"></s:hidden>
				<div class="box">
					<div class="title">
						Header
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle" >
							<tr>
								<td width="150px">No. Kontrak Adendum</td>
								<td>
									<s:textfield name="contractHeaderAdendum.contractNumber" readonly="true"></s:textfield>
								</td>
							</tr>
							<tr>
								<td width="150px">No. SPPH</td>
								<td>
									${contractHeader.prcMainHeader.ppmNomorRfq }
								</td>
							</tr>
							<tr>
								<td width="150px">No. PR</td>
								<td>
									${contractHeader.prcMainHeader.ppmNomorPr }
								</td>
							</tr>
							
							<tr>
								<td width="150px">Judul Kontrak</td>
								<td>
									<s:textfield name="contractHeaderAdendum.contractSubject" cssClass="required"></s:textfield>
								</td>
							</tr>
							<tr>
								<td>Deskripsi Kontrak</td>
								<td>
									<s:textarea name="contractHeaderAdendum.contractDescription" cssClass="required"></s:textarea>
								</td>
							</tr>
							<tr>
								<td>Jenis Kontrak</td>
								<td>
									<s:hidden name="contractHeaderAdendum.contractType" value="%{contractHeader.contractType}"></s:hidden>
									<c:if test="${contractHeader.contractType==1}">
										PO
									</c:if>
									<c:if test="${contractHeader.contractType==2}">
										PO & Perjanjian
									</c:if> 
								</td>
							</tr>
							<tr>
								<td>Tanggal Kontrak Mulai</td>
								<td><s:textfield name="contractHeaderAdendum.contractStart" id="tgl1" cssClass="required"></s:textfield></td>
							</tr>
							
							<tr>
								<td>Tanggal Kontrak Berakhir</td>
								<td><s:textfield name="contractHeaderAdendum.contractEnd" id="tgl2" cssClass="required"></s:textfield></td>
							</tr>
							
							<tr>
								<td width="150px">Alasan Adendum</td>
								<td>
									<s:textarea name="contractHeaderAdendum.contractAmmendReason" cssClass="required" id="komentarVal"></s:textarea>
								</td>
							</tr>
							
							<tr>
								<td width="150px" style="color:red; font-weight: bolder" >Pemenang</td>
								<td>
									${fn:toUpperCase(contractHeader.vndHeader.vendorName) }
								</td>
							</tr>
							
						</table>
						
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Item
						<span class="hide"></span>
					</div>
					<div class="content center">
						<input type="hidden" id="size" value="${listContractItem.size()}"/>						
						<s:set id="i" value="0"></s:set>
						<display:table name="${listContractItem }" id="s" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
							<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
							<display:column title="Kode" style="width: 70px;" class="center">${s.itemCode }</display:column>
							<display:column title="Cost Center" style="width: 150px;" class="center">
								${s.itemCostCenter }
							</display:column>
							<display:column title="Deskripsi" class="left">${s.itemDescription }</display:column>
							<display:column title="Jumlah" style="text-align:right">
								<fmt:formatNumber> ${s.itemQuantityOe }</fmt:formatNumber><hr/>
								<input style="width:inherit;text-align:right" type="text" name="listContractItem[${s_rowNum-1}].itemQuantityOe" id="itemQuantityOe${s_rowNum-1 }" value="${s.itemQuantityOe }" onkeyup="calculateData(this,${s_rowNum-1})"/>
							</display:column>
							<display:column title="Harga Satuan" style="text-align:right">
								<fmt:formatNumber> ${s.itemPriceOe }</fmt:formatNumber><hr/>
								<input style="width:inherit;text-align:right" type="text" name="listContractItem[${s_rowNum-1}].itemPriceOe" id="itemPriceOe${s_rowNum-1 }" value="${s.itemPriceOe }" onkeyup="calculateData(this,${s_rowNum-1})"/>
							</display:column>
							<display:column title="Pajak (%)" style="text-align:right">
								${s.itemTaxOe }
								<input type="hidden" value="${s.itemTaxOe }" id="itemTaxOe-${i }">
								<input type="hidden" name="listContractItem[${s_rowNum-1 }].itemTaxOe" value="${s.itemTaxOe }"/>
							</display:column>
							<display:column title="Sub Total" style="text-align:right">
								<fmt:formatNumber> ${s.itemTotalPriceOe }</fmt:formatNumber><hr/>
								<input type="hidden" value="${s.itemTotalPriceOe }" id="itemTotalPriceOe-${i }">
								<input style="width:inherit;text-align:right" type="text" name="listContractItem[${s_rowNum-1 }].itemTotalPriceOe" id="itemTotalPriceOe${s_rowNum-1 }" value="${s.itemTotalPriceOe }" onkeyup="calculateData(this,${s_rowNum-1})"/>
								<input type="hidden" name="listContractItem[${s_rowNum-1 }].admUom.uomId" value="${s.admUom.uomId }"/>
								<input type="hidden" name="listContractItem[${s_rowNum-1 }].groupCode" value="${s.groupCode }"/>
								<input type="hidden" name="listContractItem[${s_rowNum-1 }].itemTipe" value="${s.itemTipe }"/>
								<input type="hidden" name="listContractItem[${s_rowNum-1 }].itemCode" value="${s.itemCode }"/>
								<input type="hidden" name="listContractItem[${s_rowNum-1 }].itemDescription" value="${s.itemDescription }"/>
								<input type="hidden" name="listContractItem[${s_rowNum-1 }].itemCostCenter" value="${s.itemCostCenter }"/>
								<input type="hidden" name="listContractItem[${s_rowNum-1 }].attachment" value="${s.attachment }"/>
							</display:column>
							<display:column title="Total + Pajak" style="text-align:right">
								<script type="text/javascript">
									$(document).ready(function(){
										var totalAllOe = 'totaloe-'+${i};
										$('#'+totalAllOe).autoNumeric('init');
										var itax2 = parseFloat($('#itemTaxOe-${i }').val());
										var itotal2 = parseFloat($('#itemTotalPriceOe-${i }').val());

										if(!isNaN(itax2) && !isNaN(itotal2)){
											$('#'+totalAllOe).autoNumeric('set',(itax2 / 100 * itotal2 + itotal2));
										}
										
										
									});
									
								</script>
								<input type="text" readonly="readonly" id="totaloe-${i }" style="width:inherit;text-align:right"/>
							</display:column>

						<s:set var="i" value="#i+1"></s:set>
						
						</display:table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Pembuat Kontrak
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle">
							<tr>
								<td width="150px;">Pembuat Kontrak</td>
								<td>
									${fn:toUpperCase(contractHeader.admUserByContractCreator.completeName)}
								</td>
							</tr>						
						</table>
					</div>
				</div>

				
				<div class="box">
					<div class="title">
						Termin
						<span class="hide"></span>
					</div>
					<div class="content">
						
						<div id="listTop">
							<s:include value="ajax/listTopRead.jsp" />
						</div>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Jaminan Pelaksanaan
						<span class="hide"></span>
					</div>
					<div class="content">
						<div id="listPb">
							<s:include value="ajax/listPbRead.jsp" />
						</div>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Daftar Komentar
						<span class="hide"></span>
					</div>
					<div class="content center">
						<display:table name="${listContractComment }" id="k" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
							<display:column title="No." style="width: 20px; text-align: center;">${k_rowNum}</display:column>
							<display:column title="Nama" style="width: 100px;" class="center">${k.username }</display:column>
							<display:column title="Posisi" style="width: 100px;" class="center">${k.posisi }</display:column>
							<display:column title="Proses" style="width: 100px;" class="center">${k.prosesAksi }</display:column>
							<display:column title="Tgl Pembuatan" style="width: 100px;" class="center">
								<fmt:formatDate value="${k.createdDate }" pattern="dd.MM.yyyy HH:mm"/> 
							</display:column>
							<display:column title="Tgl Update" style="width: 100px;" class="center">
								<fmt:formatDate value="${k.updatedDate }" pattern="dd.MM.yyyy HH:mm"/> 
							</display:column>
							<display:column title="Aksi" class="center" style="width: 250px;">${k.aksi }</display:column>
							<display:column title="Komentar" class="center" style="width: 250px;">${k.comment }</display:column>
							<display:column title="Dokumen" class="center" style="width: 200px;">
								<a href="${contextPath }/upload/file/${k.document }" target="_blank">${k.document }</a>
							</display:column>
						</display:table>
					</div>
				</div>

				<div class="uibutton-toolbar extend" >
					<s:submit name="saveadendum" id="btnSubmit" onclick="actionAlert('btnSubmit','Adendum');return false;" value="Simpan dan Selesai" cssClass="uibutton" ></s:submit>
					<a href="javascript:void(0)" class="uibutton" onclick="confirmPopUp('regCancel();', 'Batal', 'Apakah anda yakin untuk membatalkan transaksi ?', 'Ya', 'Tidak');"><s:text name="cancel"></s:text> </a>					
				</div>
				
						
			</s:form>
			
		</div>
	</div>
</body>
</html>