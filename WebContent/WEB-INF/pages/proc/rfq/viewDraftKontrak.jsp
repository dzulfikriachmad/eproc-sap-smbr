<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Draft Kontrak</title>
	
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	
	<script type="text/javascript">	
		$(document).ready(function(){
			$(".chzn").chosen();
			
			$.datepicker.setDefaults($.datepicker.regional['in_ID']);
			$("#tgl1").datepicker({
				changeYear: true,
				changeMonth: true
			});
			$("#tgl2").datepicker({
				changeYear: true,
				changeMonth: true
			});
			$("#tgl3").datepicker({
				changeYear: true,
				changeMonth: true
			});
			
			$("#pbIssuedDate").datepicker({
				changeYear: true,
				changeMonth: true
			});
			$("#pbExpiredDate").datepicker({
				changeYear: true,
				changeMonth: true
			});
			
			// FORM VALIDATION
			$("#procForm").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler:function(form){
					FreezeScreen('Data Sedang di Proses');
					form.submit();
				}
			});
			
			$('#topPercentage').autoNumeric('init');
			$('#topValue').autoNumeric('init');
			$('#pbValue').autoNumeric('init');
			
			//onload
			if($('#ctrType')==2){
				$('#userLegal').show();
				$('#btnLegal').show();
			}
		});
		
		function calculateTermin(){
			var totalKontrak = parseFloat($('#contractAmount').val());
			var persen = parseFloat($('#topPercentage').autoNumeric('get'));
			if(persen>100){
				alert("Persentase tidak boleh lebih dari 100")
				$('#topPercentage').autoNumeric('set',0);
				$('#topValue').autoNumeric('set',0);
				return;
			}
			var hasil = (persen/100)*totalKontrak;
			$('#topValue').autoNumeric('set',hasil);
		}
		function tambahTop(){
			var cek = $('#topDescription').val();
			if(cek==''){
				alert("Termin Harus Diisi");
				return false;
			}
			
			cek = $('#topPercentage').val();
			if(cek==''){
				alert("Persentase Termin Harus Diisi");
				return false;
			}
			
			cek = $('#topValue').val();
			if(cek==''){
				alert("Nilai Termin Harus Diisi");
				return false;
			}
			
			$('#topPercentage').val($('#topPercentage').autoNumeric('get'));
			$('#topValue').val($('#topValue').autoNumeric('get'));
			sendForm(document.procForm, "<s:url action="ajax/submitTop" />", "#listTop");
			jQuery.facebox.close();
			resetTop();
		}
		
		function resetTop(){
			$('#topDescription').val('');
			$('#topPercentage').val('');
			$('#topValue').val('');
		}
		
		function tambahPb(){
			var cek = $('#pbNumber').val();
			if(cek==''){
				alert("Nomor Jaminan Harus Diisi");
				return false;
			}
			
			cek = $('#pbIssuedBy').val();
			if(cek==''){
				alert("Yang Mengeluarkan Jaminan Harus Diisi");
				return false;
			}
			
			cek = $('#pbIssuedDate').val();
			if(cek==''){
				alert("Tanggal Jaminan Harus Diisi");
				return false;
			}
			
			cek = $('#pbExpiredDate').val();
			if(cek==''){
				alert("Tanggal Expired Jaminan Harus Diisi");
				return false;
			}
			
			cek = $('#pbValue').val();
			if(cek==''){
				alert("Jumlah Jaminan Harus Diisi");
				return false;
			}
			$('#pbValue').val($('#pbValue').autoNumeric('get'));
			sendForm(document.procForm, "<s:url action="ajax/submitPb" />", "#listPb");
			jQuery.facebox.close();
			resetPb();
		}
		
		function resetPb(){
			$('#pbNumber').val('');
			$('#pbIssuedBy').val('');
			$('#pbIssuedDate').val('');
			$('#pbExpiredDate').val('');
			$('#pbValue').val('');
		}
		
		
		//regCancel Action
		function regCancel(){
			jQuery.facebox.close();
			window.location = "<s:url action="kontrak/draftKontrak" />"; 
		}
		
		//select tipe
		function selectTipeKontrak(obj){
			if(obj.value==1){
				$('#userLegal').hide();
				$('#btnLegal').hide();
				$('#btnSubmit').show();
			}else if(obj.value==2){
				$('#userLegal').show();
				$('#btnLegal').show();
				$('#btnSubmit').hide();
			}
		}
		/* $("#tgl1").live("change",function(){
			var tgl1 = $(this).val();
			var tgl2 = $("#tgl2").val();
			if(tgl1 != "" && tgl2 != "" && tgl2 < tgl1){
				$("#startAlert").show();
				$("#startAlert").html("Tanggal Mulai Harus Lebih Kecil dari Tanggal Berakhir");
				$(this).val("");
			}else{
				$("#startAlert").hide();
			}
		});
		
		$("#tgl2").live("change",function(){
			var tgl2 = $(this).val();
			var tgl1 = $("#tgl1").val();
			if(tgl1 != "" && tgl2 != "" && tgl2 < tgl1){
				$("#endAlert").show();
				$("#endAlert").html("Tanggal Berakhir Harus Lebih Besar dari Tanggal Berakhir");
				$(this).val("");
			}else{
				$("#endAlert").hide();
			}
		}); */
	</script>
	<style>caption{font-size: 15px; color: #FF9E0D; text-align: left;}</style>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="dashboard">Home</s:a></li> 
	        <li>Managemen Kontrak</li>  
	        <li><s:a action="kontrak/draftKontrak">Draft Pembuat Kontrak</s:a></li>
	        
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
    
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
					
			<s:form action="kontrak/draftKontrak" id="procForm" name="procForm" enctype="multipart/form-data" method="post">
				<s:hidden name="contractHeader.contractId" />
				<s:hidden name="contractHeader.contractAmount" id="contractAmount"></s:hidden>
				<div class="box">
					<div class="title">
						Header
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle" >
							<tr>
								<td width="150px">No. Kontrak</td>
								<td>
									${contractHeader.contractNumber }
								</td>
							</tr>
							<tr>
								<td width="150px">No. SPPH</td>
								<td>
									${contractHeader.prcMainHeader.ppmNomorRfq }
								</td>
							</tr>
							<tr>
								<td width="150px">No. PR</td>
								<td>
									${contractHeader.prcMainHeader.ppmNomorPr }
								</td>
							</tr>
							
							<tr>
								<td width="150px">Judul Kontrak</td>
								<td>
									<s:textfield name="contractHeader.contractSubject" cssClass="required"></s:textfield>
								</td>
							</tr>
							<tr>
								<td>Deskripsi Kontrak</td>
								<td>
									<s:textarea name="contractHeader.contractDescription" cssClass="required"></s:textarea>
								</td>
							</tr>
							<tr>
								<td>Jenis Kontrak</td>
								<td>
									<s:select list="#{'1':'PO', '2':'PO & Perjanjian'}" name="contractHeader.contractType" cssClass="required" headerKey="" headerValue="--Pilih Tipe--" onchange="selectTipeKontrak(this);" id="ctrType"></s:select> 
								</td>
							</tr>
							<tr>
								<td>Tanggal Kontrak Mulai</td>
								<td><s:textfield name="contractHeader.contractStart" id="tgl1" cssClass="required"></s:textfield>
									<label class="error" id="startAlert" style="display: none;"></label>
								</td>
							</tr>
							
							<tr>
								<td>Tanggal Penyerahan</td>
								<td><s:textfield name="contractHeader.tglPenyerahan" id="tgl3" cssClass="required"></s:textfield>
									<label class="error" id="serahAlert" style="display: none;"></label>
								</td>
							</tr>
							
							<tr>
								<td>Tanggal Kontrak Berakhir</td>
								<td><s:textfield name="contractHeader.contractEnd" id="tgl2" cssClass="required"></s:textfield>
									<label class="error" id="endAlert" style="display: none;"></label>
								</td>
							</tr>
							
							<tr>
								<td>Syarat Pembayaran</td>
								<td>
									<s:select list="#{
									'Z000':'Pembayaran Tunai', 
									'Z014':'Pembayaran 14 hari Setelah Billing Date',
									'Z030':'Pembayaran 30 Hari setelah Billing Date',
									'Z045':'Pembayaran 45 hari Setelah Billing Date',
									'Z060':'Pembayaran 60 hari Setelah Billing Date',
									'Z075':'Pembayaran 75 Hari Setelah Billing Date',
									'Z090':'Pembayaran 90 Hari Setelah Billing Date',
									'Z100':'Pembayaran 100 Hari setelah Billing Date',
									'Z120':'Pembayaran 120 Hari Setelah Billing Date'
									}" name="contractHeader.paymentTerm" cssClass="required" headerKey="" headerValue="--Pilih Syarat Pembayaran--" id="pymntTerm"></s:select> 
								</td>
							</tr>
							
							<tr>
								<td>Procurement Point</td>
								<td>
									<s:select list="listPurchasingOrganization" listKey="kodePurcOrg" headerValue="--Pilih ProcurementPoint--" listValue="deskripsiPlant" name="kodePurcOrg" cssClass="required" cssStyle="width:400px"></s:select> 
								</td>
							</tr>
							
							<tr>
								<td>Purchasing Group</td>
								<td>
									<s:select list="#{
										'101':'Umum',
										'102':'Spareparts',
										'103':'Raw Materials',
										'104':'Import',
										'105':'Jasa Teknik',
										'106':'Jasa Umum',
										'107':'Distribusi / STO'
									}" name="contractHeader.purchasingGroup" cssClass="required" headerKey="" headerValue="--Pilih Purchasing Group--" id="purchGroup"></s:select> 
								</td>
							</tr>
							
							<tr>
								<td width="150px">Attachment</td>
								<td>
									${contractHeader.prcMainHeader.attachment }
								</td>
							</tr>
							
							<tr>
								<td width="150px">Jumlah Kontrak</td>
								<td>
									<fmt:formatNumber> ${contractHeader.contractAmount }</fmt:formatNumber>
								</td>
							</tr>
							
							<tr>
								<td width="150px" style="color:red; font-weight: bolder" >Pemenang</td>
								<td>
									${fn:toUpperCase(contractHeader.vndHeader.vendorName) }
								</td>
							</tr>
							
							<c:if test="${contractHeader.ctrContractHeader!=null }">
								<tr>
									<td width="150px" style="color:red; font-weight: bolder" >Adendum Dari Kontrak</td>
									<td>
										${contractHeader.ctrContractHeader.contractNumber }
									</td>
								</tr>
								<tr>
									<td width="150px" style="color:red; font-weight: bolder" >Alasan ADENDUM</td>
									<td>
										${contractHeader.contractAmmendReason }
									</td>
								</tr>
							</c:if>
							
							<tr id="userLegal" style="display: none">
								<td width="150px;">User Legal</td>
								<td>
									<s:select list="listUser" listKey="id" listValue="completeName" name="contractHeader.userLegal.id" cssClass="chzn" cssStyle="width:400px"></s:select>
								</td>
							</tr>
						</table>
						
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Item
						<span class="hide"></span>
					</div>
					<div class="content center">
												
						<s:set id="i" value="0"></s:set>
						<display:table name="${listContractItem }" id="s" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
							<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
							<display:column title="Kode" style="width: 70px;" class="center">${s.itemCode }</display:column>
							<display:column title="Cost Center" style="width: 150px;" class="center">
								${s.itemCostCenter }
							</display:column>
							<display:column title="Deskripsi" class="left">${s.itemDescription }</display:column>
							<display:column title="Jumlah" style="text-align:right">
								<fmt:formatNumber> ${s.itemQuantityOe }</fmt:formatNumber>
							</display:column>
							<display:column title="Harga Satuan" style="text-align:right">
								<fmt:formatNumber> ${s.itemPriceOe }</fmt:formatNumber>
							</display:column>
							<display:column title="Pajak (%)" style="text-align:right">
								${s.itemTaxOe }
								<input type="hidden" value="${s.itemTaxOe }" id="itemTaxOe-${i }">
							</display:column>
							<display:column title="Sub Total" style="text-align:right">
								<fmt:formatNumber> ${s.itemTotalPriceOe }</fmt:formatNumber>
								<input type="hidden" value="${s.itemTotalPriceOe }" id="itemTotalPriceOe-${i }">
							</display:column>
							<display:column title="Total + Pajak" style="text-align:right">
								<script type="text/javascript">
									$(document).ready(function(){
										var totalAllOe = 'totaloe-'+${i};
										$('#'+totalAllOe).autoNumeric('init');
										var itax2 = parseFloat($('#itemTaxOe-${i }').val());
										var itotal2 = parseFloat($('#itemTotalPriceOe-${i }').val());

										if(!isNaN(itax2) && !isNaN(itotal2)){
											$('#'+totalAllOe).autoNumeric('set',(itax2 / 100 * itotal2 + itotal2));
										}
										
										
									});
									
								</script>
								<input type="text" readonly="readonly" id="totaloe-${i }" style="width:inherit;text-align:right"/>
							</display:column>
							<display:footer>
							<tr>
								<td colspan="8" style="text-align: right">Total</td>
								<td style="text-align:right">
									<input type="text" style="width:inherit;text-align:right" readonly="readonly" value="<fmt:formatNumber>${contractHeader.contractAmount}</fmt:formatNumber>"/>
								</td>
							</tr>
							
						</display:footer>
							
						<s:set var="i" value="#i+1"></s:set>
						
						</display:table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Pembuat Kontrak
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle">
							<tr>
								<td width="150px;">Pembuat Kontrak</td>
								<td>
									${fn:toUpperCase(contractHeader.admUserByContractCreator.completeName)}
								</td>
							</tr>						
						</table>
					</div>
				</div>

				
				<div class="box">
					<div class="title">
						Termin
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form fomStyle1">
							<tr>
								<td width="150px">Termin</td>
								<td>
									<s:textfield name="contractTop.topDescription"  id="topDescription"></s:textfield>
								</td>
							</tr>
							<tr>
								<td>Persentase</td>
								<td>
									<s:textfield name="contractTop.topPercentage"  id="topPercentage" onkeyup="calculateTermin();"></s:textfield>
								</td>
							</tr>
							<tr>
								<td>Nilai Termin</td>
								<td>
									<s:textfield name="contractTop.topValue"  readonly="true" id="topValue"></s:textfield>
								</td>
							</tr>
							<tr>
								<td></td>
								<td><a href="javascript:void(0)" onclick='tambahTop();' class="uibutton">Tambah Termin</a> </td>
							</tr>
						</table>
						<div id="listTop">
							<s:include value="ajax/listTop.jsp" />
						</div>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Jaminan Pelaksanaan
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form fomStyle1">
							<tr>
								<td width="150px">Nomor</td>
								<td>
									<s:textfield name="contractPb.pbNumber"  id="pbNumber"></s:textfield>
								</td>
							</tr>
							<tr>
								<td>Dikeluarkan Oleh</td>
								<td>
									<s:textfield name="contractPb.pbIssuedBy" id="pbIssuedBy"></s:textfield>
								</td>
							</tr>
							<tr>
								<td>Tanggal Dibuat</td>
								<td>
									<s:textfield name="contractPb.pbIssuedDate"  id="pbIssuedDate"></s:textfield>
								</td>
							</tr>
							
							<tr>
								<td>Tanggal Expire</td>
								<td>
									<s:textfield name="contractPb.pbExpiredDate"  id="pbExpiredDate"></s:textfield>
								</td>
							</tr>
							<tr>
								<td width="150px">Nilai</td>
								<td>
									<s:textfield name="contractPb.pbAmount" id="pbValue"></s:textfield>
								</td>
							</tr>
							
							<tr>
								<td></td>
								<td><a href="javascript:void(0)" onclick='tambahPb();' class="uibutton">Tambah Jaminan</a> </td>
							</tr>
						</table>
						<div id="listPb">
							<s:include value="ajax/listPb.jsp" />
						</div>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Daftar Komentar
						<span class="hide"></span>
					</div>
					<div class="content center">
						<display:table name="${listContractComment }" id="k" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
							<display:column title="No." style="width: 20px; text-align: center;">${k_rowNum}</display:column>
							<display:column title="Nama" style="width: 100px;" class="center">${k.username }</display:column>
							<display:column title="Posisi" style="width: 100px;" class="center">${k.posisi }</display:column>
							<display:column title="Proses" style="width: 100px;" class="center">${k.prosesAksi }</display:column>
							<display:column title="Tgl Pembuatan" style="width: 100px;" class="center">
								<fmt:formatDate value="${k.createdDate }" pattern="dd.MM.yyyy HH:mm"/> 
							</display:column>
							<display:column title="Tgl Update" style="width: 100px;" class="center">
								<fmt:formatDate value="${k.updatedDate }" pattern="dd.MM.yyyy HH:mm"/> 
							</display:column>
							<display:column title="Aksi" class="center" style="width: 250px;">${k.aksi }</display:column>
							<display:column title="Komentar" class="center" style="width: 250px;">${k.comment }</display:column>
							<display:column title="Dokumen" class="center" style="width: 200px;">
								<a href="${contextPath }/upload/file/${k.document }" target="_blank">${k.document }</a>
							</display:column>
						</display:table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Komentar
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle">
							<tr>
								<td width="150px;">Komentar (*)</td>
								<td><s:textarea name="ctrContractComment.comment" cssClass="required" id="komentarVal"></s:textarea> </td>
							</tr>	
							<tr>
								<td width="150px;">Lampiran</td>
								<td><s:file name="docsKomentar" cssClass="uibutton" /></td>
							</tr>							
						</table>
					</div>
				</div>
				
				<div class="uibutton-toolbar extend" >
					<s:submit name="legal" id="btnLegal" onclick="actionAlert('btnLegal','Ke Bagian Legal');return false;" value="Ke Bagian Legal" cssClass="uibutton" cssStyle="color:red"></s:submit>
					<s:submit name="saveAndFinish" id="btnSubmit" onclick="actionAlert('btnSubmit','simpan dan selesai');return false;" value="Simpan dan Selesai" cssClass="uibutton" ></s:submit>
					<a href="javascript:void(0)" class="uibutton" onclick="confirmPopUp('regCancel();', 'Batal', 'Apakah anda yakin untuk membatalkan transaksi ?', 'Ya', 'Tidak');"><s:text name="cancel"></s:text> </a>					
				</div>
				
						
			</s:form>
			
		</div>
	</div>
</body>
</html>