<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Pemilihan Pembuat Kontrak</title>
	
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	
	<script type="text/javascript">	
		$(document).ready(function(){
			$(".chzn").chosen();
			
			// FORM VALIDATION
			$("#procForm").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler:function(form){
					FreezeScreen('Data Sedang di Proses');
					form.submit();
				}
			});
			
			
		});
		
		//cek value
		function cekValue(obj){
			if(obj.checked){
				obj.value=1;
			}else{
				obj.value=0;
			}
		}
		
		//regCancel Action
		function regCancel(){
			jQuery.facebox.close();
			window.location = "<s:url action="kontrak/pembuatKontrak" />"; 
		}
		
	</script>
	<style>caption{font-size: 15px; color: #FF9E0D; text-align: left;}</style>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="dashboard">Home</s:a></li> 
	        <li>Managemen Kontrak</li>  
	        <li><s:a action="kontrak/pembuatKontrak">Pemilihan Pembuat Kontrak</s:a></li>
	        
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
    
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
					
			<s:form action="kontrak/pembuatKontrak" id="procForm" name="procForm" enctype="multipart/form-data" method="post">
				<s:hidden name="contractHeader.contractId" />
				
				<div class="box">
					<div class="title">
						Header
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle" >
							<tr>
								<td width="150px">No. Kontrak</td>
								<td>
									${contractHeader.contractNumber }
								</td>
							</tr>
							<tr>
								<td width="150px">No. SPPH</td>
								<td>
									${contractHeader.prcMainHeader.ppmNomorRfq }
								</td>
							</tr>
							<tr>
								<td width="150px">No. PR</td>
								<td>
									${contractHeader.prcMainHeader.ppmNomorPr }
								</td>
							</tr>
							
							<tr>
								<td width="150px">Judul Permintaan</td>
								<td>${contractHeader.prcMainHeader.ppmSubject }
								</td>
							</tr>
							<tr>
								<td>Deskripsi Permintaan</td>
								<td>${contractHeader.prcMainHeader.ppmDescription } </td>
							</tr>
							<tr>
								<td>Jenis Permintaan</td>
								<td>
									<c:if test="${contractHeader.prcMainHeader.ppmJenis.id == 1 }">Barang</c:if>
									<c:if test="${contractHeader.prcMainHeader.ppmJenis.id == 2 }">Jasa</c:if>
								</td>
							</tr>
							
							<tr>
								<td>Prioritas</td>
								<td>
									<c:if test="${contractHeader.prcMainHeader.ppmPrioritas == 1 }">High</c:if>
									<c:if test="${contractHeader.prcMainHeader.ppmPrioritas == 2 }">Normal</c:if>
									<c:if test="${contractHeader.prcMainHeader.ppmPrioritas == 3 }">Low</c:if>
								</td>
							</tr>
							<tr>
								<td width="150px">Attachment</td>
								<td>
									${contractHeader.prcMainHeader.attachment }
								</td>
							</tr>
							
							<tr>
								<td width="150px" style="color:red; font-weight: bolder" >Pemenang</td>
								<td>
									${fn:toUpperCase(contractHeader.vndHeader.vendorName) }
								</td>
							</tr>
							
							<c:if test="${contractHeader.ctrContractHeader!=null }">
								<tr>
									<td width="150px" style="color:red; font-weight: bolder" >Adendum Dari Kontrak</td>
									<td>
										${contractHeader.ctrContractHeader.contractNumber }
									</td>
								</tr>
								<tr>
									<td width="150px" style="color:red; font-weight: bolder" >Alasan ADENDUM</td>
									<td>
										${contractHeader.contractAmmendReason }
									</td>
								</tr>
							</c:if>

						</table>
						
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Item
						<span class="hide"></span>
					</div>
					<div class="content center">
												
						<s:set id="i" value="0"></s:set>
						<display:table name="${listContractItem }" id="s" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
							<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
							<display:column title="Kode" style="width: 70px;" class="center">${s.itemCode }</display:column>
							<display:column title="Cost Center" style="width: 150px;" class="center">
								${s.itemCostCenter }
							</display:column>
							<display:column title="Deskripsi" class="left">${s.itemDescription }</display:column>
							<display:column title="Jumlah" style="text-align:right">
								<fmt:formatNumber> ${s.itemQuantityOe }</fmt:formatNumber>
							</display:column>
							<display:column title="Harga Satuan" style="text-align:right">
								<fmt:formatNumber> ${s.itemPriceOe }</fmt:formatNumber>
							</display:column>
							<display:column title="Pajak (%)" style="text-align:right">
								${s.itemTaxOe }
								<input type="hidden" value="${s.itemTaxOe }" id="itemTaxOe-${i }">
							</display:column>
							<display:column title="Sub Total" style="text-align:right">
								<fmt:formatNumber> ${s.itemTotalPriceOe }</fmt:formatNumber>
								<input type="hidden" value="${s.itemTotalPriceOe }" id="itemTotalPriceOe-${i }">
							</display:column>
							<display:column title="Total + Pajak" style="text-align:right">
								<script type="text/javascript">
									$(document).ready(function(){
										var totalAllOe = 'totaloe-'+${i};
										$('#'+totalAllOe).autoNumeric('init');
										var itax2 = parseFloat($('#itemTaxOe-${i }').val());
										var itotal2 = parseFloat($('#itemTotalPriceOe-${i }').val());

										if(!isNaN(itax2) && !isNaN(itotal2)){
											$('#'+totalAllOe).autoNumeric('set',(itax2 / 100 * itotal2 + itotal2));
										}
										
										
									});
									
								</script>
								<input type="text" readonly="readonly" id="totaloe-${i }" style="width:inherit;text-align:right"/>
							</display:column>
							<display:footer>
								<tr>
									<td colspan="9" style="color:red"></td>
								</tr>
							</display:footer>
							
						<s:set var="i" value="#i+1"></s:set>
						<display:footer>
							<tr>
								<td colspan="8" style="text-align: right">Total</td>
								<td style="text-align:right">
									<input type="text" style="width:inherit;text-align:right" readonly="readonly" value="<fmt:formatNumber>${contractHeader.contractAmount}</fmt:formatNumber>"/>
								</td>
							</tr>
							
						</display:footer>
						</display:table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Pembuat Kontrak
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle">
							<tr>
								<td width="150px;">Pembuat Kontrak</td>
								<td>
									<s:select list="listUser" listKey="id" listValue="completeName" name="contractHeader.admUserByContractCreator.id" cssClass="chzn" cssStyle="width:400px"></s:select>
								</td>
							</tr>						
						</table>
					</div>
				</div>

				<div class="box">
					<div class="title">
						Daftar Komentar
						<span class="hide"></span>
					</div>
					<div class="content center">
						<display:table name="${listContractComment }" id="k" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
							<display:column title="No." style="width: 20px; text-align: center;">${k_rowNum}</display:column>
							<display:column title="Nama" style="width: 100px;" class="center">${k.username }</display:column>
							<display:column title="Posisi" style="width: 100px;" class="center">${k.posisi }</display:column>
							<display:column title="Proses" style="width: 100px;" class="center">${k.prosesAksi }</display:column>
							<display:column title="Tgl Pembuatan" style="width: 100px;" class="center">
								<fmt:formatDate value="${k.createdDate }" pattern="dd.MM.yyyy HH:mm"/> 
							</display:column>
							<display:column title="Tgl Update" style="width: 100px;" class="center">
								<fmt:formatDate value="${k.updatedDate }" pattern="dd.MM.yyyy HH:mm"/> 
							</display:column>
							<display:column title="Aksi" class="center" style="width: 250px;">${k.aksi }</display:column>
							<display:column title="Komentar" class="center" style="width: 250px;">${k.comment }</display:column>
							<display:column title="Dokumen" class="center" style="width: 200px;">
								<a href="${contextPath }/upload/file/${k.document }" target="_blank">${k.document }</a>
							</display:column>
						</display:table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Komentar
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle">
							<tr>
								<td width="150px;">Komentar (*)</td>
								<td><s:textarea name="ctrContractComment.comment" cssClass="required" id="komentarVal"></s:textarea> </td>
							</tr>	
							<tr>
								<td width="150px;">Lampiran</td>
								<td><s:file name="docsKomentar" cssClass="uibutton" /></td>
							</tr>							
						</table>
					</div>
				</div>
				
				<div class="uibutton-toolbar extend" >
					<s:submit name="saveAndFinish" id="saveAndFinish" onclick="actionAlert('saveAndFinish','simpan dan selesai');return false;" value="Simpan dan Selesai" cssClass="uibutton" ></s:submit>
					<a href="javascript:void(0)" class="uibutton" onclick="confirmPopUp('regCancel();', 'Batal', 'Apakah anda yakin untuk membatalkan transaksi ?', 'Ya', 'Tidak');"><s:text name="cancel"></s:text> </a>					
				</div>
						
			</s:form>
			
		</div>
	</div>
</body>
</html>