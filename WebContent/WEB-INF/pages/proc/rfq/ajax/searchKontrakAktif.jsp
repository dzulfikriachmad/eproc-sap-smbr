<%@ include file="/include/definitions.jsp"%>
<script type="text/javascript">
				   $(document).ready(function() {
					// DATATABLE
					 $('table.doc').dataTable({
				        "bInfo": false,
				        "iDisplayLength": 10,
				        "aLengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
				        "sPaginationType": "full_numbers",
				        "bPaginate": true,
				        "sDom": '<f>t<pl>'
				        });
					});
			</script>

<s:form action="kontrak/daftarKontrakAktif">
	<s:hidden name="contractHeader.contractId" id="ctrId" />
	<display:table name="${listContractHeader }" id="s" excludedParams="*"
		style="width: 100%;" class="doc style1">

		<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
		<display:column title="Nomor Kontrak " total="true"
			style="width: 200px;">${s.contractNumber } </display:column>
		<display:column title="Nomor PR" total="true"
			style="width: 200px;">${s.prcMainHeader.ppmNomorPr}</display:column>
		<display:column title="Nama Kontrak" style="width: 150px; "
			class="center">${s.contractSubject }</display:column>
		<display:column title="Pemenang" style="width: 150px; "
			class="center">${fn:toUpperCase(s.vndHeader.vendorName)}</display:column>
		<display:column title="Tanggal" class="center" style="width: 200px;">
			<fmt:formatDate value="${s.createdDate }"
				pattern="HH:mm:ss, dd MMM yyyy" />
		</display:column>
		<display:column title="Status">
			<c:if test="${s.contractProsesLevel==99 }">Aktif</c:if>
			<c:if test="${s.contractProsesLevel==-98 }">Adendum</c:if>
		</display:column>
		<display:column title="Tanggal Mulai" class="center" style="width: 200px;">
			<fmt:formatDate value="${s.contractStart }"
				pattern="dd MMM yyyy" />
		</display:column>
		<display:column title="Tanggal Berakhir" class="center" style="width: 200px;">
			<fmt:formatDate value="${s.contractEnd }"
				pattern="dd MMM yyyy" />
		</display:column >
		<display:column title="Nomor Kontrak SAP" class="center" style="width:200px">
			${s.contractNumberSap }
		</display:column>
		<%-- <display:column title="Nomor O1/ O2 JDE" class="center" style="width:200px">
			${s.jdeNumber }
		</display:column> --%>
		<%-- <display:column title="Status" class="center" style="width:50px">
			<c:if test="${s.statusGlobal==1 }">
				<div style="background-color: blue; width: inherit" >&nbsp;</div>
			</c:if>
			<c:if test="${s.statusGlobal==2 }">
				<div style="background-color:green;width: inherit" >&nbsp;</div>
			</c:if>
			<c:if test="${s.statusGlobal==3 }">
				<div style="background-color:orange;width: inherit" >&nbsp;</div>
			</c:if>
			<c:if test="${s.statusGlobal==4 }">
				<div style="background-color:red;width: inherit" >&nbsp;</div>
			</c:if>
		</display:column> --%>
		<%-- <display:column title="Status Tagihan" class="center">
			<c:if test="${fn:trim(s.statusGlobal)=='H' }">Voucher Barang Terbit</c:if>
			<c:if test="${fn:trim(s.statusGlobal)=='V' }">Aproved Verifikasi</c:if>
			<c:if test="${fn:trim(s.statusGlobal)=='A' }">Approved Bendahara</c:if>
			<c:if test="${fn:trim(s.
			statusGlobal)=='P' }">Sudah Dibayar</c:if>
		</display:column> --%>
		<display:column title="Nomor Kontrak SAP" class="center" style="width:200px">
			${s.contractNumberSap }
		</display:column>
		
		<display:column title="Aksi" class="center" style="width: 100px;">
		<c:if test="${s.contractNumberSap==null}">
			<input type="submit" name="viewsap" value="Push PO SAP"onclick="setValue('${s.contractId }');loadingScreen()"class="uibutton" style="width:120px;padding-bottom: 5px;padding-top: 5px"/>
		</c:if>
			<input type="submit" name="proses" value="Lihat"onclick="setValue('${s.contractId }');loadingScreen()"class="uibutton" style="width:120px;padding-bottom: 5px;padding-top: 5px"/>
			<input type="submit" name="adendum" value="Adendum"onclick="setValue('${s.contractId }');loadingScreen()"class="uibutton" style="width:120px;padding-bottom: 5px;padding-top: 5px"/>
			<s:url action="ajax/formUpdateTanggalKontrak" var="set2"/>
			<input type="button"  value="Update Tanggal" onclick="load_into_box('${set2}','contractHeader.contractId=${s.contractId }');" class="uibutton" style="width:120px;padding-bottom: 5px;padding-top: 5px"/>
			<input type="submit" name="viewclose" value="Close Kontrak"onclick="setValue('${s.contractId }');loadingScreen()"class="uibutton" style="width:120px;padding-bottom: 5px;padding-top: 5px; color: red"/>
		</display:column>
	</display:table>
</s:form>
