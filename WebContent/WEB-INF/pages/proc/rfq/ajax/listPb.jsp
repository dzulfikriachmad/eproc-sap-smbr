<%@ include file="/include/definitions.jsp"%>
<center>
	<!-- Alert -->
	<s:if test="hasActionErrors()">
		<div class="message red">
			<s:actionerror />
		</div>
	</s:if>
	<s:if test="hasActionMessages()">
		<div class="message green">
			<s:actionmessage />
		</div>
	</s:if>

	<div style="margin-top: 20px;"></div>
	<s:set id="i" value="0"></s:set>
	<display:table name="${listContractPb }" id="s" pagesize="100"
		excludedParams="*" style="width: 100%;" class="style1">
		<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
		<display:column title="Nomor" class="center">${s.pbNumber }</display:column>
		<display:column title="Issued Oleh" class="center">${s.pbIssuedBy }</display:column>
		<display:column title="Tgl Mulai" class="center">
			<fmt:formatDate value="${s.pbIssuedDate }" pattern="dd.MM.yyyy"/>	
		</display:column>
		<display:column title="Tgl Berakhir" class="center">
			<fmt:formatDate value="${s.pbExpiredDate }" pattern="dd.MM.yyyy"/> 
		</display:column>
		<display:column title="Nomor" class="center">${s.pbNumber }</display:column>
		<display:column title="Nilai" style="width: 150px;text-align:right">
		 	<fmt:formatNumber> ${s.pbAmount }</fmt:formatNumber>
		</display:column>
		<display:column title="Aksi" media="html" style="width: 30px;"
			class="center">
			<div class="uibutton-group">
				<s:url var="set" action="ajax/submitPb" />
				<a title="Hapus data PB" class="uibutton"
					href='javascript:void(0)'
					onclick='confirmPopUp("deleteObj(\"${set}\", \"act=delete&index=${i }&id=${s.id}\", \"#listPb\")", "Hapus Data Item", "Yakin ingin menghapus?", "Hapus", "Batal");'>X</a>
			</div>
		</display:column>
		<s:set var="i" value="#i+1"></s:set>
	</display:table>
</center>
