			<%@ include file="/include/definitions.jsp" %>
			<center>
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>		
			
			<div style="margin-top: 20px;"></div>
			<s:set id="i" value="0"></s:set>
			<display:table name="${listTabulasi }" id="s" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
				<c:if test="${s.statusTeknis==1 and s.statusHarga==1}">	
					<display:column title="No." style="width: 20px; text-align: center;background-color:#7CFAE7" >${s_rowNum}</display:column>
					<display:column title="Penawaran Mitra Kerja" style="width: 100px;background-color:#7CFAE7" class="center">
						<s:url var="set5" action="ajax/viewPenawaranRead" />
						<a title="Penawaran"  href='javascript:void(0)' onclick='load_into_box("${set5}", "prcMainHeader.ppmId=${s.ppmId }&prcMainVendor.id.ppmId=${s.ppmId}&prcMainVendor.id.vendorId=${s.vendorId}");'>${fn:toUpperCase(s.vendorName) }</a>
					</display:column>
					<display:column title="Total Penawaran" style="background-color:#7CFAE7">
						<c:if test="${s.statusHarga==1 }">
							LAMA :
							<fmt:formatNumber>
								${s.totalPenawaran }
							</fmt:formatNumber>
							<hr/>
							AUCTION :
							<fmt:formatNumber>
								${s.totalPenawaranEauction }
							</fmt:formatNumber>
							<hr/>
							NEGO :
							<fmt:formatNumber>
								${s.totalPenawaranNego }
							</fmt:formatNumber>
						</c:if>
					</display:column>
					<display:column title="Administrasi" style="background-color:#7CFAE7">
						<c:if test="${s.pmpAdmStatus==1 }">Lulus Administrasi</c:if>
						<c:if test="${s.pmpAdmStatus==-1 }">Tidak Lulus Administrasi</c:if>
					</display:column>
					<display:column title="Status Teknis" class="center" style="background-color:#7CFAE7">
						<c:if test="${s.statusTeknis==1 }">Lulus Teknis</c:if>
						<c:if test="${s.statusTeknis==-1 }">Tidak Lulus Teknis</c:if>
					</display:column>
					<display:column title="Passing Grade" class="center" style="background-color:#7CFAE7">
						${s.passingGrade }
					</display:column>
					<display:column title="Bobot Teknis" class="center" style="background-color:#7CFAE7">
						${s.bobotTeknis }
					</display:column>
					<display:column title="Nilai Teknis" class="center" style="background-color:#7CFAE7">
						<a href="javascript:void(0)" onclick="load_into_box('<s:url action="ajax/rfq/viewEvaluasi"/>','prcTemplate.id=${prcMainHeader.prcTemplate.id}&quote.id.vendorId=${s.vendorId }&quote.id.ppmId=${s.ppmId }&prcMainHeader.ppmId=${s.ppmId }&prcMainVendor.id.ppmId=${s.ppmId }&prcMainVendor.id.vendorId=${s.vendorId }');">${s.nilaiTeknis }</a>
						
					</display:column>
					<display:column title="Catatan Teknis" style="background-color:#7CFAE7">
						${s.catatanTeknis }
					</display:column>
					<display:column title="Status Harga" class="center" style="background-color:#7CFAE7">
						<c:if test="${s.statusHarga==1 }">Lulus Harga</c:if>
						<c:if test="${s.statusHarga==-1 }">Tidak Lulus Harga</c:if>
					</display:column>
					<display:column title="Bobot Harga" class="center" style="background-color:#7CFAE7">
						${s.bobotHarga }
					</display:column>
					<display:column title="Nilai Harga" class="center" style="background-color:#7CFAE7">
						<c:if test="${s.statusTeknis==1 }">
							<a href="javascript:void(0)" onclick="load_into_box('<s:url action="ajax/rfq/viewEvaluasiHarga"/>','prcTemplate.id=${prcMainHeader.prcTemplate.id}&quote.id.vendorId=${s.vendorId }&quote.id.ppmId=${s.ppmId }&prcMainHeader.ppmId=${s.ppmId }&prcMainVendor.id.ppmId=${s.ppmId }&prcMainVendor.id.vendorId=${s.vendorId }');">${s.nilaiHarga }</a>
						</c:if>
						<c:if test="${s.statusTeknis==-1 }">
							${s.nilaiHarga }
						</c:if>
						<c:if test="${s.statusTeknis==0 }">
							${s.nilaiHarga }
						</c:if>
						
					</display:column>
					<display:column title="Catatan Harga" style="background-color:#7CFAE7">
						${s.catatanHarga }
					</display:column>
					<display:column title="Total Nilai" style="background-color:#7CFAE7">
						<fmt:formatNumber>
							${s.totalNilai }
						</fmt:formatNumber>
					</display:column>
					
				</c:if>
				<c:if test="${s.statusTeknis==0 and s.statusHarga==0 }">
							<display:column title="No." style="width: 20px; text-align: center;background-color:#7CFAE7" >${s_rowNum}</display:column>
					<display:column title="Penawaran Mitra Kerja" style="width: 100px;background-color:#7CFAE7" class="center">
						<a href="javascript:void(0)" onclick="load_into_box('<s:url action="ajax/vendor/detail"/>','tmpVndHeader.vendorId=${s.vendorId }')">${fn:toUpperCase(s.vendorName) }</a>
					</display:column>
					<display:column title="Total Penawaran" style="background-color:#7CFAE7">
						<c:if test="${s.statusHarga==1 }">
							LAMA :
							<fmt:formatNumber>
								${s.totalPenawaran }
							</fmt:formatNumber>
							<hr/>
							AUCTION :
							<fmt:formatNumber>
								${s.totalPenawaranEauction }
							</fmt:formatNumber>
							<hr/>
							NEGO :
							<fmt:formatNumber>
								${s.totalPenawaranNego }
							</fmt:formatNumber>
						</c:if>
					</display:column>
					<display:column title="Administrasi" style="background-color:#7CFAE7">
						<c:if test="${s.pmpAdmStatus==1 }">Lulus Administrasi</c:if>
						<c:if test="${s.pmpAdmStatus==-1 }">Tidak Lulus Administrasi</c:if>
					</display:column>
					<display:column title="Status Teknis" class="center" style="background-color:#7CFAE7">
						<c:if test="${s.statusTeknis==1 }">Lulus Teknis</c:if>
						<c:if test="${s.statusTeknis==-1 }">Tidak Lulus Teknis</c:if>
					</display:column>
					<display:column title="Passing Grade" class="center" style="background-color:#7CFAE7">
						${s.passingGrade }
					</display:column>
					<display:column title="Bobot Teknis" class="center" style="background-color:#7CFAE7">
						${s.bobotTeknis }
					</display:column>
					<display:column title="Nilai Teknis" class="center" style="background-color:#7CFAE7">
						<a href="javascript:void(0)" onclick="load_into_box('<s:url action="ajax/rfq/viewEvaluasi"/>','prcTemplate.id=${prcMainHeader.prcTemplate.id}&quote.id.vendorId=${s.vendorId }&quote.id.ppmId=${s.ppmId }&prcMainHeader.ppmId=${s.ppmId }&prcMainVendor.id.ppmId=${s.ppmId }&prcMainVendor.id.vendorId=${s.vendorId }');">${s.nilaiTeknis }</a>
						
					</display:column>
					<display:column title="Catatan Teknis" style="background-color:#7CFAE7">
						${s.catatanTeknis }
					</display:column>
					<display:column title="Status Harga" class="center" style="background-color:#7CFAE7">
						<c:if test="${s.statusHarga==1 }">Lulus Harga</c:if>
						<c:if test="${s.statusHarga==-1 }">Tidak Lulus Harga</c:if>
					</display:column>
					<display:column title="Bobot Harga" class="center" style="background-color:#7CFAE7">
						${s.bobotHarga }
					</display:column>
					<display:column title="Nilai Harga" class="center" style="background-color:#7CFAE7">
						<c:if test="${s.statusTeknis==1 }">
							<a href="javascript:void(0)" onclick="load_into_box('<s:url action="ajax/rfq/viewEvaluasiHarga"/>','prcTemplate.id=${prcMainHeader.prcTemplate.id}&quote.id.vendorId=${s.vendorId }&quote.id.ppmId=${s.ppmId }&prcMainHeader.ppmId=${s.ppmId }&prcMainVendor.id.ppmId=${s.ppmId }&prcMainVendor.id.vendorId=${s.vendorId }');">${s.nilaiHarga }</a>
						</c:if>
						<c:if test="${s.statusTeknis==-1 }">
							${s.nilaiHarga }
						</c:if>
						<c:if test="${s.statusTeknis==0 }">
							${s.nilaiHarga }
						</c:if>
						
					</display:column>
					<display:column title="Catatan Harga" style="background-color:#7CFAE7">
						${s.catatanHarga }
					</display:column>
					<display:column title="Total Nilai" style="background-color:#7CFAE7">
						<fmt:formatNumber>
							${s.totalNilai }
						</fmt:formatNumber>
					</display:column>
					
				</c:if>
				<c:if test="${s.statusTeknis==1 and s.statusHarga==-1 }">	
					<display:column title="No." style="width: 20px; text-align: center;background-color:#F99EA4" >${s_rowNum}</display:column>
					<display:column title="Penawaran Mitra Kerja" style="width: 100px;background-color:#F99EA4" class="center">
						<s:url var="set5" action="ajax/viewPenawaranRead" />
						<a title="Penawaran"  href='javascript:void(0)' onclick='load_into_box("${set5}", "prcMainHeader.ppmId=${s.ppmId }&prcMainVendor.id.ppmId=${s.ppmId}&prcMainVendor.id.vendorId=${s.vendorId}");'>${fn:toUpperCase(s.vendorName) }</a>
					</display:column>
					<display:column title="Total Penawaran" style="background-color:#F99EA4">
						<c:if test="${s.statusHarga==1 }">
							LAMA :
							<fmt:formatNumber>
								${s.totalPenawaran }
							</fmt:formatNumber>
							<hr/>
							AUCTION :
							<fmt:formatNumber>
								${s.totalPenawaranEauction }
							</fmt:formatNumber>
							<hr/>
							NEGO :
							<fmt:formatNumber>
								${s.totalPenawaranNego }
							</fmt:formatNumber>
						</c:if>
					</display:column>
					<display:column title="Administrasi" style="background-color:#F99EA4">
						<c:if test="${s.pmpAdmStatus==1 }">Lulus Administrasi</c:if>
						<c:if test="${s.pmpAdmStatus==-1 }">Tidak Lulus Administrasi</c:if>
					</display:column>
					<display:column title="Status Teknis" class="center" style="background-color:#F99EA4">
						<c:if test="${s.statusTeknis==1 }">Lulus Teknis</c:if>
						<c:if test="${s.statusTeknis==-1 }">Tidak Lulus Teknis</c:if>
					</display:column>
					<display:column title="Passing Grade" class="center" style="background-color:#F99EA4">
						${s.passingGrade }
					</display:column>
					<display:column title="Bobot Teknis" class="center" style="background-color:#F99EA4">
						${s.bobotTeknis }
					</display:column>
					<display:column title="Nilai Teknis" class="center" style="background-color:#F99EA4">
						<a href="javascript:void(0)" onclick="load_into_box('<s:url action="ajax/rfq/viewEvaluasi"/>','prcTemplate.id=${prcMainHeader.prcTemplate.id}&quote.id.vendorId=${s.vendorId }&quote.id.ppmId=${s.ppmId }&prcMainHeader.ppmId=${s.ppmId }&prcMainVendor.id.ppmId=${s.ppmId }&prcMainVendor.id.vendorId=${s.vendorId }');">${s.nilaiTeknis }</a>
						
					</display:column>
					<display:column title="Catatan Teknis" style="background-color:#F99EA4">
						${s.catatanTeknis }
					</display:column>
					<display:column title="Status Harga" class="center" style="background-color:#F99EA4">
						<c:if test="${s.statusHarga==1 }">Lulus Harga</c:if>
						<c:if test="${s.statusHarga==-1 }">Tidak Lulus Harga</c:if>
					</display:column>
					<display:column title="Bobot Harga" class="center" style="background-color:#F99EA4">
						${s.bobotHarga }
					</display:column>
					<display:column title="Nilai Harga" class="center" style="background-color:#F99EA4">
						<c:if test="${s.statusTeknis==1 }">
							<a href="javascript:void(0)" onclick="load_into_box('<s:url action="ajax/rfq/viewEvaluasiHarga"/>','prcTemplate.id=${prcMainHeader.prcTemplate.id}&quote.id.vendorId=${s.vendorId }&quote.id.ppmId=${s.ppmId }&prcMainHeader.ppmId=${s.ppmId }&prcMainVendor.id.ppmId=${s.ppmId }&prcMainVendor.id.vendorId=${s.vendorId }');">${s.nilaiHarga }</a>
						</c:if>
						<c:if test="${s.statusTeknis==-1 }">
							${s.nilaiHarga }
						</c:if>
						<c:if test="${s.statusTeknis==0 }">
							${s.nilaiHarga }
						</c:if>
						
					</display:column>
					<display:column title="Catatan Harga" style="background-color:#F99EA4">
						${s.catatanHarga }
					</display:column>
					<display:column title="Total Nilai" style="background-color:#F99EA4">
						<fmt:formatNumber>
							${s.totalNilai }
						</fmt:formatNumber>
					</display:column>
					
				</c:if>
				<c:if test="${s.statusTeknis==-1 and s.statusHarga==-1 }">	
					<display:column title="No." style="width: 20px; text-align: center;background-color:#F99EA4" >${s_rowNum}</display:column>
					<display:column title="Penawaran Mitra Kerja" style="width: 100px;background-color:#F99EA4" class="center">
						<s:url var="set5" action="ajax/viewPenawaranRead" />
						<a title="Penawaran"  href='javascript:void(0)' onclick='load_into_box("${set5}", "prcMainHeader.ppmId=${s.ppmId }&prcMainVendor.id.ppmId=${s.ppmId}&prcMainVendor.id.vendorId=${s.vendorId}");'>${fn:toUpperCase(s.vendorName) }</a>
					</display:column>
					<display:column title="Total Penawaran" style="background-color:#F99EA4">
						<c:if test="${s.statusHarga==1 }">
							LAMA :
							<fmt:formatNumber>
								${s.totalPenawaran }
							</fmt:formatNumber>
							<hr/>
							AUCTION :
							<fmt:formatNumber>
								${s.totalPenawaranEauction }
							</fmt:formatNumber>
							<hr/>
							NEGO :
							<fmt:formatNumber>
								${s.totalPenawaranNego }
							</fmt:formatNumber>
						</c:if>
					</display:column>
					<display:column title="Administrasi" style="background-color:#F99EA4">
						<c:if test="${s.pmpAdmStatus==1 }">Lulus Administrasi</c:if>
						<c:if test="${s.pmpAdmStatus==-1 }">Tidak Lulus Administrasi</c:if>
					</display:column>
					<display:column title="Status Teknis" class="center" style="background-color:#F99EA4">
						<c:if test="${s.statusTeknis==1 }">Lulus Teknis</c:if>
						<c:if test="${s.statusTeknis==-1 }">Tidak Lulus Teknis</c:if>
					</display:column>
					<display:column title="Passing Grade" class="center" style="background-color:#F99EA4">
						${s.passingGrade }
					</display:column>
					<display:column title="Bobot Teknis" class="center" style="background-color:#F99EA4">
						${s.bobotTeknis }
					</display:column>
					<display:column title="Nilai Teknis" class="center" style="background-color:#F99EA4">
						<a href="javascript:void(0)" onclick="load_into_box('<s:url action="ajax/rfq/viewEvaluasi"/>','prcTemplate.id=${prcMainHeader.prcTemplate.id}&quote.id.vendorId=${s.vendorId }&quote.id.ppmId=${s.ppmId }&prcMainHeader.ppmId=${s.ppmId }&prcMainVendor.id.ppmId=${s.ppmId }&prcMainVendor.id.vendorId=${s.vendorId }');">${s.nilaiTeknis }</a>
						
					</display:column>
					<display:column title="Catatan Teknis" style="background-color:#F99EA4">
						${s.catatanTeknis }
					</display:column>
					<display:column title="Status Harga" class="center" style="background-color:#F99EA4">
						<c:if test="${s.statusHarga==1 }">Lulus Harga</c:if>
						<c:if test="${s.statusHarga==-1 }">Tidak Lulus Harga</c:if>
					</display:column>
					<display:column title="Bobot Harga" class="center" style="background-color:#F99EA4">
						${s.bobotHarga }
					</display:column>
					<display:column title="Nilai Harga" class="center" style="background-color:#F99EA4">
						<c:if test="${s.statusTeknis==1 }">
							<a href="javascript:void(0)" onclick="load_into_box('<s:url action="ajax/rfq/viewEvaluasiHarga"/>','prcTemplate.id=${prcMainHeader.prcTemplate.id}&quote.id.vendorId=${s.vendorId }&quote.id.ppmId=${s.ppmId }&prcMainHeader.ppmId=${s.ppmId }&prcMainVendor.id.ppmId=${s.ppmId }&prcMainVendor.id.vendorId=${s.vendorId }');">${s.nilaiHarga }</a>
						</c:if>
						<c:if test="${s.statusTeknis==-1 }">
							${s.nilaiHarga }
						</c:if>
						<c:if test="${s.statusTeknis==0 }">
							${s.nilaiHarga }
						</c:if>
						
					</display:column>
					<display:column title="Catatan Harga" style="background-color:#F99EA4">
						${s.catatanHarga }
					</display:column>
					<display:column title="Total Nilai" style="background-color:#F99EA4">
						<fmt:formatNumber>
							${s.totalNilai }
						</fmt:formatNumber>
					</display:column>
					
				</c:if>
				<c:if test="${s.statusTeknis==-1 and s.statusHarga==0 }">	
					<display:column title="No." style="width: 20px; text-align: center;background-color:#F99EA4" >${s_rowNum}</display:column>
					<display:column title="Penawaran Mitra Kerja" style="width: 100px;background-color:#F99EA4" class="center">
						<s:url var="set5" action="ajax/viewPenawaranRead" />
						<a title="Penawaran"  href='javascript:void(0)' onclick='load_into_box("${set5}", "prcMainHeader.ppmId=${s.ppmId }&prcMainVendor.id.ppmId=${s.ppmId}&prcMainVendor.id.vendorId=${s.vendorId}");'>${fn:toUpperCase(s.vendorName) }</a>
					</display:column>
					<display:column title="Total Penawaran" style="background-color:#F99EA4">
						<c:if test="${s.statusHarga==1 }">
							LAMA :
							<fmt:formatNumber>
								${s.totalPenawaran }
							</fmt:formatNumber>
							<hr/>
							AUCTION :
							<fmt:formatNumber>
								${s.totalPenawaranEauction }
							</fmt:formatNumber>
							<hr/>
							NEGO :
							<fmt:formatNumber>
								${s.totalPenawaranNego }
							</fmt:formatNumber>
						</c:if>
					</display:column>
					<display:column title="Administrasi" style="background-color:#F99EA4">
						<c:if test="${s.pmpAdmStatus==1 }">Lulus Administrasi</c:if>
						<c:if test="${s.pmpAdmStatus==-1 }">Tidak Lulus Administrasi</c:if>
					</display:column>
					<display:column title="Status Teknis" class="center" style="background-color:#F99EA4">
						<c:if test="${s.statusTeknis==1 }">Lulus Teknis</c:if>
						<c:if test="${s.statusTeknis==-1 }">Tidak Lulus Teknis</c:if>
					</display:column>
					<display:column title="Passing Grade" class="center" style="background-color:#F99EA4">
						${s.passingGrade }
					</display:column>
					<display:column title="Bobot Teknis" class="center" style="background-color:#F99EA4">
						${s.bobotTeknis }
					</display:column>
					<display:column title="Nilai Teknis" class="center" style="background-color:#F99EA4">
						<a href="javascript:void(0)" onclick="load_into_box('<s:url action="ajax/rfq/viewEvaluasi"/>','prcTemplate.id=${prcMainHeader.prcTemplate.id}&quote.id.vendorId=${s.vendorId }&quote.id.ppmId=${s.ppmId }&prcMainHeader.ppmId=${s.ppmId }&prcMainVendor.id.ppmId=${s.ppmId }&prcMainVendor.id.vendorId=${s.vendorId }');">${s.nilaiTeknis }</a>
						
					</display:column>
					<display:column title="Catatan Teknis" style="background-color:#F99EA4">
						${s.catatanTeknis }
					</display:column>
					<display:column title="Status Harga" class="center" style="background-color:#F99EA4">
						<c:if test="${s.statusHarga==1 }">Lulus Harga</c:if>
						<c:if test="${s.statusHarga==-1 }">Tidak Lulus Harga</c:if>
					</display:column>
					<display:column title="Bobot Harga" class="center" style="background-color:#F99EA4">
						${s.bobotHarga }
					</display:column>
					<display:column title="Nilai Harga" class="center" style="background-color:#F99EA4">
						<c:if test="${s.statusTeknis==1 }">
							<a href="javascript:void(0)" onclick="load_into_box('<s:url action="ajax/rfq/viewEvaluasiHarga"/>','prcTemplate.id=${prcMainHeader.prcTemplate.id}&quote.id.vendorId=${s.vendorId }&quote.id.ppmId=${s.ppmId }&prcMainHeader.ppmId=${s.ppmId }&prcMainVendor.id.ppmId=${s.ppmId }&prcMainVendor.id.vendorId=${s.vendorId }');">${s.nilaiHarga }</a>
						</c:if>
						<c:if test="${s.statusTeknis==-1 }">
							${s.nilaiHarga }
						</c:if>
						<c:if test="${s.statusTeknis==0 }">
							${s.nilaiHarga }
						</c:if>
						
					</display:column>
					<display:column title="Catatan Harga" style="background-color:#F99EA4">
						${s.catatanHarga }
					</display:column>
					<display:column title="Total Nilai" style="background-color:#F99EA4">
						<fmt:formatNumber>
							${s.totalNilai }
						</fmt:formatNumber>
					</display:column>
					
				</c:if>
				
				<c:if test="${s.statusTeknis==-1 and s.statusHarga==1 }">	
					<display:column title="No." style="width: 20px; text-align: center;background-color:#F99EA4" >${s_rowNum}</display:column>
					<display:column title="Penawaran Mitra Kerja" style="width: 100px;background-color:#F99EA4" class="center">
						<s:url var="set5" action="ajax/viewPenawaranRead" />
						<a title="Penawaran"  href='javascript:void(0)' onclick='load_into_box("${set5}", "prcMainHeader.ppmId=${s.ppmId }&prcMainVendor.id.ppmId=${s.ppmId}&prcMainVendor.id.vendorId=${s.vendorId}");'>${fn:toUpperCase(s.vendorName) }</a>
					</display:column>
					<display:column title="Total Penawaran" style="background-color:#F99EA4">
						<c:if test="${s.statusHarga==1 }">
							LAMA :
							<fmt:formatNumber>
								${s.totalPenawaran }
							</fmt:formatNumber>
							<hr/>
							AUCTION :
							<fmt:formatNumber>
								${s.totalPenawaranEauction }
							</fmt:formatNumber>
							<hr/>
							NEGO :
							<fmt:formatNumber>
								${s.totalPenawaranNego }
							</fmt:formatNumber>
						</c:if>
					</display:column>
					<display:column title="Administrasi" style="background-color:#F99EA4">
						<c:if test="${s.pmpAdmStatus==1 }">Lulus Administrasi</c:if>
						<c:if test="${s.pmpAdmStatus==-1 }">Tidak Lulus Administrasi</c:if>
					</display:column>
					<display:column title="Status Teknis" class="center" style="background-color:#F99EA4">
						<c:if test="${s.statusTeknis==1 }">Lulus Teknis</c:if>
						<c:if test="${s.statusTeknis==-1 }">Tidak Lulus Teknis</c:if>
					</display:column>
					<display:column title="Passing Grade" class="center" style="background-color:#F99EA4">
						${s.passingGrade }
					</display:column>
					<display:column title="Bobot Teknis" class="center" style="background-color:#F99EA4">
						${s.bobotTeknis }
					</display:column>
					<display:column title="Nilai Teknis" class="center" style="background-color:#F99EA4">
						<a href="javascript:void(0)" onclick="load_into_box('<s:url action="ajax/rfq/viewEvaluasi"/>','prcTemplate.id=${prcMainHeader.prcTemplate.id}&quote.id.vendorId=${s.vendorId }&quote.id.ppmId=${s.ppmId }&prcMainHeader.ppmId=${s.ppmId }&prcMainVendor.id.ppmId=${s.ppmId }&prcMainVendor.id.vendorId=${s.vendorId }');">${s.nilaiTeknis }</a>
						
					</display:column>
					<display:column title="Catatan Teknis" style="background-color:#F99EA4">
						${s.catatanTeknis }
					</display:column>
					<display:column title="Status Harga" class="center" style="background-color:#F99EA4">
						<c:if test="${s.statusHarga==1 }">Lulus Harga</c:if>
						<c:if test="${s.statusHarga==-1 }">Tidak Lulus Harga</c:if>
					</display:column>
					<display:column title="Bobot Harga" class="center" style="background-color:#F99EA4">
						${s.bobotHarga }
					</display:column>
					<display:column title="Nilai Harga" class="center" style="background-color:#F99EA4">
						<c:if test="${s.statusTeknis==1 }">
							<a href="javascript:void(0)" onclick="load_into_box('<s:url action="ajax/rfq/viewEvaluasiHarga"/>','prcTemplate.id=${prcMainHeader.prcTemplate.id}&quote.id.vendorId=${s.vendorId }&quote.id.ppmId=${s.ppmId }&prcMainHeader.ppmId=${s.ppmId }&prcMainVendor.id.ppmId=${s.ppmId }&prcMainVendor.id.vendorId=${s.vendorId }');">${s.nilaiHarga }</a>
						</c:if>
						<c:if test="${s.statusTeknis==-1 }">
							${s.nilaiHarga }
						</c:if>
						<c:if test="${s.statusTeknis==0 }">
							${s.nilaiHarga }
						</c:if>
						
					</display:column>
					<display:column title="Catatan Harga" style="background-color:#F99EA4">
						${s.catatanHarga }
					</display:column>
					<display:column title="Total Nilai" style="background-color:#F99EA4">
						<fmt:formatNumber>
							${s.totalNilai }
						</fmt:formatNumber>
					</display:column>
					
				</c:if>
				
				
			<s:set var="i" value="#i+1"></s:set>
			</display:table>
			</center>
			