<%@ include file="/include/definitions.jsp"%>
<center>
	<!-- Alert -->
	<s:if test="hasActionErrors()">
		<div class="message red">
			<s:actionerror />
		</div>
	</s:if>
	<s:if test="hasActionMessages()">
		<div class="message green">
			<s:actionmessage />
		</div>
	</s:if>

	<div style="margin-top: 20px;"></div>
	<s:set id="i" value="0"></s:set>
	<display:table name="${listContractTop }" id="s" pagesize="100"
		excludedParams="*" style="width: 100%;" class="style1">
		<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
		<display:column title="Termin" class="center">${s.topDescription }</display:column>
		<display:column title="Persentase" style="width: 150px;text-align:right">
		 	<fmt:formatNumber> ${s.topPercentage }</fmt:formatNumber>
		</display:column>
		<display:column title="Nilai" class="center" style="width: 150px;">
			<fmt:formatNumber> ${s.topValue }</fmt:formatNumber>
		</display:column>
		
		<display:column title="Aksi" media="html" style="width: 30px;"
			class="center">
			<div class="uibutton-group">
				<s:url var="set" action="ajax/submitTop" />
				<a title="Hapus data Top" class="uibutton"
					href='javascript:void(0)'
					onclick='confirmPopUp("deleteObj(\"${set}\", \"act=delete&index=${i }&id=${s.id}\", \"#listTop\")", "Hapus Data Item", "Yakin ingin menghapus?", "Hapus", "Batal");'>X</a>
			</div>
		</display:column>
		<s:set var="i" value="#i+1"></s:set>
	</display:table>
</center>
