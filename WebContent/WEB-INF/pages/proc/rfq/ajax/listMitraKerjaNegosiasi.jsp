			<%@ include file="/include/definitions.jsp" %>
			<center>
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>		
			
			<div style="margin-top: 20px;"></div>
			
			<s:set id="i" value="0"></s:set>
			<display:table name="${listPrcMainVendor }" id="s" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
				<display:caption>Mitra Kerja Negosiasi</display:caption>
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column title="Nama Mitra Kerja" style="width: 100px;" class="center">
					<a href="javascript:void(0)" onclick="load_into_box('<s:url action="ajax/vendor/detail"/>','tmpVndHeader.vendorId=${s.vndHeader.vendorId }')">${fn:toUpperCase(s.vndHeader.vendorName) }</a>
				</display:column>
				<display:column title="Alamat" style="width: 150px;" class="center">
					${fn:toUpperCase(s.vndHeader.vendorNpwpCity) }	
				</display:column>
				
				<display:column title="Nego" media="html" style="width: 30px;" class="center">

					<a href="javascript:void(0)" onclick="load_into_box('<s:url action="ajax/rfq/crudNegosiasi"/>','prcMainHeader.ppmId=${s.prcMainHeader.ppmId }&prcMainVendor.id.ppmId=${s.id.ppmId}&prcMainVendor.id.vendorId=${s.id.vendorId}')" class="uibutton add">Negosiasi</a>
				</display:column>
			<s:set var="i" value="#i+1"></s:set>
			</display:table>
			</center>
			