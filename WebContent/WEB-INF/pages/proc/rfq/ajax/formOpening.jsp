<%@ include file="/include/definitions.jsp" %>
	<script type="text/javascript">
		$(document).ready(function(){
			// FORM VALIDATION
			$("#formeproc").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
			});
			
			$("#formeproc").ajaxForm({
		        beforeSubmit: function() {
		           loadingScreen();
		        },
		        success: function(data) {
		            loadPage("<s:url action="ajax/rfq/openingVendor"/>?prcMainHeader.ppmId=${prcMainVendor.id.ppmId}","#result");
		            jQuery.facebox.close();
		        }
		    });
		});
		
		
	</script>
	
<div class='fbox_header'>Verfikasi Administrasi</div>
    <div class='fbox_container'>
    <s:form name="formeproc" id="formeproc" action="ajax/rfq/crudOpening" method="post">
        <div class='fbox_content'>
        	<div id="cek"></div>
            	<s:hidden name="prcMainVendor.id.ppmId"></s:hidden>
            	<s:hidden name="prcMainVendor.id.vendorId"></s:hidden>
            	<s:hidden name="quote.id.ppmId"></s:hidden>
            	<s:hidden name="quote.id.vendorId"></s:hidden>
            	<table style="min-width: 600px; margin: 0px 10px;" class="form formStyle">       
            		<tr>
            			<td width="15%"><s:label value="Nama Mitra Kerja (*)"></s:label> </td>
            			<td colspan="3">
            				${fn:toUpperCase(prcMainVendor.vndHeader.vendorName)}
            			</td>
            		</tr>
            	</table>
            	
            	<div class="box">
					<div class="title">
						Administrasi
						<span class="hide"></span>
					</div>
					<div class="content center">
						<display:table name="${listAdministrasi }" id="s" pagesize="1000" excludedParams="*" style="width: 600px;" class="style1">
							<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
							<display:column title="Item"  class="center">${s.tempItem }</display:column>
							<display:column title="Ada / Tidak (Vendor)" style="text-align:center">
								<input type="hidden" name="listAdministrasi[${s_rowNum-1 }].id.ppmId" value="${s.id.ppmId }"/>
								<input type="hidden" name="listAdministrasi[${s_rowNum-1 }].id.vendorId" value="${s.id.vendorId }"/>
								<input type="hidden" name="listAdministrasi[${s_rowNum-1 }].id.tempDetId" value="${s.id.tempDetId }"/>
								<input type="hidden" name="listAdministrasi[${s_rowNum-1 }].tempItem" value="${s.tempItem }"/>
								<input type="hidden" name="listAdministrasi[${s_rowNum-1 }].tempTipe" value="${s.tempTipe }"/>
								<input type="hidden" name="listAdministrasi[${s_rowNum-1 }].tempWeight" value="${s.tempWeight }"/>
								<input type="hidden" name="listAdministrasi[${s_rowNum-1 }].tempValue" value="${s.tempValue }"/>
								<input type="hidden" name="listAdministrasi[${s_rowNum-1 }].tempVendorDesc" value="${s.tempVendorDesc }"/>
								<input type="hidden" name="listAdministrasi[${s_rowNum-1 }].createdBy" value="${s.createdBy }"/>
								<input type="hidden" name="listAdministrasi[${s_rowNum-1 }].prcTemplateDetail.id" value="${s.prcTemplateDetail.id }"/>
								<input type="hidden" name="listAdministrasi[${s_rowNum-1 }].tempCheckVendor" value="${s.tempCheckVendor }"/>
								<input type="checkbox" disabled="disabled" <c:if test="${s.tempCheckVendor==1 }">checked="checked"</c:if> />
							</display:column>							
							<display:column title="Ada / Tidak (verifikasi)" style="text-align:center">
								<input type="checkbox" name="listAdministrasi[${s_rowNum-1 }].tempCheck" value="${s.tempCheck}" onclick="cekValue(this);" <c:if test="${s.tempCheck==1 }">checked="checked"</c:if> />
							</display:column>
						</display:table>
					</div>
				</div>
				
				<table style="min-width: 600px; margin: 0px 10px;" class="form formStyle">       
            		<tr>
            			<td width="15%"><s:label value="Komentar"></s:label> </td>
            			<td colspan="3">
            				<s:textarea name="prcMainVendor.pmpAdmNotes" cssClass="grow"></s:textarea>
            			</td>
            		</tr>
            	</table>
				
            
        </div>
        <div class='fbox_footer'>
            <s:submit name="lulus" value="Lulus" cssClass="uibutton confirm"></s:submit>
            <s:submit name="tidakLulus" value="Tidak Lulus" cssClass="uibutton confirm"></s:submit>
            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()'>Batalkan</a>
        </div>
        </s:form>
    </div>
    