			<%@ include file="/include/definitions.jsp" %>
			<center>
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>		
			
			<div style="margin-top: 20px;"></div>
			
			<s:set id="i" value="0"></s:set>
			<display:table name="${listVendor }" id="s" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
				<display:caption>Mitra Kerja</display:caption>
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column title="Nama Mitra Kerja" style="width: 100px;" class="center">
					<a href="javascript:void(0)" onclick="load_into_box('<s:url action="ajax/vendor/detail"/>','tmpVndHeader.vendorId=${s.vndHeader.vendorId }')">${fn:toUpperCase(s.vndHeader.vendorName) }</a>
				</display:column>
				<display:column title="Alamat" style="width: 150px;" class="center">
					${fn:toUpperCase(s.vndHeader.vendorNpwpCity) }	
				</display:column>
				<display:column title="Komentar Administrasi" class="center">
					${s.pmpAdmNotes }		
				</display:column>
				<display:column title="Status" class="center">
					<c:if test="${s.pmpStatus==2 }">
						Di Undang
					</c:if>
					<c:if test="${s.pmpStatus==-3 }">
						Tidak Mendaftar
					</c:if>
					<c:if test="${s.pmpStatus==3 }">
						Mendaftar Belum Menawarkan
					</c:if>
					<c:if test="${s.pmpStatus==4 }">
						Menawarkan
					</c:if>
					<c:if test="${s.pmpStatus==5 }">
						Lulus Administrasi
					</c:if>
					<c:if test="${s.pmpStatus==-5 }">
						Tidak Lulus Administrasi
					</c:if>		
				</display:column>
				<display:column title="Evaluasi" media="html" style="width: 30px;" class="center">
					<input type="hidden" name="listVendor[${s_rowNum-1 }].id.ppmId" value="${s.id.ppmId }"/>
					<input type="hidden" name="listVendor[${s_rowNum-1 }].id.vendorId" value="${s.id.vendorId }"/>
					<c:if test="${s.pmpStatus==4 or s.pmpStatus==-5 or s.pmpStatus==5}">
						<a href="javascript:void(0)" onclick="load_into_box('<s:url action="ajax/rfq/crudOpening" />','quote.id.ppmId=${s.id.ppmId }&quote.id.vendorId=${s.id.vendorId}&prcMainVendor.id.ppmId=${s.id.ppmId }&prcMainVendor.id.vendorId=${s.id.vendorId }')" class="uibutton icon edit"></a>
					</c:if>
				</display:column>
			<s:set var="i" value="#i+1"></s:set>
			</display:table>
			</center>
			