<%@ include file="/include/definitions.jsp"%>
<s:form action="kontrak/daftarKontrakNonAktif">
	<s:hidden name="contractHeader.contractId" id="ctrId" />
	<display:table name="${listContractHeader }" id="s" excludedParams="*"
		style="width: 100%;" class="all style1" pagesize="10" partialList="true" size="resultSize" requestURI="${contextPath}/ajax/kontrak/daftarKontrakNonAktif.jwebs" requestURIcontext="true">

		<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
		<display:column title="Nomor Kontrak" total="true"
			style="width: 200px;">${s.contractNumber }</display:column>
		<display:column title="Nama Kontrak" style="width: 150px; "
			class="center">${s.contractSubject }</display:column>
		<display:column title="Tanggal" class="center" style="width: 200px;">
			<fmt:formatDate value="${s.createdDate }"
				pattern="HH:mm:ss, dd MMM yyyy" />
		</display:column>
		<display:column title="Status">
			<c:if test="${s.contractProsesLevel==100 }">Non Aktif</c:if>
			<c:if test="${s.contractProsesLevel==99 }">Aktif</c:if>
			<c:if test="${s.contractProsesLevel==-98 }">Adendum</c:if>
		</display:column>
		<display:column title="Tanggal Mulai" class="center" style="width: 200px;">
			<fmt:formatDate value="${s.contractStart }"
				pattern="HH:mm:ss, dd MMM yyyy" />
		</display:column>
		<display:column title="Tanggal Berakhir" class="center" style="width: 200px;">
			<fmt:formatDate value="${s.contractEnd }"
				pattern="HH:mm:ss, dd MMM yyyy" />
		</display:column >
		<%-- <display:column title="Nomor O1/ O2 JDE" class="center" style="width:200px">
			${s.jdeNumber }
		</display:column> --%>
		<display:column title="Aksi" class="center" style="width: 100px;">
			<input type="submit" name="proses" value="Lihat"onclick="setValue('${s.contractId }');loadingScreen()"class="uibutton" style="width:120px;padding-bottom: 5px;padding-top: 5px"/>
		</display:column>
	</display:table>
</s:form>
