<%@ include file="/include/definitions.jsp" %>
	<script type="text/javascript">
		$(document).ready(function(){
			// FORM VALIDATION
			$("#formeproc").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
			});
			
			$("#formeproc").ajaxForm({
		        beforeSubmit: function() {
		           loadingScreen();
		        },
		        success: function(data) {
		            loadPage("<s:url action="ajax/rfq/tabulasi"/>?prcMainHeader.ppmId=${prcMainVendor.id.ppmId}&prcMainHeader.prcTemplate.id=${prcTemplate.id}","#result");
		            jQuery.facebox.close();
		        }
		    });
		});
		
		
	</script>
	
<div class='fbox_header'>Verfikasi Teknis</div>
    <div class='fbox_container'>
    <s:form name="formeproc" id="formeproc" action="ajax/rfq/crudEvaluasi" method="post">
        <div class='fbox_content'>
        	<div id="cek"></div>
            	<s:hidden name="prcMainVendor.id.ppmId"></s:hidden>
            	<s:hidden name="prcMainVendor.id.vendorId"></s:hidden>
            	<s:hidden name="quote.id.ppmId"></s:hidden>
            	<s:hidden name="quote.id.vendorId"></s:hidden>
            	<table style="min-width: 600px; margin: 0px 10px;" class="form formStyle">       
            		<tr>
            			<td width="15%"><s:label value="Mitra Kerja"></s:label> </td>
            			<td colspan="3">
            				${fn:toUpperCase(prcMainVendor.vndHeader.vendorName)}
            			</td>
            		</tr>
            		<tr>
            			<td width="15%"><s:label value="Passing Grade"></s:label> </td>
            			<td colspan="3">
            				${fn:toUpperCase(prcTemplate.templatePassingGrade)}
            				<input type="hidden" id="passingGrade" value="${prcTemplate.templatePassingGrade}"/>
            			</td>
            		</tr>
            		<tr>
            			<td width="15%"><s:label value="Bobot Teknis"></s:label> </td>
            			<td colspan="3">
            				${fn:toUpperCase(prcTemplate.templateTechWeight)}
            				<input type="hidden" id="techWeight" value="${prcTemplate.templateTechWeight}"/>
            			</td>
            		</tr>
            	</table>
            	
            	<div class="box">
					<div class="title">
						Teknis
						<span class="hide"></span>
						<input type="hidden" id="sizeteknis" value="${listTeknis.size() }"/>
					</div>
					<s:set value="0" var="i"></s:set>
					<div class="content center">			
						<display:table name="${listTeknis }" id="s" pagesize="1000" excludedParams="*" style="width: 600px;" class="style1">
							<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
							<display:column title="Item"  class="center">${s.tempItem }</display:column>
							<display:column title="Respons Mitra Kerja">
								${s.tempVendorDesc }
								<input type="hidden" name="listTeknis[${s_rowNum-1 }].id.ppmId" value="${s.id.ppmId }"/>
								<input type="hidden" name="listTeknis[${s_rowNum-1 }].id.vendorId" value="${s.id.vendorId }"/>
								<input type="hidden" name="listTeknis[${s_rowNum-1 }].id.tempDetId" value="${s.id.tempDetId }"/>
								<input type="hidden" name="listTeknis[${s_rowNum-1 }].tempItem" value="${s.tempItem }"/>
								<input type="hidden" name="listTeknis[${s_rowNum-1 }].tempTipe" value="${s.tempTipe }"/>
								<input type="hidden" name="listTeknis[${s_rowNum-1 }].tempWeight" value="${s.prcTemplateDetail.tempdetWeight }" id="bobotTeknis[${s_rowNum-1 }]"/>
								<input type="hidden" name="listTeknis[${s_rowNum-1 }].tempVendorDesc" value="${s.tempVendorDesc }"/>
								<input type="hidden" name="listTeknis[${s_rowNum-1 }].prcTemplateDetail.id" value="${s.prcTemplateDetail.id }"/>
								<input type="hidden" name="listTeknis[${s_rowNum-1 }].createdBy" value="${s.createdBy }"/>
								<input type="hidden" name="listTeknis[${s_rowNum-1 }].tempCheckVendor" value="${s.tempCheckVendor }"/>
								<s:hidden name="listTeknis[#i].prcTemplateDetail.id"></s:hidden>
							</display:column>
							<display:column title="Bobot" class="center">
								${s.prcTemplateDetail.tempdetWeight }
							</display:column>
							<display:column title="Nilai Teknis (%)" class="center">
								<input type="text" style="width:95%" name="listTeknis[${s_rowNum-1 }].tempValue" value="${s.tempValue }" onkeyup="calculateTeknis(this,${s_rowNum-1})" class="required number" maxlength="3"/>
							</display:column>
							<display:column title="Nilai Total" class="center">
								<input type="text" style="width:95%;color: red" name="listTeknis[${s_rowNum-1 }].tempPercentageValue" value="${s.tempPercentageValue }" id="tempValue[${s_rowNum-1 }]" readonly="readonly"/>
							</display:column>		
							<display:footer>
								<tr>
									<td colspan="5">Total</td>
									<td class="center">
										<input type="text" style="width:90%;color: red" readonly="readonly" name="prcMainVendor.pmpTechnicalValue" value="${prcMainVendor.pmpTechnicalValue}" id="nilaiTeknisTotal"/>
									</td>
								</tr>
							</display:footer>					
							<s:set var="i" value="#i+1"></s:set>
						</display:table>
					</div>
				</div>
				<div class="center" style="width:600px.text-align:center">
					<s:textfield id="notifikasi" cssStyle="color:red; font-weight:bolder;text-align:center;width:300px" readonly="true"/>
				</div>
				<table style="min-width: 600px; margin: 0px 10px;" class="form formStyle">       
            		<tr>
            			<td width="15%"><s:label value="Komentar"></s:label> </td>
            			<td colspan="3">
            				<s:textarea name="prcMainVendor.pmpTechnicalNotes" cssClass="grow"></s:textarea>
            				<input type="hidden" name="prcMainVendor.pmpTechnicalStatus" id="technicalStatus" value="${prcMainVendor.pmpTechnicalStatus}"/>
            				<input type="hidden" name="prcMainVendor.pmpStatus" id="pmpStatus" value="${prcMainVendor.pmpStatus}"/>
            				<input type="hidden" name="prcMainVendor.pmpEvalTotal" id="evalTotal" value="${prcMainVendor.pmpEvalTotal}"/>
            			</td>
            		</tr>
            	</table>
				
            
        </div>
        <div class='fbox_footer'>
            <s:submit name="save" value="Simpan" cssClass="uibutton confirm"></s:submit>
            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()'>Batalkan</a>
        </div>
        </s:form>
    </div>
    