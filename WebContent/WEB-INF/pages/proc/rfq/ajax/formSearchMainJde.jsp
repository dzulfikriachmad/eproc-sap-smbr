			<%@ include file="/include/definitions.jsp" %>
			
		<script>
			$(document).ready(function() {
				// DATATABLE
			    $('table.catalog').dataTable({
			        "bInfo": false,
			        "iDisplayLength": 10,
			        "aLengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
			        "sPaginationType": "full_numbers",
			        "bPaginate": true,
			        "sDom": '<f>t<pl>'
			    });
			});
		</script>
<div id="wrapper" style="width: 783px;">
<div id="container">		
	<div id="right">
		<div class="section" >
			<div class="box" >
				<div class="title center">
					Daftar Master
					<span class="hide"></span>
				</div>
				<div class="content center" style="width: 700px;">
					<display:table name="${ListJdeMasterMain }" id="c" excludedParams="*" style="width: 700px;" class="catalog style1">
						<display:setProperty name="paging.banner.full" value=""></display:setProperty>
						<display:setProperty name="paging.banner.first" value=""></display:setProperty>
						<display:setProperty name="paging.banner.last" value=""></display:setProperty>
						<display:column title="No." style="width: 20px; text-align: center;">
							<a href="javascript:void(0)" onclick="setItemFromPopup('${c.key}', '${idKomponen}')">${c_rowNum}</a>
						</display:column>
						<display:column title="Kode" total="true" style="width: 90px;"  class="center">
							
							<a href="javascript:void(0)" onclick="setItemFromPopup('${c.key}', '${idKomponen}')">${c.key }</a>
						</display:column>
						<display:column title="Nama" style="width: 340px; ">
							<a href="javascript:void(0)" onclick="setItemFromPopup('${c.key}', '${idKomponen}')">${c.value }</a>
						</display:column>
						<display:column title="Tipe" style="width: 100px;  text-align: center;">
							<a href="javascript:void(0)" onclick="setItemFromPopup('${c.key}', '${idKomponen}')">${c.tipe }</a>
						</display:column>				
					</display:table>
				</div>
			</div>
		</div>
	</div>
</div>
</div>