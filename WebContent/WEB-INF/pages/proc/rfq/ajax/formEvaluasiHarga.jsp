<%@ include file="/include/definitions.jsp" %>
	<script type="text/javascript">
		$(document).ready(function(){
			// FORM VALIDATION
			$("#formeproc").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
			});
			
			$("#formeproc").ajaxForm({
		        beforeSubmit: function() {
		           loadingScreen();
		        },
		        success: function(data) {
		            loadPage("<s:url action="ajax/rfq/tabulasi"/>?prcMainHeader.ppmId=${prcMainVendor.id.ppmId}&prcMainHeader.prcTemplate.id=${prcTemplate.id}","#result");
		            jQuery.facebox.close();
		        }
		    });
			
			kalkulasiPersentaseHarga();

		});
		
		
		function kalkulasiPersentaseHarga(){
			var hargaTerendah = $('#hargaTerendah').val();
			var size = $("#ukuranVendor").val();
			for(var i=0;i<size;i++){
				var harga = 'harga'+i;
				var hargaVnd = $('#'+harga).val();
				var ttl = (parseFloat(hargaTerendah) /  parseFloat(hargaVnd))*100;
				document.getElementById('pmpPriceValue['+i+']').value=parseFloat(ttl).toFixed(2);
			}
			
			
		}
		
	</script>
<div class='fbox_header'>Verfikasi Harga</div>
    <div class='fbox_container'>
    <s:form name="formeproc" id="formeproc" action="ajax/rfq/crudEvaluasiHarga" method="post">
        <div class='fbox_content'>
        	<div id="cek"></div>
        		<s:hidden id="hargaOe" name="prcMainHeader.ppmJumlahOe"></s:hidden>
        		<s:hidden id="hargaTerendah" name="hargaTerendah"></s:hidden>
            	<s:hidden name="prcMainVendor.id.ppmId"></s:hidden>
            	<s:hidden name="prcMainVendor.id.vendorId"></s:hidden>
            	<s:hidden name="quote.id.ppmId"></s:hidden>
            	<s:hidden name="quote.id.vendorId"></s:hidden>
            	<table style="min-width: 650px; margin: 0px 10px;" class="form formStyle">       
            		<tr>
            			<td width="15%"><s:label value="Bobot Harga"></s:label> </td>
            			<td colspan="3">
            				${fn:toUpperCase(prcTemplate.templatePriceWeight)}
            				<input type="hidden" id="priceWeight" value="${prcTemplate.templatePriceWeight}"/>
            			</td>
            		</tr>
            	</table>
            	
            	<div class="box">
					<s:set value="0" var="i"></s:set>
					<div class="content center">			
						<display:table name="${listPrcMainItem }" id="t" pagesize="1000" excludedParams="*" style="width: 650px;" class="style1">
							<display:setProperty name="basic.show.header" value="false"></display:setProperty>
							<display:caption media="html">
								<thead>
									<tr>
										<th>No</th>
										<th>Item</th>
										<th>HPS </th>
										<s:iterator value="listPrcMainVendor" var="m">
											<th>${fn:toUpperCase(m.vndHeader.vendorName)}</th>
										</s:iterator>		
									</tr>
								</thead>
							</display:caption>
							<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
							<display:column title="Deskripsi" class="left">${t.itemDescription }</display:column>
							<display:column title="Harga" style="text-align:right">
								<fmt:formatNumber>${t.itemTotalPriceOe * (t.itemTaxOe+100/100) }</fmt:formatNumber>
							</display:column>
							<s:iterator value="listPrcMainVendor" var="v">
								<display:column style="text-align:right">
										<s:iterator value="listQuoteItem" var="pr">
											<c:if test="${pr.id.ppmId==t.prcMainHeader.ppmId}">
													<c:if test="${v.vndHeader.vendorId==pr.id.vendorId}">
														<c:if test="${t.id==pr.id.itemId}">
															<fmt:formatNumber>${pr.itemPriceTotalPpn}</fmt:formatNumber>		
														</c:if>
													</c:if>
											</c:if>
										</s:iterator>
								</display:column>
							</s:iterator>	
						</display:table>
					</div>
				</div>
				
				<div class="box">
					<s:set var="i" value="0"></s:set>
					<div class="content center">
						<input type="hidden" id="ukuranVendor" value="${listPrcMainVendor.size()}"/>
						<display:table name="${listPrcMainVendor }" id="s" pagesize="1000" excludedParams="*" style="width: 650px;" class="style1">
						<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
						<display:column title="Nama Mitra Kerja" style="width: 100px;" class="center">
							<a href="javascript:void(0)" onclick="load_into_box('<s:url action="ajax/vendor/detail"/>','tmpVndHeader.vendorId=${s.vndHeader.vendorId }')">${fn:toUpperCase(s.vndHeader.vendorName) }</a>
						</display:column>
						<display:column title="Total Penawaran">
							<s:iterator value="listQuote" var="q">
								<c:if test="${s.vndHeader.vendorId==q.id.vendorId }">
									<fmt:formatNumber> ${q.pqmTotalPenawaran }</fmt:formatNumber>
									<input type="hidden" id="harga${s_rowNum-1}" value="${q.pqmTotalPenawaran}"/>	
								</c:if>	
							</s:iterator>
						</display:column>
						<display:column title="Validitas Penawaran" media="html" style="width: 10px;" class="center">
							<c:choose>
								<c:when test="${s.validityOffer!=null or s.pmpPriceValue!=null}">
									<input type="checkbox" name="listPrcMainVendor[${s_rowNum-1 }].validityOffer" onclick="cekValue(this)" value="${s.validityOffer }" <c:if test="${s.validityOffer==1 }">checked="checked"</c:if>/>
								</c:when>
								<c:otherwise>
									<input type="checkbox" name="listPrcMainVendor[${s_rowNum-1 }].validityOffer" onclick="cekValue(this)" value="1" checked="checked"/>
								</c:otherwise>
							</c:choose>
						</display:column>
						<display:column title="Validitas Bidbond" media="html" style="width: 10px;" class="center">
							<c:choose>
								<c:when test="${s.validityBidbond!=null or s.pmpPriceValue!=null}">
									<input type="checkbox" name="listPrcMainVendor[${s_rowNum-1 }].validityBidbond" onclick="cekValue(this)" value="${s.validityBidbond }" <c:if test="${s.validityBidbond==1 }">checked="checked"</c:if>/>
								</c:when>
								<c:otherwise>
									<input type="checkbox" name="listPrcMainVendor[${s_rowNum-1 }].validityBidbond" onclick="cekValue(this)" value="1" checked="checked"/>
								</c:otherwise>
							</c:choose>
							
							<input type="hidden" name="listPrcMainVendor[${s_rowNum-1 }].id.ppmId" value="${s.id.ppmId }"/>
							<input type="hidden" name="listPrcMainVendor[${s_rowNum-1 }].id.vendorId" value="${s.id.vendorId }"/>
						</display:column>
						<display:column title="Nilai (%)" class="center" style="width:40px">
							<input class="number required" type="text" style="width:inherit;text-align:right" name="listPrcMainVendor[${s_rowNum-1 }].pmpPriceValue" id="pmpPriceValue[${s_rowNum-1 }]" value="${s.pmpPriceValue }" onkeyup="calculatePrice(this,${s_rowNum-1})"/>
						</display:column>
						<display:column title="Catatan Harga">
							<textarea name="listPrcMainVendor[${s_rowNum-1 }].pmpPriceNotes" class="grow" style="width:inherit">${s.pmpPriceNotes }</textarea>
            				<input type="hidden" name="listPrcMainVendor[${s_rowNum-1 }].pmpPriceStatus" id="priceStatus[${s_rowNum-1 }]" value="${s.pmpPriceStatus}"/>
            				<input type="hidden" name="listPrcMainVendor[${s_rowNum-1 }].pmpStatus" id="pmpStatus[${s_rowNum-1 }]" value="${s.pmpStatus}"/>
            				<input type="hidden" name="listPrcMainVendor[${s_rowNum-1 }].pmpEvalTotal" id="evalTotal[${s_rowNum-1 }]" value="${s.pmpEvalTotal}"/>
            				<input type="hidden" name="listPrcMainVendor[${s_rowNum-1 }].priceEvaluated" id="priceEvaluated[${s_rowNum-1 }]" value="${s.priceEvaluated}"/>
            				<input type="hidden" id="eval[${s_rowNum-1 }]" value="${s.pmpEvalTotal }"/>
						</display:column>
					<s:set var="i" value="#i+1"></s:set>
					</display:table>
					</div>
				</div>
				
        </div>
        <div class='fbox_footer'>
            <s:submit name="save" value="Simpan" cssClass="uibutton confirm"></s:submit>
            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()'>Batalkan</a>
        </div>
        </s:form>
    </div>
    