<%@ include file="/include/definitions.jsp" %>
	<script type="text/javascript">
		$(document).ready(function(){
			// FORM VALIDATION
			$("#formeproc").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
			});
			
			$("#formeproc").ajaxForm({
		        beforeSubmit: function() {
		           loadingScreen();
		        },
		        success: function(data) {
		            loadPage("<s:url action="ajax/rfq/listNegosiasi"/>?prcMainHeader.ppmId=${prcMainVendor.id.ppmId}","#result");
		            jQuery.facebox.close();
		        }
		    });
		});
		
		
	</script>
	
<div class='fbox_header'>Negosiasi</div>
    <div class='fbox_container'>
    <s:form name="formeproc" id="formeproc" action="ajax/rfq/crudNegosiasi" method="post">
        <div class='fbox_content'>
            	<s:hidden name="prcMainVendor.id.ppmId"></s:hidden>
            	<s:hidden name="prcMainVendor.id.vendorId"></s:hidden>
            	<table style="min-width: 600px; margin: 0px 10px;" class="form formStyle">       
            		<tr>
            			<td style="width:15%">Ke</td>
            			<td>
            				<s:hidden name="negosiasi.prcMainHeader.ppmId" value="%{prcMainHeader.ppmId}"></s:hidden>
            				<s:hidden name="negosiasi.vndHeader.vendorId" value="%{prcMainVendor.id.vendorId}"></s:hidden>
            				<s:textfield name="negosiasi.msgTo" value="%{prcMainVendor.vndHeader.vendorName}" readonly="true"></s:textfield>
            			</td>
            		</tr>
            		
            		<tr>
            			<td style="width:15%"> Pesan</td>
            			<td>
            				<s:textarea name="negosiasi.msgComment" cssClass="required" cssStyle="width:100%"></s:textarea>
            			</td>
            		</tr>
            	</table>
        </div>
        <div class='fbox_footer'>
            <s:submit name="save" value="Simpan" cssClass="uibutton confirm"></s:submit>
            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()'>Batalkan</a>
        </div>
        </s:form>
    </div>
    