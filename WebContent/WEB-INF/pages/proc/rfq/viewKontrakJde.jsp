<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Kontrak</title>
	
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	
	<script type="text/javascript">	
		$(document).ready(function(){
			$(".chzn").chosen();
			$.datepicker.setDefaults($.datepicker.regional['in_ID']);
			$("#tgl1").datepicker({
				changeYear: true,
				changeMonth: true
			});
			$("#tgl2").datepicker({
				changeYear: true,
				changeMonth: true
			});
			$("#tgl3").datepicker({
				changeYear: true,
				changeMonth: true
			});
			$("#tgl4").datepicker({
				changeYear: true,
				changeMonth: true
			});
			
			// FORM VALIDATION
			$("#procForm").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler:function(form){
					FreezeScreen('Data Sedang di Proses');
					form.submit();
				}
			});
			
		});
		
		function setItemFromPopup(value,id){
			$('#'+id).val(value);
			jQuery.facebox.close();
		}
		
		//regCancel Action
		function regCancel(){
			jQuery.facebox.close();
			window.location = "<s:url action="kontrak/daftarKontrakAktif" />"; 
		}
	</script>
	<style>caption{font-size: 15px; color: #FF9E0D; text-align: left;}</style>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="dashboard">Home</s:a></li> 
	        
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
    
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
					
			<s:form action="kontrak/daftarKontrakAktif" id="procForm" name="procForm" enctype="multipart/form-data" method="post">
				<s:hidden name="contractHeader.contractId" />
				<div class="box">
					<div class="title">
						Header
						<span class="hide"></span>
					</div>
					<div class="content">
						
						<table class="form formStyle" >
							<tr>
								<td width="150px">No. Kontrak</td>
								<td>
									${contractHeader.contractNumber }
								</td>
							</tr>
							<tr>
								<td width="150px">No. SPPH</td>
								<td>
									${contractHeader.prcMainHeader.ppmNomorRfq }
								</td>
							</tr>
							<tr>
								<td width="150px">No. PR</td>
								<td>
									${contractHeader.prcMainHeader.ppmNomorPr }
								</td>
							</tr>
							
							<tr>
								<td width="150px">Judul Kontrak  (*)</td>
								<td>
									${contractHeader.contractSubject}
								</td>
							</tr>
							<tr>
								<td>Deskripsi Kontrak (*)</td>
								<td>
									${contractHeader.contractDescription }
								</td>
							</tr>
							<tr>
								<td>Jenis Kontrak (*)</td>
								<td>
									<c:if test="${contractHeader.contractType==1}">PO</c:if>
									<c:if test="${contractHeader.contractType==2}">Perjanjian</c:if> 
								</td>
							</tr>
							<tr>
								<td>Tanggal Kontrak Mulai</td>
								<td>
									<fmt:formatDate value="${contractHeader.contractStart }" pattern="dd MMM yyyy"/>
								</td>
							</tr>
							
							<tr>
								<td>Tanggal Kontrak Berakhir</td>
								<td>
									<fmt:formatDate value="${contractHeader.contractEnd }" pattern="dd MMM yyyy"/>
								</td>
							</tr>
							
							<tr>
								<td>Tanggal Penyerahan</td>
								<td>
									<fmt:formatDate value="${contractHeader.tglPenyerahan }" pattern="dd MMM yyyy"/>
								</td>
							</tr>
							
							<tr>
								<td width="150px">Attachment</td>
								<td>
									${contractHeader.prcMainHeader.attachment }
								</td>
							</tr>
							<tr>
								<td width="150px">Jumlah Kontrak</td>
								<td>
									<fmt:formatNumber> ${contractHeader.contractAmount }</fmt:formatNumber>
								</td>
							</tr>
							
							<tr>
								<td width="150px" style="color:red; font-weight: bolder" >Pemenang</td>
								<td>
									${fn:toUpperCase(contractHeader.vndHeader.vendorName) }
								</td>
							</tr>
							
						</table>
						
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Order Header
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle">
							<tr>
								<td>Order Number</td>
								<td> 
									<s:textfield readonly="true" value="" name="poHeader.mnOrderNumber" cssStyle="width:100px" placeholder="Nomor Order Otomatis"></s:textfield>
									<s:select list="#{'O1':'O1', 'O2':'O2','O4':'O4','O9':'O9','Z1':'Z1','Z2':'Z2','Z4':'Z4'}" name="poHeader.szOrderType"></s:select>
									<s:textfield cssClass="required" name="poHeader.szOrderCompany" cssStyle="width:100px" placeholder="Order Company"></s:textfield>
									<s:hidden cssClass="required" name="poHeader.szOrderSuffix" cssStyle="width:100px"  value="000"/>
								</td>
								<td>Site</td>
								<td>
									<s:textfield cssClass="required" name="poHeader.szBranchPlant" placeholder="Branch Plant" id="szBranchPlant" cssStyle="width:80%"></s:textfield>
									<a href="javascript:void(0)" onclick="load_into_box_alert('Master JDE', '<s:url action="ajax/formSearchMainJde" />', 'Tutup', 'idKomponen=szBranchPlant&jdeSearch=BS')" class="uibutton" >...</a> </td>
								</td>
							</tr>
							
							<tr>
								<td>Mata Uang</td>
								<td>
									<s:textfield cssClass="required" name="poHeader.szTransactionCurrencyCode" placeholder="Mata Uang" cssStyle="width:100px" id="szTransactionCurrencyCode"></s:textfield>
									<s:textfield name="poHeader.mnCurrencyExchangeRate" placeholder="Nilai Tukar" value="0" cssStyle="width:50px"></s:textfield>
									<a href="javascript:void(0)" onclick="load_into_box_alert('Master JDE', '<s:url action="ajax/formSearchMainJde" />', 'Tutup', 'idKomponen=szTransactionCurrencyCode&jdeSearch=CURR')" class="uibutton" >...</a>
								</td>
								<td></td>
								<td></td>
							</tr>	
							<tr>
								<td>Supplier</td>
								<td> 
									<s:textfield cssClass="required" name="poHeader.mnSupplierNumber" cssStyle="width:100px" placeholder="Nomor Mitra Kerja" readonly="true"></s:textfield>
									${fn:toUpperCase(contractHeader.vndHeader.vendorName) }
								</td>
								<td>Tanggal Permintaan</td>
								<td>
									<s:textfield cssClass="required" name="poHeader.jdOrderDate" placeholder="Tanggal Permintaan" id="tgl1"></s:textfield>
								</td>
							</tr>
							
							<tr>
								<td>Dikirim ke</td>
								<td>
									<s:textfield cssClass="required" name="poHeader.mnShipToNumber" id="mnShipToNumber" placeholder="Ship To" cssStyle="width:100px"></s:textfield>
									<a href="javascript:void(0)" onclick="load_into_box_alert('Master JDE', '<s:url action="ajax/formSearchMainJde" />', 'Tutup', 'idKomponen=mnShipToNumber&jdeSearch=SHIPTO')" class="uibutton" >...</a>
								</td>
								<td>Tanggal Order</td>
								<td>
									<s:textfield cssClass="required"  name="poHeader.jdRequestedDate" placeholder="Tanggal Order" id="tgl2"></s:textfield>
								</td>
							</tr>	
							
							<tr>
								<td>Unit Kerja Peminta</td>
								<td>
									<s:textfield cssClass="required" name="poHeader.mnBuyerNumber" id="mnBuyerNumber"  placeholder="Buyer" cssStyle="width:100px"></s:textfield>
									<a href="javascript:void(0)" onclick="load_into_box_alert('Master JDE', '<s:url action="ajax/formSearchMainJde" />', 'Tutup', 'idKomponen=mnBuyerNumber&jdeSearch=BUYER')" class="uibutton" >...</a>
								</td>
								<td>Promised Delivery</td>
								<td>
									<s:textfield name="poHeader.jdPromisedDate" placeholder="Tanggal Pengiriman" id="tgl3" cssClass="required"></s:textfield>
								</td>
							</tr>	
							
							<tr>
								<td>Carrier</td>
								<td>
									<s:textfield cssClass="required" name="poHeader.mnCarrierNumber" id="mnCarrierNumber" placeholder="Carrier" cssStyle="width:100px"></s:textfield>
									<a href="javascript:void(0)" onclick="load_into_box_alert('Master JDE', '<s:url action="ajax/formSearchMainJde" />', 'Tutup', 'idKomponen=mnCarrierNumber&jdeSearch=CARRIER')" class="uibutton" >...</a>
								</td>
								<td>Tanggal Cancel</td>
								<td>
									<s:textfield name="poHeader.jdCancelDate" placeholder="Tanggal Cancel" id="tgl4"></s:textfield>
								</td>
							</tr>	
							
							<tr>
								<td>Keterangan 1</td>
								<td>
									<s:textfield name="poHeader.szRemark" placeholder="Remark" cssStyle="width:90%" maxlength="30"></s:textfield>
								</td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td>Keterangan 2</td>
								<td>
									<s:textfield name="poHeader.szDescription" placeholder="Deskripsi Singkat" cssStyle="width:90%" maxlength="30"></s:textfield>
								</td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td>Jenis Jasa</td>
								<td>
									<s:textfield name="poHeader.szPrintMessage" id="szPrintMessage" placeholder="Print Message" cssStyle="width:80%"></s:textfield>
									<a href="javascript:void(0)" onclick="load_into_box_alert('Master JDE', '<s:url action="ajax/formSearchMainJde" />', 'Tutup', 'idKomponen=szPrintMessage&jdeSearch=PM')" class="uibutton" >...</a>
								</td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td>Tax Expl Code</td>
								<td>
									<s:textfield name="poHeader.szTaxExplanationCode" readonly="true" cssStyle="width:90%"></s:textfield>
								</td>
								<td>Hold Code</td>
								<td>
									<s:textfield name="poHeader.szHoldCode"  placeholder="Hold Code" cssStyle="width:80%" id="szHoldCode"></s:textfield>
									<a href="javascript:void(0)" onclick="load_into_box_alert('Master JDE', '<s:url action="ajax/formSearchMainJde" />', 'Tutup', 'idKomponen=szHoldCode&jdeSearch=HC')" class="uibutton" >...</a>
								</td>
							</tr>
							<tr>
								<td>Tax Rate/ Area</td>
								<td>
									<s:textfield name="poHeader.szTaxRateArea" readonly="true" cssStyle="width:90%"></s:textfield>
								</td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td>Certificate</td>
								<td>
									<s:textfield name="poHeader.szTaxCertificate" placeholder="Certificate" cssStyle="width:90%"></s:textfield>
								</td>
								<td>Retainage (%)</td>
								<td>
									<s:textfield name="poHeader.mnRetainage" readonly="true"></s:textfield>
								</td>
							</tr>
							<tr>
								<td>Tax ID</td>
								<td>
									<s:textfield readonly="true" cssStyle="width:90%"></s:textfield>
								</td>
								<td>Ordered By</td>
								<td>
									<s:textfield name="poHeader.szOrderedPlacedBy" placeholder="Ordered By" readonly="true"></s:textfield>
								</td>
							</tr>
							<tr>
								<td>Person/ Corp ID</td>
								<td>
									<s:textfield readonly="true" cssStyle="width:90%"></s:textfield>
								</td>
								<td>Order Taken By</td>
								<td>
									<s:textfield name="poHeader.szOrderTakenBy" placeholder="Order Taken By"></s:textfield>
								</td>
							</tr>
							<tr>
								<td>Payment Terms</td>
								<td>
									<s:textfield name="poHeader.szPaymentTerms" id="szPaymentTerms" placeholder="Payment Term" cssStyle="width:80%"></s:textfield>
									<a href="javascript:void(0)" onclick="load_into_box_alert('Master JDE', '<s:url action="ajax/formSearchMainJde" />', 'Tutup', 'idKomponen=szPaymentTerms&jdeSearch=PAYTERM')" class="uibutton" >...</a>
								</td>
								<td>AIA Document</td>
								<td>
									<s:textfield name="poHeader.szPOEMBFInternalFlags" id="cAIADocument" cssStyle="width:80%" placeholder="AIA Document" value="Y"></s:textfield>
									<a href="javascript:void(0)" onclick="load_into_box_alert('Master JDE', '<s:url action="ajax/formSearchMainJde" />', 'Tutup', 'idKomponen=cAIADocument&jdeSearch=AIA')" class="uibutton" >...</a>
								</td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>			
						</table>
					</div>
				</div>
								
				<div class="box">
					<div class="title">
						Item
						<span class="hide"></span>
					</div>
					<div class="content center" style="overflow:scroll">		
						<s:set id="i" value="0"></s:set>
						<display:table name="${listContractItem }" id="s" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
							<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
							<display:column title="Sub Ledger" style="width:100px">
								<s:textfield name="listPoDetail[%{#i}].szSubledger" cssStyle="width:inherit" readonly="true"></s:textfield>
								<input type="hidden" name="listPoDetail[${s_rowNum-1 }].szLastStatus" value="220"/>
								<input type="hidden" name="listPoDetail[${s_rowNum-1 }].szNextStatus" value="280"/>
								<input type="hidden" name="listPoDetail[${s_rowNum-1 }].szElevation" value="${s.attachment }"/>
								<s:hidden name="listPoDetail[%{#i}].mnExtendedPrice"></s:hidden>
								<s:hidden name="listPoDetail[%{#i}].mnSecondaryQty"></s:hidden>
								<s:hidden name="listPoDetail[%{#i}].szSecondaryUOM"></s:hidden>
								<s:hidden name="listPoDetail[%{#i}].jdGLDate"></s:hidden>
								<s:hidden name="listPoDetail[%{#i}].szAssetID"></s:hidden>
								<s:hidden name="listPoDetail[%{#i}].szLocation"></s:hidden>
								<s:hidden name="listPoDetail[%{#i}].szLotNumber"></s:hidden>
								<s:hidden name="listPoDetail[%{#i}].szOriginalOrderNumber"></s:hidden>
							</display:column>
							<display:column title="Sub Type">
								<s:textfield name="listPoDetail[%{#i}].subledgerType" cssStyle="float:left;width:60%" id="cSubledgerType%{#i}" readonly="true"></s:textfield>
								<!--  
								<a href="javascript:void(0)" style="float: left;" onclick="load_into_box_alert('Master JDE', '<s:url action="ajax/formSearchMainJde" />', 'Tutup', 'idKomponen=cSubledgerType${s_rowNum-1 }&jdeSearch=SUBLEDGER')" class="uibutton icon edit" ></a>
								-->
							</display:column>
							<display:column title="Line">
								<s:textfield name="listPoDetail[%{#i}].mnOrderLineNumber" cssStyle="width:50px" readonly="true"></s:textfield>
							</display:column>
							<display:column title="Kode" style="width: 70px;" class="center">
								<input type="hidden" name="listPoDetail[${s_rowNum-1 }].szUnformattedItemNumber" value="${s.itemCode }"/>
								${s.itemCode }
							</display:column>
							<display:column title="Deskripsi" class="left">
								${s.itemDescription }
							</display:column>
							<display:column title="Jumlah" style="text-align:right">
								<input type="hidden" name="listPoDetail[${s_rowNum-1 }].mnQuantityOrdered" value="${s.itemQuantityOe }"/>
								<fmt:formatNumber> ${s.itemQuantityOe }</fmt:formatNumber>
							</display:column>
							<display:column title="Uom">
								<input type="text" readonly="readonly" name="listPoDetail[${s_rowNum-1 }].szTransactionUoM" id="szTransactionUoM${s_rowNum-1 }" class="required" style="width:60%" value="${ listPoDetail[s_rowNum-1].szTransactionUoM}"/>
								<!--  
								<a href="javascript:void(0)" onclick="load_into_box_alert('Master JDE', '<s:url action="ajax/formSearchMainJde" />', 'Tutup', 'idKomponen=szTransactionUoM${s_rowNum-1 }&jdeSearch=UOM')" class="uibutton icon edit" ></a>
								-->
							</display:column>
							<display:column title="Harga Satuan" style="text-align:right">
								<input type="hidden" name="listPoDetail[${s_rowNum-1 }].mnUnitPrice" value="${s.itemPriceOe }"/>
								<fmt:formatNumber> ${s.itemPriceOe }</fmt:formatNumber>
							</display:column>
							<display:column title="Line Type">
								<input type="text" name="listPoDetail[${s_rowNum-1 }].szLineType" id="szLineType${s_rowNum-1 }" class="required" style="width:60%" value="${listPoDetail[s_rowNum-1].szLineType }" readonly="readonly"/>
								<!--  
								<a href="javascript:void(0)" onclick="load_into_box_alert('Master JDE', '<s:url action="ajax/formSearchMainJde" />', 'Tutup', 'idKomponen=szLineType${s_rowNum-1 }&jdeSearch=LINETYPE')" class="uibutton icon edit" ></a>
								-->
							</display:column>
							<display:column title="Desc 1">
								<input type="text" name="listPoDetail[${s_rowNum-1 }].szDescription1" class="required" style="width:inherit" value="${listPoDetail[s_rowNum-1].szDescription1}" readonly="readonly"/>
							</display:column>
							<display:column title="Desc 2">
								<input type="text" name="listPoDetail[${s_rowNum-1 }].szDescription2" style="width:inherit" value="${listPoDetail[s_rowNum-1].szDescription2}"/>
								<input type="hidden" value="${s.itemTaxOe }" id="itemTaxOe-${i }">
							</display:column>
							<display:column title=" Account Number">
								<input type="text" name="listPoDetail[${s_rowNum-1 }].szUnformattedAccountNumber"  style="width:inherit" value="${listPoDetail[s_rowNum-1].szUnformattedAccountNumber}"/>
							</display:column>
							<display:column title="Cost Center">
								<input type="text" name="listPoDetail[${s_rowNum-1 }].szPurchasingCostCenter"  style="width:inherit" value="${listPoDetail[s_rowNum-1].szPurchasingCostCenter }"/>
							</display:column>
							<display:column title="Obj. Account">
								<input type="text" name="listPoDetail[${s_rowNum-1 }].szObjectAccount"  style="width:inherit" value="${listPoDetail[s_rowNum-1].szObjectAccount}"/>
							</display:column>
							<display:column title="Sub">
								<input type="text" name="listPoDetail[${s_rowNum-1 }].szSubsidiary"  style="width:inherit" value="${listPoDetail[s_rowNum-1].szSubsidiary}"/>
							</display:column>
							<display:column title="Tax">
								<input type="text" name="listPoDetail[${s_rowNum-1 }].taxable" id="taxable" class="required" style="width:30px" value="${listPoDetail[s_rowNum-1].taxable}" readonly="readonly"/>
								<!--  
								<a href="javascript:void(0)" onclick="load_into_box_alert('Master JDE', '<s:url action="ajax/formSearchMainJde" />', 'Tutup', 'idKomponen=taxable${s_rowNum-1 }&jdeSearch=TAXABLE')" class="uibutton icon edit" ></a>
								-->
								<input type="hidden" name="listPoDetail[${s_rowNum-1 }].cEvaluatedReceipts" value="N"/>
								<input type="hidden" name="listPoDetail[${s_rowNum-1 }].mnDiscountFactor" value="0"/>
							</display:column>
							<display:column title="Report Code 1">
								<input type="text" name="listPoDetail[${s_rowNum-1 }].szPurchasingCategoryCode1" id="szPurchasingCategoryCode1${s_rowNum-1 }" value="${listPoDetail[s_rowNum-1].szPurchasingCategoryCode1 }" style="width:60%" readonly="readonly"/>
								<!--    
								<a href="javascript:void(0)" onclick="load_into_box_alert('Master JDE', '<s:url action="ajax/formSearchMainJde" />', 'Tutup', 'idKomponen=szPurchasingCategoryCode1${s_rowNum-1 }&jdeSearch=REPORT1')" class="uibutton icon edit" ></a>
								-->
								
							</display:column>
							<display:column title="Report Code 2">
								<input type="text" readonly="readonly" name="listPoDetail[${s_rowNum-1 }].szPurchasingCategoryCode2"  id="szPurchasingCategoryCode2${s_rowNum-1 }" value="${listPoDetail[s_rowNum-1].szPurchasingCategoryCode2 }" style="width:60%"/>
								<!--  
								<a href="javascript:void(0)" onclick="load_into_box_alert('Master JDE', '<s:url action="ajax/formSearchMainJde" />', 'Tutup', 'idKomponen=szPurchasingCategoryCode2${s_rowNum-1 }&jdeSearch=REPORT2')" class="uibutton icon edit" ></a>
								-->
							</display:column>
							<display:column title="Report Code 3">
								<input readonly="readonly" type="text" name="listPoDetail[${s_rowNum-1 }].szPurchasingCategoryCode3" id="szPurchasingCategoryCode3${s_rowNum-1 }" value="${listPoDetail[s_rowNum-1].szPurchasingCategoryCode3 }" style="width:60%"/>
								<!--  
								<a href="javascript:void(0)" onclick="load_into_box_alert('Master JDE', '<s:url action="ajax/formSearchMainJde" />', 'Tutup', 'idKomponen=szPurchasingCategoryCode3${s_rowNum-1 }&jdeSearch=REPORT3')" class="uibutton icon icon" ></a>
								-->
							</display:column>
							<display:column title="Report Code 4">
								<input readonly="readonly" type="text" name="listPoDetail[${s_rowNum-1 }].szPurchasingCategoryCode4" id="szPurchasingCategoryCode4${s_rowNum-1 }" value="${listPoDetail[s_rowNum-1].szPurchasingCategoryCode4 }" style="width:60%"/>
								<!--  
								<a href="javascript:void(0)" onclick="load_into_box_alert('Master JDE', '<s:url action="ajax/formSearchMainJde" />', 'Tutup', 'idKomponen=szPurchasingCategoryCode4${s_rowNum-1 }&jdeSearch=REPORT4')" class="uibutton icon edit" ></a>
								-->
							</display:column>
							<display:column title="Wt UOM">
								<input readonly="readonly" type="text" name="listPoDetail[${s_rowNum-1 }].szWeightUoM" id="szWeightUoM${s_rowNum-1 }" class="" style="width:60%" value="${listPoDetail[s_rowNum-1].szWeightUoM}"/>
								<!--  
								<a href="javascript:void(0)" onclick="load_into_box_alert('Master JDE', '<s:url action="ajax/formSearchMainJde" />', 'Tutup', 'idKomponen=szWeightUoM${s_rowNum-1 }&jdeSearch=UOM')" class="uibutton icon edit" ></a>
								-->
							</display:column>
							<display:column title="Vol UOM">
								<input readonly="readonly" type="text" name="listPoDetail[${s_rowNum-1 }].szVolumeUoM" id="szVolumeUoM${s_rowNum-1 }" class="" style="width:60%" value="${listPoDetail[s_rowNum-1].szVolumeUoM}"/>
								<!--  
								<a href="javascript:void(0)" onclick="load_into_box_alert('Master JDE', '<s:url action="ajax/formSearchMainJde" />', 'Tutup', 'idKomponen=szVolumeUoM${s_rowNum-1 }&jdeSearch=UOM')" class="uibutton icon edit" ></a>
								-->
							</display:column>
							<display:column title="G/L Offset">
								<input readonly="readonly" type="text" name="listPoDetail[${s_rowNum-1 }].szGLClassCode" id="szGLClassCode${s_rowNum-1 }" style="width:inherit" value="${listPoDetail[s_rowNum-1].szGLClassCode}"/>
							</display:column>
							<display:column title="Sub Total" style="text-align:right">
								<fmt:formatNumber> ${s.itemTotalPriceOe }</fmt:formatNumber>
								<input type="hidden" value="${s.itemTotalPriceOe }" id="itemTotalPriceOe-${i }">
							</display:column>
							<display:column title="Total + Pajak" style="text-align:right">
								<script type="text/javascript">
									$(document).ready(function(){
										var totalAllOe = 'totaloe-'+${i};
										$('#'+totalAllOe).autoNumeric('init');
										var itax2 = parseFloat($('#itemTaxOe-${i }').val());
										var itotal2 = parseFloat($('#itemTotalPriceOe-${i }').val());

										if(!isNaN(itax2) && !isNaN(itotal2)){
											$('#'+totalAllOe).autoNumeric('set',(itax2 / 100 * itotal2 + itotal2));
										}
										
										
									});
									
								</script>
								<input type="text" readonly="readonly" id="totaloe-${i }" style="width:inherit;text-align:right"/>
							</display:column>
							<display:footer>
								<tr>
									<td colspan="9" style="color:red"></td>
								</tr>
							</display:footer>
							
						<s:set var="i" value="#i+1"></s:set>
						</display:table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Pembuat Kontrak
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle">
							<tr>
								<td width="150px;">Pembuat Kontrak</td>
								<td>
									${fn:toUpperCase(contractHeader.admUserByContractCreator.completeName)}
								</td>
							</tr>						
						</table>
					</div>
				</div>

				
				<div class="box">
					<div class="title">
						Termin
						<span class="hide"></span>
					</div>
					<div class="content">
						<div id="listTop">
							<s:include value="ajax/listTopRead.jsp" />
						</div>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Jaminan Pelaksanaan
						<span class="hide"></span>
					</div>
					<div class="content">
						<div id="listPb">
							<s:include value="ajax/listPbRead.jsp" />
						</div>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Daftar Komentar
						<span class="hide"></span>
					</div>
					<div class="content center">
						<display:table name="${listContractComment }" id="k" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
							<display:column title="No." style="width: 20px; text-align: center;">${k_rowNum}</display:column>
							<display:column title="Nama" style="width: 100px;" class="center">${k.username }</display:column>
							<display:column title="Posisi" style="width: 100px;" class="center">${k.posisi }</display:column>
							<display:column title="Proses" style="width: 100px;" class="center">${k.prosesAksi }</display:column>
							<display:column title="Tgl Pembuatan" style="width: 100px;" class="center">
								<fmt:formatDate value="${k.createdDate }" pattern="dd.MM.yyyy HH:mm"/> 
							</display:column>
							<display:column title="Tgl Update" style="width: 100px;" class="center">
								<fmt:formatDate value="${k.updatedDate }" pattern="dd.MM.yyyy HH:mm"/> 
							</display:column>
							<display:column title="Aksi" class="center" style="width: 250px;">${k.aksi }</display:column>
							<display:column title="Komentar" class="center" style="width: 250px;">${k.comment }</display:column>
							<display:column title="Dokumen" class="center" style="width: 200px;">
								<a href="${contextPath }/upload/file/${k.document }" target="_blank">${k.document }</a>
							</display:column>
						</display:table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						User Login JDE
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle">
							<tr>
								<td>Username JDE</td>
								<td>
									<s:textfield name="jdeUser" cssClass="required"></s:textfield>
								</td>
							</tr>
							<tr>
								<td>Password JDE</td>
								<td>
									<s:password name="jdePassword" cssClass="required" maxlength="10"></s:password>
								</td>
							</tr>
						</table>
					</div>
				</div>
				
				
				
				<div class="uibutton-toolbar extend" >
					<s:hidden id="komentarVal" value="Push ke JDE"></s:hidden>
					<s:submit name="savejde" id="btnSubmit" onclick="actionAlert('btnSubmit','Simpan ke JDE');return false;" value="Simpan dan Selesai" cssClass="uibutton special" ></s:submit>
					<a href="javascript:void(0)" class="uibutton" onclick="confirmPopUp('regCancel();', 'Batal', 'Apakah anda yakin untuk membatalkan transaksi ?', 'Ya', 'Tidak');"><s:text name="cancel"></s:text> </a>					
				</div>
				
						
			</s:form>
			
		</div>
	</div>
</body>
</html>