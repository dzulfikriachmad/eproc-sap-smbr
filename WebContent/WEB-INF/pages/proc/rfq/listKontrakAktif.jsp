<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<title>Daftar Kontrak</title>
	
	<script type="text/javascript">
		function setValue(id){
			$('#ctrId').val(id);
		}
	</script>
	<script type="text/javascript">
		if (!display)
			var display = {};
		display.jwebs = {
			onTableLoad : function() {
				// call when data loaded 
				$("table#s th.sortable").each(function() {
					$(this).click(function() {
						var link = $(this).find("a").attr("href");
						jQuery.facebox('<div style="padding: 17px; font-size: 36px;">Loading....</div>');
						$("div#hasil").load(link, {}, display.jwebs.onTableLoad);
						jQuery.facebox.close();
						return false;
					});
				});
	
				$("div#hasil .pagelinks a").each(function() {
					// Iterate over the pagination-generated links to override also
					$(this).click(function() {
						var link = $(this).attr("href");
						jQuery.facebox('<div style="padding: 17px; font-size: 36px;">Loading....</div>');
						$("div#hasil").load(link, {}, display.jwebs.onTableLoad);
						jQuery.facebox.close();
						return false;
					});
				});
			}
		};
	
		$(document).ready(
				function() {
					jQuery.facebox('<div style="padding: 17px; font-size: 36px;">Loading....</div>');
					$("div#hasil").load("${contextPath}/ajax/kontrak/daftarKontrakAktif.jwebs", {},display.jwebs.onTableLoad);
					jQuery.facebox.close();
				});
	
		$("#searchButton").live("click",function(){
			$.ajax({
		        url: "<s:url action="ajax/report/searchKontrakAktif" />", 
		        data: $(document.formSearchKontrak.elements).serialize(),
		        beforeSend: function(){
		        	$("#hasil").html('<img src="${contextPath}/assets/images/loading/loading-circle.gif"/>');
		        },
		        success: function(response){
		                $("#hasil").html(response);
		            },
		        type: "post", 
		        dataType: "html"
		    }); 
		});
		$("#searchNomoKontrak").live("keypress",function(e){
			   if(e.keyCode == 13){
				   $("#searchButton").click();
				   return false;
			   }
		   });
		
	</script>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="dashboard">Home</s:a></li> 
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
	
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
			
				<div class="box">
					<div class="title">
						Daftar Kontrak
						<span class="hide"></span>
					</div>
					
					<div class="content">	
					<s:form id="formSearchKontrak">
					<table style="margin-bottom: 30px;" class="form formStyle1">
						<tr>
							<td width="20%">Nomor Kontrak</td>
							<td>:&nbsp;</td>
							<td>
								<s:textfield name="contractHeader.contractNumber" id="searchNomoKontrak"></s:textfield>
								<s:hidden name="contractHeader.contractProsesLevel" value="99"></s:hidden>
							</td>
						</tr>
						
						<tr>
							<td width="20%">Periode Akhir Kontrak</td>
							<td>:&nbsp;</td>
							<td>
								<s:select list="#{'0':'-- Pilih --','1':'>3 bulan','2':'2 - 3 Bulan','3':'1 - 2 Bulan', '4':'< 1 Bulan'}" name="periode"></s:select>
							</td>
						</tr>
						<tr>
							<td width="20%" valign="top">Keterangan</td>
							<td>:&nbsp;</td>
							<td style="display: inline;direction: ltr;">
								<div style="background-color: blue; width: 100px; float: left" > <span style="color:white">> 3 Bulan</span></div> 
								<div style="background-color:green;width: 100px ; float: left" ><span style="color:white">2 - 3 Bulan</span></div> 
								<div style="background-color:orange;width: 100px;float: left" ><span style="color:white">1 - 2 Bulan</span></div> 
								<div style="background-color:red;width: 100px;float: left" ><span style="color:white"> < 1 Bulan</span></div>
							</td>
						</tr>
						
						<tr>
							<td></td>
							<td>&nbsp;</td>
							<td>
								<input type="button" value="Cari" class="uibutton" id="searchButton"/> 
							</td>
						</tr>
					</table>
					</s:form>					
					<div id="hasil"></div>
					</div>
				</div>
		</div>
	</div>
</body>
</html>