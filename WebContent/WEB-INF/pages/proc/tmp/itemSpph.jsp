						<%@ include file="/include/definitions.jsp"%>
						<script type="text/javascript">var totalEE=0,totalOE=0;</script>						
						<s:set id="i" value="0"></s:set>
						<display:table name="${listPrcMainItem }" id="s" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
							<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
							<display:column title="Kode" style="width: 70px;" class="center">
								${fn:toUpperCase(s.itemCode) }
							</display:column>
							<display:column title="Deskripsi" class="left">${s.itemDescription }</display:column>
							<display:column title="Jumlah" style="text-align:right">
								<fmt:formatNumber> ${s.itemQuantity }</fmt:formatNumber>
								<hr />
								<span style="font-weight:bold;"><fmt:formatNumber> ${s.itemQuantityOe }</fmt:formatNumber></span>
							</display:column>
							<display:column class="center" title="Satuan">
								${s.admUom.uomName }
							</display:column>
							<display:column title="Harga Satuan" style="text-align:right">
								<fmt:formatNumber> ${s.itemPrice }</fmt:formatNumber>
								<hr />
								<span style="font-weight:bold;"><fmt:formatNumber> ${s.itemPriceOe }</fmt:formatNumber></span>
							</display:column>
							<display:column title="Pajak (%)" style="text-align:right">
								${s.itemTax }
								<hr />
								<span style="font-weight:bold;">${s.itemTaxOe }</span>
								<input type="hidden" value="${s.itemTax }" id="itemTax-${i }">
								<input type="hidden" value="${s.itemTaxOe }" id="itemTaxOe-${i }">
							</display:column>
							<display:column title="Sub Total" style="text-align:right">
								<fmt:formatNumber> ${s.itemPrice * s.itemQuantity }</fmt:formatNumber>
								<hr />
								<span style="font-weight:bold;"><fmt:formatNumber> ${s.itemTotalPriceOe }</fmt:formatNumber></span>
								<input type="hidden" value="${s.itemTotalPrice }" id="itemTotalPrice-${i }">
								<input type="hidden" value="${s.itemTotalPriceOe }" id="itemTotalPriceOe-${i }">
							</display:column>
							<display:column title="Total + Pajak" style="text-align:right">
								<script type="text/javascript">
									$(document).ready(function(){
										var itax = parseFloat($('#itemTax-${i }').val());
										var itotal = parseFloat($('#itemTotalPrice-${i }').val());
										var totalAll = 'total-'+${i};
										$('#'+totalAll).autoNumeric('init');
										if(!isNaN(itax) && !isNaN(itotal)){
											$('#'+totalAll).autoNumeric('set',(itotal + ((itax/100)*itotal) ));
										}else{
											$('#'+totalAll).autoNumeric('set',0);
										}
										$("#totalAllEE").autoNumeric('init');
										totalEE = totalEE + (itotal + ((itax/100)*itotal));
										$("#totalAllEE").autoNumeric('set',totalEE);
										
										var totalAllOe = 'totaloe-'+${i};
										$('#'+totalAllOe).autoNumeric('init');
										var itax2 = parseFloat($('#itemTaxOe-${i }').val());
										var itotal2 = parseFloat($('#itemTotalPriceOe-${i }').val());

										if(!isNaN(itax2) && !isNaN(itotal2)){
											$('#'+totalAllOe).autoNumeric('set',(itax2 / 100 * itotal2 + itotal2));
										}
										$("#totalAllOE").autoNumeric('init');
										totalOE = totalOE + (itax2 / 100 * itotal2 + itotal2);
										$("#totalAllOE").autoNumeric('set',totalOE);
									});
									
								</script>
								<input type="text" readonly="readonly" id="total-${i }" style="width:inherit; ;text-align:right"/>
								<hr/>
								<input type="text" readonly="readonly" id="totaloe-${i }" style="width:inherit;text-align:right"/>
							</display:column>
							<display:footer>
								<tr style="font-weight: bold;">
									<td colspan="7">&nbsp;</td>
									<td style="text-align: right;">Total EE</td>
									<td style="text-align: right;">
									<input type="text" readonly="readonly" id="totalAllEE" style="width:inherit;text-align:right;font-weight:bold"/>
									</td>
								</tr>
								<tr style="font-weight: bold;">
									<td colspan="7" style="color:red">&nbsp;</td>
									<td style="text-align: right;color:blue">Total OE</td>
									<td style="text-align: right;">
									<input type="text" readonly="readonly" id="totalAllOE" style="width:inherit;text-align:right;color:blue;font-weight:bold"/>
									</td>
								</tr>
								<tr>
									<td colspan="10" style="font-weight: bold;">(* Nilai OE berhuruf tebal Biru</td>
								</tr>
							</display:footer>
							
						<s:set var="i" value="#i+1"></s:set>
						</display:table>