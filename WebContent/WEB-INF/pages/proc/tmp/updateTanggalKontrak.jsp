<%@ include file="/include/definitions.jsp"%>
<script type="text/javascript">
	$(document).ready(function() {
		// FORM VALIDATION
		$('.chzn').chosen();
		$.datepicker.setDefaults($.datepicker.regional['in_ID']);
		$("#tgl1").datepicker({
			changeYear : true,
			changeMonth : true
		});
		$("#tgl2").datepicker({
			changeYear : true,
			changeMonth : true
		});
		$("#tgl3").datepicker({
			changeYear : true,
			changeMonth : true
		});

		$("#formeproc").validate({
			meta : "validate",
			errorPlacement : function(error, element) {
				error.insertAfter(element);
			},
		});

		$("#formeproc").ajaxForm({
			beforeSubmit : function() {
				loadingScreen();
			},
			success : function(data) {
				//loadPage("<s:url action="ajax/adm/listKantor"/>", "#result");
				jQuery.facebox.close();
			}
		});
	});
</script>

<div class='fbox_header'>Update Tanggal Kontrak</div>
<div class='fbox_container'>
	<s:form name="formeproc" id="formeproc" action="ajax/formUpdateTanggalKontrak">
		<div class='fbox_content'>
			<div id="cek"></div>

			<s:hidden name="contractHeader.contractId"></s:hidden>
			<table style="min-width: 600px; margin: 0px 10px;"
				class="form formStyle">
				<tr>
					<td>Tanggal Kontrak Mulai</td>
					<td><s:textfield name="contractHeader.contractStart" id="tgl1"
							cssClass="required"></s:textfield></td>
				</tr>

				<tr>
					<td>Tanggal Penyerahan</td>
					<td><s:textfield name="contractHeader.tglPenyerahan" id="tgl3"
							cssClass="required"></s:textfield></td>
				</tr>

				<tr>
					<td>Tanggal Kontrak Berakhir</td>
					<td><s:textfield name="contractHeader.contractEnd" id="tgl2"
							cssClass="required"></s:textfield></td>
				</tr>
			</table>

		</div>
		<div class='fbox_footer'>
			<s:submit key="save" cssClass="uibutton confirm"></s:submit>
			<a class="uibutton" href='javascript:void(0)'
				onclick='jQuery.facebox.close()'>Batalkan</a>
		</div>
	</s:form>
</div>
