<%@ include file="/include/definitions.jsp" %>
	<script type="text/javascript">
		$(document).ready(function(){
			// FORM VALIDATION
			$("#formeproc").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
			});
			
			$("#formeproc").ajaxForm({
		        beforeSubmit: function() {
		            loadingScreen();
		        },
		        success: function(data) {
		            //loadPage("<s:url action="ajax/adm/listNegara"/>","#result");
		            $('#result').html(data);
		            jQuery.facebox.close();
		        }
		    });
		});
		
		
	</script>
	
<div class='fbox_header'>Form Panitia</div>
    <div class='fbox_container'>
    <s:form name="formeproc" id="formeproc">
    	<s:hidden name="prcMainHeader.ppmId"/>
    	<s:hidden name="panitia.id"></s:hidden>
        <div class='fbox_content'>
        	<div id="cek"></div>
            	<table style="min-width: 600px; margin: 0px 10px;" class="form formStyle">       
            		<tr>
            			<td><s:label value="Nama Panitia (*)"></s:label> </td>
            			<td colspan="3"><s:textfield name="panitia.namaPanitia" cssClass="required" /> </td>
            		</tr>
            		<tr>
            			<td><s:label value="Keterangan Panitia (*)"></s:label> </td>
            			<td colspan="3"><s:textfield name="panitia.keterangan" cssClass="required" /> </td>
            		</tr>
            	</table>
            
        </div>
        <div class='fbox_footer'>
            <s:submit key="save" cssClass="uibutton confirm"></s:submit>
            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()'>Batalkan</a>
        </div>
        </s:form>
    </div>
    