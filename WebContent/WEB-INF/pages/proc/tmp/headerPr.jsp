<%@ include file="/include/definitions.jsp"%>
<table class="form formStyle">
	<tr>
		<td width="150px">No. SPPH</td>
		<td>${prcMainHeader.ppmNomorRfq }</td>
	</tr>
	<tr>
		<td width="150px">No. PR</td>
		<td>${prcMainHeader.ppmNomorPr }</td>
	</tr>
	<tr>
		<td width="150px">Pembuat</td>
		<td>${prcMainHeader.namaPembuatPr }</td>
	</tr>

	<tr>
		<td>Departemen</td>
		<td>${prcMainHeader.admDept.deptName }</td>
	</tr>
	<tr>
		<td>Kantor</td>
		<td>${prcMainHeader.admDistrict.districtName }</td>
	</tr>
	<tr>
		<td width="150px">Nama Permintaan (*)</td>
		<td>${prcMainHeader.ppmSubject }</td>
	</tr>
	<tr>
		<td>Deskripsi Permintaan (*)</td>
		<td>${prcMainHeader.ppmDescription }</td>
	</tr>
	<tr>
		<td>Jenis Permintaan (*)</td>
		<td>${prcMainHeader.ppmJenisDetail.namaJenis}</td>
	</tr>

	<tr>
		<td>Prioritas</td>
		<td><c:if test="${prcMainHeader.ppmPrioritas == 1 }">High</c:if>
			<c:if test="${prcMainHeader.ppmPrioritas == 2 }">Normal</c:if> <c:if
				test="${prcMainHeader.ppmPrioritas == 3 }">Low</c:if></td>
	</tr>
	<tr>
		<td>Chatting</td>
		<td><a href="javascript:void(0)"
			onclick="load_into_box('<s:url action="ajax/chatInternal"/>','prcMainHeader.ppmId=${prcMainHeader.ppmId }')"><span
				id="chat">Chatting Disini</span></a></td>
	</tr>

	<tr>
		<td>Attachment</td>
		<td>${prcMainHeader.attachment }</td>
	</tr>
	<tr>
		<td>File</td>
		<td><c:if
				test="${prcMainHeader.attachment!=null and prcMainHeader.attachment!='' }">
				<a href="${contextPath }/upload/file/${prcMainHeader.attachment}"
					target="_blank">Donload</a>
			</c:if></td>
	</tr>

</table>