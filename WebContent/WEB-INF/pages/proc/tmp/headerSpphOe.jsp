<%@ include file="/include/definitions.jsp"%>
<table class="form formStyle">
	<tr>
		<td width="250px;">No. SPPH</td>
		<td>${prcMainHeader.ppmNomorRfq }</td>
	</tr>
	<tr>
		<td>Satuan Kerja Pelaksana</td>
		<td>${prcMainHeader.admDept.deptName }</td>
	</tr>
	<tr>
		<td>Lokasi Pembuatan OE</td>
		<td>${prcMainHeader.admDistrict.districtName }</td>
	</tr>
	<tr>
		<td width="250px;">Nama Permintaan</td>
		<td>${prcMainHeader.ppmSubject }</td>
	</tr>
	<tr>
		<td>Deskripsi Permintaan</td>
		<td>${prcMainHeader.ppmDescription }</td>
	</tr>

	<tr>
		<td>Chatting (Khusus Logistik)</td>
		<td><a href="javascript:void(0)"
			onclick="load_into_box('<s:url action="ajax/chatInternal"/>','prcMainHeader.ppmId=${prcMainHeader.ppmId }')">Chatting (Khusus Logistik)
				Disini</a></td>
	</tr>

	<tr>
		<td>Tanggal Berlaku OE</td>
		<td><fmt:formatDate value="${prcMainHeader.tanggalOe}"
				pattern="dd MMM yyyy" /></td>
	</tr>

</table>