			<%@ include file="/include/definitions.jsp"%>
			<div style="margin-top: 20px;"></div>
			<s:set id="i" value="0"></s:set>
			<c:set value="0" var="totalPpn"></c:set>
			<display:table name="${listPrcMainItem }" id="s" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
				<display:column title="No" style="width:15px" class="center">
					${s_rowNum }
				</display:column>
				<display:column title="Kode" style="width: 100px;" class="center">
					<a href="javascript:void(0)" onclick="load_into_box('<s:url action="ajax/viewCommodity"/>','comCatalog.comCatalog=${s.itemCode}')">${fn:toUpperCase(s.itemCode) }</a>
				</display:column>
				<display:column title="Cost Center" style="width: 80px;" class="center">${s.itemCostCenter }</display:column>
				<display:column title="Item" class="center" style="width: 150px;">${s.itemDescription }</display:column>
				<display:column title="Jumlah" class="center" style="width: 40px;">${s.itemQuantity }</display:column>
				<display:column title="Satuan" class="center" style="width: 30px;">${s.admUom.uomName }</display:column>
				<display:column title="Harga Satuan" style="text-align:right;width:90px"><fmt:formatNumber> ${s.itemPrice }</fmt:formatNumber></display:column>
				<display:column title="Pajak" class="center" style="width: 20px">
					${s.itemTax }
					<input type="hidden" value="${s.itemTax }" id="itemTax-${i }">
				</display:column>
				<display:column title="Sub Total"  style="text-align:right;width:90px">
					<fmt:formatNumber> ${s.itemTotalPrice }</fmt:formatNumber>
					<input type="hidden" value="${s.itemTotalPrice }" id="itemTotalPrice-${i }">
				</display:column>
				<display:column title="Total + Pajak" style="text-align:right;width:100px">
					<fmt:formatNumber>${(s.itemTotalPrice *(s.itemTax+100))/100}</fmt:formatNumber>
					<c:set var="d" value="${(s.itemTotalPrice *(s.itemTax+100))/100}"></c:set>
					<c:set var="totalPpn" value="${d+totalPpn }"></c:set>
				</display:column>
			<s:set var="i" value="#i+1"></s:set>
			<display:footer>
				<tr>
					<td colspan="10" style="text-align:right;font-weight: bold;">Total EE </td>
					<td style="text-align:right;font-weight: bold;">
						<fmt:formatNumber>${totalPpn }</fmt:formatNumber>
					</td>
				</tr>
			</display:footer>
			</display:table>