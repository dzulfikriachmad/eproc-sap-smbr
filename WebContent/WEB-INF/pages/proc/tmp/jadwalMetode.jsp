<%@ include file="/include/definitions.jsp"%>
<div class="box">
	<div class="title">
		Jadwal Pengadaan <span class="hide"></span>
	</div>
	<div class="content">
		<table class="form formStyle">
			<tr>
				<td width="250px;">Tanggal Pembukaan Pendaftaran</td>
				<td>:&nbsp; <fmt:formatDate
						value="${prcMainPreparation.registrastionOpening}"
						pattern="dd.MM.yyyy HH:mm" /></td>
			</tr>
			<tr>
				<td width="250px;">Tanggal Penutupan Pendaftaran</td>
				<td>:&nbsp; <fmt:formatDate
						value="${prcMainPreparation.registrastionClosing}"
						pattern="dd.MM.yyyy HH:mm" /></td>
			</tr>
			<tr>
				<td width="250px;">Tanggal Aanwijzing</td>
				<td>:&nbsp; <fmt:formatDate
						value="${prcMainPreparation.aanwijzingDate}"
						pattern="dd.MM.yyyy HH:mm" /></td>
			</tr>
			<tr>
				<td width="250px;">Tempat Aanwijzing</td>
				<td>:&nbsp; ${prcMainPreparation.aanwijzingPlace}</td>
			</tr>
			<tr>
				<td width="250px;">Tanggal Pembukaan Penawaran</td>
				<td>:&nbsp; <fmt:formatDate
						value="${prcMainPreparation.quotationOpening}"
						pattern="dd.MM.yyyy HH:mm" /></td>
			</tr>
		</table>
	</div>
</div>

<div class="box">
	<div class="title">
		Metode Pengadaan <span class="hide"></span>
	</div>
	<div class="content">
		<table class="form formStyle">
			<tr>
				<td width="250px;">Metode Pengadaan</td>
				<td>${prcMainHeader.prcMethod.methodName }</td>
			</tr>
			<tr>
				<td width="250px;">Metode Penilaian</td>
				<td>${prcMainHeader.prcTemplate.templateName }</td>
			</tr>
			<tr>
				<td width="250px;">Tipe Penawaran</td>
				<td>Tipe A <input type="checkbox"
					<c:if test="${prcMainHeader.ppmTipeA==1 }">checked="checked"</c:if>
					disabled="disabled" /> &nbsp; Tipe B <input type="checkbox"
					<c:if test="${prcMainHeader.ppmTipeB==1 }">checked="checked"</c:if>
					disabled="disabled" /> &nbsp; Tipe C <input type="checkbox"
					<c:if test="${prcMainHeader.ppmTipeC==1 }">checked="checked"</c:if>
					disabled="disabled" /> &nbsp; <br style="padding: -10px" /> <pre>(* A: sesuai spek, B: Spek Beda Jumlah sama, C:Spek Beda, Jumlah Beda)</pre>
				</td>
			</tr>
			<tr>
				<td width="250px;">Harga</td>
				<td><c:if test="${prcMainHeader.oeOpen==1 }">Terbuka</c:if> <c:if
						test="${prcMainHeader.oeOpen!=1 }">Tertutup</c:if></td>
			</tr>
			<tr>
				<td width="250px;">Eauction</td>
				<td><c:if test="${prcMainHeader.ppmEauction==1 }">Ya</c:if> <c:if
						test="${prcMainHeader.ppmEauction!=1 }">Tidak</c:if></td>
			</tr>
		</table>
	</div>
</div>