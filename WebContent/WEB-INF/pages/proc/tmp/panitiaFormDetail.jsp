<%@ include file="/include/definitions.jsp" %>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.chzn').chosen();
			// FORM VALIDATION
			$("#formeproc").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
			});
			
			$("#formeproc").ajaxForm({
		        beforeSubmit: function() {
		        	loadingScreen();
		        },
		        success: function(data) {
		            //loadPage("<s:url action="ajax/adm/listNegara"/>","#result");
		            $('#result').html(data);
		            jQuery.facebox.close();
		        }
		    });
		});
		
		
	</script>
	
<div class='fbox_header'>Form Panitia Detail</div>
    <div class='fbox_container'>
    <s:form name="formeproc" id="formeproc">
    	<s:hidden name="panitia.id" />
    	<s:hidden name="panitiaDetail.panitia.id" value="%{panitia.id}"/>
        <div class='fbox_content'>
        	<div id="cek"></div>
            	<table style="min-width: 600px; margin: 0px 10px;min-height: 300px" class="form formStyle">       
            		<tr>
            			<td><s:label value="Anggota (*)"></s:label> </td>
            			<td colspan="3">
            				<s:select list="listUser" listKey="id" listValue="completeName" name="panitiaDetail.anggota.id" cssClass="chzn" cssStyle="width:400px"></s:select>
            			</td>
            		</tr>
            		<tr>
            			<td><s:label value="Posisi (*)"></s:label> </td>
            			<td colspan="3">
            				<s:select list="#{'1':'Ketua','2':'Sekretaris','3':'Anggota' }" name="panitiaDetail.posisi"></s:select>
            			</td>
            		</tr>
            		<tr>
            			<td><s:label value="Status (*)"></s:label> </td>
            			<td colspan="3">
            				<s:select list="#{'0':'-','1':'Setuju','2':'Setuju Pemenang' }" name="panitiaDetail.status"></s:select>
            			</td>
            		</tr>
            	</table>
            
        </div>
        <div class='fbox_footer'>
            <s:submit key="save" cssClass="uibutton confirm"></s:submit>
            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()'>Batalkan</a>
        </div>
        </s:form>
    </div>
    