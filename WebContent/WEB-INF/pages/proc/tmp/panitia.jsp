<%@ include file="/include/definitions.jsp" %>
<div class="box">
	<div class="title">
		Panitia Pengadaan <span class="hide"></span>
	</div>
	<div class="content center">
		<table class="form formStyle">
			<tr>
				<td width="250px;">Nama Panitia</td>
				<td>${panitia.namaPanitia }</td>
			</tr>
			<tr>
				<td width="250px;">Keterangan</td>
				<td>${panitia.keterangan }</td>
			</tr>
		</table>
		<div>
			<s:set id="i" value="0"></s:set>
			<display:table name="${listPanitiaDetail }" id="s" excludedParams="*"
				style="width: 100%;" class="style1" pagesize="100">
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column title="Nama Anggota" style="width: 100px;"
					class="center">${s.anggota.completeName }</display:column>
				<display:column title="Posisi" style="width: 150px;" class="center">
					<c:if test="${s.posisi==1 }">Ketua</c:if>
					<c:if test="${s.posisi==2 }">Sekretaris</c:if>
					<c:if test="${s.posisi==3 }">Anggota</c:if>
				</display:column>
				<display:column title="Status" style="width: 150px;" class="center">
					<c:if test="${s.status==0 }"></c:if>
					<c:if test="${s.status==1 }">Setuju</c:if>
					<c:if test="${s.status==2 }">Setuju Pemenang</c:if>
				</display:column>

				<s:set var="i" value="#i+1"></s:set>
			</display:table>
		</div>
	</div>
</div>