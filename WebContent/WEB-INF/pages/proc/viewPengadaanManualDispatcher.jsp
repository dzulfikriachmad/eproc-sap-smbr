<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Persetujuan Permintaan Pengadaan </title>
	
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	
	<script type="text/javascript">	
		$(document).ready(function(){
			$(".chzn").chosen();
			// FORM VALIDATION
			$("#procForm").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler:function(form){
					FreezeScreen('Data Sedang di Proses');
					form.submit();
				}
			});
			
			$('#tblDoc').on('click', 'input[type="button"]', function(e){
				   $(this).closest('tr').remove()
			})
		});
		
		
		//regCancel Action
		function regCancel(){
			jQuery.facebox.close();
			window.location = "<s:url action="pr/daftarPr" />"; 
		}
		
		function cekItemCategory(obj){
			console.log(obj);
		}
		
		function addRow(tableId){
			var table = document.getElementById(tableId);
			var rowCount = table.rows.length;
			var row = table.insertRow(rowCount);
			
			/* var cell1 = row.insertCell(0);
			var element1 = document.createElement('input');
			element1.type = "checkbox";
			cell1.appendChild(element1); */
			
			var cell3 = row.insertCell(0);
			cell3.innerHTML='<input type="text" name="prcManDocumentCategory" style="width:90%" maxlength="30"/>';
			
			var cell4 = row.insertCell(1);
			cell4.innerHTML='<input type="text" name="prcManDocumentDescription" style="width:96%" maxlength="40"/>';

			var cell5 = row.insertCell(2);
			cell5.innerHTML='<input type="file" name="prcManDocument" size="40" value="[]" id="prcManDocument"/>';
			
			var cell6 = row.insertCell(3);
			
			cell6.innerHTML='<input type="button" name="prcManDocument" title="Hapus" size="40" value="x" class="uibutton "/>';
			var els = document.getElementById(tableId).getElementsByTagName("td");

			for(var i=0;i<els.length;i++){
			  els[i].style.textAlign="center";
			}
		}

		function delRow(tableId){
			try{
				var table = document.getElementById(tableId);
				var rowCount = table.rows.length;
				for(var i=0;i<rowCount;i++){
					var row = table.rows[i];
					var chkbox = row.cells[0].childNodes[0];
					if(null!=chkbox && true == chkbox.checked){
						table.deleteRow(i);
						rowCount--;
						i--;
					}
				}
			}catch(e){
				alert(e);
			}
		}
		
	</script>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="dashboard">Home</s:a></li> 
	        
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
    
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
					
			<s:form action="procurement/dispatcherManual" id="procForm" name="procForm" enctype="multipart/form-data" method="post">
				<s:hidden name="orHeader.id.nomorOr" id="nomorOr"></s:hidden>
				<s:hidden name="orHeader.id.orH" id="orH"></s:hidden>
				<s:hidden name="orHeader.id.headerSite" id="headerSite"></s:hidden>
				<s:hidden name="orHeader.id.groupCode" ></s:hidden>
				<div class="box">
					<div class="title">
						Header
						<span class="hide"></span>
					</div>
					<div class="content">
						<table  class="form formStyle" >
							<tr>
								<td width="150px">No. PR</td>
								<td>
									${orHeader.id.nomorOr } 
								</td>
							</tr>
							<tr>
								<td width="150px">Pembuat PR</td>
								<td>
									<c:if test="${orHeader.namaPembuatOm !='null' and orHeader.namaPembuatOm !=null}">
										${orHeader.namaPembuatOm }
									</c:if> 
								</td>
							</tr>
							<tr>
								<td>Satuan Kerja</td>
								<td>
									${admDept.deptName }
								</td>
							</tr>
							
							<tr>
								<td >Nama Permintaan  (*)</td>
								<td>
								<s:textfield name="prcMainHeader.ppmSubject" readonly="true"></s:textfield>
								<s:hidden name="prcMainHeader.groupCode"></s:hidden>
								</td>
							</tr>
							<tr>
								<td>Deskripsi Permintaan (*)</td>
								<td><s:textarea name="prcMainHeader.ppmDescription" readonly="true"></s:textarea> </td>
							</tr>
							<tr>
								<td>Jenis Permintaan (*)</td>
								 <!-- <td><s:select list="#{'1':'Barang', '2':'Jasa'}" name="prcMainHeader.ppmJenis"></s:select> </td>-->
								 <td><s:select list="listAdmJenis" listKey="id" listValue="namaJenis" name="prcMainHeader.ppmJenis.id" onchange="cekItemCategory(this.value)" 
								 cssClass="required" cssStyle="width:200px"></s:select></td>
							</tr>
							
							<tr>
								<td>Prioritas</td>
								<td><s:select list="#{'1':'High', '2':'Normal', '3':'Low' }" name="prcMainHeader.ppmPrioritas"></s:select></td>
							</tr>
							
							
							<tr>
								<td>Attachment Header</td>
								<td>${orHeader.attachment} </td>
							</tr>
							
							<tr>
								<td>File</td>
								<td>
									<c:if test="${orHeader.attachment!=null and orHeader.attachment!='' }">
										<a href="${contextPath }/upload/file/${orHeader.attachment}" target="_blank">Download</a>
									</c:if>
								</td>
							</tr>
						</table>
						
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Item
						<span class="hide"></span>
					</div>
					<div class="content center">		
						<s:set id="i" value="0"></s:set>
						<display:table name="${listOrDetail }" id="s" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
							<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
							<display:column title="Kode Barang"  class="center">
							${s.kodeBarang }
							<input type="hidden" id="tipe" value="${s.itemCategory }"/>
							</display:column>
							<display:column title="Nama Barang"  >${s.namaBarang }</display:column>
							<%-- <display:column title="Material PO Text" >${s.namaBarangExt }</display:column> --%>
							<display:column title="Deskripsi" >${s.descriptionText }</display:column>
							<display:column title="Jumlah" class="center" style="width: 40px;"><fmt:formatNumber type = "number" value="${s.jumlah}"></fmt:formatNumber></display:column>
							<display:column title="Satuan" class="center">${s.satuan }</display:column>
							<display:column title="Harga Satuan" style="text-align:right" >
								<fmt:formatNumber> ${s.hargaSatuan }</fmt:formatNumber>
							</display:column>
							<%-- <display:column title="Attachment" >${s.attachment}</display:column>
							<display:column title="File" >
								<c:if test="${s.attachment!=null and s.attachment!='' }">
									<a href="${contextPath }/upload/file/${s.attachment}" target="_blank">Download</a>
								</c:if>
							</display:column> --%>
						<s:set var="i" value="#i+1"></s:set>
						</display:table>
						
					</div>
				</div>
				
				
				
				<div class="box">
					<div class="title">
						Lampiran Dokumen Pengadaan Manual
						<span class="hide"></span>
					</div>
					<div class="content center">
					<input type="button" onclick="addRow('tblDoc')" value="Tambah" class="uibutton icon add">
						<!-- <input type="button" onclick="delRow('tblDoc')" value="Hapus" class="uibutton"> -->
						
						<table class="style1" id="tblDoc" style="margin-top: 10px;">
							<thead>
								<tr>
									
									<th width="30%">
										Nama Dokumen
									</th>
									<th width="50%" >
										Deskripsi
									</th>
		
									<th width="20%" >
										Dokumen
									</th>
									<th class="center">Aksi</th>
								</tr>
							</thead>
						</table>
						<div class="content center">
						
					</div>
					</div>
				</div>
				<div class="box">
					<div class="title">
						Komentar
						<span class="hide"></span>
					</div>
					<div class="content">
						<s:hidden name="histMainComment.username" value="%{admEmployee.empFullName}" />
						<table class="form formStyle">
							<tr>
								<td width="150px;">Komentar (*)</td>
								<td><s:textarea name="histMainComment.comment" id="komentarVal" cssClass="required"></s:textarea> </td>
							</tr>	
							<tr>
								<td width="150px;">Lampiran</td>
								<td><s:file name="docsKomentar" cssClass="uibutton" /></td>
							</tr>							
						</table>
					</div>
				</div>
					
				<div class="uibutton-toolbar extend">
					<s:submit name="save" value="Pengadaan Manual" cssClass="uibutton"></s:submit>		
					<a href="javascript:void(0)" class="uibutton" onclick="confirmPopUp('regCancel();', 'Batal', 'Apakah anda yakin untuk membatalkan transaksi ?', 'Ya', 'Tidak');"><s:text name="cancel"></s:text> </a>					
				</div>
						
			</s:form>
			
		</div>
	</div>
</body>
</html>