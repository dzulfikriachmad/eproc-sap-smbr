<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Persetujuan Permintaan Pengadaan </title>
	
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	
	<script type="text/javascript">	
		$(document).ready(function(){
			// FORM VALIDATION
			$("#procForm").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				}
			});
		});
		
		//regCancel Action
		function regCancel(){
			jQuery.facebox.close();
			window.location = "<s:url action="procurement/approval" />"; 
		}
		
		function setCostCenter(){
			$('#costCenter').val($('#admCostCenter').val());
		}
		
		function hitungSubTotal(){
			var jumlah = parseInt($('#jumlahItem').val());
			var hargaSatuan = parseInt($('#hargaSatuanItem').val());
			
			if(!isNaN(jumlah) && !isNaN(hargaSatuan)){
				$('#hargaTotalItem').val(jumlah*hargaSatuan);
			}else{
				$('#hargaTotalItem').val(0);
			}
		}
		
		function setItemFromPopup(kode, desc, tipe, satuan){
			$('#kodeItem').val(kode);
			$('#deskripsiItem').val(desc);
			$('#itemTipe').val(tipe);
			$('#satuan').val(satuan);
			jQuery.facebox.close();
		}
		
		function resetItem(){
			$('#kodeItem').val('');
			$('#deskripsiItem').val('');
			$('#itemTipe').val('');
			$('#taxItem').val('');
			$('#jumlahItem').val('');
			$('#hargaSatuanItem').val('');
			$('#hargaTotalItem').val('');
		}
		
		
		/* function FreezeScreen(msg) {
		      scroll(0,0);
		      var outerPane = document.getElementById('FreezePane');
		      var innerPane = document.getElementById('InnerFreezePane');
		      if (outerPane) outerPane.className = 'FreezePaneOn';
		      if (innerPane) innerPane.innerHTML = msg;
		 } */
	</script>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="dashboard">Home</s:a></li> 
	        <li>Manajemen Vendor</li> 
	        <li><s:a action="procurement/request">Persetujuan Permintaan Pengadaan</s:a></li>
	        
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
    
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
					
			<s:form action="procurement/approval" id="procForm" name="procForm" enctype="multipart/form-data" method="post">
				<s:hidden name="prcMainHeader.ppmId" />
				<s:hidden name="prcMainHeader.admProses.id"></s:hidden>
				<s:hidden name="prcMainHeader.ppmProsesLevel"></s:hidden>
				
				<div class="box">
					<div class="title">
						Header
						<span class="hide"></span>
					</div>
					<div class="content">
						<table style="width: 700px;" class="form formStyle" >
							<tr>
								<td width="150px">No. PR</td>
								<td>
									${prcMainHeader.ppmNomorPr }
								</td>
							</tr>
							<tr>
								<td width="150px">Pembuat</td>
								<td>${admEmployee.empFullName }</td>
							</tr>
							<tr>
								<td>Departemen</td>
								<td>
									${admEmployee.admDept.deptName }
								</td>
							</tr>
							<tr>
								<td>Kantor</td>
								<td>
									${admEmployee.admDept.admDistrict.districtName }
								</td>
							</tr>
							<tr>
								<td width="150px">Nama Permintaan  (*)</td>
								<td>${prcMainHeader.ppmSubject }</td>
							</tr>
							<tr>
								<td>Deskripsi Permintaan (*)</td>
								<td>${prcMainHeader.ppmDescription } </td>
							</tr>
							<tr>
								<td>Jenis Permintaan (*)</td>
								<td>
									${prcMainHeader.ppmJenisDetail.namaJenis}
								</td>
							</tr>
							
							<tr>
								<td>Prioritas</td>
								<td>
									<c:if test="${prcMainHeader.ppmPrioritas == 1 }">High</c:if>
									<c:if test="${prcMainHeader.ppmPrioritas == 2 }">Normal</c:if>
									<c:if test="${prcMainHeader.ppmPrioritas == 3 }">Low</c:if>
								</td>
							</tr>
							<%-- <tr>
								<td>Tipe Permintaan (*)</td>
								<td><s:select list="" name="prcMainHeader.ppmTipe"></s:select></td>
							</tr>
							<tr>
								<td>Tanggal Dibutuhkan</td>
								<td><s:textfield name="prcMainHeader.vendorContactEmail"></s:textfield></td>
							</tr>
							<tr>
								<td>Lokasi Penyerahan</td>
								<td><s:textfield name="prcMainHeader.vendorContactEmail"></s:textfield></td>
							</tr> --%>
						</table>
						
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Item
						<span class="hide"></span>
					</div>
					<div class="content center">
												
						<s:set id="i" value="0"></s:set>
						<display:table name="${listPrcMainItem }" id="s" pagesize="10" excludedParams="*" style="width: 100%;" class="style1">
							<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
							<display:column title="Kode" style="width: 100px;" class="center">${s.itemCode }</display:column>
							<display:column title="Cost Center" style="width: 150px;" class="center">${s.itemCostCenter }</display:column>
							<display:column title="Item" class="center" style="width: 150px;">${s.itemDescription }</display:column>
							<display:column title="Jumlah" class="center" style="width: 40px;">${s.itemQuantity }</display:column>
							<display:column title="Harga Satuan" style="width: 120px;" class="center">${s.itemPrice }</display:column>
							<display:column title="Pajak" class="center" style="width: 40px;">
								${s.itemTax }
								<input type="hidden" value="${s.itemTax }" id="itemTax-${i }">
							</display:column>
							<display:column title="Sub Total" class="center" style="width: 100px;">
								${s.itemTotalPrice }
								<input type="hidden" value="${s.itemTotalPrice }" id="itemTotalPrice-${i }">
							</display:column>
							<display:column title="Total + Pajak" class="center" style="width: 100px;">
								<script type="text/javascript">
									$(document).ready(function(){
										var itax = parseFloat($('#itemTax-${i }').val());
										var itotal = parseFloat($('#itemTotalPrice-${i }').val());
										if(!isNaN(itax) && !isNaN(itotal)){
											$('#total-${i }').text(itax / 100 * itotal + itotal + " IDR");
										}else{
											$('#total-${i }').text("0 IDR");
										}
									});
									
								</script>
								<div id="total-${i }"></div>
							</display:column>
							
						<s:set var="i" value="#i+1"></s:set>
						</display:table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Daftar Komentar
						<span class="hide"></span>
					</div>
					<div class="content center">
						<display:table name="${listHistMainComment }" id="k" excludedParams="*" style="width: 100%;" class="all style1">
							<display:column title="No." style="width: 20px; text-align: center;">${k_rowNum}</display:column>
							<display:column title="Nama" style="width: 100px;" class="center">${k.username }</display:column>
							<display:column title="Proses" style="width: 100px;" class="center">${k.proses }</display:column>
							<display:column title="Komentar" class="center" style="width: 250px;">${k.comment }</display:column>
							<display:column title="Dokumen" class="center" style="width: 200px;">
								<a href="${contextPath }/upload/file/${k.document }" target="_blank">${k.document }</a>
							</display:column>
						</display:table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Pakta Integritas
						<span class="hide"></span>
					</div>
					<div class="content">
							<p align="justify">
							Saya, yang membuat permintaan pengadaan barang/jasa nomor : &nbsp;&nbsp;<b>${prcMainHeader.ppmNomorPr }</b> <br/>
							Dengan ini menyatakan dengan sebenarnya bahwa : </p>
							<ol>
								<li>Barang/jasa tersebut adalah benar dibutuhkan untuk menunjang kegiatan operasional perusahaan</li>
								<li>Dalam pengadaan barang/jasa tersebut saya tidak memiliki kepentingan pribadi atau tujuan untuk melakukan sesuatu manfaat bagi diri saya sendiri, atau melakukan praktek KKN (KORUPSI, KOLUSI NEPOTISME) dalam pengadaan barang/jasa tersebut</li>
							</ol>
							<p align="justify">
							Demikian pernyataan ini kami sampaikan dengan sebenar-benarnya, tanpa menyembunyikan fakta dan hal material apapun dan dengan demikian kami akan bertanggung jawab sepenuhnya atas kebenaran dari hal-hal yang kami nyatakan disini. <br/>
							Demikian pernyataan ini kami buat untuk digunakan sebagaimana mestinya.
							</p>
					</div>
				</div>
								
				<div class="uibutton-toolbar extend">
					<s:a action="procurement/request" cssClass="uibutton icon prev">Kembali</s:a>
					
				</div>
						
			</s:form>
			
		</div>
	</div>
</body>
</html>