
<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Pembuatan OE</title>
	
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	
	<script type="text/javascript">	
		$(document).ready(function(){
			$(".chzn").chosen();
			// FORM VALIDATION
			$("#procForm").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler:function(form){
					//re format
					FreezeScreen('Data Sedang di Proses');
					var size = $("#size").val();
					for(var i=0;i<size;i++){
						var price = 'priceOe'+i;
						var totalPrice = 'totalPriceOe'+i;
						var totalAll = 'totaloe'+i;
						$('#'+price).val($('#'+price).autoNumeric('get'));
						$('#'+totalPrice).val($('#'+totalPrice).autoNumeric('get'));
						$('#'+totalAll).val($('#'+totalAll).autoNumeric('get'));
					}
					form.submit();
				}
			});
			
			//format sumber
			var size = $("#size").val();
			for(var i=0;i<size;i++){
				var price = 'priceOe'+i;
				var totalPrice = 'totalPriceOe'+i;
				var totalAll = 'totaloe'+i;
				$('#'+price).autoNumeric('init', {vMin: 0, mDec: 3});
				$('#'+totalPrice).autoNumeric('init');
				$('#'+totalAll).autoNumeric('init');
			}			
		});
		
		//regCancel Action
		function regCancel(){
			jQuery.facebox.close();
			window.location = "<s:url action="oe/create" />"; 
		}
		
		function hitungSubTotal(){
			var jumlah = parseFloat($('#jumlahItem').val());
			var hargaSatuan = parseFloat($('#hargaSatuanItem').val());
			
			if(!isNaN(jumlah) && !isNaN(hargaSatuan)){
				$('#hargaTotalItem').val(jumlah*hargaSatuan);
			}else{
				$('#hargaTotalItem').val(0);
			}
		}
		
		function calculateData(obj,index, sizeList){
			var totalAllOE = 0;	
			var totalPriceOe=0.0;
			var varPriceOe = 'priceOe'+index;
			var itemQtyOe = parseFloat(document.forms[0].elements['listPrcMainItem['+index+'].itemQuantityOe'].value);
			//var itemPriceOe = parseFloat($('#'+varPriceOe).autoNumeric('get'));
			var itemPriceOe = parseFloat($('#'+varPriceOe).autoNumeric('get'));
			var itemTaxOe = parseFloat(document.forms[0].elements['listPrcMainItem['+index+'].itemTaxOe'].value);
			var itemQtyOe = parseFloat(document.forms[0].elements['listPrcMainItem['+index+'].itemQuantityOe'].value);
			totalPriceOe = itemQtyOe * itemPriceOe;
			//totalAllOE += totalPriceOe;
			if(isNaN(totalPriceOe))
				totalPriceOe=0;
			var totalPpn = totalPriceOe * ((itemTaxOe+100)/100);
			if(isNaN(totalPpn))
				totalPpn=0;
			//init again
			var totalPrice = 'totalPriceOe'+index
			$('#'+totalPrice).autoNumeric('set',totalPriceOe);
			var totalAll = 'totaloe'+index;
			$('#'+totalAll).autoNumeric('set',totalPpn);
			var itotal = 0;
			for(var i=0; i<sizeList; i++){
				
				var total =  parseFloat($('#'+'totaloe'+i).autoNumeric('get'));
				totalAllOE = totalAllOE + total;
			}
			$("#totalAllOE").autoNumeric('set',totalAllOE);

			
		}
		
	</script>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="dashboard">Home</s:a></li> 
	        <li>Managemen Pengadaan</li> 
	        <li>Perencanaan Pengadaan</li> 
	        <li><s:a action="oe/create">Pembuatan OE</s:a></li>
	        
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
    
	<div id="right">
		<div class="section" style="background-color: white">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
					
			<s:form action="oe/create" id="procForm" name="procForm" enctype="multipart/form-data" method="post">
				<s:hidden name="prcMainHeader.ppmId" />
				<s:hidden name="prcMainHeader.admProses.id"></s:hidden>
				<s:hidden name="prcMainHeader.ppmProsesLevel"></s:hidden>
				
				<div class="box">
					<div class="title">
						Header
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle" >
							<tr>
								<td width="150px">No. PR</td>
								<td>
									${prcMainHeader.ppmNomorPr }
								</td>
							</tr>
							<tr>
								<td width="150px">Pembuat</td>
								<td>${prcMainHeader.namaPembuatPr }</td>
							</tr>
							<tr>
								<td>Departemen</td>
								<td>
									${prcMainHeader.admDept.deptName }
								</td>
							</tr>
							<tr>
								<td>Kantor</td>
								<td>
									${prcMainHeader.admDistrict.districtName }
								</td>
							</tr>
							<tr>
								<td width="150px">Nama Permintaan</td>
								<td>${prcMainHeader.ppmSubject }</td>
							</tr>
							<tr>
								<td>Deskripsi Permintaan</td>
								<td>${prcMainHeader.ppmDescription } </td>
							</tr>
							<tr>
								<td>Jenis Permintaan</td>
								<td>
									<c:if test="${prcMainHeader.ppmJenisDetail == null}">
										<c:if test="${prcMainHeader.ppmJenis.id == 1 }">Barang</c:if>
										<c:if test="${prcMainHeader.ppmJenis.id == 2 }">Jasa</c:if>
									</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 3 }">Barang Raw Material</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 4 }">Barang Sparepart</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 5 }">Barang Umum</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 6 }">Jasa Teknik</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 7 }">Jasa Umum</c:if>
								</td>
							</tr>
							
							<tr>
								<td>Prioritas</td>
								<td>
									<c:if test="${prcMainHeader.ppmPrioritas == 1 }">High</c:if>
									<c:if test="${prcMainHeader.ppmPrioritas == 2 }">Normal</c:if>
									<c:if test="${prcMainHeader.ppmPrioritas == 3 }">Low</c:if>
								</td>
							</tr>
							
							<tr>
								<td>Chatting</td>
								<td>
								
									<a href="javascript:void(0)" onclick="load_into_box('<s:url action="ajax/chatInternal"/>','prcMainHeader.ppmId=${prcMainHeader.ppmId }')"><span id="chat">Chatting Disini</span></a>
								</td>
							</tr>
							<tr>
								<td>Attachment</td>
								<td>
									${prcMainHeader.attachment }									
								</td>
							</tr>
							<tr>
								<td>File</td>
								<td>
									<c:if test="${prcMainHeader.attachment!=null and prcMainHeader.attachment!='' }">
										<a href="${contextPath }/upload/file/${prcMainHeader.attachment}" target="_blank">Download</a>
									</c:if>
								</td>
							</tr>
							<%-- <tr>
								<td>Tipe Permintaan (*)</td>
								<td><s:select list="" name="prcMainHeader.ppmTipe"></s:select></td>
							</tr>
							<tr>
								<td>Tanggal Dibutuhkan</td>
								<td><s:textfield name="prcMainHeader.vendorContactEmail"></s:textfield></td>
							</tr>
							--%>
							<tr>
								<td>Lokasi Penyerahan</td>
								<td><s:textfield name="prcMainHeader.lokasiPenyerahan" cssClass="required"></s:textfield></td>
							</tr>
						</table>
						
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Item
						<span class="hide"></span>
					</div>
					<div class="content center">
						<script type="text/javascript">var totalEE=0,totalOE=0;</script>						
						<input type="hidden" id="size" value="${listPrcMainItem.size() }"/>						
						<s:set id="i" value="0"></s:set>
						<display:table name="${listPrcMainItem }" id="s" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
							<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
							<display:column title="Kode" style="width: 70px;" class="center">${s.itemCode }</display:column>
							<display:column title="Deskripsi" class="left">${s.itemDescription }</display:column>
							<display:column title="Keterangan">
								<s:hidden name="listPrcMainItem[%{#i}].id" />
								<s:hidden name="listPrcMainItem[%{#i}].prcMainHeader.ppmId" />
								<s:hidden name="listPrcMainItem[%{#i}].itemTipe" />
								<s:hidden name="listPrcMainItem[%{#i}].itemCode" />
								<s:hidden name="listPrcMainItem[%{#i}].itemDescription" />
								<s:hidden name="listPrcMainItem[%{#i}].itemCostCenter" />
								<s:hidden name="listPrcMainItem[%{#i}].createdBy" />
								<s:hidden name="listPrcMainItem[%{#i}].createdDate" />
								<s:hidden name="listPrcMainItem[%{#i}].attachment" />
								<s:hidden name="listPrcMainItem[%{#i}].fileName" />
								<s:hidden name="listPrcMainItem[%{#i}].lineId" />
								<s:hidden name="listPrcMainItem[%{#i}].itemCategory" />
								<s:hidden name="listPrcMainItem[%{#i}].materialGroup" />
								<s:hidden name="listPrcMainItem[%{#i}].storageLocation" />
								<s:hidden name="listPrcMainItem[%{#i}].purchasingGroup" />
								<s:hidden name="listPrcMainItem[%{#i}].deliveryDate" />
								<s:hidden name="listPrcMainItem[%{#i}].keterangan" />
<%-- 								<c:choose> --%>
<%-- 									<c:when test="${s.fileName!=null and s.fileName!='' }"> --%>
<%-- 										<a href="${contextPath }/upload/file/${s.fileName}" target="_blank">${s.attachment }</a> --%>
<%-- 									</c:when> --%>
<%-- 									<c:otherwise> --%>
<%-- 										${s.attachment } --%>
<%-- 									</c:otherwise> --%>
<%-- 								</c:choose> --%>
								${s.keterangan}
							</display:column>
							<display:column title="Jumlah" style="width: 40px;text-align:right"  >
								${s.itemQuantity }<s:hidden name="listPrcMainItem[%{#i}].itemQuantity" />
								<br />
								<input type="text" name="listPrcMainItem[${s_rowNum-1 }].itemQuantityOe" readonly="readonly" <c:if test="${s.itemQuantityOe==null }">value="${s.itemQuantity}"</c:if> <c:if test="${s.itemQuantityOe!=null }"> value="${s.itemQuantityOe }"</c:if> style="width:inherit;color: red;font-weight: bold;text-align: right" onkeyup="calculateData(this,${s_rowNum-1});"/>
							</display:column>
							<display:column title="Satuan" style="width: 40px;text-align:right">
								${s.admUom.uomName }
								<s:hidden name="listPrcMainItem[%{#i}].admUom.uomId"></s:hidden>
							</display:column>
							<display:column title="Harga Satuan" style="width: 120px;text-align:right">
								<span style="font-size: 11px;"><fmt:formatNumber> ${s.itemPrice } </fmt:formatNumber></span>
								<s:hidden name="listPrcMainItem[%{#i}].itemPrice" />
								<br />
								<input type="text" id="priceOe${s_rowNum-1 }" name="listPrcMainItem[${s_rowNum-1 }].itemPriceOe" <c:if test="${s.itemPriceOe==null }">value="${s.itemPrice }"</c:if> <c:if test="${s.itemPriceOe!=null }"> value="${s.itemPriceOe }"</c:if> style="width:inherit; color:red;font-weight: bold;text-align: right" onkeyup="calculateData(this,${s_rowNum-1},${listPrcMainItem.size()});"/>
							</display:column>
							<display:column title="Pajak (%)" style="width: 40px;text-align:right">
								${s.itemTax }
								<s:hidden name="listPrcMainItem[%{#i}].itemTax" />
								<br />
								<input type="text" name="listPrcMainItem[${s_rowNum-1 }].itemTaxOe" <c:if test="${s.itemTaxOe==null }">value="${s.itemTax }"</c:if> <c:if test="${s.itemTaxOe!=null }"> value="${s.itemTaxOe }"</c:if> style="width:inherit;color:red;font-weight: bold;text-align: right" onkeyup="calculateData(this,${s_rowNum-1},${listPrcMainItem.size()});"/>
								<input type="hidden" value="${s.itemTax }" id="itemTax-${i }">
								<input type="hidden" value="${s.itemTaxOe }" id="itemTaxOe-${i }">
							</display:column>
							<display:column title="Sub Total"  style="text-align:right">
								<span style="font-size: 11px;"><fmt:formatNumber> ${s.itemTotalPrice }</fmt:formatNumber>.00</span>
								<s:hidden name="listPrcMainItem[%{#i}].itemTotalPrice" />
								<br />
								<input type="text" id="totalPriceOe${s_rowNum-1 }" name="listPrcMainItem[${s_rowNum-1 }].itemTotalPriceOe" <c:if test="${s.itemTotalPriceOe==null }">value="${s.itemTotalPrice }"</c:if> <c:if test="${s.itemTotalPriceOe!=null }"> value="${s.itemTotalPriceOe }"</c:if>  style="width:inherit;color:red;font-weight: bold;text-align: right" readonly="readonly"/>
								<input type="hidden" value="${s.itemTotalPrice }" id="itemTotalPrice-${i }">
								<input type="hidden" value="${s.itemTotalPriceOe }" id="itemTotalPriceOe-${i }">
							</display:column>
							<display:column title="Total + Pajak" class="center" >
								<script type="text/javascript">
									$(document).ready(function(){
										var itax = parseFloat($('#itemTax-${i }').val());
										var itotal = parseFloat($('#itemTotalPrice-${i }').val());
										var itotaloe = parseFloat($('#itemTotalPriceOe-${i }').val());
										var totalAll = 'total-'+${s_rowNum-1};
										var total = 0;
										$('#'+totalAll).autoNumeric('init');
										$('#totaloe${i }').autoNumeric('init');
										if(!isNaN(itax) && !isNaN(itotal)){
											$('#'+totalAll).autoNumeric('set',itax / 100 * itotal + itotal);
											if(!isNaN(itotaloe)){
												$('#totaloe${i }').autoNumeric('set',itax / 100 * itotaloe + itotaloe );
												totalOE = totalOE + (itax / 100 * itotaloe + itotaloe);
												totalEE = totalEE + (itax / 100 * itotal + itotal);
											}else{
												$('#totaloe${i }').autoNumeric('set',itax / 100 * itotal + itotal );	
												totalOE = totalOE + (itax / 100 * itotal + itotal);
												totalEE = totalEE + (itax / 100 * itotal + itotal);
											}
										}else{
											$('#'+totalAll).autoNumeric('set',0);
											$('#totaloe${i }').autoNumeric('set',0 );
										}
										//totalEE = totalOE;
										
										$("#totalAllOE").autoNumeric('init');
										$("#totalAllOE").autoNumeric('set',totalOE);
										
										$("#totalAllEE").autoNumeric('init');
										$("#totalAllEE").autoNumeric('set',totalEE);
										
									});
									
								</script>
								<input type="text" id="total-${i }" readonly="readonly" style="width:inherit;font-weight:bold;text-align:right"/>
								<input type="text" readonly="readonly" name="totaloe-${i }" id="totaloe${i }" style="width: inherit;color: red;font-weight: bold; text-align: right"/>
							</display:column>
							<display:footer>
								<tr style="font-weight: bold;">
									<td colspan="7" style="color:red">&nbsp;</td>
									<td style="text-align: right;">Total EE</td>
									<td style="text-align: right;">
									<input type="text" readonly="readonly" id="totalAllEE" style="width:inherit;text-align:right;color:black;font-weight:bold"/>
									</td>
								</tr>
								<tr style="font-weight: bold;">
									<td colspan="7" style="color:red">&nbsp;</td>
									<td style="text-align: right;color:red">Total OE</td>
									<td style="text-align: right;">
									<input type="text" readonly="readonly" id="totalAllOE" style="width:inherit;text-align:right;color:red;font-weight:bold"/>
									</td>
								</tr>
								<tr>
									<td colspan="9" style="font-weight: bold;">(* Nilai OE berhuruf tebal Merah</td>
								</tr>
							</display:footer>
							
						<s:set var="i" value="#i+1"></s:set>
						</display:table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Estimator
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle">
							<tr>
								<td width="150px;">Estimator</td>
								<td>${prcMainHeader.admUserByPpmEstimator.completeName }</td>
							</tr>						
						</table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Daftar Komentar
						<span class="hide"></span>
					</div>
					<div class="content center">
						<display:table name="${listHistMainComment }" id="k" excludedParams="*" style="width: 100%;" class="style1" pagesize="1000">
							<display:column title="No." style="width: 20px; text-align: center;">${k_rowNum}</display:column>
							<display:column title="Nama" style="width: 100px;" class="center">${k.username }</display:column>
							<display:column title="Posisi" style="width: 100px;" class="center">${k.posisi }</display:column>
							<display:column title="Proses" style="width: 100px;" class="center">${k.prosesAksi }</display:column>
							<display:column title="Tgl Pembuatan" style="width: 100px;" class="center">
								<fmt:formatDate value="${k.createdDate }" pattern="dd.MM.yyyy HH:mm"/> 
							</display:column>
							<display:column title="Tgl Update" style="width: 100px;" class="center">
								<fmt:formatDate value="${k.updatedDate }" pattern="dd.MM.yyyy HH:mm"/> 
							</display:column>
							<display:column title="Aksi" class="center" style="width: 250px;">${k.aksi }</display:column>
							<display:column title="Komentar" class="center" style="width: 250px;">${k.comment }</display:column>
							<display:column title="Dokumen" class="center" style="width: 200px;">
								<a href="${contextPath }/upload/file/${k.document }" target="_blank">${k.document }</a>
							</display:column>
						</display:table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Pakta Integritas
						<span class="hide"></span>
					</div>
					<div class="content">
							<p align="justify">
							Saya, yang membuat permintaan pengadaan barang/jasa nomor : &nbsp;&nbsp;<b>${prcMainHeader.ppmNomorPr }</b> <br/>
							Dengan ini menyatakan dengan sebenarnya bahwa : </p>
							<ol>
								<li>Barang/jasa tersebut adalah benar dibutuhkan untuk menunjang kegiatan operasional perusahaan</li>
								<li>Dalam pengadaan barang/jasa tersebut saya tidak memiliki kepentingan pribadi atau tujuan untuk melakukan sesuatu manfaat bagi diri saya sendiri, atau melakukan praktek KKN (KORUPSI, KOLUSI NEPOTISME) dalam pengadaan barang/jasa tersebut</li>
							</ol>
							<p align="justify">
							Demikian pernyataan ini kami sampaikan dengan sebenar-benarnya, tanpa menyembunyikan fakta dan hal material apapun dan dengan demikian kami akan bertanggung jawab sepenuhnya atas kebenaran dari hal-hal yang kami nyatakan disini. <br/>
							Demikian pernyataan ini kami buat untuk digunakan sebagaimana mestinya.
							</p>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Komentar
						<span class="hide"></span>
					</div>
					<div class="content">
						<s:hidden name="histMainComment.username" value="%{admEmployee.empFullName}" />
						<table class="form formStyle">
							<tr>
								<td width="150px;">Komentar (*)</td>
								<td><s:textarea name="histMainComment.comment" id="komentarVal" cssClass="required"></s:textarea> </td>
							</tr>	
							<tr>
								<td width="150px;">Lampiran</td>
								<td><s:file name="docsKomentar" cssClass="uibutton" /></td>
							</tr>							
						</table>
					</div>
				</div>
				
				<div class="uibutton-toolbar extend">
					<s:submit name="saveAndFinish" id="saveAndFinish" onclick="actionAlert('saveAndFinish','simpan dan selesai');return false;" value="Simpan dan Selesai" cssClass="uibutton"></s:submit>
					<s:submit name="save" value="Draft" cssClass="uibutton"></s:submit>
					<%-- <s:submit name="sap" value="Validate" cssClass="uibutton"></s:submit> --%>
					<%-- <s:submit name="revisiee" value="REVISI EE" cssClass="uibutton" cssStyle="color:red"></s:submit> --%>
					<%-- <s:submit name="upload" value="UPLOAD" cssClass="uibutton" cssStyle="color:red"></s:submit> --%>
					
					<a href="javascript:void(0)" class="uibutton" onclick="confirmPopUp('regCancel();', 'Batal', 'Apakah anda yakin untuk membatalkan transaksi ?', 'Ya', 'Tidak');"><s:text name="cancel"></s:text> </a>					
				</div>
						
			</s:form>
			
		</div>
	</div>
</body>
</html>