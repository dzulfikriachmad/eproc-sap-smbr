
<%@ include file="/include/definitions.jsp"%>
<center>
	<!-- Alert -->
	<s:if test="hasActionErrors()">
		<div class="message red">
			<s:actionerror />
		</div>
	</s:if>
	<s:if test="hasActionMessages()">
		<div class="message green">
			<s:actionmessage />
		</div>
	</s:if>

	<s:set id="i" value="0"></s:set>

	<display:table name="${ListEauctionVendor }" id="s" pagesize="1000"
		excludedParams="*" style="width: 100%;" class="style1">
		<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
		<display:column title="Nama Mitra Kerja" style="width: 100px;"
			class="center">
			<a href="javascript:void(0)"
				onclick="load_into_box('<s:url action="ajax/vendor/detail"/>','tmpVndHeader.vendorId=${s.vndHeader.vendorId }')">${fn:toUpperCase(s.vndHeader.vendorName) }</a>
		</display:column>
		<display:column title="Alamat" style="width: 150px;" class="center">
								${fn:toUpperCase(s.vndHeader.vendorNpwpCity) }	
							</display:column>
		<display:column title="Status" style="width: 150px;" class="center">
								<c:if test="${s.status==-1 }"><span style="color: red"> Didiskualifikasi </span></c:if> <c:if test="${s.status==null }"><span style="color: blue">Aktif</span></c:if>
							</display:column>

		<display:column title="" media="html" style="width: 30px;"
			class="center">
			
			<input type="button" class="uibutton" <c:if test="${s.status==-1 }"> value="Aktifkan"</c:if> <c:if test="${s.status==null }">value="Diskualifikasi"</c:if>
				onclick=" confirmPopUp('diskualifikasi(${s.id});', 'Batal', 'Apakah anda yakin untuk Men- DISKUALIFIKASI vendor ini ? ?', 'Ya', 'Tidak');"/>
		</display:column>
		<s:set var="i" value="#i+1"></s:set>
	</display:table>
</center>
