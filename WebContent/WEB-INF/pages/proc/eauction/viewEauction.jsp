<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Eauction</title>
	
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	
	<script type="text/javascript">	
		$(document).ready(function(){
			$(".chzn").chosen();
			
			$.datepicker.setDefaults($.datepicker.regional['in_ID']);
			$("#tglMulai").datetimepicker({
				changeYear: true,
				changeMonth: true
			});
			$("#tglBerakhir").datetimepicker({
				changeYear: true,
				changeMonth: true
			});
			
			// FORM VALIDATION
			$("#procForm").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler:function(form){
					loadingScreen();
					form.submit();
				}
			});
			
			
			
			loadVendor();
			getEauctionTime();
			callDetailEauction();
			//invokeRemoteTimer();
			
		});
		
		
		//regCancel Action
		function regCancel(){
			jQuery.facebox.close();
			window.location = "<s:url action="rfq/eauction" />"; 
		}
		
		 //Server Time
		function getEauctionTime(){
			$.ajax({
		        url: "<s:url action="timerEauctionJson" />?ppmId=${prcMainHeader.ppmId}",
		        success: function(response){
		        	var remaining = response.remaningTime;
		        	if(remaining==0 || remaining <=1){
		        		$('#btn').hide();
		        		$('#cls').show();
		        		clearInterval(countdown);
		        	}else{
		        		$('#btn').show();
		        		$('#cls').hide();
		        	}
		        	countdownTime(remaining);
		        	/* var countdown = setInterval(function(){
			        	if (remaining < 1){
			        		remaining = 0;
			        		clearInterval(countdown);
			        	}else{
			        		remaining--;
			        	}
			        	
		        	}, 1000); */
		        	
		            //setTimeEauction(remaining);
		        	//setTimeEauction(remaining);
		        	//getDetailEauction();
		        	$('#startMessage').html(response.globalMessage);
		        	console.log(response.globalMessage);
		        },
		        type: "post", 
		        dataType: "json"
		    }); 
		}
		
		
		
		
		function countdownTime(time){
			var countdown = setInterval(function(){
				$('#timeEauction').html(time);
	        	if (time < 1){
	        		time = 0;
	        		clearInterval(countdown);
	        	}else{
	        		time--;
	        	}
	        	
        	}, 1000);
					
		}
	    function setTimeEauction(remaining){
	    	$('#timeEauction').html(remaining);
 	    	setTimeout("getEauctionTime()",1000);
	    }
	   
	    function callDetailEauction(){	    	
	    	setTimeout("getDetailEauction()",1000);
	    	//setTimeout("getEauctionTime()",1000);
	    }
	   
	    function getDetailEauction(){
	    	$.ajax({
	            url: "<s:url action="ajax/detailEauction" />?ppmId=${prcMainHeader.ppmId}",
	            success: function(response){
	                $("#hasilEauction").html(response);
	                callDetailEauction();
	            },
	            dataType:"html"  		
	        });
	    }
	    
	    function invokeRemoteTimer(){
	    	$.ajax({
		        url: "${vendorUrl}/initTimerEauctionJson.jwebs?ppmId=${prcMainHeader.ppmId}",
		        success: function(response){
					//do nothing
		        },
		        type: "post", 
		        dataType: "json"
		    });
	    }
	    
	    
	    
	    function loadVendor(){
	    	$.ajax({
	    		url: "<s:url action="ajax/detailVendorEauction" />?eauctionHeader.ppmId=${eauctionHeader.ppmId}&prcMainHeader.ppmId=${prcMainHeader.ppmId}",
		        success: function(response){
		        	$("#vendorEauction").html(response);
		        },
		        type: "post", 
		        dataType: "html"
		    });
	    }
	    
	    function diskualifikasi(id){
	    	$.ajax({
		        url: "<s:url action="ajax/diskualifikasiEauctionJson" />?eauctionVendorId="+id,
		        success: function(response){
		        	loadVendor();
		        },
		        type: "post", 
		        dataType: "json"
		    }); 
	    	jQuery.facebox.close();
	    }
		
	   
	    
	</script>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="dashboard">Home</s:a></li> 
	        <li>Managemen Pengadaan</li> 
	        <li>Perencanaan Pengadaan</li> 
	        <li><s:a action="rfq/eauction">Eauction</s:a></li>
	        
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
    
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
					
			<s:form action="rfq/eauction" id="procForm" name="procForm" enctype="multipart/form-data" method="post">
				<s:hidden name="prcMainHeader.ppmId" />
				<s:hidden name="eauctionHeader.ppmId" />
				<s:hidden name="eauctionHeader.latihan" id="latihanVal"/>
				<s:hidden name="eauctionHeader.prcMainHeader.ppmId" value="%{prcMainHeader.ppmId}"/>
				
				<div class="box">
					<div class="title">
						Header
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle" >
							<tr>
								<td width="150px">No. SPPH</td>
								<td>
									${prcMainHeader.ppmNomorRfq }
								</td>
							</tr>
							<tr>
								<td width="150px">No. PR</td>
								<td>
									${prcMainHeader.ppmNomorPr }
								</td>
							</tr>
							
							<tr>
								<td width="150px">Nama Permintaan  (*)</td>
								<td>${prcMainHeader.ppmSubject }</td>
							</tr>
							<tr>
								<td>Deskripsi Permintaan (*)</td>
								<td>${prcMainHeader.ppmDescription } </td>
							</tr>
							<tr>
								<td>Jenis Permintaan (*)</td>
								<td>
									<c:if test="${prcMainHeader.ppmJenisDetail == null}">
										<c:if test="${prcMainHeader.ppmJenis.id == 1 }">Barang</c:if>
										<c:if test="${prcMainHeader.ppmJenis.id == 2 }">Jasa</c:if>
									</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 3 }">Barang Raw Material</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 4 }">Barang Sparepart</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 5 }">Barang Umum</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 6 }">Jasa Teknik</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 7 }">Jasa Umum</c:if>
								</td>
							</tr>
							<tr>
								<td>Chatting</td>
								<td>
								
									<a href="javascript:void(0)" onclick="load_into_box('<s:url action="ajax/chatInternal"/>','prcMainHeader.ppmId=${prcMainHeader.ppmId }')"><span id="chat">Chatting Disini</span></a>
								</td>
							</tr>
							<tr>
								<td>Judul Eauction (*)</td>
								<td>
									<s:textfield name="eauctionHeader.judul" cssClass="required"></s:textfield>
								</td>
							</tr>
							
							<tr>
								<td>Deskripsi Eauction (*)</td>
								<td>
									<s:textarea name="eauctionHeader.deskripsi" cssClass="required"></s:textarea>
								</td>
							</tr>
							<tr>
								<td>Tanggal Mulai (*)</td>
								<td>
									<s:textfield name="eauctionHeader.tanggalMulai" id="tglMulai" cssClass="required"></s:textfield>
								</td>
							</tr>
							
							<tr>
								<td>Waktu (*menit)</td>
								<td>
									<s:textfield name="eauctionHeader.waktu" cssClass="required number"></s:textfield>
								</td>
							</tr>

							<tr>
								<td>HPS Eauction (*)</td>
								<td>
									<s:textfield name="eauctionHeader.hps" value="%{prcMainHeader.ppmJumlahOe}" readonly="true"></s:textfield>
								</td>
							</tr>
						</table>
						
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Item
						<span class="hide"></span>
					</div>
					<div class="content center">
						<script type="text/javascript">var totalEE=0,totalOE=0;</script>						
						<s:set id="i" value="0"></s:set>
						<display:table name="${listItem }" id="s" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
							<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
							<display:column title="Kode" style="width: 70px;" class="center">${s.itemCode }</display:column>
							<display:column title="Cost Center" style="width: 150px;" class="center">
								${s.itemCostCenter }
							</display:column>
							<display:column title="Deskripsi" class="left">${s.itemDescription }</display:column>
							<display:column title="Jumlah" style="text-align:right">
								<fmt:formatNumber> ${s.itemQuantity }</fmt:formatNumber>
								<hr />
								<span style="font-weight:bold;"><fmt:formatNumber> ${s.itemQuantityOe }</fmt:formatNumber></span>
							</display:column>
							<display:column title="Harga Satuan" style="text-align:right">
								<fmt:formatNumber> ${s.itemPrice }</fmt:formatNumber>
								<hr />
								<span style="font-weight:bold;"><fmt:formatNumber> ${s.itemPriceOe }</fmt:formatNumber></span>
							</display:column>
							<display:column title="Pajak (%)" style="text-align:right">
								${s.itemTax }
								<hr />
								<span style="font-weight:bold;">${s.itemTaxOe }</span>
								<input type="hidden" value="${s.itemTax }" id="itemTax-${i }">
								<input type="hidden" value="${s.itemTaxOe }" id="itemTaxOe-${i }">
							</display:column>
							<display:column title="Sub Total" style="text-align:right">
								<fmt:formatNumber> ${s.itemTotalPrice }</fmt:formatNumber>
								<hr />
								<span style="font-weight:bold;"><fmt:formatNumber> ${s.itemTotalPriceOe }</fmt:formatNumber></span>
								<input type="hidden" value="${s.itemTotalPrice }" id="itemTotalPrice-${i }">
								<input type="hidden" value="${s.itemTotalPriceOe }" id="itemTotalPriceOe-${i }">
							</display:column>
							<display:column title="Total + Pajak" style="text-align:right">
								<script type="text/javascript">
									$(document).ready(function(){
										var itax = parseFloat($('#itemTax-${i }').val());
										var itotal = parseFloat($('#itemTotalPrice-${i }').val());
										var totalAll = 'total-'+${i};
										$('#'+totalAll).autoNumeric('init');
										if(!isNaN(itax) && !isNaN(itotal)){
											$('#'+totalAll).autoNumeric('set',(itax / 100 * itotal + itotal));
										}else{
											$('#'+totalAll).autoNumeric('set',0);
										}
										$("#totalAllEE").autoNumeric('init');
										totalEE = totalEE + (itax / 100 * itotal + itotal);
										$("#totalAllEE").autoNumeric('set',totalEE);
										
										var totalAllOe = 'totaloe-'+${i};
										$('#'+totalAllOe).autoNumeric('init');
										var itax2 = parseFloat($('#itemTaxOe-${i }').val());
										var itotal2 = parseFloat($('#itemTotalPriceOe-${i }').val());

										if(!isNaN(itax2) && !isNaN(itotal2)){
											$('#'+totalAllOe).autoNumeric('set',(itax2 / 100 * itotal2 + itotal2));
										}
										$("#totalAllOE").autoNumeric('init');
										totalOE = totalOE + (itax2 / 100 * itotal2 + itotal2);
										$("#totalAllOE").autoNumeric('set',totalOE);
									});
									
								</script>
								<input type="text" readonly="readonly" id="total-${i }" style="width:inherit;text-align:right"/>
								<hr/>
								<input type="text" readonly="readonly" id="totaloe-${i }" style="width:inherit;text-align:right"/>
							</display:column>
							<display:footer>
								<tr>
									<td colspan="7" style="color:red">&nbsp;</td>
									<td style="text-align: right;">Total EE</td>
									<td style="text-align: right;">
									<input type="text" readonly="readonly" id="totalAllEE" style="width:inherit;text-align:right"/>
									</td>
								</tr>
								<tr style="font-weight: bold;">
									<td colspan="7" style="color:red">&nbsp;</td>
									<td style="text-align: right;">Total OE</td>
									<td style="text-align: right;">
									<input type="text" readonly="readonly" id="totalAllOE" style="width:inherit;text-align:right"/>
									</td>
								</tr>
								<tr>
									<td colspan="9" style="font-weight: bold;">(* Nilai OE berhuruf tebal</td>
								</tr>
							</display:footer>
							
						<s:set var="i" value="#i+1"></s:set>
						</display:table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Daftar Mitra Kerja Eauction
						<span class="hide"></span>
					</div>
					<div class="content center">
						<%-- <s:set id="i" value="0"></s:set>

						<display:table name="${ListEauctionVendor }" id="s" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
							<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
							<display:column title="Nama Mitra Kerja" style="width: 100px;" class="center">
								<a href="javascript:void(0)" onclick="load_into_box('<s:url action="ajax/vendor/detail"/>','tmpVndHeader.vendorId=${s.vndHeader.vendorId }')">${fn:toUpperCase(s.vndHeader.vendorName) }</a>
							</display:column>
							<display:column title="Alamat" style="width: 150px;" class="center">
								${fn:toUpperCase(s.vndHeader.vendorNpwpCity) }	
							</display:column>
							
							<display:column title="" media="html" style="width: 30px;" class="center">
								<input type="button" value="diskualifikasi" onclick="diskualifikasi(${s.id})"/>
							</display:column>
						<s:set var="i" value="#i+1"></s:set>
						</display:table> --%>
						<div id="vendorEauction"></div>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Sisa Waktu
						<span class="hide"></span>
					</div>
					<div class="content center">
						<span id="startMessage" style="font-weight: bolder;font-size: large;color: red;float: left"></span>
						<span id="timeEauction" style="font-weight: bolder;font-size: x-large;color: red"></span>
					</div>
				</div>
				<div class="box">
					<div class="title">
						Monitoring Eauction
						<span class="hide"></span>
					</div>
					<div class="content">
						<div id="hasilEauction"></div>
					</div>
				</div>

				<div class="uibutton-toolbar extend">
                  <c:choose>
                  	<c:when test="${eauctionHeader.latihan == 1 }">
                  		<s:submit name="save" value="Simpan / Update Waktu" cssClass="uibutton" cssStyle="display:none;"></s:submit>
                  		<s:submit name="saveLatihan" value="Simpan Data latihan" cssClass="uibutton" id="saveLat"></s:submit>
                  		<s:submit name="resume" value="Mulai Lagi" cssClass="uibutton"></s:submit>
                 		<s:submit name="pause" value="Stop Latihan" cssClass="uibutton"></s:submit>
                  		
                  	</c:when>
               
                  	<c:otherwise>
                  		<s:submit name="save" value="Simpan / Update Waktu" cssClass="uibutton"></s:submit>
                  	</c:otherwise>
                  
                  </c:choose>
                  <c:choose>
                  	<c:when test="${eauctionHeader.latihan == 2}">
                  		<s:submit name="latihan" value="Latihan" cssClass="uibutton" cssStyle="display:none;"></s:submit>
                  	</c:when>
                  	<c:otherwise>
                  		<s:submit name="latihan" value="Latihan" cssClass="uibutton"></s:submit>
                  	</c:otherwise>
                  </c:choose>
                   
                  
                 
                  
					<span id="cls" style="display : none">
				</span>	
					
					<s:submit name="reset" value="Reset Data" onclick="return confirm('Apakah yakin akan mereset data eauction?');" cssStyle="color:red;" cssClass="uibutton" id="resetEauctionBtn"></s:submit>
				
					<a href="javascript:void(0)" class="uibutton" onclick="confirmPopUp('regCancel();', 'Batal', 'Apakah anda yakin untuk membatalkan transaksi ?', 'Ya', 'Tidak');">Tutup / Selesai </a>					
				</div>
						
			</s:form>
			
		</div>
	</div>
</body>
</html>