			<%@ include file="/include/definitions.jsp" %>
			<center>
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>		
			
			<div style="margin-top: 20px;"></div>
			<h4>Bid Terakhir</h4>
			<display:table name="${ListModelVendor }" id="s" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column title="Nama Mitra Kerja" style="width: 100px;" class="center">
					${fn:toUpperCase(s.namaVendor) }
				</display:column>
				<display:column title="Bid Terakhir"  class="center">
					<fmt:formatNumber> ${s.lastBid }</fmt:formatNumber>
				</display:column>
				<display:column title="Waktu Bid"  class="center">
					<c:if test="${s.tglBid!=null }">
						${s.tglBidString }
					</c:if>
				</display:column>
				<display:column title="" media="html" style="width: 30px;" class="center">
				</display:column>
			</display:table>
			
			<br/>
			<br/>
			<h4>History Bid</h4>
			<display:table name="${ListHistory }" id="s" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column title="Nama Mitra Kerja" style="width: 100px;" class="center">
					${fn:toUpperCase(s.vendorName) }
				</display:column>
				<display:column title="Jumlah Bid" class="center">
					<fmt:formatNumber>${s.jumlahBid }</fmt:formatNumber>	
				</display:column>
				<display:column title="Waktu Bid"  class="center">
					<c:if test="${s.tglBid!=null }">
						${s.tglBidString }
					</c:if>
				</display:column>
				<display:column title="" media="html" style="width: 30px;" class="center">
					
				</display:column>
			</display:table>
			</center>
			