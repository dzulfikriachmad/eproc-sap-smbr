<%@ include file="/include/definitions.jsp" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Daftar Template</title>
<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
<script type="text/javascript">
	function setId(id){
		$("#id").val(id);
	}
</script>
</head>
<body>
<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="#">Home</s:a></li> 
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
		
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
			
			<div class="uibutton-toolbar extend">
					<a href='javascript:void(0)' class='uibutton icon add' onclick='load_into_box("<s:url action="ajax/proc/crudTemplate" />")' <c:if test="${create!=1 }"> style="visibility: hidden;"</c:if> >Tambah Template</a>
			</div>
			
			
			<div class="box">
				<div class="title center">
					Daftar Template
					<span class="hide"></span>
				</div>
				<div class="content">
					<div id="result">
						<jsp:include page="ajax/listTemplate.jsp"></jsp:include>
					</div>
				</div>
			</div>
				
		</div>
	</div>

</body>
</html>