
<%@ include file="/include/definitions.jsp"%>
<script type="text/javascript">
	$(document).ready(function() {
		// DATATABLE
		$('table.doc').dataTable({
			"bInfo" : false,
			"iDisplayLength" : 10,
			"aLengthMenu" : [ [ 5, 10, 25, 50, 100 ], [ 5, 10, 25, 50, 100 ] ],
			"sPaginationType" : "full_numbers",
			"bPaginate" : true,
			"sDom" : '<f>t<pl>'
		});
	});
</script>
<center>
	<!-- Alert -->
	<s:if test="hasActionErrors()">
		<div class="message red">
			<s:actionerror />
		</div>
	</s:if>
	<s:if test="hasActionMessages()">
		<div class="message green">
			<s:actionmessage />
		</div>
	</s:if>
	<c:set var="totalWeight" value="0"></c:set>
	<s:set id="i" value="0"></s:set>
	<display:table name="${listTemplateDetail }" id="s"
		requestURI="ajax/proc/listTemplateDetail.jwebs" excludedParams="*"
		style="width: 100%;" class="style1" pagesize="1000">
		<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
		<display:column title="Template Detail Name" class="center">${s.tempdetName}</display:column>
		<display:column title="Tipe" class="center">
			<c:if test="${s.tipe==1 }">Administrasi</c:if>
			<c:if test="${s.tipe==2 }">Teknis</c:if>
		</display:column>
		<display:column title="Bobot">
					${s.tempdetWeight }
					<c:set var="totalWeight" value="${totalWeight + s.tempdetWeight }"></c:set>
		</display:column>
		<display:column title="Aksi" media="html" style="width: 80px;"
			class="center">
			<div class="uibutton-group">
				<s:url var="set" action="ajax/submitPrcTemplateDetail" />
				<a title="Hapus data" class="uibutton" href='javascript:void(0)' onclick='deletePopUp("${set}","act=delete&index=${i }&id=${s.id}","#listDetail")'>X</a>
			</div>
			<script type="text/javascript">
			$(document).ready(function() {
				$("#totalWeight").val('${totalWeight}')
			});
			</script>
		</display:column>
		<s:set var="i" value="#i+1"></s:set>
	</display:table>
</center>
