<%@ include file="/include/definitions.jsp"%>
<!-- Alert -->
<s:if test="hasActionErrors()">
	<div class="message red">
		<s:actionerror />
	</div>
</s:if>
<s:if test="hasActionMessages()">
	<div class="message green">
		<s:actionmessage />
	</div>
</s:if>


<display:table name="listTemplate" id="s" requestURI=".jwebs"
	excludedParams="*" style="width: 100%;" class="all style1">
	<display:setProperty name="paging.banner.full" value=""></display:setProperty>
	<display:setProperty name="paging.banner.first" value=""></display:setProperty>
	<display:setProperty name="paging.banner.last" value=""></display:setProperty>

	<display:column title="No."
		style="width: 27px; text-align: center; background: #fafafa;">${s_rowNum }</display:column>
	<display:column title="Nama Template">${s.templateName }</display:column>
	<display:column title="Passing Grade">
		<fmt:formatNumber>${s.templatePassingGrade }</fmt:formatNumber>
	</display:column>
	<display:column title="Bobot Teknis">
		<fmt:formatNumber>${s.templateTechWeight }</fmt:formatNumber>
	</display:column>
	<display:column title="Bobot Harga">
		<fmt:formatNumber>${s.templatePriceWeight }</fmt:formatNumber>
	</display:column>
	<display:column>
		<div class="uibutton-group">
			<s:url var="set" action="ajax/proc/crudTemplate" />
			<a <c:if test="${update !=1 }"> style="visibility:hidden"</c:if> title="Ubah data" class="uibutton icon edit" href='javascript:void(0)' onclick='load_into_box("${set}", "xCommand=edit&template.id=${s.id}");'>&nbsp;</a>
			<a <c:if test="${delete !=1 }"> style="visibility:hidden"</c:if> title="Hapus data Kantor" class="uibutton" href='javascript:void(0)' onclick='confirmPopUp("deleteObj(\"${set}\", \"xCommand=delete&template.id=${s.id}\", \"#result\")", "Hapus Data District", "Yakin ingin menghapus?", "Hapus", "Batal");'>X</a>
		</div>
	</display:column>
</display:table>
