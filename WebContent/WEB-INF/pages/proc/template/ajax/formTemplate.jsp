<%@ include file="/include/definitions.jsp" %>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.chzn').chosen();
			// FORM VALIDATION
			$("#formeproc").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
			});
			
			$("#formeproc").ajaxForm({
		        beforeSubmit: function() {
		            //$('#notif').html('<img src="${contextPath}/assets/images/loading/loading-circle.gif"/>');
		        },
		        success: function(data) {
		            loadPage("<s:url action="ajax/proc/listTemplate"/>","#result");
		            jQuery.facebox.close();
		        }
		    });
		});
		
		function tambahData(){
			if($("#tempdetName").val()==''){
				alert ('Nama Template Detail Harus diisi');
				return false;
			}
			var total = parseFloat($("#totalWeight").val());
			var w = parseFloat($("#tempdetWeight").val());
			if(!isNaN(w)){
				total = total + w;
			}
			if(total>100){
				alert('Total Weight harus <=100');
				return false;
			}
			sendFormAlertPopUp(document.formeproc, "<s:url action="ajax/submitPrcTemplateDetail" />", "#listDetail");
			$("#tempdetName").val('');
			$("#tempdetWeight").val('');
		}
		
		function selectTipe(obj){
			if(obj.value==2){
				$("#bobot").show();
			}else{
				$("#bobot").hide();
			}
			
		}
		
	</script>
	
<div class='fbox_header'>Tambah Template Detail</div>
    <div class='fbox_container'>
    <!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
    <s:form name="formeproc" id="formeproc" action="ajax/proc/crudTemplate" method="post">
        <div class='fbox_content'>
        	<div id="cek"></div>
        		<s:hidden name="template.id"></s:hidden>
            	<s:hidden name="template.createdDate"></s:hidden>
            	<s:hidden name="template.createdBy"></s:hidden>
            	<s:hidden name="template.updatedDate"></s:hidden>
            	<s:hidden name="template.updatedBy"></s:hidden>
            	<table style="min-width: 600px; margin: 0px 10px;" class="form formStyle">   
            		<tr>
            			<td><s:label value="Nama Template (*)"></s:label> </td>
            			<td colspan="3"><s:textfield name="template.templateName" cssClass="required" />
            				<s:hidden id="totalWeight"></s:hidden>
            			</td>
            		</tr>
            		<tr>
            			<td><s:label value="Passing Grade (*)"></s:label> </td>
            			<td colspan="3"><s:textfield name="template.templatePassingGrade" cssClass="required number" /> </td>
            		</tr>
            		<tr>
            			<td><s:label value="Bobot Teknis (*)"></s:label> </td>
            			<td colspan="3"><s:textfield name="template.templateTechWeight" cssClass="required number"/> </td>
            		</tr>
            		<tr>
            			<td><s:label value="Bobot Harga (*)"></s:label> </td>
            			<td colspan="3"><s:textfield name="template.templatePriceWeight" cssClass="required number" /> </td>
            		</tr>     
            	</table>
            	<br/>
            	<br/>
            	<table style="min-width: 600px; margin: 0px 10px;" class="form formStyle">   
            		<caption style="font-weight: bold;text-align: left">Template Detail</caption>
            		<tr>
            			<td><s:label value="Nama Detail(*)"></s:label> </td>
            			<td colspan="3"><s:textfield name="templateDetail.tempdetName" id="tempdetName"/> </td>
            		</tr>
            		<tr>
            			<td><s:label value="Tipe (*)"></s:label> </td>
            			<td colspan="3">
            				<s:select list="#{'1':'Administrasi', '2':'Teknis' }" name="templateDetail.tipe" onchange="selectTipe(this)"></s:select>
            			</td>
            		</tr>
            		<tr id="bobot" style="display: none;">
            			<td><s:label value="Bobot (*)"></s:label> </td>
            			<td colspan="3"><s:textfield name="templateDetail.tempdetWeight" id="tempdetWeight"/> </td>
            		</tr>  
            		<tr>
						<td></td>
						<td><a href="javascript:void(0)" onclick='tambahData();' class="uibutton">Tambah Data</a> </td>
					</tr>  
            	</table>
            	<div id="listDetail">
						<s:include value="listTemplateDetail.jsp" />
				</div>
            
        </div>
        <div class='fbox_footer'>
            <s:submit key="save" name="save" cssClass="uibutton confirm" ></s:submit>
            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()'>Batalkan</a>
        </div>
        </s:form>
    </div>
    