<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Persiapan Pengadaan</title>
	
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	
	<script type="text/javascript">	
		$(document).ready(function(){
			$(".chzn").chosen();
			
			// FORM VALIDATION
			$("#procForm").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler:function(form){
					if($('#jmlOE').val()>1000000000){
						if($('#jumlah').val()<1){
							alert("Anggota Panitia Belum Ditambahkan");
							return false;
						}
					}
					
					FreezeScreen('Data Sedang di Proses');
					form.submit();
				}
			});
			
			//jika harus ada panitia
			if($('#jmlOE').val()>1000000000){
				$("#panitiapengadaan").show();
			}
			
			
		});
		
		//add data panitia
		function tambahAnggota(){
			sendForm(document.procForm, "<s:url action="ajax/submitPanitiaDetail" />", "#listPanitiaDetail");
			$("#agt").val('');
			$("#pos").val('');
		}
		//select pelaksana
		function selectPelaksana(obj){
			/*
			if(obj.value==1){//tunjuk langsung
				$("#staffpengadaan").show();
				$("#panitiapengadaan").hide();
			}else if(obj.value==2 || obj.value==3){//pemilihan langsung 1 sampul dan 2 sampul
				$("#panitiapengadaan").show();
			}else if(obj.value==4 || obj.value==5 || obj.value==6){//lelang 1sampul, 2 sampul, 2 tahap
				$("#panitiapengadaan").show();
			}
			*/
		}
		//regCancel Action
		function regCancel(){
			jQuery.facebox.close();
			window.location = "<s:url action="pp/setup" />"; 
		}
	</script>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="dashboard">Home</s:a></li> 
	        <li>Managemen Pengadaan</li> 
	        <li>Perencanaan Pengadaan</li> 
	        <li><s:a action="pp/setup">Persiapan Pengadaan</s:a></li>
	        
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
    
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
					
			<s:form action="pp/setup" id="procForm" name="procForm" enctype="multipart/form-data" method="post">
				<s:hidden name="prcMainHeader.ppmId" />
				<s:hidden name="prcMainHeader.admProses.id"></s:hidden>
				<s:hidden name="prcMainHeader.ppmProsesLevel"></s:hidden>
				<s:hidden name="prcMainHeader.ppmJenis.id"></s:hidden>
				<input type="hidden" id="jmlOE" value="${prcMainHeader.ppmJumlahOe }"/>
				
				<div class="box">
					<div class="title">
						Header
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle" >
							<tr>
								<td width="150px">No. RFQ</td>
								<td>
									<input type="text" name="prcMainHeader.ppmNomorRfq" style="width:300px" class="required" 
									<c:if test="${fn:contains(prcMainHeader.ppmNomorPrSap,'ZMN') }">value="${fn:replace(prcMainHeader.ppmNomorPrSap,'ZMN','RFQ') }"</c:if>
									 <c:if test="${fn:contains(prcMainHeader.ppmNomorPrSap,'ZST') }">value="${fn:replace(prcMainHeader.ppmNomorPrSap,'ZST','RFQ') }"</c:if>
									 <c:if test="${fn:contains(prcMainHeader.ppmNomorPrSap,'ZST') }">value="${fn:replace(prcMainHeader.ppmNomorPrSap,'ZAD','RFQ') }"</c:if>
									 <c:if test="${fn:contains(prcMainHeader.ppmNomorPrSap,'ZST') }">value="${fn:replace(prcMainHeader.ppmNomorPrSap,'NB','RFQ') }"</c:if>
									 <c:if test="${fn:contains(prcMainHeader.ppmNomorPrSap,'ZST') }">value="${fn:replace(prcMainHeader.ppmNomorPrSap,'FO','RFQ') }"</c:if>
									 <c:if test="${fn:contains(prcMainHeader.ppmNomorPrSap,'ZST') }">value="${fn:replace(prcMainHeader.ppmNomorPrSap,'RV','RFQ') }"</c:if>
									  readonly="readonly"/>
								</td>
							</tr>
							
							<tr>
								<td width="150px">No. PR</td>
								<td>
									${prcMainHeader.ppmNomorPr }
								</td>
							</tr>
							<tr>
								<td width="150px">Pembuat</td>
								<td>${prcMainHeader.namaPembuatPr }</td>
							</tr>
							<tr>
								<td>Departemen</td>
								<td>
									${prcMainHeader.admDept.deptName }
								</td>
							</tr>
							<tr>
								<td>Kantor</td>
								<td>
									${prcMainHeader.admDistrict.districtName }
								</td>
							</tr>
							<tr>
								<td width="150px">Nama Permintaan</td>
								<td>${prcMainHeader.ppmSubject }</td>
							</tr>
							<tr>
								<td>Deskripsi Permintaan</td>
								<td>${prcMainHeader.ppmDescription } </td>
							</tr>
							<tr>
								<td>Jenis Permintaan</td>
								<td>
									<c:if test="${prcMainHeader.ppmJenisDetail == null}">
										<c:if test="${prcMainHeader.ppmJenis.id == 1 }">Barang</c:if>
										<c:if test="${prcMainHeader.ppmJenis.id == 2 }">Jasa</c:if>
									</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 3 }">Barang Raw Material</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 4 }">Barang Sparepart</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 5 }">Barang Umum</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 6 }">Jasa Teknik</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 7 }">Jasa Umum</c:if>
								</td>
								
							</tr>
							
							<tr>
								<td>Prioritas</td>
								<td>
									<c:if test="${prcMainHeader.ppmPrioritas == 1 }">High</c:if>
									<c:if test="${prcMainHeader.ppmPrioritas == 2 }">Normal</c:if>
									<c:if test="${prcMainHeader.ppmPrioritas == 3 }">Low</c:if>
								</td>
							</tr>
							
							<tr>
								<td>Chatting</td>
								<td>
								
									<a href="javascript:void(0)" onclick="load_into_box('<s:url action="ajax/chatInternal"/>','prcMainHeader.ppmId=${prcMainHeader.ppmId }')"><span id="chat">Chatting Disini</span></a>
								</td>
							</tr>
							<tr>
								<td>Attachment</td>
								<td>
									${prcMainHeader.attachment }									
								</td>
							</tr>
							<tr>
								<td>File</td>
								<td>
									<c:if test="${prcMainHeader.attachment!=null and prcMainHeader.attachment!='' }">
										<a href="${contextPath }/upload/file/${prcMainHeader.attachment}" target="_blank">Download</a>
									</c:if>
								</td>
							</tr>
							
							<%-- <tr>
								<td>Tipe Permintaan (*)</td>
								<td><s:select list="" name="prcMainHeader.ppmTipe"></s:select></td>
							</tr>
							<tr>
								<td>Tanggal Dibutuhkan</td>
								<td><s:textfield name="prcMainHeader.vendorContactEmail"></s:textfield></td>
							</tr>
							<tr>
								<td>Lokasi Penyerahan</td>
								<td><s:textfield name="prcMainHeader.vendorContactEmail"></s:textfield></td>
							</tr> --%>
						</table>
						
					</div>
				</div>
				
				
				<div class="box">
					<div class="title">
						Item
						<span class="hide"></span>
					</div>
					<div class="content center">
						<jsp:include page="../tmp/listItem.jsp"></jsp:include>
					</div>
				</div>
				
				
				
				<div class="box">
					<div class="title">
						Estimator
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle">
							<tr>
								<td width="150px;">Estimator</td>
								<td>${prcMainHeader.admUserByPpmEstimator.completeName }</td>
							</tr>						
						</table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Pembeli
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle">
							<tr>
								<td width="150px;">Pembeli</td>
								<td>${prcMainHeader.admUserByPpmBuyer.completeName }</td>
							</tr>						
						</table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Metode Pengadaan
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle">
							<tr>
								<td width="150px;">Metode Pengadaan</td>
								<td>
									<s:select list="listMetode" listKey="id" listValue="methodName" name="prcMainHeader.prcMethod.id" cssClass="chzn required" onchange="selectPelaksana(this)" cssStyle="width:400px"></s:select>
								</td>
							</tr>						
						</table>
					</div>
				</div>
				
				<s:hidden name="prcMainHeader.admUserByPpmAdmBuyer.id" value="%{prcMainHeader.admUserByPpmBuyer.id}"></s:hidden>
				<!--  
				<div class="box" id="staffpengadaan">
					<div class="title">
						Pelaksana Pengadaan
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle">
							<tr>
								<td width="150px;">Staff Pengadaan</td>
								<td>
									<s:select list="listStaffPembeli" listKey="id" listValue="completeName" name="prcMainHeader.admUserByPpmAdmBuyer.id" cssClass="chzn" cssStyle="width:300px"></s:select>
								</td>
							</tr>	
						</table>
					</div>
				</div>
				-->
				<div class="box" id="panitiapengadaan" style="display: none">
					<div class="title">
						Panitia Pengadaan
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle">
							<tr>
								<td width="150px;">Nama Panitia</td>
								<td>
									<s:textfield name="panitia.namaPanitia" cssClass="required"></s:textfield>
								</td>
							</tr>
							<tr>
								<td width="150px;">Keterangan</td>
								<td>
									<s:textfield name="panitia.keterangan" cssClass="required"></s:textfield>
								</td>
							</tr>	
						</table>
						
						<table class="form formStyle">
							<tr>
								<td width="150px;">Anggota</td>
								<td>
									<s:select id="agt" cssStyle="width:300px" list="listUser" listKey="id" listValue="admEmployee.empStructuralPos" cssClass="chzn" name="panitiaDetail.anggota.id"></s:select>
								</td>
							</tr>
							<tr>
								<td width="150px;">Posisi</td>
								<td>
									<s:select id="pos" cssStyle="width:300px" list="#{'1':'Ketua', '2':'Sekretaris', '3':'Anggota' }" name="panitiaDetail.posisi" cssClass="chzn"></s:select>
								</td>
							</tr>
							<tr>
								<td></td>
								<td><a href="javascript:void(0)" onclick='tambahAnggota();' class="uibutton">Tambah Anggota</a> </td>
							</tr>
						</table>
						
						<div id="listPanitiaDetail">
							<s:include value="ajax/listPanitiaDetail.jsp" />
						</div>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Daftar Komentar
						<span class="hide"></span>
					</div>
					<div class="content center">
						<display:table name="${listHistMainComment }" id="k" excludedParams="*" style="width: 100%;" class="style1" pagesize="1000">
							<display:column title="No." style="width: 20px; text-align: center;">${k_rowNum}</display:column>
							<display:column title="Nama" style="width: 100px;" class="center">${k.username }</display:column>
							<display:column title="Posisi" style="width: 100px;" class="center">${k.posisi }</display:column>
							<display:column title="Proses" style="width: 100px;" class="center">${k.prosesAksi }</display:column>
							<display:column title="Tgl Pembuatan" style="width: 100px;" class="center">
								<fmt:formatDate value="${k.createdDate }" pattern="dd.MM.yyyy HH:mm"/> 
							</display:column>
							<display:column title="Tgl Update" style="width: 100px;" class="center">
								<fmt:formatDate value="${k.updatedDate }" pattern="dd.MM.yyyy HH:mm"/> 
							</display:column>
							<display:column title="Aksi" class="center" style="width: 250px;">${k.aksi }</display:column>
							<display:column title="Komentar" class="center" style="width: 250px;">${k.comment }</display:column>
							<display:column title="Dokumen" class="center" style="width: 200px;">
								<a href="${contextPath }/upload/file/${k.document }" target="_blank">${k.document }</a>
							</display:column>
						</display:table>
					</div>
				</div>
				
				
				<div class="box">
					<div class="title">
						Komentar
						<span class="hide"></span>
					</div>
					<div class="content">
						<s:hidden name="histMainComment.username" value="%{admEmployee.empFullName}" />
						<table class="form formStyle">
							<tr>
								<td width="150px;">Komentar (*)</td>
								<td><s:textarea name="histMainComment.comment" cssClass="required" id="komentarVal"></s:textarea> </td>
							</tr>	
							<tr>
								<td width="150px;">Lampiran</td>
								<td><s:file name="docsKomentar" cssClass="uibutton" /></td>
							</tr>							
						</table>
					</div>
				</div>
				
				<div class="uibutton-toolbar extend">
					<s:submit name="saveAndFinish" id="saveAndFinish" onclick="actionAlert('saveAndFinish','simpan dan selesai');return false;" value="Simpan dan Selesai" cssClass="uibutton" ></s:submit>
					<!--  
					<s:submit name="revisi" value="Revisi" cssClass="uibutton"></s:submit>
					-->
					<a href="javascript:void(0)" class="uibutton" onclick="confirmPopUp('regCancel();', 'Batal', 'Apakah anda yakin untuk membatalkan transaksi ?', 'Ya', 'Tidak');"><s:text name="cancel"></s:text> </a>					
				</div>
						
			</s:form>
			
		</div>
	</div>
</body>
</html>