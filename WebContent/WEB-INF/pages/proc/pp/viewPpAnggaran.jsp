<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Anggaran</title>
	
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	
	<script type="text/javascript">	
		$(document).ready(function(){
			$(".chzn").chosen();
			// FORM VALIDATION
			$("#procForm").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler:function(form){
					if($("#jmlAnggaran").val()!=0){
						FreezeScreen('Data Sedang di Proses');
						form.submit();
					}else{
						alert('Anggaran Harus Diisi !!!!');
						return false;
					} 
				}
			});
			
			
		});
		
		//regCancel Action
		function regCancel(){
			jQuery.facebox.close();
			window.location = "<s:url action="pp/anggaran" />"; 
		}
		
		function setItemPopUp(value,keterangan,sisaSaldo){
			load_into_box("<s:url action="ajax/pp/crudAnggaran" />?prcMainHeader.ppmId=${prcMainHeader.ppmId }&anggaranJdeDetail.rkaid="+value.trim()+"&anggaranJdeDetail.keterangan="+keterangan+"&prcMainHeader.ppmJumlahOe="+sisaSaldo);
		}
		
		function generateAnggaran(){
			sendForm(document.procForm, "<s:url action="ajax/generateAnggaranJde" />", "#hasilAnggaran");
		}
	</script>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="dashboard">Home</s:a></li> 
	        <li>Managemen Pengadaan</li> 
	        <li>Perencanaan Pengadaan</li> 
	        <li><s:a action="pp/anggaran">Anggaran</s:a></li>
	        
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
    
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
					
			<s:form action="pp/anggaran" id="procForm" name="procForm" enctype="multipart/form-data" method="post">
				<s:hidden name="prcMainHeader.ppmId" />
				<s:hidden name="prcMainHeader.admProses.id"></s:hidden>
				<s:hidden name="prcMainHeader.ppmProsesLevel"></s:hidden>
				<s:hidden id="jmlOeHeader" value="%{prcMainHeader.ppmJumlahOe}"/>
				<s:hidden id="jmlAnggaran"></s:hidden>
				
				<div class="box">
					<div class="title">
						Header
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle" >
							<tr>
								<td width="150px">No. PR</td>
								<td>
									${prcMainHeader.ppmNomorPr }
								</td>
							</tr>
							<tr>
								<td width="150px">Pembuat</td>
								<td>${prcMainHeader.namaPembuatPr }</td>
							</tr>
							<tr>
								<td>Departemen</td>
								<td>
									${prcMainHeader.admDept.deptName }
								</td>
							</tr>
							<tr>
								<td>Kantor</td>
								<td>
									${prcMainHeader.admDistrict.districtName }
								</td>
							</tr>
							<tr>
								<td width="150px">Nama Permintaan  (*)</td>
								<td>${prcMainHeader.ppmSubject }</td>
							</tr>
							<tr>
								<td>Deskripsi Permintaan (*)</td>
								<td>${prcMainHeader.ppmDescription } </td>
							</tr>
							<tr>
								<td>Jenis Permintaan (*)</td>
								<td>
									<c:if test="${prcMainHeader.ppmJenisDetail == null}">
										<c:if test="${prcMainHeader.ppmJenis.id == 1 }">Barang</c:if>
										<c:if test="${prcMainHeader.ppmJenis.id == 2 }">Jasa</c:if>
									</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 3 }">Barang Raw Material</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 4 }">Barang Sparepart</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 5 }">Barang Umum</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 6 }">Jasa Teknik</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 7 }">Jasa Umum</c:if>
								</td>
							</tr>
							
							<tr>
								<td>Prioritas</td>
								<td>
									<c:if test="${prcMainHeader.ppmPrioritas == 1 }">High</c:if>
									<c:if test="${prcMainHeader.ppmPrioritas == 2 }">Normal</c:if>
									<c:if test="${prcMainHeader.ppmPrioritas == 3 }">Low</c:if>
								</td>
							</tr>
							
							<tr>
								<td>Chatting</td>
								<td>
								
									<a href="javascript:void(0)" onclick="load_into_box('<s:url action="ajax/chatInternal"/>','prcMainHeader.ppmId=${prcMainHeader.ppmId }')"><span id="chat">Chatting Disini</span></a>
								</td>
							</tr>
							<tr>
								<td>Attachment</td>
								<td>
									${prcMainHeader.attachment }									
								</td>
							</tr>
							<tr>
								<td>File</td>
								<td>
									<c:if test="${prcMainHeader.attachment!=null and prcMainHeader.attachment!='' }">
										<a href="${contextPath }/upload/file/${prcMainHeader.attachment}" target="_blank">Donload</a>
									</c:if>
								</td>
							</tr>
							
							<s:hidden name="prcMainHeader.brt" value="0"></s:hidden>
							<!--  
							<tr style="color: RED;font-weight: bold;">
									<td>Melalui BRT</td>
									<td>
										<s:select list="#{'0':'Tidak', '1':'Ya'}" name="prcMainHeader.brt" cssClass="chzn" cssStyle="width:200px"></s:select>
									</td>
								</tr>
							-->
							<%-- <tr>
								<td>Tipe Permintaan (*)</td>
								<td><s:select list="" name="prcMainHeader.ppmTipe"></s:select></td>
							</tr>
							<tr>
								<td>Tanggal Dibutuhkan</td>
								<td><s:textfield name="prcMainHeader.vendorContactEmail"></s:textfield></td>
							</tr>
							<tr>
								<td>Lokasi Penyerahan</td>
								<td><s:textfield name="prcMainHeader.vendorContactEmail"></s:textfield></td>
							</tr> --%>
						</table>
						
					</div>
				</div>
				
				
				<div class="box">
					<div class="title">
						Item
						<span class="hide"></span>
					</div>
					<div class="content center">
						<script type="text/javascript">var totalEE=0,totalOE=0;</script>						
						<s:set id="i" value="0"></s:set>
						<display:table name="${listPrcMainItem }" id="s" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
							<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
							<display:column title="Kode" style="width: 70px;" class="center">${s.itemCode }</display:column>
							<display:column title="Deskripsi" class="left">${s.itemDescription }</display:column>
							<display:column title="Keterangan">
								<%-- <c:choose>
									<c:when test="${s.fileName!=null and s.fileName!='' }">
										<a href="${contextPath }/upload/file/${s.fileName}" target="_blank">${s.attachment }</a>
									</c:when>
									<c:otherwise>
										${s.attachment }
									</c:otherwise>
								</c:choose> --%>
								${s.keterangan}
							</display:column>
							<display:column title="Jumlah" style="text-align:right">
								<fmt:formatNumber> ${s.itemQuantity }</fmt:formatNumber>
								<hr />
								<span style="font-weight:bold;"><fmt:formatNumber> ${s.itemQuantityOe }</fmt:formatNumber></span>
							</display:column>
							<display:column title="Harga Satuan" style="text-align:right">
								<fmt:formatNumber> ${s.itemPrice }</fmt:formatNumber>
								<hr />
								<span style="font-weight:bold;"><fmt:formatNumber> ${s.itemPriceOe }</fmt:formatNumber></span>
							</display:column>
							<display:column title="Pajak (%)" style="text-align:right">
								${s.itemTax }
								<hr />
								<span style="font-weight:bold;">${s.itemTaxOe }</span>
								<input type="hidden" value="${s.itemTax }" id="itemTax-${i }">
								<input type="hidden" value="${s.itemTaxOe }" id="itemTaxOe-${i }">
							</display:column>
							<display:column title="Sub Total" style="text-align:right">
								<fmt:formatNumber> ${s.itemTotalPrice }</fmt:formatNumber>
								<hr />
								<span style="font-weight:bold;"><fmt:formatNumber> ${s.itemTotalPriceOe }</fmt:formatNumber></span>
								<input type="hidden" value="${s.itemTotalPrice }" id="itemTotalPrice-${i }">
								<input type="hidden" value="${s.itemTotalPriceOe }" id="itemTotalPriceOe-${i }">
							</display:column>
							<display:column title="Total + Pajak" style="text-align:right">
								<script type="text/javascript">
									$(document).ready(function(){
										var itax = parseFloat($('#itemTax-${i }').val());
										var itotal = parseFloat($('#itemTotalPrice-${i }').val());
										var totalAll = 'total-'+${i};
										$('#'+totalAll).autoNumeric('init');
										if(!isNaN(itax) && !isNaN(itotal)){
											$('#'+totalAll).autoNumeric('set',(itax / 100 * itotal + itotal));
										}else{
											$('#'+totalAll).autoNumeric('set',0);
										}
										$("#totalAllEE").autoNumeric('init');
										totalEE = totalEE + (itax / 100 * itotal + itotal);
										$("#totalAllEE").autoNumeric('set',totalEE);
										
										var totalAllOe = 'totaloe-'+${i};
										$('#'+totalAllOe).autoNumeric('init');
										var itax2 = parseFloat($('#itemTaxOe-${i }').val());
										var itotal2 = parseFloat($('#itemTotalPriceOe-${i }').val());

										if(!isNaN(itax2) && !isNaN(itotal2)){
											$('#'+totalAllOe).autoNumeric('set',(itax2 / 100 * itotal2 + itotal2));
										}
										$("#totalAllOE").autoNumeric('init');
										totalOE = totalOE + (itax2 / 100 * itotal2 + itotal2);
										$("#totalAllOE").autoNumeric('set',totalOE);
									});
									
								</script>
								<input type="text" readonly="readonly" id="total-${i }" style="width:inherit;text-align:right"/>
								<hr/>
								<input type="text" readonly="readonly" id="totaloe-${i }" style="width:inherit;text-align:right"/>
							</display:column>
							<display:footer>
								<tr style="font-weight: bold;">
									<td colspan="7" style="color:red">&nbsp;</td>
									<td style="text-align: right;color:red">Total EE</td>
									<td style="text-align: right;">
									<input type="text" readonly="readonly" id="totalAllEE" style="width:inherit;text-align:right;color:red;font-weight:bold"/>
									</td>
								</tr>
								<tr style="font-weight: bold;">
									<td colspan="7" style="color:red">&nbsp;</td>
									<td style="text-align: right;color:blue">Total OE</td>
									<td style="text-align: right;">
									<input type="text" readonly="readonly" id="totalAllOE" style="width:inherit;text-align:right;color:blue;font-weight:bold"/>
									</td>
								</tr>
								<tr>
									<td colspan="9" style="font-weight: bold;">(* Nilai OE berhuruf tebal Biru</td>
								</tr>
							</display:footer>
							
						<s:set var="i" value="#i+1"></s:set>
						</display:table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Anggaran
						<span class="hide"></span>
					</div>
					<div class="content">
						<div class="uibutton-toolbar extend">
							<a href='javascript:void(0)' class='uibutton icon add' onclick='load_into_box("<s:url action="ajax/pp/crudAnggaran" />?prcMainHeader.ppmId=${prcMainHeader.ppmId }")'>Tambah Data Anggaran</a>
							<c:if test="${listAnggaranJdeDetail.size()==0 }">
								<input type="button" class="uibutton" value="Generate Anggaran ON" onclick="generateAnggaran();"/>
							</c:if>
						</div>
						<div id="hasilAnggaran">
							<s:include value="ajax/listAnggaran.jsp" />
						</div>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Estimator
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle">
							<tr>
								<td width="150px;">Estimator</td>
								<td>${prcMainHeader.admUserByPpmEstimator.completeName }</td>
							</tr>						
						</table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Daftar Komentar
						<span class="hide"></span>
					</div>
					<div class="content">
						<display:table name="${listHistMainComment }" id="k" excludedParams="*" style="width: 100%;" class="style1" pagesize="1000">
							<display:column title="No." style="width: 20px; text-align: center;">${k_rowNum}</display:column>
							<display:column title="Nama" style="width: 100px;" class="center">${k.username }</display:column>
							<display:column title="Posisi" style="width: 100px;" class="center">${k.posisi }</display:column>
							<display:column title="Proses" style="width: 100px;" class="center">${k.prosesAksi }</display:column>
							<display:column title="Tgl Pembuatan" style="width: 100px;" class="center">
								<fmt:formatDate value="${k.createdDate }" pattern="dd.MM.yyyy HH:mm"/> 
							</display:column>
							<display:column title="Tgl Update" style="width: 100px;" class="center">
								<fmt:formatDate value="${k.updatedDate }" pattern="dd.MM.yyyy HH:mm"/> 
							</display:column>
							<display:column title="Aksi" class="center" style="width: 250px;">${k.aksi }</display:column>
							<display:column title="Komentar" class="center" style="width: 250px;">${k.comment }</display:column>
							<display:column title="Dokumen" class="center" style="width: 200px;">
								<a href="${contextPath }/upload/file/${k.document }" target="_blank">${k.document }</a>
							</display:column>
						</display:table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Pakta Integritas
						<span class="hide"></span>
					</div>
					<div class="content">
							<p align="justify">
							Saya, yang membuat permintaan pengadaan barang/jasa nomor : &nbsp;&nbsp;<b>${prcMainHeader.ppmNomorPr }</b> <br/>
							Dengan ini menyatakan dengan sebenarnya bahwa : </p>
							<ol>
								<li>Barang/jasa tersebut adalah benar dibutuhkan untuk menunjang kegiatan operasional perusahaan</li>
								<li>Dalam pengadaan barang/jasa tersebut saya tidak memiliki kepentingan pribadi atau tujuan untuk melakukan sesuatu manfaat bagi diri saya sendiri, atau melakukan praktek KKN (KORUPSI, KOLUSI NEPOTISME) dalam pengadaan barang/jasa tersebut</li>
							</ol>
							<p align="justify">
							Demikian pernyataan ini kami sampaikan dengan sebenar-benarnya, tanpa menyembunyikan fakta dan hal material apapun dan dengan demikian kami akan bertanggung jawab sepenuhnya atas kebenaran dari hal-hal yang kami nyatakan disini. <br/>
							Demikian pernyataan ini kami buat untuk digunakan sebagaimana mestinya.
							</p>
					</div>
				</div>
								
				<div class="box">
					<div class="title">
						Komentar
						<span class="hide"></span>
					</div>
					<div class="content">
						<s:hidden name="histMainComment.username" value="%{admEmployee.empFullName}" />
						<table class="form formStyle">
							<tr>
								<td width="150px;">Komentar (*)</td>
								<td><s:textarea name="histMainComment.comment" id="komentarVal" cssClass="required"></s:textarea> </td>
							</tr>
							<tr>
								<td width="150px;">Lampiran</td>
								<td><s:file name="docsKomentar" cssClass="uibutton" /></td>
							</tr>							
						</table>
					</div>
				</div>
				
				<div class="uibutton-toolbar extend">
					<s:submit name="saveAndFinish" value="Simpan dan Selesai" id="saveAndFinish" onclick="actionAlert('saveAndFinish','simpan dan selesai');return false;" cssClass="uibutton"></s:submit>
					<s:submit name="realokasi" value="SALDO TIDAK CUKUP" id="saldo" onclick="actionAlert('saldo','Saldo Tidak Cukup');return false;" cssClass="uibutton" cssStyle="color: red"></s:submit>
					<a href="javascript:void(0)" class="uibutton" onclick="confirmPopUp('regCancel();', 'Batal', 'Apakah anda yakin untuk membatalkan transaksi ?', 'Ya', 'Tidak');"><s:text name="cancel"></s:text> </a>					
				</div>
						
			</s:form>
			
		</div>
	</div>
</body>
</html>