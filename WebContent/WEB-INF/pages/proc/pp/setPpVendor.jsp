<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Persiapan Pengadaan</title>
	
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	
	<script type="text/javascript">	
		$(document).ready(function(){
			$(".chzn").chosen();
			refreshMitraKerja();
			//Tanggal
			$.datepicker.setDefaults($.datepicker.regional['in_ID']);
			$("#opening").datetimepicker({
				changeYear: true,
				changeMonth: true
			});
			$("#closing").datetimepicker({
				changeYear: true,
				changeMonth: true
			});
			$("#aanwijzing").datetimepicker({
				changeYear: true,
				changeMonth: true
			});
			$("#quotation").datetimepicker({
				changeYear: true,
				changeMonth: true
			});
			
			// FORM VALIDATION
			$("#procForm").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler:function(form){
					if($('#metode').val()<4){
						if($('#sizemitra').val()!='' && $('#sizemitra').val()!=0){
							//validasi tipe penawaran
							if(($('#tipeA').val()==0 && $('#tipeB').val()==0 && $('#tipeC').val()==0) || $('#tipeA').val()=='' && $('#tipeB').val()=='' && $('#tipeC').val()==''){
								alert("Tipe Penawaran Harus diisi");
								return false;
							}
							FreezeScreen('Data Sedang di Proses');
							form.submit();
						}else{
							alert('Mitra Kerja tidak boleh 0')
							return false;
						}
					}else{
						//validasi tipe penawaran
						if(($('#tipeA').val()==0 && $('#tipeB').val()==0 && $('#tipeC').val()==0) || $('#tipeA').val()=='' && $('#tipeB').val()=='' && $('#tipeC').val()==''){
							alert("Tipe Penawaran Harus diisi");
							return false;
						}
						FreezeScreen('Data Sedang di Proses');
						form.submit();
					}
				}
			});
			
			//cek jika lelang
			if($('#metode').val()>=4){
				$('#listMitraKerja').hide();
				$('#buttonRefresh').hide();
			}
			
		});
		
		//function refresh mitra kerja
		function refreshMitraKerja(){
			sendForm(document.procForm, "<s:url action="ajax/submitMitraKerja" />", "#listMitraKerja");
		}
		
		//regCancel Action
		function regCancel(){
			jQuery.facebox.close();
			window.location = "<s:url action="pp/vendor" />"; 
		}
		
		//cek value
		function cekValue(obj){
			if(obj.checked){
				obj.value=1;
			}else{
				obj.value=0;
			}
		}
		
		function selectPelaksana(obj){
			if(obj.value>=4){
				$('#listMitraKerja').hide();
				$('#buttonRefresh').hide();
			}else{
				if(obj.value==1){
					refreshMitraKerja();
				}
				$('#listMitraKerja').show();
				$('#buttonRefresh').show();
			}
				
		}
		
		//dokumen vendor
		function addRow(tableId){
			var table = document.getElementById(tableId);
			var rowCount = table.rows.length;
			var row = table.insertRow(rowCount);
			
			var cell1 = row.insertCell(0);
			var element1 = document.createElement('input');
			element1.type = "checkbox";
			cell1.appendChild(element1);
			
			var cell3 = row.insertCell(1);
			cell3.innerHTML='<input type="text" name="ppdCategory" style="width:90%" maxlength="30"/>';
			
			var cell4 = row.insertCell(2);
			cell4.innerHTML='<input type="text" name="ppdDescription" style="width:96%" maxlength="40"/>';

			var cell5 = row.insertCell(3);
			cell5.innerHTML='<input type="file" name="ppdDocument" size="40" value="[]" id="ppdDocument"/>';
		}

		function delRow(tableId){
			try{
				var table = document.getElementById(tableId);
				var rowCount = table.rows.length;
				for(var i=0;i<rowCount;i++){
					var row = table.rows[i];
					var chkbox = row.cells[0].childNodes[0];
					if(null!=chkbox && true == chkbox.checked){
						table.deleteRow(i);
						rowCount--;
						i--;
					}
				}
			}catch(e){
				alert(e);
			}
		}
		
		/* $("#opening").live("change",function(){
			var opening = $(this).val();
			var closing = $("#closing").val();
			var aanwijzing = $("#aanwijzing").val();
			var quotation = $("#quotation").val();
			if(quotation != "" && opening != "" && opening > quotation){
				$("#openingAlert").show();
				$("#openingAlert").html("Tanggal Pembukaan Pendaftaran harus lebih kecil dari Tanggal Pembukaan Penawaran");
				$(this).val("");
			}else if(opening != "" && aanwijzing != "" && opening > aanwijzing){
				$("#openingAlert").show();
				$("#openingAlert").html("Tanggal Pembukaan Pendaftaran harus lebih kecil dari Tanggal Aanwijzing");
				$(this).val("");
			}else if(closing != "" && closing < opening && opening != ""){
				$("#openingAlert").show();
				$("#openingAlert").html("Tanggal Pembukaan Pendaftaran harus lebih kecil dari Tanggal Penutupan Pendaftaran");
				$(this).val("");
			}else{
				$("#openingAlert").hide();
			}
		});
		
		$("#closing").live("change",function(){
			var opening = $("#opening").val();
			var closing = $(this).val();
			var aanwijzing = $("#aanwijzing").val();
			var quotation = $("#quotation").val();
			if(closing != "" && quotation != "" && closing > quotation){
				$("#closingAlert").show();
				$("#closingAlert").html("Tanggal Penutupan Pendaftaran harus lebih kecil dari Tanggal Pembukaan Penawaran");
				$(this).val("");
			}else if(aanwijzing != "" && aanwijzing < closing && closing != ""){
				$("#closingAlert").show();
				$("#closingAlert").html("Tanggal Penutupan Pendaftaran harus lebih kecil dari Tanggal Aanwijzing");
				$(this).val("");
			}else if(closing < opening && opening != "" && closing != ""){
				$("#closingAlert").show();
				$("#closingAlert").html("Tanggal Penutupan Pendaftaran harus lebih besar dari Tanggal Pembukaan Pendaftaran");
				$(this).val("");
			}else{
				$("#closingAlert").hide();
			}
		});
		
		$("#aanwijzing").live("change",function(){
			var opening = $("#opening").val();
			var closing = $("#closing").val();
			var aanwijzing = $(this).val();
			var quotation = $("#quotation").val();
			if(aanwijzing > quotation && quotation != "" && aanwijzing !=""){
				$(this).val("");
				$("#aanwijzingAlert").show();
				$("#aanwijzingAlert").html("Tanggal Aaanwijzing harus lebih kecil dari Tanggal Pembukaan Penawaran");
			}else if(opening != "" && aanwijzing != "" && aanwijzing < opening){
				$("#aanwijzingAlert").show();
				$("#aanwijzingAlert").html("Peringatan", "Tanggal Aaanwijzing harus lebih besar dari Tanggal Pembukaan Pendaftaran", "Tutup");
				$(this).val("");
			}if(closing != "" && aanwijzing < closing && aanwijzing != ""){
				$(this).val("");
				$("#aanwijzingAlert").show();
				$("#aanwijzingAlert").html("Tanggal Aanwijzing harus lebih besar dari Tanggal Penutupan Pendaftaran");
			}else{
				$("#aanwijzingAlert").hide()
			}
		});
		
		$("#quotation").live("change",function(){
			var opening = $("#opening").val();
			var closing = $("#closing").val();
			var aanwijzing = $("#aanwijzing").val();
			var quotation = $(this).val();
			if(opening != "" && quotation != "" && quotation < opening){
				$("#quotationAlert").show();
				$("#quotationAlert").html("Tanggal Pembukaan Penawaran harus lebih besar dari Tanggal Pembukaan Pendaftaran");
				$(this).val("");
			}else if(quotation < closing && closing != "" && quotation != ""){
				$(this).val("");
				$("#quotationAlert").show();
				$("#quotationAlert").html("Tanggal Pembukaan Penawaran harus lebih besar dari Tanggal Penutupan Pendaftaran");
			}else if(quotation < aanwijzing && aanwijzing != "" && quotation != ""){
				$(this).val("");
				$("#quotationAlert").show();
				$("#quotationAlert").html("Tanggal Pembukaan Penawaran harus lebih besar dari Tanggal Aaanwijzing");
			}else{
				$("#quotationAlert").hide();
			}
		}); */
	</script>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="dashboard">Home</s:a></li> 
	        <li>Managemen Pengadaan</li> 
	        <li>Perencanaan Pengadaan</li> 
	        <li><s:a action="pp/vendor">Persiapan Pengadaan</s:a></li>
	        
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
    
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
					
			<s:form action="pp/vendor" id="procForm" name="procForm" enctype="multipart/form-data" method="post">
				<s:hidden name="prcMainHeader.ppmId" />
				<s:hidden name="prcMainHeader.admProses.id"></s:hidden>
				<s:hidden name="prcMainHeader.ppmProsesLevel"></s:hidden>

				<div class="box">
					<div class="title">
						Header
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle" >
							<tr>
								<td width="150px">No. SPPH</td>
								<td>
									${prcMainHeader.ppmNomorRfq }
								</td>
							</tr>
							<tr>
								<td width="150px">No. PR</td>
								<td>
									${prcMainHeader.ppmNomorPr }
								</td>
							</tr>
							<tr>
								<td width="150px">Pembuat</td>
								<td>${prcMainHeader.namaPembuatPr }</td>
							</tr>
							<tr>
								<td>Departemen</td>
								<td>
									${prcMainHeader.admDept.deptName }
								</td>
							</tr>
							<tr>
								<td>Kantor</td>
								<td>
									${prcMainHeader.admDistrict.districtName }
								</td>
							</tr>
							<tr>
								<td width="150px">Nama Permintaan</td>
								<td>${prcMainHeader.ppmSubject }</td>
							</tr>
							<tr>
								<td>Deskripsi Permintaan</td>
								<td>${prcMainHeader.ppmDescription } </td>
							</tr>
							<tr>
								<td>Jenis Permintaan</td>
								<td>
									<c:if test="${prcMainHeader.ppmJenisDetail == null}">
										<c:if test="${prcMainHeader.ppmJenis.id == 1 }">Barang</c:if>
										<c:if test="${prcMainHeader.ppmJenis.id == 2 }">Jasa</c:if>
									</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 3 }">Barang Raw Material</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 4 }">Barang Sparepart</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 5 }">Barang Umum</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 6 }">Jasa Teknik</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 7 }">Jasa Umum</c:if>
								</td>
							</tr>
							
							<tr>
								<td>Prioritas</td>
								<td>
									<c:if test="${prcMainHeader.ppmPrioritas == 1 }">High</c:if>
									<c:if test="${prcMainHeader.ppmPrioritas == 2 }">Normal</c:if>
									<c:if test="${prcMainHeader.ppmPrioritas == 3 }">Low</c:if>
								</td>
							</tr>
							<tr>
								<td>Chatting</td>
								<td>
								
									<a href="javascript:void(0)" onclick="load_into_box('<s:url action="ajax/chatInternal"/>','prcMainHeader.ppmId=${prcMainHeader.ppmId }')"><span id="chat">Chatting Disini</span></a>
								</td>
							</tr>
							<tr>
								<td>Attachment</td>
								<td>
									${prcMainHeader.attachment }									
								</td>
							</tr>
							<tr>
								<td>File</td>
								<td>
									<c:if test="${prcMainHeader.attachment!=null and prcMainHeader.attachment!='' }">
										<a href="${contextPath }/upload/file/${prcMainHeader.attachment}" target="_blank">Donload</a>
									</c:if>
								</td>
							</tr>
							
							<%-- <tr>
								<td>Tipe Permintaan (*)</td>
								<td><s:select list="" name="prcMainHeader.ppmTipe"></s:select></td>
							</tr>
							<tr>
								<td>Tanggal Dibutuhkan</td>
								<td><s:textfield name="prcMainHeader.vendorContactEmail"></s:textfield></td>
							</tr>
							<tr>
								<td>Lokasi Penyerahan</td>
								<td><s:textfield name="prcMainHeader.vendorContactEmail"></s:textfield></td>
							</tr> --%>
						</table>
						
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Item
						<span class="hide"></span>
					</div>
					<div class="content center">
						<jsp:include page="../tmp/listItem.jsp"></jsp:include>
					</div>
				</div>
				
			
				
				<div class="box">
					<div class="title">
						Estimator
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle">
							<tr>
								<td width="150px;">Estimator</td>
								<td>${prcMainHeader.admUserByPpmEstimator.completeName }</td>
							</tr>						
						</table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Pembeli
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle">
							<tr>
								<td width="150px;">Pembeli</td>
								<td>${prcMainHeader.admUserByPpmBuyer.completeName }</td>
							</tr>						
						</table>
					</div>
				</div>
				<c:if test="${prcMainHeader.ppmJumlahOe > 100000000 }">
				<div class="box">
					<div class="title">
						Panitia Pengadaan
						<span class="hide"></span>
					</div>
					<div class="content center">
						<table class="form formStyle">
							<tr>
								<td width="150px;">Nama Panitia</td>
								<td>
									${panitia.namaPanitia }
								</td>
							</tr>
							<tr>
								<td width="150px;">Keterangan</td>
								<td>
									${panitia.keterangan }
								</td>
							</tr>	
						</table>
						<div style="padding-top: 50px;">
						<s:set id="i" value="0"></s:set>
						<display:table name="${listPanitiaDetail }" id="s" excludedParams="*" style="width: 100%;" class="all style1">
							<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
							<display:column title="Nama Anggota" style="width: 100px;" class="center">${s.anggota.completeName }</display:column>
							<display:column title="Posisi" style="width: 150px;" class="center">
								<c:if test="${s.posisi==1 }">Ketua</c:if>
								<c:if test="${s.posisi==2 }">Sekretaris</c:if>
								<c:if test="${s.posisi==3 }">Anggota</c:if>
							</display:column>
							
						<s:set var="i" value="#i+1"></s:set>
						</display:table>
						</div>
					</div>
				</div>
				</c:if>
				<div class="box">
					<div class="title">
						Jadwal Pengadaan
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle">
							<tr>
								<td width="150px;">Tanggal Pembukaan Pendaftaran</td>
								<td>
									<s:textfield name="prcMainPreparation.registrastionOpening" id="opening" cssClass="required"></s:textfield>
									<label class="error" id="openingAlert" style="display: none;"></label>
								</td>
							</tr>
							<tr>
								<td width="150px;">Tanggal Penutupan Pendaftaran</td>
								<td>
									<s:textfield name="prcMainPreparation.registrastionClosing" id="closing" cssClass="required"></s:textfield>
									<label class="error" id="closingAlert" style="display: none;"></label>
								</td>
							</tr>	
							<tr>
								<td width="150px;">Tanggal Aanwijzing</td>
								<td>
									<s:textfield name="prcMainPreparation.aanwijzingDate" id="aanwijzing"></s:textfield>
									<label class="error" id="aanwijzingAlert" style="display: none;"></label>
								</td>
							</tr>
							<tr>
								<td width="150px;">Tempat Aanwijzing</td>
								<td>
									<s:textfield name="prcMainPreparation.aanwijzingPlace"></s:textfield>
								</td>
							</tr>		
							<tr>
								<td width="150px;">Tanggal Pembukaan Penawaran</td>
								<td>
									<s:textfield name="prcMainPreparation.quotationOpening" id="quotation" cssClass="required"></s:textfield>
									<label class="error" id="quotationAlert" style="display: none;"></label>
								</td>
							</tr>																																											
						</table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Metode Pengadaan
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle">
							<tr>
								<td>Grup Commodity</td>
								<td>
									<s:select name="prcMainHeader.groupCode" onchange="refreshMitraKerja();" list="listGroup" listKey="groupCode" listValue="groupName" cssClass="chzn"></s:select>
								</td>
							</tr>
							<tr>
								<td width="150px;">Metode Pengadaan</td>
								<td>
									<s:select id="metode" list="listMetode" listKey="id" listValue="methodName" name="prcMainHeader.prcMethod.id" cssClass="chzn required" onchange="selectPelaksana(this)" cssStyle="width:400px"></s:select>
								</td>
							</tr>
							<tr>
								<td width="150px;">Metode Penilaian</td>
								<td>
									<s:select list="listTemplate" listKey="id" listValue="templateName" name="prcMainHeader.prcTemplate.id" cssClass="chzn" id="template" cssStyle="width:300px"></s:select>
								</td>
							</tr>
							<tr>
								<td width="150px;">Tipe Penawaran</td>
								<td>
									Tipe A <input type="checkbox" id="tipeA" name="prcMainHeader.ppmTipeA" onclick="cekValue(this)" <c:if test="${prcMainHeader.ppmTipeA==1 }">checked="checked"</c:if> value="${ prcMainHeader.ppmTipeA}"/> &nbsp;
									Tipe B <input type="checkbox" id="tipeB" name="prcMainHeader.ppmTipeB" onclick="cekValue(this)" <c:if test="${prcMainHeader.ppmTipeB==1 }">checked="checked"</c:if> value="${ prcMainHeader.ppmTipeB}" /> &nbsp;
									Tipe C <input type="checkbox" id="tipeC" name="prcMainHeader.ppmTipeC" onclick="cekValue(this)" <c:if test="${prcMainHeader.ppmTipeC==1 }">checked="checked"</c:if> value="${ prcMainHeader.ppmTipeC}" /> &nbsp;
									<br/>
									<pre>(* A: sesuai spek, B: Spek Beda, Jumlah sama, C:Spek Beda, Jumlah Beda)</pre>
								</td>
							</tr>
							<tr>
								<td width="150px;">Harga </td>
								<td>
									<input type="hidden" name="prcMainHeader.oeOpen" value="0" />
									Tertutup
								</td>
							</tr>
							<tr>
								<td width="150px;">Sistem Penawaran </td>
								<td>
									<s:select list="#{'0':'Itemize', '1':'Paket'}" name="prcMainHeader.ppmEauction" cssClass="chzn" cssStyle="width:200px"></s:select>
								</td>
							</tr>						
						</table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Lampiran Dokumen Untuk Vendor
						<span class="hide"></span>
					</div>
					<div class="content center">
						<input type="button" onclick="addRow('tblDoc')" value="Tambah" class="uibutton icon add">
						<input type="button" onclick="delRow('tblDoc')" value="Hapus" class="uibutton">
						
						<table class="style1" id="tblDoc" style="margin-top: 10px;">
							<thead>
								<tr>
									<th></th>
									<th width="30%">
										Nama Dokumen
									</th>
									<th width="50%" >
										Deskripsi
									</th>
		
									<th width="20%" >
										Dokumen
									</th>
								</tr>
							</thead>
						</table>
						
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Mitra Kerja
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle">
							<tr>
								<td width="150px;">Klasifikasi Peserta</td>
								<td>
									Kecil <input type="checkbox" name="prcMainHeader.kecil" onclick="cekValue(this)" <c:if test="${prcMainHeader.kecil==1 }">checked="checked"</c:if> value="${ prcMainHeader.kecil}"/> &nbsp;
									Non Kecil <input type="checkbox" name="prcMainHeader.menengah" onclick="cekValue(this)" <c:if test="${prcMainHeader.menengah==1 }">checked="checked"</c:if> value="${ prcMainHeader.menengah}"/> &nbsp;
									<!--  
									Besar <input type="checkbox" name="prcMainHeader.besar" onclick="cekValue(this)" <c:if test="${prcMainHeader.besar==1 }">checked="checked"</c:if> value="${ prcMainHeader.besar}" /> &nbsp;
									Grd 2 <input type="checkbox" name="prcMainHeader.grade2" onclick="cekValue(this)" <c:if test="${prcMainHeader.grade2==1 }">checked="checked"</c:if> value="${ prcMainHeader.grade2}"/> &nbsp;
									Grd 3 <input type="checkbox" name="prcMainHeader.grade3" onclick="cekValue(this)" <c:if test="${prcMainHeader.grade3==1 }">checked="checked"</c:if> value="${ prcMainHeader.grade3}"/> &nbsp;
									Grd 4 <input type="checkbox" name="prcMainHeader.grade4" onclick="cekValue(this)" <c:if test="${prcMainHeader.grade4==1 }">checked="checked"</c:if> value="${ prcMainHeader.grade4}"/> &nbsp;
									Grd 5 <input type="checkbox" name="prcMainHeader.grade5" onclick="cekValue(this)" <c:if test="${prcMainHeader.grade5==1 }">checked="checked"</c:if> value="${ prcMainHeader.grade5}"/> &nbsp;
									Grd 6 <input type="checkbox" name="prcMainHeader.grade6" onclick="cekValue(this)" <c:if test="${prcMainHeader.grade6==1 }">checked="checked"</c:if> value="${ prcMainHeader.grade6}"/> &nbsp;
									Grd 7 <input type="checkbox" name="prcMainHeader.grade7" onclick="cekValue(this)" <c:if test="${prcMainHeader.grade7==1 }">checked="checked"</c:if> value="${ prcMainHeader.grade7}"/> &nbsp;
									-->
									<s:hidden id="sizemitra"></s:hidden>
								</td>
							</tr>
							<tr>
								<td></td>
								<td id="buttonRefresh"><a href="javascript:void(0)" onclick='refreshMitraKerja();' class="uibutton">Refresh Mitra Kerja</a> </td>
							</tr>
						</table>
						<div id="listMitraKerja">
							<s:include value="ajax/listMitraKerja.jsp" />
						</div>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Daftar Komentar
						<span class="hide"></span>
					</div>
					<div class="content center">
						<display:table name="${listHistMainComment }" id="k" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
							<display:column title="No." style="width: 20px; text-align: center;">${k_rowNum}</display:column>
							<display:column title="Nama" style="width: 100px;" class="center">${k.username }</display:column>
							<display:column title="Posisi" style="width: 100px;" class="center">${k.posisi }</display:column>
							<display:column title="Proses" style="width: 100px;" class="center">${k.prosesAksi }</display:column>
							<display:column title="Tgl Pembuatan" style="width: 100px;" class="center">
								<fmt:formatDate value="${k.createdDate }" pattern="dd.MM.yyyy HH:mm"/> 
							</display:column>
							<display:column title="Tgl Update" style="width: 100px;" class="center">
								<fmt:formatDate value="${k.updatedDate }" pattern="dd.MM.yyyy HH:mm"/> 
							</display:column>
							<display:column title="Aksi" class="center" style="width: 250px;">${k.aksi }</display:column>
							<display:column title="Komentar" class="center" style="width: 250px;">${k.comment }</display:column>
							<display:column title="Dokumen" class="center" style="width: 200px;">
								<a href="${contextPath }/upload/file/${k.document }" target="_blank">${k.document }</a>
							</display:column>
						</display:table>
					</div>
				</div>
				
				
				<div class="box">
					<div class="title">
						Komentar
						<span class="hide"></span>
					</div>
					<div class="content">
						<s:hidden name="histMainComment.username" value="%{admEmployee.empFullName}" />
						<table class="form formStyle">
							<tr>
								<td width="150px;">Komentar (*)</td>
								<td><s:textarea name="histMainComment.comment" cssClass="required" id="komentarVal"></s:textarea> </td>
							</tr>	
							<tr>
								<td width="150px;">Lampiran</td>
								<td><s:file name="docsKomentar" cssClass="uibutton" /></td>
							</tr>							
						</table>
					</div>
				</div>
				
				<div class="uibutton-toolbar extend">
					<s:submit name="saveAndFinish" id="saveAndFinish" onclick="actionAlert('saveAndFinish','simpan dan selesai');return false;" value="Simpan dan Selesai" cssClass="uibutton" ></s:submit>
					<a href="javascript:void(0)" class="uibutton" onclick="confirmPopUp('regCancel();', 'Batal', 'Apakah anda yakin untuk membatalkan transaksi ?', 'Ya', 'Tidak');"><s:text name="cancel"></s:text> </a>					
				</div>
						
			</s:form>
			
		</div>
	</div>
</body>
</html>