			<%@ include file="/include/definitions.jsp" %>
			<script type="text/javascript">
			$(document).ready(function(){
				$('#sizemitra').val('${listMitraKerja.size()}')
				
			});
			
			</script>
			<center>
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>		
			
			<div style="margin-top: 20px;"></div>
			
			<s:set id="i" value="0"></s:set>
			<display:table name="${listVendor }" id="s" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
				<display:caption>Mitra Kerja</display:caption>
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column title="Nama Mitra Kerja" >
					<a href="javascript:void(0)" onclick="load_into_box('<s:url action="ajax/vendor/detail"/>','tmpVndHeader.vendorId=${s.vndHeader.vendorId }')">${fn:toUpperCase(s.vndHeader.vendorName) }</a>
				</display:column>
				<display:column title="Alamat">
					${fn:toUpperCase(s.vndHeader.vendorNpwpCity) }	
				</display:column>
				<display:column title="Status" media="html" style="width: 50px;" class="center">
					<c:choose>
						<c:when  test="${s.pmpPraqStatus==1}">Lulus</c:when>
						<c:otherwise>Tidak Lulus</c:otherwise>
					</c:choose>
					
				</display:column>
			<s:set var="i" value="#i+1"></s:set>
			</display:table>
			</center>
			