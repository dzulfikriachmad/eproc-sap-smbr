			<%@ include file="/include/definitions.jsp" %>
			
			<center>
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>		
			
			<div style="margin-top: 20px;"></div>
			
			<s:set id="i" value="0"></s:set>
			<c:set value="0" var="total"></c:set>
			<display:table name="${listAnggaranJdeDetail }" id="s" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column title="Mata Anggaran" style="width: 100px;" class="center">
					${s.rkaid }
				</display:column>
				<display:column title="Jumlah" style="width: 150px;text-align:right" class="center">
					<fmt:formatNumber>${s.rkacm1 }</fmt:formatNumber>
					<c:set value="${total+s.rkacm1 }" var="total"></c:set>
				</display:column>
				<display:column title="Keterangan"  >
					${s.keterangan}
				</display:column>
				<display:column title="" media="html" style="width: 30px;" class="center">
					
				</display:column>
				<display:footer>
					<tr>
						<td></td>
						<td style="text-align: center;font-weight: bold;color: red">Total</td>
						<td style="text-align: right;font-weight: bold;color: red"><fmt:formatNumber> ${total }</fmt:formatNumber></td>
						<td colspan="2"></td>
					</tr>
				</display:footer>
			<s:set var="i" value="#i+1"></s:set>
			</display:table>
			</center>
			