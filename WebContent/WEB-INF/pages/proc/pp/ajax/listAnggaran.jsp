			<%@ include file="/include/definitions.jsp" %>
			<script type="text/javascript">
				$(document).ready(function(){
					var ang = $('#jumlahAnggaran').val();
					var oe = $('#jmlOeHeader').val()
					
					if(parseFloat(ang)!=parseFloat(oe)){
						$('#pesan').html('Anggaran Dan OE Tidak Sama');
					}
					
					if(parseFloat(ang)==parseFloat(oe)){
						$('#pesan2').html('Anggaran Dan OE Sama');
					}
					$("#jmlAnggaran").val($("#jmlAng").val())
				});
			</script>
			<center>
			<!-- Alert -->
			<span id="pesan" class="red" style="font-size: xx-large;color: red"></span>
				
			<s:if test="hasActionErrors()">
				<div class="message red">
					<span id="pesan"></span>
				<s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>		
			
			<div style="margin-top: 20px;"></div>
			<input type="hidden" id="jmlAng" value="${listAnggaranJdeDetail.size()}"/>
			<s:set id="i" value="0"></s:set>
			<c:set var="total" value="0"></c:set>
			<display:table name="${listAnggaranJdeDetail }" id="s" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column title="Mata Anggaran" style="width: 100px;" class="center">
					${s.rkaid }
				</display:column>
				<display:column title="Jumlah" style="width: 150px;text-align:right" class="center">
					<fmt:formatNumber>${s.rkacm1 }</fmt:formatNumber>
					<c:set value="${total+s.rkacm1 }" var="total"></c:set>
				</display:column>
				<display:column title="Keterangan"  >
					${s.keterangan}
				</display:column>
				<display:column title="" media="html" class="center">
					<div class="uibutton-group">
						<s:url var="set" action="ajax/pp/crudAnggaran" />
						<a title="Ubah data Anggran" class="uibutton icon edit" href='javascript:void(0)' onclick='load_into_box("${set}", "xCommand=edit&anggaranJdeDetail.id=${s.id}&prcMainHeader.ppmId=${s.prcMainHeader.ppmId }");'>&nbsp;</a>
						<a title="Hapus data Anggaran" class="uibutton" href='javascript:void(0)' onclick='confirmPopUp("deleteObj(\"${set}\", \"xCommand=delete&anggaranJdeDetail.id=${s.id}&prcMainHeader.ppmId=${s.prcMainHeader.ppmId }\", \"#hasilAnggaran\")", "Hapus Data Item", "Yakin ingin menghapus?", "Hapus", "Batal");'>X</a>
					</div>
				</display:column>
				<display:footer>
					<tr>
						<td></td>
						<td style="text-align:center;font-weight: bold;color: red">Total</td>
						<td style="text-align:right;font-weight: bold;color: red">
							<input type="hidden" id="jumlahAnggaran" value="${total }"/>
							<fmt:formatNumber>${total }</fmt:formatNumber> 
						</td>
						<td colspan="2"></td>
					</tr>
				</display:footer>
			<s:set var="i" value="#i+1"></s:set>
			</display:table>
			</center>
			