<%@ include file="/include/definitions.jsp" %>
	<script type="text/javascript">
		$(document).ready(function(){
			// FORM VALIDATION
			$("#formeproc").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
			});	
			
			$("#formeproc").ajaxForm({
		        beforeSubmit: function() {

		        },
		        success: function(data) {
		        	$.ajax({
				        url: "<s:url action="ajax/pp/listAnggaranJde" />", 
				        data: $(document.formeproc.elements).serialize(),
				        beforeSend: function(){
				        	jQuery.facebox('<div style="padding: 17px; font-size: 36px;">Loading....</div>');
				        },
				        success: function(response){
				                jQuery.facebox(response);
				            },
				        type: "post", 
				        dataType: "html"
				    }); 
		        }
		    });
			
		});

	</script>
		<script>
			$(document).ready(function() {
				// DATATABLE
			    $('table.catalog').dataTable({
			        "bInfo": false,
			        "iDisplayLength": 10,
			        "aLengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
			        "sPaginationType": "full_numbers",
			        "bPaginate": true,
			        "sDom": '<f>t<pl>'
			    });
			});
		</script>
		
<div class='fbox_header'> Data Anggaran</div>
    <div class='fbox_container' style="width:720px">
    <s:form name="formeproc" id="formeproc">
    <s:hidden name="prcMainHeader.ppmId"></s:hidden>
    <table>
    	<tr>
    		<td style="width:150px">Tahun (digit terakhir)</td>
    		<td valign="top">
    			<s:textfield name="tahunAnggaran" cssClass="number required" maxlength="2"></s:textfield>
    		</td>
    	</tr>
    	<tr>
    		<td style="width:150px">Kode Anggaran</td>
    		<td valign="top">
    			<s:textfield name="kodeAnggaran" cssClass="number required"></s:textfield>
    		</td>
    	</tr>
    </table>
        <div class='fbox_content'>
        	<display:table name="${ListJdeMasterMain }" id="c" excludedParams="*" style="width: 700px;" class="catalog style1">
						<display:setProperty name="paging.banner.full" value=""></display:setProperty>
						<display:setProperty name="paging.banner.first" value=""></display:setProperty>
						<display:setProperty name="paging.banner.last" value=""></display:setProperty>
						<display:column title="No." style="width: 20px; text-align: center;">
							<a href="javascript:void(0)" onclick="setItemPopUp('${c.key}','${c.value }','<fmt:formatNumber pattern="###"> ${c.saldoAwal - c.transaksi } </fmt:formatNumber>')">${c_rowNum}</a>
						</display:column>
						<display:column title="Kode" total="true" style="width: 90px;"  class="center">
							<a href="javascript:void(0)" onclick="setItemPopUp('${c.key}','${c.value }','<fmt:formatNumber pattern="###"> ${c.saldoAwal - c.transaksi } </fmt:formatNumber>')">${c.key }</a>
						</display:column>
						<display:column title="Nama" style="width: 340px; ">
							<a href="javascript:void(0)" onclick="setItemPopUp('${c.key}','${c.value }','<fmt:formatNumber pattern="###"> ${c.saldoAwal - c.transaksi } </fmt:formatNumber>')">${c.value }</a>
						</display:column>
						<display:column title="Rekening" style="width: 340px; ">
							<a href="javascript:void(0)" onclick="setItemPopUp('${c.key}','${c.value }','<fmt:formatNumber pattern="###"> ${c.saldoAwal - c.transaksi } </fmt:formatNumber>')">${c.rekening }</a>
						</display:column>
						<display:column title="Saldo" style="  text-align: right;">
							<a href="javascript:void(0)" onclick="setItemPopUp('${c.key}','${c.value }','<fmt:formatNumber pattern="###"> ${c.saldoAwal - c.transaksi } </fmt:formatNumber>')">
								<fmt:formatNumber> ${c.saldoAwal }</fmt:formatNumber>
							</a>
						</display:column>
						<display:column title="Transaksi" style="text-align: right;">
							<a href="javascript:void(0)" onclick="setItemPopUp('${c.key}','${c.value }','<fmt:formatNumber pattern="###"> ${c.saldoAwal - c.transaksi } </fmt:formatNumber>')">
								<fmt:formatNumber> ${c.transaksi }</fmt:formatNumber>
							</a>
						</display:column>
						<display:column title="Sisa" style="text-align: right;">
							<fmt:formatNumber> ${c.saldoAwal - c.transaksi } </fmt:formatNumber>
						</display:column>				
					</display:table>
            	
        </div>
        <div class='fbox_footer'>
            <s:submit name="cari" value="Cari" cssClass="uibutton confirm"></s:submit>
            <a class="uibutton" href='javascript:void(0)' onclick='load_into_box("<s:url action="ajax/pp/crudAnggaran" />?prcMainHeader.ppmId=${prcMainHeader.ppmId }")'>Kembali</a>
        </div>
        </s:form>
    </div>
    
