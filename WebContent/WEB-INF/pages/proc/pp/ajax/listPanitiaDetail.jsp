			<%@ include file="/include/definitions.jsp" %>
			
			<center>
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>		
			
			<div style="margin-top: 20px;"></div>
			<s:set id="i" value="0"></s:set>
			<input type="hidden" id="jumlah" value="${fn:length(listPanitiaDetail) }">
			<display:table name="${listPanitiaDetail }" id="s" excludedParams="*" style="width: 100%;" class="all style1">
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column title="Nama Anggota" style="width: 100px;" class="center">${s.anggota.completeName }</display:column>
				<display:column title="Posisi" style="width: 150px;" class="center">
					<c:if test="${s.posisi==1 }">Ketua</c:if>
					<c:if test="${s.posisi==2 }">Sekretaris</c:if>
					<c:if test="${s.posisi==3 }">Anggota</c:if>
				</display:column>
				
				<display:column title="Aksi" media="html" style="width: 30px;" class="center">
					<div class="uibutton-group">
						<s:url var="set" action="ajax/submitPanitiaDetail" />
						<a title="Hapus data" class="uibutton" href='javascript:void(0)' onclick='confirmPopUp("deleteObj(\"${set}\", \"act=delete&index=${i }&id=${s.id}\", \"#listPanitiaDetail\")", "Hapus Data", "Yakin ingin menghapus?", "Hapus", "Batal");'>X</a>
					</div>
				</display:column>
			<s:set var="i" value="#i+1"></s:set>
			</display:table>
			</center>
			