<%@ include file="/include/definitions.jsp" %>
	<script type="text/javascript">
		$(document).ready(function(){
			// FORM VALIDATION
			$('.chzn').chosen();
			$("#formeproc").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
			});
			
			$("#formeproc").ajaxForm({
		        beforeSubmit: function() {
		            loadingScreen();
		        },
		        success: function(data) {
		            loadPage("<s:url action="ajax/pp/listAnggaran"/>?prcMainHeader.ppmId=${prcMainHeader.ppmId}","#hasilAnggaran");
		            jQuery.facebox.close();
		        }
		    });
			
			$('#rkaap').autoNumeric('init');	
			$('#costCC').focus();
		});
		
		
	</script>
	
	<script type="text/javascript">
		function showTooltip(){
			$('div.tooltip').remove();
			var val = $('#costCC').val();
			if(val!=''){
				var data;
				$.ajax({
					 url: "<s:url action="anggaranDeskripsiJson"/>?kode="+val,
				        success: function(response){
				        	if(response.status != "-1"){
				        		$('#res').html($('<div class="tooltip">'+response.deskripsi+'</div>'));
				        	}else{
				        		$('#res').html($('<div class="tooltip">'+response.deskripsi+'</div>'));
				        		$('#costCC').val('');
				        	}
				        },
				        type: "post", 
				        dataType: "json"
			    });
			}
		}
		function removeBind(){
			$('div.tooltip').remove();
		}
		
		$("#rkaap").live("keyup",function(){
			var rkap = $('#rkaap').autoNumeric('get');
			var sisa = $("#sisaSaldo").val();
			if(parseFloat(rkap) > parseFloat(sisa)){
				$("#sisaAlert").show();
				$("#sisaAlert").html("Nilai Alokasi harus lebih kecil atau sama dengan sisa saldo");
				$('#simpanBtn').hide();
			}else{
				$("#sisaAlert").hide();
				$('#simpanBtn').show();
			}
		});
		
		 
	</script>
<div class='fbox_header'>Tambah Data Anggaran</div>
    <div class='fbox_container'>
    <s:form name="formeproc" id="formeproc" onsubmit="$('#rkaap').val($('#rkaap').autoNumeric('get'));">
        <div class='fbox_content'>
        	<div id="cek"></div>
        		<s:hidden id="sisaSaldo" name="prcMainHeader.ppmJumlahOe"></s:hidden>
            	<s:hidden name="anggaranJdeDetail.id"></s:hidden>
            	<s:hidden name="anggaranJdeDetail.prcMainHeader.ppmId" value="%{prcMainHeader.ppmId}"></s:hidden>
            	<s:hidden name="anggaranJdeDetail.rklnid"></s:hidden>
            	<table style="min-width: 600px; margin: 0px 10px;" class="form formStyle">
            		<tr>
            			<td><s:label value="Kode Mata Anggaran"></s:label></td>
            			<td colspan="3">
            				<s:textfield name="anggaranJdeDetail.rkaid" readonly="true" id="costCC" cssStyle="width:70%" onblur="showTooltip()"></s:textfield>
            				<input type="button" class="uibutton" value="Cari" id="cari" onclick='load_into_box("<s:url action="ajax/pp/listAnggaranJde" />?prcMainHeader.ppmId=${prcMainHeader.ppmId }")'/>
            				<span id="res" onclick="removeBind();"></span><br/>
            				<span style="font-weight: bold;color: red">Sisa Saldo : <fmt:formatNumber> ${prcMainHeader.ppmJumlahOe }</fmt:formatNumber></span>
            			</td>
            		</tr>        
            		<tr>
            			<td><s:label value="Jumlah (*)"></s:label> </td>
            			<td colspan="3">
            				${anggaranJdeDetail.rkacm1}
            				<s:textfield name="anggaranJdeDetail.rkacm1" cssClass="required"  id="rkaap"/> 
            				<label class="error" id="sisaAlert" style="display: none;"></label>
            			</td>
            		</tr>
            		<tr>
            			<td><s:label value="Keterangan"></s:label> </td>
            			<td colspan="3">
            				<s:textarea name="anggaranJdeDetail.keterangan" readonly="true"></s:textarea>
            			</td>
            		</tr> 
            	</table>
        </div>
        <div class='fbox_footer'>
            <s:submit key="save" cssClass="uibutton confirm" id="simpanBtn" ></s:submit>
            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()'>Batalkan</a>
        </div>
        </s:form>
    </div>
    