<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<title>Persetujuan Permintaan Pengadaan (PR)</title>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="dashboard">Home</s:a></li> 
	        <li>Procurement Management</li> 
	        <li><s:a action="procurement/approval">Persetujuan Permintaan Pengadaan</s:a></li>
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
	
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
					
			<s:form action="procurement/request">
				<div class="box">
					<div class="title">
						Daftar Permintaan Pengadaan
						<span class="hide"></span>
					</div>
					<div class="content">
						<display:table name="${listPrcMainHeader }" id="s" excludedParams="*" style="width: 100%;" class="all style1">
							<display:setProperty name="paging.banner.full" value=""></display:setProperty>
							<display:setProperty name="paging.banner.first" value=""></display:setProperty>
							<display:setProperty name="paging.banner.last" value=""></display:setProperty>
							
							<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
							<display:column title="Permintaan" total="true" style="width: 200px;">
								<c:choose>
									<c:when test="${update ==1 }">
										<a href="${contextPath }/procurement/approval.jwebs?id=${s.ppmId }">${s.ppmNomorPr }</a>
									</c:when>
									<c:otherwise>
										${s.ppmNomorPr }
									</c:otherwise>
								</c:choose>
								
							</display:column>
							<display:column title="Nama Permintaan" style="width: 150px; " class="center">${s.ppmSubject }</display:column>
							<display:column title="User Skr" style="width: 150px;" class="center">${s.admUserByPpmCurrentUser.completeName }</display:column>
							<display:column title="Satker" style="width: 100px;" class="center">${s.admDept.deptName }</display:column>
							<display:column title="Tanggal" class="center" style="width: 200px;"><fmt:formatDate value="${s.createdDate }" pattern="HH:mm:ss, dd MMM yyyy"/> </display:column>
							<display:column title="Status" class="center" style="width: 200px;">
								<c:choose>
									<c:when test="${s.ppmProsesLevel == 1 }">Pembuatan PR</c:when>
									<c:when test="${s.ppmProsesLevel == -1 }">Revisi PR</c:when>
									<c:otherwise>Persetujuan</c:otherwise>
								</c:choose>
							</display:column>
						</display:table>
					</div>
				</div>
				
			</s:form>
		</div>
	</div>
</body>
</html>