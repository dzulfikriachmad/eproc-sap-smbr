<%@ include file="/include/definitions.jsp" %>
<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Permintaan Pengadaan </title>
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	<script type="text/javascript">	
		$(document).ready(function(){
			$(".chzn").chosen();
			// FORM VALIDATION
			$("#procForm").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler:function(form){
					FreezeScreen('Data Sedang di Proses');
					form.submit();
				}
			});
			
			
		});
		
		//regCancel Action
		function regCancel(){
			jQuery.facebox.close();
			window.location = "<s:url action="procurement/request" />"; 
		}
		
		function tambahData(){
			var cek = $('#kodeItem').val();
			if(cek==''){
				alert("Item Harus Diisi");
				return false;
			}
			
			cek = $('#deskripsiItem').val();
			if(cek==''){
				alert("Item Harus Diisi");
				return false;
			}
			
			cek = $('#itemTipe').val();
			if(cek==''){
				alert("Item Harus Diisi");
				return false;
			}
			
			cek = $('#taxItem').val();
			if(cek==''){
				alert("Pajak Harus Diisi");
				return false;
			}
			
			cek = $('#jumlahItem').val();
			if(cek==''){
				alert("Jumlah Item Harus Diisi");
				return false;
			}
			cek = $('#hargaSatuanItem').val();
			if(cek==''){
				alert("Harga Item Harus Diisi");
				return false;
			}
			
			sendForm(document.procForm, "<s:url action="ajax/submitPrcMainItem" />", "#listItem");
			resetItem();
		}
		function setCostCenter(){
			$('#costCenter').val($('#admCostCenter').val());
		}
		
		function hitungSubTotal(){
			var jumlah = parseInt($('#jumlahItem').val());
			var hargaSatuan = parseInt($('#hargaSatuanItem').val());
			
			if(!isNaN(jumlah) && !isNaN(hargaSatuan)){
				$('#hargaTotalItem').val(jumlah*hargaSatuan);
			}else{
				$('#hargaTotalItem').val(0);
			}
		}
		
		function setItemFromPopup(kode, desc, tipe, satuan,group){
			if($('#group').val()=='' || $('#group').val()==group){
				$('#kodeItem').val(kode);
				$('#deskripsiItem').val(desc);
				$('#itemTipe').val(tipe);
				$('#satuan').val(satuan);
				$('#prcMainHeader.groupCode').val(group);
				jQuery.facebox.close();
			}else{
				alert('Commodity Harus dari Group Yang Sama');
				return false;
			}
		}
		
		function resetItem(){
			$('#kodeItem').val('');
			$('#deskripsiItem').val('');
			$('#itemTipe').val('');
			$('#taxItem').val('');
			$('#jumlahItem').val('');
			$('#hargaSatuanItem').val('');
			$('#hargaTotalItem').val('');
		}
		
		function addRow(tableId){
			var table = document.getElementById(tableId);
			var rowCount = table.rows.length;
			var row = table.insertRow(rowCount);
			
			var cell1 = row.insertCell(0);
			var element1 = document.createElement('input');
			element1.type = "checkbox";
			cell1.appendChild(element1);
			
			var cell3 = row.insertCell(1);
			cell3.innerHTML='<input type="text" name="ppdCategory" style="width:90%" maxlength="30"/>';
			
			var cell4 = row.insertCell(2);
			cell4.innerHTML='<input type="text" name="ppdDescription" style="width:96%" maxlength="40"/>';

			var cell5 = row.insertCell(3);
			cell5.innerHTML='<input type="file" name="ppdDocument" size="40" value="[]" id="ppdDocument"/>';
		}

		function delRow(tableId){
			try{
				var table = document.getElementById(tableId);
				var rowCount = table.rows.length;
				for(var i=0;i<rowCount;i++){
					var row = table.rows[i];
					var chkbox = row.cells[0].childNodes[0];
					if(null!=chkbox && true == chkbox.checked){
						table.deleteRow(i);
						rowCount--;
						i--;
					}
				}
			}catch(e){
				alert(e);
			}
		}
		
	</script>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="dashboard">Home</s:a></li> 
	        <li>Procurement Management</li> 
	        <li><s:a action="procurement/request">Permintaan Pengadaan</s:a></li>
	        
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
    
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
					
			<s:form action="procurement/request" id="procForm" name="procForm" enctype="multipart/form-data" method="post">
				<s:hidden name="prcMainHeader.ppmId" />
				<s:hidden name="prcMainHeader.ppmTipe" value="1" />
				<s:hidden name="prcMainHeader.admProses.id" value="100"></s:hidden>
				<s:hidden name="prcMainHeader.ppmProsesLevel"></s:hidden>
				<s:hidden name="prcMainHeader.createdBy"></s:hidden>
				<s:hidden name="prcMainHeader.createdDate"></s:hidden>
				<s:hidden name="prcMainHeader.groupCode" id="group"></s:hidden>
				
				<div class="box">
					<div class="title">
						Header
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle" >
							<tr>
								<td width="150px">No. PR</td>
								<td>
									<s:textfield name="prcMainHeader.ppmNomorPr"></s:textfield>
								</td>
							</tr>
							<tr>
								<td width="150px">User</td>
								<td>
									<s:hidden name="prcMainHeader.admUserByPpmCreator.id" value="%{#session.user.id }"></s:hidden>
									<s:hidden name="prcMainHeader.admUserByPpmCurrentUser.id" value="%{#session.user.id }"></s:hidden>
									<s:hidden name="prcMainHeader.admRole.id" value="%{admEmployee.admRoleByEmpRole1.id}"></s:hidden>
									${admEmployee.empFullName }
								</td>
							</tr>
							<tr>
								<td>Departemen</td>
								<td>
									<s:hidden name="prcMainHeader.admDept.id" value="%{admEmployee.admDept.id}"></s:hidden>
									${admEmployee.admDept.deptName }
								</td>
							</tr>
							<tr>
								<td>Kantor</td>
								<td>
									<s:hidden name="prcMainHeader.admDistrict.id" value="%{admEmployee.admDept.admDistrict.id}"></s:hidden>
									${admEmployee.admDept.admDistrict.districtName }
								</td>
							</tr>
							<tr>
								<td width="150px">Nama Permintaan  (*)</td>
								<td><s:textfield name="prcMainHeader.ppmSubject" cssClass="required"></s:textfield></td>
							</tr>
							<tr>
								<td>Deskripsi Permintaan (*)</td>
								<td><s:textarea name="prcMainHeader.ppmDescription" cssClass="required"></s:textarea> </td>
							</tr>
							<tr>
								<td>Jenis Permintaan (*)</td>
								<td>
									<c:if test="${prcMainHeader.ppmJenisDetail == null}">
										<c:if test="${prcMainHeader.ppmJenis.id == 1 }">Barang</c:if>
										<c:if test="${prcMainHeader.ppmJenis.id == 2 }">Jasa</c:if>
									</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 3 }">Barang Raw Material</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 4 }">Barang Sparepart</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 5 }">Barang Umum</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 6 }">Jasa Teknik</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 7 }">Jasa Umum</c:if>
								 </td>
							</tr>
							
							<tr>
								<td>Prioritas</td>
								<td><s:select list="#{'1':'High', '2':'Normal', '3':'Low' }" name="prcMainHeader.ppmPrioritas"></s:select></td>
							</tr>
							<%-- <tr>
								<td>Tipe Permintaan (*)</td>
								<td><s:select list="" name="prcMainHeader.ppmTipe"></s:select></td>
							</tr>
							<tr>
								<td>Tanggal Dibutuhkan</td>
								<td><s:textfield name="prcMainHeader.vendorContactEmail"></s:textfield></td>
							</tr>
							<tr>
								<td>Lokasi Penyerahan</td>
								<td><s:textfield name="prcMainHeader.vendorContactEmail"></s:textfield></td>
							</tr> --%>
						</table>
						
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Item
						<span class="hide"></span>
					</div>
					<div class="content">
						<s:hidden name="prcMainItem.itemTipe" id="itemTipe"></s:hidden>
						<table class="form formStyle">
							<tr>
								<td width="150px"><s:label value="Kode (*)" /> </td>
								<td><s:textfield name="prcMainItem.itemCode" cssStyle="width: 200px;" id="kodeItem" readonly="true"></s:textfield>
								<a href="javascript:void(0)" onclick="load_into_box_alert('Catalog', '<s:url action="ajax/searchCommodity" />', 'Tutup', '')" class="uibutton" >...</a> </td>
							</tr>
							<tr>
								<td><s:label value="Deskripsi (*)" /></td>
								<td><s:textarea name="prcMainItem.itemDescription" id="deskripsiItem" readonly="true"></s:textarea> </td>
							</tr>
							<tr>
								<td><s:label value="Cost Center (*)" /> </td>
								<td>
									<s:select list="listAdmCostCenter" listKey="id.costCenterCode" listValue="costCenterName" headerKey="" headerValue="--Pilih Satu--" onchange="setCostCenter();" id="admCostCenter"></s:select>
									<s:textfield name="prcMainItem.itemCostCenter" readonly="true" id="costCenter"></s:textfield> 
								</td>
							</tr>
							<tr>
								<td><s:label value="Pajak (*)" /> </td>
								<td><s:textfield id="taxItem" name="prcMainItem.itemTax" cssStyle="width: 50px;" ></s:textfield> % </td>
							</tr>
							<tr>
								<td><s:label value="Jumlah (*)" /> </td>
								<td><s:textfield id="jumlahItem" name="prcMainItem.itemQuantity" cssStyle="width:50px;" onkeyup="hitungSubTotal();"></s:textfield> </td>
							</tr>
							<tr>
								<td><s:label value="Satuan (*)" /> </td>
								<td><s:select id="satuan" list="listAdmUom" listKey="uomId" listValue="uomName" name="prcMainItem.admUom.uomId"></s:select> </td>
							</tr>
							<tr>
								<td><s:label value="Harga Satuan EE (*)" /> </td>
								<td><s:textfield id="hargaSatuanItem" name="prcMainItem.itemPrice" cssStyle="width:140px;" onkeyup="hitungSubTotal();"></s:textfield> IDR </td>
							</tr>
							<tr>
								<td><s:label value="Harga Total (*)" /> </td>
								<td><s:textfield id="hargaTotalItem" name="prcMainItem.itemTotalPrice" cssStyle="width:140px;" readonly="true"></s:textfield> IDR </td>
							</tr>
							<tr>
								<td></td>
								<td><a href="javascript:void(0)" onclick='tambahData();' class="uibutton">Tambah Data</a> </td>
							</tr>
						</table>
						
						<div id="listItem">
							<s:include value="ajax/listItem.jsp" />
						</div>
					</div>
				</div>
				
				<!-- <div class="box">
					<div class="title">
						Lampiran Dokumen Untuk Vendor
						<span class="hide"></span>
					</div>
					<div class="content center">
						<input type="button" onclick="addRow('tblDoc')" value="Tambah" class="uibutton icon add">
						<input type="button" onclick="delRow('tblDoc')" value="Hapus" class="uibutton">
						
						<table class="style1" id="tblDoc" style="margin-top: 10px;">
							<thead>
								<tr>
									<th></th>
									<th width="30%">
										Nama Dokumen
									</th>
									<th width="50%" >
										Deskripsi
									</th>
		
									<th width="20%" >
										Dokumen
									</th>
								</tr>
							</thead>
						</table>
						
					</div>
				</div> -->
				
				<div class="box">
					<div class="title">
						Daftar Komentar
						<span class="hide"></span>
					</div>
					<div class="content center">
						<display:table name="${listHistMainComment }" id="k" excludedParams="*" style="width: 100%;" class="style1" pagesize="1000">
							<display:column title="No." style="width: 20px; text-align: center;">${k_rowNum}</display:column>
							<display:column title="Nama" style="width: 100px;" class="center">${k.username }</display:column>
							<display:column title="Posisi" style="width: 100px;" class="center">${k.posisi }</display:column>
							<display:column title="Proses" style="width: 100px;" class="center">${k.prosesAksi }</display:column>
							<display:column title="Tgl Pembuatan" style="width: 100px;" class="center">${k.createdDate }</display:column>
							<display:column title="Tgl Update" style="width: 100px;" class="center">${k.updatedDate }</display:column>
							<display:column title="Aksi" class="center" style="width: 250px;">${k.aksi }</display:column>
							<display:column title="Komentar" class="center" style="width: 250px;">${k.comment }</display:column>
							<display:column title="Dokumen" class="center" style="width: 200px;">
								<a href="${contextPath }/upload/file/${k.document }" target="_blank">${k.document }</a>
							</display:column>
						</display:table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Pakta Integritas
						<span class="hide"></span>
					</div>
					<div class="content">
							<p align="justify">
							Saya, yang membuat permintaan pengadaan barang/jasa nomor : &nbsp;&nbsp;<b>${prcMainHeader.ppmNomorPr }</b> <br/>
							Dengan ini menyatakan dengan sebenarnya bahwa : </p>
							<ol>
								<li>Barang/jasa tersebut adalah benar dibutuhkan untuk menunjang kegiatan operasional perusahaan</li>
								<li>Dalam pengadaan barang/jasa tersebut saya tidak memiliki kepentingan pribadi atau tujuan untuk melakukan sesuatu manfaat bagi diri saya sendiri, atau melakukan praktek KKN (KORUPSI, KOLUSI NEPOTISME) dalam pengadaan barang/jasa tersebut</li>
							</ol>
							<p align="justify">
							Demikian pernyataan ini kami sampaikan dengan sebenar-benarnya, tanpa menyembunyikan fakta dan hal material apapun dan dengan demikian kami akan bertanggung jawab sepenuhnya atas kebenaran dari hal-hal yang kami nyatakan disini. <br/>
							Demikian pernyataan ini kami buat untuk digunakan sebagaimana mestinya.
							</p>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Komentar
						<span class="hide"></span>
					</div>
					<div class="content">
						<s:hidden name="histMainComment.username" value="%{admEmployee.empFullName}" />
						<table class="form formStyle">
							<tr>
								<td width="150px;">Komentar (*)</td>
								<td><s:textarea name="histMainComment.comment" cssClass="required"></s:textarea> </td>
							</tr>	
							<tr>
								<td width="150px;">Lampiran</td>
								<td><s:file name="docsKomentar" cssClass="uibutton" /></td>
							</tr>							
						</table>
					</div>
				</div>
				
				<div class="uibutton-toolbar extend">
					<s:submit name="save" key="save" cssClass="uibutton" ></s:submit>
					<s:submit name="saveAndFinish" key="save.finish" cssClass="uibutton" ></s:submit>	
					<a href="javascript:void(0)" class="uibutton" onclick="confirmPopUp('regCancel();', 'Batal', 'Apakah anda yakin untuk membatalkan transaksi ?', 'Ya', 'Tidak');"><s:text name="cancel"></s:text> </a>
				</div>
						
			</s:form>
			
		</div>
	</div>
</body>
</html>