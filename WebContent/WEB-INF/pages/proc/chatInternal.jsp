<%@ include file="/include/definitions.jsp" %>
	<script type="text/javascript">
		$(document).ready(function(){
			// FORM VALIDATION
			$('.chzn').chosen();
			$("#formeproc").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
			});
			
			$("#formeproc").ajaxForm({
		        beforeSubmit: function() {
		            
		        },
		        success: function(data) {
		        	$.ajax({
				        url: "<s:url action="ajax/chatInternal" />?prcMainHeader.ppmId=${prcMainHeader.ppmId}", 
				        data: $(document.formeproc.elements).serialize(),
				        beforeSend: function(){
				        	jQuery.facebox('<div style="padding: 17px; font-size: 36px;">Loading....</div>');
				        },
				        success: function(response){
				                jQuery.facebox(response);
				            },
				        type: "post", 
				        dataType: "html"
				    }); 
		        }
		    });
		});
		
		
	</script>
	
<div class='fbox_header'>Chat Internal</div>
    <div class='fbox_container'>
    <s:form name="formeproc" id="formeproc" method="post" enctype="multipart/form-data">
        <div class='fbox_content'>
        	<div id="cek"></div>
            
            	<s:hidden name="chat.id"></s:hidden>
            	<s:hidden name="chat.prcMainHeader.ppmId" value="%{prcMainHeader.ppmId}"></s:hidden>
            	<table style="min-width: 600px; margin: 0px 10px;" class="form formStyle">       
            		<tr>
            			<td><s:label value="Kepada (*)"></s:label> </td>
            			<td colspan="3">
            				<s:select list="listUser" listKey="id" listValue="completeName" name="chat.kepada" cssClass="chzn" cssStyle="width:300px"></s:select>
            			</td>
            		</tr>
            		<tr>
            			<td><s:label value="Pesan (*)"></s:label> </td>
            			<td colspan="3">
            				<s:textarea name="chat.pesan" cssStyle="width:400px"></s:textarea>
            			</td>
            		</tr>
            		<tr>
            			<td>Attachment</td>
            			<td colspan="3">
            				<s:file name="docsChat"></s:file>
            			</td>
            		</tr>
            	</table>
            	
					<display:table name="${listChat }" id="s" excludedParams="*"  class="style1">
						<display:setProperty name="paging.banner.full" value=""></display:setProperty>
						<display:setProperty name="paging.banner.first" value=""></display:setProperty>
						<display:setProperty name="paging.banner.last" value=""></display:setProperty>
						<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
						<display:column title="Dari">
							${s.dari }
						</display:column>
						<display:column title="Kepada">
							${s.kepada }
						</display:column>
						<display:column title="Pesan">
							${s.pesan }
						</display:column>
						<display:column title="Tanggal Kirim">
							<fmt:formatDate value="${s.createdDate }" pattern="dd.MMM.yyyy"/>
						</display:column>		
						<display:column title="Doc">
							<c:if test="${not empty s.dokumen}">
							<a target="blank" href="${contextPath }/upload/file/${s.dokumen }">Download</a>
							</c:if>
						</display:column>		
					</display:table>
            
        </div>
        <div class='fbox_footer'>
            <s:submit key="save" cssClass="uibutton confirm"></s:submit>
            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()'>Batalkan</a>
        </div>
        </s:form>
    </div>
    