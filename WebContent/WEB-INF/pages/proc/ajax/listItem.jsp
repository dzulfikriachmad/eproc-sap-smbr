			<%@ include file="/include/definitions.jsp" %>
			
			<center>
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>		
			
			<div style="margin-top: 20px;"></div>
			<s:set id="i" value="0"></s:set>
			<display:table name="${listPrcMainItem }" id="s" pagesize="10" excludedParams="*" style="width: 100%;" class="style1">
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column title="Kode" style="width: 100px;" class="center">${s.itemCode }</display:column>
				<display:column title="Cost Center" style="width: 150px;" class="center">${s.itemCostCenter }</display:column>
				<display:column title="Item" class="center" style="width: 150px;">${s.itemDescription }</display:column>
				<display:column title="Jumlah" class="center" style="width: 40px;">${s.itemQuantity }</display:column>
				<display:column title="Harga Satuan" style="width: 120px;" class="center">${s.itemPrice }</display:column>
				<display:column title="Pajak" class="center" style="width: 40px;">
					${s.itemTax }
					<input type="hidden" value="${s.itemTax }" id="itemTax-${i }">
				</display:column>
				<display:column title="Sub Total" class="center" style="width: 100px;">
					${s.itemTotalPrice }
					<input type="hidden" value="${s.itemTotalPrice }" id="itemTotalPrice-${i }">
				</display:column>
				<display:column title="Total + Tax" class="center" style="width: 100px;">
					<fmt:formatNumber>${(s.itemTotalPrice * (s.itemTax+100/100))}</fmt:formatNumber>
				</display:column>
				<display:column title="Aksi" media="html" style="width: 30px;" class="center">
					<div class="uibutton-group">
						<s:url var="set" action="ajax/submitPrcMainItem" />
						<a title="Hapus data item" class="uibutton" href='javascript:void(0)' onclick='confirmPopUp("deleteObj(\"${set}\", \"act=delete&index=${i }&id=${s.id}\", \"#listItem\")", "Hapus Data Item", "Yakin ingin menghapus?", "Hapus", "Batal");'>X</a>
					</div>
				</display:column>
			<s:set var="i" value="#i+1"></s:set>
			</display:table>
			</center>
			