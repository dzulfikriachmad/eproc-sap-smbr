<%@ include file="/include/definitions.jsp" %>
	<link rel="stylesheet" href="${contextPath}/assets/css/base.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="${contextPath}/assets/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="${contextPath}/assets/css/pepper-grinder/jquery-ui.css" type="text/css" media="screen" />
	
	<link rel="stylesheet" href="${contextPath}/assets/css/forms.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="${contextPath}/assets/css/datatables.css" type="text/css" media="screen" />
	
	<script type="text/javascript" src="${contextPath}/assets/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.ui-1.8.17.js"></script>	
	<script type="text/javascript" src="${contextPath}/assets/js/jquery-ui-i18n.js"></script> 
<%-- 	<script type="text/javascript" src="${contextPath}/assets/js/jquery.ui.select.js"></script>  --%>

	<script type="text/javascript" src="${contextPath}/assets/js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.validate-<s:text name="lang.iso"/>.js"></script>
	
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.customInput.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.datatables.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.elastic.source.js"></script>
	<%-- <script type="text/javascript" src="${contextPath}/assets/js/jquery.filestyle.mini.js"></script> --%>	
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.placeholder.min.js"></script>
	
	<script type="text/javascript" src="${contextPath}/assets/js/chosen/chosen.jquery.js"></script>
	<link href="${contextPath}/assets/js/chosen/chosen.css" rel="stylesheet" type="text/css" />
	
	<script type="text/javascript" src="${contextPath}/assets/js/facebox/facebox.js"></script>
	<link rel="stylesheet" href="${contextPath}/assets/js/facebox/facebox.css" type="text/css" media="screen" />
	<script type="text/javascript" src="${contextPath}/assets/js/apps.js"></script><script type="text/javascript" src="${contextPath}/assets/js/font/cufon-yui.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/font/Chopin_Script_400.font.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/font/Caviar_Dreams.font.js"></script>
	<script type="text/javascript">
	 	Cufon.replace('h1', {fontFamily: 'Chopin Script'});
	    Cufon.replace('h2', {fontFamily: 'Caviar Dreams'});
	    Cufon.replace('h3', {fontFamily: 'Chopin Script'});
	    Cufon.replace('footer', {fontFamily: 'Caviar Dreams'});
	    Cufon.replace('.headerLogo', {fontFamily: 'Caviar Dreams'});
	</script>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	<script type="text/javascript">
		$(document).ready(function(){
			// FORM VALIDATION
			$('.chzn').chosen();
			$("#formeproc").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
			});
			
			/* $("#formeproc").ajaxForm({
		        beforeSubmit: function() {
		            
		        },
		        success: function(data) {
		        	$.ajax({
				        url: "<s:url action="ajax/chatInternalEx" />?prcMainHeader.ppmId=${prcMainHeader.ppmId}", 
				        data: $(document.formeproc.elements).serialize(),
				        beforeSend: function(){
				        	jQuery.facebox('<div style="padding: 17px; font-size: 36px;">Loading....</div>');
				        },
				        success: function(response){
				                jQuery.facebox(response);
				            },
				        type: "post", 
				        dataType: "html"
				    }); 
		        }
		    }); */
		});
		
		
	</script>
<div style="background-color: #FFF;">
   		 <form name="formeproc" id="formeproc" action="${contextPath }/ajax/chatInternalEx.jwebs?prcMainHeader.ppmId=${prcMainHeader.ppmId}" enctype="multipart/form-data" method="post">
        	<div id="cek"></div>
            	<s:hidden name="chat.id"></s:hidden>
            	<s:hidden name="chat.prcMainHeader.ppmId" value="%{prcMainHeader.ppmId}"></s:hidden>
            	<table style="min-width: 600px; margin: 0px 10px; background-color: #FFF;" class="form formStyle1">       
            		<tr>
            			<td><s:label value="Kepada (*)"></s:label> </td>
            			<td colspan="3">
            				<s:select list="listUser" listKey="admEmployee.empStructuralPos" listValue="admEmployee.empStructuralPos" name="chat.kepada" cssClass="chzn" cssStyle="width:300px"></s:select>
            			</td>
            		</tr>
            		<tr>
            			<td><s:label value="Pesan (*)"></s:label> </td>
            			<td colspan="3">
            				<s:textarea name="chat.pesan" cssStyle="width:400px" cssClass="required"></s:textarea>
            			</td>
            		</tr>
            		<tr>
            			<td>Attachment</td>
            			<td colspan="3">
            				<s:file name="docsChat"></s:file>
            			</td>
            		</tr>
            		<tr>
            			<td>&nbsp;</td>
            			<td colspan="3">
            				<s:submit value="Kirim" name="save" cssClass="uibutton confirm"></s:submit>
            			</td>
            		</tr>
            	</table>
        	</form>
</div>
<div style="margin-top: 10px;background-color: #FFF;">            	
<display:table name="${listChat }" id="s" excludedParams="*"  class="style1 doc">
	<display:setProperty name="paging.banner.full" value=""></display:setProperty>
	<display:setProperty name="paging.banner.first" value=""></display:setProperty>
	<display:setProperty name="paging.banner.last" value=""></display:setProperty>
	<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
	<display:column title="Dari">
		${s.dari }
	</display:column>
	<display:column title="Kepada">
		${s.kepada }
	</display:column>
	<display:column title="Pesan">
		${s.pesan }
	</display:column>
	<display:column title="Tanggal Kirim">
		<fmt:formatDate value="${s.createdDate }" pattern="dd.MM.yyyy HH:mm"/> 
	</display:column>
	<display:column title="Doc">
		<c:if test="${not empty s.dokumen}">
		<a target="blank" href="${contextPath }/upload/file/${s.dokumen }">Download</a>
		</c:if>
	</display:column>			
</display:table>
</div>
    