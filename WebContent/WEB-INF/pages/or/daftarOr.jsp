<%@ include file="/include/definitions.jsp"%>

<!DOCTYPE html>
<html lang="id">
<head>
<title>Daftar PR</title>
<script type="text/javascript">
	function setValue(nomorOr,orh,site) {
		$('#nomorOr').val(nomorOr);
		$('#orH').val(orh);
		$('#headerSite').val(site);
	}
</script>

<script type="text/javascript">
	function searchData(){
		var nomorOr=$("#searchNomorOr").val();
		var headerSite = $("#searchSite").val();
		jQuery.facebox('<div style="padding: 17px; font-size: 36px;">Loading....</div>');
		$("div#hasil").load("${contextPath}/ajax/pr/daftarPr.jwebs?orHeader.id.nomorOr="+nomorOr+"&orHeader.id.headerSite="+headerSite, {},display.jwebs.onTableLoad);
		jQuery.facebox.close();
	}
</script>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
			<li><s:a action="dashboard">Home</s:a></li>
			<li><s:a action="pr/daftarPr">Daftar PR</s:a></li>
		</ul>
	</div>
	<!-- End of BreadCrumbs -->

	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red">
					<s:actionerror />
				</div>
			</s:if>
			<s:if test="hasActionMessages()">
				<div class="message green">
					<s:actionmessage />
				</div>
			</s:if>

			<%-- <div class="box">
				<div class="title">
					Import OR 
					<span class="hide"></span>
				</div>
				<div class="content">
					<div class="uibutton-toolbar extend">
								<s:form action="pr/daftarPr" method="post">
									<s:submit cssClass="uibutton" value="Import OR" name="generate" onclick="loadingScreen();"></s:submit>
								</s:form>
					</div>
				</div>
			</div> --%>
			<div class="box">
				<div class="title">
					Daftar PR 
					<span class="hide"></span>
				</div>
				<div class="content">
					<s:form action="pr/daftarPr">
					<s:hidden name="orHeader.id.nomorOr" id="nomorOr"></s:hidden>
					<s:hidden name="orHeader.id.orH" id="orH"></s:hidden>
					<s:hidden name="orHeader.id.headerSite" id="headerSite"></s:hidden>	
						<display:table name="${lisOrHeader }" id="s" excludedParams="*" style="width: 100%;" class="all style1">
							<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
							<display:column title="Nomor PR" total="true" style="width: 200px;">
								${s.id.nomorOr }
							</display:column>
							<display:column title="Tipe" style="width: 150px; " class="center">${s.id.orH }</display:column>
							<display:column title="Site" style="width: 150px;" class="center">${s.id.headerSite }</display:column>
							<display:column title="Nama Pembuat PR" style="width: 100px;" class="center">${s.namaPembuatOm }</display:column>
							<display:column title="Nama Pembuat PR Jasa" style="width: 100px;" class="center">${s.pembuatOrJasa }</display:column>
							<display:column title="Tanggal Import PR" style="width: 100px;" class="center">
								<fmt:formatDate value="${s.createdDate }" pattern="dd/MM/yyyy"/> 
							</display:column>
							<display:column title="Tanggal Pembuatan PR" style="width: 100px;" class="center">
								<fmt:formatDate value="${s.tglOr }" pattern="dd/MM/yyyy"/> 
							</display:column>
							<display:column title="Satuan Kerja" class="center" >
								${s.unitKerja }
							</display:column>
							<display:column title="Aksi" class="center" style="width: 200px;">
								
									<input type="submit" name="proses" value="Proses"
										onclick="setValue('${s.id.nomorOr }','${s.id.orH }','${s.id.headerSite }');FreezeScreen('Data Sedang Di proses')"
										class="uibutton" />
								
							</display:column>
						</display:table>
					</s:form>
					</div>
			</div>
		</div>
	</div>
</body>
</html>