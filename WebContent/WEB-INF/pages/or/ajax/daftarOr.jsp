<%@ include file="/include/definitions.jsp"%>
<s:form action="pr/daftarPr">
<s:hidden name="orHeader.id.nomorOr" id="nomorOr"></s:hidden>
<s:hidden name="orHeader.id.orH" id="orH"></s:hidden>
<s:hidden name="orHeader.id.headerSite" id="headerSite"></s:hidden>
<display:table name="${lisOrHeader }" id="s"
	excludedParams="*" style="width: 100%;" class="data all"
	requestURIcontext="true"
	requestURI="${contextPath }/ajax/pr/daftarPr.jwebs"
	size="resultSize" partialList="true">
	<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
	<display:column title="Nomor OR" total="true" style="width: 200px;">
		${s.id.nomorOr }
	</display:column>
	<display:column title="Tipe" style="width: 150px; " class="center">${s.id.orH }</display:column>
	<display:column title="Site" style="width: 150px;" class="center">${s.id.headerSite }</display:column>
	<display:column title="Nama Pembuat ON" style="width: 100px;" class="center">${s.namaPembuatOm }</display:column>
	<display:column title="Nama Pembuat OR Jasa" style="width: 100px;" class="center">${s.pembuatOrJasa }</display:column>
	<display:column title="Tanggal Import" style="width: 100px;" class="center"><fmt:formatDate value="${s.createdDate }" pattern="dd/MM/yyyy"/></display:column>
	<display:column title="Satuan Kerja" class="center" >
		${s.unitKerja }
	</display:column>
	<display:column title="Aksi" class="center" style="width: 200px;">
		
			<input type="submit" name="proses" value="Proses"
				onclick="setValue('${s.id.nomorOr }','${s.id.orH }','${s.id.headerSite }');FreezeScreen('Data Sedang Di proses')"
				class="uibutton" />
		
	</display:column>
</display:table>
</s:form>