<%@ include file="/include/definitions.jsp" %>
	<script type="text/javascript">
		$(document).ready(function(){
			// FORM VALIDATION
			$("#formTmpVndEquip").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler: function() {
					var newPassword = $("#newPassword").val();
					var rePassword = $("#rePassword").val();
					if(newPassword != rePassword){
						$("#showError").show();
						return false;
					}else{
						$.ajax({
					        url: "<s:url action="ajax/vnd/changePassword" />", 
					        data: $(document.formTmpVndEquip.elements).serialize(),
					        beforeSend: function(){
					        	jQuery.facebox('<div style="padding: 17px; font-size: 36px;">Loading....</div>');
					        },
					        success: function(response){
					                jQuery.facebox(response);
					            },
					        type: "post", 
					        dataType: "html"
					    }); 
					}
				}
			});
		});
		$("#submitPasword").click(function(){
			var newPassword = $("#newPassword").val();
			var rePassword = $("#rePassword").val();
			if(newPassword != rePassword){
				$("#showError").show();
				return false;
			}
		});
	</script>
	
<div class='fbox_header'><s:text name="vnd.reg.header.password"></s:text> </div>
    <div class='fbox_container'>
    	<s:if test="hasActionErrors()">
			<div class="message red"><s:actionerror/></div>
		</s:if>
		<s:if test="hasActionMessages()">
		    <div class="message green"><s:actionmessage/></div>
		    <script>
		    $(function(){
		    	setTimeout(function(){
				    jQuery.facebox.close();
	    		},1000);
	    	});
		    </script>
		</s:if>
    	<s:form name="formTmpVndEquip" id="formTmpVndEquip">
        <div class='fbox_content'>
        	<div id="cek"></div>
            
            	<table style="min-width: 600px; margin: 0px 10px;" class="form formStyle">
            		<tr>
            			<td width="170px"><s:label key="vnd.reg.old.password" /> (*) </td>
            			<td colspan="3"><s:password name="oldPassword" cssClass="required" /></td>
            		</tr>            		
            		<tr>
            			<td><s:label key="vnd.reg.new.password"></s:label> (*)</td>
            			<td colspan="3"><s:password id="newPassword" name="vndHeader.vendorPassword" cssClass="required" /></td>
            		</tr>  
            		<tr>
            			<td><s:label key="vnd.reg.rewrite.password"></s:label>(*) </td>
            			<td colspan="3"><s:password id="rePassword" cssClass="required" /> 
            			<label id="showError" style="display: none;" class="error" >Password tidak sama.</label>
            			</td>
            		</tr>
            	</table>
            
        </div>
        <div class='fbox_footer'>
            <s:submit key="save" name="ubah" id="submitPasword" cssClass="uibutton confirm" ></s:submit>
            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()'><s:label key="cancel"></s:label></a>
        </div>
        </s:form>
    </div>