<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	<title>e-Proc System | Vendor Login</title>

	<!-- Slider -->
	<link rel="stylesheet" href="${contextPath}/assets/images/slider-themes/default/default.css" type="text/css" media="screen" />
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.nivo.slider.pack.js"></script>
	<link rel="stylesheet" href="${contextPath}/assets/css/slider.css" type="text/css" media="screen" />
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.validate-<s:text name="lang.iso"/>.js"></script>
	<script type="text/javascript">
	    $(window).load(function() {
	        $('#slider').nivoSlider();
	    });
	</script>
	
	<script type="text/javascript">
		$(document).ready(function(){
			getCaptcha();
			// FORM VALIDATION
			$("#loginVendor").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
		          getCaptcha();
				},
				rules: {
					textCaptcha:{
						required:true,
						equalTo: "#rnd"
					}
				},
				messages: {
					textCaptcha:{
						equalTo: "Captcha Tidak Cocok"
					}
				},
				submitHandler:function(form){
					loadingScreen();
					form.submit();
				}
			});
			
		});
		
		function getCaptcha(){
			$.ajax({
		        url: "<s:url action="jsonCaptcha" />",
		        success: function(response){
		        	var nilai = response.globalReturn;
		        	$("#captcha").html(nilai);
		        	$("#rnd").val(nilai);
		        },
		        type: "post", 
		        dataType: "json"
		    }); 
		}
		
	</script>
</head>
<body>
<!-- Main Menu -->
<%-- <nav>
	<ul>
		<li><a href="${contextPath }/">Pendaftaran Mitra Kerja</a></li>
		<li><a href="#">Pengumuman</a></li>
		<li><a href="#">Lupa Password?</a></li>
	</ul>
</nav> --%>
<!-- End of Main Menu -->

<div id="wrapper">
	<div id="container">			
		<div id="right">
			<div class="section">
				<div class="slider-wrapper theme-default">
		            <div id="slider" class="nivoSlider">
		            	<!--  
		            	<img src="${contextPath}/assets/images/slider/ep4.jpg" />
		            	-->
		                <img src="${contextPath}/assets/images/slider/ep2.png" alt="" />
		                <!--  
		                <img src="${contextPath}/assets/images/slider/ep3.jpg" alt="" />
		                -->
		            </div>
		        </div>
			</div>
			
			<div class="section">
 
				<div class="box" style="width: 900px; margin: 0px auto 30px auto;">
					<div class="title center" style="text-transform: none; font-size: 21px; padding-top: 2px; padding-bottom: 7px">
						Login Vendor - Procurement
						<span class="hide"></span>
					</div>
					<div class="content">
						<!-- Alert -->
						<s:if test="hasActionErrors()">
							<div class="message red"><s:actionerror/></div>
						</s:if>
						<s:if test="hasActionMessages()">
						    <div class="message orange"><s:actionmessage/></div>
						</s:if>
						<fieldset>
							<legend>Help Desk</legend>
								Email : eproc@semenbaturaja.co.id	<br/>
						</fieldset>
						<s:form action="vnd/login" method="post" id="loginVendor">
							<s:hidden name="lastUrl"></s:hidden>
							<table style="width: 500px; margin: 0 auto;"> 
								<tr>
									<td width="150px"> <s:label key="login.username"></s:label> </td>
									<td><s:textfield name="vndHeader.vendorLogin" placeholder="Masukan Username Anda di sini" cssClass="required"></s:textfield></td>
								</tr>
								<tr>
									<td width="90px"><s:label key="login.password"></s:label></td>
									<td><s:password name="vndHeader.vendorPassword" placeholder="***********" cssClass="required"></s:password></td>
								</tr>
								<tr>
									<td width="150px" valign="top">Captcha </td>
									<td>
										<fieldset style="width:95%">
											<s:hidden id="rnd" name="rnd"></s:hidden>
											<span id="captcha" style="color: GREEN;font-weight: bolder;font-size: x-large;text-align: center"></span> &nbsp;&nbsp;<input type="button" class="uibutton special" onclick="getCaptcha()" value="Re-Captcha" style="float: right"/><br/>
											<s:textfield id="textCaptcha" name="textCaptcha" cssClass="required" placeholder="Isi Sesuai dengan Text" cssStyle="width:95%"></s:textfield>
										</fieldset>
									</td>
								</tr>
								<tr style="line-height: 3"> 
									<td width="90px"></td>
									<td>
										<div style="text-align: center">
										<s:submit name="loginVendor" value="Login" cssClass="uibutton search" ></s:submit>
  										<s:a action="vnd/registration" cssClass="uibutton"><s:text name="login.vendor.reg"></s:text> </s:a>  
										<s:a action="vnd/resetPassword" cssClass="uibutton icon secure"><s:text name="login.lostPassword"></s:text> </s:a>
										<s:a action="vnd/aktivasiManual" cssClass="uibutton" cssStyle="color:red">Aktivasi Manual</s:a>
										</div>
										<fieldset style="text-align: center;width: 95%">
											<s:a action="vnd/daftarLelang" style="font-weight: bolder;color:red">Daftar Lelang </s:a>
											<br/>
											<a href="${contextPath }/upload/vendormanual.pdf" target="_blank" style="font-weight: bolder;color:red">Manual Book </a>
											<%-- <br/>
											<a href="${contextPath }/upload/SuratPernyataan.pdf" target="_blank" style="font-weight: bolder;color:red"><s:text name="eproc.suratpernyataan"></s:text></a>
											<br/>
											<a href="${contextPath }/upload/DokumenRegistrasi.pdf" target="_blank" style="font-weight: bolder;color:green"><s:text name="eproc.dokumenregistrasi"></s:text></a> --%>
											<br/>
											<a href="${contextPath }/upload/NDPERMINTAANpengumumanSCFEproc.pdf" target="_blank" style="font-weight: bolder;color:red"><s:text name="eproc.pengumumanscf"></s:text></a>
											<%-- <br/>
											<a href="${contextPath }/upload/NDpetunjukregistrasivendor.pdf" target="_blank" style="font-weight: bolder;color:green"><s:text name="eproc.petunjukregistrasi"></s:text></a> --%>
											<br/>
											<a href="${contextPath }/upload/manualbookpetunjukregistrasivendor.pdf" target="_blank" style="font-weight: bolder;color:red"><s:text name="eproc.manualbook"></s:text></a>
											<br/>
											<a href="${contextPath }/upload/Syarat_Kelengkapan_Dokumen_Pembayaran.pdf" target="_blank" style="font-weight: bolder;color:red">Syarat Kelengkapan Dokumen Pembayaran</a>
										</fieldset>
										
									</td>
								</tr>
								
																
							</table>
						</s:form>
			
					</div> 
				</div>
			</div>
			
		</div>		
		
		
		<div class="clear"></div>
	</div>
</div> 

</body>
</html>