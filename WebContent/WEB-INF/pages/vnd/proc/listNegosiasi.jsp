<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<title>Negosiasi Pengadaan</title>
	
	<script type="text/javascript">
		function setValue(ppmId,vendorId){
			$('#ppmId').val(ppmId);
			$('#vPpmId').val(ppmId);
			$('#vendorId').val(vendorId);
		}
	</script>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="vnd/dashboard">Home</s:a></li> 
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
	
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>

				<div class="box">
					<div class="title">
						Daftar Pengadaan
						<span class="hide"></span>
					</div>
					
					<div class="content">						
						<s:form action="vnd/negosiasi">
							<s:hidden name="prcMainHeader.ppmId" id="ppmId" />
							<s:hidden name="prcMainVendor.id.ppmId" id="vPpmId" />
							<s:hidden name="prcMainVendor.id.vendorId" id="vendorId" />
							<display:table name="${listTodo }" id="s" excludedParams="*" style="width: 100%;" class="all style1">
								<display:setProperty name="paging.banner.full" value=""></display:setProperty>
								<display:setProperty name="paging.banner.first" value=""></display:setProperty>
								<display:setProperty name="paging.banner.last" value=""></display:setProperty>
								
								<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
								<display:column title="Pengadaan">${s.nomorRfq }</display:column>
								<display:column title="Pembukaan Pendaftaran" class="center">
									<fmt:formatDate value="${s.daftarBuka }" pattern="dd.MM.yyyy HH:mm"/>
								</display:column>
								<display:column title="Penutupan Pendaftaran" class="center">
									<fmt:formatDate value="${s.daftarTutup }" pattern="dd.MM.yyyy HH:mm"/>
								</display:column>
								<display:column title="Aanwijzing" class="center">
									<fmt:formatDate value="${s.aanwijzingDate }" pattern="dd.MM.yyyy HH:mm"/>
								</display:column>
								<display:column title="Pembukaan Penawaran" class="center">
									<fmt:formatDate value="${s.quotationDate }" pattern="dd.MM.yyyy HH:mm"/> 
								</display:column>
								<display:column title="Aksi" class="center" style="width: 100px;">
									<input type="submit" name="proses" value="Lihat" onclick="setValue('${s.ppmId }','${s.vendorId }'); loadingScreen();" class="uibutton" />
								</display:column>
							</display:table>
						</s:form>
					</div>
				</div>
		</div>
	</div>
</body>
</html>