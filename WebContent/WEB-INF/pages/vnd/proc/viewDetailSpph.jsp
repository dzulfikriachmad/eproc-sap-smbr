<%@ include file="/include/definitions.jsp"%>


<script type="text/javascript">
	Cufon.replace('h3', {
		fontFamily : 'Caviar Dreams'
	});
</script>

<script type="text/javascript">
	$(document).ready(
			function() {
				// DATATABLE
				$('table.data').dataTable(
						{
							"bInfo" : false,
							"iDisplayLength" : 1000,
							"aLengthMenu" : [ [ 5, 10, 25, 50, 1000 ],
									[ 5, 10, 25, 50, 1000 ] ],
							"sPaginationType" : "full_numbers",
							"bPaginate" : true,
							"sDom" : '<f>t<pl>'
						});
			});

	//regCancel Action
	function regCancel() {
		jQuery.facebox.close();
	}
</script>

<div class='fbox_header'>Detail SPPH</div>
<div class='fbox_container'>
	<div class='fbox_content'>
		<!-- Alert -->
		<s:if test="hasActionErrors()">
			<div class="message red">
				<s:actionerror />
			</div>
		</s:if>
		<s:if test="hasActionMessages()">
			<div class="message green">
				<s:actionmessage />
			</div>
		</s:if>

		<s:hidden name="prcMainHeader.ppmId" />
		<s:hidden name="prcMainVendor.id.vendorId"></s:hidden>
		<s:hidden name="prcMainVendor.id.ppmId"></s:hidden>

		<table class="form formStyle">
			<tr>
				<td width="150px">No. SPPH</td>
				<td>${prcMainHeader.ppmNomorRfq }</td>
			</tr>

			<tr>
				<td>Kantor</td>
				<td>${prcMainHeader.admDistrict.districtName }</td>
			</tr>
			<tr>
				<td width="150px">Nama Permintaan (*)</td>
				<td>${prcMainHeader.ppmSubject }</td>
			</tr>
			<tr>
				<td>Deskripsi Permintaan (*)</td>
				<td>${prcMainHeader.ppmDescription }</td>
			</tr>
			<tr>
				<td>Jenis Permintaan (*)</td>
				<td><c:if test="${prcMainHeader.ppmJenisDetail == null}">
										<c:if test="${prcMainHeader.ppmJenis.id == 1 }">Barang</c:if>
										<c:if test="${prcMainHeader.ppmJenis.id == 2 }">Jasa</c:if>
									</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 3 }">Barang Raw Material</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 4 }">Barang Sparepart</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 5 }">Barang Umum</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 6 }">Jasa Teknik</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 7 }">Jasa Umum</c:if></td>
			</tr>

		</table>

		<s:set id="i" value="0"></s:set>
		<display:table name="${listPrcMainItem }" id="s" excludedParams="*"
			style="width: 100%;" class="style1" pagesize="1000">
			<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
			<display:column title="Kode" class="center">${s.itemCode }</display:column>
			<display:column title="Deskripsi" class="left">${s.itemDescription }</display:column>
			<display:column title="Jumlah" style="text-align:right">
								${s.itemQuantityOe }
							</display:column>
			<display:column title="Pajak (%)" style="text-align:right">
								${s.itemTaxOe }
							</display:column>
			<s:set var="i" value="#i+1"></s:set>
		</display:table>
<%-- 

		<table class="form formStyle" >
			<tr>
				<td width="150px;">Pembeli</td>
				<td>${prcMainHeader.admUserByPpmBuyer.completeName }</td>
			</tr>
		</table>

		<table class="form formStyle" >
			<tr>
				<td width="150px;">Pembeli</td>
				<td>${prcMainHeader.admUserByPpmAdmBuyer.completeName }</td>
			</tr>
		</table> --%>

		<table class="form formStyle" >
			<tr>
				<td width="150px;">Tanggal Pembukaan Pendaftaran</td>
				<td><fmt:formatDate
						value="${prcMainPreparation.registrastionOpening}"
						pattern="dd.MM.yyyy HH:mm" /></td>
			</tr>
			<tr>
				<td width="150px;">Tanggal Penutupan Pendaftaran</td>
				<td><fmt:formatDate
						value="${prcMainPreparation.registrastionClosing}"
						pattern="dd.MM.yyyy HH:mm" /></td>
			</tr>
			<tr>
				<td width="150px;">Tanggal Aanwijzing</td>
				<td><fmt:formatDate
						value="${prcMainPreparation.aanwijzingDate}"
						pattern="dd.MM.yyyy HH:mm" /></td>
			</tr>
			<tr>
				<td width="150px;">Tempat Aanwijzing</td>
				<td>${prcMainPreparation.aanwijzingPlace}</td>
			</tr>
			<tr>
				<td width="150px;">Tanggal Pembukaan Penawaran</td>
				<td><fmt:formatDate
						value="${prcMainPreparation.quotationOpening}"
						pattern="dd.MM.yyyy HH:mm" /></td>
			</tr>
			<tr>
				<td width="150px;">Metode Pengadaan</td>
				<td>${prcMainHeader.prcMethod.methodName }</td>
			</tr>
			<tr>
				<td width="150px;">Metode Penilaian</td>
				<td>${prcMainHeader.prcTemplate.templateName }</td>
			</tr>
			<tr>
				<td width="150px;">Tipe Penawaran</td>
				<td>Tipe A <input type="checkbox"
					<c:if test="${prcMainHeader.ppmTipeA==1 }">checked="checked"</c:if>
					disabled="disabled" /> &nbsp; Tipe B <input type="checkbox"
					<c:if test="${prcMainHeader.ppmTipeB==1 }">checked="checked"</c:if>
					disabled="disabled" /> &nbsp; Tipe C <input type="checkbox"
					<c:if test="${prcMainHeader.ppmTipeC==1 }">checked="checked"</c:if>
					disabled="disabled" /> &nbsp;

				</td>
			</tr>
			<tr>
				<td width="150px;">Harga</td>
				<td><c:if test="${prcMainHeader.oeOpen==1 }">Terbuka</c:if> <c:if
						test="${prcMainHeader.oeOpen!=1 }">Tertutup</c:if></td>
			</tr>
			<tr>
				<td width="150px;">Sistem Penawaran</td>
				<td><c:if test="${prcMainHeader.ppmEauction==1 }">Paket</c:if> <c:if
						test="${prcMainHeader.ppmEauction!=1 }">Itemize</c:if></td>
			</tr>
		</table>

		<display:table list="${listPrcMainDoc }" id="k" excludedParams="*"
			style="width: 100%;" class="style1" pagesize="1000">
			<display:column title="No." style="width: 20px; text-align: center;">${k_rowNum}</display:column>
			<display:column title="Kategori " style="width: 100px;"
				class="center">${k.docCategory }</display:column>
			<display:column title="Nama" style="width: 100px;" class="center">${k.docName }</display:column>
			<display:column title="Dokumen" class="center" style="width: 200px;">
				<a href="${contextPath }/upload/file/${k.docPath }">${k.docPath
					}</a>
			</display:column>
		</display:table>


		<div class='fbox_footer'>
			<a class="uibutton" href='javascript:void(0)'
				onclick='jQuery.facebox.close()'>Tutup</a>
		</div>

	</div>
</div>
</div>

