<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Daftar Pengadaan</title>
	
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	
	<script type="text/javascript">	
		$(document).ready(function(){
			$(".chzn").chosen();
			
			// FORM VALIDATION
			$("#procForm").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler:function(form){
					loadingScreen();
					form.submit();
				}
			});
			
			
		});
		
		
		//regCancel Action
		function regCancel(){
			jQuery.facebox.close();
			window.location = "<s:url action="vnd/daftarLelang" />"; 
		}
		
	</script>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><a href="javascript:void(0)">Home</a> </li> 
	        
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
    
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
					
			<s:form action="vnd/daftarLelang" id="procForm" name="procForm" enctype="multipart/form-data" method="post">
				<s:hidden name="prcMainHeader.ppmId" />
				<s:hidden name="prcMainVendor.id.vendorId"></s:hidden>
				<s:hidden name="prcMainVendor.id.ppmId"></s:hidden>
				<div class="box">
					<div class="title">
						Header
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle" >
							<tr>
								<td width="150px">No. SPPH</td>
								<td>
									${prcMainHeader.ppmNomorRfq }
								</td>
							</tr>
							
							<tr>
								<td>Kantor</td>
								<td>
									${prcMainHeader.admDistrict.districtName }
								</td>
							</tr>
							<tr>
								<td width="150px">Nama Permintaan</td>
								<td>${prcMainHeader.ppmSubject }</td>
							</tr>
							<tr>
								<td>Deskripsi Permintaan</td>
								<td>${prcMainHeader.ppmDescription } </td>
							</tr>
							<tr>
								<td>Jenis Permintaan</td>
								<td>
									<c:if test="${prcMainHeader.ppmJenisDetail == null}">
										<c:if test="${prcMainHeader.ppmJenis.id == 1 }">Barang</c:if>
										<c:if test="${prcMainHeader.ppmJenis.id == 2 }">Jasa</c:if>
									</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 3 }">Barang Raw Material</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 4 }">Barang Sparepart</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 5 }">Barang Umum</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 6 }">Jasa Teknik</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 7 }">Jasa Umum</c:if>
								</td>
							</tr>
							<tr>
								<td>Keterangan</td>
								<td>
									${prcMainHeader.attachment}
								</td>
							</tr>
							
							
						</table>
						
					</div>
				</div>
				
				
				<div class="box">
					<div class="title">
						Pengumuman
						<span class="hide"></span>
					</div>
					<div class="content">
						${prcMainHeader.pengumumanLelang }
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Item
						<span class="hide"></span>
					</div>
					<div class="content center">
												
						<s:set id="i" value="0"></s:set>
						<display:table name="${listPrcMainItem }" id="s" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
							<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
							<display:column title="Kode"  class="center">${s.itemCode }</display:column>
							<display:column title="Deskripsi" class="left">${s.itemDescription }</display:column>
							<display:column title="Jumlah" style="text-align:right">
								${s.itemQuantityOe }
							</display:column>
							<display:column title="Satuan" style="text-align:right">
								${s.admUom.uomName }
							</display:column>
							<display:column title="Pajak (%)" style="text-align:right">
								${s.itemTaxOe }
							</display:column>	
							<display:column title="Keterangan">
								${s.keterangan }
							</display:column>						
						<s:set var="i" value="#i+1"></s:set>
						</display:table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Jadwal Pengadaan
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle">
							<tr>
								<td width="150px;">Tanggal Pembukaan Pendaftaran</td>
								<td>
									<fmt:formatDate value="${prcMainPreparation.registrastionOpening}" pattern="dd.MM.yyyy HH:mm"/>
								</td>
							</tr>
							<tr>
								<td width="150px;">Tanggal Penutupan Pendaftaran</td>
								<td>
									<fmt:formatDate value="${prcMainPreparation.registrastionClosing}" pattern="dd.MM.yyyy HH:mm"/>
								</td>
							</tr>
							<!--  	
							<tr>
								<td width="150px;">Tanggal Aanwijzing</td>
								<td>
									<fmt:formatDate value="${prcMainPreparation.aanwijzingDate}" pattern="dd.MM.yyyy HH:mm"/>
								</td>
							</tr>
							<tr>
								<td width="150px;">Tempat Aanwijzing</td>
								<td>
									${prcMainPreparation.aanwijzingPlace}
								</td>
							</tr>		
							<tr>
								<td width="150px;">Tanggal Pembukaan Penawaran</td>
								<td>
									<fmt:formatDate value="${prcMainPreparation.quotationOpening}" pattern="dd.MM.yyyy HH:mm"/>
								</td>
							</tr>	
							-->																																										
						</table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Metode Pengadaan
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle">
							<tr>
								<td width="150px;">Metode Pengadaan</td>
								<td>${prcMainHeader.prcMethod.methodName }</td>
							</tr>
							<tr>
								<td width="150px;">Metode Penilaian</td>
								<td>
									${prcMainHeader.prcTemplate.templateName }
								</td>
							</tr>
							<tr>
								<td width="150px;">Tipe Penawaran</td>
								<td>
									  
									Tipe A <input type="checkbox" <c:if test="${prcMainHeader.ppmTipeA==1 }">checked="checked"</c:if> disabled="disabled"/> &nbsp;
									Tipe B <input type="checkbox" <c:if test="${prcMainHeader.ppmTipeB==1 }">checked="checked"</c:if> disabled="disabled"/> &nbsp;
									Tipe C <input type="checkbox" <c:if test="${prcMainHeader.ppmTipeC==1 }">checked="checked"</c:if> disabled="disabled"/> &nbsp;
									<br/>
									<pre>(* A: sesuai spek, B: Spek Beda jumlah Jumlah sama, C:Spek Beda, Jumlah Beda)</pre>
								</td>
							</tr>
							<tr>
								<td width="150px;">Harga </td>
								<td>
									<c:if test="${prcMainHeader.oeOpen==1 }">Terbuka</c:if>
									<c:if test="${prcMainHeader.oeOpen!=1 }">Tertutup</c:if>
								</td>
							</tr>
							<tr>
								<td width="150px;">Sistem Penawaran </td>
								<td>
									<c:if test="${prcMainHeader.ppmEauction==1 }">Paket</c:if>
									<c:if test="${prcMainHeader.ppmEauction!=1 }">Itemize</c:if>
								</td>
							</tr>						
						</table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Lampiran Dokumen Untuk Vendor
						<span class="hide"></span>
					</div>
					<div class="content center">
						<display:table list="${listPrcMainDoc }" id="k" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
							<display:column title="No." style="width: 20px; text-align: center;">${k_rowNum}</display:column>
							<display:column title="Kategori " style="width: 100px;" class="center">${k.docCategory }</display:column>
							<display:column title="Nama" style="width: 100px;" class="center">${k.docName }</display:column>
							<display:column title="Dokumen" class="center" style="width: 200px;">
								<a href="${contextPath }/upload/file/${k.docPath }">${k.docPath }</a>
							</display:column>
						</display:table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						User Vendor
						<span class="hide"></span>
					</div>
					<div class="content center">
						<table class="form formStyle1">
							<tr>
								<td width="150px">UserName</td>
								<td>
									<s:textfield name="vndHeader.vendorLogin" cssClass="required"></s:textfield>
								</td>
							</tr>
							<tr>
								<td width="150px">Password</td>
								<td>
									<s:password name="vndHeader.vendorPassword" cssClass="required"></s:password>
								</td>
							</tr>
						</table>
					</div>
				</div>
				
				<div class="uibutton-toolbar extend">
					<s:submit name="daftar" value="Daftar" cssClass="uibutton"></s:submit>
					<a href="javascript:void(0)" class="uibutton" onclick="confirmPopUp('regCancel();', 'Batal', 'Apakah anda yakin untuk membatalkan transaksi ?', 'Ya', 'Tidak');"><s:text name="cancel"></s:text> </a>					
				</div>
						
			</s:form>
			
		</div>
	</div>
</body>
</html>