<%@ include file="/include/definitions.jsp" %>
<s:iterator var="i" value="%{listSanggahan }">
	<c:choose>
		<c:when test="${i.msgFrom == 'PEMBELI' }">
			<div style="width:470px;float:left;margin-bottom: 5px;">
				<div style="background-color: #EBEBEB;width: 400px; float:left; padding:10px; border-radius:10px;">
				<span style="font-weight: bold;">${i.msgFrom }</span><br />
				${i.msgComment }
				</div>
			</div>
		</c:when>
		<c:otherwise>
			<div style="width:470px;float:left;margin-bottom: 5px;">
				<div style="background-color: #41A317;color:#FFF;width: 400px;float:right; padding:10px; border-radius:10px;">
				<span style="font-weight: bold;">${i.msgFrom }</span><br />
				${i.msgComment }
				</div>
			</div>
		</c:otherwise>
	</c:choose>
</s:iterator>
<s:if test="hasActionErrors()">
	<div class="message red"><s:actionerror/></div>
</s:if>
        	
