<%@ include file="/include/definitions.jsp" %>
<script type="text/javascript">
$(document).ready(function(){
	var size = document.getElementById('size').value;
	if(size>0){
		for(var i=0;i<size;i++){
			var item = 'itemPrice'+i;
			$('#'+item).autoNumeric('init');
			var total = 'itemPriceTotal'+i;
			$('#'+total).autoNumeric('init');
			var totalPp = 'itemPriceTotalPpn'+i;
			$('#'+totalPp).autoNumeric('init');
			
			var itemn = 'itemPriceNego'+i;
			$('#'+itemn).autoNumeric('init');
			var totaln = 'itemPriceTotalNego'+i;
			$('#'+totaln).autoNumeric('init');
			var totalPpn = 'itemPriceTotalPpnNego'+i;
			$('#'+totalPpn).autoNumeric('init');
		}
	}
	
	$('#pqmTotalPenawaran').autoNumeric('init');
	$('#pqmTotalPenawaranNego').autoNumeric('init');
	$('#bidBond').autoNumeric('init');
});
</script>
<div class='fbox_header'>Detail Penawaran</div>
    <div class='fbox_container' style="width:800px">
        <div class='fbox_content'>
				<div class="box">
					<div class="content">
						<table class="form formStyle" style="width:760px">
							<tr>
								<td width="200px">Nomor Penawaran SPPH (*)</td>
								<td>
									<s:url action="ajax/proc/viewDetailSpph" id="ur"></s:url>
									<a href="javascript:void(0)" onclick="load_into_box('${ur}','prcMainHeader.ppmId=${prcMainHeader.ppmId }')">
										${prcMainHeader.ppmNomorRfq }
									</a>
								</td>
							</tr>
							
							<tr>
								<td width="200px">Nomor Penawaran (*)</td>
								<td>
									<s:textfield name="quote.pqmNumber" cssClass="required" readonly="true"></s:textfield>
								</td>
							</tr>
							
							<tr>
								<td width="200px">Tipe Penawaran (*)</td>
								<td>
									<c:if test="${quote.pqmType=='A' }">Tipe A</c:if>
									<c:if test="${quote.pqmType=='B' }">Tipe B</c:if>
									<c:if test="${quote.pqmType=='C' }">Tipe C</c:if>
									<s:hidden name="quote.pqmType" id="pqmType"></s:hidden>
								</td>
							</tr>
							<tr>
								<td width="200px">Kandungan Lokal (%)</td>
								<td>
									<s:textfield name="quote.pqmLocalContent" cssClass="number required" cssStyle="width:200px" readonly="true"></s:textfield>
								</td>
							</tr>
							
							<tr>
								<td width="200px">Waktu Pengiriman / Pelaksanaan</td>
								<td>
									<s:textfield name="quote.pqmDeliveryTime" cssClass="number required" cssStyle="width:200px" readonly="true"></s:textfield>
									<s:select list="#{'Hari':'Hari Kalender', 'Bulan':'Bulan','Tahun':'Tahun'}" name="quote.pqmDeliveryUnit" cssClass="required"></s:select>
								</td>
							</tr>
							
							<tr>
								<td width="200px">Validitas Penawaran</td>
								<td>
									45 Hari
								</td>
							</tr>
							<tr>
								<td width="200px">Lokasi Penyerahan</td>
								<td>
									${prcMainHeader.lokasiPenyerahan }
								</td>
							</tr>
							<tr>
								<td>Catatan</td>
								<td>
									${quote.pqmNotes}
								</td>
							</tr>
						</table>
						
					</div>
				</div>
				  
				<div class="box">
					<div class="content center">
						<input type="hidden" id="size" value="${listQuoteItem.size()}"/>
						<display:table name="${listQuoteItem }" id="s" pagesize="1000" excludedParams="*" style="width:760px" class="style1">
							<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
							<display:column title="Kode"  class="center">${s.prcMainItem.itemCode }</display:column>
							<display:column title="Deskripsi" class="left">
								${s.itemDescription }
							</display:column>
							<display:column title="Keterangan" class="left">
								${s.prcMainItem.keterangan }
							</display:column>
							<display:column title="Jumlah" style="text-align:right">
								${s.itemQuantity }
							</display:column>
							<display:column title="Jumlah Penawaran (non PPN)" style="text-align:right">
								<input style="width:95%;text-align: right" type="text" name="listQuoteItemC[${s_rowNum-1 }].itemQuantity" id="listQuoteItemC[${s_rowNum-1 }].itemQuantity" value="${s.itemQuantity }" class="number required" readonly="readonly" />
							</display:column>
							<display:column title="Harga Penawaran (non PPN)" style="text-align:right">
								LAMA :<input style="width:95%;text-align: right" type="text" name="listQuoteItemC[${s_rowNum-1 }].itemPrice" id="itemPrice${s_rowNum-1 }" value="${s.itemPrice }" class="number required" readonly="readonly"/><br/>
								NEGO :<input style="width:95%;text-align: right" type="text" name="listQuoteItemC[${s_rowNum-1 }].itemPriceNego" id="itemPriceNego${s_rowNum-1 }" value="${s.itemPriceNego }" class="number required" readonly="readonly"/>
							</display:column>
							<display:column title="Pajak (%)" style="text-align:right">
								${s.itemPajak }
							</display:column>
							<display:column title="Harga Total Penawaran" style="text-align:right">
								LAMA :<input style="width:95%;text-align: right" type="text" name="listQuoteItemC[${s_rowNum-1 }].itemPriceTotal" id="itemPriceTotal${s_rowNum-1 }" value="${s.itemPriceTotal }" readonly="readonly" /><br/>
								NEGO :<input style="width:95%;text-align: right" type="text" name="listQuoteItemC[${s_rowNum-1 }].itemPriceNegoTotal" id="itemPriceTotalNego${s_rowNum-1 }" value="${s.itemPriceNegoTotal }" readonly="readonly" />
							</display:column>
							<display:column title="Harga Total Penawaran Ppn" style="text-align:right">
								LAMA :<input style="width:95%;text-align: right" type="text" name="listQuoteItemC[${s_rowNum-1 }].itemPriceTotalPpn" id="itemPriceTotalPpn${s_rowNum-1 }" value="${s.itemPriceTotalPpn }" readonly="readonly"/>
								NEGO :<input style="width:95%;text-align: right" type="text" name="listQuoteItemC[${s_rowNum-1 }].itemPriceNegoTotalPpn" id="itemPriceTotalPpnNego${s_rowNum-1 }" value="${s.itemPriceNegoTotalPpn }" readonly="readonly" />
							</display:column>
							<display:column>
								<input type="hidden" name="listQuoteItemC[${s_rowNum-1 }].id.itemId" value="${s.id.itemId }"/>
								<input type="hidden" name="listQuoteItemC[${s_rowNum-1 }].id.ppmId" value="${s.id.ppmId }"/>
								<input type="hidden" name="listQuoteItemC[${s_rowNum-1 }].id.vendorId" value="${s.id.vendorId }"/>
								<input type="hidden" name="listQuoteItemC[${s_rowNum-1 }].itemPajak" id="listQuoteItemC[${s_rowNum-1 }].itemPajak" value="${s.itemPajak }"/>
								
								<input type="hidden" name="listQuoteItemC[${s_rowNum-1 }].prcMainItem.id" value="${s.prcMainItem.id }"/>
							</display:column>	
							
							<display:footer>
								<tr>
									<td colspan="8"></td>
									<td ></td>
								</tr>
							</display:footer>						
						</display:table>
					</div>
					<div class="content">
						<table class="form formStyle" style="width:760px">
							
							<tr>
								<td width="200px">Total Penawaran</td>
								<td>
									<s:textfield name="quote.pqmTotalPenawaran" id="pqmTotalPenawaran" cssClass="number required" readonly="true"></s:textfield>
								</td>
							</tr>
							
							<tr>
								<td width="200px">Bid Bond (minimal)</td>
								<td>
									<s:textfield name="quote.pqmBidBond"  cssClass="number required" id="bidBond"></s:textfield>
								</td>
							</tr>
							
							<tr>
								<td width="200px">Total Penawaran Nego</td>
								<td>
									<s:textfield name="quote.pqmTotalPenawaranNego" id="pqmTotalPenawaranNego" cssClass="number required" readonly="true"></s:textfield>
								</td>
							</tr>
							<c:if test="${not empty quote.pqmAttachment }">
							<tr>
								<td width="200px">Attachment</td>
								<td>
									<a target="blank" href="${contextPath }/upload/file/${quote.pqmAttachment }">${quote.pqmAttachment }</a>
								</td>
							</tr>
							</c:if>
						</table>
					</div>					
					</div>		
        </div>
        <div class='fbox_footer'>
            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()'>Batalkan</a>
        </div>
    </div>
    