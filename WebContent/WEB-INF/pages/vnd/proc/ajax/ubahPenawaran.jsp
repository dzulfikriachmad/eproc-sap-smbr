<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Daftar Pengadaan</title>
	
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	
	<script type="text/javascript">	
		$(document).ready(function(){
			$(".chzn").chosen();
			//tgl
			$.datepicker.setDefaults($.datepicker.regional['in_ID']);
			$("#tgl").datepicker({
				changeYear: true,
				changeMonth: true
			});
			// FORM VALIDATION
			$("#procForm").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler:function(form){
					var awal = $('#totalPenawaran').val();
					var negoAkhir = $('#totalPenawaranNego').val();
					
					if(parseFloat(awal)<parseFloat(negoAkhir)){
						alert('Harga Nego Harus lebih kecil dari penawaran awal');
						return false;
					}
					
					loadingScreen();
					form.submit();
				}
			});	
			
			onloadData();
		});
		
		//kalkulasi
		function calculateData(obj,index){
			
			try{
				if(!IsNumeric(obj.value) || obj.value=='')
				var totalPriceOe=0.0;
				var itemQtyOe = parseFloat(document.getElementById('listQuoteItem['+index+'].itemQuantity').value);
				var itemPriceOe = parseFloat(document.getElementById('listQuoteItem['+index+'].itemPriceNego').value);
				var itemTaxOe = parseFloat(document.getElementById('listQuoteItem['+index+'].itemPajak').value);
				var element = document.getElementById('listQuoteItem['+index+'].itemPriceNego');
				element.addEventListener('input', (event) => {
				  event.target.value = event.target.value
				    // Remove anything that isn't valid in a number
				    .replace(/[^\d-.]/g, '')
				    // Remove all dashes unless it is the first character
				    .replace(/(?!^)-/g, '')
				    // Remove all periods unless it is the last one
				    .replace(/\.(?=.*\.)/g, '');
				});
				totalPriceOe = itemQtyOe * itemPriceOe;
				if(isNaN(totalPriceOe))
					totalPriceOe=0;
				var totalPpn = totalPriceOe * ((itemTaxOe+100)/100);
				if(isNaN(totalPpn))
					totalPpn=0;
	
				document.getElementById('listQuoteItem['+index+'].itemPriceTotalNego').value=totalPriceOe;
				document.getElementById('listQuoteItem['+index+'].itemPriceTotalPpnNego').value=totalPpn;
				//calculate total
				var size = document.getElementById('size').value;
				var totalPenawaran = 0;
				if(size>0){
					for(var i=0;i<size;i++){
						totalPenawaran = totalPenawaran + parseFloat(document.getElementById('listQuoteItem['+i+'].itemPriceTotalPpnNego').value);
					}	
				}
				
				
				//document.getElementById("totalPenawaranNego").value=totalPenawaran;
				document.getElementById("totalPenawaranNego").value=totalPenawaran.toFixed(3);

			}catch (e) {
				alert(e);
			}
			
		}
		
		function calculateDataB(obj,index){
			try{
				if(!IsNumeric(obj.value) || obj.value=='')
				var totalPriceOe=0.0;
				var itemQtyOe = parseFloat(document.getElementById('listQuoteItemB['+index+'].itemQuantity').value);
				var itemPriceOe = parseFloat(document.getElementById('listQuoteItemB['+index+'].itemPriceNego').value);
				var itemTaxOe = parseFloat(document.getElementById('listQuoteItemB['+index+'].itemPajak').value);
				var element = document.getElementById('listQuoteItem['+index+'].itemPriceNego');
				element.addEventListener('input', (event) => {
				  event.target.value = event.target.value
				    // Remove anything that isn't valid in a number
				    .replace(/[^\d-.]/g, '')
				    // Remove all dashes unless it is the first character
				    .replace(/(?!^)-/g, '')
				    // Remove all periods unless it is the last one
				    .replace(/\.(?=.*\.)/g, '');
				});
				totalPriceOe = itemQtyOe * itemPriceOe;
				if(isNaN(totalPriceOe))
					totalPriceOe=0;
				var totalPpn = totalPriceOe * ((itemTaxOe+100)/100);
				if(isNaN(totalPpn))
					totalPpn=0;
				
				document.getElementById('listQuoteItemB['+index+'].itemPriceTotalNego').value=totalPriceOe;
				document.getElementById('listQuoteItemB['+index+'].itemPriceTotalPpnNego').value=totalPpn.toFixed(2);
				
				//calculate total
				var size = document.getElementById('size').value;
				var totalPenawaran = 0;
				if(size>0){
					for(var i=0;i<size;i++){
						totalPenawaran = totalPenawaran + parseFloat(document.getElementById('listQuoteItemB['+i+'].itemPriceTotalPpnNego').value);
					}	
				}
				//document.getElementById("totalPenawaranNego").value=totalPenawaran;
				document.getElementById("totalPenawaranNego").value=totalPenawaran.toFixed(3);

			}catch (e) {
				alert(e);
			}
			
		}
		
		function calculateDataC(obj,index){
			try{
				if(!IsNumeric(obj.value) || obj.value=='')
					obj.value=0;
				var totalPriceOe=0.0;
				var itemQtyOe = parseFloat(document.getElementById('listQuoteItemC['+index+'].itemQuantity').value);
				var itemPriceOe = parseFloat(document.getElementById('listQuoteItemC['+index+'].itemPriceNego').value);
				var itemTaxOe = parseFloat(document.getElementById('listQuoteItemC['+index+'].itemPajak').value);
				var element = document.getElementById('listQuoteItem['+index+'].itemPriceNego');
				element.addEventListener('input', (event) => {
				  event.target.value = event.target.value
				    // Remove anything that isn't valid in a number
				    .replace(/[^\d-.]/g, '')
				    // Remove all dashes unless it is the first character
				    .replace(/(?!^)-/g, '')
				    // Remove all periods unless it is the last one
				    .replace(/\.(?=.*\.)/g, '');
				});
				totalPriceOe = itemQtyOe * itemPriceOe;
				if(isNaN(totalPriceOe))
					totalPriceOe=0;
				var totalPpn = totalPriceOe * ((itemTaxOe+100)/100);
				if(isNaN(totalPpn))
					totalPpn=0;
	
				document.getElementById('listQuoteItemC['+index+'].itemPriceTotalNego').value=totalPriceOe;
				document.getElementById('listQuoteItemC['+index+'].itemPriceTotalPpnNego').value=totalPpn;
				
				//calculate total
				var size = document.getElementById('size').value;
				var totalPenawaran = 0;
				if(size>0){
					for(var i=0;i<size;i++){
						totalPenawaran = totalPenawaran + parseFloat(document.getElementById('listQuoteItemC['+i+'].itemPriceTotalPpnNego').value);
					}	
				}
				//document.getElementById("totalPenawaranNego").value=totalPenawaran.toFixed(2);
				document.getElementById("totalPenawaranNego").value=totalPenawaran.toFixed(3);
			}catch (e) {
				alert(e);
			}
			
		}
		
		//cek value
		function cekValue(obj){
			if(obj.checked){
				obj.value=1;
			}else{
				obj.value=0;
			}
		}
		//select tipe
		function selectTipe(obj){
			if(obj.value=='A'){
				var dt = document.getElementById('ppmTipeA').value;
				if(dt!=1){
					alert('Tipe A Tidak Diijinkan');
					obj.value='';
					return false;
				}
				$("#tipeA").show();
				$("#tipeB").hide();
				$("#tipeC").hide();
			}else if(obj.value=='B'){
				var dt = document.getElementById('ppmTipeB').value;
				if(dt!=1){
					alert('Tipe B Tidak Diijinkan');
					obj.value='';
					return false;
				}
				$("#tipeA").hide();
				$("#tipeB").show();
				$("#tipeC").hide();
			}else if(obj.value=='C'){
				var dt = document.getElementById('ppmTipeC').value;
				if(dt!=1){
					alert('Tipe C Tidak Diijinkan');
					obj.value='';
					return false;
				}
				$("#tipeA").hide();
				$("#tipeB").hide();
				$("#tipeC").show();
			}
		}
		
		//regCancel Action
		function regCancel(){
			jQuery.facebox.close();
			window.location = "<s:url action="vnd/negosiasi" />?prcMainHeader.ppmId=${prcMainHeader.ppmId}&prcMainVendor.id.ppmId=${prcMainHeader.ppmId}&prcMainVendor.id.vendorId=${prcMainVendor.id.vendorId}"; 
		}
		
		//function onloadDaa
		function onloadData(){
			var tipe = $("#pqmType").val();
			if(tipe=='A'){
				$("#tipeA").show();
				$("#tipeB").hide();
				$("#tipeC").hide();
			}else if(tipe=='B'){
				$("#tipeA").hide();
				$("#tipeB").show();
				$("#tipeC").hide();
			}else if(tipe=='C'){
				$("#tipeA").hide();
				$("#tipeB").hide();
				$("#tipeC").show();
			}
		}
	</script>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="vnd/dashboard">Home</s:a></li> 
	        
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
    
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
					
			<s:form action="vnd/ubahPenawaranRfq" id="procForm" name="procForm" enctype="multipart/form-data" method="post">
				<s:hidden name="prcMainHeader.ppmId" />
				<s:hidden name="prcMainVendor.id.vendorId"></s:hidden>
				<s:hidden name="prcMainVendor.id.ppmId"></s:hidden>
				<s:hidden name="prcMainHeader.ppmTipeA" id="ppmTipeA"></s:hidden>
				<s:hidden name="prcMainHeader.ppmTipeB" id="ppmTipeB"></s:hidden>
				<s:hidden name="prcMainHeader.ppmTipeC" id="ppmTipeC"></s:hidden>
				<s:hidden name="quote.pqmTotalEauction"></s:hidden>
				<s:hidden name="quote.id.ppmId" value="%{prcMainVendor.id.ppmId}"></s:hidden>
				<s:hidden name="quote.id.vendorId" value="%{prcMainVendor.id.vendorId}"></s:hidden>
				<div class="box">
					<div class="title">
						Header
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle" >
							<tr>
								<td width="200px">Nomor Penawaran SPPH (*)</td>
								<td>
									<s:url action="ajax/proc/viewDetailSpph" id="ur"></s:url>
									<a href="javascript:void(0)" onclick="load_into_box('${ur}','prcMainHeader.ppmId=${prcMainHeader.ppmId }')">
										${prcMainHeader.ppmNomorRfq }
									</a>
								</td>
							</tr>
							
							<tr>
								<td width="200px">Nomor Penawaran (*)</td>
								<td>
									<s:textfield name="quote.pqmNumber" cssClass="required"></s:textfield>
								</td>
							</tr>
							
							<tr>
								<td width="200px">Tipe Penawaran (*)</td>
								<td>
									<c:if test="${quote.pqmType=='A' }">Tipe A</c:if>
									<c:if test="${quote.pqmType=='B' }">Tipe B</c:if>
									<c:if test="${quote.pqmType=='C' }">Tipe C</c:if>
									<s:hidden name="quote.pqmType" id="pqmType"></s:hidden>
								</td>
							</tr>
							<tr>
								<td width="200px">Kandungan Lokal (%)</td>
								<td>
									<s:textfield name="quote.pqmLocalContent" cssClass="number required" cssStyle="width:200px"></s:textfield>
								</td>
							</tr>
							
							<tr>
								<td width="200px">Waktu Pengiriman / Pelaksanaan</td>
								<td>
									<s:textfield name="quote.pqmDeliveryTime" cssClass="number required" cssStyle="width:200px"></s:textfield>
									<s:select list="#{'Hari':'Hari Kalender', 'Bulan':'Bulan','Tahun':'Tahun'}" name="quote.pqmDeliveryUnit" cssClass="required"></s:select>
								</td>
							</tr>
							
							<tr>
								<td width="200px">Validitas Penawaran</td>
								<td>
									<s:hidden name="quote.pqmValidTo"></s:hidden>
									45 Hari
								</td>
							</tr>
							<tr>
								<td>Catatan</td>
								<td>
									<s:textarea name="quote.pqmNotes" cssClass="required grow"></s:textarea>
								</td>
							</tr>
						</table>
						
					</div>
				</div>
				  
				<div class="box">
					<div class="title">
						Item
						<input type="hidden" id="size" value="${listQuoteItem.size() }"/>
						<span class="hide"></span>
					</div>
					<div class="content center" id="tipeA" style="display:none">
						<c:set var="totalA" value="0"></c:set>
						<display:table name="${listQuoteItem }" id="s" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
							<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum} A</display:column>
							<display:column title="Kode"  class="center">${s.prcMainItem.itemCode }</display:column>
							<display:column title="Deskripsi" class="left">
								${s.prcMainItem.itemDescription }
								<input type="hidden" name="listQuoteItem[${s_rowNum-1 }].itemDescription" id="listQuoteItem[${s_rowNum-1 }].itemDescription" value="${s.itemDescription }"/>
							</display:column>
							<display:column title="Keterangan" class="left">
								${s.prcMainItem.keterangan }
							</display:column>
							<display:column title="Satuan" style="width: 40px;text-align:right">
								${s.prcMainItem.admUom.uomName }
							</display:column>
							<display:column title="Jumlah" style="text-align:right">
								${s.itemQuantity }
							</display:column>
							<display:column title="Jumlah Penawaran (non PPN)" style="text-align:right">
								<input style="width:95%;text-align: right" type="text" name="listQuoteItem[${s_rowNum-1 }].itemQuantity" id="listQuoteItem[${s_rowNum-1 }].itemQuantity" value="${s.itemQuantity }" class="number required" readonly="readonly" onkeyup="calculateData(this,${s_rowNum-1});"/>
							</display:column>
							
							<display:column title="Harga Penawaran (non PPN)" style="text-align:right">
								LAMA :<input style="width:95%;text-align: right" type="text" name="listQuoteItem[${s_rowNum-1 }].itemPrice" id="listQuoteItem[${s_rowNum-1 }].itemPrice" value="${s.itemPrice }" class="number required" readonly="readonly"/><br/>
								NEGO :<input style="width:95%;text-align: right" type="text" name="listQuoteItem[${s_rowNum-1 }].itemPriceNego" id="listQuoteItem[${s_rowNum-1 }].itemPriceNego" value="${s.itemPriceNego }" class="number required" onkeyup="calculateData(this,${s_rowNum-1});"/>
							</display:column>
							<display:column title="Pajak (%)" style="text-align:right">
								${s.itemPajak }
							</display:column>
							<display:column title="Harga Total Penawaran" style="text-align:right">
								LAMA :<input style="width:95%;text-align: right" type="text" name="listQuoteItem[${s_rowNum-1 }].itemPriceTotal" id="listQuoteItem[${s_rowNum-1 }].itemPriceTotal" value="${s.itemPriceTotal }" readonly="readonly"/><br/>
								NEGO :<input style="width:95%;text-align: right" type="text" name="listQuoteItem[${s_rowNum-1 }].itemPriceNegoTotal" id="listQuoteItem[${s_rowNum-1 }].itemPriceTotalNego" value="${s.itemPriceNegoTotal }" readonly="readonly" onkeyup="calculateData(this,${s_rowNum-1});"/>
							</display:column>
							<display:column title="Harga Total Penawaran Ppn" style="text-align:right">
								LAMA :<input style="width:95%;text-align: right" type="text" name="listQuoteItem[${s_rowNum-1 }].itemPriceTotalPpn" id="listQuoteItem[${s_rowNum-1 }].itemPriceTotalPpn" value="${s.itemPriceTotalPpn }" readonly="readonly" /><br/>
								NEGO :<input style="width:95%;text-align: right" type="text" name="listQuoteItem[${s_rowNum-1 }].itemPriceNegoTotalPpn" id="listQuoteItem[${s_rowNum-1 }].itemPriceTotalPpnNego" value="${s.itemPriceNegoTotalPpn }" readonly="readonly" onkeyup="calculateData(this,${s_rowNum-1});"/>
							</display:column>
							<display:column>
								<input type="hidden" name="listQuoteItem[${s_rowNum-1 }].id.itemId" value="${s.id.itemId }"/>
								<input type="hidden" name="listQuoteItem[${s_rowNum-1 }].id.ppmId" value="${s.id.ppmId }"/>
								<input type="hidden" name="listQuoteItem[${s_rowNum-1 }].id.vendorId" value="${s.id.vendorId }"/>
								<input type="hidden" name="listQuoteItem[${s_rowNum-1 }].itemPajak" id="listQuoteItem[${s_rowNum-1 }].itemPajak" value="${s.itemPajak }"/>
								
								<input type="hidden" name="listQuoteItem[${s_rowNum-1 }].prcMainItem.id" value="${s.prcMainItem.id }"/>
							</display:column>	
							<display:footer>
								<tr>
									<td colspan="8"></td>
									<td ></td>
								</tr>
							</display:footer>						
						</display:table>
					</div>
					
					
					<div class="content center" id="tipeB" style="display:none">
						<display:table name="${listQuoteItem }" id="s" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
							<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum} B</display:column>
							<display:column title="Kode"  class="center">${s.prcMainItem.itemCode }</display:column>
							<display:column title="Deskripsi" class="left">
								${s.prcMainItem.itemDescription }
								<input type="hidden" name="listQuoteItemB[${s_rowNum-1 }].itemDescription" id="listQuoteItemB[${s_rowNum-1 }].itemDescription" value="${s.itemDescription }"/>
							</display:column>
							<display:column title="Keterangan" class="left">
								${s.prcMainItem.keterangan }
							</display:column>
							<display:column title="Satuan" style="width: 40px;text-align:right">
								${s.prcMainItem.admUom.uomName }
							</display:column>
							<display:column title="Jumlah" style="text-align:right">
								${s.itemQuantity }
							</display:column>
							<display:column title="Jumlah Penawaran (non PPN)" style="text-align:right">
								<input style="width:95%;text-align: right" type="text" name="listQuoteItemB[${s_rowNum-1 }].itemQuantity" id="listQuoteItemB[${s_rowNum-1 }].itemQuantity" value="${s.itemQuantity }" class="number required" onkeyup="calculateDataB(this,${s_rowNum-1});" readonly="readonly"/>
							</display:column>
							
							<display:column title="Harga Penawaran (non PPN)" style="text-align:right">
								LAMA :<input style="width:95%;text-align: right" type="text" name="listQuoteItemB[${s_rowNum-1 }].itemPrice" id="listQuoteItemB[${s_rowNum-1 }].itemPrice" value="${s.itemPrice }" class="number required" /><br/>
								NEGO :<input style="width:95%;text-align: right" type="text" name="listQuoteItemB[${s_rowNum-1 }].itemPriceNego" id="listQuoteItemB[${s_rowNum-1 }].itemPriceNego" value="${s.itemPriceNego }" class="number required" onkeyup="calculateDataB(this,${s_rowNum-1});"/>
							</display:column>
							<display:column title="Pajak (%)" style="text-align:right">
								${s.itemPajak }
							</display:column>
							<display:column title="Harga Total Penawaran" style="text-align:right">
								LAMA :<input style="width:95%;text-align: right" type="text" name="listQuoteItemB[${s_rowNum-1 }].itemPriceTotal" id="listQuoteItemB[${s_rowNum-1 }].itemPriceTotal" value="${s.itemPriceTotal }" readonly="readonly" /><br/>
								NEGO :<input style="width:95%;text-align: right" type="text" name="listQuoteItemB[${s_rowNum-1 }].itemPriceNegoTotal" id="listQuoteItemB[${s_rowNum-1 }].itemPriceTotalNego" value="${s.itemPriceNegoTotal }" readonly="readonly" onkeyup="calculateDataB(this,${s_rowNum-1});"/>
							</display:column>
							<display:column title="Harga Total Penawaran Ppn" style="text-align:right">
								LAMA :<input style="width:95%;text-align: right" type="text" name="listQuoteItemB[${s_rowNum-1 }].itemPriceTotalPpn" id="listQuoteItemB[${s_rowNum-1 }].itemPriceTotalPpn" value="${s.itemPriceTotalPpn }" readonly="readonly" /><br/>
								NEGO :<input style="width:95%;text-align: right" type="text" name="listQuoteItemB[${s_rowNum-1 }].itemPriceNegoTotalPpn" id="listQuoteItemB[${s_rowNum-1 }].itemPriceTotalPpnNego" value="${s.itemPriceNegoTotalPpn }" readonly="readonly" onkeyup="calculateDataB(this,${s_rowNum-1});"/>
							</display:column>
							<display:column>
								<input type="hidden" name="listQuoteItemB[${s_rowNum-1 }].id.itemId" value="${s.id.itemId }"/>
								<input type="hidden" name="listQuoteItemB[${s_rowNum-1 }].id.ppmId" value="${s.id.ppmId }"/>
								<input type="hidden" name="listQuoteItemB[${s_rowNum-1 }].id.vendorId" value="${s.id.vendorId }"/>
								<input type="hidden" name="listQuoteItemB[${s_rowNum-1 }].itemPajak" id="listQuoteItemB[${s_rowNum-1 }].itemPajak" value="${s.itemPajak }"/>
								
								<input type="hidden" name="listQuoteItemB[${s_rowNum-1 }].prcMainItem.id" value="${s.prcMainItem.id }"/>
							</display:column>	
							<display:footer>
								<tr>
									<td colspan="8"></td>
									<td ></td>
								</tr>
							</display:footer>						
						</display:table>
					</div>
					
					
					<div class="content center" id="tipeC" style="display:none">
						<display:table name="${listQuoteItem }" id="s" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
							<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum} C</display:column>
							<display:column title="Kode"  class="center">${s.prcMainItem.itemCode }</display:column>
							<display:column title="Deskripsi" class="left">
								<textarea name="listQuoteItemC[${s_rowNum-1 }].itemDescription" id="listQuoteItemC[${s_rowNum-1 }].itemDescription" value="${s.itemDescription }" class="grow" style="width:95%" readonly="readonly"></textarea>
							</display:column>
							<display:column title="Satuan" style="width: 40px;text-align:right">
								${s.prcMainItem.admUom.uomName }
							</display:column>
							<display:column title="Jumlah" style="text-align:right">
								${s.itemQuantity }
							</display:column>
							<display:column title="Jumlah Penawaran (non PPN)" style="text-align:right">
								<input style="width:95%;text-align: right" type="text" name="listQuoteItemC[${s_rowNum-1 }].itemQuantity" id="listQuoteItemC[${s_rowNum-1 }].itemQuantity" value="${s.itemQuantity }" class="number required" readonly="readonly" onkeyup="calculateDataC(this,${s_rowNum-1});"/>
							</display:column>
							<display:column title="Harga Penawaran (non PPN)" style="text-align:right">
								LAMA :<input style="width:95%;text-align: right" type="text" name="listQuoteItemC[${s_rowNum-1 }].itemPrice" id="listQuoteItemC[${s_rowNum-1 }].itemPrice" value="${s.itemPrice }" class="number required" /><br/>
								NEGO :<input style="width:95%;text-align: right" type="text" name="listQuoteItemC[${s_rowNum-1 }].itemPriceNego" id="listQuoteItemC[${s_rowNum-1 }].itemPriceNego" value="${s.itemPriceNego }" class="number required" onkeyup="calculateDataC(this,${s_rowNum-1});"/>
							</display:column>
							<display:column title="Pajak (%)" style="text-align:right">
								${s.itemPajak }
							</display:column>
							<display:column title="Harga Total Penawaran" style="text-align:right">
								LAMA :<input style="width:95%;text-align: right" type="text" name="listQuoteItemC[${s_rowNum-1 }].itemPriceTotal" id="listQuoteItemC[${s_rowNum-1 }].itemPriceTotal" value="${s.itemPriceTotal }" readonly="readonly" /><br/>
								NEGO :<input style="width:95%;text-align: right" type="text" name="listQuoteItemC[${s_rowNum-1 }].itemPriceNegoTotal" id="listQuoteItemC[${s_rowNum-1 }].itemPriceTotalNego" value="${s.itemPriceNegoTotal }" readonly="readonly" onkeyup="calculateDataC(this,${s_rowNum-1});"/>
							</display:column>
							<display:column title="Harga Total Penawaran Ppn" style="text-align:right">
								LAMA :<input style="width:95%;text-align: right" type="text" name="listQuoteItemC[${s_rowNum-1 }].itemPriceTotalPpn" id="listQuoteItemC[${s_rowNum-1 }].itemPriceTotalPpn" value="${s.itemPriceTotalPpn }" readonly="readonly"/>
								NEGO :<input style="width:95%;text-align: right" type="text" name="listQuoteItemC[${s_rowNum-1 }].itemPriceNegoTotalPpn" id="listQuoteItemC[${s_rowNum-1 }].itemPriceTotalPpnNego" value="${s.itemPriceNegoTotalPpn }" readonly="readonly" onkeyup="calculateDataC(this,${s_rowNum-1});"/>
							</display:column>
							<display:column>
								<input type="hidden" name="listQuoteItemC[${s_rowNum-1 }].id.itemId" value="${s.id.itemId }"/>
								<input type="hidden" name="listQuoteItemC[${s_rowNum-1 }].id.ppmId" value="${s.id.ppmId }"/>
								<input type="hidden" name="listQuoteItemC[${s_rowNum-1 }].id.vendorId" value="${s.id.vendorId }"/>
								<input type="hidden" name="listQuoteItemC[${s_rowNum-1 }].itemPajak" id="listQuoteItemC[${s_rowNum-1 }].itemPajak" value="${s.itemPajak }"/>
								
								<input type="hidden" name="listQuoteItemC[${s_rowNum-1 }].prcMainItem.id" value="${s.prcMainItem.id }"/>
							</display:column>	
							
							<display:footer>
								<tr>
									<td colspan="8"></td>
									<td ></td>
								</tr>
							</display:footer>						
						</display:table>
					</div>
					<div class="content">
						<table class="form formStyle" >
							
							<tr>
								<td width="200px">Total Penawaran</td>
								<td>
									<s:textfield name="quote.pqmTotalPenawaran" id="totalPenawaran" cssClass="number required" readonly="true"></s:textfield>
								</td>
							</tr>
							
							<tr>
								<td width="200px">Bid Bond (minimal)</td>
								<td>
									<s:textfield name="quote.pqmBidBond"  cssClass="number required"></s:textfield>
								</td>
							</tr>
							
							<tr>
								<td width="200px">Total Penawaran Nego</td>
								<td>
									<s:textfield name="quote.pqmTotalPenawaranNego" id="totalPenawaranNego" cssClass="number required" readonly="true"></s:textfield>
								</td>
							</tr>
							<c:if test="${not empty quote.pqmAttachment }">
							<tr>
								<td width="200px">Attachment</td>
								<td>
									<a target="blank" href="${contextPath }/upload/file/${quote.pqmAttachment }">${quote.pqmAttachment }</a>
								</td>
							</tr>
							</c:if>
						</table>
					</div>					
					</div>
					
				<div class="uibutton-toolbar extend">
					<s:submit name="saveAndFinish" value="Simpan dan Selesai" cssClass="uibutton"></s:submit>
					<a href="javascript:void(0)" class="uibutton" onclick="confirmPopUp('regCancel();', 'Batal', 'Apakah anda yakin untuk membatalkan transaksi ?', 'Ya', 'Tidak');"><s:text name="cancel"></s:text> </a>					
				</div>
						
			</s:form>
			
		</div>
	</div>
</body>
</html>