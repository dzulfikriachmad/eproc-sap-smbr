<%@ include file="/include/definitions.jsp" %>
<script>
$(document).ready(function(){
	var image_load = "<div style='text-align: center;' ><img src='"+imageLoading+"' /></div>";
	var idPrcHeader = $("#prcMainHeaderVal").val();
	$.ajax({
        url: "<s:url action="ajax/vnd/addSanggahan" />", 
        data: {"prcMainHeader.ppmId":idPrcHeader},
        beforeSend: function(){
        	$("#msgArea").html(image_load);
        },
        success: function(response){
        	$("#msgArea").html(response);
            },
        type: "post", 
        dataType: "html"
    });
});
$("#sendSanggahan").click(function(){
	var image_load = "<div style='text-align: center;' ><img src='"+imageLoading+"' /></div>";
	$.ajax({
        url: "<s:url action="ajax/vnd/addSanggahan" />", 
        data: $(document.formSanggahan.elements).serialize(),
        beforeSend: function(){
        	$("#msgArea").html(image_load);
        },
        success: function(response){
        	$("#msgArea").html(response);
        	$("#msgValue").val("");
            },
        type: "post", 
        dataType: "html"
    }); 
	return false;
});
</script>
<div class='fbox_header'>Sanggahan</div>
    <div class='fbox_container' style="padding-top: 0px;">
        <div class='fbox_content' style="width: 500px;background-color:#F2F2F2;">
        	<div id="msgArea" style="width: 480px;border-radius:10px;background-color: #FFF;height: 300px;padding: 5px;">
        	
	    	</div>
	    	<s:form name="formSanggahan" id="formSanggahan">
        	<div style="width: 480px;margin-top: 5px;">
        	<s:textarea name="msgSanggahan.msgComment" id="msgValue" cssStyle="border-radius:10px;width:400px;float:left; "></s:textarea>
        	<s:submit id="sendSanggahan" name="save" cssStyle="margin-top: 20px;margin-left: 5px;" value="Kirim" cssClass="uibutton special"></s:submit>
        	</div>
        	<s:hidden name="msgSanggahan.id"></s:hidden>
           	<s:hidden name="msgSanggahan.createdDate"></s:hidden>
           	<s:hidden name="msgSanggahan.createdBy"></s:hidden>
           	<s:hidden name="msgSanggahan.updatedDate"></s:hidden>
           	<s:hidden name="msgSanggahan.updatedBy"></s:hidden>
        	<input type="hidden" name="msgSanggahan.prcMainHeader.ppmId" value="${prcMainHeader.ppmId }">
        	<input type="hidden" id="prcMainHeaderVal" name="prcMainHeader.ppmId" value="${prcMainHeader.ppmId }" />
        	</s:form>
        </div>
        <div class='fbox_footer' style="margin-top: 0px;">
            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()'>Tutup</a>
        </div>
    </div>