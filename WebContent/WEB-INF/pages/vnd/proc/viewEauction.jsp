<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Daftar Pengadaan Eauction</title>
	
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	
	<script type="text/javascript">	
		$(document).ready(function(){
			$(".chzn").chosen();
			//tgl
			$.datepicker.setDefaults($.datepicker.regional['in_ID']);
			$("#tgl").datepicker({
				changeYear: true,
				changeMonth: true
			});
			// FORM VALIDATION
			$("#procForm").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler:function(form){
					$('#totalPenawaran').val($('#totalPenawaran').autoNumeric('get'));
					var lastBid = parseFloat($('#lastBidding').val());
					var lastTawar = parseFloat($('#totalPenawaran').val());
					if(lastBid <= lastTawar){
						//alert('Penawaran Harus Lebih Rendah dari penawaran sebelumnya');
						alertPopUp("Eauction", "Penawaran Harus Lebih Rendah dari penawaran sebelumnya", "Tutup");
						return false;
					}
					loadingScreen();
					
					form.submit();
				}
			});	
			getEauctionTime();
			getLowestBidder();
			onloadData();
		});

		//regCancel Action
		function regCancel(){
			jQuery.facebox.close();
			window.location = "<s:url action="vnd/eauction" />"; 
		}
	
		
		//function onloadData
		function onloadData(){
			$('#totalPenawaran').autoNumeric('init');
		}
		
		 //Server Time
		function getEauctionTime(){
			$.ajax({
		        url: "<s:url action="timerEauctionJson" />?ppmId=${prcMainHeader.ppmId}",
		        success: function(response){
		        	var remaining = response.remaningTime;
		        	if(remaining==0 || remaining <=1){
		        		$('#btn').hide();
		        		$('#cls').show();
		        		//clearInterval(countdown);
		        	}else{
		        		$('#btn').show();
		        		$('#cls').hide();
		        	}
		        	countdownTime(remaining);
		        	/* var countdown = setInterval(function(){
			        	if (remaining < 1){
			        		remaining = 0;
			        		clearInterval(countdown);
			        	}else{
			        		remaining--;
			        	}
		        	}, 1000) */
		        	//setTimeEauction(remaining);
		        },
		        type: "post", 
		        dataType: "json"
		    }); 
		}
	    
		function countdownTime(time){
			var countdown = setInterval(function(){
				$('#timeEauction').html(time);
	        	if (time < 1){
	        		time = 0;
	        		clearInterval(countdown);
	        	}else{
	        		time--;
	        	}
	        	
        	}, 1000);
					
		}
		 
		function getLowestBidder(){
			$.ajax({
		        url: "<s:url action="bidderEauctionJson" />?ppmId=${prcMainHeader.ppmId}",
		        success: function(response){
		        	var lowest = response.lowestBid;
		        	setLowestBidder(lowest);
		        },
		        type: "post", 
		        dataType: "json"
		    }); 
		}
		
	    function setTimeEauction(remaining){
	    	$('#timeEauction').html(remaining);
	    	//setTimeout("getEauctionTime()",1000);
	    }
	    
	    function setLowestBidder(lowest){
	    	$('#lowestBid').html(lowest);
	    	setTimeout("getLowestBidder()",1000);
	    	//setTimeout("getEauctionTime()",1000);
	    }
	    
	    
	   
	</script>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="vnd/dashboard">Home</s:a></li> 
	        
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
    
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
					
			<s:form action="vnd/eauction" id="procForm" name="procForm" enctype="multipart/form-data" method="post">
				<s:hidden name="prcMainHeader.ppmId" />
				<s:hidden name="quote.id.ppmId" ></s:hidden>
				<s:hidden name="quote.id.vendorId"></s:hidden>
				<s:hidden name="eauctionHeader.ppmId"></s:hidden>

				
				<c:if test="${eauctionHeader.latihan == 1 }">
					<div class="box" id= "latihan">
					
						<div class = "content">
						<span style="font-size: 100px;color: green;font-weight: bold;">
						<marquee> HANYA LATIHAN </marquee>
						</span>
						</div>
					
				</div>
				</c:if>
				
				
				<div class="box">
					<div class="title">
						Header
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle" >
							<tr>
								<td width="200px">Nomor SPPH (*)</td>
								<td>
									${eauctionHeader.prcMainHeader.ppmNomorRfq }
								</td>
							</tr>
							
							<tr>
								<td width="200px">Deskripsi Spph (*)</td>
								<td>
									${eauctionHeader.prcMainHeader.ppmSubject}
								</td>
							</tr>
							
							<tr>
								<td width="200px">Judul Eauction (*)</td>
								<td>
									${eauctionHeader.judul }
								</td>
							</tr>
							<tr>
								<td width="200px">Deskripsi</td>
								<td>
									${eauctionHeader.deskripsi }
								</td>
							</tr>
							
							<tr>
								<td width="200px">Tanggal Mulai</td>
								<td>
									<fmt:formatDate value="${eauctionHeader.tanggalMulai }" pattern="dd.MMM.yyyy HH:mm:ss"/>
								</td>
							</tr>
			
							<tr>
								<td width="200px">Waktu (menit)</td>
								<td>
									${eauctionHeader.waktu }
								</td>
							</tr>
							
						</table>
						
					</div>
				</div>
				<div class="box">
					<div class="title">
						Waktu
						<span class="hide"></span>
					</div>
					<div class="content center" >
						<span id="timeEauction" style="font-size: xx-large;font-weight: bolder;color: red"></span>
						<br/>
						<span style="font-size: large;color: green;font-weight: bold;">
						PENAWARAN TERAKHIR :
						<input type="hidden" id="lastBidding" value="${eauctionVendor.lastBid}"/>
						<c:if test="${eauctionVendor.lastBid!=null}">
							<fmt:formatNumber> ${eauctionVendor.lastBid}</fmt:formatNumber>
						</c:if> 
						</span>
						<br/>
						<br/>
						<span style="font-size: large;color: BLUE;font-weight: bold;">
						HARGA TERENDAH :
							<span id="lowestBid"></span>
						</span>
					</div>
				</div>	  
			
				
				<div class="box">
					<div class="title">
						Penawaran
						<span class="hide"></span>
					</div>
					<div class="content center">
						<s:hidden name="eauctionVendor.id"></s:hidden>
						<s:hidden name="eauctionVendor.vndHeader.vendorId"></s:hidden>
						<s:textfield name="eauctionVendor.lastBid"  id="totalPenawaran" cssClass="required" cssStyle="height:70px;font-weight:bolder;font-size: 70px;text-align:right"></s:textfield>
					</div>
					
										
					</div>
					
				<div class="uibutton-toolbar extend">
					<div id="btn" >
						<s:submit name="save" value="Simpan BID" cssClass="uibutton"></s:submit>
					</div>
					<div id="cls" style="display: none">
					<a href="javascript:void(0)" class="uibutton" onclick="confirmPopUp('regCancel();', 'Batal', 'Apakah anda yakin untuk membatalkan transaksi ?', 'Ya', 'Tidak');">Tutup / Close </a>
					</div>					
				</div>
						
			</s:form>
			
		</div>
	</div>
</body>
</html>