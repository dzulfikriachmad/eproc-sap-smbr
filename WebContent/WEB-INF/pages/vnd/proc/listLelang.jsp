<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<title>Daftar Pengadaan</title>
	
	<script type="text/javascript">
		function setValue(ppmId){
			$('#ppmId').val(ppmId);
		}
	</script>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="vnd/dashboard">Home</s:a></li> 
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
	
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
			
				<div class="box">
					<div class="title">
						Daftar Pengadaan
						<span class="hide"></span>
					</div>
					
					<div class="content">						
						<s:form action="vnd/daftarLelang">
						<s:hidden name="prcMainHeader.ppmId" id="ppmId"></s:hidden>
						<display:table name="${listLelang }" id="s" excludedParams="*" style="width: 100%;" class="all style1">
							<display:setProperty name="paging.banner.full" value=""></display:setProperty>
							<display:setProperty name="paging.banner.first" value=""></display:setProperty>
							<display:setProperty name="paging.banner.last" value=""></display:setProperty>
							
							<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
							<display:column title="Permintaan" total="true" style="width: 200px;">
								<c:choose>
									<c:when test="${s.nomorSpph==null or s.nomorSpph=='' }">
										${s.nomorPr }
									</c:when>
									<c:otherwise>
										${s.nomorSpph }
									</c:otherwise>
								</c:choose>
							</display:column>
							<display:column title="Nama Permintaan" style="width: 150px; " class="center">${s.judulSpph}</display:column>
							<display:column title="User" style="width: 150px;" class="center">${s.completeName }</display:column>
							<display:column title="Tanggal" class="center" style="width: 200px;"><fmt:formatDate value="${s.updatedDate }" pattern="HH:mm:ss, dd MMM yyyy"/> </display:column>
							<display:column title="Status" class="center" style="width: 200px;">
								${s.keterangan }
							</display:column>
							<display:column title="Proses" class="center" style="width: 200px;">
								<input type="submit" name="proses" value="Lihat" onclick="setValue(${s.ppmId});loadingScreen();" class="uibutton"/>
							</display:column>
						</display:table> 
						</s:form>
					</div>
				</div>
		</div>
	</div>
</body>
</html>