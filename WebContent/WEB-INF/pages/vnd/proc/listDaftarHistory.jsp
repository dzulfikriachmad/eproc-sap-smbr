<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<title>Daftar Pengadaan</title>
	
	<script type="text/javascript">
		function setValue(ppmId,vendorId){
			$('#ppmId').val(ppmId);
			$('#vPpmId').val(ppmId);
			$('#vendorId').val(vendorId);
		}
	</script>
	<script>
			$(document).ready(function() {
				// DATATABLE
			    $('table.proc').dataTable({
			        "bInfo": false,
			        "iDisplayLength": 5,
			        "aLengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
			        "sPaginationType": "full_numbers",
			        "bPaginate": true,
			        "sDom": '<f>t<pl>'
			    });
				
			    $('table.con').dataTable({
			        "bInfo": false,
			        "iDisplayLength": 5,
			        "aLengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
			        "sPaginationType": "full_numbers",
			        "bPaginate": true,
			        "sDom": '<f>t<pl>'
			    });
			});
		</script>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="vnd/dashboard">Home</s:a></li> 
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
	
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
			
				<div class="box">
					<div class="title">
						Daftar Pengadaan
						<span class="hide"></span>
					</div>
					
					<div class="content">						
						<s:form action="vnd/daftarHistory">
							<s:hidden name="prcMainHeader.ppmId" id="ppmId" />
							<s:hidden name="prcMainVendor.id.ppmId" id="vPpmId" />
							<s:hidden name="prcMainVendor.id.vendorId" id="vendorId" />
							<display:table name="${listTodo }" id="s" excludedParams="*" style="width: 100%;" class="proc style1">
								<display:setProperty name="paging.banner.full" value=""></display:setProperty>
								<display:setProperty name="paging.banner.first" value=""></display:setProperty>
								<display:setProperty name="paging.banner.last" value=""></display:setProperty>
								
								<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
								<display:column title="Pengadaan">${s.nomorRfq }</display:column>
								<display:column title="Status" class="center">
									<c:if test="${s.status==2 }">Di Undang <br/> (Invited)</c:if>
									<c:if test="${s.status==3 }">Mendaftar <br/> Belum Menawarkan (Registered)</c:if>
									<c:if test="${s.status==4 }">Mengirimkan Penawaran <br/> (Offer)</c:if>
									<c:if test="${s.status==5 }">Lulus Administrasi <br/> (Administation Passed)</c:if>
									<c:if test="${s.status==6 }">Lulus Teknis <br/> (Tecnical Evaluation Passed)</c:if>
									<c:if test="${s.status==7 }">Lulus Harga <br/> (Price Evaluation Passed)</c:if>
									<c:if test="${s.status==-3 }">Tidak Mendaftar <br/> (Not register)</c:if>
									<c:if test="${s.status==-5 }">Tidak Lulus Administrasi <br/> (Administration Failed)</c:if>
									<c:if test="${s.status==-6 }">Tidak Lulus Teknis <br/> (Technical Evaluation Failed)</c:if>
									<c:if test="${s.status==-7 }">Tidak Lulus Harga <br/> (Price Evaluation Failed)</c:if>
								</display:column>
								<display:column title="Administrasi" class="center">
									<c:if test="${s.administrasi==1 }">Lulus Administrasi <br/> (Administration Passed)</c:if>
									<c:if test="${s.administrasi==-1 }">Tidak Lulus Administrasi <br/> (Administration Failed)</c:if>
								</display:column>
								<display:column title="Teknis" class="center">
									<c:if test="${s.teknis==1 }">Lulus Teknis <br/> (Technical Evaluation Passed)</c:if>
									<c:if test="${s.teknis==-1 }">Tidak Lulus Teknis <br/> (Technical Evaluation Failed)</c:if>
								</display:column>
								<display:column title="Harga" class="center">
									 <c:if test="${s.harga==1 }">Lulus Harga <br/> (Price Evaluation Passed)</c:if>
									<c:if test="${s.harga==-1 }">Tidak Lulus Harga <br/> (Price Evaluation Failed)</c:if>
								</display:column>
								<display:column title="Aksi" class="center" style="width: 100px;">
									<input type="submit" name="proses" value="Proses" onclick="setValue('${s.ppmId }','${s.vendorId }');loadingScreen();" class="uibutton" />
									<s:url var="set" action="ajax/viewPenawaranRead" />
									<a title="Penawaran" class="uibutton" href='javascript:void(0)' onclick='load_into_box("${set}", "prcMainHeader.ppmId=${s.ppmId }&prcMainVendor.id.ppmId=${s.ppmId}&prcMainVendor.id.vendorId=${s.vendorId}");'>View Penawaran (Quotation)</a>
								</display:column>
							</display:table>
						</s:form>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Daftar Kontrak / PO
						<span class="hide"></span>
					</div>
					
					<div class="content">						
							<display:table name="${listKontrak }" id="k" excludedParams="*" style="width: 100%;" class="con style1">
								<display:setProperty name="paging.banner.full" value=""></display:setProperty>
								<display:setProperty name="paging.banner.first" value=""></display:setProperty>
								<display:setProperty name="paging.banner.last" value=""></display:setProperty>
								<display:column title="No." style="width: 20px; text-align: center;">${k_rowNum}</display:column>
								<display:column title="Nomor Kontrak PO">${k.contractNumber }</display:column>
								<display:column title="Jumlah" class="center">
									<fmt:formatNumber>${k.contractAmount }</fmt:formatNumber>
								</display:column>
								<display:column title="Nomor Pembayaran" class="center">
									${k.nomorPembayaran }
								</display:column>
								<display:column title="Judul Kontrak" class="center">
									${k.judulKontrak }
								</display:column>
								<display:column title="Tanggal Mulai" class="center">
									<fmt:formatDate value="${k.tglKontrakAwal }" pattern="dd MMM yyyy"/>
								</display:column>
								<display:column title="Tanggal Berakhir" class="center">
									<fmt:formatDate value="${k.tglKontrakAkhir }" pattern="dd MMM yyyy"/>
								</display:column>
								<display:column title="Status Tagihan" class="center">
									<c:if test="${fn:trim(k.statusPembayaran)=='H' }">Voucher Barang Terbit</c:if>
									<c:if test="${fn:trim(k.statusPembayaran)=='V' }">Aproved Verifikasi</c:if>
									<c:if test="${fn:trim(k.statusPembayaran)=='A' }">Approved Bendahara</c:if>
									<c:if test="${fn:trim(k.statusPembayaran)=='P' }">Sudah Dibayar</c:if>
								</display:column>
								<display:column title="" class="center" style="">
									
								</display:column>
							</display:table>
					</div>
				</div>
		</div>
	</div>
</body>
</html>