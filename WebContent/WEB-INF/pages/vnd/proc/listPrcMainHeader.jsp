<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Laporan Statistik Proses Pengadaan</title>
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	<script type="text/javascript">
		$(document).ready(function(){	
			$('table.doc').dataTable({
		        "bInfo": false,
		        "iDisplayLength": 10,
		        "aLengthMenu": [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
		        "sPaginationType": "full_numbers",
		        "bPaginate": true,
		        "sDom": '<f>t<pl>'
		        });
		});
		$(".sanggah").live('click',function(){
		   var idPrcHeader=$(this).attr("id").replace("sanggah_","");
		   $.ajax({
		        url: "<s:url action="ajax/vnd/listSanggahan" />", 
		        data: {"prcMainHeader.ppmId":idPrcHeader},
		        beforeSend: function(){
		        	jQuery.facebox('<div style="padding: 17px; font-size: 36px;">Loading....</div>');
		        },
		        success: function(response){
		                jQuery.facebox(response);
		            },
		        type: "post", 
		        dataType: "html"
		    });
		});
	</script>
	<script type="text/javascript" src="${contextPath}/assets/js/FusionCharts.js"></script>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="dashboard">Home</s:a></li> 
	        <li>Pengadaan</li> 
	        <li>Sanggahan</li> 
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
    
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
			
				<div class="box">
					<div class="title">
						Laporan Statistik Proses Pengadaan
						<span class="hide"></span>
					</div>
					<div class="content center">
						<display:table name="${listPrcMainHeader }" id="s" style="width: 100%;" class="doc style1" requestURI="ajax/adm/searchPengadaan.jwebs" >
							<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
							<display:column title="Nomor Pr" total="true" style="width: 200px;">
									${s.ppmNomorPr }
							</display:column>
							<display:column title="Nomor SPPH" total="true" style="width: 200px;">
								${s.ppmNomorRfq }
							</display:column>
							<display:column title="Nama Permintaan" style="width: 150px; "
								class="center">${s.ppmSubject }</display:column>
							<display:column title="Tanggal" class="center" style="width: 200px;">
								<fmt:formatDate value="${s.createdDate }"
									pattern="HH:mm:ss, dd MMM yyyy" />
							</display:column>
							<display:column title="Aksi" class="center" style="width: 200px;">
								<a id="sanggah_${s.ppmId }" class="uibutton sanggah">Sanggah</a>
							</display:column>
						</display:table>
					</div>
				</div>
				
		</div>
	</div>
</body>
</html>