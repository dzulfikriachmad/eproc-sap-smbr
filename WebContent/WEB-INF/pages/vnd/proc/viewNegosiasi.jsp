<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Negosiasi Pengadaan</title>
	
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	
	<script type="text/javascript">	
		$(document).ready(function(){
			$(".chzn").chosen();
			
			// FORM VALIDATION
			$("#procForm").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler:function(form){
					loadingScreen();
					form.submit();
				}
			});
			
			
		});
		
		
		//regCancel Action
		function regCancel(){
			window.location = "<s:url action="vnd/negosiasi" />"; 
		}
		
	</script>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="vnd/dashboard">Home</s:a></li> 
	        
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
    
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
					
			<s:form action="vnd/negosiasi" id="procForm" name="procForm" enctype="multipart/form-data" method="post">
				<s:hidden name="prcMainHeader.ppmId" />
				<s:hidden name="prcMainVendor.id.vendorId"></s:hidden>
				<s:hidden name="prcMainVendor.id.ppmId"></s:hidden>
				<div class="box">
					<div class="title">
						Header
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle" >
							<tr>
								<td width="150px">No. SPPH</td>
								<td>
									${prcMainHeader.ppmNomorRfq } <br/>
									<s:url action="vnd/ubahPenawaranRfq" id="ur"></s:url>
									<a href="${ur }?876uyt65639ilokjhgt098767234679=1&prcMainHeader.ppmId=${prcMainHeader.ppmId}&876uyt65639ilokjhgt098767234679=12098poiljghagshtytwe861827&prcMainVendor.id.ppmId=${prcMainVendor.id.ppmId}&prcMainVendor.id.vendorId=${prcMainVendor.id.vendorId}" style="font-weight: bold;color: red">Ubah Penawaran</a>
								</td>
							</tr>
							
							<tr>
								<td width="150px">Nama Permintaan</td>
								<td>${prcMainHeader.ppmSubject }</td>
							</tr>
							<tr>
								<td>Deskripsi Permintaan</td>
								<td>${prcMainHeader.ppmDescription } </td>
							</tr>
							<tr>
								<td>Jenis Permintaan</td>
								<td>
									<c:if test="${prcMainHeader.ppmJenisDetail == null}">
										<c:if test="${prcMainHeader.ppmJenis.id == 1 }">Barang</c:if>
										<c:if test="${prcMainHeader.ppmJenis.id == 2 }">Jasa</c:if>
									</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 3 }">Barang Raw Material</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 4 }">Barang Sparepart</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 5 }">Barang Umum</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 6 }">Jasa Teknik</c:if>
									<c:if test="${prcMainHeader.ppmJenisDetail.id == 7 }">Jasa Umum</c:if>
								</td>
							</tr>
							
							<tr>
								<td>Ke</td>
								<td>
									<s:hidden name="negosiasi.prcMainHeader.ppmId" value="%{prcMainHeader.ppmId}"></s:hidden>
            						<s:hidden name="negosiasi.vndHeader.vendorId" value="%{prcMainVendor.id.vendorId}"></s:hidden>
									<s:textfield name="negosiasi.msgTo" readonly="true" value="PEMBELI"></s:textfield>
								</td>
							</tr>
							
							<tr>
								<td>Pesan</td>
								<td>
									<s:textarea name="negosiasi.msgComment" cssClass="required"></s:textarea>
								</td>
							</tr>
							
							
						</table>
						
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Negosiasi
						<span class="hide"></span>
					</div>
					<div class="content center">
												
						<s:set id="i" value="0"></s:set>
							<display:table name="${listNegosiasi }" id="s" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
								<display:caption>Histori Negosiasi</display:caption>
								<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
								<display:column title="Dari" class="center">
									${fn:toUpperCase(s.msgFrom) }
								</display:column>
								<display:column title="Ke" class="center">
									${fn:toUpperCase(s.msgTo) }
								</display:column>
								<display:column title="Pesan"  class="center">
									${fn:toUpperCase(s.msgComment) }
								</display:column>
								<display:column title="Tanggal Pesan"  class="center">
									<fmt:formatDate value="${s.msgDate }" pattern="dd.MM.yyyy HH:mm:ss"/>	
								</display:column>
							<s:set var="i" value="#i+1"></s:set>
							</display:table>
					</div>
				</div>
				
				
				
				<div class="uibutton-toolbar extend">
					<s:submit name="saveAndFinish" value="Simpan" cssClass="uibutton"></s:submit>
					<a href="javascript:void(0)" class="uibutton" onclick="confirmPopUp('regCancel();', 'Batal', 'Apakah anda yakin untuk membatalkan transaksi ?', 'Ya', 'Tidak');"><s:text name="cancel"></s:text> </a>					
				</div>
						
			</s:form>
			
		</div>
	</div>
</body>
</html>