<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<title>Daftar Eauction</title>
	
	<script type="text/javascript">
		function setValue(ppmId){
			$('#ppmId').val(ppmId);
		}
	</script>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="vnd/dashboard">Home</s:a></li> 
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
	
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
			
				<div class="box">
					<div class="title">
						Daftar Eauction
						<span class="hide"></span>
					</div>
					
					<div class="content">						
						<s:form action="vnd/eauction">
							<s:hidden name="prcMainHeader.ppmId" id="ppmId" />
							<display:table name="${listEauctionHeader }" id="s" excludedParams="*" style="width: 100%;" class="all style1">
								<display:setProperty name="paging.banner.full" value=""></display:setProperty>
								<display:setProperty name="paging.banner.first" value=""></display:setProperty>
								<display:setProperty name="paging.banner.last" value=""></display:setProperty>
								
								<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
								<display:column title="No. Pengadaan">${s.prcMainHeader.ppmNomorRfq }</display:column>
								<display:column title="Pengadaan">${s.prcMainHeader.ppmSubject }</display:column>
								<display:column title="Tanggal Mulai Eauction" class="center">
									<fmt:formatDate value="${s.tanggalMulai }" pattern="dd.MM.yyyy HH:mm"/>
								</display:column>
								<display:column title="Aksi" class="center" style="width: 100px;">
									<input type="submit" name="proses" value="Lihat" onclick="setValue('${s.ppmId }');loadingScreen();" class="uibutton" />
								</display:column>
							</display:table>
						</s:form>
					</div>
				</div>
		</div>
	</div>
</body>
</html>