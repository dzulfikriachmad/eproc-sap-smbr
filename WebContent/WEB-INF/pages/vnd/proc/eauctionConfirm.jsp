<%@ include file="/include/definitions.jsp"%>

<!DOCTYPE html>
<html lang="id">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><s:text name="tos.title"></s:text></title>
<script type="text/javascript">
	Cufon.replace('h3', {
		fontFamily : 'Caviar Dreams'
	});
</script>

</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="vnd/dashboard">Home</s:a></li> 
	        
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->

	<div id="right">
		<div class="section">

			<div class="box">
				<div class="title center">
					KETENTUAN EAUCTION ePROCUREMENT PT SEMEN BATURAJA (PERSERO) Tbk.
					<span class="hide"></span>
				</div>
				<div class="content">
					<!-- TOS 
						<h2><s:label key="tos.breadcrumbs"></s:label></h2>-->
				
					<!--<ol>
							 <s:text name="tos.content"></s:text>
						</ol> -->
						<p style="text-align: justify;">Saya yang mengikuti proses e-auction pengadaan barang/jasa nomor <b><c:out value="${prcMainHeader.ppmNomorRfq }"/></b>
						<br/>
						Dengan ini menyatakan dengan sebenarnya bahwa :</p>
						<ol>
							<li> Saya akan mematuhi keputusan / hasil dari e-auction ini</li>
							<li> Tidak melakukan praktek KKN (Kolusi, Korupsi & Nepotisme)</li>
							<li> Bersedia mengundurkan diri atau di diskualifikasi jika terjadi kesalahan input nilai harga 
							pada saat proses negosiasi e-auction.</li>
							<li> Saya memahami dan menerima risiko bahwa apabila melakukan input nilai harga (bidding) 
							di detik-detik terakhir, terdapat kemungkinan delay yang diakibatkan oleh jaringan internet &
							Saya tidak akan menuntut pihak PT Semen Baturaja (Persero) Tbk.</li>
						</ol>
						<p style="text-align: justify;">Demikian pernyataan ini kami sampaikan dengan sebenar-benarnya, tanpa menyembunyikan fakta dan 
						hal material apapun dan dengan demikian kami akan bertanggung jawab sepenuhnya atas kebenaran dari 
						hal-hal yang kami nyatakan disini.</p>
						<p  style="text-align: justify;">Demikian pernyataan ini kami buat untuk digunakan sebagaimana mestinya.</p>
					<p style="text-align: justify;">Apakah anda setuju untuk mengikuti Eauction?</p>

					
					<div class="uibutton-toolbar">
						<s:form action="vnd/eauction">
							<s:hidden name="prcMainHeader.ppmId" />
							<s:hidden name="quote.id.ppmId" ></s:hidden>
							<s:hidden name="quote.id.vendorId"></s:hidden>
							<s:hidden name="eauctionHeader.ppmId"></s:hidden>
							<s:submit name="setuju" key="yes" cssClass="uibutton"></s:submit>
							<s:a action="vnd/eauction" cssClass="uibutton">
								<s:text name="no"></s:text>
							</s:a>
						</s:form>

					</div>

					<!-- End of TOS -->
				</div>
			</div>

		</div>
	</div>
</body>
</html>