<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Daftar Pengadaan</title>
	
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	
	<script type="text/javascript">	
		$(document).ready(function(){
			$(".chzn").chosen();
			//tgl
			$.datepicker.setDefaults($.datepicker.regional['in_ID']);
			$("#tgl").datepicker({
				changeYear: true,
				changeMonth: true
			});
			// FORM VALIDATION
			$("#procForm").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler:function(form){
					loadingScreen();
					var size = document.getElementById('size').value;
					var tipe = $("#pqmType").val();
					for(var i=0;i<size;i++){
						var item = 'itemPrice'+i;
						$('#'+item).autoNumeric('init', {vMin: 0, mDec: 3});			
						$('#'+item).val($('#'+item).autoNumeric('get'));
						var total = 'priceTotal'+i;
						$('#'+total).autoNumeric('init');
						$('#'+total).val($('#'+total).autoNumeric('get'));
						var totalPp = 'priceTotalPpn'+i;
						$('#'+totalPp).autoNumeric('init');
						$('#'+totalPp).val($('#'+totalPp).autoNumeric('get'));
						
						var itemB = 'itemPriceB'+i;
						$('#'+itemB).autoNumeric('init', {vMin: 0, mDec: 3});
						$('#'+itemB).val($('#'+itemB).autoNumeric('get'));
						var totalB = 'priceTotalB'+i;
						$('#'+totalB).autoNumeric('init');
						$('#'+totalB).val($('#'+totalB).autoNumeric('get'));
						var totalPpB = 'priceTotalPpnB'+i;
						$('#'+totalPpB).autoNumeric('init');
						$('#'+totalPpB).val($('#'+totalPpB).autoNumeric('get'));
							
						var itemC = 'itemPriceC'+i;
						$('#'+itemC).autoNumeric('init', {vMin: 0, mDec: 3});
						$('#'+itemC).val($('#'+itemC).autoNumeric('get'));
						var totalC = 'priceTotalC'+i;
						$('#'+totalC).autoNumeric('init');
						$('#'+totalC).val($('#'+totalC).autoNumeric('get'));
						var totalPpC = 'priceTotalPpnC'+i;
						$('#'+totalPpC).autoNumeric('init');
						$('#'+totalPpC).val($('#'+totalPpC).autoNumeric('get'));
					}
					form.submit();
				}
			});	
			
			onloadData();
			
			
		});
		
		//kalkulasi
		function calculateData(obj,index){
			try{
				var item = 'itemPrice'+index;
				var total = 'priceTotal'+index;
				var totalPp = 'priceTotalPpn'+index;
				
				var totalPriceOe=0.0;
				var itemQtyOe = parseFloat(document.getElementById('listQuoteItem['+index+'].itemQuantity').value);
				var itemPriceOe = parseFloat($('#'+item).autoNumeric('get'));
				var itemTaxOe = parseFloat(document.getElementById('listQuoteItem['+index+'].itemPajak').value);
	
				totalPriceOe = itemQtyOe * itemPriceOe;
				if(isNaN(totalPriceOe))
					totalPriceOe=0;
				var totalPpn = totalPriceOe * ((itemTaxOe+100)/100);
				if(isNaN(totalPpn))
					totalPpn=0;
	
				$('#'+total).autoNumeric('set',totalPriceOe);
				$('#'+totalPp).autoNumeric('set',totalPpn);
				
				//calculate total
				var size = document.getElementById('size').value;
				var totalPenawaran = 0;
				if(size>0){
					for(var i=0;i<size;i++){
						var tp = 'priceTotalPpn'+i;
						totalPenawaran = totalPenawaran + parseFloat($('#'+tp).autoNumeric('get'));
					}	
				}
				//document.getElementById("quote.pqmTotalPenawaran").value=totalPenawaran.toFixed(2);
				document.getElementById("quote.pqmTotalPenawaran").value=totalPenawaran;
			}catch (e) {
				alert(e);
			}
			
		}
		
		function calculateDataB(obj,index){
			try{
				var item = 'itemPriceB'+index;
				var total = 'priceTotalB'+index;
				var totalPp = 'priceTotalPpnB'+index;
				
				var totalPriceOe=0.0;
				var itemQtyOe = parseFloat(document.getElementById('listQuoteItemB['+index+'].itemQuantity').value);
				var itemPriceOe = parseFloat($('#'+item).autoNumeric('get'));
				var itemTaxOe = parseFloat(document.getElementById('listQuoteItemB['+index+'].itemPajak').value);
	
				totalPriceOe = itemQtyOe * itemPriceOe;
				if(isNaN(totalPriceOe))
					totalPriceOe=0;
				var totalPpn = totalPriceOe * ((itemTaxOe+100)/100);
				if(isNaN(totalPpn))
					totalPpn=0;
	
				$('#'+total).autoNumeric('set',totalPriceOe);
				$('#'+totalPp).autoNumeric('set',totalPpn);
				
				//calculate total
				var size = document.getElementById('size').value;
				var totalPenawaran = 0;
				if(size>0){
					for(var i=0;i<size;i++){
						var tp = 'priceTotalPpnB'+i;
						totalPenawaran = totalPenawaran + parseFloat($('#'+tp).autoNumeric('get'));
					}	
				}
				//document.getElementById("quote.pqmTotalPenawaran").value=totalPenawaran.toFixed(2);
				document.getElementById("quote.pqmTotalPenawaran").value=totalPenawaran;
			}catch (e) {
				alert(e);
			}
			
		}
		
		function calculateDataC(obj,index){
			try{
				var item = 'itemPriceC'+index;
				var total = 'priceTotalC'+index;
				var totalPp = 'priceTotalPpnC'+index;
				
				var totalPriceOe=0.0;
				var itemQtyOe = parseFloat(document.getElementById('listQuoteItemC['+index+'].itemQuantity').value);
				var itemPriceOe = parseFloat($('#'+item).autoNumeric('get'));
				var itemTaxOe = parseFloat(document.getElementById('listQuoteItemC['+index+'].itemPajak').value);
	
				totalPriceOe = itemQtyOe * itemPriceOe;
				if(isNaN(totalPriceOe))
					totalPriceOe=0;
				var totalPpn = totalPriceOe * ((itemTaxOe+100)/100);
				if(isNaN(totalPpn))
					totalPpn=0;
	
				$('#'+total).autoNumeric('set',totalPriceOe);
				$('#'+totalPp).autoNumeric('set',totalPpn);
				
				//calculate total
				var size = document.getElementById('size').value;
				var totalPenawaran = 0;
				if(size>0){
					for(var i=0;i<size;i++){
						var tp = 'priceTotalPpnC'+i;
						totalPenawaran = totalPenawaran + parseFloat($('#'+tp).autoNumeric('get'));
					}	
				}
//				document.getElementById("quote.pqmTotalPenawaran").value=totalPenawaran.toFixed(2);
				document.getElementById("quote.pqmTotalPenawaran").value=totalPenawaran;
			}catch (e) {
				alert(e);
			}
			
		}
		
		//cek value
		function cekValue(obj){
			if(obj.checked){
				obj.value=1;
			}else{
				obj.value=0;
			}
		}
		//select tipe
		function selectTipe(obj){
			if($('#method').val() != 6 || $('#statusTeknis').val()==1){
				if(obj.value=='A'){
					var dt = document.getElementById('ppmTipeA').value;
					if(dt!=1){
						alert('Tipe A Tidak Diijinkan');
						obj.value='';
						return false;
					}
					$("#tipeA").show();
					$("#tipeB").hide();
					$("#tipeC").hide();
					var size = document.getElementById('size').value;
					if(size>0){
						for(var i=0;i<size;i++){
							var item = 'itemPrice'+i;
							$('#'+item).autoNumeric('init', {vMin: 0, mDec: 3});
							var total = 'priceTotal'+i;
							$('#'+total).autoNumeric('init');
							var totalPp = 'priceTotalPpn'+i;
							$('#'+totalPp).autoNumeric('init');
						}
					}
				}else if(obj.value=='B'){
					var dt = document.getElementById('ppmTipeB').value;
					if(dt!=1){
						alert('Tipe B Tidak Diijinkan');
						obj.value='';
						return false;
					}
					$("#tipeA").hide();
					$("#tipeB").show();
					$("#tipeC").hide();
					var size = document.getElementById('size').value;
					if(size>0){
						for(var i=0;i<size;i++){
							var item = 'itemPriceB'+i;
							$('#'+item).autoNumeric('init', {vMin: 0, mDec: 3});
							var total = 'priceTotalB'+i;
							$('#'+total).autoNumeric('init');
							var totalPp = 'priceTotalPpnB'+i;
							$('#'+totalPp).autoNumeric('init');
						}
					}
				}else if(obj.value=='C'){
					var dt = document.getElementById('ppmTipeC').value;
					if(dt!=1){
						alert('Tipe C Tidak Diijinkan');
						obj.value='';
						return false;
					}
					$("#tipeA").hide();
					$("#tipeB").hide();
					$("#tipeC").show();
					var size = document.getElementById('size').value;
					if(size>0){
						for(var i=0;i<size;i++){
							var item = 'itemPriceC'+i;
							$('#'+item).autoNumeric('init', {vMin: 0, mDec: 3});
							var total = 'priceTotalC'+i;
							$('#'+total).autoNumeric('init');
							var totalPp = 'priceTotalPpnC'+i;
							$('#'+totalPp).autoNumeric('init');
						}
					}
				}if(obj.value=='A'){
					var dt = document.getElementById('ppmTipeA').value;
					if(dt!=1){
						alert('Tipe A Tidak Diijinkan');
						obj.value='';
						return false;
					}
					$("#tipeA").show();
					$("#tipeB").hide();
					$("#tipeC").hide();
					var size = document.getElementById('size').value;
					if(size>0){
						for(var i=0;i<size;i++){
							var item = 'itemPrice'+i;
							$('#'+item).autoNumeric('init', {vMin: 0, mDec: 3});
							var total = 'priceTotal'+i;
							$('#'+total).autoNumeric('init');
							var totalPp = 'priceTotalPpn'+i;
							$('#'+totalPp).autoNumeric('init');
						}
					}
				}else if(obj.value=='B'){
					var dt = document.getElementById('ppmTipeB').value;
					if(dt!=1){
						alert('Tipe B Tidak Diijinkan');
						obj.value='';
						return false;
					}
					$("#tipeA").hide();
					$("#tipeB").show();
					$("#tipeC").hide();
					var size = document.getElementById('size').value;
					if(size>0){
						for(var i=0;i<size;i++){
							var item = 'itemPriceB'+i;
							$('#'+item).autoNumeric('init', {vMin: 0, mDec: 3});
							var total = 'priceTotalB'+i;
							$('#'+total).autoNumeric('init');
							var totalPp = 'priceTotalPpnB'+i;
							$('#'+totalPp).autoNumeric('init');
						}
					}
				}else if(obj.value=='C'){
					var dt = document.getElementById('ppmTipeC').value;
					if(dt!=1){
						alert('Tipe C Tidak Diijinkan');
						obj.value='';
						return false;
					}
					$("#tipeA").hide();
					$("#tipeB").hide();
					$("#tipeC").show();
					var size = document.getElementById('size').value;
					if(size>0){
						for(var i=0;i<size;i++){
							var item = 'itemPriceC'+i;
							$('#'+item).autoNumeric('init', {vMin: 0, mDec: 3});
							var total = 'priceTotalC'+i;
							$('#'+total).autoNumeric('init');
							var totalPp = 'priceTotalPpnC'+i;
							$('#'+totalPp).autoNumeric('init');
						}
					}
				}
			}
			
		}
		
		//regCancel Action
		function regCancel(){
			jQuery.facebox.close();
			window.location = "<s:url action="vnd/penawaranRfq" />"; 
		}
		
		//function onloadDaa
		function onloadData(){
			var tipe = $("#pqmType").val();
			if($('#method').val() != 6 || $('#statusTeknis').val()==1){
				if(tipe=='A'){
					$("#tipeA").show();
					$("#tipeB").hide();
					$("#tipeC").hide();
					var size = document.getElementById('size').value;
					if(size>0){
						for(var i=0;i<size;i++){
							var item = 'itemPrice'+i;
							$('#'+item).autoNumeric('init', {vMin: 0, mDec: 3});
							var total = 'priceTotal'+i;
							$('#'+total).autoNumeric('init');
							var totalPp = 'priceTotalPpn'+i;
							$('#'+totalPp).autoNumeric('init');
						}
					}
				}else if(tipe=='B'){
					$("#tipeA").hide();
					$("#tipeB").show();
					$("#tipeC").hide();
					var size = document.getElementById('size').value;
					if(size>0){
						for(var i=0;i<size;i++){
							var item = 'itemPriceB'+i;
							$('#'+item).autoNumeric('init', {vMin: 0, mDec: 3});
							var total = 'priceTotalB'+i;
							$('#'+total).autoNumeric('init');
							var totalPp = 'priceTotalPpnB'+i;
							$('#'+totalPp).autoNumeric('init');
						}
					}
				}else if(tipe=='C'){
					$("#tipeA").hide();
					$("#tipeB").hide();
					$("#tipeC").show();
					var size = document.getElementById('size').value;
					if(size>0){
						for(var i=0;i<size;i++){
							var item = 'itemPriceC'+i;
							$('#'+item).autoNumeric('init', {vMin: 0, mDec: 3});
							var total = 'priceTotalC'+i;
							$('#'+total).autoNumeric('init');
							var totalPp = 'priceTotalPpnC'+i;
							$('#'+totalPp).autoNumeric('init');
						}
					}
				}

			}
			
			if($('#method').val() == 6 && $('#statusTeknis').val()!=1){
			$('#totalPenawaranRow').hide();
			$('#bidBondRow').hide();
			//$('#attachment').hide();
			}
			
		}
	</script>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="vnd/dashboard">Home</s:a></li> 
	        
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
    
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
					
			<s:form action="vnd/penawaranRfq" id="procForm" name="procForm" enctype="multipart/form-data" method="post">
				<s:hidden name="prcMainHeader.ppmId" />
				<s:hidden name="prcMainVendor.id.vendorId"></s:hidden>
				<s:hidden name="prcMainVendor.id.ppmId"></s:hidden>
				<s:hidden name="prcMainHeader.ppmTipeA" id="ppmTipeA"></s:hidden>
				<s:hidden name="prcMainHeader.ppmTipeB" id="ppmTipeB"></s:hidden>
				<s:hidden name="prcMainHeader.ppmTipeC" id="ppmTipeC"></s:hidden>
				<s:hidden name="prcMainHeader.prcMethod.id" id="method"></s:hidden>
				<s:hidden name="prcMainVendor.pmpTechnicalStatus" id="statusTeknis"></s:hidden>
				<s:hidden name="quote.id.ppmId" value="%{prcMainVendor.id.ppmId}"></s:hidden>
				<s:hidden name="quote.id.vendorId" value="%{prcMainVendor.id.vendorId}"></s:hidden>
				
				<div class="box">
					<div class="title">
						Header
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle" >
							<tr>
								<td width="200px">Nomor Penawaran SPPH (*)</td>
								<td>
									<s:url action="ajax/proc/viewDetailSpph" id="ur"></s:url>
									<a href="javascript:void(0)" onclick="load_into_box('${ur}','prcMainHeader.ppmId=${prcMainHeader.ppmId }')">
										${prcMainHeader.ppmNomorRfq }
									</a>
								</td>
							</tr>
							
							<tr>
								<td width="200px">Nomor Penawaran (*)</td>
								<td>
									<s:textfield name="quote.pqmNumber" cssClass="required"></s:textfield>
								</td>
							</tr>
							
							<tr>
								<td width="200px">Tipe Penawaran (*)</td>
								<td>
									<s:select list="#{'A':'Tipe A', 'B':'Tipe B','C':'Tipe C'}" name="quote.pqmType" id="pqmType" headerKey="" headerValue="" cssClass="required" onchange="selectTipe(this)"></s:select>
									<br/>
									<pre>(* A: sesuai spek, B: Spek Beda jumlah sama, C:Spek Beda, Jumlah Beda)</pre>
								</td>
							</tr>
							<tr>
								<td width="200px">Kandungan Lokal (%)</td>
								<td>
									<s:textfield name="quote.pqmLocalContent" cssStyle="width:200px" cssClass="number"></s:textfield>
								</td>
							</tr>
							
							<tr>
								<td width="200px">Waktu Pengiriman / Pelaksanaan</td>
								<td>
									<s:textfield name="quote.pqmDeliveryTime" cssClass="number required" cssStyle="width:200px"></s:textfield>
									<s:select list="#{'Hari':'Hari Kalender', 'Bulan':'Bulan','Tahun':'Tahun'}" name="quote.pqmDeliveryUnit" cssClass="required"></s:select>
								</td>
							</tr>
							
							<tr>
								<td width="200px">Validitas Penawaran</td>
								<td>
									45 Hari
								</td>
							</tr>
							
							<tr>
								<td width="200px">Lokasi Penyerahan</td>
								<td>
									${prcMainHeader.lokasiPenyerahan }
								</td>
							</tr>
							
							<tr>
								<td width="200px">Mata Uang</td>
								<td>
									<s:select list="listCurrency" listKey="id" listValue="currencyCode" name="quote.admCurrency.id"></s:select>
								</td>
							</tr>
							<tr>
								<td>Catatan</td>
								<td>
									<s:textarea name="quote.pqmNotes" cssClass="required grow"></s:textarea>
								</td>
							</tr>
						</table>
						
					</div>
				</div>
				  
				<div class="box">
					<div class="title">
						Administrasi
						<span class="hide"></span>
					</div>
					<div class="content center">
						<display:table name="${listAdministrasi }" id="s" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
							<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
							<display:column title="Item"  class="center">${s.tempItem }</display:column>
							<display:column title="Ada / Tidak" style="text-align:center">
								<input type="hidden" name="listAdministrasi[${s_rowNum-1 }].id.ppmId" value="${s.id.ppmId }"/>
								<input type="hidden" name="listAdministrasi[${s_rowNum-1 }].id.vendorId" value="${s.id.vendorId }"/>
								<input type="hidden" name="listAdministrasi[${s_rowNum-1 }].id.tempDetId" value="${s.id.tempDetId }"/>
								<input type="hidden" name="listAdministrasi[${s_rowNum-1 }].tempItem" value="${s.tempItem }"/>
								<input type="hidden" name="listAdministrasi[${s_rowNum-1 }].tempTipe" value="${s.tempTipe }"/>
								<input type="hidden" name="listAdministrasi[${s_rowNum-1 }].tempWeight" value="${s.tempWeight }"/>
								<input type="hidden" name="listAdministrasi[${s_rowNum-1 }].tempValue" value="${s.tempValue }"/>
								<input type="hidden" name="listAdministrasi[${s_rowNum-1 }].tempVendorDesc" value="${s.tempVendorDesc }"/>
								<input type="hidden" name="listAdministrasi[${s_rowNum-1 }].createdBy" value="${s.createdBy }"/>
								<input type="hidden" name="listAdministrasi[${s_rowNum-1 }].prcTemplateDetail.id" value="${s.prcTemplateDetail.id }"/>
								<input type="checkbox" name="listAdministrasi[${s_rowNum-1 }].tempCheckVendor" value="${s.tempCheckVendor }" onclick="cekValue(this)" <c:if test="${s.tempCheckVendor==1 }">checked="checked"</c:if>/>
							</display:column>							
						</display:table>
					</div>
				</div>
				
				  
				<div class="box">
					<div class="title">
						Teknis
						<span class="hide"></span>
					</div>
					<div class="content center">			
						<display:table name="${listTeknis }" id="s" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
							<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
							<display:column title="Item"  class="center">${s.tempItem }</display:column>
							<display:column title="Respons Mitra Kerja">
								<input type="hidden" name="listTeknis[${s_rowNum-1 }].id.ppmId" value="${s.id.ppmId }"/>
								<input type="hidden" name="listTeknis[${s_rowNum-1 }].id.vendorId" value="${s.id.vendorId }"/>
								<input type="hidden" name="listTeknis[${s_rowNum-1 }].id.tempDetId" value="${s.id.tempDetId }"/>
								<input type="hidden" name="listTeknis[${s_rowNum-1 }].tempItem" value="${s.tempItem }"/>
								<input type="hidden" name="listTeknis[${s_rowNum-1 }].tempTipe" value="${s.tempTipe }"/>
								<input type="hidden" name="listTeknis[${s_rowNum-1 }].tempWeight" value="${s.tempWeight }"/>
								<input type="hidden" name="listTeknis[${s_rowNum-1 }].tempValue" value="${s.tempValue }"/>
								<textarea style="width: 95%" name="listTeknis[${s_rowNum-1 }].tempVendorDesc" value="${s.tempVendorDesc }" class="required">${s.tempVendorDesc }</textarea>
								<input type="hidden" name="listTeknis[${s_rowNum-1 }].createdBy" value="${s.createdBy }"/>
								<input type="hidden" name="listTeknis[${s_rowNum-1 }].tempCheckVendor" value="${s.tempCheckVendor }"/>
								<input type="hidden" name="listTeknis[${s_rowNum-1 }].prcTemplateDetail.id" value="${s.prcTemplateDetail.id }"/>
							</display:column>							
						</display:table>
					</div>
				</div>
				
				<div class="box">
					<div class="title">
						Item
						<input type="hidden" id="size" value="${listQuoteItem.size() }"/>
						<span class="hide"></span>
					</div>
					<div class="content center" id="tipeA" style="display:none">
						<c:set var="totalA" value="0"></c:set>
						<display:table name="${listQuoteItem }" id="s" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
							<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum} A</display:column>
							<display:column title="Kode"  class="center">${s.prcMainItem.itemCode }</display:column>
							<display:column title="Deskripsi" class="left">
								${s.prcMainItem.itemDescription }
								<input type="hidden" name="listQuoteItem[${s_rowNum-1 }].itemDescription" id="listQuoteItem[${s_rowNum-1 }].itemDescription" value="${s.itemDescription }"/>
							</display:column>
							<display:column title="Keterangan" class="left">
								${s.prcMainItem.keterangan }
							</display:column>
							<display:column title="Satuan" style="width: 40px;text-align:right">
								${s.prcMainItem.admUom.uomName }
							</display:column>
							<display:column title="Jumlah" style="text-align:right">
								${s.itemQuantity }
							</display:column>
							<display:column title="Jumlah Penawaran (non PPN)" style="text-align:right">
								<input style="width:95%;text-align: right" type="text" name="listQuoteItem[${s_rowNum-1 }].itemQuantity" id="listQuoteItem[${s_rowNum-1 }].itemQuantity" value="${s.itemQuantity }" class="number required" readonly="readonly" onkeyup="calculateData(this,${s_rowNum-1});"/>
							</display:column>
							
							<display:column title="Harga Penawaran (non PPN)" style="text-align:right">
								<input style="width:95%;text-align: right" type="text" name="listQuoteItem[${s_rowNum-1 }].itemPrice" id="itemPrice${s_rowNum-1 }" value="${s.itemPrice }" class="number required" onkeyup="calculateData(this,${s_rowNum-1});"/>
							</display:column>
							<display:column title="Pajak (%)" style="text-align:right">
								${s.itemPajak }
							</display:column>
							<display:column title="Harga Total Penawaran" style="text-align:right">
								<input style="width:95%;text-align: right" type="text" name="listQuoteItem[${s_rowNum-1 }].itemPriceTotal" id="priceTotal${s_rowNum-1 }" value="${s.itemPriceTotal }" readonly="readonly" onkeyup="calculateData(this,${s_rowNum-1});"/>
							</display:column>
							<display:column title="Harga Total Penawaran Ppn" style="text-align:right">
								<input style="width:95%;text-align: right" type="text" name="listQuoteItem[${s_rowNum-1 }].itemPriceTotalPpn" id="priceTotalPpn${s_rowNum-1 }" value="${s.itemPriceTotalPpn }" readonly="readonly" onkeyup="calculateData(this,${s_rowNum-1});"/>
							</display:column>
							<display:column>
								<input type="hidden" name="listQuoteItem[${s_rowNum-1 }].id.itemId" value="${s.id.itemId }"/>
								<input type="hidden" name="listQuoteItem[${s_rowNum-1 }].id.ppmId" value="${s.id.ppmId }"/>
								<input type="hidden" name="listQuoteItem[${s_rowNum-1 }].id.vendorId" value="${s.id.vendorId }"/>
								<input type="hidden" name="listQuoteItem[${s_rowNum-1 }].itemPajak" id="listQuoteItem[${s_rowNum-1 }].itemPajak" value="${s.itemPajak }"/>
								
								<input type="hidden" name="listQuoteItem[${s_rowNum-1 }].prcMainItem.id" value="${s.prcMainItem.id }"/>
							</display:column>	
							<display:footer>
								<tr>
									<td colspan="8"></td>
									<td ></td>
								</tr>
							</display:footer>						
						</display:table>
					</div>
					
					
					<div class="content center" id="tipeB" style="display:none">
						<display:table name="${listQuoteItem }" id="s" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
							<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum} B</display:column>
							<display:column title="Kode"  class="center">${s.prcMainItem.itemCode }</display:column>
							<display:column title="Deskripsi" class="left">
								${s.prcMainItem.itemDescription }
								<input type="hidden" name="listQuoteItemB[${s_rowNum-1 }].itemDescription" id="listQuoteItemB[${s_rowNum-1 }].itemDescription" value="${s.itemDescription }"/>
							</display:column>
							<display:column title="Keterangan" class="left">
								${s.prcMainItem.keterangan }
							</display:column>
							<display:column title="Satuan" style="width: 40px;text-align:right">
								${s.prcMainItem.admUom.uomName }
							</display:column>
							<display:column title="Jumlah" style="text-align:right">
								${s.itemQuantity }
							</display:column>
							<display:column title="Jumlah Penawaran (non PPN)" style="text-align:right">
								<input style="width:95%;text-align: right" type="text" name="listQuoteItemB[${s_rowNum-1 }].itemQuantity" id="listQuoteItemB[${s_rowNum-1 }].itemQuantity" value="${s.itemQuantity }" class="number required" onkeyup="calculateDataB(this,${s_rowNum-1});"/>
							</display:column>
							
							<display:column title="Harga Penawaran (non PPN)" style="text-align:right">
								<input style="width:95%;text-align: right" type="text" name="listQuoteItemB[${s_rowNum-1 }].itemPrice" id="itemPriceB${s_rowNum-1 }" value="${s.itemPrice }" class="number required" onkeyup="calculateDataB(this,${s_rowNum-1});"/>
							</display:column>
							<display:column title="Pajak (%)" style="text-align:right">
								${s.itemPajak }
							</display:column>
							<display:column title="Harga Total Penawaran" style="text-align:right">
								<input style="width:95%;text-align: right" type="text" name="listQuoteItemB[${s_rowNum-1 }].itemPriceTotal" id="priceTotalB${s_rowNum-1 }" value="${s.itemPriceTotal }" readonly="readonly" onkeyup="calculateDataB(this,${s_rowNum-1});"/>
							</display:column>
							<display:column title="Harga Total Penawaran Ppn" style="text-align:right">
								<input style="width:95%;text-align: right" type="text" name="listQuoteItemB[${s_rowNum-1 }].itemPriceTotalPpn" id="priceTotalPpnB${s_rowNum-1 }" value="${s.itemPriceTotalPpn }" readonly="readonly" onkeyup="calculateDataB(this,${s_rowNum-1});"/>
							</display:column>
							<display:column>
								<input type="hidden" name="listQuoteItemB[${s_rowNum-1 }].id.itemId" value="${s.id.itemId }"/>
								<input type="hidden" name="listQuoteItemB[${s_rowNum-1 }].id.ppmId" value="${s.id.ppmId }"/>
								<input type="hidden" name="listQuoteItemB[${s_rowNum-1 }].id.vendorId" value="${s.id.vendorId }"/>
								<input type="hidden" name="listQuoteItemB[${s_rowNum-1 }].itemPajak" id="listQuoteItemB[${s_rowNum-1 }].itemPajak" value="${s.itemPajak }"/>
								
								<input type="hidden" name="listQuoteItemB[${s_rowNum-1 }].prcMainItem.id" value="${s.prcMainItem.id }"/>
							</display:column>	
							<display:footer>
								<tr>
									<td colspan="8"></td>
									<td ></td>
								</tr>
							</display:footer>						
						</display:table>
					</div>
					
					
					<div class="content center" id="tipeC" style="display:none">
						<display:table name="${listQuoteItem }" id="s" pagesize="1000" excludedParams="*" style="width: 100%;" class="style1">
							<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum} C</display:column>
							<display:column title="Kode"  class="center">${s.prcMainItem.itemCode }</display:column>
							<display:column title="Deskripsi" class="left">
								<textarea name="listQuoteItemC[${s_rowNum-1 }].itemDescription" id="listQuoteItemC[${s_rowNum-1 }].itemDescription" value="${s.itemDescription }" class="grow" style="width:95%"></textarea>
							</display:column>
							<display:column title="Keterangan" class="left">
								${s.prcMainItem.keterangan }
							</display:column>
							<display:column title="Satuan" style="width: 40px;text-align:right">
								${s.prcMainItem.admUom.uomName }
							</display:column>
							<display:column title="Jumlah" style="text-align:right">
								${s.itemQuantity }
							</display:column>
							<display:column title="Jumlah Penawaran (non PPN)" style="text-align:right">
								<input style="width:95%;text-align: right" type="text" name="listQuoteItemC[${s_rowNum-1 }].itemQuantity" id="listQuoteItemC[${s_rowNum-1 }].itemQuantity" value="${s.itemQuantity }" class="number required" readonly="readonly" onkeyup="calculateDataC(this,${s_rowNum-1});"/>
							</display:column>
							<display:column title="Harga Penawaran (non PPN)" style="text-align:right">
								<input style="width:95%;text-align: right" type="text" name="listQuoteItemC[${s_rowNum-1 }].itemPrice" id="itemPriceC${s_rowNum-1 }" value="${s.itemPrice }" class="number required" onkeyup="calculateDataC(this,${s_rowNum-1});"/>
							</display:column>
							<display:column title="Pajak (%)" style="text-align:right">
								${s.itemPajak }
							</display:column>
							<display:column title="Harga Total Penawaran" style="text-align:right">
								<input style="width:95%;text-align: right" type="text" name="listQuoteItemC[${s_rowNum-1 }].itemPriceTotal" id="priceTotalC${s_rowNum-1 }" value="${s.itemPriceTotal }" readonly="readonly" onkeyup="calculateDataC(this,${s_rowNum-1});"/>
							</display:column>
							<display:column title="Harga Total Penawaran Ppn" style="text-align:right">
								<input style="width:95%;text-align: right" type="text" name="listQuoteItemC[${s_rowNum-1 }].itemPriceTotalPpn" id="priceTotalPpnC${s_rowNum-1 }" value="${s.itemPriceTotalPpn }" readonly="readonly" onkeyup="calculateDataC(this,${s_rowNum-1});"/>
							</display:column>
							<display:column>
								<input type="hidden" name="listQuoteItemC[${s_rowNum-1 }].id.itemId" value="${s.id.itemId }"/>
								<input type="hidden" name="listQuoteItemC[${s_rowNum-1 }].id.ppmId" value="${s.id.ppmId }"/>
								<input type="hidden" name="listQuoteItemC[${s_rowNum-1 }].id.vendorId" value="${s.id.vendorId }"/>
								<input type="hidden" name="listQuoteItemC[${s_rowNum-1 }].itemPajak" id="listQuoteItemC[${s_rowNum-1 }].itemPajak" value="${s.itemPajak }"/>
								
								<input type="hidden" name="listQuoteItemC[${s_rowNum-1 }].prcMainItem.id" value="${s.prcMainItem.id }"/>
							</display:column>	
							
							<display:footer>
								<tr>
									<td colspan="8"></td>
									<td ></td>
								</tr>
							</display:footer>						
						</display:table>
					</div>
					<div class="content">
						<table class="form formStyle" >
							<tr id="totalPenawaranRow">
								<td width="200px">Total Penawaran</td>
								<td>
									<s:textfield name="quote.pqmTotalPenawaran" id="quote.pqmTotalPenawaran" cssClass="number required" readonly="true"></s:textfield>
								</td>
							</tr>
							
							<tr id="bidBondRow">
								<td width="200px">Bid Bond (minimal)</td>
								<td>
									<s:textfield name="quote.pqmBidBond"  cssClass="number required" id="bidBond"></s:textfield>
								</td>
							</tr>
							<tr id="attachment">
								<td width="200px">Attachment</td>
								<td>
									<c:choose>
										<c:when test="${not empty quote.pqmAttachment }">
											<a target="blank" href="${contextPath }/upload/file/${quote.pqmAttachment }">${quote.pqmAttachment }</a>
										</c:when>
										<c:otherwise>
											<s:file name="docsPenawaran" cssClass="required"></s:file>
										</c:otherwise>
									</c:choose>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<p align="justify">
										Kami berminat mengikuti pelaksanaan pengadaan Barang/Jasa di PT. Semen Baturaja (Persero), <br/>berupa :<b> ${prcMainHeader.ppmSubject}</b> <br/>
										Dengan ini menyatakan dengan sebenarnya bahwa : <br/>
										<ol>
											<li>Dalam mengikuti pengadaan Barang/Jasa diatas, kami tidak akan melakukan praktek KKN (Korupsi, Kolusi, Nepotisme).</li>
											<li>Kami akan mengikuti proses pengadaan ini secara bersih dan profesional.</li>
											<li>Kami memahami ketentuan/peraturan mengenai pengadaan barang dan jasa yang berlaku di PT. Semen Baturaja (Persero) .</li>
											<li>Kami akan mengikuti dan memahami ketentuan/peraturan mengenai pengadaan Barang dan Jasa di PT. Semen Baturaja (Persero) .</li>
											<li>Apabila kami melanggar hal-hal yang telah kami nyatakan dalam Pakta Integritas ini, kami bersedia dikenakan sanksi sesuai dengan ketentuan peraturan perundang-undangan yang berlaku.</li>
										</ol>
										Demikian, pernyataan ini Kami sampaikan dengan sebenar-benarnya, tanpa menyembunyikan fakta dan hal material apapun, dan dengan demikian kami akan bertanggung jawab sepenuhnya atas kebenaran dari hal-hal yang kami nyatakan disini.
										Demikian pernyataan ini kami buat untuk digunakan sebagaimana mestinya.
									</p>
								</td>
							</tr>
						</table>
					</div>					
					</div>
					
				<div class="uibutton-toolbar extend">
					<s:submit name="saveAndFinish" value="Simpan dan Selesai" cssClass="uibutton"></s:submit>
					<a href="javascript:void(0)" class="uibutton" onclick="confirmPopUp('regCancel();', 'Batal', 'Apakah anda yakin untuk membatalkan transaksi ?', 'Ya', 'Tidak');"><s:text name="cancel"></s:text> </a>					
				</div>
						
			</s:form>
			
		</div>
	</div>
</body>
</html>