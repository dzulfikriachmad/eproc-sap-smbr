<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title><s:text name="vnd.reg.title"></s:text> </title>
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	
	<script type="text/javascript">
		$(document).ready(function(){
			// FORM VALIDATION
			$("#resetpassword").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler:function(form){
					loadingScreen();
					form.submit();
				}
			});
		});
	</script>
</head>
<body>
	<!-- BreadCrumbs -->
    <div class="breadCrumb module">
       	<ul>
           	<li><s:a action="vnd/login">Login</s:a></li> 
    	</ul>
    </div>
    <!-- End of BreadCrumbs -->
    
	<div id="right">
		<div class="section">
		
				<div class="box">
					<div class="title center">
						RESET PASSWORD
						<span class="hide"></span>
					</div>
					<div class="content">
						<!-- Alert -->
						<s:if test="hasActionErrors()">
							<div class="message red"><s:actionerror/></div>
						</s:if>
						<s:if test="hasActionMessages()">
						    <div class="message green"><s:actionmessage/></div>
						</s:if>
						
						<s:form action="vnd/resetPassword" id="resetpassword">			
							<table class="form formStyle">  
								<tr>
									<td width="200px"><s:label key="vnd.reg.email.address" /></td>
									<td><s:textfield name="vndHeader.vendorEmail" placeholder="procurement@safeer.com" cssClass="email required" /></td>
								</tr>
								<tr>
									<td><s:label key="login.username"></s:label> (*)</td>
									<td><s:textfield name="vndHeader.vendorLogin" placeholder="current username" cssClass="required" minlength="6" /></td>
								</tr>
							</table>
						
					
							<div class="uibutton-toolbar">
								<s:submit key="save" name="reset" cssClass="uibutton" />
								<s:a action="vnd/login" cssClass="uibutton"><s:text name="cancel"></s:text> </s:a>
							</div>
						</s:form>
					</div>
				</div>
				
		</div>
	</div>
</body>
</html>