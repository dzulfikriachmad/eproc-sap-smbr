			<%@ include file="/include/definitions.jsp" %>
			
			<center>
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>		
						
			<display:table name="${listTmpVndFinRpt }" id="s" pagesize="10" requestURI="financeInformation.jwebs" excludedParams="*" style="width: 100%;" class="style1">
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column titleKey="fin.inf.financial.year" total="true" style="width: 80px;" class="center">${s.finYear }</display:column>
				<display:column titleKey="fin.inf.financial.tipe" style="width: 100px; " class="center">
					<c:if test="${s.finType == 1}">
						AUDIT (AUDITED)
					</c:if>
					<c:if test="${s.finType == 2}">
						NON AUDIT (NON AUDITED)
					</c:if>
				</display:column>
				<display:column titleKey="fin.inf.financial.matauang" style="width: 100px;" class="center">${s.admCurrency.currencyName }</display:column>
				<display:column titleKey="fin.inf.financial.nilaiasset" style="width: 100px;" class="center"><fmt:formatNumber> ${s.finAssetValue }</fmt:formatNumber></display:column>
				<display:column titleKey="fin.inf.financial.hutang" class="center" style="width: 100px;"><fmt:formatNumber>${s.finHutang }</fmt:formatNumber></display:column>
				<display:column titleKey="fin.inf.financial.pendapatankotor" class="center" style="width: 150px;"><fmt:formatNumber> ${s.finRevenue }</fmt:formatNumber></display:column>
				<display:column titleKey="fin.inf.financial.lababersih" class="center" style="width: 100px;"><fmt:formatNumber> ${s.finNetIncome }</fmt:formatNumber></display:column>
				<display:column title="Aksi" media="html" style="width: 60px;" class="center">
					<div class="uibutton-group">
						<s:url var="set" action="ajax/setTmpVndFinRpt" />
						<a title="Ubah data laporan keuangan perusahaan" class="uibutton icon edit" href='javascript:void(0)' onclick='load_into_box("${set}", "act=edit&id=${s.id}");'>&nbsp;</a>
						<a title="Hapus data laporan keuangan perusahaan" class="uibutton" href='javascript:void(0)' onclick='confirmPopUp("deleteObj(\"${set}\", \"act=delete&id=${s.id}\", \"#tmpVndFinRpt\")", "Hapus Data Laporan Keuangan Perusahaan", "Yakin ingin menghapus?", "Hapus", "Batal");'>X</a>
					</div>
				</display:column>
			</display:table>
			</center>
			<script>
				$(document).ready(function(){	
					// SYSTEM MESSAGES
					$(".message").click(function () {
				      $(this).fadeOut();
				    });
				});
			</script>