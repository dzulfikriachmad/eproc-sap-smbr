<%@ include file="/include/definitions.jsp" %>

	<script type="text/javascript">
		$(document).ready(function(){
			// FORM VALIDATION
			$("#formTmpVndBank").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler: function() {
					sendForm(document.formTmpVndBank,"<s:url action="ajax/submitTmpVndBank" />","#tmpVndBank");jQuery.facebox.close();
				}
			});
		});
	</script>

<div class='fbox_header'><s:text name="fin.inf.bank.add"></s:text> </div>
    <div class='fbox_container'>
    <s:form name="formTmpVndBank" id="formTmpVndBank">
        <div class='fbox_content'>
        	<div id="cek"></div>
            
            	<s:hidden name="tmpVndBank.id"></s:hidden>
            	<s:hidden name="tmpVndBank.tmpVndHeader.vendorId"></s:hidden>
            	<table style="min-width: 600px; margin: 0px 10px;" class="form formStyle">
            		<tr>
            			<td width="150px"><s:label key="fin.inf.bank.rekeningnomor"/> (*) </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndBank.bankAccountNo" cssClass="required" /></td>
            		</tr>    
            		<tr>
            			<td><s:label key="fin.inf.bank.pemegangrekening"/> (*) </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndBank.bankAccountName" cssClass="required" /></td>
            		</tr>            		
            		<tr>
            			<td><s:label key="fin.inf.bank.namabank"/> (*) </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndBank.bankName" cssClass="required" /> </td>
            		</tr>
            		<tr>
            			<td><s:label key="fin.inf.bank.cabang"/> (*) </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndBank.bankBranch" cssClass="required" /> </td>
            		</tr> 
            		<tr>
            			<td><s:label key="fin.inf.bank.alamat"/> (*) </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndBank.bankAddress" cssClass="required" /> </td>
            		</tr>  
            	</table>
            
        </div>
        <div class='fbox_footer'>
            <s:submit key="save" cssClass="uibutton confirm" ></s:submit>
            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()'><s:text name="cancel"/> </a>
        </div>
        </s:form>
    </div>