<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title><s:text name="fin.inf.title"></s:text> </title>
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	<script type="text/javascript">
		$(document).ready(function(){	
			
		});
		
		//Disable Enter key on form
		function stopRKey(evt) {
		  var evt = (evt) ? evt : ((event) ? event : null);
		  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
		  if ((evt.keyCode == 13) && (node.type=="text"))  {return false;}
		}
		
		document.onkeypress = stopRKey;
		
		$(document).ready(function(){
			// FORM VALIDATION
			$(".chzn").chosen();
			$("#finInf").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler:function(form){
					loadingScreen();
					$("#baseCap").val($("#baseCap").autoNumeric('get'));
					$("#capIn").val($("#capIn").autoNumeric('get'));
					form.submit();
				}
			});
			
			//format number
			$("#baseCap").autoNumeric('init');
			$("#capIn").autoNumeric('init');
		});
	</script>
</head>
<body>
	<!-- BreadCrumbs -->
    <%@ include file="/WEB-INF/pages/vnd/reg/regBreadcrumbs.jsp" %>
    <!-- End of BreadCrumbs -->
    
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
			
			<s:form id="finInf">
	
				<!-- REKENING BANK -->
				<div class="box">
					<div class="title center">
						<s:label key="fin.inf.bank.account.label"/>
						<span class="hide"></span>
					</div>
					<div class="content center">
						<a href='javascript:void(0)' class='uibutton icon add' onclick='load_into_box("<s:url action="ajax/setTmpVndBank" />")'><s:label key="fin.inf.bank.account.add"/> </a>						
						<div id="tmpVndBank" style="margin-top: 20px;">
							<s:include value="listBank.jsp" />
						</div>
					</div>
				</div>
				
				<!-- MODAL SESUAI DENGAN AKTA TERAKHIR -->
				<div class="box">
					<div class="title center">
						<s:label key="fin.inf.capital.label"/>
						<span class="hide"></span>
					</div>
					<div class="content center">
						<table class="form formStyle">
							<tr>
								<td width="170px"><s:label key="fin.inf.capital.dasar"/> (*)</td>
								<td width="60px"><s:select list="listAdmCurr" listKey="id" listValue="currencyCode" name="tmpVndHeader.admCurrencyByVendorCurrBaseCap.id" cssStyle="width: 150px" cssClass="required chzn"></s:select></td>
								<td><s:textfield name="tmpVndHeader.vendorBaseCap" cssClass="required number" id="baseCap"></s:textfield></td>
							</tr>
							<tr>
								<td><s:label key="fin.inf.capital.disetor"/> (*)</td>
								<td><s:select list="listAdmCurr" listKey="id" listValue="currencyCode" name="tmpVndHeader.admCurrencyByVendorCurrInCap.id" cssStyle="width: 150px" cssClass="required chzn"></s:select></td>
								<td><s:textfield name="tmpVndHeader.vendorInCap" cssClass="required number"  id="capIn"></s:textfield></td>
							</tr>
						</table>
					</div>
				</div>
				
				<!-- LAP. KEUANGAN -->
				<div class="box">
					<div class="title center">
						<s:label key="find.inf.financial.label"/>
						<span class="hide"></span>
					</div>
					<div class="content center">
						<a href='javascript:void(0)' class='uibutton icon add' onclick='load_into_box("<s:url action="ajax/setTmpVndFinRpt" />")'><s:label key="find.inf.financial.add"/> </a>						
						<div id="tmpVndFinRpt" style="margin-top: 20px;">
							<s:include value="listFinRpt.jsp" />
						</div>
					</div>
				</div>
				
				<!-- KLASIFIKASI PERUSAHAAN -->
				<div class="box">
					<div class="title center">
						<s:label key="fin.inf.klasifikasi.label"/>
						<span class="hide"></span>
					</div>
					<div class="content center">
						<table style="width: 700px;">
							<tr>
								<td width="170px"><s:label key="fin.inf.klasifikasi.label"/> 1 (*) </td>
								<td><s:select list="#{'1':'KECIL', '2':'NON - KECIL'}" name="tmpVndHeader.vendorFinClass" cssClass="required chzn" cssStyle="width:200px"></s:select></td>
							</tr>
							<tr>
								<td width="170px"><s:label key="fin.inf.klasifikasi.label"/> 2 (Optional)</td>
								<td><s:select list="#{'1':'KECIL', '2':'NON - KECI'}" name="tmpVndHeader.vendorFinClass1" cssClass="required chzn" cssStyle="width:200px" headerKey="" headerValue="------"></s:select></td>
							</tr>
							<tr>
								<td width="170px"><s:label key="fin.inf.klasifikasi.label"/> 3 (Optional)</td>
								<td><s:select list="#{'1':'KECIL', '2':'NON - KECIL'}" name="tmpVndHeader.vendorFinClass2" cssClass="required chzn" cssStyle="width:200px" headerKey="" headerValue="-------"></s:select></td>
							</tr>
						</table>
					</div>
				</div>
																
				<div class="uibutton-toolbar extend">
					<a href="javascript:void(0)" class="uibutton" onclick="confirmPopUp('regCancel();', 'Batal', 'Apakahanda yakin untuk membatalkan transaksi ?', 'Ya', 'Tidak');"><s:text name="cancel"></s:text> </a>
					<s:submit name="save" key="save.temp" cssClass="uibutton"></s:submit>
					<s:submit name="saveAndNext" key="save.next" cssClass="uibutton"></s:submit>	
				</div>
									
			</s:form>	
		</div>
	</div>
</body>
</html>