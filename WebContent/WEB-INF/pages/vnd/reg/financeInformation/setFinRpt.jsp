<%@ include file="/include/definitions.jsp" %>

	<script type="text/javascript">
		$(document).ready(function(){
			// FORM VALIDATION
			$(".chzn").chosen();
			$("#formTmpVndFinRpt").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler: function() {
					$("#finAsset").val($("#finAsset").autoNumeric('get'));
					$("#finHutang").val($("#finHutang").autoNumeric('get'));
					$("#finRevenue").val($("#finRevenue").autoNumeric('get'));
					$("#finNetIncome").val($("#finNetIncome").autoNumeric('get'));
					
					sendForm(document.formTmpVndFinRpt,"<s:url action="ajax/submitTmpVndFinRpt" />","#tmpVndFinRpt");
					jQuery.facebox.close();
				}
			});
			
			$("#finAsset").autoNumeric('init');
			$("#finHutang").autoNumeric('init');
			$("#finRevenue").autoNumeric('init');
			$("#finNetIncome").autoNumeric('init');
		});
	</script>
	
<div class='fbox_header'><s:text name="find.inf.financial.add"></s:text> </div>
    <div class='fbox_container'>
    	<s:form name="formTmpVndFinRpt" id="formTmpVndFinRpt">
        <div class='fbox_content'>
        	<div id="cek"></div>
            
            	<s:hidden name="tmpVndFinRpt.id"></s:hidden>
            	<s:hidden name="tmpVndFinRpt.tmpVndHeader.vendorId"></s:hidden>
            	<table style="min-width: 600px; margin: 0px 10px;" class="form formStyle">
            		<tr>
            			<td width="150px"><s:label key="fin.inf.financial.year"/> (*) </td>
            			<td colspan="3"><s:textfield name="tmpVndFinRpt.finYear" cssClass="required number" /></td>
            		</tr>    
            		<tr>
            			<td><s:label key="fin.inf.financial.tipe"/> (*) </td>
            			<td colspan="3"><s:select list="#{'1':'AUDIT (AUDITED)', '2':'NON AUDIT (NON-AUDITED)'}" cssClass="chzn" name="tmpVndFinRpt.finType"></s:select></td>
            		</tr>            		
            		<tr>
            			<td><s:label key="fin.inf.financial.matauang"/> (*) </td>
            			<td colspan="3"><s:select list="listAdmCurr" listKey="id" listValue="currencyCode" name="tmpVndFinRpt.admCurrency.id" cssClass="chzn" cssStyle="width:200px"></s:select> </td>
            		</tr>
            		<tr>
            			<td><s:label key="fin.inf.financial.nilaiasset"/> (*) </td>
            			<td colspan="3"><s:textfield name="tmpVndFinRpt.finAssetValue" cssClass="required number" id="finAsset"/> </td>
            		</tr>  
            		<tr>
            			<td><s:label key="fin.inf.financial.hutang"/>(*) </td>
            			<td colspan="3"><s:textfield name="tmpVndFinRpt.finHutang" cssClass="required number" id="finHutang"/> </td>
            		</tr> 
            		<tr>
            			<td><s:label key="fin.inf.financial.pendapatankotor"/> (*) </td>
            			<td colspan="3"><s:textfield name="tmpVndFinRpt.finRevenue" cssClass="required number" id="finRevenue"/></td>
            		</tr>            		
            		<tr>
            			<td><s:label key="fin.inf.financial.lababersih"/>(*) </td>
            			<td colspan="3"><s:textfield name="tmpVndFinRpt.finNetIncome" cssClass="required number" id="finNetIncome"/> </td>
            		</tr>
            	</table>
            
        </div>
        <div class='fbox_footer'>
            <s:submit key="save" cssClass="uibutton confirm" ></s:submit>
            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()'><s:text name="cancel"></s:text> </a>
        </div>
        </s:form>
    </div>