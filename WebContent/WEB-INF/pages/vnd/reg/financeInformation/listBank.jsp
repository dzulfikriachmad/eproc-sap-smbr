			<%@ include file="/include/definitions.jsp" %>
			
			<center>
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>		
						
			<display:table name="${listTmpVndBank }" id="s" pagesize="10" requestURI="financeInformation.jwebs" excludedParams="*" style="width: 100%;" class="style1">
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column titleKey="fin.inf.bank.rekeningnomor" total="true" style="width: 200px;">${s.bankAccountNo }</display:column>
				<display:column titleKey="fin.inf.bank.pemegangrekening" style="width: 150px; " class="center">${s.bankAccountName }</display:column>
				<display:column titleKey="fin.inf.bank.namabank" style="width: 150px;" class="center">${s.bankName }</display:column>
				<display:column titleKey="fin.inf.bank.cabang" class="center" style="width: 200px;">${s.bankBranch }</display:column>
				<display:column titleKey="fin.inf.bank.alamat" style="width: 100px;" class="center">${s.bankAddress }</display:column>
				<display:column title="" media="html" style="width: 60px;" class="center">
					<div class="uibutton-group">
						<s:url var="set" action="ajax/setTmpVndBank" />
						<a title="Ubah data rekening bank perusahaan" class="uibutton icon edit" href='javascript:void(0)' onclick='load_into_box("${set}", "act=edit&id=${s.id}");'>&nbsp;</a>
						<a title="Hapus data rekening bank perusahaan" class="uibutton" href='javascript:void(0)' onclick='confirmPopUp("deleteObj(\"${set}\", \"act=delete&id=${s.id}\", \"#tmpVndBank\")", "Hapus Data Rekening Bank Perusahaan", "Yakin ingin menghapus?", "Hapus", "Batal");'>X</a>
					</div>
				</display:column>
			</display:table>
			</center>
			<script>
				$(document).ready(function(){	
					// SYSTEM MESSAGES
					$(".message").click(function () {
				      $(this).fadeOut();
				    });
				});
			</script>