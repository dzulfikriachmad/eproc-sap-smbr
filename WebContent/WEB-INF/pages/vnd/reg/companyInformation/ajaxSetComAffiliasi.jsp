<%@ include file="/include/definitions.jsp" %>
<script type="text/javascript">
$(document).ready(function(){
	// FORM VALIDATION
	$(".chzn").chosen();
	$("#formAjax").validate({
		meta: "validate",
		errorPlacement: function(error, element) {
          error.insertAfter(element);
		}
	});
	
	$("#formAjax").ajaxForm({
        beforeSubmit: function() {
            loadingScreen();
        },
        success: function(data) {
            loadPage("<s:url action="ajax/listTmpVndTambahan"/>","#aff");
            jQuery.facebox.close();
        }
    });
});
</script>
<div class='fbox_header'><s:label key="com.inf.aff.label"></s:label> </div>
    <div class='fbox_container'>
    	<s:form name="formAjax" id="formAjax" action="ajax/submitTmpVndTambahan">
        <div class='fbox_content'>
        	<div id="cek"></div>
            	<s:hidden name="tmpVndTambahan.id"></s:hidden>
            	<s:hidden name="tmpVndTambahan.tmpVndHeader.vendorId"></s:hidden>
            	<table style="min-width: 600px;" class="form formStyle">
            		<tr>
            			<td><s:label key="com.inf.aff.tipe"></s:label> (*) </td>
            			<td colspan="3">
            				<s:select list="listTipeAfiliasi" listKey="key" listValue="value" name="tmpVndTambahan.tipe" cssClass="required chzn" cssStyle="width:300px"></s:select>
            			</td>
            		</tr>        
            		<tr>
            			<td><s:label key="com.inf.aff.nama"></s:label> (*) </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndTambahan.nama" cssClass="required"/> </td>
            		</tr>            		
            		<tr valign="middle">
            			<td><s:label key="com.inf.aff.alamat"></s:label> (*) </td>
            			<td colspan="3"><s:textarea onkeyup="upper(this);" maxlength="200" name="tmpVndTambahan.alamat" rows="3" cols="23" cssClass="required"></s:textarea> </td>
            		</tr>   
            		<tr>
            			<td><s:label key="com.inf.aff.kodepos"></s:label> </td>
            			<td colspan="3"><s:textfield name="tmpVndTambahan.kodePos" cssClass="required"/> </td>
            		</tr> 
            		<tr>
            			<td width="150px"><s:label key="com.inf.aff.negara"/> </td>
            			<td><s:select list="listAdmCountry" listKey="id" listValue="countryName" name="tmpVndTambahan.negara.id" id="negara" cssClass="required chzn" cssStyle="width:300px"></s:select> </td>
            		</tr>
            		<tr>
            			<td><s:label key="com.inf.aff.kualifikasi"></s:label> </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndTambahan.kualifikasi" cssClass="required"/> </td>
            		</tr>  
            		<tr>
            			<td><s:label key="com.inf.aff.hubungan"></s:label> </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndTambahan.hubunganKerja" cssClass="required"/> </td>
            		</tr>
            	</table>
        </div>
        <div class='fbox_footer'>
            <s:submit key="save" cssClass="uibutton confirm"></s:submit>
            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()'><s:label key="cancel"></s:label> </a>
        </div>
        </s:form>
    </div>