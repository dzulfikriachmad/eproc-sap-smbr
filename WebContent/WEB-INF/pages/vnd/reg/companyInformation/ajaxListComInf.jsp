			<%@ include file="/include/definitions.jsp" %>
			
			<center>
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>		
			<input type="hidden" name="ukuran" id="ukuran" value="${listTmpVndAddress.size() }"/>			
			<display:table name="${listTmpVndAddress }" id="s" pagesize="10" requestURI="companyInformation.jwebs" excludedParams="*" style="width: 100%;" class="style1">
				<display:column titleKey="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column titleKey="com.inf.add.tipe" total="true" style="width: 100px;">${s.admAddType.addTypeName }</display:column>
				<display:column titleKey="com.inf.add.negara" style="width: 100px;">${s.admCountry.countryName }</display:column>
				<display:column titleKey="com.inf.add.provinsi" style="width: 100px;">${s.addProvince }</display:column>
				<display:column titleKey="com.inf.add.kota">${s.addCity }</display:column>
				<display:column titleKey="com.inf.add.alamat" style="width: 200px;">${s.addAddress  }, ${s.addPostCode }</display:column>
				<display:column titleKey="com.inf.add.telp" style="width: 130px;">${s.addPhone1 }</display:column>
				<display:column title="" media="html" style="width: 60px;">
					<div class="uibutton-group">
						<s:url var="set" action="ajax/setTmpVndAdd" /> 
						<a title="Ubah data kontak perusahaan" class="uibutton icon edit" href='javascript:void(0)' onclick='load_into_box("${set}", "act=edit&id=${s.id}");'>&nbsp;</a>
						<a title="Hapus data kontak perusahaan" class="uibutton" href='javascript:void(0)' onclick='confirmPopUp("deleteObj(\"${set}\", \"act=delete&id=${s.id}\", \"#hasil\")", "Hapus Kontak Perusahaan", "Yakin ingin menghapus?", "Hapus", "Batal");'>X</a>
					</div>
				</display:column>
			</display:table>
			</center>
			<script>
				$(document).ready(function(){	
					// SYSTEM MESSAGES
					$(".message").click(function () {
				      $(this).fadeOut();
				    });
				});
			</script>