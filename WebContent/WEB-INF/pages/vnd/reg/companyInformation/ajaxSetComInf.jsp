<%@ include file="/include/definitions.jsp" %>
<script type="text/javascript">
$(document).ready(function(){
	// FORM VALIDATION
	$(".chzn").chosen();
	$("#formAjax").validate({
		meta: "validate",
		errorPlacement: function(error, element) {
          error.insertAfter(element);
		}
	});
	
	$("#formAjax").ajaxForm({
        beforeSubmit: function() {
            loadingScreen();
        },
        success: function(data) {
            loadPage("<s:url action="ajax/listTmpVndAdd"/>","#hasil");
            jQuery.facebox.close();
        }
    });
});
</script>
<div class='fbox_header'><s:label key="com.inf.add.label"></s:label> </div>
    <div class='fbox_container'>
    	<s:form name="formAjax" id="formAjax" action="ajax/submitTmpVndAdd">
        <div class='fbox_content'>
        	<div id="cek"></div>
            	<s:hidden name="tmpVndAdd.id"></s:hidden>
            	<s:hidden name="tmpVndAdd.tmpVndHeader.vendorId"></s:hidden>
            	<table style="min-width: 600px;" class="form formStyle">
            		<tr>
            			<td><s:label key="com.inf.add.fax"></s:label> </td>
            			<td colspan="3"><s:textfield name="tmpVndAdd.addFax"  cssClass="required"/> </td>
            		</tr>            		
            		<tr>
            			<td width="150px"><s:label key="com.inf.add.telp"/> 1 (*) </td>
            			<td><s:textfield name="tmpVndAdd.addPhone1" cssClass="required"/> </td>
            			<td style="width: 100px; text-align: right;"><s:label key="com.inf.add.telp"/> 2&nbsp;</td>
            			<td><s:textfield name="tmpVndAdd.addPhone2" /> </td>
            		</tr>
            		<tr>
            			<td width="150px"><s:label key="com.inf.add.negara" /> (*) </td>
            			<td><s:select list="listAdmCountry" listKey="id" listValue="countryName"  name="tmpVndAdd.admCountry.id" id="negara" cssClass="required chzn" cssStyle="width:200px"></s:select> </td>
            			<td style="width: 100px; text-align: right;"><s:label key="com.inf.add.tipe" /> (*)&nbsp;</td>
            			<td><s:select list="listAddType" listKey="id" listValue="addTypeName"  name="tmpVndAdd.admAddType.id" id="jenis" cssClass="required chzn" cssStyle="width:200px"></s:select> </td>
            		</tr>
            		<tr>
            			<td><s:label key="com.inf.add.provinsi"></s:label>(*) </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndAdd.addProvince" cssClass="required"/> </td>
            		</tr>
            		<tr>
            			<td><s:label key="com.inf.add.kota"></s:label> (*) </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndAdd.addCity" cssClass="required"/> </td>
            		</tr>  
            		<tr valign="middle">
            			<td><s:label key="com.inf.add.alamat"></s:label> (*) </td>
            			<td colspan="3"><s:textarea onkeyup="upper(this);" maxlength="60" name="tmpVndAdd.addAddress" rows="3" cols="23" cssClass="required"></s:textarea> </td>
            		</tr>   
            		<tr>
            			<td><s:label key="com.inf.add.kodepos"></s:label> </td>
            			<td colspan="3"><s:textfield name="tmpVndAdd.addPostCode" cssClass="required number" maxlength="6"/> </td>
            		</tr>   
            		<tr>
            			<td><s:label key="com.inf.add.web"></s:label> </td>
            			<td colspan="3"><s:textfield name="tmpVndAdd.addWebsite" /> </td>
            		</tr>         		
            	</table>
            
        </div>
        <div class='fbox_footer'>
            <s:submit key="save" id="submitForm" cssClass="uibutton confirm"></s:submit>
            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()' ><s:label key="cancel"></s:label> </a>
        </div>
        </s:form>
    </div>