<%@ include file="/include/definitions.jsp" %>
<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title><s:text name="com.inf.title"></s:text> </title>
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	<script type="text/javascript">
		//Disable Enter key on form
		function stopRKey(evt) {
		  var evt = (evt) ? evt : ((event) ? event : null);
		  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
		  if ((evt.keyCode == 13) && (node.type=="text"))  {return false;}
		}
		document.onkeypress = stopRKey;
		
		$(".chzn").chosen();
		$(document).ready(function(){
			// FORM VALIDATION
			$("#comInf").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler:function(form){
					loadingScreen();
					form.submit();
				}
			});
		});
	</script> 
</head>
<body>
	<!-- BreadCrumbs -->
    <%@ include file="/WEB-INF/pages/vnd/reg/regBreadcrumbs.jsp" %>
    <!-- End of BreadCrumbs -->
    
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
					
			<s:form action="vnd/companyInformation" id="comInf">
				<s:hidden name="vndHeader.vendorId" />
				<!-- Nama Perusahaan -->
				<div class="box">
					<div class="title">
						<s:text name="com.inf.name.header"></s:text>
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle">
							<tr>
								<td width="150px"><s:label key="com.inf.prefix"></s:label> </td> 
								<td width="150px"><s:select list="{'PT','CV', 'Lainnya'}" name="vndHeader.vendorPrefix" headerKey="" headerValue="--------" cssClass="chzn" cssStyle="width:200px"></s:select></td>
								<td width="200px" style="text-align: right;"><s:label key="com.inf.lainnya"></s:label> &nbsp;</td>
								<td><s:textfield onkeyup="upper(this);" name="vndHeader.vendorPrefixOther"></s:textfield> </td>
							</tr>
							<tr>
								<td> <s:label key="com.inf.nama"></s:label> </td>
								<td colspan="3"><s:textfield onkeyup="upper(this);" name="vndHeader.vendorName" cssClass="required" maxLength="40"></s:textfield></td>
							</tr>
							<tr>
								<td><s:label key="com.inf.suffix"></s:label> </td>
								<td><s:select list="{'BV','Bhd', 'Gmbh', 'NV', 'Pte. Ltd', 'TBK', 'Lainnya'}" name="vndHeader.vendorSuffix" headerKey="" headerValue="---------" cssClass="chzn" cssStyle="width:200px"></s:select></td>
								<td style="text-align: right;"><s:label key="com.inf.lainnya"></s:label> &nbsp;</td>
								<td><s:textfield onkeyup="upper(this);" name="vndHeader.vendorSuffixOther"></s:textfield> </td>
							</tr>
							<tr>
								<td><s:label key="com.inf.tipe.perusahaan"></s:label> </td>
								<td colspan="3">
									<s:select list="listCompanyType" name="companyType" listKey="id" listValue="companyTypeName" multiple="true" cssClass="required"></s:select>
								</td>
							</tr>
						</table>
					</div>
				</div>
				
				<!-- Kontak Perusahaan -->		
				<div class="box">
					<div class="title center">
						<s:text name="com.inf.contact.header"></s:text>
						<span class="hide"></span>
					</div>
					<div class="content center">
						<a href='javascript:void(0)' class='uibutton icon add' onclick='load_into_box("<s:url action="ajax/setTmpVndAdd" />")'><s:label key="com.inf.add.label"></s:label> </a>						
						<div id="hasil" style="margin-top: 20px;">
							<s:include value="ajaxListComInf.jsp" />
						</div>

					</div>
				</div>
				
				<!-- Afiliasi Perusahaan -->		
				<div class="box">
					<div class="title center">
						<s:text name="com.add.name.header"></s:text>
						<span class="hide"></span>
					</div>
					<div class="content center">
						<a href='javascript:void(0)' class='uibutton icon add' onclick='load_into_box("<s:url action="ajax/setTmpVndTambahan" />")'><s:label key="com.inf.aff.label"></s:label> </a>						
						<div id="aff" style="margin-top: 20px;">
							<s:include value="ajaxListAfiliasi.jsp" />
						</div>

					</div>
				</div>
								
				<!-- COntact Person -->
				<div class="box">
					<div class="title center">
						Contact Person
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="form formStyle">
							<tr>
								<td width="150px"><s:label key="com.inf.cp.nama"></s:label> (*)</td>
								<td><s:textfield onkeyup="upper(this);" name="vndHeader.vendorContactName" cssClass="required"></s:textfield></td>
							</tr>
							<tr>
								<td><s:label key="com.inf.cp.jabatan"></s:label> (*)</td>
								<td><s:textfield onkeyup="upper(this);" name="vndHeader.vendorContactPos" cssClass="required"></s:textfield></td>
							</tr>
							<tr>
								<td><s:label key="com.inf.cp.telp"></s:label> (*)</td>
								<td><s:textfield onkeyup="upper(this);" name="vndHeader.vendorContactPhone" cssClass="required" maxlength="16" placeholder="(0251) 12345 678"></s:textfield></td>
							</tr>
							<tr>
								<td><s:label key="com.inf.cp.fax"></s:label> </td>
								<td><s:textfield name="vndHeader.vendorContactFax" placeholder="(0251) 12345 678"></s:textfield></td>
							</tr>
							<tr>
								<td><s:label key="com.inf.cp.email"></s:label> (*)</td>
								<td><s:textfield name="vndHeader.vendorContactEmail" cssClass="required email" placeholder="email@domain.com"></s:textfield></td>
							</tr>
						</table>
					</div>
				</div>
				
				<div class="uibutton-toolbar extend">
					<a href="javascript:void(0)" class="uibutton" onclick="confirmPopUp('regCancel();', 'Batal', 'Apakah anda yakin untuk membatalkan transaksi ?', 'Ya', 'Tidak');"><s:text name="cancel"></s:text> </a>
					<s:submit name="save" key="save.temp" cssClass="uibutton"></s:submit>
					<s:submit name="saveAndNext" key="save.next" cssClass="uibutton"></s:submit>	
				</div>
						
			</s:form>
				
		</div>
	</div>
</body>
</html>