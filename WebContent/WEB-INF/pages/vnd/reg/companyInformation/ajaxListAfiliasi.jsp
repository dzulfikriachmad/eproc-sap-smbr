			<%@ include file="/include/definitions.jsp" %>
			
			<center>
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>					
			<display:table name="${listTmpVndTambahan }" id="s" pagesize="10" requestURI="companyInformation.jwebs" excludedParams="*" style="width: 100%;" class="style1">
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column titleKey="com.inf.aff.tipe" total="true" style="width: 100px;">${s.tipe }</display:column>
				<display:column titleKey="com.inf.aff.nama" style="width: 200px;">${s.nama  }</display:column>
				<display:column titleKey="com.inf.aff.alamat" style="width: 200px;">${s.alamat  }, ${s.kodePos }</display:column>
				<display:column titleKey="com.inf.aff.negara" style="width: 100px;">${s.negara.countryName }</display:column>
				<display:column titleKey="" media="html" style="width: 60px;">
					<div class="uibutton-group">
						<s:url var="set" action="ajax/setTmpVndTambahan" /> 
						<a title="Ubah data  perusahaan" class="uibutton icon edit" href='javascript:void(0)' onclick='load_into_box("${set}", "act=edit&id=${s.id}");'>&nbsp;</a>
						<a title="Hapus data  perusahaan" class="uibutton" href='javascript:void(0)' onclick='confirmPopUp("deleteObj(\"${set}\", \"act=delete&tmpVndTambahan.id=${s.id}\", \"#aff\")", "Hapus Data", "Yakin ingin menghapus?", "Hapus", "Batal");'>X</a>
					</div>
				</display:column>
			</display:table>
			</center>
			<script>
				$(document).ready(function(){	
					// SYSTEM MESSAGES
					$(".message").click(function () {
				      $(this).fadeOut();
				    });
				});
			</script>