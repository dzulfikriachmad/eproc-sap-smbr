			<%@ include file="/include/definitions.jsp" %>
			
			<center>
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>		
						
			<display:table name="${listTmpVndEquip }" id="s" pagesize="10" requestURI="companyFacility.jwebs" excludedParams="*" style="width: 100%;" class="style1">
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column titleKey="com.fac.tipe" style="width: 200px; " class="center">
					<c:if test="${s.equipTipe == 1}">
						<s:text name="com.fac.type1"></s:text>
					</c:if>
					<c:if test="${s.equipTipe == 2}">
						<s:text name="com.fac.type2"></s:text>
					</c:if>
					<c:if test="${s.equipTipe == 3}">
						<s:text name="com.fac.type3"></s:text>
					</c:if>
					<c:if test="${s.equipTipe == 4}">
						<s:text name="com.fac.type4"></s:text>
					</c:if>
					<c:if test="${s.equipTipe == 5}">
						<s:text name="com.fac.type5"></s:text>
					</c:if>
					<c:if test="${s.equipTipe == 6}">
						<s:text name="com.fac.type6"></s:text>
					</c:if>
					<c:if test="${s.equipTipe == 7}">
						<s:text name="com.fac.type7"></s:text>
					</c:if>
					<c:if test="${s.equipTipe == 8}">
						<s:text name="com.fac.type8"></s:text>
					</c:if>					
				</display:column>
				<display:column titleKey="com.fac.nama" style="width: 150px;" class="center">${s.equipName }</display:column>
				<display:column titleKey="com.fac.spesifikasi" class="center" style="width: 200px;">${s.equipSpecification }</display:column>
				<display:column titleKey="com.fac.quantity" class="center" style="width: 40px;">${s.equipQuantity }</display:column>
				<display:column titleKey="com.fac.status" class="center" style="width: 60px;">
					<c:if test="${s.equipStatus == 1}">
						MILIK (Own Property)
					</c:if>
					<c:if test="${s.equipStatus == 2}">
						SEWA (Rent)
					</c:if>		
				</display:column>
				<display:column titleKey="com.fac.year" class="center" >${s.equipYearMade }</display:column>
				<display:column titleKey="com.fac.pembelian" class="center" style="width: 70px;"><fmt:formatDate value="${s.equipBuyDate }" pattern="dd-MMM-yyyy"/> </display:column>
				<display:column title="" media="html" style="width: 80px;" class="center">
					<div class="uibutton-group">
						<s:url var="set" action="ajax/setTmpVndEquip" />
						<a title="Ubah data fasilitas perusahaan" class="uibutton icon edit" href='javascript:void(0)' onclick='load_into_box("${set}", "act=edit&id=${s.id}");'>&nbsp;</a>
						<a title="Hapus data fasilitas perusahaan" class="uibutton" href='javascript:void(0)' onclick='confirmPopUp("deleteObj(\"${set}\", \"act=delete&id=${s.id}\", \"#fac\")", "Hapus Data Fasilitas / Peralatan Perusahaan", "Yakin ingin menghapus?", "Hapus", "Batal");'>X</a>
					</div>
				</display:column>
			</display:table>
			</center>
			<script>
				$(document).ready(function(){	
					// SYSTEM MESSAGES
					$(".message").click(function () {
				      $(this).fadeOut();
				    });
				});
			</script>