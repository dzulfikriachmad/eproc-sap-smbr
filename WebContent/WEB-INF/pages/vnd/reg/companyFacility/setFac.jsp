<%@ include file="/include/definitions.jsp" %>
	<script type="text/javascript">
		$(document).ready(function(){
			// FORM VALIDATION
			$(".chzn").chosen();
			$("#formTmpVndEquip").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler: function() {
					sendForm(document.formTmpVndEquip,"<s:url action="ajax/submitTmpVndEquip" />","#fac");jQuery.facebox.close();
				}
			});
		});
	</script>
	
<div class='fbox_header'><s:text name="com.fac.add"></s:text> </div>
    <div class='fbox_container'>
    	<s:form name="formTmpVndEquip" id="formTmpVndEquip">
        <div class='fbox_content'>
        	<div id="cek"></div>
            
            	<s:hidden name="tmpVndEquip.id"></s:hidden>
            	<s:hidden name="tmpVndEquip.tmpVndHeader.vendorId"></s:hidden>
            	<table style="min-width: 600px; margin: 0px 10px;" class="form formStyle">
            		<tr>
            			<td width="170px"><s:label key="com.fac.tipe"/>(*) </td>
            			<td colspan="3"><s:select cssClass="chzn" list="listEquipType" listKey="key" listValue="value" name="tmpVndEquip.equipTipe"></s:select></td>
            		</tr>            		
            		<tr>
            			<td><s:label key="com.fac.nama"/> </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndEquip.equipName" cssClass="required" /> </td>
            		</tr>  
            		<tr>
            			<td><s:label key="com.fac.spesifikasi"/>(*) </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" maxLength="100" name="tmpVndEquip.equipSpecification" cssClass="required" /> </td>
            		</tr> 
            		<tr>
            			<td><s:label key="com.fac.quantity"/>(*) </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndEquip.equipQuantity" cssStyle="width: 60px;" cssClass="required number" /></td>
            		</tr>
            		<tr>
            			<td><s:label key="com.fac.status"/>(*) </td>
            			<td colspan="3"><s:select cssClass="chzn" list="#{'1':'MILIK (OWN PROPERTY)', '2':'SEWA (RENT)'}" name="tmpVndEquip.equipStatus"></s:select></td>
            		</tr>    
            		<tr>
            			<td><s:label key="com.fac.year"/>(*) </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndEquip.equipYearMade" cssStyle="width: 60px;" maxlength="4" cssClass="required number" /></td>
            		</tr>
            		<tr>
            			<td><s:label key="com.fac.pembelian"/>(*) </td>
            			<td colspan="3"><s:textfield  name="tmpVndEquip.equipBuyDate" cssStyle="width: 80px;" id="tanggal1" cssClass="required" /></td>
            		</tr>            		
            		
            	</table>
            
        </div>
        <div class='fbox_footer'>
            <s:submit key="save" cssClass="uibutton confirm" ></s:submit>
            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()'>Batalkan</a>
        </div>
        </s:form>
    </div>
    
    <script type="text/javascript">
		$(document).ready(function(){	
			$.datepicker.setDefaults($.datepicker.regional['${locale}']);
			$("#tanggal1").datepicker({
				changeYear: true,
				changeMonth: true,
				yearRange: '1970:2030'
			});
			
		});
	</script>
