<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Selesai</title>
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	<script type="text/javascript">
		$(document).ready(function(){	
			
		});
		
		//Disable Enter key on form
		function stopRKey(evt) {
		  var evt = (evt) ? evt : ((event) ? event : null);
		  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
		  if ((evt.keyCode == 13) && (node.type=="text"))  {return false;}
		}
		
		document.onkeypress = stopRKey;
	</script>
</head>
<body>
	<div class="breadCrumb module">
       	<ul>
        	<li><s:a href="javascript:void(0)">Home</s:a></li> 
        	<li><s:a action="vnd/regFinish"><s:text name="reg.finish.breadcrumbs" /></s:a></li> 
        </ul>
    </div>
    
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
			
			<s:form>				
				<div class="message blue">
					<span>
						<b>Informasi / <i>Information</i></b>: 
						Pemberitahuan tentang status terbaru registrasi anda akan selalu dikirimkan melalui email<br/>
						<i>
						Notification about registration status will be sent to current registered email
						</i>
					</span>
				</div>
				
				<div class="box">
					<div class="title center">
						Selesai /<i> Finish </i>
						<span class="hide"></span>
					</div>
					<div class="content center">
						${statusText }
						<br/>
						Untuk Proses Verifikasi, silahkan bawa dokumen yang terlampir attachment dibawah ini: <br/>
						<i>For Verification process, please bring all document listed on attachment below</i> <br/>
						<a href="${contextPath }/upload/DokumenRegistrasi.pdf" target="_blank" style="font-weight: bolder;font-size:large; ;color:red"><s:text name="eproc.dokumenregistrasi"></s:text></a>
					</div>
				</div>
																
				<div class="uibutton-toolbar extend">
					<%-- <a href="javascript:void(0)" class="uibutton" onclick="confirmPopUp('regCancel();', 'Batal', 'Apakahanda yakin untuk membatalkan transaksi ?', 'Ya', 'Tidak');"><s:text name="cancel"></s:text> </a>
					<s:submit name="save" key="save" cssClass="uibutton"></s:submit>
					<s:submit name="saveAndNext" key="save.next" cssClass="uibutton"></s:submit> --%>	
				</div>
									
			</s:form>	
		</div>
	</div>
</body>
</html>