<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Registrasi Online e-Procurement</title>
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	
</head>
<body>
	<!-- BreadCrumbs -->
    <div class="breadCrumb module">
       	<ul>
           	<li><s:a action="vnd/login">Login </s:a></li>  
           	<li><s:label key="vnd.reg.title"></s:label> </li>
           	<li> </li>
    	</ul>
    </div>
    <!-- End of BreadCrumbs -->
    
	<div id="right">
		<div class="section">
		
				<div class="box">
					<div class="title center">
						<s:label key="vnd.reg.email.conf.header"></s:label>
						<span class="hide"></span>
					</div>
					<div class="content center">
						<!-- Alert -->
						<s:if test="hasActionErrors()">
							<div class="message red"><s:actionerror/></div>
						</s:if>
						<s:if test="hasActionMessages()">
						    <div class="message green"><s:actionmessage/></div>
						</s:if>
						<img alt="Email Notification" src="${contextPath }/assets/images/icon/mail.png"><br />				
						<s:text name="vnd.reg.email.conf.content"></s:text> 
					</div>
				</div>
				
		</div>
	</div>
</body>
</html>