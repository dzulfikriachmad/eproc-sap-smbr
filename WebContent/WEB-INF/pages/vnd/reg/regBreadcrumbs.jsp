<%@	taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<div class="breadCrumb module">
       	<ul>
        	<li><a>Home</a></li> 
        	<c:if test="${pageId >= 1}">
        		<li><s:a action="vnd/companyInformation" onclick="loadingScreen();"><s:text name="com.inf.breadcrumbs" /></s:a></li>
        	</c:if>
        	<c:if test="${pageId >= 2}">
        		<li><s:a action="vnd/legalInformation" onclick="loadingScreen();"><s:text name="leg.inf.breadcrumbs" /></s:a></li> 
        	</c:if>
        	<c:if test="${pageId >= 3}">
        		<li><s:a action="vnd/managementInformation" onclick="loadingScreen();"><s:text name="man.inf.breadcrumbs" /></s:a></li> 
        	</c:if>
        	<c:if test="${pageId >= 4}">
        		<li><s:a action="vnd/companyFinanceInformation" onclick="loadingScreen();"><s:text name="fin.inf.breadcrumbs" /></s:a></li> 
        	</c:if>
        	<c:if test="${pageId >= 5}">
        		<li><s:a action="vnd/qualificationInformation" onclick="loadingScreen();"><s:text name="qua.inf.breadcrumbs" /></s:a></li> 
        	</c:if>
        	<c:if test="${pageId >= 6}">
        		<li><s:a action="vnd/resourcesInformation" onclick="loadingScreen();"><s:text name="sdm.inf.breadcrumbs" /></s:a></li> 
        	</c:if>
        	<c:if test="${pageId >= 7}">
        		<li><s:a action="vnd/companyFacility" onclick="loadingScreen();"><s:text name="com.fac.breadcrumbs" /></s:a></li> 
        	</c:if>
        	<c:if test="${pageId >= 8}">
        		<li><s:a action="vnd/projectExperience" onclick="loadingScreen();"><s:text name="pro.exp.breadcrumbs" /></s:a></li> 
        	</c:if>
        	<c:if test="${pageId >= 9}">
        		<li><s:a action="vnd/commodityWorkingAreaInformation" onclick="loadingScreen();"><s:text name="com.work.breadcrumbs" /></s:a></li> 
        	</c:if>
        	<c:if test="${pageId >= 10}">
        		<li><s:a action="vnd/regFinish" onclick="loadingScreen();"><s:text name="reg.finish.breadcrumbs" /></s:a></li> 
        	</c:if>
    	</ul>
    </div>