<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Restricted </title>
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	
</head>
<body>
	<!-- BreadCrumbs -->
    <%@ include file="/WEB-INF/pages/vnd/reg/regBreadcrumbs.jsp" %>
    <!-- End of BreadCrumbs -->
    
	<div id="right">
		<div class="section">
		
				<div class="box">
					<div class="title center">
						Restricted
						<span class="hide"></span>
					</div>
					<div class="content center">
						<!-- Alert -->
						<s:if test="hasActionErrors()">
							<div class="message red"><s:actionerror/></div>
						</s:if>
						<s:if test="hasActionMessages()">
						    <div class="message green"><s:actionmessage/></div>
						</s:if>
						<img alt="Email Notification" src="${contextPath }/assets/images/icon/restricted.png"><br />				
						Anda tidak mempunyai akses untuk halaman ini<br />
						You are not allowed to access this page <br/>
						Anda harus melengkapi data sebelumnya utk dapat mengakses halaman ini <br/>
						You Should finish your current data to access this page 
					</div>
				</div>
				
		</div>
	</div>
</body>
</html>