<%@ include file="/include/definitions.jsp" %>

	<script type="text/javascript">
		$(document).ready(function(){
			// FORM VALIDATION
			$('.chzn').chosen();
			$("#formTmpVndCert").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler: function() {
					sendForm(document.formTmpVndCert,"<s:url action="ajax/submitTmpVndCert" />","#tmpVndCert");jQuery.facebox.close();
				}
			});
		});
	</script>
	
<div class='fbox_header'><s:text name="qua.inf.add"></s:text> </div>
    <div class='fbox_container'>
    	<s:form name="formTmpVndCert" id="formTmpVndCert">
        <div class='fbox_content'>
        	<div id="cek"></div>
            
            	<s:hidden name="tmpVndCert.id"></s:hidden>
            	<s:hidden name="tmpVndCert.tmpVndHeader.vendorId"></s:hidden>
            	<table style="min-width: 600px; margin: 0px 10px;" class="form formStyle">
            		<tr>
            			<td width="170px"><s:label key="qua.inf.tipe"/>(*) </td>
            			<td colspan="3"><s:select cssClass="chzn" list="#{'1':'MUTU (QUALITY)', '2':'PENGHARGAAN (APPRECIATION)', '3':'ASOSIASI DAN PROFESI (ASSOCIATION)', '4':'PATENT DAN LISENSI (PATENT AND LICENSE)', '5':'LINGKUNGAN HIDUP (ENVIROMENT)', '6':'LAINNYA (OTHERS)'}" name="tmpVndCert.certTipe"></s:select></td>
            		</tr>            		
            		<tr>
            			<td><s:label key="qua.inf.nomor"/>(*) </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndCert.certNo" cssClass="required" /> </td>
            		</tr>  
            		<tr>
            			<td><s:label key="qua.inf.nama"/>(*) </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndCert.certName" cssClass="required" /> </td>
            		</tr> 
            		<tr>
            			<td><s:label key="qua.inf.deskripsi"/>(*) </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndCert.certDescription" cssClass="required" /></td>
            		</tr>            		
            		<tr>
            			<td><s:label key="qua.inf.dikeluarkan"/>(*) </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndCert.certIssued" cssClass="required" /> </td>
            		</tr>
            		<tr>
            			<td><s:label key="qua.inf.start"/>(*) </td>
            			<td><s:textfield name="tmpVndCert.certFrom" id="tanggal1" cssStyle="width: 80px;" cssClass="required" /> </td>
            			<td style="text-align: right;"><s:label key="qua.inf.end"/>(*) </td>
            			<td style="text-align: right;"><s:textfield name="tmpVndCert.certTo" id="tanggal2" cssStyle="width: 80px;" cssClass="required" /> </td>
            		</tr>
            	</table>
            
        </div>
        <div class='fbox_footer'>
            <s:submit key="save" cssClass="uibutton confirm" ></s:submit>
            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()'><s:text name="cancel"></s:text> </a>
        </div>
        </s:form>
    </div>
    
    <script type="text/javascript">
		$(document).ready(function(){	
			$.datepicker.setDefaults($.datepicker.regional['${locale}']);
			$("#tanggal1").datepicker({
				changeYear: true,
				changeMonth: true,
				yearRange: '1970:2030'
			});
			$("#tanggal2").datepicker({
				changeYear: true,
				changeMonth: true,
				yearRange: '1970:2030'
			});
		});
	</script>