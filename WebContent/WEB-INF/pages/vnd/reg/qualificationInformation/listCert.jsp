			<%@ include file="/include/definitions.jsp" %>
			
			<center>
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>		
						
			<display:table name="${listTmpVndCert }" id="s" pagesize="10" requestURI="qualificationInformation.jwebs" excludedParams="*" style="width: 100%;" class="style1">
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column titleKey="qua.inf.tipe" style="width: 200px; " class="center">
					<c:if test="${s.certTipe == 1}">
						MUTU (QUALITY)
					</c:if>
					<c:if test="${s.certTipe == 2}">
						PENGAHARGAAN (APPRECIATION)
					</c:if>
					<c:if test="${s.certTipe == 3}">
						ASOSIASI DAN PROFESI (ASSOCIATION AND PROFESSION)
					</c:if>
					<c:if test="${s.certTipe == 4}">
						PATENT DAN LISENSI (PATENT AND LICENSE)
					</c:if>
					<c:if test="${s.certTipe == 5}">
						LINGKUNGAN HIDUP (ENVIROMENT)
					</c:if>
					<c:if test="${s.certTipe == 6}">
						Lainnya (OTHERS)
					</c:if>
				</display:column>
				<display:column titleKey="qua.inf.nomor" style="width: 150px;" class="center">${s.certNo }</display:column>
				<display:column titleKey="qua.inf.nama" style="width: 150px;" class="center">${s.certName }</display:column>
				<display:column titleKey="qua.inf.deskripsi" class="center" style="width: 150px;">${s.certDescription }</display:column>
				<display:column titleKey="qua.inf.dikeluarkan" class="center" style="width: 159px;">${s.certIssued }</display:column>
				<display:column titleKey="qua.inf.start" class="center" style="width: 90px;"><fmt:formatDate value="${s.certFrom }" pattern="dd-MMM-yyyy"/><br /> <s:text name="qua.inf.end"></s:text> <br /><fmt:formatDate value="${s.certTo }" pattern="dd-MMM-yyyy"/></display:column>
				<display:column title="" media="html" style="width: 80px;" class="center">
					<div class="uibutton-group">
						<s:url var="set" action="ajax/setTmpVndCert" />
						<a title="Ubah data Kualifikasi perusahaan" class="uibutton icon edit" href='javascript:void(0)' onclick='load_into_box("${set}", "act=edit&id=${s.id}");'>&nbsp;</a>
						<a title="Hapus data Kualifikasi perusahaan" class="uibutton" href='javascript:void(0)' onclick='confirmPopUp("deleteObj(\"${set}\", \"act=delete&id=${s.id}\", \"#tmpVndCert\")", "Hapus Data Laporan Kualifikasi Perusahaan", "Yakin ingin menghapus?", "Hapus", "Batal");'>X</a>
					</div>
				</display:column>
			</display:table>
			</center>
			<script>
				$(document).ready(function(){	
					// SYSTEM MESSAGES
					$(".message").click(function () {
				      $(this).fadeOut();
				    });
				});
			</script>