<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title><s:text name="tos.title"></s:text></title>
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	
</head>
<body>
	<!-- BreadCrumbs -->
    <div class="breadCrumb module">
       	<ul>
           	<li><s:a action="vnd/login">Home</s:a></li> 
           	<li><s:text name="login.vendor.reg"></s:text> </li> 
           	<li><s:a action="vnd/tos"><s:text name="tos.breadcrumbs"></s:text> </s:a></li>
    	</ul>
    </div>
    <!-- End of BreadCrumbs -->
    
	<div id="right">
		<div class="section">
		
				<div class="box">
					<div class="title center">
						<s:label key="tos.title"></s:label>
						<span class="hide"></span>
					</div>
					<div class="content">
						<!-- TOS 
						<h2><s:label key="tos.breadcrumbs"></s:label></h2>-->
						<center><b style="font-size:14px;">KETENTUAN PENGGUNAAN ePROCUREMENT <br /> PT SEMEN BATURAJA (PERSERO) Tbk. <br /> OLEH PENYEDIA BARANG & JASA</b></center>
						<!--<ol>
							 <s:text name="tos.content"></s:text>
						</ol> -->
<p style="text-align: justify;">Setiap Penyedia Barang dan Jasa pengguna aplikasi eProcurement PT Semen Baturaja (Persero)
Tbk. diatur oleh ketentuan yang telah dipersyaratkan di bawah ini.</p>

<p style="font-weight:bold;font-size:14px;">I. Ketentuan Umum</p>
	<ol>
		<li style="text-align:justify;">Sistem eProcurement PT Semen Baturaja (Persero) Tbk. adalah perangkat lunak aplikasi
		yang bertujuan memfasilitasi Penyedia Barang dan Jasa dan PT Semen Baturaja 
		(Persero) Tbk. agar dapat melakukan transaksi pengadaan barang dan jasa melalui 
		media internet, termasuk registrasi online untuk menjadi Penyedia Barang dan Jasa.</li>
		<li style="text-align:justify;">Penyedia Barang dan Jasa PT Semen Baturaja (Persero) Tbk. wajib tunduk pada 
		persyaratan eProcurement PT Semen Baturaja (Persero) Tbk. yang tertera dalam 
		ketentuan ini serta kebijakan lain yang ditetapkan oleh PT Semen Baturaja (Persero)
		Tbk.</li>
		<li style="text-align:justify;">Transaksi melalui eProcurement PT Semen Baturaja (Persero) Tbk. hanya boleh 
		dilakukan/diikuti oleh Penyedia Barang dan Jasa yang sudah terdaftar dan teraktivasi 
		untuk bisa mengikuti transaksi secara elektronik.</li>
	</ol>
<p style="font-weight:bold;font-size:14px;">II. PERSYARATAN KEANGGOTAAN ePROCUREMENT PT SEMEN BATURAJA (Persero) Tbk.</p>
	<ol>
		<li style="text-align:justify;">Penyedia Barang dan Jasa harus berbentuk badan usaha dan dianggap mampu 
		melakukan perbuatan hukum.</li>
		<li style="text-align:justify;">Untuk mendapatkan account dalam sistem eProcurement PT Semen Baturaja (Persero) 
		Tbk., calon Penyedia Barang dan Jasa terlebih dahulu harus melakukan registrasi online 
		dengan data yang benar dan akurat, sesuai keadaan yang sebenarnya.</li>
		<li style="text-align:justify;">Calon Penyedia Barang dan Jasa dapat melakukan transaksi melalui Sistem 
		eProcurement PT Semen Baturaja (Persero) Tbk., apabila telah menerima konfirmasi 
		aktivasi keanggotaannya dari system eProcurement PT Semen Baturaja (Persero) Tbk.</li>
		<li style="text-align:justify;">Penyedia Barang dan Jasa wajib memperbaharui data perusahaannya jika tidak sesuai 
		lagi dengan keadaan yang sebenarnya atau jika tidak sesuai dengan ketentuan ini.</li>
		<li style="text-align:justify;">Account dalam Sistem eProcurement PT Semen Baturaja (Persero) Tbk. akan berakhir 
		apabila:
		<ul>
			<li style="text-align:justify;">Penyedia Barang dan Jasa mengundurkan diri dengan cara mengirimkan email 
			kepada Biro Pengadaan PT Semen Baturaja (Persero) Tbk. dan mendapatkan email 
			konfirmasi atas pengunduran dirinya.</li>
			<li style="text-align:justify;">Melanggar ketentuan yang telah ditetapkan oleh PT Semen Baturaja (Persero) Tbk. </li>
			<li style="text-align:justify;">Kinerja Penyedia Barang dan Jasa berada pada status black‐list sesuai dengan 
			Kebijakan Pengadaan Barang dan Jasa PT Semen Baturaja (Persero) Tbk</li>
		</ul>
		</li>
		<li style="text-align:justify;">Penyedia Barang dan Jasa setuju bahwa transaksi melalui sistem eProcurement PT 
		Semen Baturaja (Persero) Tbk. tidak boleh menyalahi peraturan perundangan maupun 
		etika bisnis yang berlaku di Indonesia.</li>
		<li style="text-align:justify;">Penyedia Barang dan Jasa tunduk pada semua peraturan yang berlaku di Indonesia yang 
		berhubungan dengan, tetapi tidak terbatas pada, penggunaan jaringan yang terhubung 
		pada jasa dan transmisi data teknis, baik di wilayah Indonesia maupun ke luar dari 
		wilayah Indonesia melalui sistem ini.</li>
		<li style="text-align:justify;">Penyedia Barang dan Jasa menyadari bahwa usaha apapun untuk dapat menembus 
		sistem komputer dengan tujuan memanipulasi data eProcurement PT Semen Baturaja 
		(Persero) Tbk. merupakan tindakan melanggar hukum.</li>
		<li style="text-align:justify;">Biro Pengadaan PT Semen Baturaja (Persero) Tbk. berhak memutuskan perjanjian 
		dengan Penyedia Barang dan Jasa secara sepihak apabila Penyedia Barang dan Jasa 
		dianggap tidak dapat menaati ketentuan yang ada.</li>
	</ol>
<p style="font-weight:bold;font-size:14px;">III. TANGGUNG JAWAB PENYEDIA BARANG DAN JASA</p>
	<ol>
		<li style="text-align:justify;">Penyedia Barang dan Jasa bertanggung jawab atas penjagaan kerahasiaan password‐nya 
		dan bertanggung jawab atas transaksi dan kegiatan lain yang menggunakan account 
		miliknya.</li>
		<li style="text-align:justify;">Penyedia Barang dan Jasa setuju untuk segera memberitahukan kepada Biro Pengadaan 
		PT Semen Baturaja (Persero) Tbk. apabila mengetahui adanya penyalahgunaan account 
		miliknya oleh pihak lain yang tidak berhak atau jika ada gangguan keamanan atas 
		account miliknya itu.</li>
	</ol>	
<p style="font-weight:bold;font-size:14px;">V. KRITIK DAN SARAN</p>
<p style="text-align:justify;">PT Semen Baturaja (Persero) Tbk. dapat memperbaiki, menambah, atau mengurangi ketentuan 
ini setiap saat, dengan atau tanpa pemberitahuan sebelumnya. Setiap Penyedia Barang dan 
Jasa terikat dan tunduk kepada ketentuan yang telah diperbaiki/ditambah/dikurangi.</p>
<p style="font-weight:bold;font-size:14px;">IV. PERUBAHAN KETENTUAN</p>
<p style="text-align:justify;">PT Semen Baturaja (Persero) Tbk. membuka diri bagi kritik dan saran dari Penyedia Barang dan 
Jasa. Kritik dan saran dapat disampaikan melalui email ke : eproc@semenbaturaja.co.id</p>						
						<h3 class="center"><s:text name="tos.agreement"></s:text> </h3>
						<div class="uibutton-toolbar">
							<s:form action="vnd/registration">
								<s:submit name="tos" key="yes" cssClass="uibutton"></s:submit>
								<s:a action="vnd/login" cssClass="uibutton"><s:text name="no"></s:text> </s:a>
							</s:form>
							
						</div>
								
						<!-- End of TOS -->
					</div>
				</div>
				
		</div>
	</div>
</body>
</html>