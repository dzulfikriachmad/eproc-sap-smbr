<%@ include file="/include/definitions.jsp" %>
	<script type="text/javascript">
		$(document).ready(function(){
			// FORM VALIDATION
			$(".chzn").chosen();
			$("#formTmpVndSdm").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler: function() {
					sendForm(document.formTmpVndSdm,"<s:url action="ajax/submitTmpVndSdm" />","#sdm");jQuery.facebox.close();
				}
			});
		});
	</script>
	
<div class='fbox_header'><s:text name="sdm.inf.tenagaahli.add"></s:text> </div>
    <div class='fbox_container'>
    	<s:form name="formTmpVndSdm" id="formTmpVndSdm">
        <div class='fbox_content'>
        	<div id="cek"></div>
            
            	<s:hidden name="tmpVndSdm.id"></s:hidden>
            	<s:hidden name="tmpVndSdm.tmpVndHeader.vendorId"></s:hidden>
            	<table style="min-width: 600px; margin: 0px 10px;" class="form formStyle">
            		<tr>
            			<td width="170px"><s:label key="sdm.inf.tipe"/>(*) </td>
            			<td colspan="3"><s:select list="#{'1':'TETAP (Employee)', '2':'OUTSOURCING', '3':'LAIN - LAIN (Others)'}" name="tmpVndSdm.sdmTipe" cssClass="chzn" cssStyle="width:200px"></s:select></td>
            		</tr>            		
            		<tr>
            			<td><s:label key="sdm.inf.nama"/> (*) </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndSdm.sdmName" cssClass="required" /> </td>
            		</tr>  
            		<tr>
            			<td><s:label key="sdm.inf.pendidikan"/>(*) </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndSdm.sdmLastEducation" cssClass="required" /> </td>
            		</tr> 
            		<tr>
            			<td><s:label key="sdm.inf.skill"/>(*) </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndSdm.sdmSkillDesc" cssClass="required" /></td>
            		</tr>            		
            		<tr>
            			<td><s:label key="sdm.inf.pengalaman"/>(*) </td>
            			<td colspan="3"><s:textfield name="tmpVndSdm.sdmExperienceYear" cssStyle="width: 60px;" cssClass="required number" /> Tahun (Year) </td>
            		</tr>
            	</table>
            
        </div>
        <div class='fbox_footer'>
            <s:submit key="save" cssClass="uibutton confirm" ></s:submit>
            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()'><s:text name="cancel"></s:text> </a>
        </div>
        </s:form>
    </div>
