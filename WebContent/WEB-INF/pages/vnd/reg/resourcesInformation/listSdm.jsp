			<%@ include file="/include/definitions.jsp" %>
			
			<center>
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>		
						
			<display:table name="${listTmpVndSdm }" id="s" pagesize="10" requestURI="resourcesInformation.jwebs" excludedParams="*" style="width: 100%;" class="style1">
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column titleKey="sdm.inf.tipe" style="width: 200px; " class="center">
					<c:if test="${s.sdmTipe == 1}">
						TETAP (Employee)
					</c:if>
					<c:if test="${s.sdmTipe == 2}">
						OUTSOURCING
					</c:if>
					<c:if test="${s.sdmTipe == 3}">
						LAIN - LAIN (Others)
					</c:if>
					
				</display:column>
				<display:column titleKey="sdm.inf.nama" style="width: 150px;" class="center">${s.sdmName }</display:column>
				<display:column titleKey="sdm.inf.pendidikan" class="center" style="width: 200px;">${s.sdmLastEducation }</display:column>
				<display:column titleKey="sdm.inf.skill" class="center" style="width: 250px;">${s.sdmSkillDesc }</display:column>
				<display:column titleKey="sdm.inf.pengalaman" class="center" style="width: 70px;">${s.sdmExperienceYear } Tahun</display:column>
				<display:column title="" media="html" style="width: 80px;" class="center">
					<div class="uibutton-group">
						<s:url var="set" action="ajax/setTmpVndSdm" />
						<a title="Ubah data Tenaga Ahli perusahaan" class="uibutton icon edit" href='javascript:void(0)' onclick='load_into_box("${set}", "act=edit&id=${s.id}");'>&nbsp;</a>
						<a title="Hapus data Tenaga Ahli perusahaan" class="uibutton" href='javascript:void(0)' onclick='confirmPopUp("deleteObj(\"${set}\", \"act=delete&id=${s.id}\", \"#sdm\")", "Hapus Data Tenaga Ahli Perusahaan", "Yakin ingin menghapus?", "Hapus", "Batal");'>X</a>
					</div>
				</display:column>
			</display:table>
			</center>
			<script>
				$(document).ready(function(){	
					// SYSTEM MESSAGES
					$(".message").click(function () {
				      $(this).fadeOut();
				    });
				});
			</script>