<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title><s:text name="com.work.title"></s:text> </title>
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	<script type="text/javascript">
		var hasil = 0;
		$(document).ready(function(){	
			// FORM VALIDATION
			$("#comWorkForm").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler:function(form){
					if(confirm('Apakah anda yakin sudah mengisi data dengan lengkap dan tepat ? ( Are You Sure You have filled All Your company data correctly ? )')){
						loadingScreen();
						form.submit();
					}
					return false;
				}
			});
			
		});
		

		//Disable Enter key on form
		function stopRKey(evt) {
		  var evt = (evt) ? evt : ((event) ? event : null);
		  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
		  if ((evt.keyCode == 13) && (node.type=="text"))  {return false;}
		}
		
		document.onkeypress = stopRKey;
		
		function finishPopup(){
			var title = "Pemberitahuan / Notification";
			var text = "Apakah anda yakin sudah mengisi data dengan lengkap dan tepat ? <br/> Are You Sure You have filled All Your company data correctly ?";
			var yes = "Ya / Yes";
			var no = "Tidak / No";
			var html = "<div class='fbox_header'>"+title+"</div>" +
					"<div class='fbox_container'><div class='fbox_content'>" +
					text+
					"</div><div class='fbox_footer'>" +
					"<a class=\"uibutton confirm\" href='javascript:void(0)' onclick='submitData();'>"+yes+"</a>" +
					"<a class=\"uibutton\" href='javascript:void(0)' onclick='jQuery.facebox.close()'>"+no+"</a>" +
					"</div></div>";
			jQuery.facebox(html);
		}
	</script>
</head>
<body>
	<!-- BreadCrumbs -->
    <%@ include file="/WEB-INF/pages/vnd/reg/regBreadcrumbs.jsp" %>
    <!-- End of BreadCrumbs -->
    
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
			
			<s:form id="comWorkForm" name="comWorkForm">
			<s:hidden name="tmpVndHeader.vendorId" />
				<div class="box">
					<div class="title center">
						<s:text name="com.work.title"></s:text>
						<span class="hide"></span>
					</div>
					<s:if test="%{tmpVndHeader.vendorNextPage == 9}">
						<div class="message orange" style="height:25px;"><s:text name="eproc.alert.sementara"></s:text></div>
					</s:if>
					<div class="content center">
						<a href='javascript:void(0)' class='uibutton icon add' onclick='load_into_box("<s:url action="ajax/setTmpVndProduct" />")'><s:text name="com.work.add"></s:text> </a>						
						<div id="comWork" style="margin-top: 20px;">
							<s:include value="listProd.jsp" />
						</div>
					</div>
				</div>
				
				
				<div class="box">
					<div class="title center">
						<s:text name="com.work.workingarea"/>
						
						<span class="hide"></span>
					</div>
					<div class="content">
						<table style="width: 700px;">
							<tr>
								<td width="80px"><s:text name="com.work.workingarea"/></td>
								<td colspan="3" style="text-align: left;">
									<s:select list="listAdmDistrict" name="comDistrict" listKey="id" listValue="districtName" multiple="true" cssStyle="width:100%"></s:select>
								</td>
							</tr>
						</table>
					</div>
				</div>
																				
				<div class="uibutton-toolbar extend">
					<a href="javascript:void(0)" class="uibutton" onclick="confirmPopUp('regCancel();', 'Batal', 'Apakah anda yakin untuk membatalkan transaksi ?', 'Ya', 'Tidak');"><s:text name="cancel"></s:text> </a>
					<s:submit name="save" key="save.temp" cssClass="uibutton"></s:submit>
					<s:submit name="saveAndNext" key="save.finalisasi" cssClass="uibutton" cssStyle="color:red;"></s:submit>	
				</div>
									
			</s:form>	
		</div>
	</div>
</body>
</html>