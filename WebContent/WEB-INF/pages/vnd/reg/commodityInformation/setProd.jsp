<%@ include file="/include/definitions.jsp" %>
	<script type="text/javascript">
		$(document).ready(function(){
			$(".chzn").chosen();
			// FORM VALIDATION
			$("#formTmpVndProd").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler: function() {
					sendForm(document.formTmpVndProd,"<s:url action="ajax/submitTmpVndProduct" />","#comWork");
					jQuery.facebox.close();
				}
			});
		});
	</script>
	
<div class='fbox_header'><s:text name="com.work.add"></s:text> </div>
    <div class='fbox_container'>
    <s:form name="formTmpVndProd" id="formTmpVndProd">
        <div class='fbox_content'>
        	<div id="cek"></div>
            
            	<s:hidden name="tmpVndProduct.id"></s:hidden>
            	<s:hidden name="tmpVndProduct.tmpVndHeader.vendorId"></s:hidden>
            	<table style="min-width: 600px; margin: 0px 10px;" class="form formStyle">        
            		<tr>
            			<td><s:label key="com.work.tipe"/> (*) </td>
            			<td colspan="3">
            				<s:select list="listComGroup" listKey="groupCode" listValue="groupName" name="tmpVndProduct.comGroup.groupCode" cssClass="chzn" cssStyle="width:200px"></s:select>
            			</td>
            		</tr>		
            		<tr>
            			<td><s:label key="com.work.product"/> (*) </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndProduct.productName" cssClass="required" /> </td>
            		</tr>  
            		<tr>
            			<td><s:label key="com.work.merek"/>(*) </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndProduct.productBrand" /> </td>
            		</tr>  
            		<tr>
            			<td><s:label key="com.work.source"/> </td>
            			<td colspan="3"><s:select  name="tmpVndProduct.productSource" list="{'LOKAL (LOCAL)', 'IMPORT', 'LOKAL DAN IMPORT (LOCAL AND IMPORT)','LAINNYA (OTHERS)' }" headerKey="" headerValue="" cssClass="chzn" cssStyle="width:300px"> </s:select> </td>
            		</tr>  
            		<tr>
            			<td><s:label key="com.work.prodtipe"/> </td>
            			<td colspan="3"><s:select name="tmpVndProduct.productTipe" list="#{'A':'AGEN (AGENT)', 'B':'SOLE AGENT', 'C':'NON AGENT', 'D':'DISTRIBUTOR', 'E':'MANUFAKTUR', 'F':'LAIN-LAIN (OTHERS)' }" headerKey="" headerValue="" cssClass="chzn" cssStyle="width:300px"></s:select> </td>
            		</tr>  
            		<tr>
            			<td><s:label key="com.work.description"/> </td>
            			<td colspan="3"><s:textarea  name="tmpVndProduct.productDescription" cssClass="required" onkeyup="upper(this);" maxLength="200"></s:textarea> </td>
            		</tr>  
            	</table>
            
        </div>
        <div class='fbox_footer'>
            <s:submit key="save" cssClass="uibutton confirm" ></s:submit>
            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()'><s:text name="cancel"></s:text> </a>
        </div>
        </s:form>
    </div>
    