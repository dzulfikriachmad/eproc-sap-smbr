			<%@ include file="/include/definitions.jsp" %>
			
			<center>
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>		
						
			<display:table name="${listTmpVndProduct }" id="s" pagesize="10" requestURI="qualificationInformation.jwebs" excludedParams="*" style="width: 100%;" class="style1">
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column titleKey="com.work.tipe" style="width: 250px;" class="center">${s.comGroup.groupName }</display:column>
				<display:column titleKey="com.work.product" style="width: 150px;" class="center">${s.productName }</display:column>
				<display:column titleKey="com.work.merek" class="center" style="width: 150px;">${s.productBrand }</display:column>
				<display:column titleKey="com.work.source" class="center" style="width: 100px;">${s.productSource }</display:column>
				<display:column titleKey="com.work.prodtipe" class="center" style="width: 100px;">
					<c:if test="${s.productTipe == 'A' }">AGENT</c:if>
					<c:if test="${s.productTipe == 'B' }">SOLE AGENT</c:if>
					<c:if test="${s.productTipe == 'C' }">NON AGENT</c:if>
					<c:if test="${s.productTipe == 'D' }">DISTRIBUTOR</c:if>
					<c:if test="${s.productTipe == 'E' }">MANUFAKTUR</c:if>
					<c:if test="${s.productTipe == 'F' }">LAIN - LAIN (OTHERS)</c:if>
				</display:column>
				<display:column titleKey="com.work.description" class="center" style="width: 300px;">${s.productDescription }</display:column>
				<display:column title="" media="html" style="width: 80px;" class="center">
					<div class="uibutton-group">
						<s:url var="set" action="ajax/setTmpVndProduct" />
						<a title="Ubah data komoditas" class="uibutton icon edit" href='javascript:void(0)' onclick='load_into_box("${set}", "act=edit&id=${s.id}");'>&nbsp;</a>
						<a title="Hapus data komoditas" class="uibutton" href='javascript:void(0)' onclick='confirmPopUp("deleteObj(\"${set}\", \"act=delete&id=${s.id}\", \"#comWork\")", "Hapus Data Komoditas", "Yakin ingin menghapus?", "Hapus", "Batal");'>X</a>
					</div>
				</display:column>
			</display:table>
			</center>
			<script>
				$(document).ready(function(){	
					// SYSTEM MESSAGES
					$(".message").click(function () {
				      $(this).fadeOut();
				    });
				});
			</script>