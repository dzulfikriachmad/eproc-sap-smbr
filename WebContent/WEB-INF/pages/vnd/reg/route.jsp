<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Route to Vendor </title>
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	<script type="text/javascript">
		$(document).ready(function(){	
			
		});
		
		//Disable Enter key on form
		function stopRKey(evt) {
		  var evt = (evt) ? evt : ((event) ? event : null);
		  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
		  if ((evt.keyCode == 13) && (node.type=="text"))  {return false;}
		}
		
		document.onkeypress = stopRKey;
	</script>
</head>
<body>
	<!-- BreadCrumbs -->
    <%@ include file="/WEB-INF/pages/vnd/reg/regBreadcrumbs.jsp" %>
    <!-- End of BreadCrumbs -->
    
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
			
			<s:form>				
				<div class="message blue">
					<span>
						<b>Informasi</b>: 
						Pemberitahuan tentang status terbaru registrasi anda akan selalu dikirimkan melalui email,
					</span>
				</div>
				
				<div class="box">
					<div class="title center">
						Route to Vendor
						<span class="hide"></span>
					</div>
					<div class="content center">
						Opppss..<br />
						Ada ketidakcocokan dengan data yang anda masukan,..<br />
						Harap segera diperiksa...						
						
					</div>
				</div>
																
				<div class="uibutton-toolbar extend">
					<%-- <a href="javascript:void(0)" class="uibutton" onclick="confirmPopUp('regCancel();', 'Batal', 'Apakahanda yakin untuk membatalkan transaksi ?', 'Ya', 'Tidak');"><s:text name="cancel"></s:text> </a>
					<s:submit name="save" key="save" cssClass="uibutton"></s:submit>
					<s:submit name="saveAndNext" key="save.next" cssClass="uibutton"></s:submit> --%>	
				</div>
									
			</s:form>	
		</div>
	</div>
</body>
</html>