<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title><s:text name="vnd.reg.title"></s:text> </title>
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	
	<script type="text/javascript">
		$(document).ready(function(){
			// FORM VALIDATION
			$("#registration").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				rules: {
					password2: {
						required: true,
						equalTo: "#password"
					}
				},
				messages: {
					password2: {
						equalTo: "Tidak Cocok dengan password di atas"
					},
					textCaptcha:{
						equalTo: "Captcha Tidak Cocok"
					}
				},
				submitHandler:function(form){
					loadingScreen();
					form.submit();
				}
			});
			getCaptcha();
			
			
			$(function() {
			    $('#username').on('keypress', function(e) {
			        if (e.which == 32)
			            return false;
			    });
			});
		});
		
		function getCaptcha(){
			$.ajax({
		        url: "<s:url action="jsonCaptcha" />",
		        success: function(response){
		        	var nilai = response.globalReturn;
		        	$("#captcha").html(nilai);
		        	$("#rnd").val(nilai);
		        },
		        type: "post", 
		        dataType: "json"
		    }); 
		}
		
		/*$("#npwp").live("keyup", function(){
			var test = $("#npwp").val();
			console.log(test);

			if($("#npwp").val()=='' || $("#npwp").val()==null){
				$("#npwpAlert").hide();
				$("#sbmtButton").show();
				console.log('test');
			}else{
				$("#npwpAlert").show();
				$("#npwpAlert").html('NPWP sudah terdaftar');			
				$("#sbmtButton").hide();
				console.log('testasdas');
			}
			
			/*$.ajax({
			url: "<s:url action="ajax/vnd/checkNpwp" />",
			success: function(response){
				if(response == false){
					$("#npwpAlert").show();
					$("#npwpAlert").html('NPWP sudah terdaftar');			
					$("#sbmtButton").hide();
				}else{
					$("#npwpAlert").hide();
					$("#sbmtButton").show();
				}
			}
			});*/
		//});
	</script>
</head>
<body>
	<!-- BreadCrumbs -->
    <div class="breadCrumb module">
       	<ul>
           	<li><s:a action="vnd/login">Home</s:a></li> 
           	<li><s:text name="login.vendor.reg"></s:text> </li> 
           	<li><s:a action="vnd/tos"><s:text name="tos.breadcrumbs"></s:text> </s:a></li>
           	<li><s:a action="vnd/registration"><s:text name="vnd.reg.breadcrumbs"></s:text></s:a></li>
    	</ul>
    </div>
    <!-- End of BreadCrumbs -->
    
	<div id="right">
		<div class="section">
		
				<div class="box">
					<div class="title center">
						<s:text name="vnd.reg.header"></s:text>
						<span class="hide"></span>
					</div>
					<div class="content">
						<!-- Alert -->
						<s:if test="hasActionErrors()">
							<div class="message red"><s:actionerror/></div>
						</s:if>
						<s:if test="hasActionMessages()">
						    <div class="message green"><s:actionmessage/></div>
						</s:if>
						
						<s:form action="vnd/registration" id="registration" >
							<s:hidden name="vndHeader.vendorStatus" value="P"></s:hidden>
							<s:hidden name="vndHeader.vendorIsActivate" value="0"></s:hidden>
							<s:hidden name="vndHeader.vendorNextPage" value="1"></s:hidden>
							
							<table class="form formStyle">  
								<tr>
									<td width="200px"><s:label key="vnd.reg.email.address" /></td>
									<td><s:textfield name="vndHeader.vendorEmail" placeholder="procurement@safeer.com" cssClass="email required" /></td>
								</tr>
								<tr>
									<td><s:label key="login.username"></s:label> (*)</td>
									<td><s:textfield name="vndHeader.vendorLogin" placeholder="username" cssClass="required" id="username" minlength="6" /></td>
								</tr>
								<tr>
									<td><!--<s:label key="login.username"></s:label>-->NPWP (*) </td>
									<td><s:textfield name="vndHeader.vendorNpwpNo" placeholder="NPWP" cssClass="required" id="npwp" maxlength="16"/></td>
									<td><label class="error" id="npwpAlert" style="display: none;"></label></td>
								</tr>
								<tr>
									<td><s:label key="login.password"></s:label> (*)</td>
									<td><s:password id="password" name="vndHeader.vendorPassword" placeholder="password" cssClass="required" minlength="6" /></td>
								</tr>
								<tr>
									<td><s:label key="vnd.reg.pass"></s:label>  (*)</td>
									<td><s:password name="password2" placeholder="Ketik Ulang Password" /></td>
								</tr>
								<tr>
									<td>Captcha </td>
									<td>
										<fieldset style="width:50%">
											<s:hidden id="rnd" name="rnd"></s:hidden>
											<span id="captcha" style="color: GREEN;font-weight: bolder;font-size: x-large;text-align: center"></span>&nbsp;&nbsp;<input type="button" class="uibutton special" onclick="getCaptcha()" value="Re-Captcha" style="float: right"/><br/>
											<s:textfield id="textCaptcha" name="textCaptcha" cssClass="required" placeholder="Isi Sesuai dengan Text Diatas"></s:textfield>
										</fieldset>
									</td>
								</tr>
							</table>
						
					
							<div class="uibutton-toolbar">
								<s:submit name="registerVendor" value="Register" cssClass="uibutton" id="sbmtButton" />
								<s:a action="vnd/login" cssClass="uibutton">Batal</s:a>
							</div>
						</s:form>
					</div>
				</div>
				
		</div>
	</div>
</body>
</html>