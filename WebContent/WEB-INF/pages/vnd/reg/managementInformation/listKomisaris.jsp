			<%@ include file="/include/definitions.jsp" %>
			
			<center>
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>		
						
			<display:table name="${listTmpVndBoardKomisaris }" id="s" pagesize="10" requestURI="managementInformation.jwebs" excludedParams="*" style="width: 100%;" class="style1">
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column titleKey="man.inf.komisaris.nama" total="true" style="width: 100px;">${s.boardName }</display:column>
				<display:column titleKey="man.inf.komisaris.jabatan" style="width: 100px; " class="center">${s.boardPosition }</display:column>
				<display:column titleKey="man.inf.komisaris.telp" style="width: 100px;" class="center">${s.boardPhone }</display:column>
				<display:column titleKey="man.inf.komisaris.ktp" style="width: 100px;" class="center">${s.boardKtp }</display:column>
				<display:column titleKey="man.inf.komisaris.npwp" class="center" style="width: 200px;">${s.boardNpwpNo }</display:column>
				<display:column titleKey="man.inf.komisaris.negara" style="width: 100px;" class="center">${s.admCountry.countryName }</display:column>
				<display:column title="" media="html" style="width: 60px;" class="center">
					<div class="uibutton-group">
						<s:url var="set" action="ajax/setTmpVndBoardKomisaris" /> 
						<a title="Ubah data dewan komisaris perusahaan" class="uibutton icon edit" href='javascript:void(0)' onclick='load_into_box("${set}", "act=edit&id=${s.id}");'>&nbsp;</a>
						<a title="Hapus data dewan komisaris perusahaan" class="uibutton" href='javascript:void(0)' onclick='confirmPopUp("deleteObj(\"${set}\", \"act=delete&id=${s.id}\", \"#komisarisPerusahaan\")", "Hapus Data Dewan Komisaris Perusahaan", "Yakin ingin menghapus?", "Hapus", "Batal");'>X</a>
					</div>
				</display:column>
			</display:table>
			</center>
			<script>
				$(document).ready(function(){	
					// SYSTEM MESSAGES
					$(".message").click(function () {
				      $(this).fadeOut();
				    });
				});
			</script>