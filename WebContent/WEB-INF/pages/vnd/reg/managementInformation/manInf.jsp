<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title><s:text name="man.inf.title"></s:text> </title>
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	<script type="text/javascript">
		$(document).ready(function(){
			// FORM VALIDATION
			$("#manInformation").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler:function(form){
					if($("#sizeDir").val()==0){
						alert('<s:text name="man.inf.direksi.minimal"/>');
						return false;
					}
					loadingScreen();
					form.submit();
				}
			});
		});
		
		//Disable Enter key on form
		function stopRKey(evt) {
		  var evt = (evt) ? evt : ((event) ? event : null);
		  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
		  if ((evt.keyCode == 13) && (node.type=="text"))  {return false;}
		}
		
		document.onkeypress = stopRKey;
	</script>
	
</head>
<body>
	<!-- BreadCrumbs -->
    <%@ include file="/WEB-INF/pages/vnd/reg/regBreadcrumbs.jsp" %>
    <!-- End of BreadCrumbs -->
    
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
			<input type="hidden" id="sizeDir"/>
			<s:form id="manInformation" name="manInformation">
	
				<!-- DEWAN KOMISARIS -->
				<div class="box">
					<div class="title center">
						<s:label key="man.inf.komisaris.label"></s:label>
						<span class="hide"></span>
					</div>
					<div class="content center">
						<a href='javascript:void(0)' class='uibutton icon add' onclick='load_into_box("<s:url action="ajax/setTmpVndBoardKomisaris" />")'><s:label key="man.inf.komisaris.add"></s:label></a>						
						<div id="komisarisPerusahaan" style="margin-top: 20px;">
							<s:include value="listKomisaris.jsp" />
						</div>
					</div>
				</div>
				
				<!-- DEWAN DIREKSI -->
				<div class="box">
					<div class="title center">
						<s:label key="man.inf.direksi.label"></s:label>
						<span class="hide"></span>
					</div>
					<div class="content center">
						<a href='javascript:void(0)' class='uibutton icon add' onclick='load_into_box("<s:url action="ajax/setTmpVndBoardDireksi" />")'><s:label key="man.inf.direksi.add"></s:label></a>						
						<div id="direksiPerusahaan" style="margin-top: 20px;">
							<s:include value="listDireksi.jsp" />
						</div>
					</div>
				</div>
																
				<div class="uibutton-toolbar extend">
					<a href="javascript:void(0)" class="uibutton" onclick="confirmPopUp('regCancel();', 'Batal', 'Apakahanda yakin untuk membatalkan transaksi ?', 'Ya', 'Tidak');"><s:text name="cancel"></s:text> </a>
					<s:submit name="save" key="save.temp" cssClass="uibutton"></s:submit>
					<s:submit name="saveAndNext" key="save.next" cssClass="uibutton"></s:submit>	
				</div>
									
			</s:form>	
		</div>
	</div>
</body>
</html>