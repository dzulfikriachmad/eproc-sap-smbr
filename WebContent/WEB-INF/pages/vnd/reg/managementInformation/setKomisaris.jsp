<%@ include file="/include/definitions.jsp" %>

	<script type="text/javascript">
		$(document).ready(function(){
			// FORM VALIDATION
			$(".chzn").chosen();
			$("#formKomisaris").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler: function() {
					sendForm(document.formKomisaris,"<s:url action="ajax/submitTmpVndBoardKomisaris" />","#komisarisPerusahaan");jQuery.facebox.close();
				}
			});
		});
	</script>
	
<div class='fbox_header'><s:label key="man.inf.komisaris.add"/> </div>
    <div class='fbox_container'>
    	<s:form name="formKomisaris" id="formKomisaris">
        <div class='fbox_content'>
        	<div id="cek"></div>
            
            	<s:hidden name="tmpVndBoard.id"></s:hidden>
            	<s:hidden name="tmpVndBoard.boardTipe" value="2"></s:hidden>
            	<s:hidden name="tmpVndBoard.tmpVndHeader.vendorId"></s:hidden>
            	<table style="min-width: 600px; margin: 0px 10px;" class="form formStyle">
            		<tr>
            			<td width="150px"><s:label key="man.inf.komisaris.nama"/> (*) </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndBoard.boardName" cssClass="required" /></td>
            		</tr>            		
            		<tr>
            			<td><s:label key="man.inf.komisaris.jabatan"/> (*) </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndBoard.boardPosition" cssClass="required"/> </td>
            		</tr>
            		<tr>
            			<td><s:label key="man.inf.komisaris.telp"/> (*) </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndBoard.boardPhone" cssClass="required" /> </td>
            		</tr>  
            		<tr>
            			<td><s:label key="man.inf.komisaris.ktp"/> (*) </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndBoard.boardKtp" cssClass="required" /> </td>
            		</tr> 
            		<tr>
            			<td><s:label key="man.inf.komisaris.npwp"/> (*) </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndBoard.boardNpwpNo" cssClass="required" /> </td>
            		</tr>   
            		<tr>
            			<td><s:label key="man.inf.komisaris.negara"/> (*) </td>
            			<td colspan="3"><s:select cssStyle="width:200px" list="listAdmCountry" listKey="id" listValue="countryName" name="tmpVndBoard.admCountry.id" id="negara" cssClass="required chzn"  /> </td>
            		</tr>
            	</table>
            
        </div>
        <div class='fbox_footer'>
            <s:submit key="save" cssClass="uibutton confirm" ></s:submit>
            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()'>Batalkan</a>
        </div>
        </s:form>
    </div>