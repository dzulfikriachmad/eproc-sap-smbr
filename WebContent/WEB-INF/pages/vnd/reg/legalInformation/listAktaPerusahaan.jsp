			<%@ include file="/include/definitions.jsp" %>
			
			<center>
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>		
						
			<display:table name="${listTmpVndAkta }" id="s" pagesize="10" requestURI="legalInformation.jwebs" excludedParams="*" style="width: 100%;" class="style1">
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column titleKey="leg.inf.akta.tipe" total="true" style="width: 40px;"  class="center">
					<c:if test="${s.aktaType == 1 }">PENDIRIAN (ESTABLISHMENT)</c:if>
					<c:if test="${s.aktaType == 2 }">PERUBAHAN (CHANGES)</c:if>
				</display:column>
				<display:column titleKey="leg.inf.akta.nomor" style="width: 140px; ">${s.aktaNomor }</display:column>
				<display:column titleKey="leg.inf.akta.pembuatan" style="width: 110px;" class="center"><fmt:formatDate value="${s.aktaCreatedDate }" pattern="dd-MMM-yyyy"/> </display:column>
				<display:column titleKey="leg.inf.akta.beritanegara" class="center" style="width: 100px;"><fmt:formatDate value="${s.aktaBeritaNegara }" pattern="dd-MMM-yyyy"/> </display:column>
				<display:column titleKey="leg.inf.akta.hakim" style="width: 110px;" class="center"><fmt:formatDate value="${s.aktaPengesahanHakim }" pattern="dd-MMM-yyyy"/> </display:column>
				<display:column titleKey="leg.inf.akta.notaris" style="width: 130px;" class="center">${s.aktaNotarisName }</display:column>
				<display:column title="" media="html" style="width: 60px;" class="center">
					<div class="uibutton-group">
						<s:url var="set" action="ajax/setTmpVndAkta" /> 
						<a title="Ubah data akta perusahaan" class="uibutton icon edit" href='javascript:void(0)' onclick='load_into_box("${set}", "act=edit&id=${s.id}");'>&nbsp;</a>
						<a title="Hapus data akta perusahaan" class="uibutton" href='javascript:void(0)' onclick='confirmPopUp("deleteObj(\"${set}\", \"act=delete&id=${s.id}\", \"#aktaPerusahaan\")", "Hapus Akta Perusahaan", "Yakin ingin menghapus?", "Hapus", "Batal");'>X</a>
					</div>
				</display:column>
			</display:table>
			</center>
			<script>
				$(document).ready(function(){	
					// SYSTEM MESSAGES
					$(".message").click(function () {
				      $(this).fadeOut();
				    });
				});
			</script>