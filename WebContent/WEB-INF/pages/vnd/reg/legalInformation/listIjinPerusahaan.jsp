			<%@ include file="/include/definitions.jsp" %>
			
			<center>
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>		
						
			<display:table name="${listIjin }" id="s" pagesize="10" requestURI="legalInformation.jwebs" excludedParams="*" style="width: 100%;" class="style1">
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column titleKey="leg.inf.ijin.tipe" total="true" style="width: 40px;"  class="center">
					${s.vndIjinTipeName }
				</display:column>
				<display:column titleKey="leg.inf.ijin.nomor" style="width: 140px; ">${s.vndIjinNo }</display:column>
				<display:column titleKey="leg.inf.ijin.issued" style="width: 110px;" class="center">
					${s.vndIjinIssued }
				</display:column>
				<display:column titleKey="leg.inf.ijin.start" class="center" style="width: 100px;"><fmt:formatDate value="${s.vndIjinStart }" pattern="dd-MMM-yyyy"/> </display:column>
				<display:column titleKey="leg.inf.ijin.end" style="width: 110px;" class="center"><fmt:formatDate value="${s.vndIjinEnd }" pattern="dd-MMM-yyyy"/> </display:column>
				<display:column titleKey="" media="html" style="width: 60px;" class="center">
					<div class="uibutton-group">
						<s:url var="set" action="ajax/setTmpVndIjin" /> 
						<a title="Ubah data Ijin perusahaan" class="uibutton icon edit" href='javascript:void(0)' onclick='load_into_box("${set}", "act=edit&id=${s.id}");'>&nbsp;</a>
						<a title="Hapus data Ijin perusahaan" class="uibutton" href='javascript:void(0)' onclick='confirmPopUp("deleteObj(\"${set}\", \"act=delete&id=${s.id}\", \"#ijinPerusahaan\")", "Hapus Akta Perusahaan", "Yakin ingin menghapus?", "Hapus", "Batal");'>X</a>
					</div>
				</display:column>
			</display:table>
			</center>
			<script>
				$(document).ready(function(){	
					// SYSTEM MESSAGES
					$(".message").click(function () {
				      $(this).fadeOut();
				    });
				});
			</script>