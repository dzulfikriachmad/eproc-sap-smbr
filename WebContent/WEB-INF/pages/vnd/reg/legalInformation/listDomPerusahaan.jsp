			<%@ include file="/include/definitions.jsp" %>
			
			<center>
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>		
						
			<display:table name="${listTmpVndDom }" id="s" pagesize="10" requestURI="legalInformation.jwebs" excludedParams="*" style="width: 100%;" class="style1">
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column titleKey="leg.inf.domisili.nomor" total="true" style="width: 100px;"  class="center">${s.domNomorDomisili }</display:column>
				<display:column titleKey="leg.inf.domisili.tanggalmulai" style="width: 100px; "><fmt:formatDate value="${s.domTglDomisili }" pattern="dd-MMM-yyyy"/></display:column>
				<display:column titleKey="leg.inf.domisili.tanggalkadaluarsa" style="width: 100px;" class="center"><fmt:formatDate value="${s.domTglKadaluarsa }" pattern="dd-MMM-yyyy"/> </display:column>
				<display:column titleKey="leg.inf.domisili.alamat" class="center" style="width: 200px;">${s.domAddress }, ${s.domPostCode }</display:column>
				<display:column titleKey="leg.inf.domisili.kota" style="width: 100px;" class="center">${s.comCity }, ${s.domProvince }</display:column>
				<display:column titleKey="leg.inf.domisili.negara" style="width: 90px;" class="center">${s.admCountry.countryName }</display:column>
				<display:column title="" media="html" style="width: 60px;" class="center">
					<div class="uibutton-group">
						<s:url var="set" action="ajax/setTmpVndDom" /> 
						<a title="Ubah data domisili perusahaan" class="uibutton icon edit" href='javascript:void(0)' onclick='load_into_box("${set}", "act=edit&id=${s.id}");'>&nbsp;</a>
						<a title="Hapus data domisili perusahaan" class="uibutton" href='javascript:void(0)' onclick='confirmPopUp("deleteObj(\"${set}\", \"act=delete&id=${s.id}\", \"#domPerusahaan\")", "Hapus Domisili Perusahaan", "Yakin ingin menghapus?", "Hapus", "Batal");'>X</a>
					</div>
				</display:column>
			</display:table>
			</center>
			<script>
				$(document).ready(function(){	
					// SYSTEM MESSAGES
					$(".message").click(function () {
				      $(this).fadeOut();
				    });
				});
			</script>