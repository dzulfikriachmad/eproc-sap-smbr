<%@ include file="/include/definitions.jsp" %>
	<script type="text/javascript">
		$(document).ready(function(){
			// FORM VALIDATION
			$("#formAktaPerusahaan").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler: function() {
					sendForm(document.formAktaPerusahaan,"<s:url action="ajax/submitTmpVndAkta" />","#aktaPerusahaan");jQuery.facebox.close();
				}
			});
		});
	</script>
	
<div class='fbox_header'><s:label key="leg.inf.akta.add"></s:label> </div>
    <div class='fbox_container'>
    	<s:form name="formAktaPerusahaan" id="formAktaPerusahaan">
        	<div class='fbox_content'>
        	
            	<s:hidden name="tmpVndAkta.id"></s:hidden>
            	<s:hidden name="tmpVndAkta.tmpVndHeader.vendorId"></s:hidden>
            	
            	<table style="min-width: 600px; margin: 0px 10px;" class="form formStyle">
            		<tr>
            			<td width="150px"><s:label key="leg.inf.akta.tipe"></s:label> (*) </td>
            			<td colspan="3"><s:select list="listAktaType" listKey="key" listValue="value" name="tmpVndAkta.aktaType"></s:select> </td>
            		</tr>            		
            		<tr>
            			<td><s:label key="leg.inf.akta.nomor"></s:label> (*) </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndAkta.aktaNomor" cssClass="required" /> </td>
            		</tr>
            		<tr>
            			<td><s:label key="leg.inf.akta.pembuatan"></s:label> (*) </td>
            			<td colspan="3"><s:textfield name="tmpVndAkta.aktaCreatedDate" id="tanggal1" cssClass="required" /> </td>
            		</tr>  
            		<tr>
            			<td><s:label key="leg.inf.akta.beritanegara"></s:label> </td>
            			<td colspan="3"><s:textfield name="tmpVndAkta.aktaBeritaNegara" id="tanggal2" cssClass="required" /> </td>
            		</tr>  
            		<tr>
            			<td><s:label key="leg.inf.akta.hakim"></s:label> </td>
            			<td colspan="3"><s:textfield name="tmpVndAkta.aktaPengesahanHakim" id="tanggal3" cssClass="required" /> </td>
            		</tr>  
            		<tr>
            			<td><s:label key="leg.inf.akta.notaris"></s:label> </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndAkta.aktaNotarisName" cssClass="required" /> </td>
            		</tr>  
            		<tr valign="middle">
            			<td><s:label key="leg.inf.akta.alamatnotaris"></s:label></td>
            			<td colspan="3"><s:textarea onkeyup="upper(this);" maxlength="200" name="tmpVndAkta.aktaNotarisAddress" rows="3" cols="23" cssClass="required"></s:textarea> </td>
            		</tr>   
            		
            	</table>
            
        	</div>
	        <div class='fbox_footer'>
	           	<s:submit key="save" cssClass="uibutton confirm" ></s:submit>
	            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()'><s:label key="cancel"></s:label> </a>
	        </div>
        </s:form>
    </div>
    
    <script type="text/javascript">
		$(document).ready(function(){	
			$.datepicker.setDefaults($.datepicker.regional['${locale}']);
			$("#tanggal1").datepicker({
				changeYear: true,
				changeMonth: true
			});
			$("#tanggal2").datepicker({
				changeYear: true,
				changeMonth: true
			});
			$("#tanggal3").datepicker({
				changeYear: true,
				changeMonth: true
			});
			
			/* $("#detailAccount").validate({
				errorElement: "em",
				success: function(label) {
					label.text("ok!").addClass("valid");
				},
				errorClass: "invalid"
			}); */
		});
	</script>