<%@ include file="/include/definitions.jsp" %>
	
	<script type="text/javascript">
		$(document).ready(function(){
			// FORM VALIDATION
			$(".chzn").chosen();
			$("#formDomPerusahaan").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler: function() {
					sendForm(document.formDomPerusahaan,"<s:url action="ajax/submitTmpVndDom" />","#domPerusahaan");
					jQuery.facebox.close();
				}
			});
		});
	</script>
	
	
<div class='fbox_header'><s:label key="leg.inf.domisili.add"></s:label> </div>
    <div class='fbox_container'>
    <s:form name="formDomPerusahaan" id="formDomPerusahaan">
        <div class='fbox_content'>
        	<div id="cek"></div>
            	<s:hidden name="tmpVndDom.id"></s:hidden>
            	<s:hidden name="tmpVndDom.tmpVndHeader.vendorId"></s:hidden>
            	<table style="min-width: 600px; margin: 0px 10px;" class="form formStyle">
            		<tr>
            			<td width="150px"><s:label key="leg.inf.domisili.nomor"></s:label> (*) </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndDom.domNomorDomisili" cssClass="required" /></td>
            		</tr>            		
            		
            		<tr>
            			<td><s:label key="leg.inf.domisili.tanggalmulai"></s:label> (*) </td>
            			<td colspan="3"><s:textfield cssClass="required" name="tmpVndDom.domTglDomisili" id="tanggal1" cssStyle="width: 120px;" /> </td>
            		</tr>  
            		<tr>
            			<td><s:label key="leg.inf.domisili.tanggalkadaluarsa"></s:label> (*) </td>
            			<td colspan="3"><s:textfield cssClass="required" name="tmpVndDom.domTglKadaluarsa" id="tanggal2" cssStyle="width: 120px;" /> </td>
            		</tr>  
            		<tr>
            			<td><s:label key="leg.inf.domisili.kota"></s:label> (*) </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" cssClass="required" name="tmpVndDom.comCity" /> </td>
            		</tr>
            		<tr>
            			<td><s:label key="leg.inf.domisili.provinsi"></s:label> (*) </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" cssClass="required" name="tmpVndDom.domProvince" /> </td>
            		</tr>  
            		<tr>
            			<td><s:label key="leg.inf.domisili.negara"></s:label> (*) </td>
            			<td colspan="3"><s:select cssClass="required chzn" list="listAdmCountry" listKey="id" listValue="countryName" name="tmpVndDom.admCountry.id" id="negara" cssStyle="width:200px"/> </td>
            		</tr>
            		
            		<tr valign="middle">
            			<td><s:label key="leg.inf.domisili.alamat"></s:label> (*) </td>
            			<td colspan="3"><s:textarea onkeyup="upper(this);" maxlength="200" cssClass="required" name="tmpVndDom.domAddress" rows="3" cols="23"></s:textarea> </td>
            		</tr>   
            		<tr>
            			<td><s:label key="leg.inf.domisili.kodepos"></s:label> (*) </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" cssClass="required number" name="tmpVndDom.domPostCode" cssStyle="width: 100px;" /> </td>
            		</tr>  
            	</table>
            
        </div>
        <div class='fbox_footer'>
            <s:submit key="save" cssClass="uibutton confirm" ></s:submit>
            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()'><s:label key="cancel"></s:label> </a>
        </div>
        </s:form>
    </div>
    
    <script type="text/javascript">
		$(document).ready(function(){	
			$.datepicker.setDefaults($.datepicker.regional['${locale}']); 
			$("#tanggal1").datepicker({
				changeYear: true,
				changeMonth: true
			});
			$("#tanggal2").datepicker({
				changeYear: true,
				changeMonth: true
			});
		});
	</script>