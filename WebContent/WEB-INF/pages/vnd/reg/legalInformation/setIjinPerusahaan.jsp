<%@ include file="/include/definitions.jsp" %>
	<script type="text/javascript">
		$(document).ready(function(){
			// FORM VALIDATION
			$(".chzn").chosen();
			$("#formIjinPerusahaan").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler: function() {
					sendForm(document.formIjinPerusahaan,"<s:url action="ajax/submitTmpVndIjin" />","#ijinPerusahaan");
					jQuery.facebox.close();
				}
			});
		});
	</script>
	
<div class='fbox_header'><s:label key="leg.inf.ijin.add"></s:label> </div>
    <div class='fbox_container'>
    	<s:form name="formIjinPerusahaan" id="formIjinPerusahaan">
        	<div class='fbox_content'>
        	
            	<s:hidden name="ijin.id"></s:hidden>
            	<s:hidden name="ijin.tmpVndHeader.vendorId"></s:hidden>
            	
            	<table style="min-width: 600px; margin: 0px 10px;" class="form formStyle">
            		<tr>
            			<td width="150px"><s:label key="leg.inf.ijin.tipe"></s:label> (*) </td>
            			<td colspan="3">
            				<s:select list="{'UIJK','IUI','SITU','SIUJK','SIUJPT','SMK3','SM5','KEAGENAN','PENGENAL IMPORTIR','LAINNYA' }" name="ijin.vndIjinTipeName" cssClass="chzn" cssStyle="width:200px"></s:select>
            			 </td>
            		</tr>            		
            		<tr>
            			<td><s:label key="leg.inf.ijin.nomor"></s:label> (*) </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" name="ijin.vndIjinNo" cssClass="required" /> </td>
            		</tr>
            		<tr>
            			<td><s:label key="leg.inf.ijin.issued"></s:label> (*) </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" name="ijin.vndIjinIssued" cssClass="required" /> </td>
            		</tr>  
            		<tr>
            			<td><s:label key="leg.inf.ijin.start"></s:label> </td>
            			<td colspan="3"><s:textfield name="ijin.vndIjinStart" id="tanggal2" cssClass="required" /> </td>
            		</tr>  
            		<tr>
            			<td><s:label key="leg.inf.ijin.end"></s:label> </td>
            			<td colspan="3"><s:textfield name="ijin.vndIjinEnd" id="tanggal3" cssClass="required" /> </td>
            		</tr>  

            	</table>
            
        	</div>
	        <div class='fbox_footer'>
	           	<s:submit key="save" cssClass="uibutton confirm" ></s:submit>
	            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()'><s:label key="cancel"></s:label> </a>
	        </div>
        </s:form>
    </div>
    
    <script type="text/javascript">
		$(document).ready(function(){	
			$.datepicker.setDefaults($.datepicker.regional['${locale}']);
			$("#tanggal2").datepicker({
				changeYear: true,
				changeMonth: true
			});
			$("#tanggal3").datepicker({
				changeYear: true,
				changeMonth: true
			});
			
			/* $("#detailAccount").validate({
				errorElement: "em",
				success: function(label) {
					label.text("ok!").addClass("valid");
				},
				errorClass: "invalid"
			}); */
		});
	</script>