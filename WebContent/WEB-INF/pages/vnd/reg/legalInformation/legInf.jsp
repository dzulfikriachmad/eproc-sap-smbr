<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title><s:text name="leg.inf.title"></s:text> </title>
	<script type="text/javascript">Cufon.replace('h3', {fontFamily: 'Caviar Dreams'});</script>
	<script type="text/javascript">
		$(document).ready(function(){	
			noPkpAction();
			$(".chzn").chosen();
			$.datepicker.setDefaults($.datepicker.regional['${locale}']);
			$("#berlakuMulai").datepicker({
				changeYear: true,
				changeMonth: true
			});
			$("#berlakuSampai").datepicker({
				changeYear: true,
				changeMonth: true
			});
			
			$("#vendorSiupFrom").datepicker({
				changeYear: true,
				changeMonth: true
			});
			$("#vendorSiupTo").datepicker({
				changeYear: true,
				changeMonth: true
			});
			
			//Disable Enter key on form
			function stopRKey(evt) {
			  var evt = (evt) ? evt : ((event) ? event : null);
			  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
			  if ((evt.keyCode == 13) && (node.type=="text"))  {return false;}
			}
			
			document.onkeypress = stopRKey;
		});
		
		function noPkpAction(){
			if($('#pkp').val() == 'Y'){
				$('#noPkp').fadeIn();
			}else{
				$('#noPkp').fadeOut();
			}
			
		}
	</script>
	<script type="text/javascript">
		$(document).ready(function(){
			// FORM VALIDATION
			$("#legalInformation").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler:function(form){
					loadingScreen();
					form.submit();
				}
			});
		});
	</script>
</head>
<body>
	<!-- BreadCrumbs -->
    <%@ include file="/WEB-INF/pages/vnd/reg/regBreadcrumbs.jsp" %>
    <!-- End of BreadCrumbs -->
    
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
			
			<s:form id="legalInformation">
				<!-- Akta Perusahaan -->
				<div class="box">
					<div class="title center">
						<s:label key="leg.inf.akta.label"></s:label>
						<span class="hide"></span>
					</div>
					<div class="content center">
						<a href='javascript:void(0)' class='uibutton icon add' onclick='load_into_box("<s:url action="ajax/setTmpVndAkta" />")'><s:label key="leg.inf.akta.add"></s:label> </a>						
						<div id="aktaPerusahaan" style="margin-top: 20px;">
							<s:include value="listAktaPerusahaan.jsp" />
						</div>
					</div>
				</div>
				
				<!-- Domisili Perusahaan -->
				<div class="box">
					<div class="title center">
						<s:label key="leg.inf.domisili.label"></s:label>
						<span class="hide"></span>
					</div>
					<div class="content center">
						<a href='javascript:void(0)' class='uibutton icon add' onclick='load_into_box("<s:url action="ajax/setTmpVndDom" />")'><s:label key="leg.inf.domisili.add"></s:label> </a>						
						<div id="domPerusahaan" style="margin-top: 20px;">
							<s:include value="listDomPerusahaan.jsp" />
						</div>
					</div>
				</div>	
				
				<!-- NPWP -->
				<div class="box">
					<div class="title center">
						<s:label key="leg.inf.npwp.label"></s:label>
						<span class="hide"></span>
					</div>
					<div class="content center">
						<table class="form formStyle">
							<tr>
								<td width="180px"><s:label key="leg.inf.npwp.no"></s:label> (*)</td>
								<td colspan="3"><s:textfield name="tmpVndHeader.vendorNpwpNo" cssClass="required" maxlength="16"></s:textfield></td>
							</tr>
							<tr>
								<td ><s:label key="leg.inf.npwp.address"></s:label>(*)</td>
								<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndHeader.vendorNpwpAddress" cssClass="required"></s:textfield></td>
							</tr>
							<tr>
								<td><s:label key="leg.inf.npwp.provinsi"></s:label> (*)</td>
								<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndHeader.vendorNpwpProvince" cssClass="required"></s:textfield></td>
							</tr>
							<tr>
								<td><s:label key="leg.inf.npwp.city"></s:label> (*)</td>
								<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndHeader.vendorNpwpCity" cssClass="required"></s:textfield></td>
							</tr>
							<tr>
								<td><s:label key="leg.inf.npwp.kodepos"></s:label> </td>
								<td colspan="3"><s:textfield name="tmpVndHeader.vendorNpwpPostCode" cssStyle="width: 100px" cssClass="number" maxlength="5"></s:textfield></td>
							</tr>
							<tr>
								<td><s:label key="leg.inf.npwp.pkp"></s:label> (*)</td>
								<td colspan="3">
									<s:select cssStyle="width:100px;" list="listPkp" name="tmpVndHeader.vendorNpwpPkp" listKey="key" listValue="value" cssClass="required chzn" onchange="noPkpAction();" id="pkp" ></s:select>
								</td>
							</tr>
							<tr id="noPkp">
								<td ><s:label key="leg.inf.npwp.pkpno"></s:label> </td>
								<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndHeader.vendorNpwpPkpNo"></s:textfield></td>
							</tr>
						</table>
					</div>
				</div>				
				
				<!-- JENIS MITRA KERJA -->
				<!-- <div class="box">
					<div class="title center">
						Jenis Mitra Kerja
						<span class="hide"></span>
					</div>
					<div class="content center">
						Content
					</div>
				</div> -->
				
				<!-- SIUP -->
				<div class="box">
					<div class="title center">
						<s:label key="leg.inf.siup.label"></s:label>
						<span class="hide"></span>
					</div>
					<div class="content center">
						<table class="form formStyle">
							<tr>
								<td width="180px"><s:label key="leg.inf.siup.dikeluarkan"></s:label> (*)</td>
								<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndHeader.vendorSiupIssued" cssClass="required"></s:textfield></td>
							</tr>
							<tr>
								<td><s:label key="leg.inf.siup.nomor"></s:label> (*)</td>
								<td width="250px"><s:textfield onkeyup="upper(this);" name="tmpVndHeader.vendorSiupNo" cssClass="required"></s:textfield></td>
								<td style="width: 180px; text-align: right;"><s:label key="leg.inf.siup.jenisusaha"></s:label> (*)&nbsp;</td>
								<td>
									<s:select list="{'ATK (STATIONERY)', 'MANUFAKTUR (MANUFACTURE)', 'KONTRAKTOR (CONTRACTOR)', 'LAINNYA (OTHERS)'}" name="tmpVndHeader.vendorSiupTipe"  cssClass="required chzn" ></s:select>
								</td>
							</tr>
							<tr>
								<td><s:label key="leg.inf.siup.berlakumulai"></s:label> (*)</td>
								<td width="250px"><s:textfield name="tmpVndHeader.vendorSiupFrom" id="vendorSiupFrom" cssStyle="width: 130px;" cssClass="required" /></td>
								<td style="width: 180px; text-align: right;"><s:label key="leg.inf.siup.berlakusampai"></s:label> (*)&nbsp;</td>
								<%--<td><s:textfield name="tmpVndHeader.vendorSiupTo" id="vendorSiupTo" cssStyle="width: 130px;" cssClass="required" /></td>--%>
								
							</tr>						
						</table>
					</div>
				</div>	
								
				<div class="box">
					<div class="title center">
						<s:label key="leg.inf.ijin.label"></s:label>
						<span class="hide"></span>
					</div>
					<div class="content center">
						<a href='javascript:void(0)' class='uibutton icon add' onclick='load_into_box("<s:url action="ajax/setTmpVndIjin" />")'><s:label key="leg.inf.ijin.add"></s:label> </a>						
						<div id="ijinPerusahaan" style="margin-top: 20px;">
							<s:include value="listIjinPerusahaan.jsp" />
						</div>
					</div>
				</div>	 
				
				<!-- TDP -->
				<div class="box">
					<div class="title center">
						<s:label key="leg.inf.tdp.label"></s:label>
						<span class="hide"></span>
					</div>
					<div class="content center">
						<table class="form formStyle">
							<tr>
								<td width="180px"><s:label key="leg.inf.tdp.dikeluarkan"></s:label> (*)</td>
								<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndHeader.vendorTdpIssued" cssClass="required"></s:textfield></td>
							</tr>
							<tr>
								<td><s:label key="leg.inf.tdp.nomor"></s:label> (*)</td>
								<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndHeader.vendorTdpNo" cssClass="required"></s:textfield></td>
								
							</tr>
							<tr>
								<td><s:label key="leg.inf.tdp.berlakumulai"></s:label> (*)</td>
								<td width="250px"><s:textfield name="tmpVndHeader.vendorTdpFrom" id="berlakuMulai" cssStyle="width: 130px;" cssClass="required" /></td>
								<td style="width: 180px; text-align: right;"><s:label key="leg.inf.tdp.to"></s:label> (*)&nbsp;</td>
								<td><s:textfield name="tmpVndHeader.vendorTdpTo" id="berlakuSampai" cssStyle="width: 130px;" cssClass="required" /></td>
							</tr>						
						</table>
					</div>
				</div>
				
				<div class="uibutton-toolbar extend">
					<a href="javascript:void(0)" class="uibutton" onclick="confirmPopUp('regCancel();', 'Batal', 'Apakah anda yakin untuk membatalkan transaksi ?', 'Ya', 'Tidak');"><s:text name="cancel"></s:text> </a>
					<s:submit name="save" key="save.temp" cssClass="uibutton"></s:submit>
					<s:submit name="saveAndNext" key="save.next" cssClass="uibutton"></s:submit>	
				</div>
									
			</s:form>	
		</div>
	</div>
</body>
</html>