			<%@ include file="/include/definitions.jsp" %>
			
			<center>
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>		
						
			<display:table name="${listTmpVndCv }" id="s" pagesize="10" requestURI="qualificationInformation.jwebs" excludedParams="*" style="width: 100%;" class="style1">
				<display:column title="No." style="width: 20px; text-align: center;">${s_rowNum}</display:column>
				<display:column titleKey="pro.exp.client" style="width: 150px;" class="center">${s.cvClientName }</display:column>
				<display:column titleKey="pro.exp.projectname" style="width: 150px;" class="center">${s.cvProjectName }</display:column>
				<display:column titleKey="pro.exp.contract" class="center" style="width: 150px;">${s.cvContractNo }</display:column>
				<display:column titleKey="pro.exp.projectvalue" class="center" style="width: 120px;">${s.admCurrency.currencyCode} <fmt:formatNumber>${s.cvProjectAmount }</fmt:formatNumber></display:column>
				<display:column titleKey="pro.exp.start" class="center" style="width: 90px;"><fmt:formatDate value="${s.cvProjectStart }" pattern="dd-MMM-yyyy"/><br /><s:text name="pro.exp.end"></s:text> <br /><fmt:formatDate value="${s.cvProjectEnd }" pattern="dd-MMM-yyyy"/></display:column>
				<display:column titleKey="pro.exp.cp" class="center" style="width: 100px;">${s.cvProjectContactPhone }</display:column>
				<display:column title="" media="html" style="width: 80px;" class="center">
					<div class="uibutton-group">
						<s:url var="set" action="ajax/setTmpVndCv" />
						<a title="Ubah data pengalaman proyek perusahaan" class="uibutton icon edit" href='javascript:void(0)' onclick='load_into_box("${set}", "act=edit&id=${s.id}");'>&nbsp;</a>
						<a title="Hapus data pengalaman proyek perusahaan" class="uibutton" href='javascript:void(0)' onclick='confirmPopUp("deleteObj(\"${set}\", \"act=delete&id=${s.id}\", \"#tmpVndCv\")", "Hapus Data Pengalaman Poryek Perusahaan", "Yakin ingin menghapus?", "Hapus", "Batal");'>X</a>
					</div>
				</display:column>
			</display:table>
			</center>
			<script>
				$(document).ready(function(){	
					// SYSTEM MESSAGES
					$(".message").click(function () {
				      $(this).fadeOut();
				    });
				});
			</script>