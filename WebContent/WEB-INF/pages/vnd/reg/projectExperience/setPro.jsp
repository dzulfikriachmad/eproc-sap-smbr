<%@ include file="/include/definitions.jsp" %>
	<script type="text/javascript">
		$(document).ready(function(){
			$(".chzn").chosen();
			// FORM VALIDATION
			$("#formTmpVndCv").validate({
				meta: "validate",
				errorPlacement: function(error, element) {
		          error.insertAfter(element);
				},
				submitHandler: function() {
					$("#projectAmount").val($("#projectAmount").autoNumeric('get'));
					sendForm(document.formTmpVndCv,"<s:url action="ajax/submitTmpVndCv" />","#tmpVndCv");jQuery.facebox.close();
				}
			});
			
			$("#projectAmount").autoNumeric('init');
		});
	</script>
	
<div class='fbox_header'><s:text name="pro.exp.add"/> </div>
    <div class='fbox_container'>
    	<s:form name="formTmpVndCv" id="formTmpVndCv">
        <div class='fbox_content'>
        	<div id="cek"></div>
            
            	<s:hidden name="tmpVndCv.id"></s:hidden>
            	<s:hidden name="tmpVndCv.tmpVndHeader.vendorId"></s:hidden>
            	<table style="min-width: 600px; margin: 0px 10px;" class="form formStyle">        		
            		<tr>
            			<td width="150px"><s:label key="pro.exp.client"/>(*) </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndCv.cvClientName" cssClass="required" /> </td>
            		</tr>  
            		<tr>
            			<td><s:label key="pro.exp.projectname"/>(*) </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndCv.cvProjectName" cssClass="required" /> </td>
            		</tr>  
            		<tr>
            			<td><s:label key="pro.exp.contract"/>(*) </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndCv.cvContractNo" cssClass="required" /> </td>
            		</tr>  
            		      		
            		<tr>
            			<td><s:label key="pro.exp.projectvalue"/> </td>
            			<td colspan="3">
            				<s:select list="listAdmCurrency" listKey="id" listValue="currencyCode" name="tmpVndCv.admCurrency.id" cssClass="chzn" cssStyle="width:200px"></s:select>
            				<s:textfield name="tmpVndCv.cvProjectAmount" cssStyle="width: 100px" cssClass="required number" id="projectAmount"/>
            			</td>
            		</tr>
            		<tr>
            			<td><s:label key="pro.exp.start"/>(*) </td>
            			<td><s:textfield name="tmpVndCv.cvProjectStart" id="tanggal1" cssStyle="width: 80px;" cssClass="required" /> </td>
            			<td style="text-align: right;"><s:label key="pro.exp.end"></s:label> </td>
            			<td style="text-align: right;"><s:textfield name="tmpVndCv.cvProjectEnd" id="tanggal2" cssStyle="width: 80px;" cssClass="required" /> </td>
            		</tr>
            		<tr>
            			<td><s:label key="pro.exp.cp"/>(*) </td>
            			<td colspan="3"><s:textfield onkeyup="upper(this);" name="tmpVndCv.cvProjectContact" cssClass="required" /> </td>
            		</tr>  
            		<tr>
            			<td><s:label key="pro.exp.cpphone"/>(*) </td>
            			<td colspan="3"><s:textfield name="tmpVndCv.cvProjectContactPhone" cssClass="required" /> </td>
            		</tr>  
            		
            	</table>
            
        </div>
        <div class='fbox_footer'>
            <s:submit key="save" cssClass="uibutton confirm" ></s:submit>
            <a class="uibutton" href='javascript:void(0)' onclick='jQuery.facebox.close()'><s:text name="cancel"></s:text> </a>
        </div>
        </s:form>
    </div>
    
    <script type="text/javascript">
		$(document).ready(function(){	
			$.datepicker.setDefaults($.datepicker.regional['${locale}']);
			$("#tanggal1").datepicker({
				changeYear: true,
				changeMonth: true,
				yearRange: '1970:2030'
			});
			$("#tanggal2").datepicker({
				changeYear: true,
				changeMonth: true,
				yearRange: '1970:2030'
			});
		});
	</script>