<%@ include file="/include/definitions.jsp"%>

<!DOCTYPE html>
<html lang="id">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Dashboard</title>
<script type="text/javascript">
	Cufon.replace('h3', {
		fontFamily : 'Caviar Dreams'
	});
</script>
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
			<li><s:a action="vnd/dashboard">Home</s:a></li>
			<li>Vendor Module e-Procurement System</li>
		</ul>
	</div>
	<!-- End of BreadCrumbs -->

	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red">
					<s:actionerror />
				</div>
			</s:if>
			<s:if test="hasActionMessages()">
				<div class="message green">
					<s:actionmessage />
				</div>
			</s:if>

			<div class="box">
				<div class="title">
					Dashboard <span class="hide"></span>
				</div>
				<div class="content center">
					<table class="form formStyle">
						<tr>
							<td width="150px;"><s:label key="vnd.dash.reg"></s:label> </td>
							<td style="font-weight: bolder;">
								<s:url action="vnd/daftarRfq" id="daftarRfq"></s:url>
								<a href="${daftarRfq }">${undangan }</a>								
							</td>
							<td width="150px;"><s:label key="vnd.dash.neg"></s:label> </td>
							<td style="font-weight: bolder;">
								<s:url action="vnd/negosiasi" id="nego"></s:url>
								<a href="${nego }">${negosiasiPengadan }</a>								
							</td>
						</tr>
						<tr>
							<td width="150px;"><s:label key="vnd.dash.waitquote"></s:label> </td>
							<td>
								<s:url action="vnd/penawaranRfq" id="penawaranRfq"></s:url>
								<a href="${penawaranRfq }">${menungguPenawaran }</a>			
							</td>
							<td><s:label key="vnd.dash.eauction"></s:label> </td>
							<td>
								<s:url action="vnd/eauction" id="eauc"></s:url>
								<a href="${eauc }">${eauction }</a>
							</td>
						</tr>
						<tr>
							<td width="150px;"><s:label key="vnd.dash.quote"></s:label> </td>
							<td>
								<s:url action="vnd/penawaranRfq" id="penawaranRfq"></s:url>
								<a href="${penawaranRfq }">${sudahMenawarkan }</a>	
							</td>
							<td><s:label key="vnd.dash.history"></s:label></td>
							<td>
								<s:url action="vnd/daftarHistory" id="hist"></s:url>
								<a href="${hist }">${history }</a>
							</td>
						</tr>
					</table>
				</div>
				
				<div align="left">
					<fieldset>
						<legend style="color: blue">VENDOR'S NOTE</legend>
						<ul>
						<li>We guarantee that every quotation/transaction you make at this application will be secure, which means nobody (include buyer) can view your information/quotation until bid open</li>
						<li>It's <b style="color: red">highly recommended</b> that you'll be sending your quotation as soon as possible, this as a result will give you more time to verify your quotation further until closing time</li>
						<li>This application will be automatically force you to logout if your browser idle more than 20 minutes. To insure security, it's highly recommended to change your access password regularly every 30 or 60 days  </li>
						<li>This application is best view with <b style="color: red">Mozilla firefox 10.0 or later, and Google Chrome</b> with minimum screen resolution of 1024 x 768 pixel or better </li>
						<li>
							<a href="${contextPath }/upload/KetentuanUmumPTSB.pdf" target="_blank" style="font-weight: bolder;color:blue">Syarat dan Ketentuan Umum Pengadaan PT Semen Baturaja</a>
						</li>
					</ul>
					</fieldset>
					
				</div>
			</div>

			<div class="uibutton-toolbar extend"></div>
		</div>
	</div>
</body>
</html>