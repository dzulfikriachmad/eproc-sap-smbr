<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Profile</title>
	
		<script type="text/javascript">
		$(function() {
			$(".tabs").tabs();
		});
		//First Tab Load
		loadPage("<s:url action="ajax/companyInformation" />?tmpVndHeader.vendorId=${vendor.vendorId }", "#tabsContent");
	</script>
	<script type="text/javascript">
		function regCancel(){
			jQuery.facebox.close();
			window.location = "<s:url action="vnd/dashboard" />"; 
		}
	</script>
	<link rel="stylesheet" href="${contextPath}/assets/css/tabs-toggle.css" type="text/css" media="screen" />
</head>
<body>
	<!-- BreadCrumbs -->
	<div class="breadCrumb module">
		<ul>
	    	<li><s:a action="vnd/dashboard">Home</s:a></li> 
	        
	    </ul>
	</div>
	<!-- End of BreadCrumbs -->	
    
	<div id="right">
		<div class="section">
			<!-- Alert -->
			<s:if test="hasActionErrors()">
				<div class="message red"><s:actionerror/></div>
			</s:if>
			<s:if test="hasActionMessages()">
			    <div class="message green"><s:actionmessage/></div>
			</s:if>
			<c:if test="${vendor.vendorStatus=='E' and vendor.vendorNextPage<=9 }">
					<span class="message red" style="color:red">
					Profil Anda Belum Selesai di Ubah <br/>
					(Your Profile Editing need to be finished)
					</span>
				</c:if>
			<div class="box" >
				<div class="title">
					Detail Vendor
					<span class="hide"></span>
				</div>
				<div class="content nopadding" style="padding: 0px;">
					<div class="tabs">
						<div class="tabmenu">
							<ul> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/companyInformation" />?tmpVndHeader.vendorId=${vendor.vendorId }", "#tabsContent");'>Data Utama</a></li> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/legInf" />?tmpVndHeader.vendorId=${vendor.vendorId }", "#tabsContent");' >Data Legal</a></li> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/manInf" />?tmpVndHeader.vendorId=${vendor.vendorId }", "#tabsContent");'>Pengurus</a></li> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/finInf" />?tmpVndHeader.vendorId=${vendor.vendorId }", "#tabsContent");'>Keuangan</a></li> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/quaInf" />?tmpVndHeader.vendorId=${vendor.vendorId }", "#tabsContent");'>Barang / Jasa & Sertifikasi</a></li> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/resInf" />?tmpVndHeader.vendorId=${vendor.vendorId }", "#tabsContent");'>SDM</a></li> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/comFac" />?tmpVndHeader.vendorId=${vendor.vendorId }", "#tabsContent");'>Fasilitas / Peralatan</a></li> 
								<li><a href="javascript:void(0)" onclick='loadPage("<s:url action="ajax/proExp" />?tmpVndHeader.vendorId=${vendor.vendorId }", "#tabsContent");'>Pengalaman Proyek</a></li>  
							</ul>
						</div>
						
						<div id="tabsContent">
							 
						</div>
								
					</div>
				</div>
			</div>
					
			<s:form action="vnd/viewProfile" id="procForm" name="procForm" enctype="multipart/form-data" method="post">
				<s:hidden id="komentarVal" value="Edit Data"></s:hidden>
				<div class="uibutton-toolbar extend">
					<s:submit name="edit" id="edit" onclick="actionAlertVnd('edit','Edit Profile');return false;" value="Edit Profile" cssClass="uibutton" ></s:submit>
					<a href="javascript:void(0)" class="uibutton" onclick="confirmPopUp('regCancel();', 'Batal', 'Apakah anda yakin untuk membatalkan transaksi ?', 'Ya', 'Tidak');"><s:text name="cancel"></s:text> </a>					
				</div>
						
			</s:form>
			
		</div>
	</div>
</body>
</html>