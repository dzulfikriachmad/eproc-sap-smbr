function load_into_box(page,dt){
    $.ajax({
        url: page,
        data: dt,
        beforeSend: function(){
        	jQuery.facebox('<div style="padding: 17px; font-size: 36px;">Loading....</div>');
        },
        success: function(response){			
            jQuery.facebox(response);
        },
        type:"post",
        dataType:"html"  		
    });

    return false;
}

function load_into_box_alert(title,page,ok,dt){
    $.ajax({
        url: page,
        data: dt,
        beforeSend: function(){
        	jQuery.facebox('<div style="padding: 17px; font-size: 36px;">Loading....</div>');
        },
        success: function(response){	
//        	jQuery.facebox.close();
        	alertPopUp(title, response, ok);
        },
        type:"post",
        dataType:"html"  		
    });

    return false;
}

function sendForm(formObj,action,responseDIV){
    $.ajax({
        url: action, 
        data: $(formObj.elements).serialize(),
        beforeSend: function(){
        	jQuery.facebox.close();
        	jQuery.facebox('<div style="padding: 17px; font-size: 36px;">Loading....</div>');
        },
        success: function(response){
        		jQuery.facebox.close();
                $(responseDIV).html(response);
            },
        type: "post", 
        dataType: "html"
    }); 

    return false;
}

function sendFormAlertPopUp(formObj,action,responseDIV){
	var image_load = "<div style='text-align: center;' ><img src='"+imageLoading+"' /></div>";
    $.ajax({
        url: action, 
        data: $(formObj.elements).serialize(),
        beforeSend: function(){
//        	jQuery.facebox.close();
//        	jQuery.facebox('<div style="padding: 17px; font-size: 36px;">Loading....</div>');
        	$(responseDIV).html(image_load);
        },
        success: function(response){
//        		jQuery.facebox.close();
                $(responseDIV).html(response);
            },
        type: "post", 
        dataType: "html"
    }); 
    return false;
}

function confirmPopUp(command, title, text, yes, no){
	var html = "<div class='fbox_header'>"+title+"</div>" +
			"<div class='fbox_container'><div class='fbox_content'>" +
			text+
			"</div><div class='fbox_footer'>" +
			"<a class=\"uibutton confirm\" href='javascript:void(0)' onclick='"+command+"'>"+yes+"</a>" +
			"<a class=\"uibutton\" href='javascript:void(0)' onclick='jQuery.facebox.close()'>"+no+"</a>" +
			"</div></div>";
	jQuery.facebox(html);
}

function alertPopUp(title, text, ok){
	var html = "<div class='fbox_header'>"+title+"</div>" +
			"<div class='fbox_container'><div class='fbox_content'>" +
			text+
			"</div><div class='fbox_footer'>" +
			"<a class=\"uibutton confirm\" href='javascript:void(0)' onclick='jQuery.facebox.close()'>"+ok+"</a>" +
			"</div></div>";
	jQuery.facebox(html);
}

function deleteObj(action, data, responseDIV){
	$.ajax({
        url: action, 
        data: data,
        beforeSend: function(){
        	jQuery.facebox.close();
        	jQuery.facebox('<div style="padding: 17px; font-size: 36px;">Loading....</div>');
        },
        success: function(response){
                $(responseDIV).html(response);
        },
        type: "post", 
        dataType: "html"
    }); 
}

function deletePopUp(action, data, responseDIV){
	var image_load = "<div style='text-align: center;' ><img src='"+imageLoading+"' /></div>";
	$.ajax({
        url: action, 
        data: data,
        beforeSend: function(){
        	$(responseDIV).html(image_load);
        },
        success: function(response){
                $(responseDIV).html(response);
        },
        type: "post", 
        dataType: "html"
    }); 
}

function loadPage(page,div){
	var image_load = "<div style='text-align: center; margin-top: 20px;'><img src='"+imageLoading+"'/></div>";
        $.ajax({
            url: page,
            beforeSend: function(){
                $(div).html(image_load);
            },
            success: function(response){
                $(div).html(response);
            },
            dataType:"html"  		
        });
        return false;
}

function IsNumeric(strString){
	if(!/\D/.test(strString)) return true;//IF NUMBER
	else if(/^\d+\,\d+$/.test(strString)) return true;//IF A DECIMAL NUMBER HAVING AN INTEGER ON EITHER SIDE OF THE DOT(.)
	else if(/^\d+\.\d+$/.test(strString)) return true;
	else return false;
}

function IsReal(strString){
	if(!/\D/.test(strString)) return true;//IF NUMBER
	else return false;
}

function isDate(str){
	var datePat = /^(0[1-9]|[12][0-9]|3[01])(\/)(0[1-9]|1[012])(\/)(\d{2}|\d{4})$/;
	var matchArray = str.match(datePat); // is the format ok?

	if (matchArray == null)return false;else return true;
}

//Currencry
var acceptChar = "1234567890.";		
function formatNumber(obj){
	var str = obj.value;
	if(str.toString().indexOf(".")==str.length-1){
		if(str.length==1)
			obj.value = "0.";
		return;
	}
	
	str = str.replace(",","");
	var formattedStr = '';
	var commas = false;
	if(str.length==1 && str=='0'){
		obj.value='';
		return;
	}
	for(var x=0;x<str.length;x++){
		var ch1 = str.charAt(x);
		if(acceptChar.indexOf(ch1)!=-1){
			if(ch1=='.'){
				if(!commas)
					formattedStr = formattedStr.concat(ch1.toString());
				commas = true;
			}else{
				formattedStr = formattedStr.concat(ch1.toString());
			}
		}
	}
	var preCommas = "";
	var afterCommas = "";
	var idxCommas = formattedStr.indexOf(".");
	if(idxCommas!=-1){
		preCommas = formattedStr.substring(0,idxCommas);
		afterCommas = formattedStr.substring(idxCommas+1,formattedStr.length);
	}else{
		preCommas = formattedStr;
	}
	var lengthBig = preCommas.toString().length;
	var divs = parseInt(lengthBig%3);
	var all = parseInt(lengthBig/3);
	
	var finalStr = "";
	if(all>0){
		if(divs>0){
			finalStr = formattedStr.substr(0,divs);
			//alert("div > 0");
		}
		for(var x=0;x<all;x++){
			var idx = (x*3)+divs;
			var add = formattedStr.substr(idx,3);
			//alert("idx "+idx+" add "+add);
			if(finalStr.length==0)
				finalStr = add;
			else
				finalStr = finalStr.concat(",").concat(add);
		}
	}else{
		finalStr = formattedStr;
	}
	
	if(afterCommas.length>0 && finalStr.indexOf(",")!=-1)
		obj.value = finalStr.concat(".").concat(afterCommas);
	else
		obj.value = finalStr;
}

function loadingScreen(){
	jQuery.facebox('<div style="padding: 17px; font-size: 36px;">Loading....</div>');
}

function upper(obj){
	obj.value = obj.value.toUpperCase();
}