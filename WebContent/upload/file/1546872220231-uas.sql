
create table trapesium(
s1 NUMBER,
s2 NUMBER,
s3 NUMBER,
s4 NUMBER,
t NUMBER
);

insert into trapesium values (8,16,2,5,4);


declare
luasTrapesium number;
kelilingTrapesium number;
begin
for trpsium in(select * from trapesium)
loop
luasTrapesium:= (trpsium.s1+trpsium.s2)*(trpsium.t)/2;
kelilingTrapesium:= trpsium.s1+trpsium.s2+trpsium.s3+trpsium.s4;
dbms_output.put_line('LUAS TRAPESIUM MENGGUNAKAN SISI 1 DAN SISI 2');
dbms_output.put_line('Sisi 1: '|| trpsium.s1);
dbms_output.put_line('Sisi 2: '|| trpsium.s2);
dbms_output.put_line('Tinggi: '|| trpsium.t);
dbms_output.put_line('Luas: '|| luasTrapesium);

dbms_output.put_line('KELILING TRAPESIUM');

dbms_output.put_line('Sisi 1: '|| trpsium.s1);
dbms_output.put_line('Sisi 2: '|| trpsium.s2);
dbms_output.put_line('Sisi 3: '|| trpsium.s3);
dbms_output.put_line('Sisi 4: '|| trpsium.s4);
dbms_output.put_line('Tinggi: '|| trpsium.t);
dbms_output.put_line('Keliling: '|| kelilingTrapesium);
end loop;
end;