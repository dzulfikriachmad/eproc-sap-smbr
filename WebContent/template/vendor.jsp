<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	<title><decorator:title default="iProc | eProcurement System" /></title>
	
	<link rel="stylesheet" href="${contextPath}/assets/css/base.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="${contextPath}/assets/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="${contextPath}/assets/css/pepper-grinder/jquery-ui.css" type="text/css" media="screen" />
	
	<link rel="stylesheet" href="${contextPath}/assets/css/forms.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="${contextPath}/assets/css/datatables.css" type="text/css" media="screen" />
	
	<script type="text/javascript" src="${contextPath}/assets/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.ui-1.8.17.js"></script>	
	<script type="text/javascript" src="${contextPath}/assets/js/jquery-ui-i18n.js"></script> 
<%-- 	<script type="text/javascript" src="${contextPath}/assets/js/jquery.ui.select.js"></script>  --%>

	<script type="text/javascript" src="${contextPath}/assets/js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.validate-<s:text name="lang.iso"/>.js"></script>
	
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.customInput.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.datatables.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.elastic.source.js"></script>
	<%-- <script type="text/javascript" src="${contextPath}/assets/js/jquery.filestyle.mini.js"></script> --%>	
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.placeholder.min.js"></script>
	
	<script type="text/javascript" src="${contextPath}/assets/js/chosen/chosen.jquery.js"></script>
	<link href="${contextPath}/assets/js/chosen/chosen.css" rel="stylesheet" type="text/css" />
	
	<script type="text/javascript" src="${contextPath}/assets/js/facebox/facebox.js"></script>
	<link rel="stylesheet" href="${contextPath}/assets/js/facebox/facebox.css" type="text/css" media="screen" />
	<script type="text/javascript" src="${contextPath}/assets/js/apps.js"></script>
	
	
	
	<!-- Date Picker -->
	<script type="text/javascript">
		$(document).ready(function() {
			$.datepicker.setDefaults($.datepicker.regional['in_ID']);
			$("#tanggal").datepicker({
				changeYear: true,
				changeMonth: true,
				yearRange: '1970:2000'
			});
			
			getServerTime();
		});
		
		//Server Time
		function getServerTime(){
			$.ajax({
		        url: "<s:url action="dateJson" />",
		        success: function(response){
		        	if(response.tanggal != ""){
		        		var tanggal = response.tanggal;
		        		var waktu = response.waktu.split(":");
		        		
		        		var jam = waktu[0];
		    	    	var menit = waktu[1];
		    	    	var detik = waktu[2];
		        		setTime(tanggal, jam, menit, detik);
		        	}
		        },
		        type: "post", 
		        dataType: "json"
		    }); 
		}
	    
		function setTime(tanggal, jam, menit, detik){
	    	if(detik == 60){
	    		menit = parseFloat(menit) +1;
	    		detik = 0;
	    	}
	    	if(menit == 60){
	    		jam = parseFloat(jam) +1;
	    		menit = 0;
	    	}
	    	if(jam == 24){
	    		jam = 0;
	    	}
	    	
	    	if(detik.toString().length == 1)detik = "0" + detik;
	    	if(menit.toString().length == 1)menit = "0" + menit;
	    	if(jam.toString().length == 1)jam = "0" + jam;
	    	
	    	var waktu = jam + ":" + menit + ":" + detik;
	    	var serverTime = tanggal + " | " + waktu;
	    	$('#time').html(serverTime);
	    	
	    	setTimeout("setTime('"+tanggal+"', '" + parseFloat(jam) + "', '" + parseFloat(menit) + "', '" + (parseFloat(detik)+1) + "')",1000);
	    }
		
		
		function actionAlertVnd(idButton, action){
	    	$("#"+idButton).removeAttr("onclick");
	    	var komen = $("#komentarVal").val();
	    	var html = "<div class='fbox_header'>Konfirmasi Persetujuan</div>" +
			"<div class='fbox_container'><div class='fbox_content'>" +
			"Apakah anda yakin akan "+action+" proses ini:?<br /><br /><b>"+
			komen+
			"</b></div><div class='fbox_footer'>" +
			"<a class=\"uibutton confirm\" href='javascript:void(0)' onclick=\"setujuProses('"+idButton+"','"+action+"');\">Ya</a>" +
			"<a class=\"uibutton\" href='javascript:void(0)' onclick=\"batalProses('"+idButton+"','"+action+"')\">Tidak</a>" +
			"</div></div>";
			jQuery.facebox(html);
			return false;
	    }
	    
	    function setujuProses(idButton,action){
	    	jQuery.facebox.close();
	    	$("#"+idButton).click();
	    	setTimeout(function(){
				$("#"+idButton).attr("onclick","actionAlert('"+idButton+"','"+action+"');return false;");
	    		},600);
	    }
	    
	    function batalProses(idButton,action){
			$("#"+idButton).attr("onclick","actionAlert('"+idButton+"','"+action+"');return false;");
	    	jQuery.facebox.close();
	    }
	</script>
	
	<!-- BreadCumbs -->
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.jBreadCrumb.1.1.js"></script>
	<script type="text/javascript">
    	jQuery(document).ready(function(){
    		jQuery(".breadCrumb").jBreadCrumb(); 
        });
    </script>
	
	<!-- Cufon -->
	<script type="text/javascript" src="${contextPath}/assets/js/font/cufon-yui.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/font/Chopin_Script_400.font.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/font/Caviar_Dreams.font.js"></script>
	<script type="text/javascript">
	 	Cufon.replace('h1', {fontFamily: 'Chopin Script'});
	    Cufon.replace('h2', {fontFamily: 'Caviar Dreams'});
	    Cufon.replace('h3', {fontFamily: 'Chopin Script'});
	    Cufon.replace('footer', {fontFamily: 'Caviar Dreams'});
	    Cufon.replace('.headerLogo', {fontFamily: 'Caviar Dreams'});
	    
	    var imageLoading = "${contextPath}/assets/images/loading/loading-circle.gif";
	    var contextPath = "${contextPath}";
	    var isLoggedIn = false;
	</script>
	<s:if test="%{#session.vendor.vendorId != ''}"><script>var isLoggedIn = true;</script></s:if>
	
	<script type="text/javascript">
		//regCancel Action
		function regCancel(){
			jQuery.facebox.close();
			window.location = "<s:url action="vnd/logout" />"; 
		}
	</script>

	<script type="text/javascript" src="${contextPath}/assets/js/custom.js"></script>
	<!-- uplaod file -->
    <script type="text/javascript" src="${contextPath}/assets/js/jquery.form.js"></script>
    <!-- FOrmatter -->
	<script type="text/javascript" src="${contextPath}/assets/js/autoNumeric.js"></script>
	<decorator:head />
</head>
<body>

<div class="topbar"> 
    <div class="content"> 
    	<div style="padding:10px 0px; float: left; width: 350px">
			<s:text name="index.language" /> : 
				<a href="?request_locale=in_ID" title="Bahasa Indonesia" onclick="loadingScreen();"><img src="${contextPath}/assets/images/flag/id.png" /></a>
				<a href="?request_locale=en_US" title="English Language (US)" onclick="loadingScreen();"><img src="${contextPath}/assets/images/flag/us.png" /></a>
		</div>		
		<div class="serverTime">
			<!-- Waktu Server : 24 Feb 2012 | 05:00 WIB -->
			<span id="time"></span> WIB
		</div>				
		<div style="float: right; width: 350px; position: relative;">
			<s:if test="%{#session.vendor.vendorId != ''}">
				<ul class="topMenu">
					<%-- <li><s:a action="vnd/home">Dashboard</s:a></li> --%>
					<li><s:a href="#">${fn:toUpperCase(sessionScope.vendor.vendorName) }</s:a></li>
					<li id="loginBox" class="last"><span class="loginBoxToggle">&nbsp;&nbsp;&nbsp;&nbsp;</span></li>
				</ul>	
				<div class="clear"></div>
				<div id="loginBoxContent">
					<div class="topMenuDrop" onclick="load_into_box('<s:url action="ajax/vnd/changePassword" />')"><s:text name="login.changePassword" /></div>
					<div class="topMenuDrop" onclick="window.location=&quot;<s:url action="vnd/logout" />&quot;"><s:text name="login.logout" /></div><hr />
					<%-- <div class="topMenuDrop" onclick="window.location=&quot;<s:url action="index" />#maintenance&quot;"><s:text name="help" /></div> --%>
				</div> 
			</s:if>
			<s:else>
				<ul class="topMenu">
					<li class="last">
						<s:a action="vnd/login">Login</s:a> &nbsp;
					</li>
				</ul> 
			</s:else>
		</div>
    </div>  
</div>
<div class="clear"></div>

<div class="header">
	&nbsp;
	<div class="headerLogo">
		<img alt="header" src="${contextPath }/assets/images/logo3.png"/> &nbsp;
	</div>
</div>

<!-- Main Menu -->
<nav>
	<ul>
		<c:if test="${sessionScope.vendor.vendorStatus=='A' or sessionScope.vendor.vendorStatus=='E' or sessionScope.vendor.vendorStatus=='EE' }">
		<s:if test="%{#session.vendor.vendorId != '' }">
			<li><s:a action="vnd/dashboard"><s:label key="vnd.menu.home"></s:label> </s:a> </li>
			<li><a href="javascript:void(0)"><s:label key="vnd.menu.procurement"></s:label> </a>
				<ul>
					<li><s:a action="vnd/daftarRfq"><s:label key="vnd.menu.procregistration"></s:label> </s:a>  </li>
					<li><s:a action="vnd/penawaranRfq"><s:label key="vnd.menu.quotation"></s:label> </s:a>  </li>
					<li><s:a action="vnd/negosiasi"><s:label key="vnd.menu.negotiation"></s:label> </s:a>  </li>
					<li><s:a action="vnd/sanggahan"><s:label key="vnd.menu.sanggahan"></s:label> </s:a>  </li>
					<li><s:a action="vnd/eauction"><s:label key="vnd.menu.eauction"></s:label> </s:a>  </li>
					<li><s:a action="vnd/daftarHistory"><s:label key="vnd.menu.history"></s:label> </s:a>  </li>
					
				</ul>
			</li>
			<li><a href="javascript:void(0)"><s:label key="vnd.menu.profile"></s:label> 
				<ul>
					<li><s:a action="vnd/viewProfile"><s:label key="vnd.menu.viewprofile"></s:label> </s:a></li>
				</ul>
			</li>
		</s:if>
		</c:if>	
	</ul>
</nav>
<!-- End of Main Menu -->

<div id="wrapper">
	<div id="container">
	
		<!-- Content -->			
		<decorator:body />
		<!-- End of Decoretor -->	
		
		<div class="clear"></div>
		<div id="toTop">^ Ke Atas</div>
	</div>
</div> 

<div class="clear"></div>
<footer>
	<h2>Copyright &copy; 2013<br />iProc - Procurement Module</h2>
</footer>
</body>
</html>