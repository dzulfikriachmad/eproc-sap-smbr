<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	<title><decorator:title default="SAFEER | Procurement" /></title>
	
	<link rel="stylesheet" href="${contextPath}/assets/css/base.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="${contextPath}/assets/css/style.css" type="text/css" media="screen" />
	<%-- <link rel="stylesheet" href="${contextPath}/assets/css/pepper-grinder/jquery-ui.css" type="text/css" media="screen" /> --%>
	
	<link rel="stylesheet" href="${contextPath}/assets/css/forms.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="${contextPath}/assets/css/datatables.css" type="text/css" media="screen" />
	
	<script type="text/javascript" src="${contextPath}/assets/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.ui-1.8.17.js"></script>	
	<script type="text/javascript" src="${contextPath}/assets/js/jquery-ui-i18n.js"></script> 
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.ui.select.js"></script> 
	
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.customInput.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.datatables.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.elastic.source.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.filestyle.mini.js"></script>	
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.placeholder.min.js"></script>
	
	<!-- Date Picker -->
	<script type="text/javascript">
		$(document).ready(function() {
			$.datepicker.setDefaults($.datepicker.regional['in_ID']);
			$("#tanggal").datepicker({
				changeYear: true,
				changeMonth: true,
				yearRange: '1970:2000'
			});
			
			getServerTime();
		});
		
		//Server Time
		function getServerTime(){
			$.ajax({
		        url: "<s:url action="dateJson" />",
		        success: function(response){
		        	if(response.tanggal != ""){
		        		var tanggal = response.tanggal;
		        		var waktu = response.waktu.split(":");
		        		
		        		var jam = waktu[0];
		    	    	var menit = waktu[1];
		    	    	var detik = waktu[2];
		        		setTime(tanggal, jam, menit, detik);
		        	}
		        },
		        type: "post", 
		        dataType: "json"
		    }); 
		}
	    
		function setTime(tanggal, jam, menit, detik){
	    	if(detik == 60){
	    		menit = parseFloat(menit) +1;
	    		detik = 0;
	    	}
	    	if(menit == 60){
	    		jam = parseFloat(jam) +1;
	    		menit = 0;
	    	}
	    	if(jam == 24){
	    		jam = 0;
	    	}
	    	
	    	if(detik.toString().length == 1)detik = "0" + detik;
	    	if(menit.toString().length == 1)menit = "0" + menit;
	    	if(jam.toString().length == 1)jam = "0" + jam;
	    	
	    	var waktu = jam + ":" + menit + ":" + detik;
	    	var serverTime = tanggal + " | " + waktu;
	    	$('#time').html(serverTime);
	    	
	    	setTimeout("setTime('"+tanggal+"', '" + parseFloat(jam) + "', '" + parseFloat(menit) + "', '" + (parseFloat(detik)+1) + "')",1000);
	    }
	</script>
	
	<!-- Cufon -->
	<script type="text/javascript" src="${contextPath}/assets/js/font/cufon-yui.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/font/Chopin_Script_400.font.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/font/Caviar_Dreams.font.js"></script>
	<script type="text/javascript">
	 	Cufon.replace('h1', {fontFamily: 'Chopin Script'});
	    Cufon.replace('h2', {fontFamily: 'Chopin Script'});
	    Cufon.replace('h3', {fontFamily: 'Chopin Script'});
	    Cufon.replace('footer', {fontFamily: 'Caviar Dreams'});
	    
	    var imageLoading = "${contextPath}/assets/images/loading/loading-circle.gif";
	    var contextPath = "${contextPath}";
	    var isLoggedIn = false;
	</script>
	<s:if test="%{#session.user.id != ''}"><script>var isLoggedIn = true;</script></s:if>

	<script type="text/javascript" src="${contextPath}/assets/js/custom.js"></script>
	<decorator:head />
</head>
<body>

<div class="topbar"> 
    <div class="content"> 
    	<div style="padding:10px 0px; float: left;">
			<s:text name="index.language" /> : 
				<a href="?request_locale=in_ID" title="Bahasa Indonesia"><img src="${contextPath}/assets/images/flag/id.png" /></a>
				<a href="?request_locale=en_US" title="English Language (US)"><img src="${contextPath}/assets/images/flag/us.png" /></a>	
		</div>	
		<div class="serverTime">
			<!-- Waktu Server : 24 Feb 2012 | 05:00 WIB -->
			Waktu Server : <span id="time"></span> WIB
		</div>			
		<div style="float: right; width: 400px; position: relative;">
			<s:if test="%{#session.user.id != ''}">
				<ul class="topMenu">
					<li><s:a action="home">Dashboard</s:a></li>
					<li><s:a action="user/%{#session.account.username}">${sessionScope.user.namaDepan } ${sessionScope.user.namaBelakang }</s:a></li>
					<li id="loginBox" class="last"><span class="loginBoxToggle">&nbsp;&nbsp;&nbsp;&nbsp;</span></li>
				</ul>	
				<div class="clear"></div>
				<div id="loginBoxContent">
					<div class="topMenuDrop" onclick="window.location=&quot;<s:url action="user/edit" />&quot;"><s:text name="label.menu.editProfile" /></div>
					<div class="topMenuDrop" onclick="window.location=&quot;<s:url action="user/setting" />&quot;"><s:text name="label.menu.setting" /></div>
					<div class="topMenuDrop" onclick="window.location=&quot;<s:url action="logout" />&quot;"><s:text name="label.menu.logout" /></div><hr />
					<div class="topMenuDrop" onclick="window.location=&quot;<s:url action="index" />#maintenance&quot;"><s:text name="label.menu.help" /></div>
				</div>
			</s:if>
			<s:else>
				<ul class="topMenu"><li id="loginBox" class="last">Testing <span class="loginBoxToggle">&nbsp;&nbsp;&nbsp;&nbsp;</span></li></ul> 
				<div id="loginBoxContent"></div>
			</s:else>
		</div>
    </div>  
</div>
<div class="clear"></div>

<div class="header">
	ini header
</div>

<!-- Main Menu -->
<nav>
	<ul>
		<li><a href="${contextPath }/">Beranda</a></li>
		<li><s:a href="#">Commodity Management</s:a> </li>
		<li class="sub"><s:a>Vendor Management</s:a>
			<ul>
				<li><s:a href="#">Daftar Mitra Kerja </s:a></li>
				<li class="sub"><s:a href="#">Verifikasi Mitra Kerja </s:a>
					<ul>
						<li><s:a action="vendor/verification" >Mitra Kerja Baru</s:a></li>
						<li><s:a >Perubahan Data</s:a> </li>
					</ul>
				</li>
				<li><s:a href="#">Persetujuan Mitra Kerja </s:a></li>
				<li><s:a href="#">Opsi </s:a></li>
				<li><s:a href="#">Kinerja Mitra Kerja </s:a></li>
			</ul>
		</li>
		<li><a href="#">Procurement Management</a></li>
		<li><a href="#">Contract Management</a></li>
		<li><a href="#">Report</a></li>
	</ul>
</nav>
<!-- End of Main Menu -->

<div id="wrapper">
	<div id="container">			
		<div id="right">
			<div class="section">
				<div class="message red">
					<span><b>Error</b>: This is a error message</span>

				</div>
				
				<div class="message orange">
					<span><b>Warning</b>: This is a warning message</span>
				</div>
				
				<div class="message green">
					<span><b>Succes</b>: This is a succes message</span>
				</div>

				
				<div class="message blue">
					<span><b>Information</b>: This is a information message</span>
				</div>
			</div>
			
			<div class="section">

				<div class="box">
					<div class="title">
						Checkboxes, radiobuttons and file upload
						<span class="hide"></span>
					</div>
					<div class="content">
						<form action="">
							<div class="row">
								<label>Checkboxes</label>

								<div class="right">
									<input type="checkbox" name="" value="" id="first-check" checked="checked" />
									<label for="first-check">Check on</label>
									
									<input type="checkbox" name="" value="" id="second-check" />
									<label for="second-check">Check off</label>
								</div>
							</div>
							<div class="row">

								<label>Radiobuttons</label>
								<div class="right">
									<input type="radio" name="radiobutton" id="radio-1" checked="checked" /> 
									<label for="radio-1">Radio on</label>
									
									<input type="radio" name="radiobutton" id="radio-2" /> 
									<label for="radio-2">Radio off</label>
								</div>
							</div>

							<div class="row">
								<label>File upload</label>
								<div class="right"><input type="file" class="file" /></div>
							</div>
							
							<div class="row">
								<label>Small selectmenu</label>
								<div class="right">
									<select>
										<option selected="selected" value="">2012</option>
										<option value="">2011</option>
										<option value="">2012010201201020120102012010</option>
										<option value="">2009</option>
										<option value="">2008</option>
										<option value="">2007</option>
									</select>
								</div>
							</div>
							
							<div class="row">
								<label>Big selectmenu</label>
								<div class="right">
									<select class="big">
										<option selected="selected" value="">2012</option>
										<option value="">2011</option>
										<option value="">2012010201201020120102012010</option>
										<option value="">2009</option>
										<option value="">2008</option>
										<option value="">2007</option>
									</select>
								</div>
							</div>
							
							<div class="row">
								<label>Textarea auto grow</label>

								<div class="right"><textarea rows="" cols="" class="grow" placeholder="This textarea is going to grow when you fill it with text." style="height : 100px;"></textarea></div>
							</div>
							
							<div class="row">
								<label>Text Input</label>
								<div class="right"><input type="text" value="" placeholder="Coba input box"  /></div>
							</div>
							
							<div class="row">
								<label>Datepicker</label>
								<div class="right"><input type="text" value="" id="tanggal" style="width: 100px;" /></div>
							</div>
							
							<div class="row">
								<label>Contoh Tombol Submit</label>
								<div class="right">
									<s:submit value="Contoh Submit 1" cssClass="uibutton confirm"></s:submit>
									<s:submit value="Contoh Submit 2" cssClass="uibutton special"></s:submit>
									<s:submit value="Contoh Submit 3" cssClass="uibutton large"></s:submit>
									<s:submit value="Contoh Submit 4" cssClass="uibutton"></s:submit>
								</div>
							</div>
							<div class="row">
								<label>Contoh Button</label>
								<div class="right">
									<a href="#" class="uibutton icon add">Contoh 1</a>
									<a href="#" class="uibutton icon edit">Contoh 2</a>
									<a href="#" class="uibutton icon prev">Contoh 3</a>
									<a href="#" class="uibutton icon next">Contoh 4</a>
									<a href="#" class="uibutton icon secure">Contoh 5</a>
								</div>
							</div>
							
						</form>
					</div> 
				</div>
			</div>
			
			<div class="section">

				<div class="box">
					<div class="title">
						Table with all options
						<span class="hide"></span>
					</div>
					<div class="content">
						<table class="all style1"> 
							<thead> 
								<tr>
									<th>Username</th>

									<th>Duration</th>
									<th>Date</th>
									<th>Last visit page</th>
								</tr>
							</thead>
							<tbody>
								<tr>

									<td>John Do</td>
									<td>10 min</td>
									<td>23 January 2012</td>
									<td><a href="#">Dashboard</a></td>
								</tr>
								<tr>
									<td>Hong Gildong</td>

									<td>3 min</td>
									<td>23 January 2012</td>
									<td><a href="#">Login</a></td>
								</tr>
								<tr>
									<td>Israel Israeli</td>
									<td>7 min</td>

									<td>23 January 2012</td>
									<td><a href="#">Our Company</a></td>
								</tr>
								<tr>
									<td>John Smith</td>
									<td>3 hours</td>
									<td>23 January 2012</td>

									<td><a href="#">Message inbox</a></td>
								</tr>
								<tr>
									<td>Luther Blissett</td>
									<td>41 min</td>
									<td>23 January 2012</td>
									<td><a href="#">My profile</a></td>

								</tr>
								<tr>
									<td>Tommy Atkins</td>
									<td>1 hour</td>
									<td>23 January 2012</td>
									<td><a href="#">Settings</a></td>
								</tr>

								<tr>
									<td>Average Joe</td>
									<td>39 min</td>
									<td>23 January 2012</td>
									<td><a href="#">Contact form</a></td>
								</tr>
								<tr>

									<td>Nomen nescio</td>
									<td>56 sec</td>
									<td>23 January 2012</td>
									<td><a href="#">Build a page</a></td>
								</tr>
								<tr>
									<td>Joe Shmoe</td>

									<td>45 min</td>
									<td>23 January 2012</td>
									<td><a href="#">My statics</a></td>
								</tr>
								<tr>
									<td>Jane Doe</td>
									<td>23 min</td>

									<td>23 January 2012</td>
									<td><a href="#">Dashboard</a></td>
								</tr>
								<tr>
									<td>John Do</td>
									<td>10 min</td>
									<td>23 January 2012</td>

									<td><a href="#">Dashboard</a></td>
								</tr>
								<tr>
									<td>Hong Gildong</td>
									<td>3 min</td>
									<td>23 January 2012</td>
									<td><a href="#">Login</a></td>

								</tr>
								<tr>
									<td>Israel Israeli</td>
									<td>7 min</td>
									<td>23 January 2012</td>
									<td><a href="#">Our Company</a></td>
								</tr>

								<tr>
									<td>John Smith</td>
									<td>3 hours</td>
									<td>23 January 2012</td>
									<td><a href="#">Message inbox</a></td>
								</tr>
								<tr>

									<td>Luther Blissett</td>
									<td>41 min</td>
									<td>23 January 2012</td>
									<td><a href="#">My profile</a></td>
								</tr>
								<tr>
									<td>Tommy Atkins</td>

									<td>1 hour</td>
									<td>23 January 2012</td>
									<td><a href="#">Settings</a></td>
								</tr>
								<tr>
									<td>Average Joe</td>
									<td>39 min</td>

									<td>23 January 2012</td>
									<td><a href="#">Contact form</a></td>
								</tr>
								<tr>
									<td>Nomen nescio</td>
									<td>56 sec</td>
									<td>23 January 2012</td>

									<td><a href="#">Build a page</a></td>
								</tr>
								<tr>
									<td>Joe Shmoe</td>
									<td>45 min</td>
									<td>23 January 2012</td>
									<td><a href="#">My statics</a></td>

								</tr>
								<tr>
									<td>Jane Doe</td>
									<td>23 min</td>
									<td>23 January 2012</td>
									<td><a href="#">Dashboard</a></td>
								</tr>

								<tr>
									<td>John Do</td>
									<td>10 min</td>
									<td>23 January 2012</td>
									<td><a href="#">Dashboard</a></td>
								</tr>
								<tr>

									<td>Hong Gildong</td>
									<td>3 min</td>
									<td>23 January 2012</td>
									<td><a href="#">Login</a></td>
								</tr>
								<tr>
									<td>Israel Israeli</td>

									<td>7 min</td>
									<td>23 January 2012</td>
									<td><a href="#">Our Company</a></td>
								</tr>
								<tr>
									<td>John Smith</td>
									<td>3 hours</td>

									<td>23 January 2012</td>
									<td><a href="#">Message inbox</a></td>
								</tr>
								<tr>
									<td>Luther Blissett</td>
									<td>41 min</td>
									<td>23 January 2012</td>

									<td><a href="#">My profile</a></td>
								</tr>
								<tr>
									<td>Tommy Atkins</td>
									<td>1 hour</td>
									<td>23 January 2012</td>
									<td><a href="#">Settings</a></td>

								</tr>
								<tr>
									<td>Average Joe</td>
									<td>39 min</td>
									<td>23 January 2012</td>
									<td><a href="#">Contact form</a></td>
								</tr>

								<tr>
									<td>Nomen nescio</td>
									<td>56 sec</td>
									<td>23 January 2012</td>
									<td><a href="#">Build a page</a></td>
								</tr>
								<tr>

									<td>Joe Shmoe</td>
									<td>45 min</td>
									<td>23 January 2012</td>
									<td><a href="#">My statics</a></td>
								</tr>
								<tr>
									<td>Jane Doe</td>

									<td>23 min</td>
									<td>23 January 2012</td>
									<td><a href="#">Dashboard</a></td>
								</tr>
							</tbody>
						</table>
					</div>

				</div>
			</div>
			<div id="toTop">^ Back to Top</div>
		
		</div>		
		<div class="clear"></div>
	</div>
</div> 

<div class="clear"></div>
<footer>
	<h2>Copyright &copy; 2012<br />SAFEER - Procurement Module</h2>
</footer>
</body>
</html>