<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	<title><decorator:title default="SAFEER | eProc System" /></title>
	
	<link rel="stylesheet" href="${contextPath}/assets/css/base.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="${contextPath}/assets/css/style.css" type="text/css" media="screen" />
	
	<link rel="stylesheet" href="${contextPath}/assets/css/forms.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="${contextPath}/assets/css/datatables.css" type="text/css" media="screen" />
	
	<script type="text/javascript" src="${contextPath}/assets/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.ui-1.8.17.js"></script>	
	<script type="text/javascript" src="${contextPath}/assets/js/jquery-ui-i18n.js"></script> 
<%-- 	<script type="text/javascript" src="${contextPath}/assets/js/jquery.ui.select.js"></script>  --%>

<script type="text/javascript" src="${contextPath}/assets/js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.validate-<s:text name="lang.iso"/>.js"></script>
	
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.customInput.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.datatables.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.elastic.source.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.filestyle.mini.js"></script>	
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.placeholder.min.js"></script>
	
	<script type="text/javascript" src="${contextPath}/assets/js/facebox/facebox.js"></script>
	<link rel="stylesheet" href="${contextPath}/assets/js/facebox/facebox.css" type="text/css" media="screen" />
	<script type="text/javascript" src="${contextPath}/assets/js/apps.js"></script>

	<!-- BreadCumbs -->
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.jBreadCrumb.1.1.js"></script>
	<script type="text/javascript">
    	jQuery(document).ready(function(){
    		jQuery(".breadCrumb").jBreadCrumb(); 
    		getServerTime();
        });
    	
    	//Server Time
		function getServerTime(){
			$.ajax({
		        url: "<s:url action="dateJson" />",
		        success: function(response){
		        	if(response.tanggal != ""){
		        		var tanggal = response.tanggal;
		        		var waktu = response.waktu.split(":");
		        		
		        		var jam = waktu[0];
		    	    	var menit = waktu[1];
		    	    	var detik = waktu[2];
		        		setTime(tanggal, jam, menit, detik);
		        	}
		        },
		        type: "post", 
		        dataType: "json"
		    }); 
		}
	    
		function setTime(tanggal, jam, menit, detik){
	    	if(detik == 60){
	    		menit = parseFloat(menit) +1;
	    		detik = 0;
	    	}
	    	if(menit == 60){
	    		jam = parseFloat(jam) +1;
	    		menit = 0;
	    	}
	    	if(jam == 24){
	    		jam = 0;
	    	}
	    	
	    	if(detik.toString().length == 1)detik = "0" + detik;
	    	if(menit.toString().length == 1)menit = "0" + menit;
	    	if(jam.toString().length == 1)jam = "0" + jam;
	    	
	    	var waktu = jam + ":" + menit + ":" + detik;
	    	var serverTime = tanggal + " | " + waktu;
	    	$('#time').html(serverTime);
	    	
	    	setTimeout("setTime('"+tanggal+"', '" + parseFloat(jam) + "', '" + parseFloat(menit) + "', '" + (parseFloat(detik)+1) + "')",1000);
	    }
    </script>
	
	<!-- Cufon -->
	<script type="text/javascript" src="${contextPath}/assets/js/font/cufon-yui.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/font/Chopin_Script_400.font.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/font/Caviar_Dreams.font.js"></script>
	<script type="text/javascript">
	 	Cufon.replace('h1', {fontFamily: 'Chopin Script'});
	    Cufon.replace('h2', {fontFamily: 'Caviar Dreams'});
	    Cufon.replace('h3', {fontFamily: 'Chopin Script'});
	    Cufon.replace('footer', {fontFamily: 'Caviar Dreams'});
	    Cufon.replace('.headerLogo', {fontFamily: 'Caviar Dreams'});
	    
	    var imageLoading = "${contextPath}/assets/images/loading/loading-circle.gif";
	    var contextPath = "${contextPath}";
	    var isLoggedIn = false;
	</script>
	<s:if test="%{#session.user.id != ''}"><script>var isLoggedIn = true;</script></s:if>

	<script type="text/javascript" src="${contextPath}/assets/js/custom.js"></script>
	<decorator:head />
</head>
<body>

<div class="topbar"> 
    <div class="content"> 
    	<div style="padding:10px 0px; float: left; width: 350px">
			<%-- <s:text name="index.language" /> : 
				<a href="?request_locale=in_ID" title="Bahasa Indonesia"><img src="${contextPath}/assets/images/flag/id.png" /></a>
				<a href="?request_locale=en_US" title="English Language (US)"><img src="${contextPath}/assets/images/flag/us.png" /></a> --%>	
			SAFEER
		</div>		
		<div class="serverTime">
			<!-- Waktu Server : 24 Feb 2012 | 05:00 WIB -->
			Waktu Server : <span id="time"></span> WIB
		</div>			
		<div style="float: right; width: 350px; position: relative;">
			<s:if test="%{#session.user.id != ''}">
				<ul class="topMenu">
					<li>${sessionScope.user.completeName }</li>
					<li id="loginBox" class="last"><span class="loginBoxToggle">&nbsp;&nbsp;&nbsp;&nbsp;</span></li>
				</ul>	
				<div class="clear"></div>
				<div id="loginBoxContent">
					<%-- <div class="topMenuDrop" onclick="window.location=&quot;<s:url action="user/setting" />&quot;"><s:text name="login.changePassword" /></div> --%>
					<div class="topMenuDrop" onclick="window.location=&quot;<s:url action="logout" />&quot;"><s:text name="login.logout" /></div><hr />
					<div class="topMenuDrop" onclick="window.location=&quot;<s:url action="index" />#maintenance&quot;"><s:text name="help" /></div>
				</div> 
			</s:if>
			<s:else>
				<ul class="topMenu">
					<li class="last">
						<s:a action="login">Login</s:a> &nbsp;
					</li>
				</ul> 
			</s:else>
		</div>
    </div>  
</div>
<div class="clear"></div>

<div class="header">
	&nbsp;
	<div class="headerLogo">
		SAFEER<br />
		e-Procurement Module
	</div>
</div>

<!-- Main Menu -->
<nav>
	<ul>
		<li><s:a action="dashboard">Beranda</s:a></li>
		<%-- <li><s:a href="#">Commodity Management</s:a> </li> --%>
		<li class="sub"><s:a href="javascript:void(0)">Manajemen Vendor</s:a>
			<ul>
				<li><s:a action="vendor/list">Daftar Mitra Kerja </s:a></li>
				<li><s:a action="vendor/verification" >Mitra Kerja Baru</s:a></li>
				<li><s:a action="vendor/activation">Persetujuan Mitra Kerja </s:a></li>
				
			</ul>
		</li>
		<li class="sub"><s:a href="javascript:void(0)">Manajemen Pengadaan</s:a>
			<ul>
				<li class="sub"><s:a href="javascript:void(0)">Permintaan Pengadaan</s:a>
					<ul>
						<li><s:a action="procurement/request">Pembuatan Permintaan (PR)</s:a></li>
						<li><s:a action="procurement/approval" >Persetujuan Permintaan</s:a></li>
					</ul>
				</li>
				<li class="sub"><s:a href="javascript:void(0)">Perencanaan Pengadaan</s:a>
					<ul>
						<li><s:a action="oe/estimator">Pemilihan Estimator</s:a></li>
						<li><s:a action="oe/create">Pembuatan OE</s:a></li>
						<%-- <li><s:a action="oe/approval">Persetujuan OE</s:a></li> --%>
					</ul>
				</li>
				<%--<li><s:a href="javascript:void(0)">Persiapan Pengadaan</s:a></li>
				<li><s:a href="javascript:void(0)">Proses Pengadaan</s:a></li>
				<li><s:a href="javascript:void(0)">Penetapan Pemenang</s:a></li>
				<li><s:a href="javascript:void(0)">Peralatan</s:a></li> --%>
			</ul>
		</li>
		<!-- <li><a href="#">Contract Management</a></li> -->
		<li class="sub"><a href="javascript:void(0)">Laporan</a>
			<ul>
				<li><s:a action="report/vendor">Statistik Vendor</s:a> </li>
				<li><s:a action="report/prc">Statistik Proses Pengadaan</s:a> </li>
			</ul>
		</li>
	</ul>
</nav>
<!-- End of Main Menu -->

<div id="wrapper">
	<div id="container">
				
		<!-- Content -->			
		<decorator:body />
		<!-- End of Decoretor -->	
		
		<div class="clear"></div>
		<div id="toTop">^ Ke Atas</div>
	</div>
</div> 

<div class="clear"></div>
<footer>
	<h2>Copyright &copy; 2012<br />SAFEER - Procurement Module</h2>
</footer>
</body>
</html>