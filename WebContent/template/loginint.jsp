
<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	<title><decorator:title default="SAFEER | Procurement Login" /></title>
	
	<link rel="stylesheet" href="${contextPath}/assets/css/base.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="${contextPath}/assets/css/style.css" type="text/css" media="screen" />
	<%-- <link rel="stylesheet" href="${contextPath}/assets/css/pepper-grinder/jquery-ui.css" type="text/css" media="screen" /> --%>
	
	<link rel="stylesheet" href="${contextPath}/assets/css/forms.css" type="text/css" media="screen" />
	
	<script type="text/javascript" src="${contextPath}/assets/js/jquery-1.7.1.js"></script>
	
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.customInput.js"></script>	
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.placeholder.min.js"></script>
		
	<!-- Cufon -->
	<script type="text/javascript" src="${contextPath}/assets/js/font/cufon-yui.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/font/Chopin_Script_400.font.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/font/Caviar_Dreams.font.js"></script>
	
	<script type="text/javascript" src="${contextPath}/assets/js/facebox/facebox.js"></script>
	<link rel="stylesheet" href="${contextPath}/assets/js/facebox/facebox.css" type="text/css" media="screen" />
	<script type="text/javascript" src="${contextPath}/assets/js/apps.js"></script>
	
	<script type="text/javascript">
	 	Cufon.replace('h1', {fontFamily: 'Chopin Script'});
	    Cufon.replace('h2', {fontFamily: 'Chopin Script'});
	    Cufon.replace('.title', {fontFamily: 'Chopin Script'});
	    Cufon.replace('footer', {fontFamily: 'Caviar Dreams'});
	    
	    var imageLoading = "${contextPath}/assets/images/loading/loading-circle.gif";
	    var contextPath = "${contextPath}";
	    var isLoggedIn = false;
	    
	    $(document).ready(function(){	
	    	//Hide Box
			$('.title .hide').showContent();		
			
			// INPUT PLACEHOLDER
			$('input[placeholder], textarea[placeholder]').placeholder();
			
			// SYSTEM MESSAGES
			$(".message").click(function () {
		      	$(this).fadeOut();
		    });
			
			getServerTime();
		});		
	    
	  //Server Time
		function getServerTime(){
			$.ajax({
		        url: "<s:url action="dateJson" />",
		        success: function(response){
		        	if(response.tanggal != ""){
		        		var tanggal = response.tanggal;
		        		var waktu = response.waktu.split(":");
		        		
		        		var jam = waktu[0];
		    	    	var menit = waktu[1];
		    	    	var detik = waktu[2];
		        		setTime(tanggal, jam, menit, detik);
		        	}
		        },
		        type: "post", 
		        dataType: "json"
		    }); 
		}
	    
		function setTime(tanggal, jam, menit, detik){
	    	if(detik == 60){
	    		menit = parseFloat(menit) +1;
	    		detik = 0;
	    	}
	    	if(menit == 60){
	    		jam = parseFloat(jam) +1;
	    		menit = 0;
	    	}
	    	if(jam == 24){
	    		jam = 0;
	    	}
	    	
	    	if(detik.toString().length == 1)detik = "0" + detik;
	    	if(menit.toString().length == 1)menit = "0" + menit;
	    	if(jam.toString().length == 1)jam = "0" + jam;
	    	
	    	var waktu = jam + ":" + menit + ":" + detik;
	    	var serverTime = tanggal + " | " + waktu;
	    	$('#time').html(serverTime);
	    	
	    	setTimeout("setTime('"+tanggal+"', '" + parseFloat(jam) + "', '" + parseFloat(menit) + "', '" + (parseFloat(detik)+1) + "')",1000);
	    }
	    
	    $.fn.showContent = function() {
			return this.each(function() {
				var box = $(this);
				var content = $(this).parent().next('.content');

				box.toggle(function() {
					content.slideUp(500);
				}, function() {
					content.slideDown(500);
				});

			});
		};
	</script>
	<s:if test="%{#session.user.id != ''}"><script>var isLoggedIn = true;</script></s:if>
	
	<%-- <script type="text/javascript" src="${contextPath}/assets/js/custom.js"></script> --%>
	<decorator:head />
</head>
<body>

<div class="topbar"> 
    <div class="content"> 
    	<div style="padding:10px 0px; float: left; width: 350px">
			<s:text name="index.language" /> : 
				<a href="?request_locale=in_ID" title="Bahasa Indonesia"><img src="${contextPath}/assets/images/flag/id.png" /></a>
				<!--  
				<a href="?request_locale=en_US" title="English Language (US)"><img src="${contextPath}/assets/images/flag/us.png" /></a>
				-->	
		</div>		
		<div class="serverTime">
			<!-- Waktu Server : 24 Feb 2012 | 05:00 WIB -->
			<span id="time"></span> WIB
		</div>		
		<div style="float: right; width: 350px; position: relative;">
			<ul class="topMenu">
				<li class="last"><s:a action="vnd/login" >Login Vendor &nbsp;</s:a></li>
				
			</ul>
		</div>
    </div>  
</div>
<div class="clear"></div>

<!-- 
<div class="header">
	ini header
</div> 
-->

<!-- Content -->
<decorator:body />
<!-- End of COntent -->

<div class="clear"></div>
<footer>
	<h2>Copyright &copy; 2013<br />iProc - Procurement Module</h2>
	<!--  
	<img alt="Logo Safeer" src="${contextPath }/assets/images/safeer.png" width="60px">
	-->
</footer>

</body>
</html>