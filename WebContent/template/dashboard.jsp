<%@ include file="/include/definitions.jsp" %>

<!DOCTYPE html> 
<html lang="id">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	<title><decorator:title default="iProc | eProcurement System" /></title>
	
	<link rel="stylesheet" href="${contextPath}/assets/css/base.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="${contextPath}/assets/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="${contextPath}/assets/css/pepper-grinder/jquery-ui.css" type="text/css" media="screen" />
	
	<link rel="stylesheet" href="${contextPath}/assets/css/forms.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="${contextPath}/assets/css/datatables.css" type="text/css" media="screen" />
	
	<script type="text/javascript" src="${contextPath}/assets/js/jquery-1.7.1.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.ui-1.8.17.js"></script>	
	<script type="text/javascript" src="${contextPath}/assets/js/jquery-ui-i18n.js"></script> 
<%-- 	<script type="text/javascript" src="${contextPath}/assets/js/jquery.ui.select.js"></script>  --%>

	<script type="text/javascript" src="${contextPath}/assets/js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.validate-<s:text name="lang.iso"/>.js"></script>
	
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.customInput.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.datatables.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.elastic.source.js"></script>
	<%-- <script type="text/javascript" src="${contextPath}/assets/js/jquery.filestyle.mini.js"></script> --%>	
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.placeholder.min.js"></script>
	
	<script type="text/javascript" src="${contextPath}/assets/js/chosen/chosen.jquery.js"></script>
	<link href="${contextPath}/assets/js/chosen/chosen.css" rel="stylesheet" type="text/css" />
	
	<!-- Icon => Font Awesome -->
	<link rel="stylesheet" href="${contextPath}/assets/css/font-awesome/4.0.3/css/font-awesome.min.css" type="text/css" />
	
	<script type="text/javascript" src="${contextPath}/assets/js/facebox/facebox.js"></script>
	<link rel="stylesheet" href="${contextPath}/assets/js/facebox/facebox.css" type="text/css" media="screen" />
	<script type="text/javascript" src="${contextPath}/assets/js/apps.js"></script>

	<!-- BreadCumbs -->
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/jquery.jBreadCrumb.1.1.js"></script>
	<script type="text/javascript">
    	jQuery(document).ready(function(){
    		jQuery(".breadCrumb").jBreadCrumb(); 
    		getServerTime();
            noBack();
            getMessageCount();
    	});
    </script>
	
	<!-- Cufon -->
	<script type="text/javascript" src="${contextPath}/assets/js/font/cufon-yui.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/font/Chopin_Script_400.font.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/font/Caviar_Dreams.font.js"></script>
	<script type="text/javascript">
	 	Cufon.replace('h1', {fontFamily: 'Chopin Script'});
	    Cufon.replace('h2', {fontFamily: 'Caviar Dreams'});
	    Cufon.replace('h3', {fontFamily: 'Chopin Script'});
	    Cufon.replace('footer', {fontFamily: 'Caviar Dreams'});
	    Cufon.replace('.headerLogo', {fontFamily: 'Caviar Dreams'});
	    
	    var imageLoading = "${contextPath}/assets/images/loading/loading-circle.gif";
	    var contextPath = "${contextPath}";
	    var isLoggedIn = false;
	    
	    function actionAlert(idButton, action){
	    	$("#"+idButton).removeAttr("onclick");
	    	var komen = $("#komentarVal").val();
	    	var html = "<div class='fbox_header'>Konfirmasi Persetujuan</div>" +
			"<div class='fbox_container'><div class='fbox_content'>" +
			"Apakah anda yakin akan "+action+" proses ini dengan komentar berikut?<br /><br /><b>"+
			komen+
			"</b></div><div class='fbox_footer'>" +
			"<a class=\"uibutton confirm\" href='javascript:void(0)' onclick=\"setujuProses('"+idButton+"','"+action+"');\">Ya</a>" +
			"<a class=\"uibutton\" href='javascript:void(0)' onclick=\"batalProses('"+idButton+"','"+action+"')\">Tidak</a>" +
			"</div></div>";
			jQuery.facebox(html);
			return false;
	    }
	    
	    function setujuProses(idButton,action){
	    	jQuery.facebox.close();
	    	$("#"+idButton).click();
	    	setTimeout(function(){
				$("#"+idButton).attr("onclick","actionAlert('"+idButton+"','"+action+"');return false;");
	    		},600);
	    }
	    
	    function batalProses(idButton,action){
			$("#"+idButton).attr("onclick","actionAlert('"+idButton+"','"+action+"');return false;");
	    	jQuery.facebox.close();
	    }
	    
	    //Server Time
		function getServerTime(){
			$.ajax({
		        url: "<s:url action="dateJson" />",
		        success: function(response){
		        	if(response.tanggal != ""){
		        		var tanggal = response.tanggal;
		        		var waktu = response.waktu.split(":");
		        		
		        		var jam = waktu[0];
		    	    	var menit = waktu[1];
		    	    	var detik = waktu[2];
		        		setTime(tanggal, jam, menit, detik);
		        	}
		        },
		        type: "post", 
		        dataType: "json"
		    }); 
		}
	    
	    function setTime(tanggal, jam, menit, detik){
	    	if(detik == 60){
	    		menit = parseFloat(menit) +1;
	    		detik = 0;
	    	}
	    	if(menit == 60){
	    		jam = parseFloat(jam) +1;
	    		menit = 0;
	    	}
	    	if(jam == 24){
	    		jam = 0;
	    	}
	    	
	    	if(detik.toString().length == 1)detik = "0" + detik;
	    	if(menit.toString().length == 1)menit = "0" + menit;
	    	if(jam.toString().length == 1)jam = "0" + jam;
	    	
	    	var waktu = jam + ":" + menit + ":" + detik;
	    	var serverTime = tanggal + " | " + waktu;
	    	$('#time').html(serverTime);
	    	
	    	setTimeout("setTime('"+tanggal+"', '" + parseFloat(jam) + "', '" + parseFloat(menit) + "', '" + (parseFloat(detik)+1) + "')",1000);
	    }

		window.history.forward();

        function noBack() { 
            window.history.forward(); 
        }

        function getMessageCount(){
			$.ajax({
		        url: "<s:url action="jsonMessageCount" />?status=${prcMainHeader.ppmId}",
		        success: function(response){
		        	var count = response.status;
		        	if(count>0){
		        		$('#chat').css("color","red");
		        		$('#chat').css("font-weight","bold");
		        	}
		        },
		        type: "post", 
		        dataType: "json"
		    }); 
		}
	</script>
	<s:if test="%{#session.user.id != ''}"><script>var isLoggedIn = true;</script></s:if>

	<script type="text/javascript" src="${contextPath}/assets/js/custom.js"></script>
	
	<!-- uplaod file -->
    <script type="text/javascript" src="${contextPath}/assets/js/jquery.form.js"></script>

    <!-- tree view --> 
	<script type="text/javascript" src="${contextPath}/assets/js/tree/jquery.cookie.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/tree/jquery.treeview.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/tree/inittree.js"></script>
	<link href="${contextPath}/assets/js/tree/jquery.treeview.css" rel="stylesheet" type="text/css" />
	
	<!-- time picker -->
	<script type="text/javascript" src="${contextPath}/assets/js/timepicker/jquery-ui-sliderAccess.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/timepicker/jquery-ui-timepicker-addon.js"></script>
	<link href="${contextPath}/assets/js/timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" />
	
	<!-- Fusion Chart -->
	<script type="text/javascript" src="${contextPath}/assets/js/fusion/FusionCharts.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/fusion/FusionChartsExportComponent.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/fusion/FusionCharts.HC.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/fusion/FusionCharts.HC.Charts.js"></script>
	<script type="text/javascript" src="${contextPath}/assets/js/fusion/FusionCharts.jqueryplugin.js"></script>
	
	
	<!-- FOrmatter -->
	<script type="text/javascript" src="${contextPath}/assets/js/autoNumeric.js"></script>
	<decorator:head />
</head>
<body>

<div class="topbar"> 
    <div class="content"> 
    	<div style="padding:10px 0px; float: left; width: 350px">
			<%-- <s:text name="index.language" /> : 
				<a href="?request_locale=in_ID" title="Bahasa Indonesia"><img src="${contextPath}/assets/images/flag/id.png" /></a>
				<a href="?request_locale=en_US" title="English Language (US)"><img src="${contextPath}/assets/images/flag/us.png" /></a> --%>	
			e-Procurement
		</div>		
		<div class="serverTime">
			<!-- Waktu Server : 24 Feb 2012 | 05:00 WIB -->
			<span id="time"></span> WIB
		</div>			
		<div style="float: right; width: 350px; position: relative;">
			<s:if test="%{#session.user.id != ''}">
				<ul class="topMenu">
					<li>${sessionScope.user.completeName }</li>
					<li id="loginBox" class="last"><span class="loginBoxToggle">&nbsp;&nbsp;&nbsp;&nbsp;</span></li>
				</ul>	
				<div class="clear"></div>
				<div id="loginBoxContent">
					<%-- <div class="topMenuDrop" onclick="window.location=&quot;<s:url action="user/setting" />&quot;"><s:text name="login.changePassword" /></div> --%>
					<div class="topMenuDrop" onclick="window.location=&quot;<s:url action="logout" />&quot;"><s:text name="login.logout" /></div>
					<div class="topMenuDrop" onclick="window.location=&quot;<s:url action="bypass/adm/gantiPassword" />&quot;">Ganti Password</div>
					<hr />
					<div class="topMenuDrop" onclick="window.location=&quot;<s:url action="index" />#maintenance&quot;"><s:text name="help" /></div>
				</div> 
			</s:if>
			<s:else>
				<ul class="topMenu">
					<li class="last">
						<s:a action="login">Login</s:a> &nbsp;
					</li>
				</ul> 
			</s:else>
		</div>
    </div>  
</div>
<div class="clear"></div>

<div class="header">
	&nbsp;
	
	<div class="headerLogo">
		<%-- <img alt="header" src="${contextPath }/assets/images/logo3.png"/> &nbsp; --%>
	</div>
</div>


<nav>
	<ul>
		<li><s:a action="dashboard"><span>BERANDA</span></s:a></li>
		<s:iterator value="#session.menu" var="i">
			<c:if test="${i.menuParent==null }">
				<li>
					<c:choose>
						<c:when test="${i.menuUrl == '#' || empty i.menuUrl }">
							<s:a href="javascript:void(0)">${i.menuLabel }</s:a>
						</c:when>
						<c:otherwise>
							<s:a action="%{#i.menuUrl}" onclick="FreezeScreen('Data sedang di proses');">${i.menuLabel }</s:a>
						</c:otherwise>
					</c:choose>
					<!-- second level -->
					<ul>
					<s:iterator value="#session.menu" var="i2">
						<c:if test="${i2.menuParent.id == i.id }">
							<li>	
								<c:choose>
									<c:when test="${i2.menuUrl == '#' || empty i2.menuUrl }">
										<s:a href="javascript:void(0)">${i2.menuLabel }</s:a>
									</c:when>
									<c:otherwise>
										<s:a action="%{#i2.menuUrl}" onclick="FreezeScreen('Data sedang di proses');">${i2.menuLabel }</s:a>
									</c:otherwise>
								</c:choose>
								
								<!-- third level -->
								<ul>
								<s:iterator value="#session.menu" var="i3">
									<c:if test="${i3.menuParent.id==i2.id }">
										<li>
											<s:a action="%{#i3.menuUrl}" onclick="FreezeScreen('Data sedang di proses');">${i3.menuLabel }</s:a>
										</li>
									</c:if>
								</s:iterator>
								</ul>
							</li>
						</c:if>
					</s:iterator>
					</ul>
				</li>
			</c:if>
		</s:iterator>
	</ul>
</nav>
<!-- Main Menu 
<nav>
	<ul>
		<li><s:a action="dashboard">Beranda</s:a></li>
		<li class="sub"><s:a href="javascript:void(0)">Manajemen Aplikasi</s:a>
			<ul>
				<li><s:a action="adm/listSatker">Daftar Satuan Kerja </s:a></li>
				<li><s:a action="adm/listKantor">Daftar Kantor </s:a></li>
				<li><s:a action="adm/listNegara">Daftar Negara </s:a></li>
				<li><s:a action="adm/listDokumen">Daftar Dokumen </s:a></li>
				<li><s:a action="adm/listRole">Daftar Role </s:a></li>
				<li><s:a action="adm/listMenu">Daftar Menu </s:a></li>
				<li><s:a action="adm/listUser">Daftar User </s:a></li>
				<li><s:a action="adm/listUom">Daftar UOM </s:a></li>
				<li><s:a action="adm/listProses">Daftar Proses </s:a></li>
				<li><s:a action="adm/listHirarkiProses">Daftar Hirarki Proses </s:a></li>
				<li><s:a action="adm/listCostCenter">Daftar Cost Center </s:a></li>
			</ul>
		</li>
		<%-- <li><s:a href="#">Commodity Management</s:a> </li> --%>
		<li class="sub"><s:a href="javascript:void(0)">Manajemen Vendor</s:a>
			<ul>
				<li><s:a action="vendor/list">Daftar Mitra Kerja </s:a></li>
				<li><s:a action="vendor/verification" >Mitra Kerja Baru</s:a></li>
				<li><s:a action="vendor/activation">Persetujuan Mitra Kerja </s:a></li>
				
			</ul>
		</li>
		<li class="sub"><s:a href="javascript:void(0)">Manajemen Pengadaan</s:a>
			<ul>
				<li class="sub"><s:a href="javascript:void(0)">Permintaan Pengadaan</s:a>
					<ul>
						<li><s:a action="procurement/request">Pembuatan Permintaan (PR)</s:a></li>
						<li><s:a action="procurement/approval" >Persetujuan Permintaan</s:a></li>
					</ul>
				</li>
				<li class="sub"><s:a href="javascript:void(0)">Perencanaan Pengadaan</s:a>
					<ul>
						<li><s:a action="oe/estimator">Pemilihan Estimator</s:a></li>
						<li><s:a action="oe/create">Pembuatan OE</s:a></li>
						<%-- <li><s:a action="oe/approval">Persetujuan OE</s:a></li> --%>
					</ul>
				</li>
				<%--<li><s:a href="javascript:void(0)">Persiapan Pengadaan</s:a></li>
				<li><s:a href="javascript:void(0)">Proses Pengadaan</s:a></li>
				<li><s:a href="javascript:void(0)">Penetapan Pemenang</s:a></li>
				<li><s:a href="javascript:void(0)">Peralatan</s:a></li> --%>
			</ul>
		</li>
		<!-- <li><a href="#">Contract Management</a></li> 
		<li class="sub"><a href="javascript:void(0)">Laporan</a>
			<ul>
				<li><s:a action="report/vendor">Statistik Vendor</s:a> </li>
				<li><s:a action="report/prc">Statistik Proses Pengadaan</s:a> </li>
			</ul>
		</li>
	</ul>
</nav>
-->
<!-- End of Main Menu -->

<div id="wrapper">
	<div id="container">
				
		<!-- Content -->			
		<decorator:body />
		<!-- End of Decoretor -->	
		
		<div class="clear"></div>
		<div id="toTop">^ Ke Atas</div>
	</div>
</div> 

<div class="clear"></div>
<footer>
	<h2>Copyright &copy; 2013<br />iProc - Procurement Module</h2>
</footer>
			
<!-- Freeze Screen -->
<div align="center" id="FreezePane" class="FreezePaneOff">
	<div class="FreezeOverlay"></div>
	<div id="InnerFreezePane" class="InnerFreezePane">
	</div>
</div>
<!-- End Of Freeze -->
</body>
</html>